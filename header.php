<?php
/**********************************************************************
* MODERN ELEGANCE WORDPRESS THEME
* (Ideal For Business And Personal Use: Portfolio or Blog)   
* 
* File name:   
*      header.php
* Brief:       
*      Header and navigation code for all theme files
* Author:      
*      DigitalCavalry
* Author URI:
*      http://themeforest.net/user/DigitalCavalry
* Contact:
*      digitalcavalry@gmail.com  
***********************************************************************/  

   ob_start();                          
?>  
 
<!DOCTYPE html>
<html <?php language_attributes(); ?> >
    <!-- Developed by Digital Cavalry 2012 (http://themeforest.net/user/DigitalCavalry) -->  
    <head>
        <meta name="author" content="DigitalCavalry" />
        <meta name="language" content="english" />                     
        <meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
        <meta name="generator" content="WordPress <?php bloginfo('version'); ?>" />
        <meta name="HandheldFriendly" content="True">
        <meta name="MobileOptimized" content="320">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
        <?php  
            GetDCCPInterface()->printHeadMeta();

            the_post();
            global $post;             
        ?>             
        <meta name="Robots" content="index, follow, all">
        <title> <?php if(is_home()) { ?>Memphis orthodontist – Dr. Fred Piper – Braces, TMJ therapy<?php } ?><?php bloginfo('name'); ?><?php wp_title(''); ?></title>
        <?php GetDCCPInterface()->getIGeneral()->renderFavicon(); ?>
       
        <!-- CSS (Cascading Style Sheets) Files -->
                                        
        <?php             $is_homepage = is_page_template('homepage.php');
            $is_accordion = false;
            $is_progress = false;
            $slider_type = GetDCCPInterface()->getIGeneral()->getSliderType(); ;
            
            if($is_homepage)
            {
                if(GetDCCPInterface()->getIGeneral()->showClientPanel())
                {
                    $client_slider_type = GetDCCPInterface()->getIClientPanel()->getValue('slider_type');
                    if($client_slider_type !== null)
                    {
                        $slider_type = $client_slider_type;    
                    }                
                }
                if($slider_type == CPGeneral::ACCORDION_SLIDER) { $is_accordion = true; }
                if($slider_type == CPGeneral::PROGRESS_SLIDER) { $is_progress = true; } 
            }       
        ?>
        <link type="text/css" rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" />  		        <link type="text/css" rel="stylesheet" href="<?php bloginfo("template_url"); ?>/custom/css/custom.css" />                      
		<link href='http://fonts.googleapis.com/css?family=Raleway:400,300,200,700' rel='stylesheet' type='text/css'>
        <link type="text/css" rel="stylesheet" href="<?php bloginfo("template_url"); ?>/css/pphoto/prettyPhoto.css" />  
        <?php GetDCCPInterface()->getThemeSkinCSSLink(); ?>
        <?php if($is_accordion) { ?> <link type="text/css" rel="stylesheet" href="<?php bloginfo("template_url"); ?>/css/slider_accordion.css" /> <?php } ?>
        <?php if($is_progress) { ?> <link type="text/css" rel="stylesheet" href="<?php bloginfo("template_url"); ?>/css/slider_progress.css" /> <?php } ?>       
        <?php GetDCCPInterface()->getSelectedFontLink(CMS_CSS_AREA); ?>                                                                                       
                                                                                                 
                                                                                                 
        <!-- JavaScript Files -->
        <?php
            $GLOBALS['dc_pagecommon_opt'] = null;
            $GLOBALS['dc_postcommon_opt'] = null;
            $is_page = is_page();
            $is_single = is_single();
            if($is_page) { $GLOBALS['dc_pagecommon_opt'] = get_post_meta($post->ID, 'pagecommon_opt', true); }
            if($is_single) { $GLOBALS['dc_postcommon_opt'] = get_post_meta($post->ID, 'post_common_opt', true); }
         
            $post_generate_ogp = false;
            if($is_single and $GLOBALS['dc_postcommon_opt'] != '') { $post_generate_ogp = (bool)$GLOBALS['dc_postcommon_opt']['post_generate_ogp_cbox']; }             
            // facebook like info and thumb
            if($is_single and GetDCCPInterface()->getIGeneral()->isFacebookShowed() and $post_generate_ogp)
            {
                $out = '';
                $out .= '<meta property="og:title" content="'.$post->post_title.'"/> ';
                $out .= '<meta property="og:type" content="article" /> ';
                $out .= '<meta property="og:url" content="'.get_permalink($post->ID).'" /> ';
                $out .= '<meta property="og:site_name" content="'.get_bloginfo('name').'"/> ';
                
                $desc = '';
                if($post->post_excerpt != '')
                {                  
                    $desc = $post->post_excerpt;     
                } else                
                {   
                    $desc = dcf_strNWords($post->post_content);
                }
                $out .= '<meta property="og:description" content="'.$desc.'"/> ';                
                
                if(GetDCCPInterface()->getIGeneral()->isFacebookThumbShowed())
                {
                    if(GetDCCPInterface()->getIGeneral()->isFacebookUseOwnThumb())
                    {
                        $image_url = GetDCCPInterface()->getIGeneral()->getFacebookThumbURL(); 
                        if($image_url != '')
                        {
                            $out .= '<meta property="og:image" content="'.$image_url.'"/> '; 
                        }   
                    } else
                    {
                        $post_opt = get_post_meta($post->ID, 'post_opt', true);
                        $post_opt['image_url'] = dcf_isNGGImageID($post_opt['image_url']); 
                        
                        if($post_opt['image_url'] != '')
                        {
                            $out .= '<meta property="og:image" content="'.$post_opt['image_url'].'"/> ';     
                        }
                    }             
                }
        								
                echo $out;
            }         
         
            if(is_singular()) 
            {   
                wp_enqueue_script('comment-reply');
            }          
            wp_head();       
            GetDCCPInterface()->getIGeneral()->customCSS();
            
            if($is_page and $GLOBALS['dc_pagecommon_opt'] != '')
            {
                if(trim($GLOBALS['dc_pagecommon_opt']['page_bg_img']) != '' and !GetDCCPInterface()->getIGeneral()->isThemeBgForced())
                {
                    $out = '';
                    $out .= '<style type="text/css">';
                        $out .= 'body { ';
                            $out .= 'background-image:url('.$GLOBALS['dc_pagecommon_opt']['page_bg_img'].');';
                            $out .= 'background-repeat:'.$GLOBALS['dc_pagecommon_opt']['page_bg_repeat'].';';        
                            $out .= 'background-position:center top;';
                            $out .= 'background-attachment:'.$GLOBALS['dc_pagecommon_opt']['page_bg_attachment'].';';
                        $out .= ' }';
                    $out .= '</style>';
                    
                    echo $out;
                }
                
                if((bool)$GLOBALS['dc_pagecommon_opt']['page_bg_color_use_cbox'] and !GetDCCPInterface()->getIGeneral()->isThemeBgColorForced())
                {
                    $out = '';
                    $out .= '<style type="text/css">';
                        $out .= 'body { ';
                            $out .= 'background-color:'.$GLOBALS['dc_pagecommon_opt']['page_bg_color'].';';
                        $out .= ' }';
                    $out .= '</style>';
                    
                    echo $out;                    
                }
            }
            
            if($is_single and $GLOBALS['dc_postcommon_opt'] != '')
            {
                if(trim($GLOBALS['dc_postcommon_opt']['post_bg_img']) != '' and !GetDCCPInterface()->getIGeneral()->isThemeBgForced())
                {
                    $out = '';
                    $out .= '<style type="text/css">';
                        $out .= 'body { ';
                            $out .= 'background-image:url('.$GLOBALS['dc_postcommon_opt']['post_bg_img'].');';
                            $out .= 'background-repeat:'.$GLOBALS['dc_postcommon_opt']['post_bg_repeat'].';';
                            $out .= 'background-position:center top;';
                            $out .= 'background-attachment:'.$GLOBALS['dc_postcommon_opt']['post_bg_attachment'].';';
                        $out .= ' }';
                    $out .= '</style>';
                    
                    echo $out;
                }
                
                if((bool)$GLOBALS['dc_postcommon_opt']['post_bg_color_use_cbox'] and !GetDCCPInterface()->getIGeneral()->isThemeBgColorForced())
                {
                    $out = '';
                    $out .= '<style type="text/css">';
                        $out .= 'body { ';
                            $out .= 'background-color:'.$GLOBALS['dc_postcommon_opt']['post_bg_color'].';';
                        $out .= ' }';
                    $out .= '</style>';
                    
                    echo $out;                    
                }                
            }            

            
            GetDCCPInterface()->getIMenu()->customCSS();
            if($is_accordion)
            {
                GetDCCPInterface()->getIAccordionSlider()->customCSS();
                GetDCCPInterface()->getIAccordionSlider()->javaScript();
            }
            if($is_progress)
            {
                GetDCCPInterface()->getIProgressSlider()->customCSS();
                GetDCCPInterface()->getIProgressSlider()->javaScript();
            }
            GetDCCPInterface()->getSelectedFontLink(CMS_JAVASCRIPT_AREA); 
            GetDCCPInterface()->getIGeneral()->printHeaderTrackingCode(); 
            
            
        ?>   
        <script type="text/javascript" src="<?php bloginfo("template_url"); ?>/custom/js/jquery.min.js"></script>
        <?php if(is_home()) { ?>
		<script type="text/javascript">				
			var $=jQuery;
			$(document).ready(function(){
				$(".sf-menu").css("left","117px");
				$("#navigation-wrapper ul.sf-menu li:first-child:contains('Home')").hide();
			});							
		</script>	
		<script type="text/javascript" src="<?php bloginfo("template_url"); ?>/custom/js/jquery.ui.core.js"></script>
		<script type="text/javascript" src="<?php bloginfo("template_url"); ?>/custom/js/jquery.ui.effect.js"></script>
		<script type="text/javascript" src="<?php bloginfo("template_url"); ?>/custom/js/jquery.ui.effect-slide.js"></script>
        <script type="text/javascript" src="<?php bloginfo("template_url"); ?>/custom/js/jquery.bxslider.min.js"></script>
        <link type="text/css" rel="stylesheet" href="<?php bloginfo("template_url"); ?>/custom/css/jquery.bxslider.css" /> 
	<?php } ?>  
    </head>
    <body>
    <div class="parent-container">
    <!-- Facebook -->
	<div id="fb-root"></div>
	<script>(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));</script>
	<!-- End Facebook -->

          
    <?php 
        global $wp_query;  
        $pageid = $post->ID;
        $GLOBALS['dc_post_id'] = $pageid;      
    
        GetDCCPInterface()->getIGeneral()->renderSplashScreen();
        GetDCCPInterface()->getIGeneral()->renderAnnouncementBar(); 
        GetDCCPInterface()->getIFloatingObjects()->renderObjects('browser-window');
        
        if(GetDCCPInterface()->getIGeneral()->showClientPanel())
        {
            GetDCCPInterface()->getIClientPanel()->renderPanel();
        }
    ?>             
    <a name="top-anchor"></a>
    <div id="content-top-empty-space">
        <?php GetDCCPInterface()->getIFloatingObjects()->renderObjects('content-top-empty-space'); ?> 
    </div>
        <?php GetDCCPInterface()->getIGeneral()->renderHeaderMenu(); ?>

    <div id="header-container">
        <a href="#sidr-right" id="sidr-right-link" class="hidden-large"><img src="<?php echo get_stylesheet_directory_uri(); ?>/custom/images/menu.png"></a>
        <a class="logo" href="<?php echo get_bloginfo('url'); ?>"></a>
        <?php            
             GetDCCPInterface()->getIGeneral()->renderSearchHPanel(CPGeneral::SPANEL_PLACE_ABOVE_MENU); 
             GetDCCPInterface()->getIGeneral()->renderHeaderIcons();
             GetDCCPInterface()->getIFloatingObjects()->renderObjects('header-container'); 
         ?>                                                                                                      		<a id="header-phone" href="tel:9017564316">Call for FREE Consultation<br />901.756.4316</a>
    </div> <!-- header-container -->

   <div id="content-menu-wrapper" class="visible-large"> 
    
    <?php         
            GetDCCPInterface()->getIMenu()->renderMenu();            
        ?> 
        
    </div>

    <?php
        if($is_page and $GLOBALS['dc_pagecommon_opt'] != '')
        {
            if((bool)$GLOBALS['dc_pagecommon_opt']['page_area_show_cbox'])
            {       
                $bg_color = '';
                if((bool)$GLOBALS['dc_pagecommon_opt']['page_area_bg_color_use_cbox'])
                {
                    $bg_color = 'background-color:'.$GLOBALS['dc_pagecommon_opt']['page_area_bg_color'].';';
                }
                $add_style = ' style="'.$bg_color.'width:'.$GLOBALS['dc_pagecommon_opt']['page_area_width'].'px;height:'.$GLOBALS['dc_pagecommon_opt']['page_area_height'].'px;" ';                                
                
                $out = '';
                $out .= '<div class="page-post-custom-area" '.$add_style.'>';                     
                    $out .= apply_filters('the_content', $GLOBALS['dc_pagecommon_opt']['page_area_content']); ;
                    $out .= GetDCCPInterface()->getIFloatingObjects()->renderObjects('page-post-custom-area', false);
                $out .= '</div>';
                echo $out;
            }    
        }
    ?>
<div id="page-wrapper">
    <div id="top-menu-and-search">
    
    <div style="padding-top: 10px;">
    <?php            
             GetDCCPInterface()->getIQuickNewsPanel()->renderPanel();
    ?>
    </div>
    
    <?php            
             GetDCCPInterface()->getIGeneral()->renderSearchHPanel(CPGeneral::SPANEL_PLACE_UNDER_MENU);
    ?>
    
    <div class="clear-both"></div></div>    

    
    
    
    
    <div id="content-wrapper"> 
    
    
            

            
             
    <div class="content-top-separator"></div>