<?php
/*
Template Name: Team
*/

/**********************************************************************
* MODERN ELEGANCE WORDPRESS THEME 
* (Ideal For Business And Personal Use: Portfolio or Blog)   
* 
* File name:   
*      team.php
* Brief:       
*      Theme team page template code
* Author:      
*      DigitalCavalry
* Author URI:
*      http://themeforest.net/user/DigitalCavalry
* Contact:
*      digitalcavalry@gmail.com   
***********************************************************************/ 

    get_header();                                                
?>

    <div id="content">
         
        <?php 
            $page_common_opt = $GLOBALS['dc_pagecommon_opt'];                        
            $team_opt = get_post_meta($post->ID, 'team_opt', true);
            
            
            if($team_opt['team_layout'] == CMS_TEAM_LAYOUT_FULL)
            {
                echo '<div class="page-width-full">';     
            } else
            {
                GetDCCPInterface()->getIGeneral()->includeSidebar($page_common_opt['page_sid'], $page_common_opt['page_sid_pos']); 
                if(GetDCCPInterface()->getIGeneral()->getSidebarGlobalPos($page_common_opt['page_sid_pos']) == CMS_SIDEBAR_RIGHT)
                {
                    echo '<div class="page-width-left">';                              
                } else
                {
                    echo '<div class="page-width-right">';     
                }
            }   
            dcf_naviTree($post->ID, 0);   
            
            $per_page = (int)$team_opt['team_per_page'];
            if($per_page == 0) { $per_page = -1; }
            
            $selected_members = trim($team_opt['team_members']);
            if($selected_members != '')
            {
                $selected_members = explode(',', $selected_members);        
            }
            
            
            echo '<h1>'.$post->post_title.'</h1>';
            the_content();
                      
            
            $paged = (get_query_var('paged')) ? get_query_var('paged') : 1; 
            $args = Array(
                'paged' => $paged,
                'post_type' => CPThemeCustomPosts::PT_MEMBER_POST,
                'post_status' => 'publish', 
                'posts_per_page' => $per_page
                );
                
            if($selected_members != '')
            {
                $args['post__in'] = $selected_members;
            }
                
            query_posts($args); 
            $max_page = $wp_query->max_num_pages; 
            
        if(have_posts())
        {
            if($team_opt['team_layout'] == CMS_TEAM_LAYOUT_FULL) 
            {
               $out = '';                 
                
                $counter = 0;
                while(have_posts()) 
                {
                    if($counter > 0 and (($counter % 6) == 0))
                    {
                        $out .= '<div class="team-member-full-separator"></div>';
                    }
                    
                    $counter++;

                                        
                    the_post();
                    $pm = & $post;
                    $pm_opt = get_post_meta($pm->ID, 'member_opt', true);
                    $permalink = get_permalink($pm->ID);
                    $allow_read_more = ((bool)$pm_opt['allow_readmore_cbox'] and (bool)$team_opt['team_readmore_cbox']);  
                    
                    if(($counter % 6) == 0)
                    {
                        $out .= '<div class="team-member-full-last">';                  
                    } else
                    {
                        $out .= '<div class="team-member-full">';  
                            
                    }
                    
                        $href = '';
                        if($allow_read_more)
                        {
                            $href = ' href="'.$permalink.'" ';   
                        }
                        
                        $out .= '<a '.$href.' class="async-img-s main-photo" rel="'.dcf_getTimThumbURL($pm_opt['image_url'], 128, 190).'"></a>';                    
                        $out .= '<div class="head">'; 
                            $out .= '<h4 class="title">';
                                if($allow_read_more) { $out .= '<a href="'.$permalink.'">'; }
                                $out .= $pm->post_title;
                                if($allow_read_more) { $out .= '</a>'; } 
                            $out .= '</h4>';
                            if($pm_opt['title'] != '')
                            {
                                $out .= '<div class="subtitle">'.$pm_opt['title'].'</div>';
                            }                   
                        $out .= '</div>';

                        if($pm->post_excerpt != '')
                        {
                            $pm->post_excerpt = wptexturize($pm->post_excerpt, true);
                            $pm->post_excerpt = convert_smilies($pm->post_excerpt);
                            $pm->post_excerpt = convert_chars($pm->post_excerpt);
                            
                            $link = '';
                            if($allow_read_more)
                            {
                                $link = ' <a class="more-link" href="'.$permalink.'" >&raquo;&nbsp;'.__('Read&nbsp;more', 'dc_theme').'</a>';    
                            }

                            $out .= '<div class="desc">'.$pm->post_excerpt.$link.'</div>';                                  
                        } else
                        {
                            global $more;
                            $more = 0;
                            $link = '';
                            if($allow_read_more)
                            {
                                $link = __('&raquo;&nbsp;Read&nbsp;more', 'dc_theme');
                            }
                            $out .= '<div class="desc">'.apply_filters('the_content', get_the_content($link)).'</div>'; 
                        }                           
                        
                    $out .= '</div>';   
                }              
                 
                $out .= '<div class="clear-both"></div>';                            
                echo $out;                                              
            } else
            if($team_opt['team_layout'] == CMS_TEAM_LAYOUT_SMALL)
            {
                $out = '';                 
                
                $counter = 0;
                while(have_posts()) 
                {
                    if($counter > 0 and (($counter % 3) == 0))
                    {
                        $out .= '<div class="team-member-small-separator"></div>';
                    }
                    
                    $counter++;

                                        
                    the_post();
                    $pm = & $post;
                    $pm_opt = get_post_meta($pm->ID, 'member_opt', true);
                    $permalink = get_permalink($pm->ID);
                    $allow_read_more = ((bool)$pm_opt['allow_readmore_cbox'] and (bool)$team_opt['team_readmore_cbox']);  
                    
                    if(($counter % 3) == 0)
                    {
                        $out .= '<div class="team-member-small-last">';                  
                    } else
                    {
                        $out .= '<div class="team-member-small">';  
                            
                    }
                    
                        $href = '';
                        if($allow_read_more)
                        {
                            $href = ' href="'.$permalink.'" ';   
                        }
                        
                        $out .= '<a '.$href.' class="async-img-s main-photo" rel="'.dcf_getTimThumbURL($pm_opt['image_url'], 180, 220).'"></a>';                    
                        $out .= '<div class="head">'; 
                            $out .= '<h4 class="title">';
                                if($allow_read_more) { $out .= '<a href="'.$permalink.'">'; }
                                $out .= $pm->post_title;
                                if($allow_read_more) { $out .= '</a>'; } 
                            $out .= '</h4>';
                            if($pm_opt['title'] != '')
                            {
                                $out .= '<div class="subtitle">'.$pm_opt['title'].'</div>';
                            }                   
                        $out .= '</div>';
                    $out .= '</div>';   
                }              
                 
                $out .= '<div class="clear-both"></div>';                            
                echo $out;                              
            } else
            {            
                $out = '';
                            
                while(have_posts()) 
                {
                    the_post();
                    $pm = & $post;
                    $pm_opt = get_post_meta($pm->ID, 'member_opt', true);
                    $permalink = get_permalink($pm->ID);
                    $allow_read_more = ((bool)$pm_opt['allow_readmore_cbox'] and (bool)$team_opt['team_readmore_cbox']);
                    
                    $out .= '<div class="team-member-big">';
                        $href = '';
                        if($allow_read_more)
                        {
                            $href = ' href="'.$permalink.'" ';   
                        }
                        
                        $out .= '<a '.$href.' class="async-img-s main-photo" rel="'.dcf_getTimThumbURL($pm_opt['image_url'], 148, 190).'"></a>';
                        $out .= '<div class="content-right">';
                            $out .= '<div class="head">';
                                $out .= '<h3 class="title">';
                                    if($allow_read_more) { $out .= '<a href="'.$permalink.'">'; }
                                    $out .= $pm->post_title;
                                    if($allow_read_more) { $out .= '</a>'; } 
                                $out .= '</h3>';
                                if($pm_opt['title'] != '')
                                {
                                    $out .= '<div class="subtitle">'.$pm_opt['title'].'</div>';
                                }
                                if((bool)$pm_opt['show_add_info_cbox'] and $pm_opt['add_info'] != '')
                                {
                                    $out .= '<div class="add-info">'.$pm_opt['add_info'].'</div>';
                                }
                            $out .= '</div>';
                            
                            if($pm->post_excerpt != '')
                            {
                                $pm->post_excerpt = wptexturize($pm->post_excerpt, true);
                                $pm->post_excerpt = convert_smilies($pm->post_excerpt);
                                $pm->post_excerpt = convert_chars($pm->post_excerpt);
                                
                                $link = '';
                                if($allow_read_more)
                                {
                                    $link = ' <a class="more-link" href="'.$permalink.'" >&raquo;&nbsp;'.__('Read&nbsp;more', 'dc_theme').'</a>';    
                                }

                                $out .= '<p>'.$pm->post_excerpt.$link.'</p>';                                  
                            } else
                            {
                                global $more;
                                $more = 0;
                                $link = '';
                                if($allow_read_more)
                                {
                                    $link = __('&raquo;&nbsp;Read&nbsp;more', 'dc_theme');
                                }
                                $out .= apply_filters('the_content', get_the_content($link)); 
                            }                                                    
                        
                              
                        $out .= '</div>';
                        
                        
                        $out .= '<div class="clear-both"></div>';
                    $out .= '</div>';
                }
                    
                echo $out;
            
            }
        }               
            
                                

            GetDCCPInterface()->getIGeneral()->renderSitePagination($paged, $max_page);               
                                                               
        ?>
                           
        </div>  <!-- page-width-xx0 -->
        <div class="clear-both"></div>
    </div> <!-- content -->
    
<?php    
    get_footer();
?>



