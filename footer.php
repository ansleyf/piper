<?php
/**********************************************************************
* MODERN ELEGANCE WORDPRESS THEME 
* (Ideal For Business And Personal Use: Portfolio or Blog)     
* 
* File name:   
*      footer.php
* Brief:       
*      Footer code for all theme files
* Author:      
*      DigitalCavalry
* Author URI:
*      http://themeforest.net/user/DigitalCavalry
* Contact:
*      digitalcavalry@gmail.com  
***********************************************************************/
?>

    <!-- FOOTER -->
    <div class="content-bottom-separator"></div>
    
    
    </div> <!--content-wrapper-->       
    
    <?php  
    
    GetDCCPInterface()->getIGeneral()->renderWidgetizedFooter();
    
    ?>
    
    
    
    <?php
     
    if(GetDCCPInterface()->GetIGeneral()->isFooterLinksShowed()) { ?>
    <div id="footer">
        <div class="links-info-container">
    
             <?php echo GetDCCPInterface()->getIGeneral()->getFooterLinksContent(); ?>
            
            <div class="clear-both"></div>    
        </div> <!--links-info-container--> 
    </div> <!--footer-->
    <?php } ?> 
                 
    <?php 
     
        if(GetDCCPInterface()->GetIGeneral()->isFooterLogoShowed() or GetDCCPInterface()->GetIGeneral()->isFooterCopyShowed()) 
        {    
            $out = '';
            $out .= '<div id="footer-bottom">';
                if(GetDCCPInterface()->GetIGeneral()->isFooterCopyShowed())
                {
                    $out .= '<div class="copyright" style="text-align:'.GetDCCPInterface()->getIGeneral()->getFooterCopyAlign().';">'.GetDCCPInterface()->getIGeneral()->getFooterCopy().'</div>';
                }
                if(GetDCCPInterface()->GetIGeneral()->isFooterLogoShowed())
                { 
                    $out .= '<a class="footer-logo" href="'.get_bloginfo('url').'">';
                        $out .= '<img src="'.GetDCCPInterface()->GetIGeneral()->getFooterLogoPath().'" />';
                    $out .= '</a>';
                }
            $out .= '';
            echo $out;
        }
                
    ?>    		<div id="facebook-box">			<a href="https://www.facebook.com/pages/Piper-Orthodontics/151903814826069" target="_blank">				<img src="<?php bloginfo("template_url"); ?>/custom/css/FB-f-Logo__blue_29.png" />			</a>		</div>
    </div></div><div class="clear"></div>
    <div id="content-bottom-empty-space">
        <?php GetDCCPInterface()->getIFloatingObjects()->renderObjects('content-bottom-empty-space'); ?> 
    </div>
    <?php 
        wp_footer();  
        GetDCCPInterface()->getIGeneral()->printFooterTrackingCode(); 
    ?>
    </div>
    <script>
        $(function() {
            $('.header-icons-panel').remove();
        });
    </script>
</body>
</html>

<?php ob_end_flush(); ?>

