/**********************************************************************
* MODERN ELEGANCE WORDPRESS THEME 
* (Ideal For Business And Personal Use: Portfolio or Blog)   
* 
* File name:   
*      common.js
* Brief:       
*      JavaScript code
* Author:      
*      DigitalCavalry
* Author URI:
*      http://themeforest.net/user/DigitalCavalry
* Contact:
*      digitalcavalry@gmail.com 
**********************************************************************/

function dcf_createCookie(name,value,seconds) 
{
    var expires = '';
    if(seconds != 0) 
    {
        var date = new Date();  
        date.setTime(date.getTime()+(seconds*1000));
        expires = "; expires="+date.toGMTString();
    }
    document.cookie = name+"="+value+expires+"; path=/";
}

function dcf_readCookie(name) 
{
    var nameEQ = name + '=';
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}

function dcf_eraseCookie(name) 
{
    createCookie(name,'',-1);
}

/*
 * jQuery EasIng v1.1.2 - http://gsgd.co.uk/sandbox/jquery.easIng.php
 *
 * Uses the built In easIng capabilities added In jQuery 1.1
 * to offer multiple easIng options
 *
 * Copyright (c) 2007 George Smith
 * Licensed under the MIT License:
 *   http://www.opensource.org/licenses/mit-license.php
 */

// t: current time, b: begInnIng value, c: change In value, d: duration

jQuery.extend( jQuery.easing,
{
    easeInQuad: function (x, t, b, c, d) {
        return c*(t/=d)*t + b;
    },
    easeOutQuad: function (x, t, b, c, d) {
        return -c *(t/=d)*(t-2) + b;
    },
    easeInOutQuad: function (x, t, b, c, d) {
        if ((t/=d/2) < 1) return c/2*t*t + b;
        return -c/2 * ((--t)*(t-2) - 1) + b;
    },
    easeInCubic: function (x, t, b, c, d) {
        return c*(t/=d)*t*t + b;
    },
    easeOutCubic: function (x, t, b, c, d) {
        return c*((t=t/d-1)*t*t + 1) + b;
    },
    easeInOutCubic: function (x, t, b, c, d) {
        if ((t/=d/2) < 1) return c/2*t*t*t + b;
        return c/2*((t-=2)*t*t + 2) + b;
    },
    easeInQuart: function (x, t, b, c, d) {
        return c*(t/=d)*t*t*t + b;
    },
    easeOutQuart: function (x, t, b, c, d) {
        return -c * ((t=t/d-1)*t*t*t - 1) + b;
    },
    easeInOutQuart: function (x, t, b, c, d) {
        if ((t/=d/2) < 1) return c/2*t*t*t*t + b;
        return -c/2 * ((t-=2)*t*t*t - 2) + b;
    },
    easeInQuint: function (x, t, b, c, d) {
        return c*(t/=d)*t*t*t*t + b;
    },
    easeOutQuint: function (x, t, b, c, d) {
        return c*((t=t/d-1)*t*t*t*t + 1) + b;
    },
    easeInOutQuint: function (x, t, b, c, d) {
        if ((t/=d/2) < 1) return c/2*t*t*t*t*t + b;
        return c/2*((t-=2)*t*t*t*t + 2) + b;
    },
    easeInSine: function (x, t, b, c, d) {
        return -c * Math.cos(t/d * (Math.PI/2)) + c + b;
    },
    easeOutSine: function (x, t, b, c, d) {
        return c * Math.sin(t/d * (Math.PI/2)) + b;
    },
    easeInOutSine: function (x, t, b, c, d) {
        return -c/2 * (Math.cos(Math.PI*t/d) - 1) + b;
    },
    easeInExpo: function (x, t, b, c, d) {
        return (t==0) ? b : c * Math.pow(2, 10 * (t/d - 1)) + b;
    },
    easeOutExpo: function (x, t, b, c, d) {
        return (t==d) ? b+c : c * (-Math.pow(2, -10 * t/d) + 1) + b;
    },
    easeInOutExpo: function (x, t, b, c, d) {
        if (t==0) return b;
        if (t==d) return b+c;
        if ((t/=d/2) < 1) return c/2 * Math.pow(2, 10 * (t - 1)) + b;
        return c/2 * (-Math.pow(2, -10 * --t) + 2) + b;
    },
    easeInCirc: function (x, t, b, c, d) {
        return -c * (Math.sqrt(1 - (t/=d)*t) - 1) + b;
    },
    easeOutCirc: function (x, t, b, c, d) {
        return c * Math.sqrt(1 - (t=t/d-1)*t) + b;
    },
    easeInOutCirc: function (x, t, b, c, d) {
        if ((t/=d/2) < 1) return -c/2 * (Math.sqrt(1 - t*t) - 1) + b;
        return c/2 * (Math.sqrt(1 - (t-=2)*t) + 1) + b;
    },
    easeInElastic: function (x, t, b, c, d) {
        var s=1.70158;var p=0;var a=c;
        if (t==0) return b;  if ((t/=d)==1) return b+c;  if (!p) p=d*.3;
        if (a < Math.abs(c)) { a=c; var s=p/4; }
        else var s = p/(2*Math.PI) * Math.asin (c/a);
        return -(a*Math.pow(2,10*(t-=1)) * Math.sin( (t*d-s)*(2*Math.PI)/p )) + b;
    },
    easeOutElastic: function (x, t, b, c, d) {
        var s=1.70158;var p=0;var a=c;
        if (t==0) return b;  if ((t/=d)==1) return b+c;  if (!p) p=d*.3;
        if (a < Math.abs(c)) { a=c; var s=p/4; }
        else var s = p/(2*Math.PI) * Math.asin (c/a);
        return a*Math.pow(2,-10*t) * Math.sin( (t*d-s)*(2*Math.PI)/p ) + c + b;
    },
    easeInOutElastic: function (x, t, b, c, d) {
        var s=1.70158;var p=0;var a=c;
        if (t==0) return b;  if ((t/=d/2)==2) return b+c;  if (!p) p=d*(.3*1.5);
        if (a < Math.abs(c)) { a=c; var s=p/4; }
        else var s = p/(2*Math.PI) * Math.asin (c/a);
        if (t < 1) return -.5*(a*Math.pow(2,10*(t-=1)) * Math.sin( (t*d-s)*(2*Math.PI)/p )) + b;
        return a*Math.pow(2,-10*(t-=1)) * Math.sin( (t*d-s)*(2*Math.PI)/p )*.5 + c + b;
    },
    easeInBack: function (x, t, b, c, d, s) {
        if (s == undefined) s = 1.70158;
        return c*(t/=d)*t*((s+1)*t - s) + b;
    },
    easeOutBack: function (x, t, b, c, d, s) {
        if (s == undefined) s = 1.70158;
        return c*((t=t/d-1)*t*((s+1)*t + s) + 1) + b;
    },
    easeInOutBack: function (x, t, b, c, d, s) {
        if (s == undefined) s = 1.70158; 
        if ((t/=d/2) < 1) return c/2*(t*t*(((s*=(1.525))+1)*t - s)) + b;
        return c/2*((t-=2)*t*(((s*=(1.525))+1)*t + s) + 2) + b;
    },
    easeInBounce: function (x, t, b, c, d) {
        return c - jQuery.easing.easeOutBounce (x, d-t, 0, c, d) + b;
    },
    easeOutBounce: function (x, t, b, c, d) {
        if ((t/=d) < (1/2.75)) {
            return c*(7.5625*t*t) + b;
        } else if (t < (2/2.75)) {
            return c*(7.5625*(t-=(1.5/2.75))*t + .75) + b;
        } else if (t < (2.5/2.75)) {
            return c*(7.5625*(t-=(2.25/2.75))*t + .9375) + b;
        } else {
            return c*(7.5625*(t-=(2.625/2.75))*t + .984375) + b;
        }
    },
    easeInOutBounce: function (x, t, b, c, d) {
        if (t < d/2) return jQuery.easing.easeInBounce (x, t*2, 0, c, d) * .5 + b;
        return jQuery.easing.easeOutBounce (x, t*2-d, 0, c, d) * .5 + c*.5 + b;
    }
});


/*
 * Digital Cavalry Tips jQuery Plugin JS (version 1.0)
 * http://themeforest.net/user/DigitalCavalry
 * 
 * file: jquery-dc-tips.1.0.js
 * 
 * Copyright 2011, Digital Cavalry 
 * You can not use this plugin without author permission
 * Date: 21 April 2011
 */
 
 (function($) {
  $.fn.dcTipCreate = function(options) {   
      var opts = $.extend({}, $.fn.dcTipCreate.defaults, options);
      var ttypes = { simple:'simple'};
      var tpos = { rightbottom:'right-bottom', bottom:'bottom', leftbottom:'left-bottom', left:'left', lefttop:'left-top', top:'top', righttop:'right-top', right:'right'};
      
      var DC_TIME_ID = 'dc-time-id';
      var DC_TEXT = 'dc-text'; 
      
      return this.each(function() {
        
        switch(opts.type)
        {
            case ttypes.simple:
                $(this).hover(simpleTypeHoverIn, simpleTypeHoverOut); 
                $(this).mousemove(simpleTypeMouseMove);
            break;
        }               
    });  
    
    function simpleTypeHoverIn(e)
    {
        if($('#dc-tip-simple').length) { $('#dc-tip-simple').remove(); }        
        $('body').append('<div id="dc-tip-simple"></div>');
        
        var tipbox = $('#dc-tip-simple'); 
        tipbox.css('background-color', opts.bgcolor);
        tipbox.css('border-color', opts.bordercolor); 
        tipbox.css('color', opts.color);
        tipbox.css('border-width', opts.borderwidth);
        
        tipbox.css('padding-left', opts.paddingleft); 
        tipbox.css('padding-top', opts.paddingtop); 
        tipbox.css('padding-right', opts.paddingright); 
        tipbox.css('padding-bottom', opts.paddingbottom);         
        
        if(opts.use_width) { tipbox.css('max-width', opts.width);  }
        
        var loader = $(this);   
        
        var text = '';
        if(opts.text != null)
        {
            text = opts.text;        
        } else
        {
            switch(opts.textattr)
            {
                case 'title':
                text = loader.attr('title');
                loader.removeAttr('title');
                loader.data(DC_TEXT, text);    
                break;
            }
        }  
        tipbox.text(text);   
        
        simpleSetPosition(tipbox, e);                          
        
        var timeout_id = setTimeout(function(){ simpleTypeHoverInEnd(loader); }, opts.delay);
        loader.data(DC_TIME_ID, timeout_id);
    }

    function simpleTypeHoverInEnd(loader)
    {
        var tipbox = $('#dc-tip-simple');
        tipbox.css('opacity', 0.0).css('display', 'block').stop().animate({opacity:1.0}, opts.fade);  
    }    
    
    function simpleTypeMouseMove(e)
    {
        var tipbox = $('#dc-tip-simple');

        simpleSetPosition(tipbox, e);
    }  
    
    function simpleSetPosition(tipbox, e)
    {
        var scroll_pos = $(window).scrollTop(); 
        var width = tipbox.outerWidth();
        var height = tipbox.outerHeight(); 
        var start_pos_x = e.pageX;
        var start_pos_y = e.pageY;
        var window_w = $(window).width();
        var window_h = $(window).height();
        

        switch(opts.pos)
        {
            case tpos.left:
                var final_x =  start_pos_x - width - opts.offsetX;
                var final_y =  start_pos_y - Math.round(height / 2);
                
                if(opts.viewport)
                {
                    if(final_x < 0) { final_x = start_pos_x+opts.offsetX; }  // change to right
                    if(final_y < 0) { final_y = start_pos_y+opts.offsetY; }  // change to bottom
                }
                
                tipbox.css('left', final_x);
                tipbox.css('top', final_y);                 
            break;

            case tpos.lefttop:
                var final_x =  start_pos_x-width-opts.offsetX;
                var final_y =  start_pos_y-opts.offsetY;                  
                
                if(opts.viewport)
                {
                    if(final_x < 0) { final_x = start_pos_x+opts.offsetX; }  // change to right 
                    if(final_y - scroll_pos < 0) { final_y = start_pos_y+opts.offsetY; }  // change to bottom 
                }            
            
                tipbox.css('left', final_x);
                tipbox.css('top', final_y);                 
            break;              
            
            case tpos.righttop:
                var final_x =  start_pos_x + opts.offsetX;
                var final_y =  start_pos_y - opts.offsetY;                  
                
                if(opts.viewport)
                {
                    if(final_x + width > window_w ) { final_x = start_pos_x - width - opts.offsetX; } // change to left  
                    if(final_y - scroll_pos < 0) { final_y = start_pos_y + opts.offsetY; }  // change to bottom 
                }               
            
                tipbox.css('left', final_x);
                tipbox.css('top', final_y);                 
            break;              
            
            case tpos.right:
                var final_x =  start_pos_x + opts.offsetX;
                var final_y =  start_pos_y - Math.round(height / 2);
                
                if(opts.viewport)
                {
                    if(final_x + width > window_w ) { final_x = start_pos_x - width - opts.offsetX; } // change to left
                    if(final_y < 0) { final_y = start_pos_y + opts.offsetY; } // change to bottom
                }            
            
                tipbox.css('left', final_x);
                tipbox.css('top', final_y);                                                                 
            break;            
            
            case tpos.leftbottom:
                var final_x =  start_pos_x - width-opts.offsetX;
                var final_y =  start_pos_y + opts.offsetY;              
            
                if(opts.viewport)
                {
                    if(final_x < 0) { final_x = start_pos_x + opts.offsetX; }  // change to right
                    if(final_y - scroll_pos + height > window_h) { final_y = start_pos_y - opts.offsetY - height; } // change to top     
                }
            
                tipbox.css('left', final_x);
                tipbox.css('top', final_y);                 
            break;

            case tpos.rightbottom:
                var final_x =  start_pos_x + opts.offsetX;
                var final_y =  start_pos_y + opts.offsetY;  
            
                if(opts.viewport)
                {
                    if(final_x + width > window_w ) { final_x = start_pos_x - width - opts.offsetX; } // change to left
                    if(final_y - scroll_pos + height > window_h) { final_y = start_pos_y - opts.offsetY - height; } // change to top      
                }
            
                tipbox.css('left', final_x);
                tipbox.css('top', final_y);                 
            break;
            
            case tpos.bottom:
                var final_x =  start_pos_x - Math.round(width / 2);
                var final_y =  start_pos_y + opts.offsetY;                  
                if(opts.viewport)
                {
                    if(final_x < 0) { final_x = start_pos_x + opts.offsetX; } // change to right
                    else if(final_x + width > window_w ) { final_x = start_pos_x - width - opts.offsetX; } // change to left
                     
                    if(final_y - scroll_pos + height > window_h) { final_y = start_pos_y - opts.offsetY - height; } // change to top
                }               
            
                tipbox.css('left', final_x);
                tipbox.css('top', final_y);                 
            break; 
            
            case tpos.top:
                var final_x =  start_pos_x - Math.round(width / 2);
                var final_y =  start_pos_y - opts.offsetY - height;            
            
                if(opts.viewport)
                {
                    if(final_x < 0) { final_x = start_pos_x + opts.offsetX; } // change to right
                    else if(final_x + width > window_w ) { final_x = start_pos_x - width - opts.offsetX; } // change to left
                     
                    if(final_y - scroll_pos < 0) { final_y = start_pos_y + opts.offsetY; } // change to bottom
                }                
            
                tipbox.css('left', final_x);
                tipbox.css('top', final_y);                 
            break;                        
        }          
    }    
    
    function simpleTypeHoverOut()
    {
        if(isset($(this).data(DC_TIME_ID)))
        {
            clearTimeout($(this).data(DC_TIME_ID));
            $(this).removeData(DC_TIME_ID);
        }
        if($('#dc-tip-simple').length) { $('#dc-tip-simple').stop().remove(); } 
        
        if(opts.text == null)
        {
            switch(opts.textattr)
            {
                case 'title':
                text = $(this).data(DC_TEXT);
                $(this).attr('title', text);  
                break;
            }
        }            
    }
    
    // utilities
    function isset( variable )
    {
        return( typeof( variable ) != 'undefined' );
    }    
    
  } 
  
  $.fn.dcTipCreate.defaults = {
    type:'simple', 
    pos:'bottom',
    bgcolor:'#FAFAFA',
    bordercolor:'#DDDDDD',
    borderwidth:1,
    color:'#666666', 
    paddingleft:8,
    paddingright:8,
    paddingtop:5,
    paddingbottom:5,
    width:300,
    align:'center',
    use_width:false,
    offsetX:15,
    offsetY:15,
    text:null,
    textattr:'title',
    delay:300,
    fade:200,
    viewport:true
  }
})(jQuery)



/***************************************************
  POPUP IMAGES
****************************************************/
var pu_cache = new Object();
pu_cache.name = 0;
 
pu_cache.handle = null; 
pu_cache.width = 0;
pu_cache.height = 0;
pu_cache.text = '';
pu_cache.alt = ''; 
pu_cache.istext = false;
pu_cache.mousex = 0;
pu_cache.mousey = 0;
pu_cache.browserwidth = 0;
pu_cache.browserheight = 0;
pu_cache.x = 0;
pu_cache.y = 0; 
                                          
function setupPopUpImages(_class)
{     
  //  var q = jQuery.noConflict();   
    
    q(_class).each(
        function()
        {        
           // var q = jQuery.noConflict();  
            var obj = q(this);

       obj.hover(
         function(e)
         {          
            //var q = jQuery.noConflict(); 
            // save handle in global variable
            pu_cache.handle = this;
            // image preview container x,y offset form the cursor position
            var offsetx = 0;       
            var offsety = -15;
            // keep hovered object handle in local variable
            var hoveredobject = this;        
            // get path to image file
            var imagepath = q(this).attr('rel');
            // text
            pu_cache.text = q(this).attr('title');
            q(this).removeAttr('title'); 
      
            pu_cache.alt = q(this).attr('alt'); 

               
            if(!q("#dcjp-popupimage-loader").length)
            {                
                q('body').append("<div id='dcjp-popupimage-loader'></div>");
            }
            q('#dcjp-popupimage-loader')
                        .stop()
                        .css('opacity', 0.0)
                        .css('left', (e.pageX + 8) + 'px')
                        .css('top', (e.pageY - 24) + 'px')
                        .animate({opacity: 1.0}, 400);  
                        
   
            
            // create new image object
            var img = new Image();
            // bind function to object which gona be called when new image loading is finished
            q(img).load(function() 
            {                 
               // var q = jQuery.noConflict(); 
                // check image preview overlap
                if(pu_cache.handle != hoveredobject)
                {
                    // in new image preview is in progress function return control
                    return;
                }            
            
                    
                pu_cache.width = img.width;
                pu_cache.height = img.height;
                
            // adding to page image preview container
            var htmlcode = '<div id="dcjp-popupimage"><div id="dcjp-popupimage-image"></div>';
            if((pu_cache.text != '' && pu_cache.text !== undefined) || (pu_cache.alt != '' && pu_cache.alt !== undefined)) 
            {
                htmlcode += '<div class="text" style="width:'+(pu_cache.width-12)+'px;">';
                if(pu_cache.alt != '' && pu_cache.alt !== undefined)
                {
                    htmlcode += '<span class="title" >'+pu_cache.alt+'</span>';
                }
                if(pu_cache.text != '' && pu_cache.text !== undefined)
                {
                    htmlcode += pu_cache.text;
                }
                
                htmlcode += '</div>';
                pu_cache.istext = true;
            } else
            {
                pu_cache.istext = false;
            }
            
            htmlcode += '</div>';
            q('body').append(htmlcode);
                
            var container = q('#dcjp-popupimage');
            var image = q('#dcjp-popupimage-image'); 
            
            // hide image preview container, this is necessary to correctly call show function
            container.hide();                            
                
                pu_cache.mousex = e.pageX;
                pu_cache.mousey = e.pageY;
                pu_cache.browserwidth = q(window).width();
                pu_cache.browserheight = q(window).height();                
              
              
              
                pu_cache.x = e.pageX - (pu_cache.width / 2);
                
                if((pu_cache.x + pu_cache.width) > pu_cache.browserwidth) { pu_cache.x = pu_cache.browserwidth - 20 - pu_cache.width; }
                if(pu_cache.x < 0) { pu_cache.x = 20; }
                
                pu_cache.y = e.pageY - 20 - pu_cache.height; 
                var secure_height = pu_cache.height + 20 + 5; // extra 5 px
                if(pu_cache.istext) { pu_cache.y -= 18; secure_height += 18; }
                
                if(pu_cache.y < 5 || e.clientY < secure_height)
                {                                   
                    pu_cache.y = e.pageY + 20;     
                }              
              
                // add image to preview container
                image.html(this);
                image.css('height', pu_cache.height);                  
                
                // we must set also image container position, make it visible and set opacity to max                
                container.find('.text').stop().css('opacity', 0.0);
                container.css('height', "auto")
                    .css('width', pu_cache.width)
                    .css('left', pu_cache.x)
                    .css('top', pu_cache.y)
                    .css('visibility', 'visible')
                    .css('opacity', '1.0')
                    .show('fast', function() { container.find('.text').animate({opacity:1.0}, 500); });                
               
               q("#dcjp-popupimage-loader").stop().animate({opacity: 0.0}, 400, function(){q(this).remove()}); 
            // set new value for "src" attribute, this me     
            }).attr('src', imagepath);                                    
             
         },
         
         function()
         {
            // var q = jQuery.noConflict(); 
             q("#dcjp-popupimage").stop().remove(); 
             pu_cache.handle = null;
             q('#dcjp-popupimage-loader').stop().animate({opacity: 0.0}, 400, function() { q(this).remove(); } );
             q(this).attr('title', pu_cache.text);
         }              
       ); // hover         
          
    obj.mousemove(
        function(e)
        {           
           // var q = jQuery.noConflict(); 
            var relx = e.pageX - pu_cache.mousex;
            var rely = e.pageY - pu_cache.mousey;
            
            var newx = pu_cache.x + relx;
            var newy = pu_cache.y + rely;
            
            q('#dcjp-popupimage').css('left', newx).css('top', newy);     
            q('#dcjp-popupimage-loader')
                .css("left", (e.pageX + 8))
                .css("top", (e.pageY - 24));                    
            
        }
    ); // mousemove         
                    
    });  
    
}

/***************************************************
  LINKS
****************************************************/

// remove frame focus for links 
function dc_setupLinks()
{
   // var q = jQuery.noConflict();
	q('a').focus(function(){q(this).blur();});
} // setupLinks                                       
                                                 
/**********************************************
    CONTACT FORM
***********************************************/
function dc_setupContactForm()
{
 //   var q = jQuery.noConflict(); 
    
    q('.send-email-btn').focus(function(){q(this).blur();})  
    
    q('.send-email-btn').click(
        function() 
        {
            var parent = q(this).parent();
            
            // get all data
            var name = q(parent).find('input[name=name]').val();
            var mail = q(parent).find('input[name=email]').val(); 
            var subject = q(parent).find('input[name=subject]').val();
            var message = q(parent).find('textarea[name=message]').val(); 
            var maildest = q(parent).find('input[name=contact-mail-dest]').val();
            var okay = q(parent).find('input[name=contact-okay]').val();
            var error = q(parent).find('input[name=contact-error]').val();
            var scode = q(parent).find('input[name=dc_scode]').val(); 
            var scodeuser = q(parent).find('input[name=dc_scodeuser]').val();                                                          
            
            // check user name
            var allok = true;
            if(name == '')
            {
                q(parent).find('input[name=name]').addClass('invalid');
                allok = false;
            } else
            {
                q(parent).find('input[name=name]').addClass('valid');            
            }

            // check email
            var regExp = new RegExp(/^[-a-z0-9~!$%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9]([-a-z0-9_]?[a-z0-9])*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z]{2})|([1]?\d{1,2}|2[0-4]{1}\d{1}|25[0-5]{1})(\.([1]?\d{1,2}|2[0-4]{1}\d{1}|25[0-5]{1})){3})(:[0-9]{1,5})?$/i);
            // check email address, if result is null the email string dont match to pattern
            var resultExp = regExp.exec(mail);            
            if(mail == '' || resultExp == null)
            {
                q(parent).find('input[name=email]').addClass('invalid');
                allok = false;
            } else
            {
                q(parent).find('input[name=email]').addClass('valid');           
            }
            
            // check subject
            if(subject == '')
            {
                q(parent).find('input[name=subject]').addClass('invalid');
                allok = false;
            } else
            {
                q(parent).find('input[name=subject]').addClass('valid');           
            } 
            
            // check message
            if(message == '')
            {
                q(parent).find('textarea[name=message]').addClass('invalid');
                allok = false;
            } else
            {
                q(parent).find('textarea[name=message]').addClass('valid');           
            }  
            
            // authorization code
            if(q(parent).find('input[name=dc_scodeuser]').length)
            {
                if(scodeuser == '')
                {
                    q(parent).find('input[name=dc_scodeuser]').addClass('invalid');
                    allok = false;
                } else
                {
                    q(parent).find('input[name=dc_scodeuser]').addClass('valid');           
                }
            }              
            
            
                                    
            q(parent).find('.result').stop().animate({opacity:0.0}, 300);
            
            
            function callback(data)
            {   
                
                // if success        
                if(data == "okay")
                {   
                    
                    q(parent).find('.result').css('opacity', '0.0').css('visibility', 'visible');
                    q(parent).find('.result').text(okay);
                    q(parent).find('.result').stop().animate({opacity:1.0}, 500);
                    
                    q(parent).find('input[name=name]').val('');
                    q(parent).find('input[name=email]').val(''); 
                    q(parent).find('input[name=subject]').val('');
                    
                    if(q(parent).find('input[name=dc_scodeuser]').length)   
                    {
                        q(parent).find('input[name=dc_scodeuser]').val('');
                    }
                    q(parent).find('textarea[name=message]').val('');                                      
                } else // if error/problem during email sending in php script
                {
                    q(parent).find('.result').css('opacity', '0.0').css('visibility', 'visible');
                    q(parent).find('.result').text(error);
                    q(parent).find('.result').stop().animate({opacity:1.0}, 500);
                }
            }             
            
            if(allok)
            {       
                // build data string for post call
                var data = "name="+name;
                data += "&"+"mail="+mail;
                data += "&"+"subject="+subject;
                data += "&"+"message="+message;
                data += "&"+"maildest="+maildest;

                if(q(parent).find('input[name=dc_scodeuser]').length) 
                {   
                    data += "&"+"scode="+scode;
                    data += "&"+"scodeuser="+scodeuser;
                }
                
                // try to send email via php script executed on server
                q.post(dc_theme_path+'/php/sendmessage.php', data, callback, "text");
            } 
        });  
}

/***************************************************
  IMAGE ASYNC LOADING
****************************************************/
function setupAsyncImages(p)
{
 //   var q = jQuery.noConflict();
    
    q(p).each(
        function()
        {   
            var loader = q(this);
            var imagePath = loader.attr('rel');
          
            var img = new Image();
            q(img).css('opacity', '0.0')

                .load(
                    function() 
                    {
                        loader.append(this).removeAttr('rel');
                        q(this)
                            .css('margin', '0px')
                            .css('opacity', '0.0')
                            .animate({opacity: 1.0}, 500,
                                function()
                                {
                                    loader.css('background-image', 'none');
                                }
                            );
                    }
                ).attr('src', imagePath);                        
        }
    );
}

/***************************************************
  PRETY PHOTO
****************************************************/
function setupPPhoto()
{
   // var q = jQuery.noConflict(); 
    
    var dc_lightbox_mode = jQuery('meta[name=cms_lightbox_mode]').attr('content');
    var dc_lightbox_overlay = parseInt(jQuery('meta[name=cms_lightbox_overlay]').attr('content')); 
    
    if(dc_lightbox_overlay == 0) { dc_lightbox_overlay = false; } else { dc_lightbox_overlay = true; } 
    
    q("a[rel^='lightbox']").prettyPhoto({theme:dc_lightbox_mode, showTitle: true, overlay_gallery:dc_lightbox_overlay});
}

/***************************************************
    HOMEPAGE FEATURED GALLERY 
****************************************************/
function setupHomeFeaturedGallery()
{
   // var q = jQuery.noConflict();
    
    q('.home-featured-gallery .ngg-gallery .triger').hover(
      function()
      {
            q(this).stop().animate({opacity:0.5}, 300);    
      },
      function()
      {
            q(this).stop().animate({opacity:0.0}, 300);            
      }
    );      
}

/***************************************************
  BLOG POSTS
****************************************************/
function setupWordPressBlogPosts()
{
   // var q = jQuery.noConflict();
        
    q('.blog-post .image-wrapper .triger').hover(
      function()
      {
            q(this).stop().animate({opacity:0.5}, 300);    
      },
      function()
      {
            q(this).stop().animate({opacity:0.0}, 300);            
      }
    );  
        
    q('.blog-post .image-wrapper').mouseenter(
      function()
      {
            // q(this).parent().stop().animate({opacity:0.5}, 300);
            q(this).find('.thumb-wrapper').stop().animate({opacity:1.0}, 300);                  
      });

    q('.blog-post .image-wrapper').mouseleave(        
      function()
      {
            q(this).find('.thumb-wrapper').stop().animate({opacity:0.0}, 900);            
      });      
    
    q('.blog-post .image-wrapper .thumb-triger').hover(
      function()
      {
            // q(this).parent().stop().animate({opacity:0.5}, 300);
            var wrapper = q(this).parent();
            var imagewrapper = q(this).parent().parent();
            imagewrapper.find('.thumb-wrapper').not(wrapper).stop().animate({opacity:0.75}, 300);                  
      },
      function()
      {
            var wrapper = q(this).parent();
            var imagewrapper = q(this).parent().parent();
            imagewrapper.find('.thumb-wrapper').not(wrapper).stop().animate({opacity:1.0}, 300);          
      }
    );   
          
    q('.blog-post-related-home .image').hover(
      function()
      {
            var parent = q(this).parent().parent();
            q(parent).find('.image').not(this).stop().animate({opacity:0.7}, 300);    
      },
      function()
      {
            var parent = q(this).parent().parent();
            q(parent).find('.image').not(this).stop().animate({opacity:1.0}, 300);            
      }
    );      
           
}


function setupThemeImages()
{
   // var q = jQuery.noConflict();
    
    q('.theme-image-left .triger, .theme-image-center .triger, .theme-image-right .triger').hover(
      function()
      {
            q(this).stop().animate({opacity:0.5}, 300);    
      },
      function()
      {
            q(this).stop().animate({opacity:0.0}, 300);            
      }
    );  
    
    q('.theme-image-left .triger-link, .theme-image-center .triger-link, .theme-image-right .triger-link').hover(
      function()
      {
            q(this).stop().animate({opacity:0.1}, 300);    
      },
      function()
      {
            q(this).stop().animate({opacity:0.0}, 300);            
      }
    );                   
}

/***************************************************
  NEWS PAGE
****************************************************/
function setupNewsPage()
{
  //  var q = jQuery.noConflict();
    
    q('.news-page-featured .image-wrapper .triger').hover(
      function()
      {
            q(this).stop().animate({opacity:0.5}, 300);    
      },
      function()
      {
            q(this).stop().animate({opacity:0.0}, 300);            
      }
    );  
    
    q('.news-page-featured .image-wrapper').mouseenter(
      function()
      {
            // q(this).parent().stop().animate({opacity:0.5}, 300);
            q(this).find('.thumb-wrapper').stop().animate({opacity:1.0}, 300);                  
      });

    q('.news-page-featured .image-wrapper').mouseleave(        
      function()
      {
            q(this).find('.thumb-wrapper').stop().animate({opacity:0.0}, 900);            
      });      
    
    q('.news-page-featured .image-wrapper .thumb-triger').hover(
      function()
      {
            // q(this).parent().stop().animate({opacity:0.5}, 300);
            var wrapper = q(this).parent();
            var imagewrapper = q(this).parent().parent();
            imagewrapper.find('.thumb-wrapper').not(wrapper).stop().animate({opacity:0.75}, 300);                  
      },
      function()
      {
            var wrapper = q(this).parent();
            var imagewrapper = q(this).parent().parent();
            imagewrapper.find('.thumb-wrapper').not(wrapper).stop().animate({opacity:1.0}, 300);          
      }
    );            
}

/***************************************************
  GALLERY
****************************************************/
function setupGalleryList()
{
   // var q = jQuery.noConflict();
    
    q('.gallery-list-item .image-wrapper .triger').hover(
      function()
      {
            q(this).stop().animate({opacity:0.5}, 300);    
      },
      function()
      {
            q(this).stop().animate({opacity:0.0}, 300);            
      }
    );  
}

function setupCompactAndTableGallery()
{
   // var q = jQuery.noConflict();
    
    q('.compact-gallery .thumb .triger').hover(
      function()
      {
            q(this).stop().animate({opacity:0.5}, 300);    
      },
      function()
      {
            q(this).stop().animate({opacity:0.0}, 300);            
      }
    ); 
    
    q('.table-gallery .triger').hover(
      function()
      {
            q(this).stop().animate({opacity:0.5}, 300); 
            q(this).data('dc_bgpos', q(this).parent().parent().css('background-position')); 
            q(this).parent().parent().css('background-position', '0px 0px');      
      },
      function()
      {
            q(this).stop().animate({opacity:0.0}, 300);
            q(this).parent().parent().css('background-position', q(this).data('dc_bgpos'));            
      }
    );       
}

/***************************************************
  PORTFOLIO
****************************************************/
function setupPortfolio()
{
  //  var q = jQuery.noConflict();

//    q('.portfolio-list-item .image-wrapper .triger').hover(
//      function()
//      {
//            q(this).stop().animate({opacity:0.5}, 300);    
//      },
//      function()
//      {
//            q(this).stop().animate({opacity:0.0}, 300);            
//      }
//    );      
    
    q('.sidebar-portfolio .triger, .sidebar-portfolio .triger-link').hover(
      function()
      {
            q(this).stop().animate({opacity:0.5}, 300);    
      },
      function()
      {
            q(this).stop().animate({opacity:0.0}, 300);            
      }
    );    
    
    q('.table2-portfolio .triger, .table2-portfolio .triger-link').hover(
      function()
      {
            q(this).stop().animate({opacity:0.5}, 300);    
      },
      function()
      {
            q(this).stop().animate({opacity:0.0}, 300);            
      }
    );
    
    q('.table3-portfolio .triger, .table3-portfolio .triger-link').hover(
      function()
      {
            q(this).stop().animate({opacity:0.5}, 300);    
      },
      function()
      {
            q(this).stop().animate({opacity:0.0}, 300);            
      }
    );
    
    q('.table4-portfolio .triger, .table4-portfolio .triger-link').hover(
      function()
      {
            q(this).stop().animate({opacity:0.5}, 300);    
      },
      function()
      {
            q(this).stop().animate({opacity:0.0}, 300);            
      }
    );            
}

/***************************************************
  SERVICES
****************************************************/
function dc_setupServices()
{
  //  var q = jQuery.noConflict(); 
           
    q('.services-big-list').find('.item').hover(
        function()
        {
            var is_hover = q(this).find('.image-hover').length;
            if(is_hover)
            {
                q(this).find('.image').css('display', 'none');
                q(this).find('.image-hover').css('display', 'block');  
            }      
        },
        function()
        {
            var is_hover = q(this).find('.image-hover').length;
            if(is_hover)
            {
                q(this).find('.image-hover').css('display', 'none');
                q(this).find('.image').css('display', 'block');                  
            }            
        }        
        );
        
    q('.services-small-list').find('.item').hover(
        function()
        {
            var is_hover = q(this).find('.image-hover').length;
            if(is_hover)
            {
                q(this).find('.image').css('display', 'none');
                q(this).find('.image-hover').css('display', 'block');  
            }      
        },
        function()
        {
            var is_hover = q(this).find('.image-hover').length;
            if(is_hover)
            {
                q(this).find('.image-hover').css('display', 'none');
                q(this).find('.image').css('display', 'block');                  
            }            
        }        
        ); 
        
    q('.services-medium-list').find('.item-left, .item-right').hover(
        function()
        {
            var is_hover = q(this).find('.image-hover').length;
            if(is_hover)
            {
                q(this).find('.image').css('display', 'none');
                q(this).find('.image-hover').css('display', 'block');  
            }      
        },
        function()
        {
            var is_hover = q(this).find('.image-hover').length;
            if(is_hover)
            {
                q(this).find('.image-hover').css('display', 'none');
                q(this).find('.image').css('display', 'block');                  
            }            
        }        
        );                 
}

/***************************************************
  TOUR
****************************************************/
function dc_setupThemeTour()
{
  //  var q = jQuery.noConflict(); 
          
    q('.tour-slider').each(function()
    {
        var $slider = q(this);        
        $slider.data('dc_curent', 0);
        $slider.data('dc_block', false);
        var count = $slider.find('.page').length;
        var effect = $slider.find('.effect').text();
        
        var maxh = 0;
        for(var i = 0; i < count; i++)
        {
            var h = $slider.find('.page:eq('+i+')').height();
            if(h > maxh) { maxh = h; }    
        } 
        $slider.find('.content').css('height', maxh);
        

        $slider.find('.page:first').css('left', 0);
        $slider.find('li:first').addClass('button-active');
        var is_hover_first = $slider.find('li:first').find('.icon-hover').length; 
        if(is_hover_first)
        {
            $slider.find('li:first').find('img:eq(0)').css('display', 'none');
            $slider.find('li:first').find('img:eq(1)').css('display', 'inline');
        }        
        
        if(count > 1)
        {
            $slider.find('.next').click(
                function() {
                    var parent = q(this).parent().parent();
                    var index = $slider.find('.content').find('.page').index(parent);
                    $slider.find('li:eq('+(index+1)+')').trigger('click');
                }); 

            $slider.find('.prev').click(
                function() {
                    var parent = q(this).parent().parent();
                    var index = $slider.find('.content').find('.page').index(parent);
                    $slider.find('li:eq('+(index-1)+')').trigger('click');
                }); 
            
            $slider.find('li').hover(
              function()
              {    
                    var is_hover = q(this).find('.icon-hover').length; 
                    var is_active = q(this).hasClass('button-active'); 
                    if(is_hover && !is_active)
                    {   
                        q(this).find('img:eq(0)').css('display', 'none');
                        q(this).find('img:eq(1)').css('display', 'inline');
                    }                        
              },
              function()
              {
                    var is_hover = q(this).find('.icon-hover').length; 
                    var is_active = q(this).hasClass('button-active'); 
                    if(is_hover && !is_active)
                    {
                        q(this).find('img:eq(0)').css('display', 'inline');
                        q(this).find('img:eq(1)').css('display', 'none');
                    }                        
              }              
            ); 
            
            $slider.find('li').click(
                function() 
                {
                    var block = $slider.data('dc_block');
                    var curent = $slider.data('dc_curent');
                    var parent = q(this).parent();
                    var index = parent.find('li').index(this);                        
                    if(block || index == curent) { return; }
                    
                    $slider.data('dc_block', true);
                    $slider.find('li:eq('+index+')').addClass('button-active');
                    var is_hover_index = $slider.find('li:eq('+index+')').find('.icon-hover').length;
                    var is_hover_current = $slider.find('li:eq('+curent+')').find('.icon-hover').length;
                    
                    if(is_hover_index)
                    {
                        $slider.find('li:eq('+index+')').find('img:eq(0)').css('display', 'none');
                        $slider.find('li:eq('+index+')').find('img:eq(1)').css('display', 'inline');
                    }
                    
                    if(is_hover_current)
                    {    
                        $slider.find('li:eq('+curent+')').find('img:eq(0)').css('display', 'inline');
                        $slider.find('li:eq('+curent+')').find('img:eq(1)').css('display', 'none');
                    }    
                    
                    $slider.find('li:eq('+curent+')').removeClass('button-active');
                    
                    if(effect == 'fade')
                    {
                        $slider.find('.page:eq('+curent+')').animate({opacity:0.0}, 400, 'easeOutCirc');
                        $slider.find('.page:eq('+index+')').css('opacity', 0).css('left', 0).stop().animate({opacity:1.0}, 500, 'easeOutCirc', function(){
                            $slider.data('dc_block', false); $slider.data('dc_curent', index);  
                        });                     
                    } else
                    {                    
                        if(index > curent)
                        {   
                            $slider.find('.page:eq('+curent+')').animate({left:-800}, 200, 'easeOutCirc');
                            $slider.find('.page:eq('+index+')').css('left', 800).stop().animate({left:0}, 400, 'easeOutCirc', function(){
                                $slider.data('dc_block', false); $slider.data('dc_curent', index);  
                            });
                        } else
                        {
                            $slider.find('.page:eq('+curent+')').animate({left:800}, 200, 'easeOutCirc');
                            $slider.find('.page:eq('+index+')').css('left', -800).stop().animate({left:0}, 400, 'easeOutCirc', function(){
                                $slider.data('dc_block', false); $slider.data('dc_curent', index);  
                            });                        
                        }
                    }

                    
                    
            });
        }
    });
}

/***************************************************
  CLIENT PANEL
****************************************************/
function setupClientPanel()
{
   // var q = jQuery.noConflict();
    
    q('#client-panel .close-btn').click(function()
      {
         // var q = jQuery.noConflict();
          
          q('#client-panel').css('display', 'none');
          q('#client-panel-open-btn').css('display', 'block');
      }
    );
    
    q('#client-panel-open-btn').click(function()
      {
         // var q = jQuery.noConflict();
          
          q('#client-panel').css('display', 'block');
          q(this).css('display', 'none');
      }
    );         
}

/***************************************************
  QUICK NEWS
****************************************************/
function setupQuickNewsPanel()
{
   // var q = jQuery.noConflict(); 
    
    q('#quick-news-panel').each(function()
    {
        var slider = q(this);
        slider.data('index', 0);
        var count = slider.find('.news').length;
        if(count == 0) return;
        
        var time = parseInt(slider.find('.time').text(), 10);
        
        var fw = slider.find('.wrapper:first');
        fw.css('display', 'inline');        
                        
        setTimeout(autoplay, time);
        function autoplay()
        {
            var index = slider.data('index');
            
            slider.find('.wrapper:eq('+index+')').animate({opacity:0.0}, 500, function()
            {
                q(this).css('display', 'none');
                index++;
                if(index >= count) { index = 0; }
                slider.find('.wrapper:eq('+index+')').css('opacity', 0.0).css('display', 'inline').animate({opacity:1.0}, 500);            
                
                slider.data('index', index);                  
            });
                          
          
            setTimeout(autoplay, time);  
        }
            
    });
}

/***************************************************
  DCS CHAIN SLIDER
****************************************************/
function setupChainSlider()
{
   //   var q = jQuery.noConflict();       
       
       q('.chain-slider .thumbs-wrapper').each(function()
       {                      
           var $slider = q(this).parent();
           var $ts = q(this).find('.thumbs-slider');
           
           $slider.data('dc_scroll', true);
           $slider.data('dc_block', false); 
           $slider.data('dc_anim', false);
           $slider.data('dc_current', 0);
           $slider.data('dc_li_index', 0);
           $slider.data('dc_timer', null);
           var size = $slider.find('.slide').length; 

           var userBorderColor = $slider.find('.border-color').text();
           var userAutoplay = $slider.find('.autoplay').text();
           var normalBorderColor = '#EEEEEE';
           var userTransMode = $slider.find('.trans').text();

           var thumb_rmargin = 5;
           var thumb_border = 1;
           
           var width = q(this).width();
           var twidth = q(this).find('li:first').width();                             
           var swidth = q(this).find('span:first').width();
           var sheight = q(this).find('span:first').height();  
           var real_twidth = twidth + (2* thumb_border) + thumb_rmargin;           
           var count = q(this).find('li').length; 
           var all_width = count * real_twidth;           
  
           if(all_width < width)
           {
                var left = Math.round(width / 2)  - Math.round(all_width / 2);
                q(this).find('.thumbs-slider').css('left', left);
                $slider.data('dc_scroll', false);         
           }             
           
            q(this).find('li').mouseenter(function()
            {
                $border = q(this).find('span');
                var parent = q(this).parent();
                $ts.find('li').not(this).stop().animate({opacity:0.7},300); 
              //  $border.stop().animate({top:-2,left:-2,height:sheight+4,width:swidth+4},100);
            });
            
            q(this).find('li').mouseleave(function()
            {    
                $border = q(this).find('span');
                var parent = q(this).parent();
                $ts.find('li').not(this).stop().animate({opacity:1.0},300);                        
              //  $border.stop().animate({top:0, left:0,height:sheight,width:swidth},200);
            });           
           
           $slider.find('.slide:first').css('display', 'block');
           q(this).find('li:first').css('border-color', userBorderColor);
                     

  
           if(userAutoplay == 'true' && size > 1)
           {
               var timer = setTimeout(autoplay, 5000);
               $slider.data('dc_timer', timer);
           } 
  
           function autoplay()
           {
                 var dc_scroll = $slider.data('dc_scroll');
                 var dc_li_index = $slider.data('dc_li_index');
                 dc_li_index++;
                 if(dc_li_index >= size)
                 {
                     dc_li_index = 0;
                 }
                 
                 $ts.find('li:eq('+dc_li_index+')').trigger('click');                 
                 
                 var timer = setTimeout(autoplay, 5000);
                 $slider.data('dc_timer', timer);               
           }  
           
           $slider.hover(
                function()
                {
                    var dc_timer = $slider.data('dc_timer');
                    clearTimeout(dc_timer);    
                },
                function()
                {
                    
                    if(userAutoplay == 'true' && size > 1)
                    {
                        var timer = setTimeout(autoplay, 5000);
                        $slider.data('dc_timer', timer);
                    }    
                }
           );

           q(this).find('.thumbs-slider li').click(
            function()
            {   
                var $ts = q(this).parent(); 
                var index = $ts.find('li').index(this);
                var picindex = q(this).find('em').text();
                
                var pos = $ts.position();                
                var dc_current = parseInt($slider.data('dc_current'));
                var dc_block = $slider.data('dc_block');
                var dc_scroll = $slider.data('dc_scroll');
                var dc_anim = $slider.data('dc_anim'); 
                var dc_index = $slider.data('dc_index');
                var dc_li_index = $slider.data('dc_li_index');
                
                if(dc_block || dc_current == picindex || dc_anim)
                {
                    return;
                }                                                                                
                $slider.data('dc_block', true);
                
                if(userTransMode == 'fade')
                {   
                    $slider.data('dc_anim', true);
                    $slider.find('.slide:eq('+dc_current+')').css('z-index', 0);
                    $slider.find('.slide:eq('+picindex+')')
                        .stop().css('opacity', 0.0).css('display', 'block')
                        .css('z-index', 3).css('left', 0).css('top', 0).animate({opacity:1.0}, 500,
                        function()
                        {
                            $slider.find('.slide:eq('+dc_current+')').css('display', 'none');
                            $slider.data('dc_current', picindex);
                            $slider.data('dc_anim', false);                             
                        });

                } else 
                if(userTransMode == 'slide')
                {
                    $slider.data('dc_anim', true);
                    var slide_width = $slider.find('.slide:eq('+picindex+')').width();                   
                    
                       $slider.find('.slide:eq('+dc_current+')').css('z-index', 0); 
                    $slider.find('.slide:eq('+picindex+')')                        
                        .css('left', slide_width).css('top', 0).css('z-index', 3).css('display', 'block');
                        $slider.find('.slide:eq('+picindex+')').animate({left:0}, 500,
                        function()
                        {
                            $slider.find('.slide:eq('+dc_current+')').css('display', 'none');
                            $slider.data('dc_current', picindex);
                            $slider.data('dc_anim', false);                             
                        });                    
                }
                else
                {
                    $slider.data('dc_anim', true);
                    $slider.find('.slide:eq('+picindex+')').css('display', 'block');
                    $slider.find('.slide:eq('+dc_current+')').css('display', 'none');
                    $slider.data('dc_current', picindex);
                    $slider.data('dc_anim', false);                     
                }

    
                if(dc_scroll)
                {   
                    // calculate side
                    var left_side = false;
                    var click_side = index * real_twidth;
                    // if(click_side < (width / 2))
                    if(index <= dc_li_index)
                    {
                        left_side = true; 
                    }
                    
                    if(!left_side)
                    {                   
                    $ts.find('li').css('border-color', normalBorderColor);
                    $ts.find('li span').css('opacity', 1.0); 
                    q(this).css('border-color', userBorderColor);
                    q(this).find('span').css('opacity', 0.0);  
                                         
                    $ts.stop().animate({left:pos.left-real_twidth}, 600,
                    function()
                    {                   
                        var li = q(this).find('li:first').detach();
                        li.appendTo(this);
                        var pos = $ts.position();
                        q(this).css('left', pos.left+real_twidth);
                        $slider.data('dc_block', false); 
                    });
                    
                    index--;
                    } else
                    {
                    $ts.find('li').css('border-color', normalBorderColor);
                    $ts.find('li span').css('opacity', 1.0); 
                    q(this).css('border-color', userBorderColor);
                    q(this).find('span').css('opacity', 0.0);  
                    
                    
                        var li = $ts.find('li:last').detach();
                        li.prependTo($ts);
                        var pos = $ts.position();
                        $ts.css('left', pos.left-real_twidth);
                        
                        pos.left -= real_twidth;                                         
                    $ts.stop().animate({left:pos.left+real_twidth}, 600,
                    function()
                    {                   
                          $slider.data('dc_block', false); 
                    });
                    
                    index++;
                    }
                } else
                {
                    $ts.find('li').css('border-color', normalBorderColor);
                    $ts.find('li span').css('opacity', 1.0); 
                    q(this).css('border-color', userBorderColor);
                    q(this).find('span').css('opacity', 0.0);
                    $slider.data('dc_block', false);                   
                }             

                $slider.data('dc_li_index', index);  
                
            });           
       
       });    
}

/*************************************
* SEARCH FORM CODE
**************************************/
function setupSearchForm()
{
   // var q = jQuery.noConflict();  
        
   q('.dc-widget-search .submit-btn, .search-page-controls .submit-btn, #header-container .search-panel .submit-btn').click(
        function()
        {
            var parent = q(this).parent();
            if(q(parent).find('input[name=s]').length)
            {
                if(q(parent).find('input[name=s]').val() == '')
                {
                    q(parent).find('input[name=s]').css('border', '1px solid #880000');
                    return false;
                } else
                {
                    q(parent).submit(); 
                }
            }
        }
   );
   
   q('.dc-widget-search form, .search-page-controls form, #header-container .search-panel form').submit(
        function()
        {            
            if(q(this).find('input[name=s]').length)
            {
                if(q(this).find('input[name=s]').val() == '')
                {
                    q(this).find('input[name=s]').css('border', '1px solid #880000');
                    return false;
                } else
                {
                    return true;
                }
            }
        }
   );  
        
}

/*************************************
* HOMEPAGE TABS CODE
**************************************/
function setupHomepageTabs()
{
  //  var q = jQuery.noConflict();     

    var tabs = q('#homepage-tabs')[0];        
    var $tabs = q('#homepage-tabs');
    $tabs.data('dc_block', false);
    var selected = q('.btn-bar .btn-selected', tabs).length;
    if(selected == 0)
    {   
        q('.btn-bar .btn:first', tabs).addClass('btn-selected');     
    }
    if(selected > 1)
    {
        q('.btn-bar .btn', tabs).removeClass('btn-selected');
        q('.btn-bar .btn:first', tabs).addClass('btn-selected');      
    }
    selected = q('.btn-bar .btn-selected:first', tabs); 
    var selected_index = q(selected).index();
    
    var height = q('.tab:eq('+selected_index+')', tabs).height();
    q('.tabs-wrapper', tabs).css('height', height);
    
    
    q('.tab:eq('+selected_index+')', tabs).css('opacity', 0.0).css('display', 'block').animate({opacity:1.0},500);
    
    q('.btn-bar .btn', tabs).click(function(){
        var index = q(this).index();
       
        var old_selected_index = q('.btn-bar .btn-selected').index();
        if(index == old_selected_index) { return; }
        var block = $tabs.data('dc_block');
        if(block) { return; }
        
        $tabs.data('dc_block', true);
        q('.btn-bar .btn').removeClass('btn-selected');
        q(this).addClass('btn-selected');
        
       
        q('.tab:eq('+old_selected_index+')', tabs).stop().animate({opacity:0.0}, 100, function() {
         
            q(this).css('display', 'none');
            height = q('.tab:eq('+index+')', tabs).height();
            q('.tabs-wrapper', tabs).stop().animate({height:height}, 250, function() {
            
                q('.tab:eq('+index+')', tabs).stop().css('opacity', 0.0).css('display', 'block').animate({opacity:1.0}, 150, function(){ $tabs.data('dc_block', false); });    
                
            });                                                              
        });
    }); 
    
    q('.thumb-bar .thumb', tabs).click(function() {
        var index = q(this).index();
        q(this).parent().parent().find('.gallery-box a:eq('+index+')', tabs).click();            
    });    
}

function setupHeaderIcons()
{
  //  var q = jQuery.noConflict();
    
    q('#header-container .header-icons-panel .hovered').hover(
        function() {
            
            var h = q(this).height();
            q(this).find('img').css('top', -h);
                   
        },
        
        function() {
            q(this).find('img').css('top', 0);    
        }
    );
}


/***************************************************
  GROUP FADING
****************************************************/
function setupGroupFading()
{
   // var q = jQuery.noConflict();
        
    q('.blog-post-related .item .thumb-wrapper').hover(
      function()
      {
            var wrapper = q(this);
            var allwrapper = q(this).parent().parent();
            allwrapper.find('.thumb-wrapper').not(wrapper).stop().animate({opacity:0.75}, 300);                  
      },
      function()
      {
            var wrapper = q(this);
            var allwrapper = q(this).parent().parent();
            allwrapper.find('.thumb-wrapper').not(wrapper).stop().animate({opacity:1.0}, 300);          
      }
    );     
    
    q('.dc-widget-ngg-images .thumb .triger').hover(
      function()
      {
            q(this).stop().animate({opacity:0.6}, 300);                  
      },
      function()
      {
            q(this).stop().animate({opacity:0.0}, 300);           
      }
    );       
    
             
}




/***************************************************
  SIDEBAR POSTS SLIDER WIDGET
****************************************************/
function setupSidebarPostSlider()
{
   // var q = jQuery.noConflict();
    
    q('.dc-widget-postslider').each(
        function()
        {
            var size = q(this).find('.slide').length;
            q(this).find('.slide:first').css('display', 'block');                         
            
            var slider = q(this);
            q(this).data('dc_size', size);
            q(this).data('dc_current', 0);
            q(this).data('dc_block', false); 
            
            q(this).find('.btn-bar a').click(
                function()
                {                                        
                    var slider = q(this).parent().parent();
                    var bar = q(this).parent();
                    var index = q(bar).find('a').index(this);
                    var prev = slider.data('dc_current');
                    
                   if(index != prev)
                   { 
                        if(slider.data('dc_block'))
                        {
                            return;
                        }
                        slider.data('dc_block', true);                   
                                                          
                        clearTimeout(slider.data('dc_handle'));                        
                       
                        bar.find('a.btn-active').removeClass('btn-active').addClass('btn');                  
                        bar.find('a:eq('+index+')').removeClass('btn').addClass('btn-active'); 
                                        
                        slider.find('.slide:eq('+prev+')').stop().animate({opacity: 0.0}, 300);
                        
                                slider.find('.slide').css('z-index', 0);
                                slider.find('.slide:eq('+index+')').css('z-index', 1);
                                slider.find('.slide:eq('+index+')').stop().css('opacity', 0.0).css('display', 'block').animate({opacity: 1.0}, 800);
                                slider.data('dc_current', index);
                                slider.data('dc_block', false); 
                              
                                if(slider.data('dc_size') > 1)
                                {
                                    var handle = setTimeout(function(){widget_minislider_autoplay(slider);}, 8000)
                                    slider.data('dc_handle', handle); 
                                }   
                         
                   }       
                }
            );
            
            function widget_minislider_autoplay(slider)
            {  
                if(slider.data('dc_block'))
                {
                    return;
                }
                slider.data('dc_block', true); 
                
                var prev = slider.data('dc_current');
                var size = slider.data('dc_size'); 
                var next = prev + 1;
                if(next >= size)
                {
                    next = 0;
                }
                
                slider.find('.btn-bar').find('a.btn-active').removeClass('btn-active').addClass('btn');
                slider.find('.btn-bar').find('a:eq('+next+')').removeClass('btn').addClass('btn-active');  
                
                slider.find('.slide:eq('+prev+')').stop().animate({opacity: 0.0}, 300);
                
                        slider.find('.slide').css('z-index', 0);
                        slider.find('.slide:eq('+next+')').css('z-index', 1);                        
                        slider.find('.slide:eq('+next+')').stop().css('opacity', 0.0).css('display', 'block').animate({opacity: 1.0}, 600);
                        slider.data('dc_current', next); 
                        slider.data('dc_block', false);                
                      
                        var handle = setTimeout(function(){widget_minislider_autoplay(slider);}, 8000);
                        slider.data('dc_handle', handle);                                
                  
            }
            
             if(size > 1)
             {
                var handle = setTimeout(function(){widget_minislider_autoplay(slider);}, 8000)
                q(this).data('dc_handle', handle);
             }              
        }    
    );
}

/***************************************************
  SETUP TAGS WIDGET
****************************************************/

function setupTagsWidget()
{
    var arr = Array();
    q('.dc-widget-tags a').each(
      function()
      {              
          var str = q(this).attr('title');
          str = str.replace("topics", "");
          str = str.replace("topic", "");
          var number = parseInt(str);
          
          var obj = new Object();
          obj.topics = number;
          obj.ref = this;
          arr.push(obj);
      }
    );
    
    var count = arr.length;
    if(count > 0)
    {
        var min = arr[0].topics;
        var max = arr[0].topics;
        for(var i = 0; i < count; i++)
        {
            if(arr[i].topics > max) { max = arr[i].topics; }
            if(arr[i].topics < min) { min = arr[i].topics; }
        }
        
        var step = (max - min)/5;
        for(var i = 0; i < count; i++)
        {
            if(arr[i].topics <= min+(0*step)) { q(arr[i].ref).css('color', '#AAAAAA'); } else
            if(arr[i].topics <= min+(1*step)) { q(arr[i].ref).css('color', '#999999'); } else
            if(arr[i].topics <= min+(2*step)) { q(arr[i].ref).css('color', '#888888'); } else
            if(arr[i].topics <= min+(3*step)) { q(arr[i].ref).css('color', '#666666'); } else
            if(arr[i].topics <= min+(4*step)) { q(arr[i].ref).css('color', '#444444'); } else
            if(arr[i].topics <= min+(5*step)) { q(arr[i].ref).css('color', '#222222'); }                             
        }
    }    
}

function setupSplashScreen()
{
    if(q('#dc-splash-screen').length)
    {
        var browser_width = q(window).width();
        var $screen = q('#dc-splash-screen');
        var $screen_bg = q('#dc-splash-screen-bg');
        var screen_width = $screen.width();
        
        var left = Math.round(browser_width / 2) - Math.round(screen_width / 2);
        $screen.css('left', left);
        
        $screen.find('.close-btn').click(function() 
        {  
            if($screen.find('.no-cookie').length == 0)
            {
                dcf_createCookie('dc_is_splash_screen', '1', 0);
            }
            $screen.remove();
            $screen_bg.remove();    
        });
        
        $screen.find('.btn').click(function() 
        {  
            if($screen.find('.no-cookie').length == 0)
            {
                dcf_createCookie('dc_is_splash_screen', '1', 0);
            }
            $screen.remove();
            $screen_bg.remove();    
        });   
        
        $screen.find('.image-wrapper').click(function() 
        {  
            if($screen.find('.no-cookie').length == 0)
            {            
                dcf_createCookie('dc_is_splash_screen', '1', 0);
            }
            $screen.remove();  
            $screen_bg.remove();  
        });
        
        q(window).resize(function()
        {
            var browser_width = q(window).width(); 
            var screen_width = $screen.width(); 
            var left = Math.round(browser_width / 2) - Math.round(screen_width / 2);
            $screen.css('left', left);                
        });                                  
    }    
}

/***************************************************
  FLOATING OBJECTS
****************************************************/
function setupFloatingObjects()
{
  //  var q = jQuery.noConflict();
    q('.fobject').hover(
    function()
    {
        q(this).find('.image-hover').css('display', 'inline');        
    },
    function()
    {
        q(this).find('.image-hover').css('display', 'none');    
    }
    );
    
    q('.fobject .close-btn').click(
        function()
        {
            q(this).parent().remove();
            return false;
        }
    );     
}
               
/***************************************************
  DCS SIMPLE GALLERY
****************************************************/
function setupDcsSimpleGallery()
{  
   // var q = jQuery.noConflict();
    
    q('.dcs-simple-gallery').each(
        function()
        {
            q(this).find('.slide:first').css('display', 'block');
            q(this).find('.slide:first .slide-desc').css('opacity', 0.85); 
            q(this).find('.slider').data('dc_current', 0);
            q(this).find('.slider').data('dc_block', false);
            
            var mode = q(this).find('.trans').html();
            var count = q(this).find('.slide').length;
            var width = q(this).find('.slide').width();
            var twidth = q(this).find('.thumb:first').width() + 5; 
            
            var offset = (width/2) - ((count*twidth)/2);
            for(var i = 0; i < count; i++)
            {
                q(this).find('.thumb:eq('+i+')').css('left', offset+i*twidth);    
            }
            
            q(this).hover(
              function()
              {
                  q(this).find('.thumb').stop().animate({opacity:1.0}, 500);
              },
              function()
              {
                  q(this).find('.thumb').stop().animate({opacity:0.0}, 1000); 
              }
            );
            
            q(this).find('.thumb').click(
              function()
              {
                  var slider = q(this).parent();
                  var index = slider.find('.thumb').index(this);                                     
                   
                  if(slider.data('dc_block') || index == slider.data('dc_current'))
                  {   
                      return;
                  }     
                  slider.data('dc_block', true);
                  var current = slider.data('dc_current');
                  slider.find('.slide:eq('+current+')').css('z-index', 1);
                     
                  if(mode == 'none')
                  {
                      slider.find('.slide:eq('+index+') .slide-desc').css('opacity', 0.0); 
                      slider.find('.slide:eq('+index+')').css('opacity', 1.0).css('z-index', 2).css('display', 'block');                                     
                      slider.find('.slide:eq('+current+')').css('display', 'none');
                      slider.data('dc_current', index);
                      slider.data('dc_block', false);
                      slider.find('.slide:eq('+index+') .slide-desc').stop().css('opacity', 0.0).animate({opacity:0.85}, 800);
                  } else
                  if(mode == 'fade')
                  {   
                      slider.find('.slide:eq('+index+') .slide-desc').css('opacity', 0.0);
                      slider.find('.slide:eq('+index+')').css('opacity', 0.0).css('z-index', 2).css('display', 'block').animate({opacity: 1.0}, 400, 
                        function()
                        {
                            var slider = q(this).parent();
                            slider.find('.slide:eq('+current+')').css('display', 'none');
                            slider.data('dc_current', index);
                            slider.data('dc_block', false);
                            
                            q(this).find('.slide-desc').stop().css('opacity', 0.0).animate({opacity:0.85}, 800);
                        });                      
                  } else
                  if(mode == 'slide')
                  {
                      var src_left = width;
                      if(index < current)
                      {
                        src_left = -width;    
                      }
                      
                      slider.find('.slide:eq('+index+') .slide-desc').css('opacity', 0.0);
                      slider.find('.slide:eq('+index+')').css('left', src_left).css('z-index', 2).css('display', 'block').animate({left: 0}, 400, 
                        function()
                        {
                            var slider = q(this).parent();
                            slider.find('.slide:eq('+current+')').css('display', 'none');
                            slider.data('dc_current', index);
                            slider.data('dc_block', false);
                            q(this).find('.slide-desc').stop().css('opacity', 0.0).animate({opacity:0.85}, 800); 
                        }); 
                    
                  } else
                  {
                      slider.find('.slide:eq('+index+') .slide-desc').css('opacity', 0.0);
                      slider.find('.slide:eq('+index+')').css('opacity', 1.0).css('z-index', 2).css('display', 'block');                                     
                      slider.find('.slide:eq('+current+')').css('display', 'none');
                      slider.data('dc_current', index);
                      slider.data('dc_block', false);
                      slider.find('.slide:eq('+index+') .slide-desc').stop().css('opacity', 0.0).animate({opacity:0.85}, 800);                       
                  }                    
                  
                  
                  
              }
            );
                
        });         
}


function setupDcsSimpleGalleryThumbs()
{    
   // var q = jQuery.noConflict();
    
    q('.dcs-simple-gallery-thumbs .thumb').hover(
      function()
      {
         q(this).stop().animate({opacity:0.6}, 200);    
      },
      function()
      {
         q(this).stop().animate({opacity:1.0}, 900); 
      }
    );
    
    q('.dcs-simple-gallery-thumbs').each(
        function()
        {
            q(this).find('.slide:first').css('display', 'block');
            q(this).find('.slide:first .slide-desc').css('opacity', 0.85); 
            q(this).find('.slider').data('dc_current', 0);
            q(this).find('.slider').data('dc_block', false);
            
            var mode = q(this).find('.trans').html();           
            var count = q(this).find('.slide').length;
            var width = q(this).find('.slide').width();
     
            q(this).find('.thumb').click(
              function()
              {
                  
                  var wrapper = q(this).parent();
                  var index = wrapper.find('.thumb').index(this);
                   
                  var slider = q(this).parent().parent().find('.slider'); 
                  if(slider.data('dc_block') || index == slider.data('dc_current'))
                  {
                      return;
                  }
                  slider.data('dc_block', true);
                  var current = slider.data('dc_current');
                  slider.find('.slide:eq('+current+')').css('z-index', 1);

                        
                  if(mode == 'none')
                  {
                      slider.find('.slide:eq('+index+') .slide-desc').css('opacity', 0.0); 
                      slider.find('.slide:eq('+index+')').css('opacity', 1.0).css('z-index', 2).css('display', 'block');                                     
                      slider.find('.slide:eq('+current+')').css('display', 'none');
                      slider.data('dc_current', index);
                      slider.data('dc_block', false);
                      slider.find('.slide:eq('+index+') .slide-desc').stop().css('opacity', 0.0).animate({opacity:0.85}, 800);
                  } else
                  if(mode == 'fade')
                  {
                      slider.find('.slide:eq('+index+') .slide-desc').css('opacity', 0.0); 
                      slider.find('.slide:eq('+index+')').css('opacity', 0.0).css('z-index', 2).css('display', 'block').animate({opacity: 1.0}, 400, 
                        function()
                        {
                            var slider = q(this).parent();
                            slider.find('.slide:eq('+current+')').css('display', 'none');
                            slider.data('dc_current', index);
                            slider.data('dc_block', false);
                            q(this).find('.slide-desc').stop().css('opacity', 0.0).animate({opacity:0.85}, 800);
                        });                      
                  } else
                  if(mode == 'slide')
                  {
                      var src_left = width;
                      if(index < current)
                      {
                        src_left = -width;    
                      }
                      
                      slider.find('.slide:eq('+index+') .slide-desc').css('opacity', 0.0); 
                      slider.find('.slide:eq('+index+')').css('left', src_left).css('z-index', 2).css('display', 'block').animate({left: 0}, 400, 
                        function()
                        {
                            var slider = q(this).parent();
                            slider.find('.slide:eq('+current+')').css('display', 'none');
                            slider.data('dc_current', index);
                            slider.data('dc_block', false);
                            q(this).find('.slide-desc').stop().css('opacity', 0.0).animate({opacity:0.85}, 800);
                        }); 
                    
                  } else
                  {
                      slider.find('.slide:eq('+index+') .slide-desc').css('opacity', 0.0); 
                      slider.find('.slide:eq('+index+')').css('opacity', 1.0).css('z-index', 2).css('display', 'block');                                     
                      slider.find('.slide:eq('+current+')').css('display', 'none');
                      slider.data('dc_current', index);
                      slider.data('dc_block', false);
                      slider.find('.slide:eq('+index+') .slide-desc').stop().css('opacity', 0.0).animate({opacity:0.85}, 800);                      
                  }
               
              });
                
        });        
}


/***************************************************
  SETUP DCS STORY
****************************************************/
function setupDCStory()
{
        q('.dc-story').each(function()
          {
              var $story = q(this); 
              var count = $story.find('.slide').length;
              var width = $story.width();
              $story.data('dc_count', count);
              $story.data('dc_active', 0);
              $story.data('dc_block', false);  
              $story.find('.info-bar .active').text(1); 
              $story.find('.info-bar .count').text(count);
              $story.find('.slide:first').css('display', 'block');
              $story.find('.description:first').css('display', 'block'); 
              
              $story.find('.info-bar .next').click(
                function()
                {
                    var is_blocked = $story.data('dc_block');
                    if(!is_blocked)
                    {
                        $story.data('dc_block', true);
                        var active = $story.data('dc_active');                        
                        var count = $story.data('dc_count', count);
                        
                        var next = active + 1;
                        if(next >= count) { next = 0; }    
                        
                        $story.find('.description:eq('+active+')').stop().animate({opacity:0.0}, 200,
                            function()
                            {
                                q(this).css('left', width).css('display', 'none');
                                $story.find('.description:eq('+next+')').stop().css('opacity', 0.0).css('display', 'block').css('left', 0).animate({opacity:0.9}, 500);     
                            });                          
                        
                        $story.find('.slide:eq('+active+')').stop().animate({opacity:0.0}, 200,
                            function()
                            {
                                q(this).css('left', width).css('display', 'none');
                                $story.find('.info-bar .active').text((next+1)); 
                                $story.find('.slide:eq('+next+')').stop().css('opacity', 0.0).css('display', 'block').css('left', 0).animate({opacity:1.0}, 500,
                                function() { 
                                        $story.data('dc_block', false); 
                                        $story.data('dc_active', next);
                                } );     
                            });     
                    }    
                }
              );

              $story.find('.info-bar .prev').click(
                function()
                {
                    var is_blocked = $story.data('dc_block');
                    if(!is_blocked)
                    {
                        $story.data('dc_block', true);
                        var active = $story.data('dc_active');                        
                        var count = $story.data('dc_count', count);
                        
                        var next = active - 1;
                        if(next < 0) { next = count - 1; }    
                        
                        
                        $story.find('.description:eq('+active+')').stop().animate({opacity:0.0}, 200,
                            function()
                            {
                                q(this).css('left', width).css('display', 'none');
                                $story.find('.description:eq('+next+')').stop().css('opacity', 0.0).css('display', 'block').css('left', 0).animate({opacity:0.9}, 500);     
                            });                           
                        
                        $story.find('.slide:eq('+active+')').stop().animate({opacity:0.0}, 200,
                            function()
                            {
                                q(this).css('left', width).css('display', 'none');
                                $story.find('.info-bar .active').text((next+1)); 
                                $story.find('.slide:eq('+next+')').stop().css('opacity', 0.0).css('display', 'block').css('left', 0).animate({opacity:1.0}, 500,
                                function() { 
                                        $story.data('dc_block', false); 
                                        $story.data('dc_active', next);
                                } );     
                            });                           
                    }    
                }
              );
              
              $story.find('.triger').hover(
                function()
                { 
                    q(this).stop().animate({opacity:0.3}, 200); 
                }, 
                function()
                { 
                    q(this).stop().animate({opacity:0.0}, 200); 
                }
              );
          });    
}

/***************************************************
  COMMON CODE
****************************************************/
function setupCommonActions()
{
  //  var q = jQuery.noConflict();
    q('.fade-in').animate({opacity:1.0}, 1000);     
}


function setupMiscellaneous()
{  
  //  var q = jQuery.noConflict();
  
    var dc_admin_url = jQuery('meta[name=cms_admin_url]').attr('content');
    
    q('.dcs-post-box .triger').hover(
      function()
      {
       q(this).stop().animate({opacity:0.5}, 300);    
      },
      function()
      {
        q(this).stop().animate({opacity:0.0}, 300);  
      }
    );
    
    q('.dcs-ngg-gallery-thumbs .triger').hover(
      function()
      {
        q(this).stop().animate({opacity:0.5}, 300);    
      },
      function()
      {
        q(this).stop().animate({opacity:0.0}, 300);  
      }
    );
    
    q('.team-member-small .main-photo, .team-member-small-last .main-photo, .team-member-big .main-photo').hover(
        function()
        {
            if(q(this).attr('href') != undefined)
            {
                q(this).stop().animate({opacity:0.8}, 300);
            }   
        },
        function()
        {
            if(q(this).attr('href') != undefined)
            {
                q(this).stop().animate({opacity:1.0}, 300);
            }              
        }
    );       
        
    q('.faq-question .quest-head').click(
        function()
        {
            if(q(this).hasClass('head-open'))
            {
                q(this).parent().find('.quest-content').css('display', 'none');
                q(this).removeClass('head-open').addClass('head-close');   
            } else
            {
                q(this).parent().find('.quest-content').css('display', 'block');
                q(this).removeClass('head-close').addClass('head-open'); 
                
                if(q(this).find('.views').length)
                {
                    var postid = q(this).find('.postid').text();

                    $head = q(this);
                    
                        q.ajax({
                       type: "POST",
                       context:q(this),
                       url: dc_admin_url+'admin-ajax.php',
                       data: "action=dc_questpost&postid="+postid,
                       success: function(str, status){
                               
                                q(this).find('.count').text(str); 
                       }
                     });
                }                                       
            } 
        });
        
    var licount = q('#header-top-menu li').length;
    if(licount > 1)
    {
        q('#header-top-menu li').slice(0, licount-1).addClass('separator');  
    }      
    
    
    q('.tag-bar-page li .strip').each(function() { var w = q(this).width(); q(this).data('dc_w', w); });
    q('.tag-bar-page li').hover(
      function()
      {
          $strip = q(this).find('.strip');
          
          $strip.stop().animate({opacity:0.0}, 200, function(){ q(this).css('display', 'none');});
          q(this).find('.link').css('display', 'block');      
      },
      function()
      {
          $strip = q(this).find('.strip'); 
          var w = $strip.data('dc_w'); 
          
          $strip.css('width', 0).css('display', 'block').stop().animate({opacity:0.8, width:w}, 1000);
          q(this).find('.link').css('display', 'none');     
      }
    );
              
    q('.hidden-content .head span').click(
        function()
        {
            if(q(this).hasClass('open'))
            {
                q(this).parent().parent().find('.data').css('display', 'block');
                q(this).removeClass('open').addClass('close');   
            } else
            {
                q(this).parent().parent().find('.data').css('display', 'none');
                q(this).removeClass('close').addClass('open');                      
            } 
        });              
             
}

/***************************************************
  CUFON
****************************************************/                  
function dc_setupCufonFont()
{                                      
  //  var q = jQuery.noConflict(); 
    var dc_font_type = jQuery('meta[name=cms_font_type]').attr('content');

    if(dc_font_type == 'cufon')
    {    
        
        /*var headings = q('h1, h2, h3, h4, h5, h6').not('.nocufon');*/        var headings = q('').not('.nocufon');
        
        Cufon.replace(headings, {fontWeight: 400});           
    }
} 

/***************************************************
  PAGE MAIN CODE
****************************************************/
var q = jQuery.noConflict();
var dc_theme_path = jQuery('meta[name=cms_theme_url]').attr('content');
var dc_theme_name = jQuery('meta[name=cms_theme_name]').attr('content');  
                 
// main code called when page is loaded 
jQuery(document).ready(function($) 
    { 
     //   var q = jQuery.noConflict();  
        
        dc_setupCufonFont();
      //  setupDCStory();
        setupMiscellaneous();
        setupDcsSimpleGallery();              
        setupDcsSimpleGalleryThumbs();
        setupFloatingObjects();
        setupCommonActions();
        setupSplashScreen();
        setupThemeImages();
        dc_setupLinks(); 
        setupGroupFading();
        setupSidebarPostSlider();
        setupHomeFeaturedGallery();
        setupHeaderIcons();
        dc_setupServices();
        //setupGalleryList();  
        // setupTagsWidget();
        setupCompactAndTableGallery();
        setupPortfolio();
        setupSearchForm();
        setupQuickNewsPanel();     
        setupAsyncImages('.async-img, .async-img-s, .async-img-black, .async-img-s-black, .async-img-none, .async-img-uni, .async-img-s-uni');
        setupChainSlider();
        setupPPhoto();  
        setupNewsPage();    
        setupClientPanel();    
        setupPopUpImages('.pu_img');
        setupWordPressBlogPosts();
        dc_setupContactForm();            
        dc_setupThemeTour();
        setupHomepageTabs();
        
        
        var cms_tip_bgcolor = jQuery('meta[name=cms_tip_bgcolor]').attr('content'); 
        var cms_tip_color = jQuery('meta[name=cms_tip_color]').attr('content'); 
        var cms_tip_bordercolor = jQuery('meta[name=cms_tip_bordercolor]').attr('content'); 
             
        
        $('.tip-left-bottom').dcTipCreate({pos:'left-bottom',color:cms_tip_color,bgcolor:cms_tip_bgcolor,bordercolor:cms_tip_bordercolor,use_width:true});
        $('.tip-bottom').dcTipCreate({pos:'bottom',color:cms_tip_color,bgcolor:cms_tip_bgcolor,bordercolor:cms_tip_bordercolor,use_width:true});
        $('.tip-top').dcTipCreate({pos:'top',color:cms_tip_color,bgcolor:cms_tip_bgcolor,bordercolor:cms_tip_bordercolor,use_width:true}); 
        /* core navigation menu */
        q('ul.sf-menu').superfish();
        q('ul.sf-menu a:first').css('border-left', 'none');        
        
        /* dc sliders */ 
        if(q('#accordion-container').length)
        {
            slider_acc.setup('#accordion-container');
        }
        if(q('#slider-a-container').length)
        {
            slider_a.setup('#slider-a-container');
        }    
        

                  
}); 
