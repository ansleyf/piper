<?php
/**********************************************************************
* MODERN ELEGANCE WORDPRESS THEME 
* (Ideal For Business And Personal Use: Portfolio or Blog)   
* 
* File name:   
*      404.php
* Brief:       
*      Theme 404 page code
* Author:      
*      DigitalCavalry
* Author URI:
*      http://themeforest.net/user/DigitalCavalry
* Contact:
*      digitalcavalry@gmail.com   
***********************************************************************/ 

    get_header();                                                
?>

    <div id="content">
         
        <?php 
            echo '<div class="page-width-full">';      
            $title = GetDCCPInterface()->getIGeneral()->get404Title();
            $text = GetDCCPInterface()->getIGeneral()->get404Text();
            dcf_naviTree($post->ID, 0, $title);   

            echo '<h1>'.$title.'</h1>';
            echo $text;
            echo '<div style="height:400px;"></div>';                                      
        ?>
                           
        </div>  <!-- page-width-xx0 -->
        <div class="clear-both"></div>
    </div> <!-- content -->
    
<?php    
    get_footer();
?>



