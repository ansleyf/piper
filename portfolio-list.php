<?php
/*
Template Name: Portfolio List
*/

/**********************************************************************
* MODERN ELEGANCE WORDPRESS THEME 
* (Ideal For Business And Personal Use: Portfolio or Blog)   
* 
* File name:   
*      portfolio-list.php
* Brief:       
*      Theme portfolio list page template code
* Author:      
*      DigitalCavalry
* Author URI:
*      http://themeforest.net/user/DigitalCavalry
* Contact:
*      digitalcavalry@gmail.com   
***********************************************************************/ 

    get_header();                                                
?>

    <div id="content">
         
        <?php 
            $page_common_opt = $GLOBALS['dc_pagecommon_opt'];            
            GetDCCPInterface()->getIGeneral()->includeSidebar($page_common_opt['page_sid'], $page_common_opt['page_sid_pos']);
            
            if(GetDCCPInterface()->getIGeneral()->getSidebarGlobalPos($page_common_opt['page_sid_pos']) == CMS_SIDEBAR_RIGHT)
            {
                echo '<div class="page-width-left">';                              
            } else
            {
                echo '<div class="page-width-right">';     
            }   
            dcf_naviTree($post->ID, 0);   

            echo '<h1>'.$post->post_title.'</h1>';
            the_content();
            
            $paged = (get_query_var('paged')) ? get_query_var('paged') : 1; 
            $max_page = 1;
            $list_opt = get_post_meta($post->ID, 'portlist_opt', true);
            //var_dump($list_opt); 
            
            $pages_list = trim((string)$list_opt['pl_selected']);            
            $per_page = (int)$list_opt['pl_per_page'];
            if($per_page < 1) { $per_page = 1; }
            $image_height = (int)$list_opt['pl_height'];
            if($image_height < 80) { $image_height = 80; }
                            
            $querystr = '';           
            if($pages_list != '')
            {                               
                $querystr .= "SELECT SQL_CALC_FOUND_ROWS ID, post_title, post_content, post_excerpt, post_author ";
                $querystr .= "FROM $wpdb->posts ";
                $querystr .= "WHERE $wpdb->posts.post_type = 'page' AND $wpdb->posts.post_status = 'publish' ";                
                $querystr .= "AND $wpdb->posts.ID IN ($pages_list) ";
                $querystr .= "ORDER BY FIELD(ID, $pages_list) ";
                $querystr .= "LIMIT ".($per_page*($paged-1)).", $per_page ";
           } else
           {     
                $querystr = "
                    SELECT SQL_CALC_FOUND_ROWS DISTINCT ID, post_title, post_content, post_excerpt, post_author, post_date, meta_value
                    FROM $wpdb->posts
                    LEFT JOIN $wpdb->postmeta ON($wpdb->posts.ID = $wpdb->postmeta.post_id)";                      
                    
                $querystr .= "WHERE $wpdb->posts.post_type = 'page'                     
                    AND $wpdb->posts.post_status = 'publish'
                    AND $wpdb->postmeta.meta_key = '_wp_page_template' 
                    AND $wpdb->postmeta.meta_value = 'portfolio.php' ";                              
                                   
                $querystr .= "ORDER BY $wpdb->posts.post_date DESC LIMIT ".($per_page*($paged-1)).", $per_page ";                 
           }
                
                global $wpdb;
                $data = $wpdb->get_results($querystr, OBJECT);                
                $allitems = dcf_getWPDBFoundRows(); 
                if($per_page > 0) { $max_page = (int)ceil($allitems / $per_page); } else { $max_page = 1; } 
                
                if(is_array($data))
                {
                    $counter = 0;
                    foreach($data as $pt)
                    {
                        $out = '';                        
                        if($counter > 0)
                        {
                            $out .= '<div class="portfolio-list-item-separator"></div>';
                        }
                        
                        $page_meta = get_post_meta($pt->ID, 'pagecommon_opt', true);
                        $image_url = $page_meta['page_image'];
                        $image_url = dcf_isNGGImageID($image_url);
                        
                        $out .= '<div class="portfolio-list-item">';
                        
                            // image
                            if($image_url != '')
                            {
                                $out .= '<div class="image-wrapper">'; 
                                    $img_style = ' style="width:608px;height:'.$image_height.'px;" ';
                                    $out .= '<a '.$img_style.' class="async-img portfolio-list-item-image" rel="'.dcf_getTimThumbURL($image_url, 608, $image_height).'"></a>';    
                                    $out .= '<a '.$img_style.' class="triger" href="'.get_permalink($pt->ID).'"></a>';                                
                                $out .= '</div>';
                                $out .= '<div class="portfolio-list-image-desc-empty"></div>';                        
                            }
                        
                            $out .= '<div class="content">';                        
                            
                            // title
                            $out .= '<h2 class="post-title">';                        
                                $out .= '<a href="'.get_permalink($pt->ID).'" >';
                                $out .= $pt->post_title;
                                $out .= '</a>';
                            $out .= '</h2>';
                            
                            // info bar
                            $authorinfo = get_user_by('id', $pt->post_author); 
                            $out .= '<div class="post-info">';
                                $out .= 'By '.'<a href="'.get_author_posts_url($authorinfo->ID).'" class="author">'.$authorinfo->display_name.'</a>';
                            $out .= '</div>';
							
							$out .= '<div class="portfolio-desc">';
                            
                            // desc
                            if($pt->post_excerpt != '')
                            {
                                $out .= $pt->post_excerpt;    
                            } else
                            if($pt->post_content != '')
                            {
                                $out .= $pt->post_content;     
                            }
                            $out .= '</div>';
							$out .= ' <a class="more-link" href="'.get_permalink($pt->ID).'">&raquo;&nbsp;'.__('View&nbsp;portfolio', CMS_TXT_DOMAIN).'</a>';
                        
                        $out .= '</div>';
                        $out .= '</div>';
                        
                        
                        echo $out;
                        
                        $counter++;
                    }    
                }
            
            
            if($max_page == 1) { echo '<div style="height:10px;"></div>'; }
            GetDCCPInterface()->getIGeneral()->renderSitePagination($paged, $max_page);                                                               
        ?>
                           
        </div>  <!-- page-width-xx0 -->
        <div class="clear-both"></div>
    </div> <!-- content -->
    
<?php    
    get_footer();
?>



