<?php
/*
Template Name: Tour
*/

/**********************************************************************
* MODERN ELEGANCE WORDPRESS THEME 
* (Ideal For Business And Personal Use: Portfolio or Blog)   
* 
* File name:   
*      tour.php
* Brief:       
*      Theme tour page template code
* Author:      
*      DigitalCavalry
* Author URI:
*      http://themeforest.net/user/DigitalCavalry
* Contact:
*      digitalcavalry@gmail.com   
***********************************************************************/ 

    get_header();                                                
?>

    <div id="content">
         
        <?php 
            echo '<div class="page-width-full">';       
            dcf_naviTree($post->ID, 0);   

            echo '<h1>'.$post->post_title.'</h1>';
            the_content();
            
            $tour_opt = get_post_meta($post->ID, 'tourpage_opt', true);
            $tour_posts = explode(',', $tour_opt['to_posts']);
                        
            $args = Array(
                'post_type' => CPThemeCustomPosts::PT_TOUR_POST,
                'post_status' => 'publish', 
                'posts_per_page' => -1,
                'post__in' => $tour_posts);
            query_posts($args);             
            $posts_count = $wp_query->post_count;
            
            // sorting posts in order which was writen by user in page custom field
            $new_posts_array = Array();
            foreach($tour_posts as $t)
            {
                // finding posts with given id
                $index = -1;
                for($i = 0; $i < $wp_query->post_count; $i++)
                {
                    if($wp_query->posts[$i]->ID == $t)
                    {
                        $index = $i;
                        break;
                    }
                }
                if($index != -1)
                {
                    array_push($new_posts_array, $wp_query->posts[$index]);
                }
            }
            $wp_query->posts = $new_posts_array;
            
        if(have_posts())
        {
            $out = '';
            $out .= '<div class="tour-slider">';
            
                $out .= '<ul class="panel">';                
                for($i = 0; $i < $wp_query->post_count; $i++)
                {
                    $slide_opt = get_post_meta($wp_query->posts[$i]->ID, 'tour_opt', true);                   
                    
                    $icon = '';
                    $icon_hover = '';
                    if($slide_opt['image_url'] != '' and (bool)$slide_opt['show_icon_cbox'])
                    {
                        $icon = '<img src="'.$slide_opt['image_url'].'" alt="'.$wp_query->posts[$i]->post_title.'" />';
                        if($slide_opt['image_url_hover'] != '' and (bool)$slide_opt['show_icon_hover_cbox'])
                        {
                            $icon_hover = '<img class="icon-hover" src="'.$slide_opt['image_url_hover'].'" alt="'.$wp_query->posts[$i]->post_title.'" />';    
                        }
                    }
                    
                    if($icon != '')
                    {
                        $icon = '<div class="icon">'.$icon.$icon_hover.'</div>';
                    }
                    
                    if($i == ($wp_query->post_count - 1))
                    {
                        $out .= '<li class="button-last corner-right-bottom">'.$icon.$wp_query->posts[$i]->post_title.'<div class="clear-both"></div></li>';    
                    } else
                    {
                        $out .= '<li class="button'.($i == 0 ? ' corner-left-top' : '').'">'.$icon.$wp_query->posts[$i]->post_title.'<div class="clear-both"></div></li>';   
                    } 
                }
                $out .= '</ul>'; 
                
                $out .= '<div class="content">';                
                    $counter = 0;
                    while(have_posts())
                    {                                             
                        $counter++;                               
                        the_post(); 
                        $out .= '<div class="page">';
                            $out .= apply_filters('the_content', get_the_content());
                            $out .= '<div class="clear-both"></div>'; 
                        $out .= '</div>';                             
                    } 
                                            
                $out .= '</div>'; # content
                
                $out .= '<div class="clear-both"></div>';
                if($tour_opt['to_effect_mode'] == CMS_TOUR_EFFECT_FADE)
                {
                    $out .= '<span class="data effect">fade</span>';
                } else
                {
                    $out .= '<span class="data effect">slide</span>';     
                }
            $out .= '</div>'; # tour slider
            echo $out;                         
        } else
        {
            $out .= '<p class="theme-exception">There are no posts</p>'; 
        }         
                
        if('open' == $post->comment_status)
        {
            echo '<a name="comments"></a>';
            comments_template();
        }                           
        ?>
                           
        </div>  <!-- page-width-xx0 -->
        <div class="clear-both"></div>
    </div> <!-- content -->
    
<?php    
    get_footer();
?>



