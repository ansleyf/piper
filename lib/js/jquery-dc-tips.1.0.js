/*
 * Digital Cavalry Tips jQuery Plugin JS (version 1.0)
 * http://themeforest.net/user/DigitalCavalry
 * 
 * file: jquery-dc-tips.1.0.js
 * 
 * Copyright 2011, Digital Cavalry 
 * You can not use this plugin without author permission
 * Date: 21 April 2011
 */
 
 (function($) {
  $.fn.dcTipCreate = function(options) {   
      var opts = $.extend({}, $.fn.dcTipCreate.defaults, options);
      var ttypes = { simple:'simple'};
      var tpos = { rightbottom:'right-bottom', bottom:'bottom', leftbottom:'left-bottom', left:'left', lefttop:'left-top', top:'top', righttop:'right-top', right:'right'};
      
      var DC_TIME_ID = 'dc-time-id';
      var DC_TEXT = 'dc-text'; 
      
      return this.each(function() {
        
        switch(opts.type)
        {
            case ttypes.simple:
                $(this).hover(simpleTypeHoverIn, simpleTypeHoverOut); 
                $(this).mousemove(simpleTypeMouseMove);
            break;
        }               
    });  
    
    function simpleTypeHoverIn(e)
    {
        if($('#dc-tip-simple').length) { $('#dc-tip-simple').remove(); }        
        $('body').append('<div id="dc-tip-simple"></div>');
        
        var tipbox = $('#dc-tip-simple'); 
        tipbox.css('background-color', opts.bgcolor);
        tipbox.css('border-color', opts.bordercolor); 
        tipbox.css('color', opts.color);
        tipbox.css('border-width', opts.borderwidth);
        
        tipbox.css('padding-left', opts.paddingleft); 
        tipbox.css('padding-top', opts.paddingtop); 
        tipbox.css('padding-right', opts.paddingright); 
        tipbox.css('padding-bottom', opts.paddingbottom);         
        
        if(opts.use_width) { tipbox.css('max-width', opts.width);  }
        
        var loader = $(this);   
        
        var text = '';
        if(opts.text != null)
        {
            text = opts.text;        
        } else
        {
            switch(opts.textattr)
            {
                case 'title':
                text = loader.attr('title');
                loader.removeAttr('title');
                loader.data(DC_TEXT, text);    
                break;
            }
        }  
        tipbox.text(text);   
        
        simpleSetPosition(tipbox, e);                          
        
        var timeout_id = setTimeout(function(){ simpleTypeHoverInEnd(loader); }, opts.delay);
        loader.data(DC_TIME_ID, timeout_id);
    }

    function simpleTypeHoverInEnd(loader)
    {
        var tipbox = $('#dc-tip-simple');
        tipbox.css('opacity', 0.0).css('display', 'block').stop().animate({opacity:1.0}, opts.fade);  
    }    
    
    function simpleTypeMouseMove(e)
    {
        var tipbox = $('#dc-tip-simple');

        simpleSetPosition(tipbox, e);
    }  
    
    function simpleSetPosition(tipbox, e)
    {
        var scroll_pos = $(window).scrollTop(); 
        var width = tipbox.outerWidth();
        var height = tipbox.outerHeight(); 
        var start_pos_x = e.pageX;
        var start_pos_y = e.pageY;
        var window_w = $(window).width();
        var window_h = $(window).height();
        

        switch(opts.pos)
        {
            case tpos.left:
                var final_x =  start_pos_x - width - opts.offsetX;
                var final_y =  start_pos_y - Math.round(height / 2);
                
                if(opts.viewport)
                {
                    if(final_x < 0) { final_x = start_pos_x+opts.offsetX; }  // change to right
                    if(final_y < 0) { final_y = start_pos_y+opts.offsetY; }  // change to bottom
                }
                
                tipbox.css('left', final_x);
                tipbox.css('top', final_y);                 
            break;

            case tpos.lefttop:
                var final_x =  start_pos_x-width-opts.offsetX;
                var final_y =  start_pos_y-opts.offsetY;                  
                
                if(opts.viewport)
                {
                    if(final_x < 0) { final_x = start_pos_x+opts.offsetX; }  // change to right 
                    if(final_y - scroll_pos < 0) { final_y = start_pos_y+opts.offsetY; }  // change to bottom 
                }            
            
                tipbox.css('left', final_x);
                tipbox.css('top', final_y);                 
            break;              
            
            case tpos.righttop:
                var final_x =  start_pos_x + opts.offsetX;
                var final_y =  start_pos_y - opts.offsetY;                  
                
                if(opts.viewport)
                {
                    if(final_x + width > window_w ) { final_x = start_pos_x - width - opts.offsetX; } // change to left  
                    if(final_y - scroll_pos < 0) { final_y = start_pos_y + opts.offsetY; }  // change to bottom 
                }               
            
                tipbox.css('left', final_x);
                tipbox.css('top', final_y);                 
            break;              
            
            case tpos.right:
                var final_x =  start_pos_x + opts.offsetX;
                var final_y =  start_pos_y - Math.round(height / 2);
                
                if(opts.viewport)
                {
                    if(final_x + width > window_w ) { final_x = start_pos_x - width - opts.offsetX; } // change to left
                    if(final_y < 0) { final_y = start_pos_y + opts.offsetY; } // change to bottom
                }            
            
                tipbox.css('left', final_x);
                tipbox.css('top', final_y);                                                                 
            break;            
            
            case tpos.leftbottom:
                var final_x =  start_pos_x - width-opts.offsetX;
                var final_y =  start_pos_y + opts.offsetY;              
            
                if(opts.viewport)
                {
                    if(final_x < 0) { final_x = start_pos_x + opts.offsetX; }  // change to right
                    if(final_y - scroll_pos + height > window_h) { final_y = start_pos_y - opts.offsetY - height; } // change to top     
                }
            
                tipbox.css('left', final_x);
                tipbox.css('top', final_y);                 
            break;

            case tpos.rightbottom:
                var final_x =  start_pos_x + opts.offsetX;
                var final_y =  start_pos_y + opts.offsetY;  
            
                if(opts.viewport)
                {
                    if(final_x + width > window_w ) { final_x = start_pos_x - width - opts.offsetX; } // change to left
                    if(final_y - scroll_pos + height > window_h) { final_y = start_pos_y - opts.offsetY - height; } // change to top      
                }
            
                tipbox.css('left', final_x);
                tipbox.css('top', final_y);                 
            break;
            
            case tpos.bottom:
                var final_x =  start_pos_x - Math.round(width / 2);
                var final_y =  start_pos_y + opts.offsetY;                  
                if(opts.viewport)
                {
                    if(final_x < 0) { final_x = start_pos_x + opts.offsetX; } // change to right
                    else if(final_x + width > window_w ) { final_x = start_pos_x - width - opts.offsetX; } // change to left
                     
                    if(final_y - scroll_pos + height > window_h) { final_y = start_pos_y - opts.offsetY - height; } // change to top
                }               
            
                tipbox.css('left', final_x);
                tipbox.css('top', final_y);                 
            break; 
            
            case tpos.top:
                var final_x =  start_pos_x - Math.round(width / 2);
                var final_y =  start_pos_y - opts.offsetY - height;            
            
                if(opts.viewport)
                {
                    if(final_x < 0) { final_x = start_pos_x + opts.offsetX; } // change to right
                    else if(final_x + width > window_w ) { final_x = start_pos_x - width - opts.offsetX; } // change to left
                     
                    if(final_y - scroll_pos < 0) { final_y = start_pos_y + opts.offsetY; } // change to bottom
                }                
            
                tipbox.css('left', final_x);
                tipbox.css('top', final_y);                 
            break;                        
        }          
    }    
    
    function simpleTypeHoverOut()
    {
        if(isset($(this).data(DC_TIME_ID)))
        {
            clearTimeout($(this).data(DC_TIME_ID));
            $(this).removeData(DC_TIME_ID);
        }
        if($('#dc-tip-simple').length) { $('#dc-tip-simple').stop().remove(); } 
        
        if(opts.text == null)
        {
            switch(opts.textattr)
            {
                case 'title':
                text = $(this).data(DC_TEXT);
                $(this).attr('title', text);  
                break;
            }
        }            
    }
    
    // utilities
    function isset( variable )
    {
        return( typeof( variable ) != 'undefined' );
    }    
    
  } 
  
  $.fn.dcTipCreate.defaults = {
    type:'simple', 
    pos:'bottom',
    bgcolor:'#FAFAFA',
    bordercolor:'#DDDDDD',
    borderwidth:1,
    color:'#666666', 
    paddingleft:8,
    paddingright:8,
    paddingtop:5,
    paddingbottom:5,
    width:300,
    align:'center',
    use_width:false,
    offsetX:15,
    offsetY:15,
    text:null,
    textattr:'title',
    delay:300,
    fade:200,
    viewport:true
  }
})(jQuery)
