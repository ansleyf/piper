<?php
/**********************************************************************
* MODERN ELEGANCE WORDPRESS EDITION  
* (Ideal For Business And Personal Use: Portfolio or Blog)     
* 
* File name:   
*      sendmessage.php
* Brief:       
*      Implementation on email sending functionality.
* Author:      
*      DigitalCavalry
* Author URI:
*      http://themeforest.net/user/DigitalCavalry
* Contact:
*      digitalcavalry@gmail.com 
***********************************************************************/

    // collect data from post table in local variables
    $name = $_POST["name"];
    $email = $_POST["mail"];
    $subject = $_POST["subject"];
    $message = $_POST["message"];
    $maildest = $_POST["maildest"];
    $scode = (string)$_POST["scode"]; 
    $scodeuser = (string)$_POST["scodeuser"];
 
    // prepare parameters for email function
    
    $code_ok = true;
    if(isset($_POST["scode"]))
    {
        if(md5(md5($scodeuser)) != $scode)
        {
            $code_ok = false;
        }    
    }
    
    $ret = false;
    if($code_ok)
    {    
        // header that describe email
        $headers = "From: $name" . " <$email>" . "\r\n" .
                   "Reply-To: " . "$email" . "\r\n" .
                   "Content-type: text/html; charset=iso-8859-1" . "\r\n" .
                   'X-Mailer: PHP/' . phpversion();
                   
        $message .= "<br /><br /><br /><strong>Additional information:</strong><br />";
        $message .= "Name: ".$name."<br />"; 
        $message .= "From: ".$email."<br />"; 
        $message .= "IP Address: ".$_SERVER['REMOTE_ADDR']."<br />";            
        $message .= "Time: ".date("F j, Y, H:i:s")."<br />"; 
                                               
        // send email           
        $ret = mail($maildest, $subject, $message, $headers);    
    }
    
    // check mail return value, true - email was accepted to send, other false
    if($ret)
    {
        // if true return text "okay"
        echo "okay";
    } else
    {
        // if false return text "error"
        echo "error maildesc:$maildest--, name:$name--, email:$email--, subject:$subject--, message:$message--, scode:$scode--, scodeuser:$scodeuser--";
    } 
    
?>