<?php
/**********************************************************************
* MODERN ELEGANCE WORDPRESS EDITION 
* (Ideal For Business And Personal Use: Portfolio or Blog)     
* 
* File name:   
*      authorization.php
* Brief:       
*      Implementation authorization code functionality.
* Author:      
*      DigitalCavalry
* Author URI:
*      http://themeforest.net/user/DigitalCavalry
* Contact:
*      digitalcavalry@gmail.com 
***********************************************************************/

//Set the correct content type 
header('content-type: image/png'); 

$code = (string)$_GET['code'];

$IMG_HEIGHT = 40;
$IMG_WIDTH = 90;

$image = null;
$image = imagecreatetruecolor($IMG_WIDTH, $IMG_HEIGHT);
$c_text = imagecolorallocate($image, 0x33, 0x99, 0xCC); 
$c_bg = imagecolorallocate($image, 0xCC, 0xC0, 0xA5); 
$c_bg = imagecolorallocate($image, 0xFF, 0xFF, 0xFF); 
$c_frame = imagecolorallocate($image, 0xAA, 0xAA, 0xAA);

$c_text = imagecolorallocate($image, 0x22, 0x22, 0x22);

imagefilledrectangle($image, 0, 0, $IMG_WIDTH-1, $IMG_HEIGHT-1, $c_frame);
imagefilledrectangle($image, 1, 1, $IMG_WIDTH-2, $IMG_HEIGHT-2, $c_bg);
imagettftext($image, 15, 0, 10, 28, $c_text, 'gongn.ttf', $code);
imagepng($image);

imagedestroy($image);      

?>