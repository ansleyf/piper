<?php
/*
Template Name: Media Gallery
*/

/**********************************************************************
* MODERN ELEGANCE WORDPRESS THEME  
* (Ideal For Business And Personal Use: Portfolio or Blog)   
* 
* File name:   
*      media-gallery.php
* Brief:       
*      Theme contact page template code
* Author:      
*      DigitalCavalry
* Author URI:
*      http://themeforest.net/user/DigitalCavalry
* Contact:
*      digitalcavalry@gmail.com   
***********************************************************************/ 

    get_header();                                                                                                   
?>

    <div id="content">
         
        <?php 
            $mg_opt = get_post_meta($post->ID, 'media_opt', 'true'); 
            
            if($mg_opt['mg_layout'] == CMS_MEDIAGALL_LAYOUT_SIDEBAR)
            {
                $page_common_opt = $GLOBALS['dc_pagecommon_opt'];            
                GetDCCPInterface()->getIGeneral()->includeSidebar($page_common_opt['page_sid'], $page_common_opt['page_sid_pos']);
                
                if(GetDCCPInterface()->getIGeneral()->getSidebarGlobalPos($page_common_opt['page_sid_pos']) == CMS_SIDEBAR_RIGHT)
                {
                    echo '<div class="page-width-left">';                              
                } else
                {
                    echo '<div class="page-width-right">';     
                }                
            } else
            {
                echo '<div class="page-width-full">';   
            } 
               
            dcf_naviTree($post->ID, 0); 
          
            echo '<h1>'.$post->post_title.'</h1>';            
            the_content();                          
           
            
            $paged = (get_query_var('paged')) ? get_query_var('paged') : 1; 
            $per_page = ((int)$mg_opt['mg_per_page'] < 1) ? 1 : $mg_opt['mg_per_page'];
            $max_page = 0;
            
            global $wpdb;
            global $wordTube;
            
            $out = '';
            $out .= '<div class="media-gallery">';
            if(isset($wordTube))
            {
                if((bool)$mg_opt['mg_use_list_cbox'] and $mg_opt['mg_list'] != CMS_NOT_SELECTED)
                {
                    $querystr = "
                        SELECT SQL_CALC_FOUND_ROWS *  
                        FROM $wpdb->wordtube 
                        LEFT JOIN $wpdb->wordtube_med2play ON($wpdb->wordtube.vid = $wpdb->wordtube_med2play.media_id)                  
                        WHERE $wpdb->wordtube_med2play.playlist_id = ".$mg_opt['mg_list']. "                     
                        ORDER BY $wpdb->wordtube_med2play.porder LIMIT ".(($paged-1)*$per_page).", $per_page"; 
                        
                        $data = $wpdb->get_results($querystr, OBJECT);                      
                        $total = intval( $wpdb->get_var( "SELECT FOUND_ROWS()" ) );            
                        $max_page = (int)ceil($total / $per_page);                    
                    
                    if(is_array($data))
                    {
                            if($mg_opt['mg_layout'] == CMS_MEDIAGALL_LAYOUT_TABLE2)
                            {
                                $count = count($data);
                                // layout 2 columns
                                $out .= '<div class="two-columns">';

                                    $counter = 0;
                                    for($i = 0; $i < $count; $i++)
                                    {
                                        $class = '';
                                        if((($counter+1) % 2) != 0)
                                        {
                                            $class = ' item-margin';
                                        }
                                        
                                        $out .= '<div class="item'.$class.'">';
                                            
                                            $out .= do_shortcode('[media id='.$data[$i]->vid.' width=455 height=300]');

                                            if((bool)$mg_opt['mg_show_author_cbox'] and $data[$i]->creator != '')
                                            {
                                                $out .= '<div class="author">'.$data[$i]->creator.'</div>';
                                            } else
                                            {
                                                $out .= '<div class="author-empty"></div>'; 
                                            }

                                            if((bool)$mg_opt['mg_show_title_cbox'])
                                            {         
                                                if((bool)$mg_opt['mg_show_link_cbox'] and trim($data[$i]->link) != '')
                                                {
                                                    $out .= '<h4 class="title"><a href="'.$data[$i]->link.'">'.stripcslashes($data[$i]->name).'</a></h4>';
                                                } else
                                                {
                                                    $out .= '<h4 class="title">'.stripcslashes($data[$i]->name).'</h4>';    
                                                }                                                                                                                   
                                            }
                                            
                                            if((bool)$mg_opt['mg_show_desc_cbox'])
                                            {
                                                $out .= '<div class="desc">'.stripcslashes($data[$i]->description);
                                                if((bool)$mg_opt['mg_show_link_cbox'] and trim($data[$i]->link) != '' and !(bool)$mg_opt['mg_show_link_title_cbox'])
                                                {
                                                    $out .= ' <a href="'.$data[$i]->link.'">'.__('View&nbsp;more', 'dc_theme').'</a>';
                                                }
                                                $out .= '</div>';
                                            }                                        
                                            
                                        $out .= '</div>';    
                                        $counter++; 
                                        
                                        if(($counter % 2) == 0)
                                        {
                                            $out .= '<div class="clear-both"></div>'; 
                                            if($counter != $per_page and ($i+1) < $count)
                                            {
                                                $out .= '<div class="separator"></div>';
                                            }   
                                        }   
                                    }
                                $out .= '<div class="clear-both"></div>';
                                if($max_page == 1)
                                {
                                    $out .= '<div class="separator"></div>';
                                }
                                $out .= '</div>';  // end 2 columns
                            } else                            
                            if($mg_opt['mg_layout'] == CMS_MEDIAGALL_LAYOUT_TABLE3)
                            {
                                $count = count($data);
                                // layout 3 columns
                                $out .= '<div class="three-columns">';

                                    $counter = 0;
                                    for($i = 0; $i < $count; $i++)
                                    {
                                        $class = '';
                                        if((($counter+1) % 3) != 0)
                                        {
                                            $class = ' item-margin';
                                        }
                                        
                                        $out .= '<div class="item'.$class.'">';
                                            
                                            $out .= do_shortcode('[media id='.$data[$i]->vid.' width=300 height=200]');

                                            if((bool)$mg_opt['mg_show_author_cbox'] and $data[$i]->creator != '')
                                            {
                                                $out .= '<div class="author">'.$data[$i]->creator.'</div>';
                                            } else
                                            {
                                                $out .= '<div class="author-empty"></div>'; 
                                            }

                                            if((bool)$mg_opt['mg_show_title_cbox'])
                                            {         
                                                if((bool)$mg_opt['mg_show_link_cbox'] and trim($data[$i]->link) != '')
                                                {
                                                    $out .= '<h4 class="title"><a href="'.$data[$i]->link.'">'.stripcslashes($data[$i]->name).'</a></h4>';
                                                } else
                                                {
                                                    $out .= '<h4 class="title">'.stripcslashes($data[$i]->name).'</h4>';    
                                                }                                                                                                                   
                                            }
                                            
                                            if((bool)$mg_opt['mg_show_desc_cbox'])
                                            {
                                                $out .= '<div class="desc">'.stripcslashes($data[$i]->description);
                                                if((bool)$mg_opt['mg_show_link_cbox'] and trim($data[$i]->link) != '' and !(bool)$mg_opt['mg_show_link_title_cbox'])
                                                {
                                                    $out .= ' <a href="'.$data[$i]->link.'">'.__('View&nbsp;more', 'dc_theme').'</a>';
                                                }
                                                $out .= '</div>';
                                            }                                        
                                            
                                        $out .= '</div>';    
                                        $counter++; 
                                        
                                        if(($counter % 3) == 0)
                                        {
                                            $out .= '<div class="clear-both"></div>'; 
                                            if($counter != $per_page  and ($i+1) < $count)
                                            {
                                                $out .= '<div class="separator"></div>';
                                            }   
                                        }   
                                    }
                                
                                $out .= '<div class="clear-both"></div>';
                                if($max_page == 1)
                                {
                                    $out .= '<div class="separator"></div>';
                                }
                                $out .= '</div>';  // end 3 columns
                            } else
                            if($mg_opt['mg_layout'] == CMS_MEDIAGALL_LAYOUT_TABLE4) 
                            {
                                $count = count($data); 
                                // layout 4 columns
                                $out .= '<div class="four-columns">';

                                    $counter = 0;
                                    for($i = 0; $i < $count; $i++)
                                    {
                                        $class = '';
                                        if((($counter+1) % 4) != 0)
                                        {
                                            $class = ' item-margin';
                                        }
                                        
                                        $out .= '<div class="item'.$class.'">';
                                            
                                            $out .= do_shortcode('[media id='.$data[$i]->vid.' width=222 height=165]');

                                            if((bool)$mg_opt['mg_show_author_cbox'] and $data[$i]->creator != '')
                                            {
                                                $out .= '<div class="author">'.$data[$i]->creator.'</div>';
                                            } else
                                            {
                                                $out .= '<div class="author-empty"></div>'; 
                                            }

                                            if((bool)$mg_opt['mg_show_title_cbox'])
                                            {         
                                                if((bool)$mg_opt['mg_show_link_cbox'] and trim($data[$i]->link) != '')
                                                {
                                                    $out .= '<h4 class="title"><a href="'.$data[$i]->link.'">'.stripcslashes($data[$i]->name).'</a></h4>';
                                                } else
                                                {
                                                    $out .= '<h4 class="title">'.stripcslashes($data[$i]->name).'</h4>';    
                                                }                                                                                                                   
                                            }
                                            
                                            if((bool)$mg_opt['mg_show_desc_cbox'])
                                            {
                                                $out .= '<div class="desc">'.stripcslashes($data[$i]->description);
                                                if((bool)$mg_opt['mg_show_link_cbox'] and trim($data[$i]->link) != '' and !(bool)$mg_opt['mg_show_link_title_cbox'])
                                                {
                                                    $out .= ' <a href="'.$data[$i]->link.'">'.__('View&nbsp;more', 'dc_theme').'</a>';
                                                }
                                                $out .= '</div>';
                                            }                                        
                                            
                                        $out .= '</div>';    
                                        $counter++; 
                                        
                                        if(($counter % 4) == 0)
                                        {
                                            $out .= '<div class="clear-both"></div>'; 
                                            if($counter != $per_page  and ($i+1) < $count)
                                            {
                                                $out .= '<div class="separator"></div>';
                                            }   
                                        }   
                                    }
                                $out .= '<div class="clear-both"></div>';
                                if($max_page == 1)
                                {
                                    $out .= '<div class="separator"></div>';
                                }
                                $out .= '</div>';  // end 4 columns                                
                            } else
                            if($mg_opt['mg_layout'] == CMS_MEDIAGALL_LAYOUT_SIDEBAR) 
                            {
                                $count = count($data);
                                // layout 1 columns
                                $out .= '<div class="one-columns">';

                                    $counter = 0;
                                    for($i = 0; $i < $count; $i++)
                                    {                                      
                                        $out .= '<div class="item">';
                                            
                                            $out .= do_shortcode('[media id='.$data[$i]->vid.' width=360 height=270]');

                                            $out .= '<div class="content">';
                                                if((bool)$mg_opt['mg_show_author_cbox'] and $data[$i]->creator != '')
                                                {
                                                    $out .= '<div class="author">'.$data[$i]->creator.'</div>';
                                                }

                                                if((bool)$mg_opt['mg_show_title_cbox'])
                                                {         
                                                    if((bool)$mg_opt['mg_show_link_cbox'] and trim($sorted[$i]->link) != '')
                                                    {
                                                        $out .= '<h4 class="title"><a href="'.$data[$i]->link.'">'.stripcslashes($data[$i]->name).'</a></h4>';
                                                    } else
                                                    {
                                                        $out .= '<h4 class="title">'.stripcslashes($data[$i]->name).'</h4>';    
                                                    }                                                                                                                   
                                                }
                                                
                                                if((bool)$mg_opt['mg_show_desc_cbox'])
                                                {
                                                    $out .= '<div class="desc">'.stripcslashes($data[$i]->description);
                                                    if((bool)$mg_opt['mg_show_link_cbox'] and trim($data[$i]->link) != '' and !(bool)$mg_opt['mg_show_link_title_cbox'])
                                                    {
                                                        $out .= ' <a href="'.$data[$i]->link.'">'.__('View&nbsp;more', 'dc_theme').'</a>';
                                                    }
                                                    $out .= '</div>';
                                                }
                                            $out .= '</div>';                                        
                                            
                                        $out .= '</div>';    
                                        $counter++; 
                                        
                                        $out .= '<div class="clear-both"></div>'; 
                                        if($counter != $per_page and ($i+1) < $count)
                                        {
                                            $out .= '<div class="separator"></div>';
                                        }   
                                    }
                                $out .= '<div class="clear-both"></div>';
                                if($max_page == 1)
                                {
                                    $out .= '<div class="separator"></div>';
                                }
                                $out .= '</div>';  // end 1 columns                                
                            }                            
                    } else
                    {
                        $out .= '<p class="theme-exception">Selected playlist is empty.</p>';  
                    }                       
                } else
                if((bool)$mg_opt['mg_use_ids_cbox'])
                {
                    $dbresult_media = $wpdb->get_results('SELECT * FROM '.$wpdb->wordtube.' WHERE vid IN ('.$mg_opt['mg_ids'].')');
                    $count = 0;
                    if(is_array($dbresult_media))
                    {
                        $count = count($dbresult_media);
                    }
                    
                    if($count > 0)
                    {
                        $max_page = (int)ceil($count / $per_page);  
                        
                        $start = ($paged - 1) * $per_page;
                        $end = $start+$per_page;
                        if($end > $count) { $end = $count; }
                        $sorted = & $dbresult_media;
                        
                        if($mg_opt['mg_layout'] == CMS_MEDIAGALL_LAYOUT_TABLE2)
                        {
                            // layout 2 columns
                            $out .= '<div class="two-columns">';
                            if($start < $count)
                            {
                                $counter = 0;
                                for($i = $start; $i < $end; $i++)
                                {
                                    $class = '';
                                    if((($counter+1) % 2) != 0)
                                    {
                                        $class = ' item-margin';
                                    }
                                    
                                    $out .= '<div class="item'.$class.'">';
                                        
                                        $out .= do_shortcode('[media id='.$sorted[$i]->vid.' width=455 height=300]');

                                        if((bool)$mg_opt['mg_show_author_cbox'] and $sorted[$i]->creator != '')
                                        {
                                            $out .= '<div class="author">'.$sorted[$i]->creator.'</div>';
                                        } else
                                        {
                                            $out .= '<div class="author-empty"></div>'; 
                                        }

                                        if((bool)$mg_opt['mg_show_title_cbox'])
                                        {         
                                            if((bool)$mg_opt['mg_show_link_cbox'] and trim($sorted[$i]->link) != '')
                                            {
                                                $out .= '<h4 class="title"><a href="'.$sorted[$i]->link.'">'.stripcslashes($sorted[$i]->name).'</a></h4>';
                                            } else
                                            {
                                                $out .= '<h4 class="title">'.stripcslashes($sorted[$i]->name).'</h4>';    
                                            }                                                                                                                   
                                        }
                                        
                                        if((bool)$mg_opt['mg_show_desc_cbox'])
                                        {
                                            $out .= '<div class="desc">'.stripcslashes($sorted[$i]->description);
                                            if((bool)$mg_opt['mg_show_link_cbox'] and trim($sorted[$i]->link) != '' and !(bool)$mg_opt['mg_show_link_title_cbox'])
                                            {
                                                $out .= ' <a href="'.$sorted[$i]->link.'">'.__('View&nbsp;more', 'dc_theme').'</a>';
                                            }
                                            $out .= '</div>';
                                        }                                        
                                        
                                    $out .= '</div>';    
                                    $counter++; 
                                    
                                    if(($counter % 2) == 0)
                                    {
                                        $out .= '<div class="clear-both"></div>'; 
                                        if($counter != $per_page and ($i+1) < $end)
                                        {
                                            $out .= '<div class="separator"></div>';
                                        }   
                                    }   
                                }
                            }
                            $out .= '<div class="clear-both"></div>';
                            if($max_page == 1)
                            {
                                $out .= '<div class="separator"></div>';
                            }
                            $out .= '</div>';  // end 2 columns
                        } else                            
                        if($mg_opt['mg_layout'] == CMS_MEDIAGALL_LAYOUT_TABLE3)
                        {
                            // layout 3 columns
                            $out .= '<div class="three-columns">';
                            if($start < $count)
                            {
                                $counter = 0;
                                for($i = $start; $i < $end; $i++)
                                {
                                    $class = '';
                                    if((($counter+1) % 3) != 0)
                                    {
                                        $class = ' item-margin';
                                    }
                                    
                                    $out .= '<div class="item'.$class.'">';
                                        
                                        $out .= do_shortcode('[media id='.$sorted[$i]->vid.' width=300 height=200]');

                                        if((bool)$mg_opt['mg_show_author_cbox'] and $sorted[$i]->creator != '')
                                        {
                                            $out .= '<div class="author">'.$sorted[$i]->creator.'</div>';
                                        } else
                                        {
                                            $out .= '<div class="author-empty"></div>'; 
                                        }

                                        if((bool)$mg_opt['mg_show_title_cbox'])
                                        {         
                                            if((bool)$mg_opt['mg_show_link_cbox'] and trim($sorted[$i]->link) != '')
                                            {
                                                $out .= '<h4 class="title"><a href="'.$sorted[$i]->link.'">'.stripcslashes($sorted[$i]->name).'</a></h4>';
                                            } else
                                            {
                                                $out .= '<h4 class="title">'.stripcslashes($sorted[$i]->name).'</h4>';    
                                            }                                                                                                                   
                                        }
                                        
                                        if((bool)$mg_opt['mg_show_desc_cbox'])
                                        {
                                            $out .= '<div class="desc">'.stripcslashes($sorted[$i]->description);
                                            if((bool)$mg_opt['mg_show_link_cbox'] and trim($sorted[$i]->link) != '' and !(bool)$mg_opt['mg_show_link_title_cbox'])
                                            {
                                                $out .= ' <a href="'.$sorted[$i]->link.'">'.__('View&nbsp;more', 'dc_theme').'</a>';
                                            }
                                            $out .= '</div>';
                                        }                                        
                                        
                                    $out .= '</div>';    
                                    $counter++; 
                                    
                                    if(($counter % 3) == 0)
                                    {
                                        $out .= '<div class="clear-both"></div>'; 
                                        if($counter != $per_page and ($i+1) < $end)
                                        {
                                            $out .= '<div class="separator"></div>';
                                        }   
                                    }   
                                }
                            }
                            $out .= '<div class="clear-both"></div>';
                            if($max_page == 1)
                            {
                                $out .= '<div class="separator"></div>';
                            }
                            $out .= '</div>';  // end 3 columns
                        } else
                        if($mg_opt['mg_layout'] == CMS_MEDIAGALL_LAYOUT_TABLE4) 
                        {
                            // layout 5 columns
                            $out .= '<div class="four-columns">';
                            if($start < $count)
                            {
                                $counter = 0;
                                for($i = $start; $i < $end; $i++)
                                {
                                    $class = '';
                                    if((($counter+1) % 4) != 0)
                                    {
                                        $class = ' item-margin';
                                    }
                                    
                                    $out .= '<div class="item'.$class.'">';
                                        
                                        $out .= do_shortcode('[media id='.$sorted[$i]->vid.' width=222 height=165]');

                                        if((bool)$mg_opt['mg_show_author_cbox'] and $sorted[$i]->creator != '')
                                        {
                                            $out .= '<div class="author">'.$sorted[$i]->creator.'</div>';
                                        } else
                                        {
                                            $out .= '<div class="author-empty"></div>'; 
                                        }

                                        if((bool)$mg_opt['mg_show_title_cbox'])
                                        {         
                                            if((bool)$mg_opt['mg_show_link_cbox'] and trim($sorted[$i]->link) != '')
                                            {
                                                $out .= '<h4 class="title"><a href="'.$sorted[$i]->link.'">'.stripcslashes($sorted[$i]->name).'</a></h4>';
                                            } else
                                            {
                                                $out .= '<h4 class="title">'.stripcslashes($sorted[$i]->name).'</h4>';    
                                            }                                                                                                                   
                                        }
                                        
                                        if((bool)$mg_opt['mg_show_desc_cbox'])
                                        {
                                            $out .= '<div class="desc">'.stripcslashes($sorted[$i]->description);
                                            if((bool)$mg_opt['mg_show_link_cbox'] and trim($sorted[$i]->link) != '' and !(bool)$mg_opt['mg_show_link_title_cbox'])
                                            {
                                                $out .= ' <a href="'.$sorted[$i]->link.'">'.__('View&nbsp;more', 'dc_theme').'</a>';
                                            }
                                            $out .= '</div>';
                                        }                                        
                                        
                                    $out .= '</div>';    
                                    $counter++; 
                                    
                                    if(($counter % 4) == 0)
                                    {
                                        $out .= '<div class="clear-both"></div>'; 
                                        if($counter != $per_page  and ($i+1) < $end)
                                        {
                                            $out .= '<div class="separator"></div>';
                                        }   
                                    }   
                                }
                            }
                            $out .= '<div class="clear-both"></div>';
                            if($max_page == 1)
                            {
                                $out .= '<div class="separator"></div>';
                            }
                            $out .= '</div>';  // end 5 columns                                
                        } else
                        if($mg_opt['mg_layout'] == CMS_MEDIAGALL_LAYOUT_SIDEBAR) 
                        {
                            // layout 1 columns
                            $out .= '<div class="one-columns">';
                            if($start < $count)
                            {
                                $counter = 0;
                                for($i = $start; $i < $end; $i++)
                                {                                      
                                    $out .= '<div class="item">';
                                        
                                        $out .= do_shortcode('[media id='.$sorted[$i]->vid.' width=360 height=270]');

                                        $out .= '<div class="content">';
                                            if((bool)$mg_opt['mg_show_author_cbox'] and $sorted[$i]->creator != '')
                                            {
                                                $out .= '<div class="author">'.$sorted[$i]->creator.'</div>';
                                            }

                                            if((bool)$mg_opt['mg_show_title_cbox'])
                                            {         
                                                if((bool)$mg_opt['mg_show_link_cbox'] and trim($sorted[$i]->link) != '')
                                                {
                                                    $out .= '<h4 class="title"><a href="'.$sorted[$i]->link.'">'.stripcslashes($sorted[$i]->name).'</a></h4>';
                                                } else
                                                {
                                                    $out .= '<h4 class="title">'.stripcslashes($sorted[$i]->name).'</h4>';    
                                                }                                                                                                                   
                                            }
                                            
                                            if((bool)$mg_opt['mg_show_desc_cbox'])
                                            {
                                                $out .= '<div class="desc">'.stripcslashes($sorted[$i]->description);
                                                if((bool)$mg_opt['mg_show_link_cbox'] and trim($sorted[$i]->link) != '' and !(bool)$mg_opt['mg_show_link_title_cbox'])
                                                {
                                                    $out .= ' <a href="'.$sorted[$i]->link.'">'.__('View&nbsp;more', 'dc_theme').'</a>';
                                                }
                                                $out .= '</div>';
                                            }
                                        $out .= '</div>';                                        
                                        
                                    $out .= '</div>';    
                                    $counter++; 
                                    
                                    $out .= '<div class="clear-both"></div>'; 
                                    if($counter != $per_page and ($i+1) < $end)
                                    {
                                        $out .= '<div class="separator"></div>';
                                    }   
                                }
                            }
                            $out .= '<div class="clear-both"></div>';
                            if($max_page == 1)
                            {
                                $out .= '<div class="separator"></div>';
                            }
                            $out .= '</div>';  // end 1 columns                                
                        }  
                    } else
                    {
                        $out .= '<p class="theme-exception">Cannot find specified media.</p>';                        
                    }                   
                }
            } else
            {
                $out .= '<p class="theme-exception">WordTube plugin is not installed.</p>';  
            }
            $out .= '</div>';
            echo $out;
            
            GetDCCPInterface()->getIGeneral()->renderSitePagination($paged, $max_page);  
           
           ?>
                     
        </div>  <!-- page-width-x -->
        <div class="clear-both"></div>
    </div> <!-- content -->
    
<?php    
    get_footer();
?>



