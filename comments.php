<?php
/**********************************************************************
* MODERN ELEGANCE WORDPRESS THEME 
* (Ideal For Business And Personal Use: Portfolio or Blog)   
* 
* File name:   
*      comments.php
* Brief:       
*      Theme comments page code
* Author:      
*      DigitalCavalry
* Author URI:
*      http://themeforest.net/user/DigitalCavalry
* Contact:
*      digitalcavalry@gmail.com   
***********************************************************************/ 
?>
 
<?php if(!empty($_SERVER['SCRIPT_FILENAME']) && 'comments.php' == basename($_SERVER['SCRIPT_FILENAME'])) : ?>      
    <?php die('You can not access this page directly!'); ?>  
<?php endif; ?>

<?php if(!empty($post->post_password)) : ?>
      <?php if($_COOKIE['wp-postpass_' . COOKIEHASH] != $post->post_password) : ?>
        <p>This post is password protected. Enter the password to view comments.</p>
      <?php endif; ?>
<?php endif; ?> 
 
 <?php

function mytheme_comment($comment, $args, $depth) 
{      
   global $post;
   $GLOBALS['comment'] = $comment; ?>
   <li <?php comment_class(); ?> id="li-comment-<?php comment_ID() ?>">     
         <div id="comment-<?php comment_ID(); ?>">
         
     <div class="comment">
         
            <?php 
                $out = '';
                
                $show_avatars = get_option('show_avatars') == 1 ? true : false;
                if($show_avatars)
                {
                    $out .= '<div class="gravatar">'; 
                    
                    $default_avatar = get_bloginfo('template_url').'/img/common/avatar1.jpg';
                    $website = get_comment_author_url($comment->comment_ID);
                    
                    $out .= get_avatar($comment, '60', $default_avatar);
                    if($website != '')
                    {
                        $out .= '<a class="author-url" target="_blank" href="'.$website.'">'.__('Visit site', 'dc_theme').'</a>';
                    } else
                    {
                        $out .= '<span class="author-url">'.__('Visit site', 'dc_theme').'</span>';
                    }
                    $out .= '</div>';
                    echo $out;
                }
            
                echo '<div class="content '.(!$show_avatars ? 'content-no-margin' : '').'">';
             
                
                $out = '';
                $out .= '<div class="date"><span class="when">'.dcf_calculatePastTime(__('Posted', 'dc_theme'), get_comment_time('H'), get_comment_time('i'), 
                    get_comment_time('s'), get_comment_date('n'), get_comment_date('j'), get_comment_date('Y')).'</span></div>';
                echo $out;
                      

                $out = ''; 
                
                echo $out; 

            ?> 
                       
            <div class="text"> 
                 <?php comment_text() ?> 
            </div>
            <div class="clear-left"></div>
          <?php if ($comment->comment_approved == '0') 
          { 
             echo '<div class="to-approve" ><em>'.__('Your comment is awaiting moderation', CMS_TXT_DOMAIN).'</em></div>';
          } ?>
            <div class="clear-left"></div>
            
            
            <?php
            
             echo '<div class="comment-info">';
             
             echo '<span>'.__('by', CMS_TXT_DOMAIN).'</span>';
             
             $out = ' '; 
                $out .= '<span class="author">'.get_comment_author($comment->comment_ID);
                
                if($post->post_author == $comment->user_id)
                {   
                    if($post->post_author == $comment->user_id)
                    {
                        $out .= ' <span class="marked">('.__('Author', CMS_TXT_DOMAIN).')</span>';   
                    }                 
                }
                $out .= '</span>';
                echo $out;
             
             
             echo '<span>'.__(' on ', 'dc_theme').'</span>';
             
            
            $format = 'F j, Y g:i a';
                $out = '';
                $out .= '<span class="date">';
                $out .= get_comment_time($format);             
                $out .= '</span>';
                echo $out;
            
            ?>
            
            <span class="reply"> | <?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))); ?></span>                                                 
                
            </div>
            
            
            
                           
            
 
            </div> <!-- content -->
            <div class="clear-both"></div>
     </div> <!-- comment -->           
          
         </div> <!-- comment-ID -->
<?php
}
?>

<div id="comments-section">

    <?php
    echo '<div class="section-title-one"><span class="title-name">'.__('Comments', 'dc_theme').'</span></div>';
    ?>

    <?php if($post->comment_count > 0) { ?>
    <h4><?php 
        $pi_general = GetDCCPInterface()->getIGeneral();
        comments_number(__('No comments', 'dc_theme'), __('One comment', 'dc_theme'), __('% comments', 'dc_theme')); 
        
        ?></h4><?php } 
                        
        ?>         
<ul class="comment-list">
<?php wp_list_comments('type=comment&callback=mytheme_comment'); ?>
</ul>
   </div> <!-- comments-section --> 
    <div class="clear-both"></div>
   <?php 

      if(is_singular() and get_option('page_comments'))
      {

          global $wp_rewrite; 
          $comment_page = get_query_var('cpage');
          if(!$comment_page)
          {
              $comment_page = 1;
          }
          
          $comment_max_page = get_comment_pages_count();
          
          if($comment_max_page > 1)
          {
              $comment_defaults = array(
                  'base' => add_query_arg('cpage', '%#%'),
                  'format' => '',
                  'total' => $comment_max_page,
                  'current' => $comment_page,
                  'echo' => true,
                  'prev_next' => true,
                  'prev_text' => '&laquo; Prev',
                  'next_text' => 'Next &raquo;',
                  'add_fragment' => '#comments',
                  'end_size' => 3,
                  'mid_size' => 2
              );
              if ( $wp_rewrite->using_permalinks() )
                  $comment_defaults['base'] = user_trailingslashit(trailingslashit(get_permalink()) . 'comment-page-%#%', 'commentpaged');
          
              $comment_args = wp_parse_args($comment_args, $comment_defaults);
              $comment_page_links = paginate_links($comment_args);   
           
              $out = '';
              $out .= '<div class="comments-page-links">';
                $out .= '<span class="before">Pages: </span>';
                $out .= $comment_page_links;
                $out .= '<div class="clear-both"></div>';
              $out .= '</div>';
              echo $out;
          }
      }
   ?>

   
<?php
    global $post;
    global $user_ID;
    
    if('open' == $post->comment_status)
    {                                
        if(get_option('comment_registration') && !$user_ID)
        {
            $out = '';
            $out .= '<span style="font-size:11px;color:#888888;">'.__('You must be', 'dc_theme').'<span> ';
            $out .= '<a style="font-size:11px;" href="'.get_bloginfo('url').'/wp-login.php?redirect_to='.urlencode(get_permalink()).'">'.__('logged in', 'dc_theme').'</a> '; 
            $out .= '<span style="font-size:11px;color:#888888;">'.__('to post comment.', 'dc_theme').'</span>';
            echo $out;             
        } else
        {
            $out  = '<div id="respond">';
            $out .= '<div class="common-form">';
            $out .= '<h4>'.__('Leave your comment', 'dc_theme').'</h4>';
            $out .= '<form action="'.get_option('siteurl').'/wp-comments-post.php" method="post" id="commentform" name="commentform">';
            
            if(!$user_ID) // ma byc !$user_ID
            {
                $rne = (bool)get_option('require_name_email'); 
                
                $out .= '<p>'.__('Your Name', CMS_TXT_DOMAIN).': <span class="required">('.($rne ? __('required', CMS_TXT_DOMAIN) : 'not required').')</span></p>';
                $out .= '<input class="text-ctrl" type="text" value="'.$comment_author.'" name="author" id="author" />';
                
                $out .= '<p>'.__('E-Mail', CMS_TXT_DOMAIN).': <span class="required">('.($rne ? __('required', CMS_TXT_DOMAIN) : 'not required').')</span></p>';
                $out .= '<input class="text-ctrl" type="text" value="'.$comment_author_email.'" name="email" id="email" />';
                
                $out .= '<p>'.__('Website', CMS_TXT_DOMAIN).': <span class="required">('.__('not required', CMS_TXT_DOMAIN).')</span></p>';
                $out .= '<input class="text-ctrl" type="text" value="'.$comment_author_url.'" name="url" id="url" />';
                
                if(GetDCCPInterface()->getIGeneral()->isAuthorizationComment()) 
                {
                    $secure_data = dcf_getSecurityImage();
                    $out .= '<input type="hidden" name="dc_scode" value="'.$secure_data['code'].'" /> ';
                    $out .= '<p>'.__('Authorization code from image', CMS_TXT_DOMAIN).': <span class="required">('.__('required', CMS_TXT_DOMAIN).')</span></p>';
                    $out .= '<input class="text-ctrl" type="edit" name="dc_scodeuser" value="" /><br />';                  
                    $out .= '<div class="authorization">'.$secure_data['image'].'</div>';              
                }
            } else
            {
                $out .= '<span style="font-size:11px;color:#888888;">'.__('Logged in as', CMS_TXT_DOMAIN).'</span> ';
                $out .= '<a style="font-size:11px;" href="'.get_bloginfo('url').'/wp-admin/profile.php">'.$user_identity.'</a><span style="font-size:11px;color:#888888;">.</span> ';
                $out .= '<span style="font-size:11px;color:#888888;">'.__('Here you can', CMS_TXT_DOMAIN).'</span> ';
                $out .= '<a style="font-size:11px;" href="'.get_bloginfo('url').'/wp-login.php?action=logout">'.__('log out', 'dc_theme').'</a> ';
                $out .= '<span style="font-size:11px;color:#888888;">'.__('of this account.', CMS_TXT_DOMAIN).'</span>';
            }

            $out .= '<p>'.__('Message', CMS_TXT_DOMAIN).': <span class="required">('.__('required', CMS_TXT_DOMAIN).')</span></p>';
            $out .= '<textarea class="textarea-ctrl" rows="5" cols="10" name="comment" id="comment" ></textarea>';        
            $out .= '<div class="clear-both"></div>'; 
            $out .= '<a class="dc-btn-small" href="#" onclick="this.blur(); return false;" onmouseup="document.commentform.submit();">'.__('Send comment', CMS_TXT_DOMAIN).'</a>';
            $out .= '<div class="clear-both"></div>';
            echo $out;
            
            comment_id_fields();
            do_action('comment_form', $post->ID);                                    
                    
            $out = '</form>'; 
            $out .= '<div class="cancel-respond" id="cancel-comment-reply">';
            echo $out;
            
            cancel_comment_reply_link(__('Cancel reply', 'dc_theme'));
            
            $out  = '</div>';               
            $out .= '</div>'; // common-form          
            $out .= '</div> <!-- respond -->';        
            echo $out;
        }
        
    } else
    {
       echo 'The comments are closed.';
    }
    
?>
