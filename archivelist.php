<?php
/*
Template Name: Archive List
*/

/**********************************************************************
* MODERN ELEGANCE WORDPRESS THEME 
* (Ideal For Business And Personal Use: Portfolio or Blog)   
* 
* File name:   
*      archivelist.php
* Brief:       
*      Theme archive list page template code
* Author:      
*      DigitalCavalry
* Author URI:
*      http://themeforest.net/user/DigitalCavalry
* Contact:
*      digitalcavalry@gmail.com   
***********************************************************************/ 

    get_header();                                                
?>

    <div id="content">
         
        <?php 
            $page_common_opt = $GLOBALS['dc_pagecommon_opt'];            
            GetDCCPInterface()->getIGeneral()->includeSidebar($page_common_opt['page_sid'], $page_common_opt['page_sid_pos']);
            
            if(GetDCCPInterface()->getIGeneral()->getSidebarGlobalPos($page_common_opt['page_sid_pos']) == CMS_SIDEBAR_RIGHT)
            {
                echo '<div class="page-width-left">';                              
            } else
            {
                echo '<div class="page-width-right">';     
            }   
            dcf_naviTree($post->ID, 0);   

            echo '<h1>'.$post->post_title.'</h1>';
            the_content();
            
            $page_opt = get_post_meta($post->ID, 'archivelist_opt', 'true');
            
            $paged = (get_query_var('paged')) ? get_query_var('paged') : 1; 
            $args = array('paged'=>$paged, 'post_type' => 'post', 'post_status' => 'publish', 'posts_per_page' => $page_opt['alist_per_page']);
        
            if(trim($page_opt['alist_cats']) != '') { $args['cat'] = $page_opt['alist_cats']; }                  
            if(trim($page_opt['alist_excluded']) != '') { $args['category__not_in'] = explode(',', $page_opt['alist_excluded']); }                        
            
            query_posts($args);                         
            $max_page = 0;
            $max_page = $wp_query->max_num_pages;         
   
            $out = '';            
            if($page_opt['alist_head'] != '')
            {
                $out .= '<div class="common-block-head"><span class="head-text">'.$page_opt['alist_head'].'</span></div>';   
            }
        
            if(have_posts())
            {
                $out .= '<div class="archive-list-page">';
                while(have_posts())
                {                
                    the_post();  
                    $out .= '<a href="'.get_permalink($post->ID).'">'.$post->post_title.'</a>';
                    $out .= ' <span class="info">('.get_the_time('F j, Y').')</span>';
                    $out .= '<br />';
                } 
                $out .= '</div>';
            }            
        
            echo $out; 
            
            GetDCCPInterface()->getIGeneral()->renderSitePagination($paged, $max_page);                        
        ?>
                           
        </div>  <!-- page-width-xx0 -->
        <div class="clear-both"></div>
    </div> <!-- content -->
    
<?php    
    get_footer();
?>



