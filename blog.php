<?php
/*
Template Name: Blog
*/

/**********************************************************************
* MODERN ELEGANCE WORDPRESS THEME 
* (Ideal For Business And Personal Use: Portfolio or Blog)   
* 
* File name:   
*      blog.php
* Brief:       
*      Theme blog page template code
* Author:      
*      DigitalCavalry
* Author URI:
*      http://themeforest.net/user/DigitalCavalry
* Contact:
*      digitalcavalry@gmail.com   
***********************************************************************/ 

    get_header();                                                                                               
?>

    <div id="content">     
        <?php 
                 
            $page_common_opt = $GLOBALS['dc_pagecommon_opt'];            
            GetDCCPInterface()->getIGeneral()->includeSidebar($page_common_opt['page_sid'], $page_common_opt['page_sid_pos']);
            
            if(GetDCCPInterface()->getIGeneral()->getSidebarGlobalPos($page_common_opt['page_sid_pos']) == CMS_SIDEBAR_RIGHT)
            {
                echo '<div class="page-width-left">';                              
            } else
            {
                echo '<div class="page-width-right">';     
            }                   
            dcf_naviTree($post->ID, 0);  
             
            $psettings = get_post_meta($post->ID, 'blogpage_opt', true);           
            $blog_cats = $psettings['b_cats'];
            $blog_catsex = $psettings['b_excats'];
            $blog_layout = $psettings['b_layout'];            
            
            echo '<h1>'.$post->post_title.'</h1>';
            the_content();
            
            // prepeare posts query 
            $paged = 1;

            $page_on_front = (int)get_option('page_on_front');
            if($page_on_front != 0 and $page_on_front == $post->ID)
            {
                $paged = ($wp_query->query_vars['page']) ? $wp_query->query_vars['page'] : 1;  
            } else            
            {
                $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
            }
            $args=array('paged'=>$paged,'post_type' => 'post');         
            
            if($blog_cats != '') { $args['cat'] = $blog_cats; }                  
            if($blog_catsex != '') { $args['category__not_in'] = explode(',', $blog_catsex); }                        
            
            query_posts($args);                         
            $max_page = $wp_query->max_num_pages; 
               
            GetDCCPInterface()->getIRenderer()->loopBlogPosts($blog_layout);        
            GetDCCPInterface()->getIGeneral()->renderSitePagination($paged, $max_page);             
                               
        ?>                    
        </div> <!-- page-width -->
        <div class="clear-both"></div>
    </div> <!-- content -->
    
<?php    
    get_footer();
?>



