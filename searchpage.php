<?php
/*
Template Name: Search Page
*/

/**********************************************************************
* MODERN ELEGANCE WORDPRESS THEME 
* (Ideal For Business And Personal Use: Portfolio or Blog)   
* 
* File name:   
*      searchpage.php
* Brief:       
*      Theme search page template code
* Author:      
*      DigitalCavalry
* Author URI:
*      http://themeforest.net/user/DigitalCavalry
* Contact:
*      digitalcavalry@gmail.com   
***********************************************************************/ 

    get_header();                                                                
?>

    <div id="content">      
        <?php 
            GetDCCPInterface()->getIGeneral()->includeSidebar(GetDCCPInterface()->getIGeneral()->getSearchSidebarID(), CMS_SIDEBAR_RIGHT);     
            
            echo '<div class="page-width-left">';
            dcf_naviTree($post->ID, 0); 
                           
            echo '<h1>'.$post->post_title.'</h1>';                                                        
                                    
            $out = '';
            $out .= '<div class="search-page-controls">';
                $out .= '<form role="search" method="get" id="searchform" action="'.get_bloginfo('url').'">';
                    $out .= '<input type="text" value="" name="s" id="s">';
                    $out .= '<a class="submit-btn"></a>';
                $out .= '</form>';
                $out .= '<div class="clear-both"></div>'; 
            $out .= '</div>';
            
            echo $out;
                       
           ?>
                           
        </div>  <!-- page-width-x -->
        <div class="clear-both"></div>
    </div> <!-- content -->
    
<?php    
    get_footer();
?>



