<?php
/*
Template Name: Services
*/

/**********************************************************************
* MODERN ELEGANCE WORDPRESS THEME 
* (Ideal For Business And Personal Use: Portfolio or Blog)   
* 
* File name:   
*      services.php
* Brief:       
*      Theme services page template code
* Author:      
*      DigitalCavalry
* Author URI:
*      http://themeforest.net/user/DigitalCavalry
* Contact:
*      digitalcavalry@gmail.com   
***********************************************************************/ 
    get_header();                                                
?>

    <div id="content">
        <?php 
            echo '<div class="page-width-full">';        
            dcf_naviTree($post->ID, 0);   

            echo '<h1>'.$post->post_title.'</h1>';
            the_content();                                          
                        
            $page_opt = get_post_meta($post->ID, 'servicepage_opt', true);
            
            if((bool)$page_opt['se_show_one_cbox'])
            {
                $id_array = explode(',', trim($page_opt['se_posts_one']));
                
                global $wpdb;
                $querystr = "
                    SELECT ID, post_title, post_content
                    FROM $wpdb->posts
                    WHERE $wpdb->posts.post_type = '".CPThemeCustomPosts::PT_SERVICE_POST."' 
                    AND $wpdb->posts.post_status = 'publish'
                    AND $wpdb->posts.ID IN (".$page_opt['se_posts_one'].")";
                $data = $wpdb->get_results($querystr, OBJECT);            
            
                $data = dcf_sortPostInUserOrder($id_array, $data); 
                           
                $header = '';
                $subtitle = '';
                $desc = '';
                if((bool)$page_opt['se_show_header_one_cbox'])
                {
                    $header = $page_opt['se_header_one'];
                }
                if((bool)$page_opt['se_show_subtitle_one_cbox'])
                {
                    $subtitle = $page_opt['se_subtitle_one'];
                }                
                if((bool)$page_opt['se_show_desc_one_cbox'])
                {
                    $desc = $page_opt['se_desc_one'];
                }  
                
                GetDCCPInterface()->getIRenderer()->renderServices($data, $page_opt['se_layout_one'], $header, $subtitle, $desc);
            }
        
            echo '</div>  <!-- page-width-x -->';
        ?>
        <div class="clear-both"></div>
    </div> <!-- content -->
    
<?php    
    get_footer();
?>



