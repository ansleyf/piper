<?php
/**********************************************************************
* MODERN ELEGANCE WORDPRESS EDITION 
* (Ideal For Business And Personal Use: Portfolio or Blog)   
* 
* File name:   
*      search.php
* Brief:       
*      Theme search page code
* Author:      
*      DigitalCavalry
* Author URI:
*      http://themeforest.net/user/DigitalCavalry
* Contact:
*      digitalcavalry@gmail.com   
***********************************************************************/ 

    get_header();            
    
    $search_query = (get_query_var( 's' )) ? get_query_var( 's' ) : '';

    
    $pre_title = GetDCCPInterface()->getIGeneral()->getSearchTitleText();
    if($pre_title != '')
    {
        $pre_title .= '&nbsp;';
    }
                                                   
?>

    <div id="content">      
        <?php 
            GetDCCPInterface()->getIGeneral()->includeSidebar(GetDCCPInterface()->getIGeneral()->getSearchSidebarID(), CMS_SIDEBAR_RIGHT);     
            
            echo '<div class="page-width-left">';
            dcf_naviTree(null, 0, $pre_title.$search_query); 
                           
            echo '<h1>'.$pre_title.$search_query.'</h1>';           
                                  
            $page_size = GetDCCPInterface()->getIGeneral()->getSearchPageSize();           
            $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
            $args = array('s' => $search_query, 'posts_per_page'=>$page_size, 'paged'=>$paged);

            $search_array = Array();
            if(GetDCCPInterface()->getIGeneral()->isSearchedInPosts()) { array_push($search_array, 'post'); }
            if(GetDCCPInterface()->getIGeneral()->isSearchedInPages()) { array_push($search_array, 'page'); }          
            if(count($search_array) == 0) { array_push($search_array, 'post'); }
            $args['post_type'] = $search_array;                 
                                    
            $new_wp_query = new WP_Query($args);
            $max_page = $new_wp_query->max_num_pages;            
            
            $out = '';
            if(GetDCCPInterface()->getIGeneral()->isSearchFormShowed())
            {
                $out .= '<div class="search-page-controls">';
                    $out .= '<form role="search" method="get" id="searchform" action="'.get_bloginfo('url').'">';
                        $out .= '<input type="text" value="'.$search_query.'" name="s" id="s">';
                        $out .= '<a class="submit-btn"></a>';
                    $out .= '</form>';
                    $out .= '<div class="clear-both"></div>'; 
                $out .= '</div>';                
            }
            
            
            if($new_wp_query->have_posts())
            {
                $out .= '<div class="search-page">';   
                $counter = 0;
                while($new_wp_query->have_posts())
                {
                    $new_wp_query->the_post(); 
                   
                    $class = '';
                    if($counter == 0) {$class = ' item-first-top-border'; }
                   
                    $out .= '<div class="item'.$class.'">';
                        $out .= '<div class="info-bar">';
                            $out .= '<span class="date">'.get_the_time('F j, Y').'</span>';
                        $out .= '</div>';
                        
                        if($post->post_type == 'page')
                        {
                            $out .= '<h4><a href="'.get_permalink($post->ID).'">'.$post->post_title.'</a></h4>';  
                        } else
                        
                        if($post->post_type == 'post')
                        {                                                                                    
                            $out .= '<h4><a href="'.get_permalink($post->ID).'">'.$post->post_title.'</a></h4>';
                        }                        
                    $out .= '</div>';
                    
                    $counter++;                               
                }                        
                $out .= '</div>';
            } else 
            {
                $out .= '<p class="theme-exception">'.GetDCCPInterface()->getIGeneral()->getSearchNoResultsText().'</p>';  
            }
            
            echo $out;
        
            GetDCCPInterface()->getIGeneral()->renderSitePagination($paged, $max_page);         
                                   
           ?>
                           
        </div>  <!-- page-width-x -->
        <div class="clear-both"></div>
    </div> <!-- content -->
    
<?php    
    get_footer();
?>



