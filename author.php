<?php
/**********************************************************************
* MODERN ELEGANCE WORDPRESS THEME  
* (Ideal For Business And Personal Use: Portfolio or Blog)   
* 
* File name:   
*      author.php
* Brief:       
*      Theme author page code
* Author:      
*      DigitalCavalry
* Author URI:
*      http://themeforest.net/user/DigitalCavalry
* Contact:
*      digitalcavalry@gmail.com   
***********************************************************************/ 

    get_header();     
    $authornameslug = get_query_var('author_name'); 
    $authorinfo = get_user_by('slug', $authornameslug);       
                                       
?>

    <div id="content">
    
        <?php 
            GetDCCPInterface()->getIGeneral()->includeSidebar(GetDCCPInterface()->getIGeneral()->getSidebarForBlogCat(), CMS_SIDEBAR_RIGHT); 
            
            echo '<div class="page-width-left">';        
            dcf_naviTree($post->ID, 0, $authorinfo->display_name);   
                        
            echo '<h1>'.$authorinfo->display_name.'</h1>';
            
            if($authorinfo->description != '')
            {
                echo '<div class="page-description">'.$authorinfo->description.'</div>';
            }    
                  
            $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;            
            $args=array('paged'=>$paged,'author'=>$authorinfo->ID);           
            
            query_posts($args);                         
            $max_page = $wp_query->max_num_pages;
                             
            GetDCCPInterface()->getIRenderer()->loopBlogPosts();                         
            GetDCCPInterface()->getIGeneral()->renderSitePagination($paged, $max_page);             
                               
        ?>                    
        </div> <!-- page-width --> 
        <div class="clear-both"></div>
    </div> <!-- content -->
    
<?php    
    get_footer();
?>



