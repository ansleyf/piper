<?php
/*
Template Name: Contact
*/

/**********************************************************************
* MODERN ELEGANCE WORDPRESS THEME  
* (Ideal For Business And Personal Use: Portfolio or Blog)   
* 
* File name:   
*      contact.php
* Brief:       
*      Theme contact page template code
* Author:      
*      DigitalCavalry
* Author URI:
*      http://themeforest.net/user/DigitalCavalry
* Contact:
*      digitalcavalry@gmail.com   
***********************************************************************/ 

    get_header();                                                                                                   
?>

    <div id="content">
        <?php 

            $page_common_opt = $GLOBALS['dc_pagecommon_opt'];            
            GetDCCPInterface()->getIGeneral()->includeSidebar($page_common_opt['page_sid'], $page_common_opt['page_sid_pos']);
            
            if(GetDCCPInterface()->getIGeneral()->getSidebarGlobalPos($page_common_opt['page_sid_pos']) == CMS_SIDEBAR_RIGHT)
            {
                echo '<div class="page-width-left">';                              
            } else
            {
                echo '<div class="page-width-right">';     
            }       
            dcf_naviTree($post->ID, 0); 
          
            echo '<h1>'.$post->post_title.'</h1>';            
            the_content();               
           
            $contact_opt = get_post_meta($post->ID, 'contactpage_opt', true);
           
            $out = '';
            $out .= '<div class="common-form">'; 

                $out .= '<form>'; 
                $out .= '<p>'.__('Your Name', CMS_TXT_DOMAIN).': <span class="required">('.__('required', CMS_TXT_DOMAIN).')</span></p>'; 
                $out .= '<input type="text" name="name" class="text-ctrl" />';

                $out .= '<p>'.__('Your Email', CMS_TXT_DOMAIN).': <span class="required">('.__('required', CMS_TXT_DOMAIN).')</span></p>'; 
                $out .= '<input type="text" name="email" class="text-ctrl" />'; 
                 
                $out .= '<p>'.__('Subject', CMS_TXT_DOMAIN).': <span class="required">('.__('required', CMS_TXT_DOMAIN).')</span></p>'; 
                $out .= '<input type="text" name="subject" class="text-ctrl" />';  

                if(GetDCCPInterface()->getIGeneral()->isAuthorizationContact())
                {
                    $secure_data = dcf_getSecurityImage();
                    $out .= '<input type="hidden" name="dc_scode" value="'.$secure_data['code'].'" /> ';
                    $out .= '<p>'.__('Authorization code from image', CMS_TXT_DOMAIN).': <span class="required">('.__('required', CMS_TXT_DOMAIN).')</span></p>';
                    $out .= '<input class="text-ctrl" type="edit" name="dc_scodeuser" value="" /><br />';                  
                    $out .= '<div class="authorization">'.$secure_data['image'].'</div>';                   
                }
                
                $out .= '<p>'.__('Your Message', CMS_TXT_DOMAIN).': <span class="required">('.__('required', CMS_TXT_DOMAIN).')</span></p>';                                      
                $out .= '<textarea cols="70" rows="8" name="message" class="textarea-ctrl"></textarea>';                   
                $out .= '<input type="hidden" value="'.$contact_opt['contact_mail'].'" name="contact-mail-dest" />';
                $out .= '<input type="hidden" value="'.__('The message has been sent. Thank you', 'dc_theme').'" name="contact-okay" />';
                $out .= '<input type="hidden" value="'.__('The message wasn\'t sent, please try again later.', CMS_TXT_DOMAIN).'" name="contact-error" />';                     
                $out .= '<div class="clear-both"></div>';
                $out .= '<a class="dc-btn-small send-email-btn" href="#" onclick="this.blur(); return false;">'.__('Send email', CMS_TXT_DOMAIN).'</a>'; 
                
                
                $out .= '<div class="clear-both"></div>';
                $out .= '<span class="result">Information about email sending process</span>';                 
                           
                $out .= '</form>'; 
            $out .= '</div> <!-- common-form -->';   
            echo $out;
           
           ?>
                     
        </div>  <!-- page-width -->
        <div class="clear-both"></div>
    </div> <!-- content -->
    
<?php    
    get_footer();
?>



