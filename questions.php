<?php
/*
Template Name: Questions
*/

/**********************************************************************
* MODERN ELEGANCE WORDPRESS THEME 
* (Ideal For Business And Personal Use: Portfolio or Blog)   
* 
* File name:   
*      questions.php
* Brief:       
*      Theme questions page template code
* Author:      
*      DigitalCavalry
* Author URI:
*      http://themeforest.net/user/DigitalCavalry
* Contact:
*      digitalcavalry@gmail.com   
***********************************************************************/ 

    get_header();                                                
?>

    <div id="content">
         
        <?php 
            $page_common_opt = $GLOBALS['dc_pagecommon_opt'];            
            GetDCCPInterface()->getIGeneral()->includeSidebar($page_common_opt['page_sid'], $page_common_opt['page_sid_pos']);
            
            if(GetDCCPInterface()->getIGeneral()->getSidebarGlobalPos($page_common_opt['page_sid_pos']) == CMS_SIDEBAR_RIGHT)
            {
                echo '<div class="page-width-left">';                              
            } else
            {
                echo '<div class="page-width-right">';     
            }   
            dcf_naviTree($post->ID, 0);   

                        
            $questions_opt = get_post_meta($post->ID, 'quest_opt', true);
            $per_page = (int)$questions_opt['quest_per_page'];
            $quest_selected = $questions_opt['quest_selected'];
            $quest_cats =  $questions_opt['quest_cats'];
            $quest_order_by_views = (bool)$questions_opt['quest_order_views_cbox'];
            $quest_group = (bool)$questions_opt['quest_group_cbox']; 
            
            echo '<h1>'.$post->post_title.'</h1>';
            the_content();                

            $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;                          
            global $wpdb;
            
            if($quest_group and $quest_cats != '')
            {
                $quest_terms = get_terms(CPThemeCustomPosts::PT_QUESTION_CAT);
                //var_dump($quest_terms);
                $cats = explode(',', $quest_cats);
                $cat_rendered = 0;
            
                if(is_array($cats) and is_array($quest_terms))
                {
                    foreach($cats as $cat_id)
                    {
                        $onlist = false;
                        $processed = null;
                        
                        foreach($quest_terms as $qterm)
                        {
                            if($qterm->term_id == (int)$cat_id) 
                            {
                                $onlist = true;
                                $processed = $qterm;
                                break;                
                            } 
                        }
                        
                        if($onlist)
                        {
                            
                            $querystr = "SELECT DISTINCT ID, post_title, post_content ";
                                if($quest_order_by_views) { $querystr .= ", meta_value "; }
                                $querystr .= "FROM $wpdb->posts ";
                                
                                if($quest_order_by_views)
                                {
                                    $querystr .= "LEFT JOIN $wpdb->postmeta ON($wpdb->posts.ID = $wpdb->postmeta.post_id) ";
                                }

                               $querystr .= "LEFT JOIN $wpdb->term_relationships ON($wpdb->posts.ID = $wpdb->term_relationships.object_id)
                                LEFT JOIN $wpdb->term_taxonomy ON($wpdb->term_relationships.term_taxonomy_id = $wpdb->term_taxonomy.term_taxonomy_id)
                                LEFT JOIN $wpdb->terms ON($wpdb->term_taxonomy.term_id = $wpdb->terms.term_id) ";                    
                                
                                $querystr .= "WHERE $wpdb->posts.post_type = '".CPThemeCustomPosts::PT_QUESTION_POST."'                     
                                AND $wpdb->posts.post_status = 'publish' ";
                                if($quest_order_by_views) 
                                {
                                    $querystr .= "AND $wpdb->postmeta.meta_key = 'questviews' ";    
                                }                  
                                       
                                $querystr .= "AND $wpdb->term_taxonomy.taxonomy = '".CPThemeCustomPosts::PT_QUESTION_CAT."'
                                AND $wpdb->term_taxonomy.term_id = $processed->term_id "; 
                                
                                $order = '';
                                if($quest_order_by_views)
                                {
                                    $order .= "ORDER BY CAST(meta_value AS UNSIGNED) ";      
                                } else
                                {
                                    $order = " ORDER BY $wpdb->posts.post_date ";     
                                }
                                
                                $desc_asc = " DESC";                    
                                $querystr .= $order.$desc_asc;

                            $data = $wpdb->get_results($querystr, OBJECT);                        
                            $count = count($data);
                            
                            $out = '';
                            if($cat_rendered > 0) { $out .= '<div class="faq-cat-separator"></div>'; }
                            $out .= '<div class="common-block-head"><span class="head-text">';
                                if((bool)$questions_opt['quest_showcatdesc_cbox'])
                                {
                                    $out .= $processed->description; 
                                } else
                                {
                                    $out .= $processed->name;
                                }
                            $out .= '</span></div>';
                            
                            if($count > 0)
                            {                                   
                                $counter = 0;            
                                for($i = 0; $i < $count; $i++) 
                                {
                                    $pq = & $data[$i];
                                    $pq_opt = get_post_meta($pq->ID, 'question_opt', true);
                                      
                                    
                                    $color = '';
                                    if((bool)$pq_opt['forcecolor_cbox'])
                                    {
                                        $color = ' style="color:'.$pq_opt['color'].';" ';    
                                    } else
                                    if((bool)$questions_opt['quest_forcecolor_cbox'])
                                    {
                                        $color = ' style="color:'.$questions_opt['quest_color'].';" ';     
                                    } else 
                                    if((bool)$pq_opt['usecolor_cbox'])
                                    {
                                        $color = ' style="color:'.$pq_opt['color'].';" ';    
                                    }                                  
                                    
                                    $counter++;
                                    $out .= '<div class="faq-question">';
                                        
                                        // head
                                        $out .= '<div class="quest-head head-close">';
                                            $out .= '<span class="postid">'.$pq->ID.'</span>';
                                            $out .= '<a class="title" '.$color.'>';
                                                if((bool)$questions_opt['quest_counter_cbox'])
                                                {
                                                    $out .= '<span class="counter">'.$counter.'.</span> ';
                                                }
                                                $out .= $pq->post_title;
                                            $out .= '</a>';
                                            if((bool)$questions_opt['quest_views_cbox'])
                                            {
                                                $views = get_post_meta($pq->ID, 'questviews', true);
                                                $out .= '<div class="views"><span class="count">'.$views.'</span> '.__('views', 'dc_theme').'</div>';
                                            }
                                        $out .= '</div>';
                                        
                                        // content
                                        $out .= '<div class="quest-content">';
                                            $out .= apply_filters('the_content', $pq->post_content); 
                                        $out .= '</div>';
                                    $out .= '</div>';

                                } 
                            }                           
                            
                            echo $out; 
                            $cat_rendered++;
                        } // onlist
                        
                    } // foreach  
                } 
                
                   
            } else
            {
            
                $querystr = "SELECT SQL_CALC_FOUND_ROWS DISTINCT ID, post_title, post_content ";
                    if($quest_order_by_views) { $querystr .= ", meta_value "; }
                    $querystr .= "FROM $wpdb->posts ";
                    
                    if($quest_order_by_views)
                    {
                        $querystr .= "LEFT JOIN $wpdb->postmeta ON($wpdb->posts.ID = $wpdb->postmeta.post_id) ";
                    }
                    
                    if($quest_cats != '' and $quest_selected == '')
                    {
                       $querystr .= "LEFT JOIN $wpdb->term_relationships ON($wpdb->posts.ID = $wpdb->term_relationships.object_id)
                        LEFT JOIN $wpdb->term_taxonomy ON($wpdb->term_relationships.term_taxonomy_id = $wpdb->term_taxonomy.term_taxonomy_id)
                        LEFT JOIN $wpdb->terms ON($wpdb->term_taxonomy.term_id = $wpdb->terms.term_id) ";                    
                    }
                    
                    $querystr .= "WHERE $wpdb->posts.post_type = '".CPThemeCustomPosts::PT_QUESTION_POST."'                     
                    AND $wpdb->posts.post_status = 'publish' ";
                    if($quest_order_by_views) 
                    {
                        $querystr .= "AND $wpdb->postmeta.meta_key = 'questviews' ";    
                    }
                   
                     
                    if($quest_selected != '')
                    {
                        $querystr .= "AND $wpdb->posts.ID IN ($quest_selected) ";    
                    }
                    
                    if($quest_cats != '' and $quest_selected == '')
                    {                
                        $querystr .= "AND $wpdb->term_taxonomy.taxonomy = '".CPThemeCustomPosts::PT_QUESTION_CAT."'
                        AND $wpdb->term_taxonomy.term_id IN ($quest_cats) "; 
                    }
                    
                    $order = '';
                    if($quest_order_by_views)
                    {
                        $order .= "ORDER BY CAST(meta_value AS UNSIGNED) ";      
                    } else
                    {
                        $order = " ORDER BY $wpdb->posts.post_date ";     
                    }
                    
                    $desc_asc = " DESC";
                    
                    $limit =  " LIMIT ".($per_page*($paged-1)).", ".$per_page;
                    if($per_page == 0) { $limit = ''; }
                    
                    $querystr .= $order.$desc_asc.$limit;

                $data = $wpdb->get_results($querystr, OBJECT);                        
                $count = count($data);            
                
                $querystr = "SELECT FOUND_ROWS();";
                $allitems = $wpdb->get_results($querystr, ARRAY_N);
                $allitems = (int)$allitems[0][0];
                if($per_page > 0) { $max_page = (int)ceil($allitems / $per_page); } else { $max_page = 1; }                          
                
                if($allitems > 0)
                {     
                    $out = '';
                    
                    $counter = 0;            
                    for($i = 0; $i < $count; $i++) 
                    {
                        $pq = & $data[$i];
                        $pq_opt = get_post_meta($pq->ID, 'question_opt', true);
                          
                        
                        $color = '';
                        if((bool)$pq_opt['forcecolor_cbox'])
                        {
                            $color = ' style="color:'.$pq_opt['color'].';" ';    
                        } else
                        if((bool)$questions_opt['quest_forcecolor_cbox'])
                        {
                            $color = ' style="color:'.$questions_opt['quest_color'].';" ';     
                        } else 
                        if((bool)$pq_opt['usecolor_cbox'])
                        {
                            $color = ' style="color:'.$pq_opt['color'].';" ';    
                        }                                  
                        
                        $counter++;
                        $out .= '<div class="faq-question">';
                            
                            // head
                            $out .= '<div class="quest-head head-close">';
                                $out .= '<span class="postid">'.$pq->ID.'</span>';
                                $out .= '<a class="title" '.$color.'>';
                                    if((bool)$questions_opt['quest_counter_cbox'])
                                    {
                                        $out .= '<span class="counter">'.$counter.'.</span> ';
                                    }
                                    $out .= $pq->post_title;
                                $out .= '</a>';
                                if((bool)$questions_opt['quest_views_cbox'])
                                {
                                    $views = get_post_meta($pq->ID, 'questviews', true);
                                    $out .= '<div class="views"><span class="count">'.$views.'</span> '.__('views', 'dc_theme').'</div>';
                                }
                            $out .= '</div>';
                            
                            // content
                            $out .= '<div class="quest-content">';
                                $out .= apply_filters('the_content', $pq->post_content); 
                            $out .= '</div>';
                        $out .= '</div>';

                    }
                        
                    echo $out;
                } else
                {
                    echo '<p class="theme-exception">There are no Questions posts for this page.</p>'; 
                }            
                                                                                         
                GetDCCPInterface()->getIGeneral()->renderSitePagination($paged, $max_page);
            
            }  
                                                               
        ?>
                           
        </div>  <!-- page-width-xx0 -->
        <div class="clear-both"></div>
    </div> <!-- content -->
    
<?php    
    get_footer();
?>



