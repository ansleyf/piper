<?php
/*
Template Name: Gallery List
*/

/**********************************************************************
* MODERN ELEGANCE WORDPRESS THEME 
* (Ideal For Business And Personal Use: Portfolio or Blog)   
* 
* File name:   
*      gallery-list.php
* Brief:       
*      Theme gallery list page template code
* Author:      
*      DigitalCavalry
* Author URI:
*      http://themeforest.net/user/DigitalCavalry
* Contact:
*      digitalcavalry@gmail.com   
***********************************************************************/ 
    get_header();                                                
?>

    <div id="content">
        <?php 
            $page_common_opt = $GLOBALS['dc_pagecommon_opt'];            
            GetDCCPInterface()->getIGeneral()->includeSidebar($page_common_opt['page_sid'], $page_common_opt['page_sid_pos']);
            
            if(GetDCCPInterface()->getIGeneral()->getSidebarGlobalPos($page_common_opt['page_sid_pos']) == CMS_SIDEBAR_RIGHT)
            {
                echo '<div class="page-width-left">';                              
            } else
            {
                echo '<div class="page-width-right">';     
            }    
            dcf_naviTree($post->ID, 0); 

            echo '<h1>'.$post->post_title.'</h1>';
         
            the_content();                                          
        
            $gallerylist_opt = get_post_meta($post->ID, 'gallerylistpage_opt', true);
           // var_dump($gallerylist_opt);
                                      
            $paged = (get_query_var('paged')) ? get_query_var('paged') : 1; 
            $per_page = (int)$gallerylist_opt['gl_per_page'];
            if($per_page < 1) { $per_page = 1; }
            
            $preview_h = (int)$gallerylist_opt['gl_height'];
            if($preview_h < 80) { $preview_h = 80; }
            
            if($gallerylist_opt['gl_order'] == CMS_ORDER_ASC)
            {
                $gallerylist_opt['gl_order'] = 'ASC';    
            } else
            {
                $gallerylist_opt['gl_order'] = 'DESC';
            }    
                           
            $max_page = 0;
            $data = dcf_getGalleriesNGG('gid', $gallerylist_opt['gl_order'], 
                true, $per_page, (($paged-1)*$per_page), true, 
                trim($gallerylist_opt['gl_selected']), trim($gallerylist_opt['gl_excluded']), $max_page);
            

            $out = '';
            $preview_pics = '';
            if(is_array($data) and count($data) > 0)
            {
                $data = array_values($data); 
                $count = count($data);
                for($i = 0, $counter = 0; $i < $count; $i++)
                {
                    if($data[$i]->previewpic !== null and $data[$i]->previewpic != 0)
                    {
                        if($counter > 0) { $preview_pics .= ','; }
                        $preview_pics .= $data[$i]->previewpic;  
                        $counter++;  
                    } 
                }
                 
                
                $preview_pics = dcf_getNGGImagesFromIDList($preview_pics);    
                //var_dump($preview_pics);
                
                                                      
                for($i = 0; $i < $count; $i++)
                {
                    // finding preview image on list
                    $pp_count = count($preview_pics);
                    $pp_index = CMS_NOT_SELECTED;
                    for($j = 0; $j < $pp_count; $j++)
                    {
                        if($preview_pics[$j]->_pid == $data[$i]->previewpic)
                        {
                            $pp_index = $j;
                            break;    
                        }            
                    }       
                    
                    if($i > 0)
                    {
                        $out .= '<div class="gallery-list-item-separator"></div>';
                    }
                    $page_gallery_meta = get_post_meta($data[$i]->pageid, 'gallery_opt', true);
                    $show_three_images = false;
                    if(is_array($page_gallery_meta))
                    {
                        $show_three_images = (bool)$page_gallery_meta['ga_show_3img_onlist_cbox'];
                    }
                    
                    $out .= '<div class="gallery-list-item" >';                              
                        if($pp_index != CMS_NOT_SELECTED) 
                        {                  
                            if($show_three_images)
                            {
                                $ret_count = 0;
                                $three_pics = dcf_getNGGImagesFromGallery($data[$i]->gid, $ret_count, 3); 
                                $out .= '<div class="image-wrapper" style="background-color: transparent; padding: 0px; border: 0px solid #E5E5E5;">';
                                    $k_counter = 0;
                                    foreach($three_pics as $pic)
                                    { 
                                        $img_style = ' style="width:185px;background-color:#FAFAFA;padding:5px;border:1px solid #E5E5E5;height:'.$preview_h.'px;';
                                        if($k_counter < 2) { $img_style .= 'margin-right:14px'; }
                                        $img_style .= '" ';
                                        $out .= '<a '.$img_style.' class="async-img gallery-list-item-image-float" rel="'.dcf_getTimThumbURL($pic->_imageURL, 185, $preview_h).'"></a>';    
                                        $k_counter++;
                                    }
                                    
                                    $triger_style = ' style="width:620px;height:'.$preview_h.'px;" ';  
                                    $out .= '<a '.$triger_style.' class="triger" href="'.get_permalink($data[$i]->pageid).'"" ></a>';                                
                                $out .= '</div>';                                
                                  
                            } else
                            {                            
                                $out .= '<div class="image-wrapper">'; 
                                    $img_style = ' style="width:608px;height:'.$preview_h.'px;" ';
                                    $out .= '<a '.$img_style.' class="async-img gallery-list-item-image" rel="'.dcf_getTimThumbURL($preview_pics[$pp_index]->_imageURL, 608, $preview_h).'"></a>';    
                                    $out .= '<a '.$img_style.' class="triger" href="'.get_permalink($data[$i]->pageid).'"" ></a>';                                
                                $out .= '</div>';
                            }
                                                          
                            // post image description
                            if($preview_pics[$pp_index]->_description != '' and (bool)$gallerylist_opt['gl_show_desc_cbox'])
                            {
                                $out .= '<div class="gallery-list-image-desc">'.$preview_pics[$pp_index]->_alttext.'</div>';    
                            } else
                            {
                                $out .= '<div class="gallery-list-image-desc-empty"></div>';     
                            }                              
                        }
                                                      
                        $out .= '<h2 class="post-title">';                        
                            if($data[$i]->pageid != 0) { $out .= '<a href="'.get_permalink($data[$i]->pageid).'" >'; }
                            $out .= $data[$i]->title;
                            if($data[$i]->pageid != 0) { $out .= '</a>'; } 
                        $out .= '</h2>';
                        
                        
                        $authorinfo = get_user_by('id', $data[$i]->author);                       
                               //    var_dump($authorinfo);
                        // info bar
                        $out .= '<div class="post-info">';
                            $out .= 'By '.'<a href="'.
                get_author_posts_url($authorinfo->ID).'" class="author">'.$authorinfo->display_name.'</a>';
                            $out .= '<span class="separator">&nbsp;|&nbsp;</span>'.$data[$i]->counter.' Images';
                        $out .= '</div>';
                        
                        $out .= '<div class="gallery-desc">'.$data[$i]->galdesc.'</div>';
                        if($data[$i]->pageid != 0) { $out .= ' <a class="more-link" href="'.get_permalink($data[$i]->pageid).'" >&raquo;&nbsp;View&nbsp;gallery</a>'; }       
                    $out .= '</div>';
                }   
                
                         
            } else
            {
                $out .= '<p class="theme-exception">There are no galleries</p>'; 
            }  
            echo $out;                                 
        
            GetDCCPInterface()->getIGeneral()->renderSitePagination($paged, $max_page);
        
            echo '</div>  <!-- page-width-x -->';
        ?>
        <div class="clear-both"></div>
    </div> <!-- content -->
    
<?php    
    get_footer();
?>



