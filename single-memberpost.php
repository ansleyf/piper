<?php
/**********************************************************************
* MODERN ELEGANCE WORDPRESS THEME  
* (Ideal For Business And Personal Use: Portfolio or Blog)   
* 
* File name:   
*      single-memberpost.php
* Brief:       
*      Theme member single page code
* Author:      
*      DigitalCavalry
* Author URI:
*      http://themeforest.net/user/DigitalCavalry
* Contact:
*      digitalcavalry@gmail.com   
***********************************************************************/ 

    get_header();                                                     
?>

    <div id="content">
         
        <?php 
                                                                  
            $post_sid = GetDCCPInterface()->getIGeneral()->getTeamMemberSidebar();            
            GetDCCPInterface()->getIGeneral()->includeSidebar($post_sid, CMS_SIDEBAR_RIGHT);
            echo '<div class="page-width-left">';                                 
            dcf_naviTree($post->ID, 0);                 
            
            the_content();
           
            if('open' == $post->comment_status)
            {
                echo '<a name="comments"></a>';
                comments_template();
            }
        ?>
                           
        </div>  <!-- page-width -->
        <div class="clear-both"></div>
    </div> <!-- content -->
    
<?php    
    get_footer();
?>