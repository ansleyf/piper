<?php
/*
Template Name: Portfolio
*/

/**********************************************************************
* MODERN ELEGANCE WORDPRESS THEME 
* (Ideal For Business And Personal Use: Portfolio or Blog)   
* 
* File name:   
*      portfolio.php
* Brief:       
*      Theme portfolio page template code
* Author:      
*      DigitalCavalry
* Author URI:
*      http://themeforest.net/user/DigitalCavalry
* Contact:
*      digitalcavalry@gmail.com   
***********************************************************************/ 

    get_header();                                                
?>

    <div id="content">
         
        <?php 
            $portfolio_opt = get_post_meta($post->ID, 'portfolio_opt', true); 
            
            if($portfolio_opt['po_layout'] == CMS_PORTFOLIO_LAYOUT_SIDEBAR)
            {
                $page_common_opt = $GLOBALS['dc_pagecommon_opt'];            
                GetDCCPInterface()->getIGeneral()->includeSidebar($page_common_opt['page_sid'], $page_common_opt['page_sid_pos']);
                
                if(GetDCCPInterface()->getIGeneral()->getSidebarGlobalPos($page_common_opt['page_sid_pos']) == CMS_SIDEBAR_RIGHT)
                {
                    echo '<div class="page-width-left">';                              
                } else
                {
                    echo '<div class="page-width-right">';     
                }                 
            } else
            {
                echo '<div class="page-width-full">';     
            }
            dcf_naviTree($post->ID, 0);   

            echo '<h1>'.$post->post_title.'</h1>';
            the_content();
            
            
            
            $po_cats = $portfolio_opt['po_cats'];
            $po_excats = $portfolio_opt['po_excats']; 
            $po_per_page = (int)$portfolio_opt['po_per_page']; 
            if($po_per_page < 1) { $po_per_page = 1; }
            
            // prepeare posts query 
            $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
            $args = array('paged'=>$paged, 'post_type' => 'post', 'posts_per_page' => $po_per_page);         
            
            if(trim($po_cats) != '') { $args['cat'] = $po_cats; }                  
            if(trim($po_excats) != '') { $args['category__not_in'] = explode(',', $po_excats); }                        
                              
            query_posts($args);                         
            $max_page = 0;
            $max_page = $wp_query->max_num_pages; 
           
            if($portfolio_opt['po_layout'] == CMS_PORTFOLIO_LAYOUT_SIDEBAR)
            {
            
                $out = '';
                if(have_posts())
                {
                    $out .= '<div class="sidebar-portfolio">';
                        $counter = 0;
                        while(have_posts())
                        {
                            if($counter > 0) { $out .= '<div class="separator"></div>'; }  
                                                    
                            $counter++;
                            the_post();      
                                                                           
                            $post_opt = get_post_meta($post->ID, 'post_opt', true);
                            $post_opt['image_url'] = dcf_isNGGImageID($post_opt['image_url']);
                            
                            $height = 240;
                            $style = '';
                            if((bool)$portfolio_opt['po_use_img_h_cbox'])
                            {
                                $style = ' style="height:'.(int)$portfolio_opt['po_img_h'].'px;" ';
                                $height = (int)$portfolio_opt['po_img_h'];
                            }                            
                                                                         
                            $out .= '<div class="item">';
                            
                                $out .= '<div class="left-side">';
                                    $out .= '<div class="image-wrapper" '.$style.'>';
                                        $out .= '<a class="image async-img" rel="'.dcf_getTimThumbURL($post_opt['image_url'], 360, $height).'" '.$style.'></a>';
                                        if((bool)$portfolio_opt['po_img_as_link_cbox'])
                                        {
                                            $out .= '<a class="triger-link" href="'.get_permalink().'" '.$style.'></a>';
                                        } else
                                        {
                                            $out .= '<a class="triger" href="'.$post_opt['image_url'].'" rel="lightbox[portolio_'.$pageid.']" '.$style.'></a>'; 
                                        }  
                                    $out .= '</div>';
                                    
                                    if((bool)$portfolio_opt['po_show_imgdesc_cbox'])   
                                    {                                  
                                        // post image description
                                        if($post_opt['image_desc'] != '')
                                        {
                                            $out .= '<div class="image-desc">'.$post_opt['image_desc'].'</div>';    
                                        } else
                                        {
                                            $out .= '<div class="image-desc-empty"></div>';     
                                        } 
                                    }                                      
                                $out .= '</div>';                                                                                      
                                
                                $out .= '<div class="right-side">';
                                    if((bool)$portfolio_opt['po_show_title_cbox'])
                                    {
                                        $out .= '<h3 class="post-title"><a href="'.get_permalink().'">'.$post->post_title.'</a></h3>';
                                    }
                                    
                                    if((bool)$portfolio_opt['po_show_desc_cbox'])
                                    {           
                                        if($post->post_excerpt != '')
                                        {                  
                                            $out .= apply_filters('the_content', $post->post_excerpt);     
                                            $out .= '<a href="'.get_permalink($post->ID).'">&raquo;&nbsp;'.__('Read&nbsp;more', CMS_TXT_DOMAIN).'</a>';
                                        } else
                                        {                                                     
                                            global $more;
                                            $more = 0;
                                            $out .= apply_filters('the_content', get_the_content(__('&raquo;&nbsp;Read&nbsp;more', CMS_TXT_DOMAIN)));
                                        }
                                    }
                                $out .= '</div>';
                                
                                $out .= '<div class="clear-both"></div>';    
                            $out .= '</div>';
                        } // while
                        $out .= '<div class="clear-both"></div>';
                    $out .= '</div>';
                } else
                {
                    $out .= '<p class="theme-exception">There are no posts</p>'; 
                }       
                echo $out;                 
                   
            } else             
            if($portfolio_opt['po_layout'] == CMS_PORTFOLIO_LAYOUT_TABLE2)
            {
            
                $out = '';
                if(have_posts())
                {
                    $out .= '<div class="table2-portfolio">';
                        $counter = 0;
                        while(have_posts())
                        {
                            if($counter > 0 and ($counter % 2) == 0) { $out .= '<div class="separator"></div>'; }  
                                                    
                            $counter++;
                            the_post();      
                                                                           
                            $post_opt = get_post_meta($post->ID, 'post_opt', true);
                            $post_opt['image_url'] = dcf_isNGGImageID($post_opt['image_url']);
                            
                            $height = 228;
                            $style = '';
                            if((bool)$portfolio_opt['po_use_img_h_cbox'])
                            {
                                $style = ' style="height:'.(int)$portfolio_opt['po_img_h'].'px;" ';
                                $height = (int)$portfolio_opt['po_img_h'];
                            }                            
                            
                            $margin = '';
                            if(($counter % 2) != 0) { $margin = ' item-margin'; }                                                
                            $out .= '<div class="item'.$margin.'">';
                                $out .= '<div class="image-wrapper" '.$style.'>';
                                    $out .= '<a class="image async-img" rel="'.dcf_getTimThumbURL($post_opt['image_url'], 453, $height).'" '.$style.'></a>';
                                    if((bool)$portfolio_opt['po_img_as_link_cbox'])
                                    {
                                        $out .= '<a class="triger-link" href="'.get_permalink().'" '.$style.'></a>';
                                    } else
                                    {
                                        $out .= '<a class="triger" href="'.$post_opt['image_url'].'" rel="lightbox[portolio_'.$pageid.']" '.$style.'></a>'; 
                                    }
                                $out .= '</div>';
                                
                                if((bool)$portfolio_opt['po_show_imgdesc_cbox'])   
                                {                                  
                                    // post image description
                                    if($post_opt['image_desc'] != '')
                                    {
                                        $out .= '<div class="image-desc">'.$post_opt['image_desc'].'</div>';    
                                    } else
                                    {
                                        $out .= '<div class="image-desc-empty"></div>';     
                                    } 
                                }
                                
                                if((bool)$portfolio_opt['po_show_title_cbox'])
                                {
                                    $out .= '<h3 class="post-title"><a href="'.get_permalink().'">'.$post->post_title.'</a></h3>';
                                }
                                
                                if((bool)$portfolio_opt['po_show_desc_cbox'])
                                {            
                                    if($post->post_excerpt != '')
                                    {                  
                                        $out .= apply_filters('the_content', $post->post_excerpt);   
                                        $out .= '<a href="'.get_permalink($post->ID).'">&raquo;&nbsp;'.__('Read&nbsp;more', CMS_TXT_DOMAIN).'</a>';   
                                    } else   
                                    {                                             
                                        global $more;
                                        $more = 0;
                                        $out .= apply_filters('the_content', get_the_content(__('&raquo;&nbsp;Read&nbsp;more', 'dc_theme')));
                                    }
                                }
                                    
                            $out .= '</div>';
                        } // while
                        $out .= '<div class="clear-both"></div>';
                    $out .= '</div>';
                } else
                {
                    $out .= '<p class="theme-exception">There are no posts</p>'; 
                }       
                echo $out;                 
                   
            } else            
            if($portfolio_opt['po_layout'] == CMS_PORTFOLIO_LAYOUT_TABLE3)
            {
            
                $out = '';
                if(have_posts())
                {
                    $out .= '<div class="table3-portfolio">';
                        $counter = 0;
                        while(have_posts())
                        {
                            if($counter > 0 and ($counter % 3) == 0) { $out .= '<div class="separator"></div>'; }  
                                                    
                            $counter++;
                            the_post();      
                                                                           
                            $post_opt = get_post_meta($post->ID, 'post_opt', true);
                            $post_opt['image_url'] = dcf_isNGGImageID($post_opt['image_url']);
                            
                            $height = 300;
                            $style = '';
                            if((bool)$portfolio_opt['po_use_img_h_cbox'])
                            {
                                $style = ' style="height:'.(int)$portfolio_opt['po_img_h'].'px;" ';
                                $height = (int)$portfolio_opt['po_img_h'];
                            }                            
                            
                            $margin = '';
                            if(($counter % 3) != 0) { $margin = ' item-margin'; }                                                
                            $out .= '<div class="item'.$margin.'">';
                                $out .= '<div class="image-wrapper" '.$style.'>';
                                    $out .= '<a class="image async-img" rel="'.dcf_getTimThumbURL($post_opt['image_url'], 288, $height).'" '.$style.'></a>';
                                    if((bool)$portfolio_opt['po_img_as_link_cbox'])
                                    {
                                        $out .= '<a class="triger-link" href="'.get_permalink().'" '.$style.'></a>';
                                    } else
                                    {
                                        $out .= '<a class="triger" href="'.$post_opt['image_url'].'" rel="lightbox[portolio_'.$pageid.']" '.$style.'></a>'; 
                                    }
                                $out .= '</div>';
                                
                                if((bool)$portfolio_opt['po_show_imgdesc_cbox'])   
                                {                                  
                                    // post image description
                                    if($post_opt['image_desc'] != '')
                                    {
                                        $out .= '<div class="image-desc">'.$post_opt['image_desc'].'</div>';    
                                    } else
                                    {
                                        $out .= '<div class="image-desc-empty"></div>';     
                                    } 
                                }                                                        
                                
                                if((bool)$portfolio_opt['po_show_title_cbox'])
                                {
                                    $out .= '<h3 class="post-title"><a href="'.get_permalink().'">'.$post->post_title.'</a></h3>';
                                }
                                
                                if((bool)$portfolio_opt['po_show_desc_cbox'])
                                {     
                                    if($post->post_excerpt != '')
                                    {                  
                                        $out .= apply_filters('the_content', $post->post_excerpt);
                                        $out .= '<a href="'.get_permalink($post->ID).'">&raquo;&nbsp;'.__('Read&nbsp;more', CMS_TXT_DOMAIN).'</a>';     
                                    } else 
                                    {                                                      
                                        global $more;
                                        $more = 0;
                                        $out .= apply_filters('the_content', get_the_content(__('&raquo;&nbsp;Read&nbsp;more', 'dc_theme')));
                                    }
                                }
                                    
                            $out .= '</div>';
                        } // while
                        $out .= '<div class="clear-both"></div>';
                    $out .= '</div>';
                } else
                {
                    $out .= '<p class="theme-exception">There are no posts</p>'; 
                }       
                echo $out;                 
                   
            } else
            if($portfolio_opt['po_layout'] == CMS_PORTFOLIO_LAYOUT_TABLE4) 
            {
                $out = '';
                if(have_posts())
                {
                    $out .= '<div class="table4-portfolio">';
                        $counter = 0;
                        while(have_posts())
                        {
                            if($counter > 0 and ($counter % 4) == 0) { $out .= '<div class="separator"></div>'; }  
                                                    
                            $counter++;
                            the_post();      
                                                                           
                            $post_opt = get_post_meta($post->ID, 'post_opt', true);
                            $post_opt['image_url'] = dcf_isNGGImageID($post_opt['image_url']);
                            
                            $height = 225;
                            $style = '';
                            if((bool)$portfolio_opt['po_use_img_h_cbox'])
                            {
                                $style = ' style="height:'.(int)$portfolio_opt['po_img_h'].'px;" ';
                                $height = (int)$portfolio_opt['po_img_h'];
                            }
                            
                            $margin = '';
                            if(($counter % 4) != 0) { $margin = ' item-margin'; }                                                
                            $out .= '<div class="item'.$margin.'">';
                                $out .= '<div class="image-wrapper" '.$style.'>';
                                    $out .= '<a class="image async-img" rel="'.dcf_getTimThumbURL($post_opt['image_url'], 213, $height).'" '.$style.'></a>';
                                    if((bool)$portfolio_opt['po_img_as_link_cbox'])
                                    {
                                        $out .= '<a class="triger-link" href="'.get_permalink().'" '.$style.'></a>';
                                    } else
                                    {
                                        $out .= '<a class="triger" href="'.$post_opt['image_url'].'" rel="lightbox[portolio_'.$pageid.']" '.$style.'></a>'; 
                                    }
                                $out .= '</div>';
                                
                                if((bool)$portfolio_opt['po_show_imgdesc_cbox'])   
                                {                                  
                                    // post image description
                                    if($post_opt['image_desc'] != '')
                                    {
                                        $out .= '<div class="image-desc">'.$post_opt['image_desc'].'</div>';    
                                    } else
                                    {
                                        $out .= '<div class="image-desc-empty"></div>';     
                                    } 
                                }                                                        
                                
                                if((bool)$portfolio_opt['po_show_title_cbox'])
                                {
                                    $out .= '<h3 class="post-title"><a href="'.get_permalink().'">'.$post->post_title.'</a></h3>';
                                }
                                
                                if((bool)$portfolio_opt['po_show_desc_cbox'])
                                {              
                                    if($post->post_excerpt != '')
                                    {                  
                                        $out .= apply_filters('the_content', $post->post_excerpt);   
                                        $out .= '<a href="'.get_permalink($post->ID).'">&raquo;&nbsp;'.__('Read&nbsp;more', CMS_TXT_DOMAIN).'</a>';   
                                    } else 
                                    {                                             
                                        global $more;
                                        $more = 0;
                                        $out .= apply_filters('the_content', get_the_content(__('&raquo;&nbsp;Read&nbsp;more', 'dc_theme')));
                                    }
                                }
                                    
                            $out .= '</div>';
                        } // while
                        $out .= '<div class="clear-both"></div>';
                    $out .= '</div>';                    
                } else
                {
                    $out .= '<p class="theme-exception">There are no posts</p>'; 
                }                 
                echo $out;      
            }
                          
            GetDCCPInterface()->getIGeneral()->renderSitePagination($paged, $max_page);               
        ?>
                           
        </div>  <!-- page-width-xx0 -->
        <div class="clear-both"></div>
    </div> <!-- content -->
    
<?php    
    get_footer();
?>



