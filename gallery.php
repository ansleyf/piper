<?php
/*
Template Name: Gallery
*/

/**********************************************************************
* MODERN ELEGANCE WORDPRESS THEME 
* (Ideal For Business And Personal Use: Portfolio or Blog)   
* 
* File name:   
*      gallery.php
* Brief:       
*      Theme gallery page template code
* Author:      
*      DigitalCavalry
* Author URI:
*      http://themeforest.net/user/DigitalCavalry
* Contact:
*      digitalcavalry@gmail.com   
***********************************************************************/ 

    get_header();                                                
?>

    <div id="content">
         
        <?php 
            echo '<div class="page-width-full">';      
            dcf_naviTree($post->ID, 0);    

            echo '<h1>'.$post->post_title.'</h1>';
            the_content();
            
            $gallery_opt = get_post_meta($post->ID, 'gallery_opt', true);
            
            $per_page = (int)$gallery_opt['ga_per_page'];
            if($per_page < 0) { $per_page = 0; }         
            
            global $nggdb; 
            $max_page = 0;  
            $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                       
            if(isset($nggdb))
            {
                $gallery = false;
                $gallery = dcf_getGalleryNGG($gallery_opt['ga_gid'], 'sortorder', 'ASC', true, $per_page, (($paged-1)*$per_page), $max_page);
                
                
                $out = '';
                if(is_array($gallery))
                {
                    if($gallery_opt['ga_layout'] == CMS_GALLERY_LAYOUT_COMPACT)
                    {
                    
                        $out .= '<div class="compact-gallery">';
                                    
                            if((bool)$gallery_opt['ga_main_image_cbox'])
                            {
                                $gall_info = $nggdb->find_gallery($gallery_opt['ga_gid']);
                                $previewpic = dcf_getNGGImagesFromIDList($gall_info->previewpic);
                                if(count($previewpic) > 0)
                                {
                                    $out .= '<a class="main-image async-img" rel="'.dcf_getTimThumbURL($previewpic[0]->_imageURL, 940, 380).'"></a>';
                                    $out .= '<div class="main-image-desc">'.$previewpic[0]->_alttext.'</div>';    
                                }
                                $out .= '<div class="main-separator"></div>';
                            }
                            
                            $thumb_w = (int)$gallery_opt['ga_thumb_w'];
                            $thumb_h = (int)$gallery_opt['ga_thumb_h'];
                            $thumb_margin = (int)$gallery_opt['ga_thumb_margin'];
                            $thumb_padding = (int)$gallery_opt['ga_thumb_padding'];
                             
                            $style_thumb = ' style="width:'.$thumb_w.'px;height:'.$thumb_h.'px;margin:0px '.$thumb_margin.'px '.$thumb_margin.'px 0px;padding:'.$thumb_padding.'px;" ';
                            $style_image_triger = ' style="width:'.$thumb_w.'px;height:'.$thumb_h.'px;left:'.$thumb_padding.'px;top:'.$thumb_padding.'px;" '; 
                            
                            $tip = '';
                            if((bool)$gallery_opt['ga_show_tip_cbox'])
                            {
                                $tip .= ' tip-top';
                            }
                            
                            $out .= '<div class="compact-gallery-thumbs">';
                            
                            foreach($gallery as $pic)
                            {
                                $title = '';
                                if((bool)$gallery_opt['ga_show_tip_cbox'])
                                {
                                    $title .= ' title="'.$pic->_alttext.'" ';
                                }                                
                                
                                $out .= '<div class="thumb" '.$style_thumb.'>';
                                    $out .= '<a '.$style_image_triger.' class="image async-img-s" rel="'.dcf_getTimThumbURL($pic->_imageURL, $thumb_w, $thumb_h).'" /></a>';  
                                    $out .= '<a '.$style_image_triger.' href="'.$pic->_imageURL.'" class="triger'.$tip.'" rel="lightbox[cg_'.$post->ID.']" '.$title.'></a>';
                                $out .= '</div>';  
                                  
                            }
                        
                            $out .= '</div>';
                            
                            $out .= '<div class="clear-both"></div>';
                        $out .= '</div>';
                    } else
                    if($gallery_opt['ga_layout'] == CMS_GALLERY_LAYOUT_TABLE3)
                    {
                        $out .= '<div class="table-gallery">';
                            if((bool)$gallery_opt['ga_main_image_cbox'])
                            {
                                $gall_info = $nggdb->find_gallery($gallery_opt['ga_gid']);
                                $previewpic = dcf_getNGGImagesFromIDList($gall_info->previewpic);
                                if(count($previewpic) > 0)
                                {
                                    $out .= '<a class="main-image async-img" rel="'.dcf_getTimThumbURL($previewpic[0]->_imageURL, 940, 380).'"></a>';
                                    $out .= '<div class="main-image-desc">'.$previewpic[0]->_alttext.'</div>';    
                                }
                                $out .= '<div class="main-separator"></div>';
                            }    
                            
                            $tip = '';
                            if((bool)$gallery_opt['ga_show_tip_cbox'])
                            {
                                $tip .= ' tip-top';
                            }                            
                            
                            $counter = 0;
                            foreach($gallery as $pic)
                            {
                                $title = '';
                                if((bool)$gallery_opt['ga_show_tip_cbox'])
                                {
                                    $title .= ' title="'.$pic->_alttext.'" ';
                                }                                 
                                
                                $out .= '<div class="thumb320">';
                                    $out .= '<div class="top-content">';
                                        $out .= '<div class="image-wrapper">';
                                            $out .= '<a class="image async-img-s" rel="'.dcf_getTimThumbURL($pic->_imageURL, 270, 150).'" /></a>';
                                            $out .= '<a href="'.$pic->_imageURL.'" class="triger'.$tip.'" rel="lightbox[cg_'.$post->ID.']" '.$title.'></a>';
                                        $out .= '</div>';
                                    $out .= '</div>';
                                    
                                    $out .= '<div class="shadow"></div>';
                                    $out .= '<div class="bottom-content">';
                                        if((bool)$gallery_opt['ga_show_title_cbox'])
                                        {
                                            $out .= '<div class="title">'.stripcslashes($pic->_alttext).'</div>';
                                        }
                                        if((bool)$gallery_opt['ga_show_desc_cbox'])
                                        {
                                            $out .= '<span class="desc">'.stripcslashes($pic->_description).'</span>'; 
                                        }
                                    $out .= '</div>';                                    
                                                                
                                $out .= '</div>';
                                
                                $counter++;  
                                if($counter % 3 == 0)
                                {
                                   $out .= '<div class="clear-both"></div>'; 
                                }
                                  
                            }                                                                        
                        
                            $out .= '<div class="clear-both"></div>';
                        $out .= '</div>';     
                    } else
                    if($gallery_opt['ga_layout'] == CMS_GALLERY_LAYOUT_TABLE5)
                    {
                        $out .= '<div class="table-gallery">';
                            if((bool)$gallery_opt['ga_main_image_cbox'])
                            {
                                $gall_info = $nggdb->find_gallery($gallery_opt['ga_gid']);
                                $previewpic = dcf_getNGGImagesFromIDList($gall_info->previewpic);
                                if(count($previewpic) > 0)
                                {
                                    $out .= '<a class="main-image async-img" rel="'.dcf_getTimThumbURL($previewpic[0]->_imageURL, 940, 380).'"></a>';
                                    $out .= '<div class="main-image-desc">'.$previewpic[0]->_alttext.'</div>';    
                                }
                                $out .= '<div class="main-separator"></div>';
                            }    
                            
                            $tip = '';
                            if((bool)$gallery_opt['ga_show_tip_cbox'])
                            {
                                $tip .= ' tip-top';
                            }                                 
                            
                            $counter = 0;
                            foreach($gallery as $pic)
                            {
                                $title = '';
                                if((bool)$gallery_opt['ga_show_tip_cbox'])
                                {
                                    $title .= ' title="'.$pic->_alttext.'" ';
                                }                                   
                                
                                $out .= '<div class="thumb192">';
                                    $out .= '<div class="top-content">';
                                        $out .= '<div class="image-wrapper">';
                                            $out .= '<a class="image async-img-s" rel="'.dcf_getTimThumbURL($pic->_imageURL, 140, 140).'" /></a>';
                                            $out .= '<a href="'.$pic->_imageURL.'" class="triger'.$tip.'" rel="lightbox[cg_'.$post->ID.']" '.$title.'></a>';
                                        $out .= '</div>';
                                    $out .= '</div>';
                                    
                                    $out .= '<div class="shadow"></div>';
                                    $out .= '<div class="bottom-content">';
                                        if((bool)$gallery_opt['ga_show_title_cbox'])
                                        {
                                            $out .= '<div class="title">'.stripcslashes($pic->_alttext).'</div>';
                                        }
                                        if((bool)$gallery_opt['ga_show_desc_cbox'])
                                        {
                                            $out .= '<span class="desc">'.stripcslashes($pic->_description).'</span>'; 
                                        }
                                    $out .= '</div>';                                    
                                                                
                                $out .= '</div>';
                                
                                $counter++;  
                                if($counter % 5 == 0)
                                {
                                   $out .= '<div class="clear-both"></div>'; 
                                }
                                  
                            }                                                                        
                        
                            $out .= '<div class="clear-both"></div>';
                        $out .= '</div>';     
                    }                     
                }
                
                echo $out;
            }
            
            GetDCCPInterface()->getIGeneral()->renderSitePagination($paged, $max_page);                                        
        ?>
                           
        </div>  <!-- page-width-xx0 -->
        <div class="clear-both"></div>
    </div> <!-- content -->
    
<?php    
    get_footer();
?>



