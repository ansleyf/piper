/*******************************
*
*  Digital Cavalry Note
*
********************************/
Our theme uses __ and _e function for mark strings to translation.

default.po, default.mo - default translation files, use it as 
base for your translations work, of course you can generate your own .po files with poEdit application 

For example if you want create theme translation for German language, 
make copy of deafult.po and default.mo file and rename it to de_DE.po and de_DE.mo
Now you can open de_DE.po file in poEdit appliaction and translate 
porticular strings. The translation will work when you will 
switch Wordpress to German language.
