<?php
/*
Template Name: Sidebar Page
*/

/**********************************************************************
* MODERN ELEGANCE WORDPRESS THEME 
* (Ideal For Business And Personal Use: Portfolio or Blog)   
* 
* File name:   
*      sidebarpage.php
* Brief:       
*      Theme default page template code
* Author:      
*      DigitalCavalry
* Author URI:
*      http://themeforest.net/user/DigitalCavalry
* Contact:
*      digitalcavalry@gmail.com   
***********************************************************************/ 

    get_header();                                                
?>

    <div id="content">
         
        <?php 
            $page_common_opt = $GLOBALS['dc_pagecommon_opt'];            
            GetDCCPInterface()->getIGeneral()->includeSidebar($page_common_opt['page_sid'], $page_common_opt['page_sid_pos']);
            
            if(GetDCCPInterface()->getIGeneral()->getSidebarGlobalPos($page_common_opt['page_sid_pos']) == CMS_SIDEBAR_RIGHT)
            {
                echo '<div class="page-width-left">';                              
            } else
            {
                echo '<div class="page-width-right">';     
            }   
            dcf_naviTree($post->ID, 0);   

            echo '<h1>'.$post->post_title.'</h1>';
            the_content();
            GetDCCPInterface()->getIGeneral()->renderWordPressPagination();
                
            if('open' == $post->comment_status)
            {
                echo '<a name="comments"></a>';
                comments_template();
            }                           
        ?>
                           
        </div>  <!-- page-width-xx0 -->
        <div class="clear-both"></div>
    </div> <!-- content -->
    
<?php    
    get_footer();
?>



