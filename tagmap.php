<?php
/*
Template Name: Tag map
*/

/**********************************************************************
* MODERN ELEGANCE WORDPRESS THEME 
* (Ideal For Business And Personal Use: Portfolio or Blog)   
* 
* File name:   
*      tagmap.php
* Brief:       
*      Theme tag map page template code
* Author:      
*      DigitalCavalry
* Author URI:
*      http://themeforest.net/user/DigitalCavalry
* Contact:
*      digitalcavalry@gmail.com   
***********************************************************************/ 

    get_header();                                                
?>

    <div id="content">
         
        <?php 
            $page_common_opt = $GLOBALS['dc_pagecommon_opt'];            
            GetDCCPInterface()->getIGeneral()->includeSidebar($page_common_opt['page_sid'], $page_common_opt['page_sid_pos']);
            
            if(GetDCCPInterface()->getIGeneral()->getSidebarGlobalPos($page_common_opt['page_sid_pos']) == CMS_SIDEBAR_RIGHT)
            {
                echo '<div class="page-width-left">';                              
            } else
            {
                echo '<div class="page-width-right">';     
            }   
            dcf_naviTree($post->ID, 0);   

            echo '<h1>'.$post->post_title.'</h1>';
            the_content();
            
            $ORDER_BY_NAME = true;
            $ttag = get_terms('post_tag', 'orderby=count&order=DESC');
            if(is_array($ttag))
            {
                $count = count($ttag);
                if($count)
                {
                    $out = '';
                    $max_tag_count = $ttag[0]->count;
                    
                    if($ORDER_BY_NAME)
                    {
                        $ttag = get_terms('post_tag', 'orderby=name&order=ASC'); 
                    }
                
                    $out .= '<ol class="tag-bar-page">';
                    foreach($ttag as $t)
                    {
                        $width = ceil(($t->count / $max_tag_count)*100); 
                        $out .= '<li class="tip-top" title="'.$width.'%">';
                            
                            $out .= '<a class="link" href="'.get_term_link($t->slug, 'post_tag').'"></a>';
                            $out .= '<span class="strip" style="width:'.$width.'%;"></span>';
                            $out .= '<span class="name">'.$t->name.'</span>';
                            $out .= '<span class="count">'.$t->count.'</span>';    
                        $out .= '</li>';    
                    }
                    $out .= '</ol>';
                    
                    echo $out;
                }
                
                
            }
            
            //var_dump($ttag);
                                       
        ?>
                           
        </div>  <!-- page-width-xx0 -->
        <div class="clear-both"></div>
    </div> <!-- content -->
    
<?php    
    get_footer();
?>



