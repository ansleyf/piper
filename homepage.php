<?php
/*
Template Name: Homepage
*/

/**********************************************************************
* MODERN ELEGANCE WORDPRESS THEME 
* (Ideal For Business And Personal Use: Portfolio or Blog)   
* 
* File name:   
*      homepage.php
* Brief:       
*      Theme homepage page template code
* Author:      
*      DigitalCavalry
* Author URI:
*      http://themeforest.net/user/DigitalCavalry
* Contact:
*      digitalcavalry@gmail.com   
***********************************************************************/ 

    get_header();                                                
?>

    <div id="content">
        <?php             
            GetDCCPInterface()->getIGeneral()->renderHomepageSlider();
            
            GetDCCPInterface()->getIRenderer()->renderHomeBlogPosts(CPHome::POSTS_UNDER_SLIDER);
            
            //GetDCCPInterface()->getIQuickNewsPanel()->renderPanel();                         
            //GetDCCPInterface()->getIRenderer()->renderHomepageTabs(); 
            GetDCCPInterface()->getIHome()->getHomepageContent(CPHome::CONTENT_ON_FULLWIDTH);
            
            ?> <div class="clear-both" style="height:10px;"></div> <?php
        
            if(GetDCCPInterface()->getIHome()->isHomepageColumnAndSidShowed())
            {   
                $page_common_opt = $GLOBALS['dc_pagecommon_opt'];            
                GetDCCPInterface()->getIGeneral()->includeSidebar($page_common_opt['page_sid'], $page_common_opt['page_sid_pos']);
                
                if(GetDCCPInterface()->getIGeneral()->getSidebarGlobalPos($page_common_opt['page_sid_pos']) == CMS_SIDEBAR_RIGHT)
                {
                    echo '<div class="page-width-left">';                              
                } else
                {
                    echo '<div class="page-width-right">';     
                }
                
               // GetDCCPInterface()->getIRenderer()->renderHomepageTabs(); 
                
                GetDCCPInterface()->getIHome()->renderHomepageSections();            
//                   GetDCCPInterface()->getIHome()->getHomepageContent(CPHome::CONTENT_ON_COLUMN);                    
//                   GetDCCPInterface()->getIRenderer()->renderHomeFeaturedVideo();
//                   GetDCCPInterface()->getIrenderer()->renderHomeFeaturedGallery();
//                   
//                   GetDCCPInterface()->getIRenderer()->renderHomeBlogPosts();
//                   GetDCCPInterface()->GetIHome()->renderFeaturedStory(); 
//                   GetDCCPInterface()->getIRenderer()->renderHomeMainStreamPosts();            
                echo '</div>  <!-- page-width-x -->';
            }
        ?>
        <div class="clear-both"></div>
        <?php GetDCCPInterface()->getIRenderer()->renderHomeBlogPosts(CPHome::POSTS_UNDER_CONTENT); ?>
    </div> <!-- content -->
    
<?php    
    get_footer();
?>



