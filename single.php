<?php
/**********************************************************************
* MODERN ELEGANCE WORDPRESS THEME  
* (Ideal For Business And Personal Use: Portfolio or Blog)   
* 
* File name:   
*      single.php
* Brief:       
*      Theme single page code
* Author:      
*      DigitalCavalry
* Author URI:
*      http://themeforest.net/user/DigitalCavalry
* Contact:
*      digitalcavalry@gmail.com   
***********************************************************************/ 

    get_header();        
    
    $catlist = wp_get_post_categories($post->ID);
    $catlist = wp_get_post_terms($post->ID, 'category');
    $count_cat = count($catlist);
    
    $extname = array();
    if($count_cat > 0)
    {       
       foreach($catlist as $cat)
       {
           $extname[$cat->name] = get_category_link($cat->term_id);
       } 
    }   
    
    if(!isset($_SESSION[('pv'.$post->ID)])) { $_SESSION[('pv'.$post->ID)] = false; } 
    if(isset($_SESSION[('pv'.$post->ID)]) and $_SESSION[('pv'.$post->ID)] == false)
    {   
        $post_visits = get_post_meta($post->ID, 'post_visits', true);
        if($post_visits == '') { $post_visits = 0; } else { $post_visits++; }
        update_post_meta($post->ID, 'post_visits', (int)$post_visits);
        $_SESSION[('pv'.$post->ID)] = true; 
    }                                                 
?>

    <div id="content">
         
        <?php 
                                                          
        
            $post_common_opt = $GLOBALS['dc_postcommon_opt'];            
            $post_custom_content = (bool)$post_common_opt['post_custom_cbox'];
            $post_fullwidth = (bool)$post_common_opt['post_fullwidth_cbox'];
            
            if($post_custom_content and $post_fullwidth)
            {
                echo '<div class="page-width-full">';        
            } else
            {
                GetDCCPInterface()->getIGeneral()->includeSidebar($post_common_opt['page_sid'], $post_common_opt['page_sid_pos']);
                   
                if(GetDCCPInterface()->getIGeneral()->getSidebarGlobalPos($post_common_opt['page_sid_pos']) == CMS_SIDEBAR_RIGHT)
                {
                    echo '<div class="page-width-left">';                              
                } else
                {
                    echo '<div class="page-width-right">';     
                }
            }      
            dcf_naviTree($post->ID, 0, '', $extname);         
            
            if($post_custom_content)
            {
                the_content();
                GetDCCPInterface()->getIGeneral()->renderWordPressPagination();     
            } else
            {           
                GetDCCPInterface()->getIRenderer()->renderBlogPostFull();
            }
            
            if('open' == $post->comment_status)
	    {
                echo '<a name="comments"></a>';
                comments_template();
            }
        ?>
                           
        </div>  <!-- page-width -->
        <div class="clear-both"></div>
    </div> <!-- content -->
    
<?php    
    get_footer();
?>