<?php
/**********************************************************************
* MODERN ELEGANCE WORDPRESS THEME  
* (Ideal For Business And Personal Use: Portfolio or Blog)   
* 
* File name:   
*      tag.php
* Brief:       
*      Theme single tag page code
* Author:      
*      DigitalCavalry
* Author URI:
*      http://themeforest.net/user/DigitalCavalry
* Contact:
*      digitalcavalry@gmail.com   
***********************************************************************/ 

    get_header();     
                                                      
    $tagname = (get_query_var('tag')) ? get_query_var('tag') : '';
    $tag_id = (get_query_var('tag_id')) ? get_query_var('tag_id') : '';
    $tag = get_tag($tag_id);                                         
?>

    <div id="content">
    
        <?php 
            GetDCCPInterface()->getIGeneral()->includeSidebar(GetDCCPInterface()->getIGeneral()->getSidebarForBlogCat(), CMS_SIDEBAR_RIGHT); 
            
            echo '<div class="page-width-left">';                   
            dcf_naviTree($post->ID, 0, $tag->name); 
             
            echo '<h1>'.$tag->name.'</h1>';
            if($tag->description !== '')
            {
                echo '<div class="page-description">'.apply_filters('the_content', $tag->description).'</div>';  
            }            
                  
            $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
            $args=array('paged'=>$paged,'tag'=>$tagname);           
            
            query_posts($args);                         
            $max_page = $wp_query->max_num_pages;

                             
            GetDCCPInterface()->getIRenderer()->loopBlogPosts();                      
            GetDCCPInterface()->getIGeneral()->renderSitePagination($paged, $max_page);             
                               
        ?>                    
        </div>  <!-- page-width-->
        <div class="clear-both"></div>
    </div> <!-- content -->
    
<?php    
    get_footer();
?>



