<?php
/**********************************************************************
* MODERN ELEGANCE WORDPRESS THEME  
* (Ideal For Business And Personal Use: Portfolio or Blog)   
* 
* File name:   
*      category.php
* Brief:       
*      Theme single category page code
* Author:      
*      DigitalCavalry
* Author URI:
*      http://themeforest.net/user/DigitalCavalry
* Contact:
*      digitalcavalry@gmail.com   
***********************************************************************/ 

    get_header();     
    $dccp = GetDCCPInterface();

    $categoryid = (get_query_var('cat')) ? get_query_var('cat') : '';
    $category = get_category($categoryid);                                       
?>
    <div id="content">
    
        <?php 
            
            GetDCCPInterface()->getIGeneral()->includeSidebar(GetDCCPInterface()->getIGeneral()->getSidebarForBlogCat(), CMS_SIDEBAR_RIGHT); 
            
            echo '<div class="page-width-left">';       
            dcf_naviTree($post->ID, 0, $category->name);                       
                      
            echo '<h1>'.$category->name.'</h1>';
            
            if($category->description !== '')
            {              
                echo '<div class="page-description">'.apply_filters('the_content', $category->description).'</div>';  
            }
                                                                     
            $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
            $args=array('paged'=>$paged,'cat'=>$categoryid);         
            
            query_posts($args);                         
            $max_page = $wp_query->max_num_pages;                                                                  
                             
            GetDCCPInterface()->getIRenderer()->loopBlogPosts();                        
            GetDCCPInterface()->getIGeneral()->renderSitePagination($paged, $max_page);             
                               
        ?>                    
        </div>  <!-- page-width -->
        <div class="clear-both"></div>
    </div> <!-- content -->
    
<?php    
    get_footer();
?>



