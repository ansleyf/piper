<?php
/**********************************************************************
* MODERN ELEGANCE WORDPRESS THEME  
* (Ideal For Business And Personal Use: Portfolio or Blog)     
* 
* File name:   
*      cp_floatingobjects.php
* Brief:       
*      Part of theme control panel.
* Author:      
*      DigitalCavalry
* Author URI:
*      http://themeforest.net/user/DigitalCavalry
* Contact:
*      digitalcavalry@gmail.com 
***********************************************************************/

/*********************************************************** 
* Definitions
************************************************************/

/*********************************************************** 
* Class name:
*    CPFloatingObj
* Descripton:
*    Implementation of CPFloatingObj 
***********************************************************/
class CPFloatingObj extends DCC_CPBaseClass 
{
    const TARGET_BLANK = 1;
    const TARGET_SELF = 2;
    
    /*********************************************************** 
    * Constructor
    ************************************************************/
    public function __construct() 
    {
        $this->_width = 0;
        $this->_height = 0;
        $this->_padding = 0;
        
        $this->_left = 0;
        $this->_right = 0;
        $this->_bottom = 0;
        $this->_top = 0;
        
        $this->_usebottom = false;
        $this->_useright = false;
        $this->_hide = 0;  
        $this->_container = null; 
        
        $this->_image = '';
        $this->_imagehover = '';
        $this->_useimagehover = false;
        $this->_fadein = false;
        
        $this->_onpage = CMS_NOT_SELECTED;
        
        $this->_link = '';
        $this->_uselink = true;
        $this->_page = CMS_NOT_SELECTED;
        $this->_target = CPFloatingObj::TARGET_BLANK;
        $this->_usepage = false;
        $this->_bgcolor = 'EEEEEE';
        $this->_usebgcolor = false;
        
        $this->_text = '';
        $this->_usetext = false;
        $this->_zindex = 1;
        $this->_fontsize = 12;
        $this->_normaltext = false;  
        
        $this->_color = '000000';
        $this->_usecolor = false;
        
        $this->_border_width = 1;
        $this->_border_color = '000000';
        $this->_border_use = false; 
        $this->_closebtn = false;       
    }
    
    public $_width;
    public $_height;
    public $_padding;
    
    public $_onpage;
    
    public $_left;
    public $_top;
    public $_bottom;
    public $_right;
    
    public $_useright;
    public $_usebottom;    
    public $_hide; 
    public $_fadein;
    
    public $_container;
    public $_image;
    public $_imagehover;
    public $_useimagehover;
    
    public $_link;
    public $_uselink;
    public $_target;
    public $_page;
    public $_usepage;    
    
    public $_bgcolor;
    public $_usebgcolor; 
    
    public $_text;
    public $_usetext;
    public $_fontsize;
    public $_normaltext;
    public $_color;
    public $_usecolor;
    
    public $_zindex;
    public $_border_width;
    public $_border_color;
    public $_border_use;
    
    public $_closebtn;
    
}


/*********************************************************** 
* Class name:
*    CPFloatingObjects
* Descripton:
*    Implementation of CPFloatingObjects 
***********************************************************/
class CPFloatingObjects extends DCC_CPBaseClass 
{    
    /*********************************************************** 
    * Constructor
    ************************************************************/
    public function __construct() 
    {
        $this->_DBIDOPT_FLOATING_OBJECTS = CMS_THEME_NAME_UPPERCASE.'_FLOATING_OBJS_OPT';
        
        // floating objects
        $this->_objs = get_option($this->_DBIDOPT_FLOATING_OBJECTS);
        if (!is_array($this->_objs))
        {
            add_option($this->_DBIDOPT_FLOATING_OBJECTS, Array());
            $this->_objs = get_option($this->_DBIDOPT_FLOATING_OBJECTS);
        }           
        
    } // constructor 

    /*********************************************************** 
    * Public members
    ************************************************************/      
    
    /*********************************************************** 
    * Private members
    ************************************************************/      
     private $_DBIDOPT_FLOATING_OBJECTS = null;
     
     private $_objs;
     private $_saved = false;
     
     // here you need save blocks class or id where objects will be placed
     private $_containers = Array(
        'content-top-empty-space' => 'Top space',
        'header-container' => 'Header space',
        'content-bottom-empty-space' => 'Bottom space',
        'dc-splash-screen' => 'Splash screen',
        'announcement-bar' => 'Announcement bar',
        'browser-window' => 'Browser window',
        'page-post-custom-area' => 'Page custom content area'
     );
   
    /*********************************************************** 
    * Public functions
    ************************************************************/               
 
     public function renderTab()
     {
        echo '<div class="cms-content-wrapper">';
        $this->process();
        $this->renderCMS();
        echo '</div>';
     }
     
     public function renderObjects($name, $echo = true)
     {
        $out = '';
        foreach($this->_objs as $o)
        {
            if($o->_hide) continue;
            if($o->_container != $name) continue;
            
            if($o->_onpage != CMS_NOT_SELECTED)
            {
                if($GLOBALS['dc_post_id'] != $o->_onpage) continue;
            }
            
            if($o->_usetext)
            {
                $style = ' style="';
                if($name == 'browser-window') { $style .= 'position:fixed;'; } else  { $style .= 'position:absolute;'; }
                if($o->_useright) { $style .= 'right:'.$o->_right.'px;'; } else { $style .= 'left:'.$o->_left.'px;'; }
                if($o->_usebottom) { $style .= 'bottom:'.$o->_bottom.'px;'; } else { $style .= 'top:'.$o->_top.'px;'; } 
                if($o->_usecolor) { $style .= 'color:'.$o->_color.';'; }
                $style .= 'font-size:'.$o->_fontsize.'px;line-height:'.$o->_fontsize.'px;';
                $style .= '" ';
                
                if($o->_normaltext)
                {
                    $out .= '<span '.$style;             
                    $out .= ' >';
                    $out .= $o->_text;
                    $out .= '</span>';                    
                } else
                {            
                    $out .= '<a '.$style;
                    
                    if(($o->_link != '' or ($o->_page != CMS_NOT_SELECTED and $o->_usepage)) and $o->_uselink)
                    {
                        $target = '';
                        if($o->_target == CPFloatingObj::TARGET_BLANK) { $target = '_blank'; } else { $target = '_self';  } 
                        $out .= ' target="'.$target.'" '; 
                        
                        if($o->_usepage)
                        {
                            $out .= ' href="'.get_permalink($o->_page).'" ';     
                        } else
                        {
                            $out .= ' href="'.$o->_link.'" ';
                        }
                    }
                    
                    $out .= ' >';
                    $out .= $o->_text;
                    $out .= '</a>';
                  
                }
                
                  
            } else
            {
                $style = ' style="';
                if($name == 'browser-window') { $style .= 'position:fixed;'; }
                $style .= 'width:'.$o->_width.'px;height:'.$o->_height.'px;padding:'.$o->_padding.'px;';
                if($o->_useright) { $style .= 'right:'.$o->_right.'px;'; } else { $style .= 'left:'.$o->_left.'px;'; }
                if($o->_usebottom) { $style .= 'bottom:'.$o->_bottom.'px;'; } else { $style .= 'top:'.$o->_top.'px;'; }
                if($o->_usebgcolor) { $style .= 'background-color:'.$o->_bgcolor.';'; }
                if($o->_border_use) { $style .= 'border:'.$o->_border_width.'px solid '.$o->_border_color.';'; }
                $style .= '" '; 
                
                $class = ' class="fobject';
                if($o->_fadein) { $class .= ' fade-in'; }
                $class .= '" ';
                
                $out .= '<a '.$class.' '.$style;                
            
                if(($o->_link != '' or ($o->_page != CMS_NOT_SELECTED and $o->_usepage)) and $o->_uselink)
                {
                    $target = '';
                    if($o->_target == CPFloatingObj::TARGET_BLANK) { $target = '_blank'; } else { $target = '_self';  } 
                    $out .= ' target="'.$target.'" '; 
                    
                    if($o->_usepage)
                    {
                        $out .= ' href="'.get_permalink($o->_page).'" ';     
                    } else
                    {
                        $out .= ' href="'.$o->_link.'" ';
                    }
                }                
                
                $out .= ' >';
                
                $out .= '<img style="top:'.$o->_padding.'px;left:'.$o->_padding.'px;" class="image" src="'.$o->_image.'" />';
                                               
                if($o->_useimagehover)
                {
                     $out .= '<img style="top:'.$o->_padding.'px;left:'.$o->_padding.'px;" class="image-hover" src="'.$o->_imagehover.'" />';
                }                
                
                    if($o->_closebtn)
                    {
                        $out .= '<span class="close-btn"></span>';
                    }
                $out .= '</a>';                
            }
        }    
        
        if($echo)
        {
            echo $out;
        } else
        {
            return $out;
        }
     }
 
    /*********************************************************** 
    * Private functions
    ************************************************************/      
    
    private function process()
    {
        if(isset($_POST['add_floating_obj']))
        {       
            $new = new CPFloatingObj();
            $this->_home['home_extra_content'] = $_POST['home_extra_content'];
            array_push($this->_objs, $new);
              
            update_option($this->_DBIDOPT_FLOATING_OBJECTS, $this->_objs);
            $this->_saved = true;   
        }   

        if(isset($_POST['delete_floating_obj']))
        {       
            $index = $_POST['index'];
            unset($this->_objs[$index]);
            $this->_objs = array_values($this->_objs);
            
            update_option($this->_DBIDOPT_FLOATING_OBJECTS, $this->_objs);
            $this->_saved = true;      
        }           

        if(isset($_POST['save_floating_obj']))
        {       
            $index = $_POST['index'];

            $this->_objs[$index]->_width = (int)$_POST['width'];
            $this->_objs[$index]->_height = (int)$_POST['height'];
            $this->_objs[$index]->_padding = (int)$_POST['padding']; 
            
            $this->_objs[$index]->_image = $_POST['image'];
            $this->_objs[$index]->_imagehover = $_POST['imagehover'];
            $this->_objs[$index]->_bgcolor = $_POST['bgcolor'];  
            $this->_objs[$index]->_color = $_POST['color'];
            $this->_objs[$index]->_text = $_POST['text'];
            $this->_objs[$index]->_fontsize = (int)$_POST['fontsize'];
            
            $this->_objs[$index]->_link  = $_POST['link'];
            $this->_objs[$index]->_page = $_POST['page'];
            $this->_objs[$index]->_target = $_POST['target'];
            
            $this->_objs[$index]->_onpage = $_POST['onpage'];
            
            $this->_objs[$index]->_left = (int)$_POST['left'];
            $this->_objs[$index]->_right = (int)$_POST['right'];
            $this->_objs[$index]->_top = (int)$_POST['top'];
            $this->_objs[$index]->_bottom = (int)$_POST['bottom'];
            
            $this->_objs[$index]->_useright = isset($_POST['useright']) ? true : false; 
            $this->_objs[$index]->_uselink = isset($_POST['uselink']) ? true : false;
            $this->_objs[$index]->_usebottom = isset($_POST['usebottom']) ? true : false; 
            $this->_objs[$index]->_hide = isset($_POST['hide']) ? true : false; 
            $this->_objs[$index]->_usepage = isset($_POST['usepage']) ? true : false;
            $this->_objs[$index]->_usebgcolor = isset($_POST['usebgcolor']) ? true : false; 
            $this->_objs[$index]->_usecolor = isset($_POST['usecolor']) ? true : false; 
            $this->_objs[$index]->_usetext = isset($_POST['usetext']) ? true : false;
            $this->_objs[$index]->_normaltext = isset($_POST['normaltext']) ? true : false; 
            $this->_objs[$index]->_useimagehover = isset($_POST['useimagehover']) ? true : false; 
            $this->_objs[$index]->_fadein = isset($_POST['fadein']) ? true : false; 
            $this->_objs[$index]->_border_use = isset($_POST['border_use']) ? true : false; 
            $this->_objs[$index]->_closebtn = isset($_POST['closebtn']) ? true : false;
            
            $this->_objs[$index]->_container = $_POST['container'];
            
            $this->_objs[$index]->_zindex = (int)$_POST['zindex'];
            $this->_objs[$index]->_border_width = (int)$_POST['border_width'];
            $this->_objs[$index]->_border_color = $_POST['border_color'];            
            
            update_option($this->_DBIDOPT_FLOATING_OBJECTS, $this->_objs);
            $this->_saved = true;      
        }                     
    }

    private function renderCMS()
    {
        if($this->_saved)
        {                    
            echo '<span class="cms-saved-bar">Settings saved</span>';            
        }         
        
        $this->renderCoreCMS();     
        $this->renderObjectsCMS();     
    }
    
    private function renderCoreCMS()
    {
         $count = count($this->_objs);
         $visible = 0;
         $hidden = 0;
         foreach($this->_objs as $o)
         {
            if($o->_hide)
            {
                $hidden++;    
            } else
            {
                $visible++;    
            }   
         }
                                              
         # core
         $out = '';
         $out .= '<a name="fobjects_core"></a>';         
         $out .= '<h6 class="cms-h6">Floating Objects Core Settings</h6><hr class="cms-hr"/>';
         $out .= '<form action="#fobjects_core" method="post" >';                        
         $out .= '<input class="cms-submit" type="submit" value="Add new floating object" name="add_floating_obj" /><br /><br />';
         $out .= '<h6 class="cms-h6s">Actual status:</h6>';
         $out .= '<span class="cms-span-10">Objects:'.$count.'</span><br />';         
         $out .= '<span class="cms-span-10">Visible:'.$visible.'</span><br />'; 
         $out .= '<span class="cms-span-10">Hidden:'.$hidden.'</span>'; 
         $out .= '</form>';
         
         echo $out;           
    }  
    
    private function renderObjectsCMS()
    {
         $count = count($this->_objs);
         if($count == 0) return;
        
         $out = '';
         $out .= '<div style="height:30px;"></div>';
         echo $out;
         for($i = 0; $i < $count; $i++)
         {   
             $o = & $this->_objs[$i];
             $style60 = ' style="width:60px;text-align:center;" ';
             $style70 = ' style="width:70px;text-align:center;" ';
             
             $out = '';
             $out .= '<a name="floating_obj'.$i.'"></a>';              
             $out .= '<h6 class="cms-h6">Object Nr: '.($i+1).'</h6><hr class="cms-hr"/>';
             $out .= '<form action="#floating_obj'.$i.'" method="post" >';
                           
             $out .= '<div style="float:left;width:250px;margin-right:10px;">';
             $out .= '<span class="cms-span-10">Width and height in pixels:</span><br />';
             $out .= '<input '.$style60.' type="text" name="width" value="'.$o->_width.'" /> Width<br />';
             $out .= '<input '.$style60.' type="text" name="height" value="'.$o->_height.'" /> Height <br />';
             $out .= '<input '.$style60.' type="text" name="padding" value="'.$o->_padding.'" /> Padding <br /><br />';  
             
             $out .= '<span class="cms-span-10">Position in pixels:</span><br />';
             $out .= '<input '.$style60.' type="text" name="left" value="'.$o->_left.'" /> Left ';
             $out .= '<input '.$style60.' type="text" name="right" value="'.$o->_right.'" /> Right <br />';
             $out .= '<input type="checkbox" name="useright" '.$this->attrChecked($o->_useright).' /> Use right position <br /><br />'; 

             $out .= '<input '.$style60.' type="text" name="top" value="'.$o->_top.'" /> Top ';
             $out .= '<input '.$style60.' type="text" name="bottom" value="'.$o->_bottom.'" /> Bottom <br />';
             $out .= '<input type="checkbox" name="usebottom" '.$this->attrChecked($o->_usebottom).' /> Use bottom position <br /><br />'; 
             
             $out .= '<input '.$style60.' type="text" name="zindex" value="'.$o->_zindex.'" /> Z-Index<br /><br />';
             
             $out .= '<input type="checkbox" name="hide" '.$this->attrChecked($o->_hide).' /> Hide object <br />';
             $out .= '<input type="checkbox" name="closebtn" '.$this->attrChecked($o->_closebtn).' /> Show close button<br />';
             $out .= '<input type="checkbox" name="fadein" '.$this->attrChecked($o->_fadein).' /> Fade in on load (only with image)<br /><br />';
             
             $out .= '<span class="cms-span-10">Select object display place:</span><br />'; 
             $out .= '<select style="width:220px;" name="container">';
                foreach($this->_containers as $key => $value)
                {
                    $out .= '<option ';
                    $out .= 'value="'.$key.'"';
                    $out .= ($o->_container == $key) ? ' selected="selected" ' : '';
                    $out .= ' >';
                    $out .= $value;
                    $out .= '</option>';
                }
             $out .= '</select>';
             
                $out .= '<br /><br /><span class="cms-span-10">Text color:</span><br />'; 
                $out .= '<input style="width:70px;text-align:center;" type="text" class="colorpicker {hash:true}" value="'.$o->_color.'" name="color" /> Text Color <br />';   
                $out .= '<input type="checkbox" name="usecolor" '.$this->attrChecked($o->_usecolor).' /> Use <br />';              
             
             $out .= '</div>'; # first floated block
             
             $out .= '<div style="float:left;width:250px;margin-right:10px;">'; 
                $out .= '<span class="cms-span-10">Object manually link:</span><br />';  
                $out .= '<input style="width:200px;" type="text" name="link" value="'.$o->_link.'" /><br /><br />';
                
                $out .= '<span class="cms-span-10">Link target:</span><br />'; 
                $out .= '<input type="radio" value="'.CPFloatingObj::TARGET_BLANK.'" name="target" '.$this->attrChecked(CPFloatingObj::TARGET_BLANK == $o->_target).' /> Blank<br />';
                $out .= '<input type="radio" value="'.CPFloatingObj::TARGET_SELF.'" name="target" '.$this->attrChecked(CPFloatingObj::TARGET_SELF == $o->_target).' /> Self<br /><br />';
                
                $out .= '<span class="cms-span-10">Selcet page to link:</span><br />'; 
                $out .= '<input type="checkbox" name="usepage" '.$this->attrChecked($o->_usepage).' /> Use page <br /><br />';  
                $out .= $this->selectCtrlPagesList($o->_page, 'page', 220); 
                
                $out .= '<br /><br /><input type="checkbox" name="uselink" '.$this->attrChecked($o->_uselink).' /> Use link'; 
                
                $out .= '<br /><br /><span class="cms-span-10">Background color:</span><br />'; 
                $out .= '<input style="width:70px;text-align:center;" type="text" class="colorpicker {hash:true}" value="'.$o->_bgcolor.'" name="bgcolor" /> Color <br />';   
                $out .= '<input type="checkbox" name="usebgcolor" '.$this->attrChecked($o->_usebgcolor).' /> Use <br />'; 
                
                $out .= '<br /><span class="cms-span-10">Border:</span><br />';
                $out .= '<input style="width:70px;text-align:center;" type="text" class="colorpicker {hash:true}" value="'.$o->_border_color.'" name="border_color" /> Color <br />';  
                $out .= '<input '.$style70.' type="text" name="border_width" value="'.$o->_border_width.'" /> Border width (px)<br />'; 
                $out .= '<input type="checkbox" name="border_use" '.$this->attrChecked($o->_border_use).' /> Show border <br />';     
                
                $out .= '<br /><span class="cms-span-10">Display only on this page or leave not selected (default):</span><br />'; 
                $out .= $this->selectCtrlPagesList($o->_onpage, 'onpage', 220); 
                                                  
             $out .= '</div>'; # second floated block

             $out .= '<div style="float:left;width:310px;">'; 
                $out .= '<span class="cms-span-10">Object image:</span><br />'; 
                $bgcolor = ($o->_usebgcolor) ? 'background-color:#'.$o->_bgcolor.';' : '';   
                $out .= '<div class="cms-bg-center" style="width:300px;height:'.$o->_height.'px;background-image:url('.$o->_image.');'.$bgcolor.'"></div>'; 
                $out .= '<input style="width:300px;" id="image'.$i.'" type="text" name="image" value="'.$o->_image.'" /><br /><br />';
                
                $out .= '<span class="cms-span-10">Object hover image:</span><br />'; 
                $out .= '<div class="cms-bg-center" style="width:300px;height:'.$o->_height.'px;background-image:url('.$o->_imagehover.');'.$bgcolor.'"></div>';
                $out .= '<input style="width:300px;" id="imagehover'.$i.'" type="text" name="imagehover" value="'.$o->_imagehover.'" /><br /><br />';
                $out .= '<input type="checkbox" name="useimagehover" '.$this->attrChecked($o->_useimagehover).' /> Use hover image <br />'; 

                $out .= '<br /><span class="cms-span-10">Text:</span><br />';
                $out .= '<input style="width:220px;" type="text" name="text" value="'.$o->_text.'" /><br />';
                $out .= '<input type="checkbox" name="usetext" '.$this->attrChecked($o->_usetext).' /> Display text link instead image<br />';
                $out .= '<input type="checkbox" name="normaltext" '.$this->attrChecked($o->_normaltext).' /> Show as normal not linkable text<br /><br />';
                $out .= '<input '.$style60.' type="text" name="fontsize" value="'.$o->_fontsize.'" /> Font size in px<br />';                  


                
             $out .= '</div>'; # third floated block
             
             $out .= '<div style="clear:both;"></div>';
             
             $out .= '<div style="height:20px;"></div>';
             $out .= '<input type="hidden" name="index" value="'.$i.'" />';                        
             $out .= '<input class="cms-submit" type="submit" value="Save" name="save_floating_obj" /> ';
             $out .= '<input onclick="return confirm('."'Delete this object?'".')" class="cms-submit-delete" type="submit" value="Delete" name="delete_floating_obj" /> ';
             $out .= '<input class="cms-upload upload_image_button" type="button" value="Upload Image" name="image'.$i.'" /> ';  
             $out .= '<input class="cms-upload upload_image_button" type="button" value="Upload Hover Image" name="imagehover'.$i.'" />'; 
             $out .= '</form>';
             
             $out .= '<div style="height:30px;"></div>';
             echo $out;
         }        
    }    
         
} // class CPFloatingObjects
        
        
?>
