<?php
/**********************************************************************
* MODERN ELEGANCE WORDPRESS EDITION 
* (Ideal For Business And Personal Use: Portfolio or Blog)     
* 
* File name:   
*      cp_shortcodes.php
* Brief:       
*      Part of theme control panel.
* Author:      
*      DigitalCavalry
* Author URI:
*      http://themeforest.net/user/DigitalCavalry
* Contact:
*      digitalcavalry@gmail.com 
***********************************************************************/ 
   
class CPThemeShortCodes 
{
    /*********************************************************** 
    * Constructor
    ************************************************************/       
    public function __construct($theme_main_color)
    {                                                              
        $this->_theme_main_color = $theme_main_color;

        // common                           
        add_shortcode('dcs_head', array(&$this, 'dcs_head'));
        add_shortcode('dcs_btn', array(&$this, 'dcs_btn'));
        add_shortcode('dcs_btn_fullwidth', array(&$this, 'dcs_btn_fullwidth'));
       // add_shortcode('dcs_story', array(&$this, 'dcs_story')); 
        add_shortcode('dcs_hidden', array(&$this, 'dcs_hidden'));         
        add_shortcode('dcs_image', array(&$this, 'dcs_image'));
        add_shortcode('dcs_span', array(&$this, 'dcs_span'));
        add_shortcode('dcs_highlight', array(&$this, 'dcs_highlight'));                
        add_shortcode('dcs_top', array(&$this, 'dcs_top'));       
        add_shortcode('dcs_empty', array(&$this, 'dcs_empty'));    
        add_shortcode('dcs_clear', array(&$this, 'dcs_clear'));
        add_shortcode('dcs_fancy_header', array(&$this, 'dcs_fancy_header'));       
        add_shortcode('dcs_small_block', array(&$this, 'dcs_small_block'));              
        add_shortcode('dcs_lb_link', array(&$this, 'dcs_lb_link'));
        add_shortcode('dcs_lb_ngg', array(&$this, 'dcs_lb_ngg'));                 
        add_shortcode('dcs_blockquote', array(&$this, 'dcs_blockquote')); 
        add_shortcode('dcs_box', array(&$this, 'dcs_box'));
        add_shortcode('dcs_note', array(&$this, 'dcs_note'));  
        add_shortcode('dcs_ul_list', array(&$this, 'dcs_ul_list'));
        add_shortcode('dcs_postbox', array(&$this, 'dcs_postbox'));
        add_shortcode('dcs_pagebox', array(&$this, 'dcs_pagebox'));         
        // columns
        add_shortcode('dcs_one_half', array(&$this, 'dcs_one_half'));
        add_shortcode('dcs_one_half_last', array(&$this, 'dcs_one_half_last'));
        add_shortcode('dcs_one_third', array(&$this, 'dcs_one_third'));
        add_shortcode('dcs_one_third_last', array(&$this, 'dcs_one_third_last')); 
        add_shortcode('dcs_two_third', array(&$this, 'dcs_two_third'));
        add_shortcode('dcs_two_third_last', array(&$this, 'dcs_two_third_last'));            
        add_shortcode('dcs_one_fourth', array(&$this, 'dcs_one_fourth'));
        add_shortcode('dcs_one_fourth_last', array(&$this, 'dcs_one_fourth_last'));   
        add_shortcode('dcs_three_fourth', array(&$this, 'dcs_three_fourth'));
        add_shortcode('dcs_three_fourth_last', array(&$this, 'dcs_three_fourth_last'));
        add_shortcode('dcs_column', array(&$this, 'dcs_column'));          
        // advanced
        add_shortcode('dcs_chain_gallery', array(&$this, 'dcs_chain_gallery'));           
        add_shortcode('dcs_simple_gallery_ngg', array(&$this, 'dcs_simple_gallery_ngg'));
        add_shortcode('dcs_simple_gallery_thumbs_ngg', array(&$this, 'dcs_simple_gallery_thumbs_ngg'));  
        
        add_shortcode('dcs_ngg', array(&$this, 'dcs_ngg'));
        add_shortcode('dcs_ngg_single', array(&$this, 'dcs_ngg_single')); 
        add_shortcode('dcs_ngg_last', array(&$this, 'dcs_ngg_last'));    
        add_shortcode('dcs_ngg_random', array(&$this, 'dcs_ngg_random'));             
        
                                                                                                                                             
    }

    /*********************************************************** 
    * Provate memebers
    ************************************************************/
    private $_theme_main_color;
    
    /*********************************************************** 
    * Public functions
    ************************************************************/
    
    # SHORTCODE: 
    #   dcs_head 
    # PAREMAETERS:
    #   color - text color, can be set to any CSS valid value for color (default empty string, color will be not set)
    # NOTES:  
    public function dcs_head($atts, $content=null, $code="")
    {
        $out = '';
        $defatts = Array(
          'color' => '',
          'top' => 0
        );        
        $atts = shortcode_atts($defatts, $atts);        
        $att_color = $atts['color'];           
        $att_top = (int)$atts['top'];
        
        $style = '';
        
        if($att_color != '' or $att_top > 0)
        {
            $style = ' style="';
            if($att_color != '')
            {
                $style .= 'color:'.$att_color.';'; 
            }
            if($att_top > 0)
            {
                $style .= 'margin-top:'.$att_top.'px;'; 
            }         
            $style .= '" ';
        }
                                                             
        $out .= '<div class="common-block-head" '.$style.'><span class="head-text">'.$content.'</span></div>';

        return $out;
    }        

    # SHORTCODE: 
    #   dcs_btn 
    # PAREMAETERS:
    # NOTES:  
    public function dcs_btn($atts, $content=null, $code="")
    {
        $out = '';
        $defatts = Array(
          'name' => '',
          'url' => 0,
		  'color' => '#666666',
		  'hcolor' => '#000000'
        );        
        
        $atts = shortcode_atts($defatts, $atts);        
        $att_name = $atts['name'];           
        $att_url = $atts['url'];
		$att_color = $atts['color'];
		$att_hcolor = $atts['hcolor'];

		if($att_color != '')
        {
            $style = ' style="color:'.$att_color.';" ';
        }
                                                             
        $out .= '<a class="dc-btn-small" href="'.$att_url.'" '.$style.' onmouseover="this.style.color=\''.$att_hcolor.'\';" onmouseout="this.style.color=\''.$att_color.'\';">'.$att_name.'</a><div class="clear-both"></div>';
        return $out;
    }
    
    # SHORTCODE: 
    #   dcs_btn_fullwidth 
    # PAREMAETERS:
    # NOTES:  
    public function dcs_btn_fullwidth($atts, $content=null, $code="")
    {
        $out = '';
        $defatts = Array(
          'name' => '',
          'url' => 0            
        );        
        
        $atts = shortcode_atts($defatts, $atts);        
        $att_name = $atts['name'];           
        $att_url = $atts['url'];    
                                                             
        $out .= '<div class="dc-btn-fw-topline"><a class="dc-btn-fullwidth" href="'.$att_url.'">'.$att_name.'</a></div><div class="clear-both"></div>';
        return $out;
    }       

    # SHORTCODE: 
    #   dcs_story 
    # PAREMAETERS:
    # NOTES:  
 /*   public function dcs_story($atts, $content=null, $code="")
    {
        $out = '';
        $defatts = Array(
          'name' => '',  
          'w' => 620,
          'h' => 300,
          'list' => '',
          'link' => '',
          'overwrite' => 'false',
          'target' => '_self',
          'showtitle' => 'true',
          'showdesc' => 'true',
          'group' => 'false',
          'fit' => 'true', 
          'margin' => '0px auto 15px auto',
          'float' => 'none'           
        );        
        
        $atts = shortcode_atts($defatts, $atts);        
        $att_name = $atts['name'];
        $att_w = (int)$atts['w'];           
        $att_h = (int)$atts['h'];    
        $att_list = trim($atts['list']); 
        $att_link = $atts['link'];     
        $att_overwrite = $atts['overwrite'] == 'true' ? true : false;
        $att_showtitle = $atts['showtitle'] == 'true' ? true : false; 
        $att_showdesc = $atts['showdesc'] == 'true' ? true : false; 
        $att_group = $atts['group'] == 'true' ? true : false;
        $att_fit = $atts['fit'] == 'true' ? true : false;
        $att_target = $atts['target'];
        if($att_target != '_self' and $att_target != '_blank') { $att_target = '_self'; }           
        
        $att_float = $atts['float']; 
        if($att_float != 'left' and $att_float != 'right' and $att_float != 'none') { $att_float = 'none'; }          
        $att_margin = $atts['margin'];                          
                                                             
        $out = '';
       
        if($att_list != '')
        {            
            $slider_style = '';
            $slider_style .= ' style="width:'.$att_w.'px;';
                if($att_float != 'none') { $slider_style .= 'float:'.$att_float.';'; }
                if($att_margin != '0px auto 15px auto') { $slider_style .= 'margin:'.$att_margin.';'; } 
            $slider_style .= '" ';
            
            $slides_wrapper_style = ' style="width:'.$att_w.'px;height:'.$att_h.'px;" ';
            $slides_style = ' style="width:'.$att_w.'px;height:'.$att_h.'px;" '; 
            
            global $wpdb;
            $data = $wpdb->get_results("SELECT ID, post_title, post_content FROM $wpdb->posts WHERE ID IN($att_list) AND post_status = 'publish' AND post_type = '".CPThemeCustomPosts::PT_STORY_POST."' ORDER BY FIELD(ID, $att_list) ");
            $count = count($data);
            
            if($count)
            {    
                // generate_id
                $story_id = '';
                $counter = 0;
                foreach($data as $slide)
                {
                   $story_id .= (string)$slide->ID;
                   $counter++;
                   if($counter >= 5) break; 
                }
                
                $out .= '<div class="dc-story" '.$slider_style.'>';
                    if($att_name != '')
                    {   
                        $out .= '<div class="common-block-head">'.$att_name.'</div>'; 
                    }
                    $out .= '<div class="slides-wrapper" '.$slides_wrapper_style.'>';
                        
                        for($i = 0; $i < $count; $i++)
                        {
                            $slide_meta = get_post_meta($data[$i]->ID, 'storypost_opt', true);                            
                            
                            $image_path = '';
                            $image_w = 0;
                            $image_h = 0;
                            
                            $image = dcf_isNGGImageID($slide_meta['image_url'], true);
                            if(is_object($image))
                            {   
                                $image_path = $image->_imageURL;
                                $image_w = $image->_width;
                                $image_h = $image->_height;   
                            } else
                            {
                                $image_dir = strstr($slide_meta['image_url'], 'wp-content');
                                $image_dir = dcf_getWPMainDir().$image_dir;
                                $image_info = getimagesize($image_dir);

                                $image_path = $slide_meta['image_url'];
                                $image_w = (int)$image_info[0];
                                $image_h = (int)$image_info[1];                             
                            }

                            $rect = null;
                            if($att_fit)
                            {                            
                                $rect = dcf_imgCalcMaxSizeInRatio($att_w, $att_h, $image_w, $image_h);
                                
                                $left = floor($att_w / 2) - floor($rect->_width / 2);
                                if($left < 0) { $left = 0; }                            

                                $top = floor($att_h / 2) - floor($rect->_height / 2);
                                if($top < 0) { $top = 0; }                            
                            } else
                            {
                                $rect = new DCC_CRectangle($att_w, $att_h);    
                            }
                            
                            $group = '';
                            if($att_group)
                            {
                                $group = '[dcstory_'.$story_id.']';       
                            }
                            
                            $slide_image_style = ' style="left:'.$left.'px;top:'.$top.'px;width:'.$rect->_width.'px;height:'.$rect->_height.'px;" ';
                            
                            $out .= '<div class="slide" '.$slides_style.'>';
                                $out .= '<a class="slide-image async-img-s-black" '.$slide_image_style.' rel="'.dcf_getTimThumbURL($image_path, $rect->_width, $rect->_height).'"></a>';
                                $out .= '<a href="'.$image_path.'" class="triger" '.$slide_image_style.' rel="lightbox'.$group.'" title="'.$data[$i]->post_title.'"></a>';                                                   
                            $out .= '</div>';
                            
                            if($att_showtitle or $att_showdesc)
                            {                            
                                $out .= '<div class="description">';
                                    
                                    $slide_link = '';
                                    $slide_target = '';

                                    if((bool)$slide_meta['link_use_cbox'])
                                    {
                                        $slide_target = $slide_meta['link_target'];
                                        if($slide_meta['link_type'] == CMS_LINK_TYPE_PAGE)
                                       {
                                            $slide_link = get_permalink($slide_meta['link_page']);   
                                       } else
                                       {
                                            $slide_link = $slide_meta['link'];    
                                       }                                
                                    }
                                    
                                    if($att_overwrite and $att_link != '')
                                    {
                                        $slide_link = $att_link;
                                        $slide_target = $att_target;  
                                    }
                    
                                    if(!$att_overwrite and $slide_link == '')
                                    {
                                        $slide_link = $att_link;
                                        $slide_target = $att_target;  
                                    }                            
                                    
                                    if($att_showtitle)
                                    {
                                        if($slide_link != '')
                                        {
                                            $out .= '<a href="'.$slide_link.'" target="'.$slide_target.'" class="title">'.$data[$i]->post_title.'</a>';
                                        } else
                                        {
                                            $out .= '<span class="title">'.$data[$i]->post_title.'</span>';   
                                        }
                                    }
                                    
                                    if($att_showdesc)
                                    {
                                        $out .= '<div class="desc-content">'.$data[$i]->post_content.'</div>';
                                    }
                                $out .= '</div>'; 
                            }
                        }
                        
                    $out .= '</div>';
                    
                    $out .= '<div class="info-bar">';
                        $out .= '<div class="counter"><span class="active">1</span><span class="separator">/</span><span class="count">10</span></div>';
                        $out .= '<div class="next">'.__('Next', 'dc_theme').'</div>';
                        $out .= '<div class="prev">'.__('Prev', 'dc_theme').'</div>';
                    $out .= '</div>';
                    
                $out .= '</div>';
            }        
        }
        return $out;
    }   
  */  
    # SHORTCODE: 
    #   dcs_hidden 
    # PAREMAETERS:
    #   title - text for block title
    # NOTES:  
    public function dcs_hidden($atts, $content=null, $code="")
    {
        $out = '';
        $defatts = Array(
          'title' => '',
          'color' => '',
          'execute' => 'true',
          'margin' => '0px 0px 15px 0px'          
        );        
        $atts = shortcode_atts($defatts, $atts);        
        $att_title = $atts['title'];                   
        $att_color = $atts['color']; 
        $att_execute = $atts['execute'];
        $att_margin = $atts['margin'];
        
        $style = '';
        if($att_color != '')
        {
            $style .= ' style="';
            if($att_color != '')
            {
                $style .= 'color:'.$att_color.';';        
            }                   
            $style .= '" ';
        }
        
        $style_c = '';
        if($att_margin != '0px 0px 15px 0px')
        {
            $style_c .= ' style="';
            $style_c .= 'margin:'.$att_margin.';';                    
            $style_c .= '" ';
        }             
        
        if($att_execute == 'true')
        {
            $content = do_shortcode($content);
        }           
                                                             
        $out .= '<div class="hidden-content" '.$style_c.'><div class="head"><span class="open">'.$att_title.'</span></div><div class="data" '.$style.'>'.$content.'<div class="clear-both"></div></div></div>';

        return $out;
    }       
    
    
    # SHORTCODE: 
    #   dcs_image 
    # PAREMAETERS:
    #   color - text color, can be set to any CSS valid value for color (default empty string, color will be not set)
    # NOTES:  
    public function dcs_image($atts, $content=null, $code="")
    {
        $out = '';
        $defatts = Array(
          'src' => '',
          'align' => 'left',
          'w' => 100,
          'h' => 100,
          'mleft' => 0,
          'mright' => 0,
          'mtop' => 0,
          'mbottom' => 0,
          'desc' => '',
          'descalign' => 'left',
          'tt' => 'true', // timthumb
          'lb' => 'true', // show in lightbox
          'imglink' => 'false', // image as link
          'desclink' => 'false', // image as link
          'link' => '',
          'viewmore' => '',  
          'async' => 'true',
          'group' => '',
          'target' => '_self',
          'borderremove' => 'false',
          'paddingremove' => 'false'
        );        
        $atts = shortcode_atts($defatts, $atts);        
        $att_src = $atts['src'];           
        $att_align = $atts['align'];
        $att_w = (int)$atts['w']; 
        $att_h = (int)$atts['h'];
        $att_mleft = (int)$atts['mleft'];
        $att_mright = (int)$atts['mright'];
        $att_mtop = (int)$atts['mtop'];
        $att_mbottom = (int)$atts['mbottom'];
        $att_desc = $atts['desc'];
        $att_descalign = $atts['descalign'];
        $att_tt = $atts['tt'] == 'true' ? true : false; 
        $att_lb = $atts['lb'] == 'true' ? true : false;
        $att_imglink = $atts['imglink'] == 'true' ? true : false;
        $att_desclink = $atts['desclink'] == 'true' ? true : false; 
        $att_async = $atts['async'] == 'true' ? true : false;
        $att_group = $atts['group'];
        $att_link = $atts['link']; 
        $att_viewmore = $atts['viewmore'];        
        $att_target = $atts['target'];
        $att_borderremove = $atts['borderremove'] == 'true' ? true : false; 
        $att_paddingremove = $atts['paddingremove'] == 'true' ? true : false;
        
        if($att_align != 'left' and $att_align != 'right' and $att_align != 'center') { $att_align = 'left'; }
        if($att_descalign != 'left' and $att_descalign != 'right' and $att_descalign != 'center') { $att_descalign = 'left'; } 
        if($att_group != '') { $att_group = '['.$att_group.']'; }
        
        $container_w = $att_w.'px';
        if($att_align == 'center') { $container_w = 'auto'; }
        $image_url_orig = $att_src;        
        
        $remove_style = '';
        $remove_triger_style = '';
        if($att_borderremove) { $remove_style .= 'border:none;'; }
        if($att_paddingremove) { $remove_style .= 'padding:0px;'; $remove_triger_style = 'left:0px;top:0px;'; }
        
        
        
        if($att_async)
        {
             
            $out .= '<div style="width:'.$container_w.';margin:'.$att_mtop.'px '.$att_mright.'px '.$att_mbottom.'px '.$att_mleft.'px;" class="theme-image-'.$att_align.'">';
            
            if($att_tt)
            {
                $att_src = dcf_getTimThumbURL($att_src, $att_w, $att_h);
            }
            
            $out .= '<div style="width:'.$att_w.'px;height:'.$att_h.'px;'.$remove_style.'" class="img-wrapper">';
                $out .= '<a style="width:'.$att_w.'px;height:'.$att_h.'px;" class="loader async-img-s" rel="'.$att_src.'"></a>';
                if($att_imglink)
                {
                    $out .= '<a style="width:'.$att_w.'px;height:'.$att_h.'px;'.$remove_triger_style.'" href="'.$att_link.'" target="'.$att_target.'" class="triger-link"></a>';    
                } else
                if($att_lb)
                {
                    $out .= '<a style="width:'.$att_w.'px;height:'.$att_h.'px;'.$remove_triger_style.'" href="'.$image_url_orig.'" class="triger" rel="lightbox'.$att_group.'" title="'.$att_desc.'"></a>';    
                }
            $out .= '</div>';
            
            if($att_desc != '' or $att_desclink)
            {
                $out .= '<div style="text-align:'.$att_descalign.';width:'.$att_w.'px;" class="desc">';
                    $out .= $att_desc;
                    if($att_desclink)
                    {
                        $out .= ' <a href="'.$att_link.'" target="'.$att_target.'">&raquo;&nbsp;'.$att_viewmore.'</a>';
                    }
                $out .= '</div>';
            }
            $out .= '</div>';
        } else
        {
            $out .= '<div style="width:'.$container_w.';margin:'.$att_mtop.'px '.$att_mright.'px '.$att_mbottom.'px '.$att_mleft.'px;" class="theme-image-'.$att_align.'">';
            
            if($att_tt)
            {
                $att_src = dcf_getTimThumbURL($att_src, $att_w, $att_h);
            }
            
            $out .= '<div style="width:'.$att_w.'px;height:'.$att_h.'px;'.$remove_style.'" class="img-wrapper">';
                $out .= '<img style="width:'.$att_w.'px;height:'.$att_h.'px;" src="'.$att_src.'" />';
                if($att_imglink)
                {
                    $out .= '<a style="width:'.$att_w.'px;height:'.$att_h.'px;'.$remove_triger_style.'" href="'.$att_link.'" target="'.$att_target.'" class="triger-link"></a>';    
                } else                
                if($att_lb)
                {
                    $out .= '<a style="width:'.$att_w.'px;height:'.$att_h.'px;'.$remove_triger_style.'" href="'.$image_url_orig.'" class="triger" rel="lightbox'.$att_group.'"></a>';    
                }                
            $out .= '</div>'; 
            
            if($att_desc != '' or $att_desclink)
            {
                $out .= '<div style="text-align:'.$att_descalign.';width:'.$att_w.'px;" class="desc">';
                    $out .= $att_desc;
                    if($att_desclink)
                    {
                        $out .= ' <a href="'.$att_link.'" target="'.$att_target.'">&raquo;&nbsp;'.$att_viewmore.'</a>';
                    }
                $out .= '</div>';
            }
            $out .= '</div>';            
        }

        return $out;
    } 
    
    # SHORTCODE: 
    #   dcs_span 
    # PAREMAETERS:
    #   color - text color, can be set to any CSS valid value for color (default empty string, color will be not set)
    #   bgcolor - background color
    # NOTES:  
    public function dcs_span($atts, $content=null, $code="")
    {
        $out = '';
        $defatts = Array(
          'color' => '',
          'bgcolor' => ''
        );        
        $atts = shortcode_atts($defatts, $atts);        
        $att_color = $atts['color'];           
        $att_bgcolor = $atts['bgcolor'];
        
        $style = '';
        
        if($att_color != '' or $att_bgcolor != '')
        {
            $style = ' style="';
            if($att_color != '')
            {
                $style .= 'color:'.$att_color.';'; 
            }
            if($att_bgcolor != '')
            {
                $style .= 'background-color:'.$att_bgcolor.';'; 
            }         
            $style .= '" ';
        }
                                                             
        $out .= '<span '.$style.'>'.$content.'</span>';

        return $out;
    }     
    
    # SHORTCODE: 
    #   dcs_highlight 
    # PAREMAETERS:
    #   color - text color, can be set to any CSS valid value for color (default empty string, color will be not set)
    # NOTES:  
    public function dcs_highlight($atts, $content=null, $code="")
    {
        $out = '';
        $defatts = Array(
          'color' => ''
        );        
        $atts = shortcode_atts($defatts, $atts);        
        $att_color = $atts['color'];           
        
        $style = '';
        
        if($att_color != '' or $att_bgcolor != '')
        {
            $style .= ' style="';
            if($att_color != '')
            {
                $style .= 'color:'.$att_color.';'; 
            }
            $style .= '" ';
        }
                                                             
        $out .= '<span class="dc-highlight" '.$style.'>'.$content.'</span>';

        return $out;
    }     
    
    
    # SHORTCODE: 
    #   dcs_top
    # PAREMAETERS:
    #   empty - if true inner line and top link is not displayed, default false
    #   top - if false inner top link is not displayed, default true
    #   style - line style, can be set to solid, dotted or dashed, default solid
    # NOTES:     
    public function dcs_top($atts, $content=null, $code="")
    {
        $out = '';
        $defatts = Array(
            'top' => 0,
            'bottom' => 15
        ); 
        
        $atts = shortcode_atts($defatts, $atts);        
        $att_top = (int)$atts['top'];
        $att_bottom = (int)$atts['bottom'];
        if($att_top < 0) { $att_top = 0; }
        if($att_bottom < 0) { $att_bottom = 15; }
        
        
        $out .= '<div class="dc-top-anchor" style="margin-bottom:'.$att_bottom.'px;"><div class="line" style="margin-top:'.$att_top.'px;"><a href="#top-anchor">'.__('Top', 'dc_theme').'</a></div></div>'; 
        return $out;
    }          
     
    # SHORTCODE: 
    #   dcs_empty 
    # PAREMAETERS:
    #   h - in this paremeter put height of empty space in pixels, this should be value grater then zero
    # NOTES:
    #   This short code generate simple empty div with clear both style and given height, default 1px           
    public function dcs_empty($atts, $content=null, $code="")
    {
        $out = '';
        $defatts = Array(
          'h' => 1
        ); 
        
        $atts = shortcode_atts($defatts, $atts);        
        $att_h = (int)$atts['h'];
        if($att_h < 1)
        {
            $att_h = 1;    
        }             
        
        $out .= '<div style="clear:both;height:'.$att_h.'px;"></div>';
        return $out;
    }               

    # SHORTCODE: 
    #   dcs_clear 
    # PAREMAETERS:
    #   h - in this paremeter put height of empty space in pixels, this should be value grater then zero
    # NOTES:
    #   This short code generate simple empty div with clear both style and given height, default 1px           
    public function dcs_clear($atts, $content=null, $code="")
    {
        $out = '';
        $defatts = Array(
          'h' => 1
        ); 
        
        $atts = shortcode_atts($defatts, $atts);        
        $att_h = (int)$atts['h'];
        if($att_h < 1)
        {
            $att_h = 1;    
        }             
        
        $out .= '<div style="clear:both;height:'.$att_h.'px;"></div>';
        return $out;
    }     
    
    # SHORTCODE: 
    #   dcs_fancy_header 
    # PAREMAETERS:
    #   bgcolor - background color, can be set to any CSS valid value for background-color property (default #222222)
    #   color - text color, can be set to any CSS valid value for color property (default #EEEEEE)
    # NOTES:  
    public function dcs_fancy_header($atts, $content=null, $code="")
    {           
        $out = '';
        $defatts = Array(
          'color' => $this->_theme_main_color,
          'margin' => '0px 0px 10px 0px',
          'smargin' => '0px auto 0px auto', 
          'align' => 'left',
          'salign' => 'left',
          'width' => 0
        );        
        $atts = shortcode_atts($defatts, $atts);        
        $att_color = $atts['color'];           
        $att_margin = $atts['margin']; 
        $att_align = $atts['align']; 
        $att_width = (int)$atts['width']; 
        $att_salign = $atts['salign'];          
        $att_smargin = $atts['smargin'];
        
        $style = ' style="';
        $style .= 'color:'.$att_color.';';
                 
        if($att_width > 0)
        {
            $style .= 'text-align:'.$att_salign.';';
            $style .= 'margin:'.$att_smargin.';';
            $style .= 'width:'.$att_width.'px;display:block;';      
        }      
        $style .= '" ';  

        $style_h = ' style="';
        $style_h .= 'margin:'.$att_margin.';';
        $style_h .= 'text-align:'.$att_align.';'; 
        $style_h .= '" '; 
                                                                  
        $out .= '<div '.$style_h.' class="dcs-fancy-header"><span '.$style.'>'.$content.'</span></div>';

        return $out;
    }      
    
    # SHORTCODE: 
    #   dcs_small_block 
    # PAREMAETERS:
    #   color - text color, can be set to any CSS valid value for color property (default #EEEEEE)
    # NOTES:  
    public function dcs_small_block($atts, $content=null, $code="")
    {   
        $out = '';
        $defatts = Array(
          'color' => '#666',
          'padding' => '0px 0px 0px 0px',
          'fsize' => 11,
          'align' => 'left',
          'fheight' => 14,
          'border' => 'false',
          'bcolor' => '#DDDDDD',
          'bgcolor' => ''
        );        
        $atts = shortcode_atts($defatts, $atts);        
        $att_color = $atts['color'];   
        $att_padding = $atts['padding'];
        $att_fsize = (int)$atts['fsize'];
        $att_align = $atts['align'];        
        $att_fheight = (int)$atts['fheight'];        
        $att_border = $atts['border'];  
        $att_bcolor = $atts['bcolor'];
        $att_bgcolor = $atts['bgcolor'];
        
        if($att_fsize < 1) { $att_fsize = 10; }
        if($att_fheight < 1) { $att_fheight = 13; }
        
        $style = ' style="';
        $style .= 'color:'.$att_color.';';
        $style .= 'text-align:'.$att_align.';'; 
        $style .= 'line-height:'.$att_fheight.'px;';          
        $style .= 'padding:'.$att_padding.';';
        if($att_bgcolor != '') { $style .= 'background-color:'.$att_bgcolor.';'; }
        
        if($att_border == 'true')
        {
            $style .= 'border:1px solid '.$att_bcolor.';';     
        }
         
        if($att_fsize != '') { $style .= 'font-size:'.$att_fsize.'px;'; }        
        $style .= '" ';                                                   
        
        $out .= '<div class="dcs-small-block" '.$style.'>'.$content.'</div>';
        return $out;    
    }        
    
    # SHORTCODE: 
    #   dcs_lb_link
    # PAREMAETERS:
    #   url - url to image or video displayed via lightbox, must have valid value
    #   group - group name for lightbox (default no set)
    #   title - image title for light box (defalt not set)
    # NOTES:     
    public function dcs_lb_link($atts, $content=null, $code="")
    {
        $out = '';
        $defatts = Array(
          'color' => '',  
          'url' => '',
          'group' => '',
          'title' => ''
        ); 
        
        $atts = shortcode_atts($defatts, $atts);                 
        $att_url = $atts['url'];      
        $att_group = $atts['group']; 
        $att_title = $atts['title'];
        $att_color = $atts['color'];
        
        $style = '';
        if($att_color != '')
        {
            $style = ' style="color:'.$att_color.';" ';    
        }
        
        $out .= '<a '.$style.' href="'.$att_url.'" rel="lightbox'.($att_group != '' ? '['.$att_group.']' : '' ).'" title="'.$att_title.'" >'.$content.'</a>';
        return $out;
    }
    
    # SHORTCODE: 
    #   dcs_lb_ngg
    # PAREMAETERS:
    #   id - image id from next gen gallery
    #   group - group name for lightbox (default no set)
    # NOTES:     
    public function dcs_lb_ngg($atts, $content=null, $code="")
    {
        $out = '';
        $defatts = Array(
          'id' => '',
          'group' => '',
          'color' => '',
        ); 
        
        $atts = shortcode_atts($defatts, $atts);                 
        $att_id = $atts['id'];      
        $att_group = $atts['group'];
        $att_color = $atts['color'];  
        
        $style = '';
        if($att_color != '')
        {
            $style = ' style="color:'.$att_color.';" ';    
        }        
        
        global $nggdb;
        $pic = false;
        if(isset($nggdb))
        {             
            $pic = $nggdb->find_image($att_id); 
        }    
        
        $out .= '<a '.$style.' href="'.$pic->imageURL.'" rel="lightbox'.($att_group != '' ? '['.$att_group.']' : '' ).'" title="'.$pic->alttext.'">'.$content.'</a>';
        return $out;
    }     

    # SHORTCODE: 
    #   dcs_blockquote
    # PAREMAETERS:
    # NOTES:     
    public function dcs_blockquote($atts, $content=null, $code="")
    {
        $out = '';
        $defatts = Array(
          'width' => 0,
          'float' => 'none',
          'color' => '',
          'margin' => '0px 0px 15px 0px',
          'align' => 'left',
          'border' => 'true',
          'author' => '',
          'title' => '' 
        ); 
        
        $atts = shortcode_atts($defatts, $atts);                      
        $att_width = (int)$atts['width'];
        $att_color = $atts['color'];  
        $att_margin = $atts['margin']; 
        $att_float = $atts['float']; 
        $att_align = $atts['align'];
        $att_border = $atts['border']; 
        $att_author = $atts['author'];
        $att_title = $atts['title']; 
        
        $style = '';
        $style .= ' style="';
            if($att_color != '') { $style .= 'color:'.$att_color.';'; }        
            if($att_width > 0) { $style .= 'width:'.$att_width.'px;'; }
            if($att_float != 'none') { $style .= 'float:'.$att_float.';'; } 
            $style .= 'margin:'.$att_margin.';';
            $style .= 'text-align:'.$att_align.';';
            if($att_border == 'false') { $style .= 'border:none;padding:0px;'; }
        $style .= '" ';
        
        global $nggdb;
        $pic = false;
        if(isset($nggdb))
        {             
            $pic = $nggdb->find_image($att_id); 
        }    
        
        $out .= '<blockquote '.$style.' >'.$content;
            if($att_author != '')
            {
                $out .= '<span class="author">'.$att_author.'</span>';
            }
            if($att_title != '')
            {
                $out .= '<span class="authortitle">'.$att_title.'</span>';
            }
        $out .= '</blockquote>';
        return $out;
    }  
    

    # SHORTCODE: 
    #   dcs_box 
    # PAREMAETERS:
    #   h - height in pixels
    # NOTES:       
    public function dcs_box($atts, $content=null, $code="")
    {
        $out = '';
        $defatts = Array(
          'w' => 'auto',
          'h' => 'auto',
          'color' => '',
          'bgcolor' => '',
          'bcolor' => '',
          'bwidth' => 1,          
          'align' => 'left',
          'execute' => 'true',
          'rounded' => 0,
          'margin' => '',
          'padding' => '',
          'fsize' => 0,
          'float' => 'none',
          'bg' => '',
          'bgpos' => 'left top',
          'bgrepeat' => 'no-repeat',
          'font' => '',
          'uppercase' => 'false'          
        ); 
        
        $atts = shortcode_atts($defatts, $atts);        
        $att_w = (int)$atts['w'];
        $att_h = (int)$atts['h'];
        $att_align = $atts['align'];
        $att_execute = $atts['execute'];
        $att_color = $atts['color'];
        $att_bgcolor = $atts['bgcolor'];
        $att_bcolor = $atts['bcolor'];
        $att_bwidth = (int)$atts['bwidth']; 
        $att_rounded = (int)$atts['rounded'];         
        $att_margin = $atts['margin'];
        $att_padding = $atts['padding'];
        $att_float = $atts['float'];
        $att_fsize = (int)$atts['fsize'];
        $att_bg = $atts['bg']; 
        $att_bgpos = $atts['bgpos'];
        $att_bgrepeat = $atts['bgrepeat'];
        $att_font = $atts['font'];
        $att_uppercase = $atts['uppercase']; 
        
        $style = '';
        $style .= ' style="';
            if($att_w != 'auto') { $style .= 'width:'.$att_w.'px;'; }
            if($att_h != 'auto') { $style .= 'height:'.$att_h.'px;'; }
            if($att_align != 'left') { $style .= 'text-align:'.$att_align.';'; }
            if($att_color != '') { $style .= 'color:'.$att_color.';'; }
            if($att_bgcolor != '') { $style .= 'background-color:'.$att_bgcolor.';'; }
            if($att_bcolor != '') { $style .= 'border:'.$att_bwidth.'px solid '.$att_bcolor.';'; }
            if($att_margin != '') { $style .= 'margin:'.$att_margin.';'; }
            if($att_padding != '') { $style .= 'padding:'.$att_padding.';'; }
            if($att_float != 'none') { $style .= 'float:'.$att_float.';'; }
            if($att_font != '') { $style .= 'font-family:'.$att_font.';'; }  
            if($att_uppercase == 'true') { $style .= 'text-transform:uppercase;'; }
            if($att_fsize != 0) { $style .= 'font-size:'.$att_fsize.'px;line-height:'.($att_fsize+3).'px;'; }  
            if($att_bg != '') { $style .= 'background-image:url('.$att_bg.');background-position:'.$att_bgpos.';'; if($att_bgrepeat != 'no-repeat') { $style .= 'background-repeat:'.$att_bgrepeat.';'; } }           
        $style .= '" ';        
   
        if($att_execute == 'true') { $content = do_shortcode($content); }     
        
        $class = '';
        if($att_rounded > 0) {  $class = ' dc-rounded-'.$att_rounded; }           
        
        $out .= '<div class="dcs-box'.$class.'" '.$style.'>'.$content.'</div>';
        return $out;
    }      
    

    # SHORTCODE: 
    #   dcs_note 
    # PAREMAETERS:
    #   h - height in pixels
    # NOTES:       
    public function dcs_note($atts, $content=null, $code="")
    {
        $out = '';
        $defatts = Array(
          'color' => '#606060',
          'bgcolor' => '#FFFBCC',
          'bcolor' => '#F1F1F1',
          'align' => 'left',
          'execute' => 'true',
          'rounded' => 0
        ); 
        
        $atts = shortcode_atts($defatts, $atts);        
        $att_color = $atts['color'];
        $att_bgcolor = $atts['bgcolor'];
        $att_bcolor = $atts['bcolor'];
        $att_align = $atts['align'];
        $att_execute = $atts['execute'];
        $att_rounded = (int)$atts['rounded'];
        
        $style = '';
        $changed = false;
        $style .= ' style="';
            if($att_color != '#606060') { $style .= 'color:'.$att_color.';'; $changed = true; }
            if($att_bgcolor != '#FFFBCC') { $style .= 'background-color:'.$att_bgcolor.';'; $changed = true; }
            if($att_bcolor != '#F1F1F1') { $style .= 'border-color:'.$att_bcolor.';'; $changed = true; }
            if($att_align != 'left') { $style .= 'text-align:'.$att_align.';'; $changed = true; }
        $style .= '" ';        
        
        if(!$changed) { $style = ''; }   
   
        if($att_execute == 'true') { $content = do_shortcode($content); }
        $class = '';
        if($att_rounded > 0) {  $class = ' dc-rounded-'.$att_rounded; }     
        
        $out .= '<div class="note-block'.$class.'" '.$style.'>'.$content.'</div>';
        return $out;
    }     

    # SHORTCODE: 
    #   dcs_postbox 
    # PAREMAETERS:
    #   id - post id to show
    # NOTES:       
    public function dcs_postbox($atts, $content=null, $code="")
    {
        $out = '';
        $defatts = Array(
          'id' => CMS_NOT_SELECTED,
          'type' => 'small-190',
          'desc' => '',
          'words' => 12,
          'float' => 'right',
          'margin' => '0px 0px 10px 10px',
          'lightbox' => 'false',
          'img' => '' 
        ); 
        
        $atts = shortcode_atts($defatts, $atts);        
        $att_id = $atts['id'];
        $att_type = $atts['type'];
        $att_desc = $atts['desc'];
        $att_words = $atts['words'];
        $att_float = $atts['float'];
        $att_margin = $atts['margin'];
        $att_lightbox = $atts['lightbox'] == 'true' ? true : false;  
        $att_img = $atts['img']; 
        
        if($att_id != CMS_NOT_SELECTED)
        {
            global $wpdb;
            $query = "SELECT ID, post_title, post_excerpt, post_content FROM $wpdb->posts WHERE $wpdb->posts.ID = $att_id AND $wpdb->posts.post_type = 'post' AND $wpdb->posts.post_status = 'publish' ";
            $result = $wpdb->get_results($query);
            
            $count = 0;
            if(is_array($result)) { $count = count($result); }                        
            if($count)
            {                                             
                $obj = $result[0];
                $PADDING = 15;   
                $BORDER = 2;
                
                $post_image = '';
                
                if($att_img == '')
                {
                    $post_opt = get_post_meta($obj->ID, 'post_opt', true);
                    $post_image = $post_opt['image_url'];
                    $post_image = dcf_isNGGImageID($post_image);                
                } else
                {
                    $post_image = $att_img;    
                }
                
                
                $width = 0;
                $img_h = 0;
                switch($att_type)
                {
                    case 'small-190': $width = 190 - $BORDER - ($PADDING*2); $img_h = 90; break;
                    case 'medium-225': $width = 225 - $BORDER - ($PADDING*2); $img_h = 120; break;
                    case 'large-300': $width = 300 - $BORDER - ($PADDING*2); $img_h = 160; break;
                    default: $width = 190 - ($PADDING*2); $img_h = 120; break;
                }
                
                $style = '';
                $style .= ' style="';
                    $style .= 'width:'.$width.'px;';
                    $style .= 'float:'.$att_float.';'; 
                    $style .= 'margin:'.$att_margin.';';
                $style .= '" ';                 
                
                $permalink = get_permalink($obj->ID);
                $out .= '<div class="dcs-post-box" '.$style.'>';
                    $out .= '<a class="title" href="'.$permalink.'">'.$obj->post_title.'</a>';
                    
                    if($att_lightbox)
                    {
                        $out .= '<div class="image-wrapper" style="width:'.$width.'px;height:'.$img_h.'px;">';
                            $out .= '<a class="image async-img-s" rel="'.dcf_getTimThumbURL($post_image, $width, $img_h).'" style="width:'.$width.'px;height:'.$img_h.'px;"></a>';
                            $out .= '<a class="triger" href="'.$post_image.'" rel="lightbox" style="width:'.$width.'px;height:'.$img_h.'px;" title="'.$obj->post_title.'"></a>';
                        $out .= '</div>';
                    } else
                    {
                        $out .= '<a href="'.$permalink.'" class="image async-img-s" rel="'.dcf_getTimThumbURL($post_image, $width, $img_h).'" style="width:'.$width.'px;height:'.$img_h.'px;"></a>';    
                    }
                    $out .= '<div class="box-desc">';                    
                        $more_link = ' <a class="more-link" href="'.$permalink.'">&raquo;&nbsp;'.__('Read&nbsp;more', 'dc_theme').'</a>'; 
                        $desc = '';
                        if($att_desc != '')
                        {
                            $desc = $att_desc;        
                        } else
                        if($obj->post_excerpt != '')
                        {
                            $desc = dcf_strNWords($obj->post_excerpt, $att_words);   
                        } else
                        if($obj->post_content != '')
                        {
                            $desc = dcf_strNWords($obj->post_content, $att_words);     
                        }
                        $out .= $desc.$more_link; 
                    $out .= '</div>'; 
                $out .= '</div>';  
                
            }                            
        }
        return $out;
    } 
    
    # SHORTCODE: 
    #   dcs_pagebox 
    # PAREMAETERS:
    #   id - post id to show
    # NOTES:       
    public function dcs_pagebox($atts, $content=null, $code="")
    {
        $out = '';
        $defatts = Array(
          'id' => CMS_NOT_SELECTED,
          'type' => 'small-190',
          'desc' => '',
          'words' => 12,
          'float' => 'right',
          'margin' => '0px 0px 10px 10px',
          'lightbox' => 'false',
          'img' => '' 
        ); 
        
        $atts = shortcode_atts($defatts, $atts);        
        $att_id = $atts['id'];
        $att_type = $atts['type'];
        $att_desc = $atts['desc'];
        $att_words = $atts['words'];
        $att_float = $atts['float'];
        $att_margin = $atts['margin'];
        $att_lightbox = $atts['lightbox'] == 'true' ? true : false;  
        $att_img = $atts['img'];
        
        if($att_id != CMS_NOT_SELECTED)
        {
            global $wpdb;
            $query = "SELECT ID, post_title, post_excerpt, post_content FROM $wpdb->posts WHERE $wpdb->posts.ID = $att_id AND $wpdb->posts.post_type = 'page' AND $wpdb->posts.post_status = 'publish' ";
            $result = $wpdb->get_results($query);
            
            $count = 0;
            if(is_array($result)) { $count = count($result); }                        
            if($count)
            {                                             
                $obj = $result[0];
                $PADDING = 15;   
                $BORDER = 2;
                
                $post_image = '';
                if($att_img == '')
                {
                    $post_opt = get_post_meta($obj->ID, 'pagecommon_opt', true);
                    $post_image = $post_opt['page_image'];
                    $post_image = dcf_isNGGImageID($post_image);                
                } else
                {
                    $post_image = $att_img;
                }
                
                $width = 0;
                $img_h = 0;
                switch($att_type)
                {
                    case 'small-190': $width = 190 - $BORDER - ($PADDING*2); $img_h = 90; break;
                    case 'medium-225': $width = 225 - $BORDER - ($PADDING*2); $img_h = 120; break;
                    case 'large-300': $width = 300 - $BORDER - ($PADDING*2); $img_h = 160; break;
                    default: $width = 190 - ($PADDING*2); $img_h = 120; break;
                }
                
                $style = '';
                $style .= ' style="';
                    $style .= 'width:'.$width.'px;';
                    $style .= 'float:'.$att_float.';'; 
                    $style .= 'margin:'.$att_margin.';';
                $style .= '" ';                 
                
                $permalink = get_permalink($obj->ID);
                $out .= '<div class="dcs-post-box" '.$style.'>';
                    $out .= '<a class="title" href="'.$permalink.'">'.$obj->post_title.'</a>';
                    
                    if($att_lightbox)
                    {
                        $out .= '<div class="image-wrapper" style="width:'.$width.'px;height:'.$img_h.'px;">';
                            $out .= '<a class="image async-img-s" rel="'.dcf_getTimThumbURL($post_image, $width, $img_h).'" style="width:'.$width.'px;height:'.$img_h.'px;"></a>';
                            $out .= '<a class="triger" href="'.$post_image.'" rel="lightbox" style="width:'.$width.'px;height:'.$img_h.'px;" title="'.$obj->post_title.'"></a>';
                        $out .= '</div>';
                    } else
                    {
                        $out .= '<a href="'.$permalink.'" class="image async-img-s" rel="'.dcf_getTimThumbURL($post_image, $width, $img_h).'" style="width:'.$width.'px;height:'.$img_h.'px;"></a>';    
                    }
                    $out .= '<div class="box-desc">';                    
                        $more_link = ' <a class="more-link" href="'.$permalink.'">&raquo;&nbsp;'.__('Read&nbsp;more', 'dc_theme').'</a>'; 
                        $desc = '';
                        if($att_desc != '')
                        {
                            $desc = $att_desc;        
                        } else
                        if($obj->post_excerpt != '')
                        {
                            $desc = dcf_strNWords($obj->post_excerpt, $att_words);   
                        } else
                        if($obj->post_content != '')
                        {
                            $desc = dcf_strNWords($obj->post_content, $att_words);     
                        }
                        $out .= $desc.$more_link; 
                    $out .= '</div>'; 
                $out .= '</div>';  
                
            }                            
        }
        return $out;
    }   
     
    # SHORTCODE: 
    #   dcs_chain_gallery
    # PAREMAETERS:
    #   id - next gen gallery id, must be set to valid value
    #   random - display random images, can be set to true or false (default true)
    #   number - number of images to display, if zero all images are displayed (default zero)
    #   set - pictures ids to show e.g '34,2,54,8' (default not set)
    #   h - slide height in pixels (default 600 px)
    #   w - slide width in pixels (default 270 px)
    #   th - thumb height in pixels (default 50 px)
    #   tw - thumb width in pixels (default 50 px)
    #   bcolor - frame color, can be set to any CSS valid valu for color property (default #FFCC00)
    #   trans - can be set to none, slide or fade (default fade) 
    #   autoplay - can best seto to true or false (default true)
    #   desc - if true images description will be displayed (default true)
    #   pageid - page id for slider (default not set)
    #   url - manullay link fo slider, if set pageid parameter will be overwrited (default not set)
    # NOTES:     
    public function dcs_chain_gallery($atts, $content=null, $code="")
    {
        $out = '';
        $defatts = Array(
          'id' => CMS_NOT_SELECTED,
          'w' => 620,
          'h' => 270,          
          'random' => 'false',
          'number' => 6,
          'set' => '',
          'tw' => 50,
          'th' => 50,
          'bcolor' => '#EDD015',
          'trans' => 'fade',
          'autoplay' => 'true',
          'desc' => 'true',
          'pageid' => CMS_NOT_SELECTED,
          'url' => '',
          'title' => 'false',
          'size' => 3,
          'float' => 'none',
          'margin' => '0px 0px 15px 0px'
        );         
        
        $atts = shortcode_atts($defatts, $atts);        
        $att_id = $atts['id'];
        $att_random = $atts['random']; 
        $att_set = $atts['set'];         
        $att_number = (int)$atts['number']; 
        $att_w = (int)$atts['w'];         
        $att_h = (int)$atts['h']; 
        $att_tw = (int)$atts['tw'];         
        $att_th = (int)$atts['th']; 
        $att_bcolor = $atts['bcolor'];
        $att_trans = $atts['trans']; 
        $att_autoplay = $atts['autoplay'];
        $att_desc = $atts['desc'];
        $att_pageid = $atts['pageid'];
        $att_url = $atts['url'];
        $att_float = $atts['float'];
        $att_margin = $atts['margin'];         
        $att_title = $atts['title'];
        $att_size = $atts['size'];         
        
        if($att_pageid != CMS_NOT_SELECTED)
        {
            $att_pageid = get_permalink($att_pageid);
            if($att_url == '')
            {
                $att_url = $att_pageid;    
            }    
        } 
        
        if($att_url != '')
        {
            $att_url = ' href="'.$att_url.'" ';
        }
               
       
        $pics_id_list = explode(',', $att_set);
       
        if($att_id != CMS_NOT_SELECTED or ($att_random == 'true'))
        {
            global $nggdb;
            if(isset($nggdb))
            {              
                $pics = null;
                if($att_random == 'true')
                {
                    if($att_id == CMS_NOT_SELECTED) { $att_id = 0; }
                    $pics = $nggdb->get_random_images($att_number, $att_id); 
                } else
                {
                    if($att_set == '')
                    {
                        $pics = $nggdb->get_gallery($att_id, 'sortorder', 'ASC', true, $att_number, $start = 0);
                    } else
                    {
                        $pics = $nggdb->find_images_in_list($pics_id_list, false);     
                    }
                }               
                
                if($pics !== null)
                {
                    $slider_style = ' style="';
                    $slider_style .= 'margin:'.$att_margin.';float:'.$att_float.';';
                    $slider_style .= '" ';
                    
                    $out .= '<div class="chain-slider" '.$slider_style.'>';                  
                    
                    $slide_style = ' style="width:'.$att_w.'px;height:'.$att_h.'px;" ';
                    $out .= '<div class="slides-wrapper" '.$slide_style.'>'; 
                    foreach($pics as $pic)
                    {   // alttext                    
                        $rel = ' rel="'.get_bloginfo('template_url').'/thumb.php?src='.$pic->imageURL.'&w='.$att_w.'&h='.$att_h.'&zc=1'.'" ';
                        $out .= '<div class="slide" '.$slide_style.'><a '.$att_url.' class="loader async-img" '.$slide_style.' '.$rel.'></a>';
                        if(($att_desc == 'true' and $pic->description != '') or ($att_title == 'true' and $pic->alttext != ''))
                        {
                            $show_title = false;
                            if($att_title == 'true' and $pic->alttext != '')
                            {
                               $show_title = true; 
                            }
                            $show_desc = false;
                            if($att_desc == 'true' and $pic->description != '')
                            {
                                $show_desc = true;
                            }                            
                            
                            $out .= '<div class="desc-wrapper" '.(!$show_title ? ' style="padding-top:10px;" ' : '').'>';
                            if($show_title) 
                            {
                                $out .= '<h'.$att_size.' class="title">'.stripcslashes($pic->alttext).'</h'.$att_size.'>';
                            }                            
                            
                            if($show_desc)
                            {
                                $out .= '<div class="desc" style="width:'.($att_w-40).'px;">'.stripcslashes($pic->description).'</div>';
                            }
                            $out .= '</div>';
                        }

                        $out .= '</div>';
                    }
                    $out .= '</div>';
                    
                    $out .= '<div class="thumbs-wrapper" style="height:'.($att_th+2).'px;">';
                        $out .= '<ul class="thumbs-slider">';
                            $counter = 0;
                            foreach($pics as $pic)
                            {
                                $rel = ' src="'.get_bloginfo('template_url').'/thumb.php?src='.$pic->thumbURL.'&w='.$att_tw.'&h='.$att_th.'&zc=1'.'" ';
                                $out .= '<li style="width:'.$att_tw.'px;height:'.$att_th.'px;"><img class="loader" '.$rel.' style="width:'.$att_tw.'px;height:'.$att_th.'px;" alt="" /><em class="data">'.$counter.'</em></li>';       
                                $counter++;
                            }
                        $out .= '</ul>';                    
                    $out .= '</div>';
                    
                    $out .= '<span class="border-color data">'.$att_bcolor.'</span>'; 
                    $out .= '<span class="trans data">'.$att_trans.'</span>';
                    $out .= '<span class="autoplay data">'.$att_autoplay.'</span>';                      
                    $out .= '</div>';
                }
            }
        }        
 
        return $out;
    }   
            

    # SHORTCODE: 
    #   dcs_simple_gallery_ngg 
    # PAREMAETERS:
    #   gid - next gen gallery id, must be set to valid value 
    #   width - slider width in pixels, default set to 600 px
    #   height - slider height in pixels, default set to 300 px
    #   desc - slider description
    #   align - slider description text align
    #   random - true to display random images from gallery, false to display ordered list, default set to false
    #   number - number of thumbs to display, should be grater then zero
    #   trans - can be set to fade, none or slide, default fade        
    # NOTES:           
    public function dcs_simple_gallery_ngg($atts, $content=null, $code="")
    {
        $out = '';
        $defatts = Array(
          'id' => CMS_NOT_SELECTED,
          'w' => 620,
          'h' => 300,
          'ts' => 50, 
          'desc' => '',          
          'align' => 'left',          
          'random' => 'true',
          'margin' => '0px auto 15px auto',
          'number' => 6,
          'float' => 'none',          
          'trans' => 'fade',          
          'bcolor' => '#FFFFFF',          
          'tb' => 15,
          'tt' => 15,
          'sdesc' => 'false',
          'stitle' => 'false'                              
        ); 
        
        $isset_tt = isset($atts['tt']); 
        
        $atts = shortcode_atts($defatts, $atts);        
        $att_gid = $atts['id'];
        $att_width = $atts['w'];
        $att_height = $atts['h'];
        $att_desc = $atts['desc'];
        $att_align = $atts['align'];
        $att_ts = (int)$atts['ts'];
        $att_random = $atts['random'];
        $att_count = (int)$atts['number'];
        $att_trans = $atts['trans']; 
        $att_sdesc = $atts['sdesc'] == 'true' ? true : false; 
        $att_stitle = $atts['stitle'] == 'true' ? true : false;
        
        $att_bcolor = $atts['bcolor'];  
        $att_tb = $atts['tb'];          
        $att_tt = $atts['tt']; 
        
        $att_float = $atts['float'];          
        $att_margin = $atts['margin'];          
        
        global $nggdb;
        if(isset($nggdb) and $att_gid != CMS_NOT_SELECTED)
        {            
            $gallery = $nggdb->find_gallery($att_gid);

            if($gallery != null)
            {    
                $pics = null;
                if($att_random == 'true')
                {
                    $pics = $nggdb->get_random_images($att_count, $att_gid); 
                } else
                {
                    $pics = $nggdb->get_gallery($att_gid, 'sortorder', 'ASC', true, $att_count, 0);    
                } 
                
             
                $con_style = ' style="';
                $con_style .= 'float:'.$att_float.';';
                $con_style .= 'margin:'.$att_margin.';';
                if($con_style != 'none')
                {
                    $con_style .= 'width:'.$att_width.';';    
                }
                $con_style .= '" ';                    
                
                $out .= '<div class="dcs-simple-gallery" '.$con_style.'>';
                    $out .= '<span class="trans">'.$att_trans.'</span>';
                    $out .= '<div class="slider" style="width:'.$att_width.'px;height:'.$att_height.'px;">';
                        foreach($pics as $pic)
                        {
                            $rel = dcf_getTimThumbURL($pic->imageURL, $att_width, $att_height);
                            $out .= '<div class="slide" style="width:'.$att_width.'px;height:'.$att_height.'px;">';
                                $out .= '<a class="image async-img" rel="'.$rel.'" style="width:'.$att_width.'px;height:'.$att_height.'px;"></a>';
                                if(($att_sdesc and $pic->description != '') or ($att_stitle and $pic->alttext != '')) 
                                {
                                    $out .= '<div class="slide-desc" style="width:'.($att_width-16).'px;">';
                                        if($att_stitle and $pic->alttext != '')
                                        {
                                            $out .= '<div class="title">'.stripcslashes($pic->alttext).'</div>';
                                            if($att_sdesc and $pic->description != '') 
                                            {
                                                $out .= '<div class="separator"></div>';
                                            }
                                        }
                                        if($att_sdesc and $pic->description != '')
                                        {
                                            $out .= '<div>'.stripcslashes($pic->description).'</div>';
                                        }
                                    $out .= '</div>';
                                }
                            $out .= '</div>';
                            
                            $thumb_style = ' style="width:'.$att_ts.'px;height:'.$att_ts.'px;border-color:'.$att_bcolor.';';
                            if($isset_tt)
                            {
                                $thumb_style .= 'top:'.$att_tt.'px;';
                                $thumb_style .= 'bottom:auto;';
                            } else
                            {
                                $thumb_style .= 'bottom:'.$att_tb.'px;';
                            }
                            $thumb_style .= '" ';                            
                            
                            $src = dcf_getTimThumbURL($pic->thumbURL, $att_ts, $att_ts); 
                            $out .= '<div class="thumb async-img-s" '.$thumb_style.' rel="'.$src.'" /></div>';
                        }
                    $out .= '</div>';
                    if($att_desc != '')
                    {
                        $out .= '<p class="desc" style="text-align:'.$att_align.';width:'.$att_width.'px;" >'.$att_desc.'</p>';
                    }
                $out .= '</div>';                
                
            }
        }    
        return $out;         
    }    

    # SHORTCODE: 
    #   dcs_simple_gallery_thumbs_ngg 
    # PAREMAETERS:
    #   gid - next gen gallery id, must be set to valid value       
    #   width - slider width in pixels, default set to 600 px
    #   height - slider height in pixels, default set to 300 px
    #   desc - slider description
    #   align - slider description text align
    #   random - true to display random images from gallery, false to display ordered list, default set to false
    #   number - number of thumbs to display, should be grater then zero  
    #   trans - can be set to fade, none or slide, default fade         
    # NOTES:           
    public function dcs_simple_gallery_thumbs_ngg($atts, $content=null, $code="")
    {
        $out = '';
        $defatts = Array(
          'id' => CMS_NOT_SELECTED,          
          'w' => 620,
          'h' => 300,
          'ts' => 40,
          'desc' => '',
          'align' => 'left',          
          'random' => 'true',
          'margin' => '0px auto 15px auto',
          'number' => 6,
          'float' => 'none',
          'trans' => 'fade',
          'bcolor' => '#FFFFFF',
          'sdesc' => 'false',
          'stitle' => 'false'                                                                          
        ); 
        
        $atts = shortcode_atts($defatts, $atts);
        $att_gid = $atts['id'];                 
        $att_width = (int)$atts['w'];
        $att_height =(int)$atts['h'];
        $att_desc = $atts['desc'];
        $att_align = $atts['align'];
        $att_ts = (int)$atts['ts'];
        $att_random = $atts['random'];
        $att_count = (int)$atts['number'];                         
        $att_trans = $atts['trans'];         
        $att_bcolor = $atts['bcolor']; 
        
        $att_float = $atts['float'];          
        $att_margin = $atts['margin']; 
        $att_sdesc = $atts['sdesc'] == 'true' ? true : false; 
        $att_stitle = $atts['stitle'] == 'true' ? true : false;                        
        
        $con_style = ' style="';
        $con_style .= 'float:'.$att_float.';';
        $con_style .= 'margin:'.$att_margin.';';
        if($con_style != 'none')
        {
            $con_style .= 'width:'.$att_width.';';    
        }
        $con_style .= '" ';          
        
        global $nggdb;
        if(isset($nggdb))
        {            
            $gallery = $nggdb->find_gallery($att_gid);
            
            
            if($gallery != null)
            {    
                $pics = null;
                if($att_random == 'true')
                {
                    $pics = $nggdb->get_random_images($att_count, $att_gid); 
                } else
                {   
                    $pics = $nggdb->get_gallery($att_gid, 'sortorder', 'ASC', true, $att_count, 0);
                        
                } 
                
                $out .= '<div class="dcs-simple-gallery-thumbs" '.$con_style.'>';
                    $out .= '<span class="trans">'.$att_trans.'</span>';
                    $out .= '<div class="slider" style="width:'.$att_width.'px;height:'.$att_height.'px;">';
                        foreach($pics as $pic)
                        {
                            $rel = dcf_getTimThumbURL($pic->imageURL, $att_width, $att_height);                            
                            $out .= '<div class="slide" style="width:'.$att_width.'px;height:'.$att_height.'px;">';
                                $out .= '<a class="image async-img" rel="'.$rel.'" style="width:'.$att_width.'px;height:'.$att_height.'px;"></a>';
                                if(($att_sdesc and $pic->description != '') or ($att_stitle and $pic->alttext != ''))
                                {
                                    $out .= '<div class="slide-desc">';
                                        if($att_stitle and $pic->alttext != '')
                                        {
                                            $out .= '<div class="title">'.stripcslashes($pic->alttext).'</div>';
                                            if($att_sdesc and $pic->description != '') 
                                            {
                                                $out .= '<div class="separator"></div>';
                                            }                                            
                                        }
                                        if($att_sdesc and $pic->description != '')
                                        {
                                            $out .= '<div>'.stripcslashes($pic->description).'</div>';
                                        }
                                    $out .= '</div>';
                                }
                            $out .= '</div>';                                                        
                        }                
                    $out .= '</div>';
                    if($att_desc != '')
                    {
                        $out .= '<p class="desc" style="text-align:'.$att_align.';width:'.$att_width.'px;" >'.$att_desc.'</p>';
                    }            
                    $out .= '<div class="thumbs-wrapper" style="width:'.$att_width.'px;">';
                    foreach($pics as $pic)
                    {            
                        $src = dcf_getTimThumbURL($pic->thumbURL, $att_ts, $att_ts); 
                        $out .= '<a class="thumb async-img-s" style="width:'.$att_ts.'px;height:'.$att_ts.'px;border-color:'.$att_bcolor.';" rel="'.$src.'" /></a>';
                    }
                    $out .= '<div class="clear-both"></div></div>';
                $out .= '</div>';               
                
            }
        }              
        return $out;          
    }      

    # SHORTCODE: 
    #   dcs_ngg
    # PAREMAETERS:
    #   gid - next gen gallery id, must set to valid value
    #   count - number of thumbs to display, should be grater then zero
    #   width - thumb width
    #   height - thumb height
    #   margin - margin for thumb
    #   framed - if true small frame is displayed around image
    #   random - true to display random images from gallery, false to display ordered list, default set to false
    #   bshow - show border, true or false, default true
    #   bwidth - border width in pixels, default one pixel width
    #   bcolor - border color, default set to white, #FFFFFF
    #   group - group name, default not set, original gruop name will be used    
    # NOTES:     
    public function dcs_ngg($atts, $content=null, $code="")
    {
        $out = '';        
        $defatts = Array(
          'id' => CMS_NOT_SELECTED,
          'number' => 10,
          'w' => 80,
          'h' => 80,
          'random' => 'false',
          'tips' => 'false',
          'bshow' => 'true',                     
          'margin' => '0px 15px 15px 0px',          
          'padding' => 0,
          'group' => '',            
          'bcolor' => '#FFFFFF',
          'bwidth' => '1',
          'rounded' => 0,         
          'mbottom' => 0,
          'bgcolor' => ''

        ); 
        
        $atts = shortcode_atts($defatts, $atts);        
        $att_gid = $atts['id'];                
        $att_count = (int)$atts['number']; 
        $att_random = $atts['random'] == 'true' ? true : false; 
        $att_width = (int)$atts['w'];
        $att_height = (int)$atts['h'];
        $att_margin = $atts['margin'];
        $att_mbottom = (int)$atts['mbottom'];
        $att_bcolor = $atts['bcolor'];
        $att_bwidth = (int)$atts['bwidth']; 
        $att_bshow = $atts['bshow'] == 'true' ? true : false;
        $att_tips = $atts['tips'] == 'true' ? true : false;         
        $att_group = $atts['group'];
        $att_rounded = (int)$atts['rounded']; 
        $att_padding = (int)$atts['padding'];
        $att_bgcolor = $atts['bgcolor']; 
        
        if($att_tips) { $att_tips = ' tip-top'; } else { $att_tips = ''; }
        if($att_rounded > 0) {  $att_rounded = ' dc-rounded-'.$att_rounded; } else { $att_rounded = ''; }        
        $con_style = '';
        if($att_mbottom > 0) { $con_style = ' style="margin-bottom:'.$att_mbottom.'px;" '; }
        
        $out .= '<div class="dcs-ngg-gallery-thumbs" '.$con_style.'>';
        
        global $nggdb;
        if(isset($nggdb) and $att_gid != CMS_NOT_SELECTED)
        {            
            $gallery = $nggdb->find_gallery($att_gid);

            if($gallery != null)
            {    
                $pics = null;
                if($att_random)
                {
                    $pics = $nggdb->get_random_images($att_count, $att_gid); 
                } else
                {
                    $pics = $nggdb->get_gallery($att_gid, 'sortorder', 'ASC', true, $att_count, $start = 0);    
                }
                
                if($pics !== null)
                {
                    $count = count($pics);
                    $counter = 1;
                    foreach($pics as $pic)
                    {
                       
                        $style = ' style="';
                        if($att_margin != '0px 15px 15px 0px') { $style .= 'margin:'.$att_margin.';'; }
                        if($counter == $count) { $style .= 'margin-right:0px;'; }
                        $style .= 'width:'.$att_width.'px;';
                        $style .= 'height:'.$att_height.'px;';
                        if($att_bgcolor != '') { $style .= 'background-color:'.$att_bgcolor.';'; } 
                        if($att_padding > 0) { $style .= 'padding:'.$att_padding.'px;'; }
                        
                        if($att_bshow)
                        {
                            $style .= 'border: '.$att_bwidth.'px solid '.$att_bcolor.';';    
                        } 
                        $style .= '" ';
                        
                        if($att_group != '')
                        {
                            $pic->thumbcode = ' rel="lightbox['.$att_group.']" ';
                        } else
                        {
                            $pic->thumbcode = ' rel="lightbox[dcs_ngg_'.$att_gid.']" ';    
                        }                                               
                        
                        $rel = dcf_getTimThumbURL($pic->imageURL, $att_width, $att_height);
                        $title = '';
                        if($atts['tips'] == 'true') { $title = ' title="'.stripcslashes($pic->alttext).'" '; }
                        
                        $out .= '<div class="thumb'.$att_rounded.'" '.$style.'>';
                        $out .= '<a class="loader async-img-s" rel="'.$rel.'" style="width:'.$att_width.'px;height:'.$att_height.'px;"></a>';
                        $out .= '<a href="'.$pic->imageURL.'" class="triger'.$att_tips.'" style="width:'.$att_width.'px;height:'.$att_height.'px;left:'.$att_padding.'px;top:'.$att_padding.'px;" '.$pic->thumbcode.' '.$title.'></a>';
                        $out .= '</div>';
                        
                        $counter++;
                    }    
                }
            }
        }        
        
        $out .= '<div class="clear-both"></div></div>';        
        return $out;
    }  
   
    # SHORTCODE: 
    #   dcs_ngg_single
    # PAREMAETERS:
    #   list - list of pictures ID's e.g "13,1,24,53,93,234,3,54"
    #   width - thumb width, default 80
    #   height - thumb height, default 80
    #   margin - margin for thumb
    #   bshow - show border, true or false, default true
    #   exclude - exclude excluded images, true or false, default false
    #   bwidth - border width in pixels, default one pixel width
    #   bcolor - border color, default set to white, #FFFFFF
    #   group - name of group or empty string, default empty string
    # NOTES:     
    public function dcs_ngg_single($atts, $content=null, $code="")
    {
        $out = '';        
        $defatts = Array(
          'list' => '',
          'w' => 80,
          'h' => 80,
          'margin' => '0px 15px 15px 0px',
          'bcolor' => '#FFFFFF',
          'bwidth' => '1',
          'bshow' => 'true',
          'exclude' => 'false',
          'group' => '',
          'rounded' => 0,
          'padding' => 0,
          'mbottom' => 0,
          'tips' => 'false',
          'bgcolor' => ''
          
        ); 
        
        $atts = shortcode_atts($defatts, $atts);                      
        $att_list = $atts['list'];  
        $att_width = (int)$atts['w'];
        $att_height = (int)$atts['h'];
        $att_margin = $atts['margin'];
        $att_bcolor = $atts['bcolor'];
        $att_bgcolor = $atts['bgcolor'];
        $att_bwidth = (int)$atts['bwidth']; 
        $att_bshow = $atts['bshow'] == 'true' ? true : false;        
        $att_exclude = $atts['exclude'] == 'true' ? true : false; 
        $att_tips = $atts['tips'] == 'true' ? true : false;
        $att_group = $atts['group'];
        $att_rounded = (int)$atts['rounded']; 
        $att_padding = (int)$atts['padding'];
        $att_mbottom = (int)$atts['mbottom']; 
        
        if($att_tips) { $att_tips = ' tip-top'; } else { $att_tips = ''; }  
        if($att_rounded > 0) {  $att_rounded = ' dc-rounded-'.$att_rounded; } else { $att_rounded = ''; }                      
        $pics_id_list = explode(',', $att_list);
        
        $con_style = '';
        if($att_mbottom > 0) { $con_style = ' style="margin-bottom:'.$att_mbottom.'px;" '; }        
        
        $out .= '<div class="dcs-ngg-gallery-thumbs" '.$con_style.'>';
        
        global $nggdb;
        if(isset($nggdb) and $att_list != '')
        {            
 
            $pics = null;
            $pics = $nggdb->find_images_in_list($pics_id_list, $att_exclude); 
              
            if($pics !== null)
            {
                foreach($pics as $pic)
                {   
                    $style = ' style="';
                        if($att_margin != '0px 15px 15px 0px') { $style .= 'margin:'.$att_margin.';'; } 
                        $style .= 'width:'.$att_width.'px;';
                        $style .= 'height:'.$att_height.'px;';
                        if($att_bgcolor != '') { $style .= 'background-color:'.$att_bgcolor.';'; }
                        if($att_padding > 0) { $style .= 'padding:'.$att_padding.'px;'; } 
                        if($att_bshow) { $style .= 'border: '.$att_bwidth.'px solid '.$att_bcolor.';'; } 
                    $style .= '" ';
                    
                    $thumbcode = '';
                    if($att_group != '') { $thumbcode = ' rel="lightbox['.$att_group.']" '; } else { $thumbcode = ' rel="lightbox" '; }                                           
                    $rel = dcf_getTimThumbURL($pic->imageURL, $att_width, $att_height);  

                    $title = '';
                    if($atts['tips'] == 'true') { $title = ' title="'.stripcslashes($pic->alttext).'" '; }                    
                    
                    $out .= '<div class="thumb'.$att_rounded.'" '.$style.'>';
                    $out .= '<a class="loader async-img-s" rel="'.$rel.'" style="width:'.$att_width.'px;height:'.$att_height.'px;"></a>';
                    $out .= '<a href="'.$pic->imageURL.'" class="triger'.$att_tips.'" style="width:'.$att_width.'px;height:'.$att_height.'px;left:'.$att_padding.'px;top:'.$att_padding.'px;" '.$thumbcode.' '.$title.'></a>';
                    $out .= '</div>';
                }    
            }
            
        }        
        
        $out .= '<div class="clear-both"></div></div>';        
        return $out;
    }     
   
    # SHORTCODE: 
    #   dcs_ngg_last
    # PAREMAETERS:
    #   id - next gen gallery id or avoid this parameter, if avoid last images from all galleries will be taken
    #   number - number of images to display, default 10
    #   page - thumbs page, default set to zero with means first page
    #   width - thumb width, default 80
    #   height - thumb height, default 80
    #   margin - margin for thumb
    #   bshow - show border, true or false, default true
    #   exclude - exclude excluded images, true or false, default false
    #   bwidth - border width in pixels, default one pixel width
    #   bcolor - border color, default set to white, #FFFFFF
    #   group - name of group or empty string, default empty string 
    # NOTES:     
    public function dcs_ngg_last($atts, $content=null, $code="")
    {
        $out = '';        
        $defatts = Array(
          'id' => 0,
          'number' => 10,
          'page' => 0,
          'w' => 80,
          'h' => 80,
          'bcolor' => '#FFFFFF',
          'bwidth' => '1',          
          
          'margin' => '0px 15px 15px 0px',
          'bshow' => 'true',
          'exclude' => 'false',
          'group' => '',
          'tips' => 'false',
          'rounded' => 0,
          'padding' => 0,
          'bgcolor' => '',
          'mbottom' => 0
        ); 
                       
        $atts = shortcode_atts($defatts, $atts);                      
        $att_gid = $atts['id'];  
        $att_count = (int)$atts['number'];
        $att_page = (int)$atts['page'];
        $att_width = (int)$atts['w'];
        $att_height = (int)$atts['h'];
        $att_margin = $atts['margin'];
        $att_bcolor = $atts['bcolor'];
        $att_bwidth = (int)$atts['bwidth']; 
        $att_bshow = $atts['bshow'] == 'true' ? true : false;        
        $att_exclude = $atts['exclude'] == 'true' ? true : false;
        $att_tips = $atts['tips'] == 'true' ? true : false;  
        $att_group = $atts['group'];
        $att_rounded = (int)$atts['rounded']; 
        $att_padding = (int)$atts['padding'];
        $att_bgcolor = $atts['bgcolor'];
        $att_mbottom = (int)$atts['mbottom'];   
        
        if($att_tips) { $att_tips = ' tip-top'; } else { $att_tips = ''; }
        if($att_rounded > 0) {  $att_rounded = ' dc-rounded-'.$att_rounded; } else { $att_rounded = ''; }                                
        $con_style = '';
        if($att_mbottom > 0) { $con_style = ' style="margin-bottom:'.$att_mbottom.'px;" '; }              
        
        $out .= '<div class="dcs-ngg-gallery-thumbs" '.$con_style.'>';
        
        global $nggdb;
        if(isset($nggdb))
        {            
            if($att_gid == CMS_NOT_SELECTED) { $att_gid = 0; }
                              
            $pics = null;
            $pics = $nggdb->find_last_images($att_page, $att_count, $att_exclude, $att_gid); 
              
            if($pics !== null)
            {
                foreach($pics as $pic)
                {   
                    $style = ' style="';
                    if($att_margin != '0px 15px 15px 0px') { $style .= 'margin:'.$att_margin.';'; } 
                    $style .= 'width:'.$att_width.'px;';
                    $style .= 'height:'.$att_height.'px;';
                    if($att_bgcolor != '') { $style .= 'background-color:'.$att_bgcolor.';'; } 
                    if($att_bshow) { $style .= 'border: '.$att_bwidth.'px solid '.$att_bcolor.';'; }
                    if($att_padding > 0) { $style .= 'padding:'.$att_padding.'px;'; } 
                    $style .= '" ';
                    
                    $rel = dcf_getTimThumbURL($pic->imageURL, $att_width, $att_height); 
                    $thumbcode = $att_group != '' ? ' rel="lightbox['.$att_group.']" ' : ' rel="lightbox" ';
                    $title = '';
                    if($atts['tips'] == 'true') { $title = ' title="'.stripcslashes($pic->alttext).'" '; }                        
                    
                    $out .= '<div class="thumb'.$att_rounded.'" '.$style.'>';
                    $out .= '<a class="loader async-img-s" rel="'.$rel.'" style="width:'.$att_width.'px;height:'.$att_height.'px;"></a>';
                    $out .= '<a href="'.$pic->imageURL.'" class="triger'.$att_tips.'" style="width:'.$att_width.'px;height:'.$att_height.'px;top:'.$att_padding.'px;left:'.$att_padding.'px;" '.$thumbcode.' '.$title.'></a>';
                    $out .= '</div>';
                }    
            }
            
        }        
        
        $out .= '<div class="clear-both"></div></div>';        
        return $out;
    }     
   
    # SHORTCODE: 
    #   dcs_ngg_random
    # PAREMAETERS:
    #   id - next gen gallery id or avoid this parameter, if avoid last images from all galleries will be taken
    #   number - number of images to display, default 10
    #   width - thumb width, default 80
    #   height - thumb height, default 80
    #   margin - margin for thumb
    #   bshow - show border, true or false, default true
    #   bwidth - border width in pixels, default one pixel width
    #   bcolor - border color, default set to white, #FFFFFF
    #   group - name of group or empty string, default empty string 
    # NOTES:     
    public function dcs_ngg_random($atts, $content=null, $code="")
    {
        $out = '';        
        $defatts = Array(
          'id' => 0,
          'number' => 10,
          'w' => 80,
          'h' => 80,
          'margin' => '0px 15px 15px 0px',
          'bcolor' => '#FFFFFF',
          'bwidth' => '1',
          'bshow' => 'true',
          'tips' => 'false',
          'group' => '',
          'rounded' => 0,
          'padding' => 0,
          'bgcolor' => '',
          'mbottom' => 0
        ); 
                       
        $atts = shortcode_atts($defatts, $atts);                      
        $att_gid = $atts['id'];  
        $att_count = (int)$atts['number'];
        $att_width = (int)$atts['w'];
        $att_height = (int)$atts['h'];
        $att_margin = $atts['margin'];
        $att_bcolor = $atts['bcolor'];
        $att_bwidth = (int)$atts['bwidth']; 
        $att_bshow = $atts['bshow'] == 'true' ? true : false;
        $att_tips = $atts['tips'] == 'true' ? true : false;          
        $att_group = $atts['group'];
        $att_rounded = (int)$atts['rounded'];
        $att_padding = (int)$atts['padding'];
        $att_bgcolor = $atts['bgcolor'];
        $att_mbottom = (int)$atts['mbottom'];   
        
        if($att_tips) { $att_tips = ' tip-top'; } else { $att_tips = ''; }
        if($att_rounded > 0) {  $att_rounded = ' dc-rounded-'.$att_rounded; } else { $att_rounded = ''; }         
        $con_style = '';
        if($att_mbottom > 0) { $con_style = ' style="margin-bottom:'.$att_mbottom.'px;" '; }   
        
        $out .= '<div class="dcs-ngg-gallery-thumbs" '.$con_style.'>';
        
        global $nggdb;
        if(isset($nggdb))
        {            
            if($att_gid == CMS_NOT_SELECTED) { $att_gid = 0; }
            
            $pics = null;
            $pics = $nggdb->get_random_images($att_count, $att_gid); 
              
            if($pics !== null)
            {
                foreach($pics as $pic)
                {   
                    $style = ' style="';
                    if($att_margin != '0px 15px 15px 0px') { $style .= 'margin:'.$att_margin.';'; } 
                    $style .= 'width:'.$att_width.'px;';
                    $style .= 'height:'.$att_height.'px;';
                    if($att_bgcolor != '') { $style .= 'background-color:'.$att_bgcolor.';'; } 
                    if($att_padding > 0) { $style .= 'padding:'.$att_padding.'px;'; } 
                    if($att_bshow) { $style .= 'border: '.$att_bwidth.'px solid '.$att_bcolor.';'; } 
                    $style .= '" ';
                    
                    $rel = dcf_getTimThumbURL($pic->imageURL, $att_width, $att_height); 
                    $thumbcode = $att_group != '' ? ' rel="lightbox['.$att_group.']" ' : ' rel="lightbox" ';
                    $title = '';
                    if($atts['tips'] == 'true') { $title = ' title="'.stripcslashes($pic->alttext).'" '; }                        
                    
                    $out .= '<div class="thumb'.$att_rounded.'" '.$style.'>';
                    $out .= '<a class="loader async-img-s" rel="'.$rel.'" style="width:'.$att_width.'px;height:'.$att_height.'px;"></a>';
                    $out .= '<a href="'.$pic->imageURL.'" class="triger'.$att_tips.'" style="width:'.$att_width.'px;height:'.$att_height.'px;top:'.$att_padding.'px;left:'.$att_padding.'px;" '.$thumbcode.' '.$title.'></a>';
                    $out .= '</div>';
                }    
            }
            
        }        
        
        $out .= '<div class="clear-both"></div></div>';        
        return $out;
    }  
   
    # SHORTCODE: 
    #   dcs_one_half
    # PAREMAETERS:
    # NOTES:     
    public function dcs_one_half($atts, $content=null, $code="")
    {
        $out = '';        
        $out .= '<div class="dcs-one-half">';
        $out .= do_shortcode($content);       
        $out .= '</div>';
        return $out;
    }     

    # SHORTCODE: 
    #   dcs_one_half_last
    # PAREMAETERS:
    # NOTES:     
    public function dcs_one_half_last($atts, $content=null, $code="")
    {
        $out = '';        
        $out .= '<div class="dcs-one-half-last">';
        $out .= do_shortcode($content);       
        $out .= '</div>';
        return $out;
    }       
   
    # SHORTCODE: 
    #   dcs_one_third
    # PAREMAETERS:
    # NOTES:     
    public function dcs_one_third($atts, $content=null, $code="")
    {
        $out = '';        
        $out .= '<div class="dcs-one-third">';
        $out .= do_shortcode($content);       
        $out .= '</div>';
        return $out;
    }     

    # SHORTCODE: 
    #   dcs_one_third_last
    # PAREMAETERS:
    # NOTES:     
    public function dcs_one_third_last($atts, $content=null, $code="")
    {
        $out = '';        
        $out .= '<div class="dcs-one-third-last">';
        $out .= do_shortcode($content);       
        $out .= '</div>';
        return $out;
    }   
    
    # SHORTCODE: 
    #   dcs_two_third
    # PAREMAETERS:
    # NOTES:     
    public function dcs_two_third($atts, $content=null, $code="")
    {
        $out = '';        
        $out .= '<div class="dcs-two-third">';
        $out .= do_shortcode($content);       
        $out .= '</div>';
        return $out;
    }     

    # SHORTCODE: 
    #   dcs_two_third_last
    # PAREMAETERS:
    # NOTES:     
    public function dcs_two_third_last($atts, $content=null, $code="")
    {
        $out = '';        
        $out .= '<div class="dcs-two-third-last">';
        $out .= do_shortcode($content);       
        $out .= '</div>';
        return $out;
    }       
    
    
    # SHORTCODE: 
    #   dcs_one_fourth
    # PAREMAETERS:
    # NOTES:     
    public function dcs_one_fourth($atts, $content=null, $code="")
    {
        $out = '';        
        $out .= '<div class="dcs-one-fourth">';
        $out .= do_shortcode($content);       
        $out .= '</div>';
        return $out;
    }     

    # SHORTCODE: 
    #   dcs_one_fourth_last
    # PAREMAETERS:
    # NOTES:     
    public function dcs_one_fourth_last($atts, $content=null, $code="")
    {
        $out = '';        
        $out .= '<div class="dcs-one-fourth-last">';
        $out .= do_shortcode($content);       
        $out .= '</div>';
        return $out;
    }   
    
    # SHORTCODE: 
    #   dcs_three_fourth
    # PAREMAETERS:
    # NOTES:     
    public function dcs_three_fourth($atts, $content=null, $code="")
    {
        $out = '';        
        $out .= '<div class="dcs-three-fourth">';
        $out .= do_shortcode($content);       
        $out .= '</div>';
        return $out;
    }     

    # SHORTCODE: 
    #   dcs_three_fourth_last
    # PAREMAETERS:
    # NOTES:     
    public function dcs_three_fourth_last($atts, $content=null, $code="")
    {
        $out = '';        
        $out .= '<div class="dcs-three-fourth-last">';
        $out .= do_shortcode($content);       
        $out .= '</div>';
        return $out;
    }     

    # SHORTCODE: 
    #   dcs_column
    # PAREMAETERS:
    #   width - column width
    #   mright - right margin
    #   mleft - left margin
    #   float - can be set to left or right, default left
    # NOTES:     
    public function dcs_column($atts, $content=null, $code="")
    {
        $out = ''; 
        $defatts = Array(
          'w' => 200,        
          'mright' => 40,    
          'mleft' => 0,      
          'float' => 'left',         
          'bgcolor' => '',    
          'border' => 'false',
          'bcolor' => '#EEEEEE',
          'bwidth' => 1,
          'padding' => '0px 0px 0px 0px', 
          'color' => '',   
          'align' => 'left'  
        ); 
        
        $atts = shortcode_atts($defatts, $atts);        
        $att_w = (int)$atts['w'];
        $att_mright = (int)$atts['mright'];
        $att_mleft = (int)$atts['mleft'];
        $att_float = $atts['float'];        
        $att_bgcolor = $atts['bgcolor'];            
        $att_border = $atts['border']; 
        $att_bcolor = $atts['bcolor'];
        $att_bwidth = (int)$atts['bwidth'];
        $att_padding = $atts['padding'];         
        $att_color = $atts['color']; 
        $att_align = $atts['align'];     
        
        $style = ' style="';
        $style .= 'padding:'.$att_padding.';';
        $style .= 'float:'.$att_float.';';
        $style .= 'width:'.$att_w.'px;';
        $style .= 'margin-right:'.$att_mright.'px;';
        $style .= 'margin-left:'.$att_mleft.'px;';

        if($att_color != '') { $style .= 'color:'.$att_color.';'; }
        $style .= 'text-align:'.$att_align.';';
        if($att_bgcolor != '') { $style .= 'background-color:'.$att_bgcolor.';'; }
        if($att_border == 'true')
        {
            $style .= 'border:'.$att_bwidth.'px solid '.$att_bcolor.';';    
        }
        $style .= '"';
               
        $out .= '<div '.$style.'>';
        $out .= do_shortcode($content);       
        $out .= '</div>';
        return $out;
    }  
     
    # SHORTCODE: 
    #   dcs_ul_list 
    # PAREMAETERS
    #   color - text color, can be set to any CSS valid value for color (default empty string, color will be not set)
    # NOTES:  
    public function dcs_ul_list($atts, $content=null, $code="")
    {
        $out = '';
        $defatts = Array(
          'type' => ''
        );        
        $atts = shortcode_atts($defatts, $atts);        
        $att_type = $atts['type'];          
                                                             
        $out .= '<ul class="list-'.$att_type.'">'.$content.'</ul>';

        return $out;
    }   
                            
    

  
} 
     
?>