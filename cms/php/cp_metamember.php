<?php
/**********************************************************************
* MODERN ELEGANCE WORDPRESS THEME 
* (Ideal For Business And Personal Use: Portfolio or Blog)     
* 
* File name:   
*      cp_metamember.php
* Brief:       
*      Part of theme control panel.
* Author:      
*      DigitalCavalry
* Author URI:
*      http://themeforest.net/user/DigitalCavalry
* Contact:
*      digitalcavalry@gmail.com 
***********************************************************************/

/*********************************************************** 
* Definitions
************************************************************/

/*********************************************************** 
* Class name:
*    CPMetaMemberOpt
* Descripton:
*    Implementation of CPMetaMemberOpt
***********************************************************/
class CPMetaMemberOpt extends DCC_MetaMultiple 
{            
    /*********************************************************** 
    * Constructor
    ************************************************************/
    public function __construct() 
    {
        $this->_names = array(
            'member_opt', 'member_opt_cbox', 
            'image_url', 'allow_readmore_cbox',
            'title', 'show_add_info_cbox', 'add_info');   
                       
        $this->_std = array(
            'member_opt_cbox' => false,
            'image_url' => '',
            'title' => '',
            'allow_readmore_cbox' => true,
            'add_info' => '',
            'show_add_info_cbox' => false
            );
                             
        $this->_title = 'TEAM MEMBER POST';
        $this->_type = 'member';
        $this->_desc = '';
    } // constructor 

    /*********************************************************** 
    * Public functions
    ************************************************************/    
    public function display()
    {       
        $value = $this->initDisplay();            
                
        global $post;
        echo '<span class="cms-meta-normal">POST INFORMATION: ID='.$post->ID.'</span>';         

        // title
        $out = ''; 
        $out .= '<div class="cms-custom-field-panel ">';
        $out .= '<div class="cms-meta-bold">'.$this->_title.'</div><br />';                       
        $out .= '<input type="hidden" name="member_opt_cbox" checked="checked" />';        
        
        // image url
        $out .= '<span class="cms-meta-normal">Optional image URL</span><br />';   
            $path = $value['image_url'];
            if($path != '')
            {
                $out .= '<img style="display:block;margin:5px 0px 5px 0px;" src="'.$path.'"/>';
            }
                                   
        $out .= '<input style="width:480px;" type="text" id="'.'image_url'.'_path" name="'.'image_url'.'" value="'.$value['image_url'].'" />'; 
        $out .= '<input style="width:140px;" class="cms-upload upload_image_button" type="button" value="Upload Image" name="'.'image_url'.'_path" /><br /><br />';                                           
        
        // member title
        $out .= '<span class="cms-meta-normal">Member title e.g <em>Lead programmer</em></span><br />'; 
        $out .= '<input style="width:480px;" type="text" name="title" value="'.$value['title'].'" /><br /><br />'; 

        // additional information
        $out .= '<span class="cms-meta-normal">Additional information e.g <em>MEMBER SINCE: July 2009</em></span><br />'; 
        $out .= '<input style="width:480px;" type="text" name="add_info" value="'.$value['add_info'].'" /><br /><br />'; 
        
        // other settings
        $out .= '<span class="cms-meta-normal">Other options:</span><br />';
        $out .= '<input type="checkbox" name="allow_readmore_cbox" '.$this->attrChecked($value['allow_readmore_cbox']).' /> Allow read more<br />';
        $out .= '<input type="checkbox" name="show_add_info_cbox" '.$this->attrChecked($value['show_add_info_cbox']).' /> Show additional information<br />';
        $out .= '<br />';       
        
        $out .= $this->getUpdateBtnHtmlCode();
        
        $out .= '</div>';
        echo $out;
    } 
} 
        
?>