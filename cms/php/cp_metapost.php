<?php
/**********************************************************************
* MODERN ELEGANCE WORDPRESS THEME 
* (Ideal For Business And Personal Use: Portfolio or Blog)     
* 
* File name:   
*      cp_metapost.php
* Brief:       
*      Part of theme control panel.
* Author:      
*      DigitalCavalry
* Author URI:
*      http://themeforest.net/user/DigitalCavalry
* Contact:
*      digitalcavalry@gmail.com 
***********************************************************************/

/*********************************************************** 
* Definitions
************************************************************/

/*********************************************************** 
* Class name:
*    CPMetaCommonPostOpt
* Descripton:
*    Implementation of CPMetaCommonPostOpt
***********************************************************/
class CPMetaCommonPostOpt extends DCC_MetaMultiple 
{        
    /*********************************************************** 
    * Constructor
    ************************************************************/
    public function __construct() 
    {
        $this->_names = array(
            'post_common_opt', 'post_common_opt_cbox', 'post_bg_img', 'post_bg_attachment',
            'post_bg_repeat', 'post_bg_color', 'post_bg_color_use_cbox', 'page_sid', 'page_sid_pos',
            'post_custom_cbox', 'post_fullwidth_cbox', 'post_generate_ogp_cbox',
            'post_hide_image_cbox', 'post_hide_author_cbox');              
        $this->_std = array(
            'post_common_opt_cbox' => false,
            'page_sid' => CMS_NOT_SELECTED,
            'page_sid_pos' => CMS_SIDEBAR_RIGHT,            
            'post_bg_img' => '',
            'post_bg_attachment' => 'scroll',
            'post_bg_repeat' => 'no-repeat',
            'post_bg_color' => 'E2E2E2',
            'post_bg_color_use_cbox' => false,
            'post_custom_cbox' => false,
            'post_fullwidth_cbox' => false,
            'post_generate_ogp_cbox' => false,
            'post_hide_image_cbox' => false,
            'post_hide_author_cbox' => false                     
            );
                             
        $this->_title = '[POST] Post Common Settings:';
        $this->_type = 'post';
        $this->_desc = '';
    } // constructor 

    /*********************************************************** 
    * Public functions
    ************************************************************/    
    public function display()
    {       
        $value = $this->initDisplay();            
                
        global $post;
        echo '<span class="cms-meta-normal">POST INFORMATION: ID='.$post->ID.'</span>';         

        // title
        $out = ''; 
        $out .= '<div class="cms-custom-field-panel ">';
        $out .= '<div class="cms-meta-bold">'.$this->_title.'</div><br />';                       
        $out .= '<input type="hidden" name="post_common_opt_cbox" checked="checked" />';        
        
        // sibedar
        $out .= '<span class="cms-meta-normal">Select page sidebar (if not set, default sidebar will be used)</span><br />';
        $out .= GetDCCPInterface()->getIGeneral()->printSidebarsList(300, 'page_sid', $value['page_sid']);         
        $out .= '<br /><br />';
        
        // sidebar position
        $out .= '<span class="cms-meta-normal">Page sidebar position</span><br />';
        $out .= '<input type="radio" name="page_sid_pos" '.($value['page_sid_pos'] == CMS_SIDEBAR_LEFT ? ' checked="checked"' : '').' value="'.CMS_SIDEBAR_LEFT.'" /> Left<br />';
        $out .= '<input type="radio" name="page_sid_pos" '.($value['page_sid_pos'] == CMS_SIDEBAR_RIGHT ? ' checked="checked"' : '').' value="'.CMS_SIDEBAR_RIGHT.'" /> Right<br />';
        $out .= '<input type="radio" name="page_sid_pos" '.($value['page_sid_pos'] == CMS_SIDEBAR_GLOBAL ? ' checked="checked"' : '').' value="'.CMS_SIDEBAR_GLOBAL.'" /> Global settings<br /><br />';         
        
         $out .= '<span class="cms-meta-normal">Post invidaul background</span><br />'; 
        $out .= '<input style="width:480px;" type="text" id="post_bg_img" name="post_bg_img" value="'.$value['post_bg_img'].'" /> '; 
        $out .= '<input style="width:180px;" class="cms-upload upload_image_button" type="button" value="Upload background image" name="post_bg_img" /><br /><br />';                                
        
        $out .= '<span class="cms-meta-normal">Background attachment</span><br />';
        $out .= '<input type="radio" name="post_bg_attachment" '.($value['post_bg_attachment'] == 'scroll' ? ' checked="checked"' : '').' value="scroll" /> Scroll<br />';
        $out .= '<input type="radio" name="post_bg_attachment" '.($value['post_bg_attachment'] == 'fixed' ? ' checked="checked"' : '').' value="fixed" /> Fixed<br /><br />';  
        
        $out .= '<span class="cms-meta-normal">Background repeat:</span><br />';
        $out .= '<input type="radio" '.$this->attrChecked($value['post_bg_repeat'] == 'repeat').' value="repeat" name="post_bg_repeat" /> Repeat<br />';         
        $out .= '<input type="radio" '.$this->attrChecked($value['post_bg_repeat'] == 'repeat-x').' value="repeat-x" name="post_bg_repeat" /> Repeat X<br />';
        $out .= '<input type="radio" '.$this->attrChecked($value['post_bg_repeat'] == 'repeat-y').' value="repeat-y" name="post_bg_repeat" /> Repeat Y<br />';
        $out .= '<input type="radio" '.$this->attrChecked($value['post_bg_repeat'] == 'no-repeat').' value="no-repeat" name="post_bg_repeat" /> No repeat<br /><br />';        
        
        // bg color
         $out .= '<span class="cms-meta-normal">Background color</span><br />';         
         $out .= '<input style="width:70px;text-align:center;" type="text" class="colorpicker {hash:true}" value="'.$value['post_bg_color'].'" name="post_bg_color" /> Background color <br /><br />'; 
         $out .= '<input type="checkbox" name="post_bg_color_use_cbox" '.$this->attrChecked($value['post_bg_color_use_cbox']).' /> Use background color<br /><br />';                  
        
        // other settings
        $out .= '<span class="cms-meta-normal">Other options:</span><br />';
        $out .= '<input type="checkbox" name="post_custom_cbox" '.$this->attrChecked($value['post_custom_cbox']).' /> Custom content, without default render<br />';
        $out .= '<input type="checkbox" name="post_fullwidth_cbox" '.$this->attrChecked($value['post_fullwidth_cbox']).' /> Full width (works only with <strong>custom content</strong> option)<br />';         
        $out .= '<input type="checkbox" name="post_hide_image_cbox" '.$this->attrChecked($value['post_hide_image_cbox']).' /> Hide main image on single page<br />';  
        $out .= '<input type="checkbox" name="post_hide_author_cbox" '.$this->attrChecked($value['post_hide_author_cbox']).' /> Hide author information on single page<br />';
        $out .= '<input type="checkbox" name="post_generate_ogp_cbox" '.$this->attrChecked($value['post_generate_ogp_cbox']).' /> Generate Open Graph Protocol information for this post, needed for facebook sharing (if checked, please add excerpt with plain text, without any HTML tags and quotation marks)<br />';        
        $out .= '<br />';             
        
        $out .= $this->getUpdateBtnHtmlCode();  
        
        $out .= '</div>';
        echo $out;
    } 
} // class CPMetaCommonPostOpt  

/*********************************************************** 
* Class name:
*    CPMetaPostOpt
* Descripton:
*    Implementation of CPMetaPostOpt
***********************************************************/
class CPMetaPostOpt extends DCC_MetaMultiple 
{        
    const WIDTH = CMS_LC_BLOG_IMG_WIDTH;
    const HEIGHT = CMS_LC_BLOG_IMG_HEIGHT;    
    
    /*********************************************************** 
    * Constructor
    ************************************************************/
    public function __construct() 
    {
        $this->_names = array(
            'post_opt', 'post_opt_cbox', 'image_url', 'image_desc', 'images_id', 'layout', 'video', 'video_use_cbox',
            'wordtube_id', 'wordtube_use_cbox', 'wordtube_height', 'post_custom_image', 'post_custom_image_cbox');              
        $this->_std = array(
            'post_opt_cbox' => false,
            'image_url' => '',
            'image_desc' => '',
            'images_id' => '',
            'layout' => CMS_POST_LAYOUT_BIG,
            'video' => '',
            'video_use_cbox' => false,
            'wordtube_id' => '',
            'wordtube_use_cbox' => false,
            'wordtube_height' => 465,
            'post_custom_image' => '', 
            'post_custom_image_cbox' => false           
            );
                             
        $this->_title = '[POST] Post Settings:';
        $this->_type = 'post';
        $this->_desc = '';
    } // constructor 

    /*********************************************************** 
    * Public functions
    ************************************************************/    
    public function display()
    {       
        $value = $this->initDisplay();            

        // title
        $out = ''; 
        $out .= '<div class="cms-custom-field-panel ">';
        $out .= '<div class="cms-meta-bold">'.$this->_title.'</div><br />';                       
        $out .= '<input type="hidden" name="post_opt_cbox" checked="checked" />';        
        
        $out .= '<span class="cms-meta-normal">Post main image URL, (you can use image with any size, but recomended size is '.CPMetaPostOpt::WIDTH.'x'.CPMetaPostOpt::HEIGHT.' px, you can also set image ID from NextGEN Gallery)</span><br />';   
            $path = dcf_isNGGImageID($value['image_url']);
            if($path != '')
            {
                $out .= '<img style="display:block;" src="'.$path.'"/>';
            }
                                   
        // box value
        $out .= '<input style="width:480px;" type="text" id="'.'image_url'.'_path" name="'.'image_url'.'" value="'.$value['image_url'].'" />'; 
        $out .= '<input style="width:140px;" class="cms-upload upload_image_button" type="button" value="Upload ('.CPMetaPostOpt::WIDTH.'x'.CPMetaPostOpt::HEIGHT.')" name="'.'image_url'.'_path" /><br /><br />';                                
        
        $out .= '<span class="cms-meta-normal">Type here description for post image or leave this field empty</span><br />';     
        $out .= '<input style="width:480px;" type="text" id="image_desc" name="image_desc" value="'.$value['image_desc'].'" /><br /><br />'; 

        $out .= '<span class="cms-meta-normal">Post Gallery: Images ID - type here comma separated images ID\'s from NextGEN Gallery (e.g 4,6,76,3,17)</span><br />';     
        $out .= '<input style="width:480px;" type="text" id="images_id" name="images_id" value="'.$value['images_id'].'" /><br /><br />'; 
        
        $out .= '<span class="cms-meta-normal">Select post layout</span><br />';
        $out .= '<input type="radio" name="layout" '.($value['layout'] == CMS_POST_LAYOUT_SMALL ? ' checked="checked"' : '').' value="'.CMS_POST_LAYOUT_SMALL.'" /> Small image on left <span class="cms-meta-light">(date)</span><br />';
        $out .= '<input type="radio" name="layout" '.($value['layout'] == CMS_POST_LAYOUT_MEDIUM ? ' checked="checked"' : '').' value="'.CMS_POST_LAYOUT_MEDIUM.'" /> Medium image on left <span class="cms-meta-light">(tags, categories, date, author, comments count)</span><br />';
        $out .= '<input type="radio" name="layout" '.($value['layout'] == CMS_POST_LAYOUT_BIG ? ' checked="checked"' : '').' value="'.CMS_POST_LAYOUT_BIG.'" /> Big image <span class="cms-meta-light">(tags, categories, date, author, comments count)</span><br />';         
        $out .= '<br >'; 
        
        // custom image
        $out .= '<span class="cms-meta-normal">Custom image area content:</span><br />';
        $out .= '<textarea style="width:640px;height:160px;" name="post_custom_image">'.stripcslashes($value['post_custom_image']).'</textarea><br />';
        $out .= '<input type="checkbox" name="post_custom_image_cbox" '.$this->attrChecked($value['post_custom_image_cbox']).' /> Use custom image code<br /><br />';           
        
        if($value['video'] != '')
        {
            $out .= $value['video'];
        }
        
        $out .= '<br /><span class="cms-meta-normal">Video code (optimal size 608x349)</span><br />';
        $out .= '<textarea style="font-size:11px;width:600px;height:160px;" name="video">'.$value['video'].'</textarea><br />';
        $out .= '<input type="checkbox" name="video_use_cbox" '.$this->attrChecked($value['video_use_cbox']).' /> Use video code<br /><br />';   
        
        $out .= '<span class="cms-meta-normal">WordTube media ID</span><br />';                               
        $out .= '<input type="text" style="width:80px;text-align:center;" name="wordtube_id" value="'.$value['wordtube_id'].'" /> Media ID<br />';  
        $out .= '<input type="text" style="width:80px;text-align:center;" name="wordtube_height" value="'.$value['wordtube_height'].'" /> Height<br />'; 
        $out .= '<input type="checkbox" name="wordtube_use_cbox" '.$this->attrChecked($value['wordtube_use_cbox']).' /> Use wordtube media (will overwrite video)<br /><br />'; 
    
        
        $out .= $this->getUpdateBtnHtmlCode(); 
                
        $out .= '</div>';
        echo $out;
    } 
} // class CPMetaPostOpt    
        
?>