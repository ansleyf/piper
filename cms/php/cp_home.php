<?php
/**********************************************************************
* MODERN ELEGANCE WORDPRESS EDITION 
* (Ideal For Business And Personal Use: Portfolio or Blog)     
* 
* File name:   
*      cp_home.php
* Brief:       
*      Part of theme control panel.
* Author:      
*      DigitalCavalry
* Author URI:
*      http://themeforest.net/user/DigitalCavalry
* Contact:
*      digitalcavalry@gmail.com 
***********************************************************************/

/*********************************************************** 
* Definitions
************************************************************/

/*********************************************************** 
* Class name:
*    CPHome
* Descripton:
*    Implementation of CPHome 
***********************************************************/
class CPHome extends DCC_CPBaseClass
{     
    const TABS_ORDER_DESC = 1;
    const TABS_ORDER_ASC = 2;
    
    const CONTENT_ON_FULLWIDTH = 1;
    const CONTENT_ON_COLUMN = 2;
    
    const POSTS_UNDER_CONTENT = 1;
    const POSTS_UNDER_SLIDER = 2;
    
    /*********************************************************** 
    * Constructor
    ************************************************************/
    public function __construct() 
    {
        $this->_DBIDOPT_HOMEPAGE_OPT = CMS_THEME_NAME_UPPERCASE.'_HOMEPAGE_OPT';
        $this->_DBIDOPT_HOME_SECTIONS_ORDER_OPT = CMS_THEME_NAME_UPPERCASE.'_HOME_SECTIONS_ORDER_OPT'; 
        
        // sections order
        $this->_order = get_option($this->_DBIDOPT_HOME_SECTIONS_ORDER_OPT);
        if (!is_array($this->_order))
        {
            add_option($this->_DBIDOPT_HOME_SECTIONS_ORDER_OPT, $this->_orderDef);
            $this->_order = get_option($this->_DBIDOPT_HOME_SECTIONS_ORDER_OPT);
        }          
        
        // home
        $this->_home = get_option($this->_DBIDOPT_HOMEPAGE_OPT);
        if (!is_array($this->_home))
        {
            add_option($this->_DBIDOPT_HOMEPAGE_OPT, $this->_homeDef);
            $this->_home = get_option($this->_DBIDOPT_HOMEPAGE_OPT);
        }           

    } // constructor 
    
    /*********************************************************** 
    * Public members
    ************************************************************/      
    
    public function saveSectionsOrder($set)
    {
        $neworder = array();
        foreach($set as $id)
        {
            $neworder[$id] = $this->_order[$id];    
        }
        
        $this->_order = $neworder;
        update_option($this->_DBIDOPT_HOME_SECTIONS_ORDER_OPT, $this->_order);       
    }
    
    /*********************************************************** 
    * Private members
    ************************************************************/      
     private $_DBIDOPT_HOMEPAGE_OPT = null; 
     private $_DBIDOPT_HOME_SECTIONS_ORDER_OPT = null; 
     
     private $_order = array();
     private $_orderDef = array(
        'homeorder-featured-video' => 'Homepage featured video',
        'homeorder-featured-gallery' => 'Homepage featured gallery',
      /*  'homeorder-blog-post-thumbs' => 'Homepage blog post thumbs', */
        'homeorder-posts-main-stream' => 'Homepage posts main stream',
        'homeorder-photo-news' => 'Homepage photo news',
        'homeorder-homepage-content' => 'Homepage content',
        'homeorder-tabs' => 'Homepage tabs'
  //      'homeorder-featured-story' => 'Homepage featured story'      
     );           
     
     private $_home;
     private $_homeDef = Array(
        // tabs
        'home_tabs_show' => true,
        'home_tabs_head_text' => 'Homepage Tabs',
        'home_tabs_hide_head' => true,        
        'home_tabs_order' => CPHome::TABS_ORDER_DESC,
        'home_tabs_count' => 5,
        'home_tabs_date_show' => true,
        'home_tabs_date_format' => 'F j, Y',
        'home_tabs_default' => CMS_NOT_SELECTED,
       // 'home_tabs_autoresize' => false,
       // 'home_tabs_autoresize_w' => 300,
       // 'home_tabs_autoresize_h' => 200,
       // 'home_tabs_keep_ratio' => false,
       // 'home_tabs_disable_gallery' => false,
        'home_tabs_selected' => '',
        'home_tabs_selected_use' => false,
       // 'home_tabs_btn_color' => 'transparent',
       
        // posts thumbs
        'home_pt_show' => false,
        'home_pt_rows' => 1,
        'home_pt_head_text' => 'Recent posts',
        'home_pt_hide_head' => true,
        'home_pt_selected' => '',
        'home_pt_selected_use' => false,
        'home_pt_categories' => '',
        'home_pt_categories_use' => false,
        'home_pt_most_use' => false,        
        'home_pt_most_cats' => '',
        'home_pt_most_cats_use' => false,
        'home_pt_most_from_last' => 15,
        'home_pt_place' => CPHome::POSTS_UNDER_SLIDER,
       
        // main stream
        'home_ms_show' => false,
        'home_ms_posts_count' => 5,
        'home_ms_show_pages' => true,
        'home_ms_layout' => CMS_POST_LAYOUT_MEDIUM,
        'home_ms_force_layout' => true,
        'home_ms_categories' => '',
        'home_ms_categories_use' => '',
        'home_ms_ex_categories' => '',
        'home_ms_head_text' => 'Latest from blog',
        'home_ms_selected' => '',
        'home_ms_selected_use' => false,
       
        // featuerd video
        'home_fv_show' => false,
        'home_fv_head_text' => 'Featured Video / Music',
        'home_fv_media_id' => 0,
        'home_fv_link_page' => CMS_NOT_SELECTED,
        'home_fv_link' => '',
        'home_fv_link_type' => CMS_LINK_TYPE_PAGE,
        'home_fv_link_use' => false,
        'home_fv_link_target' => CMS_LINK_TARGET_SELF,
        'home_fv_link_text' => 'View&nbsp;more',
       
        // featured gallery   
        'home_fg_show' => false,
        'home_fg_head_text' => 'Featured Gallery',
        'home_fg_media_id' => 0,
        'home_fg_link_page' => CMS_NOT_SELECTED,
        'home_fg_link' => '',
        'home_fg_link_type' => CMS_LINK_TYPE_PAGE,
        'home_fg_link_use' => false,
        'home_fg_link_target' => CMS_LINK_TARGET_SELF,
        'home_fg_link_text' => 'View&nbsp;more',
       
        // home content
        'home_ct_show' => false,
        'home_ct_use_page_content' => true,
        'home_ct_content' => '',
        'home_ct_place' => CPHome::CONTENT_ON_COLUMN,
        'home_ct_head_text' => 'Homepage content',
        'home_ct_show_head' => true,
        'home_ct_page' => CMS_NOT_SELECTED,
        'home_ct_add_padding' => false,
        'home_ct_show_column_and_sid' => true,
       
        // featured story
 /*       'home_story_show' => false,
        'home_story_head_text' => 'Featured story',
        'home_story_list' => '',
        'home_story_height' => 300,
        'home_story_overwrite' => false, 
        'home_story_group' => false,  
        'home_story_link' => '',
        'home_story_link_use' => false, 
        'home_story_link_type' => CMS_LINK_TYPE_PAGE,
        'home_story_link_page' => CMS_NOT_SELECTED,
        'home_story_link_target' => CMS_LINK_TARGET_SELF, 
        'home_story_fit' => true,
        'home_story_showdesc' => true,
        'home_story_showtitle' => true           
   */            
     );
     private $_saved = false;
           
     private $_panelsubmenu = array (
        'home-tabs' => 'Homepage tabs',
        'home-postthumbs' => 'Homepage blog post thumbs',
        'home-mainstream' => 'Homepage posts main stream',
        'home-featured-video' => 'Homepage featured video',
        'home-featured-gallery' => 'Homepage featured gallery',
   //     'home-featured-story' => 'Homepage featured story',
        'home-content' => 'Homepage content'      
     );           
                                               
    /*********************************************************** 
    * Public functions
    ************************************************************/               
 
     public function renderTab()
     {
         echo '<div class="cms-content-wrapper">';
         echo '<a name="gototop"></a>'; 
         $this->process();
         $this->renderPanelSubmenu($this->_panelsubmenu);
         $this->renderCMS();
         echo '</div>';
     }

     public function renderHomepageSections()
     {
        foreach($this->_order as $key => $value)
        {
            $this->renderHomepageOneSection($key);
        }
     }   
     
     public function renderHomepageOneSection($id)
     {
         switch($id)
         {
            case 'homeorder-featured-video':
                GetDCCPInterface()->getIRenderer()->renderHomeFeaturedVideo();    
            break;
            
            case 'homeorder-featured-gallery':
                GetDCCPInterface()->getIrenderer()->renderHomeFeaturedGallery(); 
            break; 
            
            case 'homeorder-tabs':
                GetDCCPInterface()->getIRenderer()->renderHomepageTabs(); 
            break;
          /*  
            case 'homeorder-blog-post-thumbs':
                GetDCCPInterface()->getIRenderer()->renderHomeBlogPosts();
            break;
          */  
            case 'homeorder-posts-main-stream':
                GetDCCPInterface()->getIRenderer()->renderHomeMainStreamPosts(); 
            break;            
            
            case 'homeorder-homepage-content':
                GetDCCPInterface()->getIHome()->getHomepageContent(CPHome::CONTENT_ON_COLUMN);     
            break;
            
//            case 'homeorder-featured-story':
//                GetDCCPInterface()->GetIHome()->renderFeaturedStory();
//            break;                                                               
         }         
     }

     
     // homepage tabs
     public function isTabsShowed() { return $this->_home['home_tabs_show']; }     
     public function getTabsOrder()
     {
         $order = 'DESC';
         if($this->_home['home_tabs_order'] == CPHome::TABS_ORDER_ASC)
         {
             $order = 'ASC';
         }
         return $order;
     }

     public function getTabsCount() { return $this->_home['home_tabs_count']; }     
     public function isTabDateShowed() { return $this->_home['home_tabs_date_show']; }     
     public function getTabDateFormat() { return $this->_home['home_tabs_date_format']; }
     public function getTabHeadText() { return $this->_home['home_tabs_head_text']; } 
     public function isTabHeadShowed() { return $this->_home['home_tabs_hide_head']; } 
     public function getDefaultTab() { return $this->_home['home_tabs_default']; }     
     public function isResizeEnabled() { return $this->_home['home_tabs_autoresize']; }     
     public function isResizeKeepSizeEnabled() { return $this->_home['home_tabs_keep_ratio']; }            
     public function getResizeWidth() { return $this->_home['home_tabs_autoresize_w']; }       
     public function getResizeHeight() { return $this->_home['home_tabs_autoresize_h']; }         
     public function isTabGalleryDisabled() { return $this->_home['home_tabs_disable_gallery']; }      
     public function useTabsUserList() { return $this->_home['home_tabs_selected_use']; }        
     public function getTabsUserList() { return $this->_home['home_tabs_selected']; }    
     public function getTabsBtnColor() { return $this->_home['home_tabs_btn_color']; }    
                                               
     // homepage blog posts thumbs     
     public function isPostThumbsShowed() { return $this->_home['home_pt_show']; }  
     public function isPostThumbsHeadShowed() { return $this->_home['home_pt_hide_head']; }
     public function getPostThumbsRowsCount() { return $this->_home['home_pt_rows']; } 
     public function getPostThumbsHeadText() { return $this->_home['home_pt_head_text']; } 
     public function getPostThumbsSelected() { return $this->_home['home_pt_selected']; } 
     public function isPostThumbsSelectedUsed() { return $this->_home['home_pt_selected_use']; } 
     public function getPostThumbsCats() { return $this->_home['home_pt_categories']; } 
     public function getPostThumbsPlace() { return $this->_home['home_pt_place']; }
     public function isPostThumbsCatsUsed() { return $this->_home['home_pt_categories_use']; } 
     
     public function isPostThumbsMostUsed() { return $this->_home['home_pt_most_use']; }                  
     public function getPostThumbsMostCats() { return $this->_home['home_pt_most_cats']; } 
     public function isPostThumbsMostCatsUsed() { return $this->_home['home_pt_most_cats_use']; }  
     public function getPostThumbsMostFromLastCount() { return $this->_home['home_pt_most_from_last']; }               
                                                       
     // homepage main stream posts
     public function isPostMainStreamShowed() { return $this->_home['home_ms_show']; } 
     public function getPostMainStreamCount() { return (int)$this->_home['home_ms_posts_count']; } 
     public function isPostMainStreamPaged() { return $this->_home['home_ms_show_pages']; }                                
     public function getPostMainStreamHeadText() { return $this->_home['home_ms_head_text']; }                        
     
     public function isPostMainStreamCatsUsed() { return $this->_home['home_ms_categories_use']; } 
     public function getPostMainStreamCats() { return $this->_home['home_ms_categories']; }
     public function getPostMainStreamExCats() { return $this->_home['home_ms_ex_categories']; } 
     public function isPostMainStreamSelectedUsed() { return $this->_home['home_ms_selected_use']; } 
     public function getPostMainStreamSelected() { return $this->_home['home_ms_selected']; }        
     public function getPostMainStreamLayout() { return $this->_home['home_ms_layout']; } 
     
     // homepage featured video
     public function isFeaturedVideoShowed() { return $this->_home['home_fv_show']; }   
     public function getFeaturedVideoHeadText() { return stripcslashes($this->_home['home_fv_head_text']); }
     public function getFeaturedVideoID() { return $this->_home['home_fv_media_id']; }           
     public function getFeaturedVideoLink() 
     { 
         $out = '';
         if($this->_home['home_fv_link_use'])
         {
             $out .= ' <a class="read-more" ';
                 if($this->_home['home_fv_link_type'] == CMS_LINK_TYPE_PAGE)
                 {
                    $out .= 'href="'.get_permalink($this->_home['home_fv_link_page']).'" ';        
                 } else
                 {
                    $out .= 'href="'.$this->_home['home_fv_link'].'" ';    
                 }  
                 
                 if($this->_home['home_fv_link_target'] == CMS_LINK_TARGET_BLANK)
                 {
                    $out .= ' target="_blank" ';        
                 } else
                 {
                    $out .= ' target="_self" ';    
                 }                   
                 $out .= '>&raquo;&nbsp;';
                 $out .= stripcslashes($this->_home['home_fv_link_text']);
             $out .= '</a>';           
         }
         
         return $out;
     }   
     
     // homepage featured gallery
     public function isFeaturedGalleryShowed() { return $this->_home['home_fg_show']; }   
     public function getFeaturedGalleryHeadText() { return stripcslashes($this->_home['home_fg_head_text']); }
     public function getFeaturedGalleryID() { return $this->_home['home_fg_media_id']; }                 
     public function getFeaturedGalleryLink() 
     { 
         $out = '';
         if($this->_home['home_fg_link_use'])
         {
             $out .= ' <a class="read-more" ';
                 if($this->_home['home_fg_link_type'] == CMS_LINK_TYPE_PAGE)
                 {
                    $out .= 'href="'.get_permalink($this->_home['home_fg_link_page']).'" ';        
                 } else
                 {
                    $out .= 'href="'.$this->_home['home_fg_link'].'" ';    
                 }  
                 
                 if($this->_home['home_fg_link_target'] == CMS_LINK_TARGET_BLANK)
                 {
                    $out .= ' target="_blank" ';        
                 } else
                 {
                    $out .= ' target="_self" ';    
                 }                   
                 $out .= '>&raquo;&nbsp;';
                 $out .= stripcslashes($this->_home['home_fg_link_text']);
             $out .= '</a>';           
         }
         
         return $out;
     } 
     
     // homepage featured story
/*     public function renderFeaturedStory()
     {
         if($this->_home['home_story_show'])
         {
             $out = '';
             
             $name = ' name="'.$this->_home['home_story_head_text'].'" ';
             $list = ' list="'.$this->_home['home_story_list'].'" ';
             $w = ' w="620" ';
             $h = ' h="'.$this->_home['home_story_height'].'" ';  
             $margin = ' margin="0px auto 20px auto" ';
             
             $fit = ' fit="'.($this->_home['home_story_fit'] ? 'true' : 'false').'" '; 
             $overwrite = ' overwrite="'.($this->_home['home_story_overwrite'] ? 'true' : 'false').'" ';              
             $group = ' group="'.($this->_home['home_story_group'] ? 'true' : 'false').'" '; 
             $showtitle = ' showtitle="'.($this->_home['home_story_showtitle'] ? 'true' : 'false').'" '; 
             $showdesc = ' showdesc="'.($this->_home['home_story_showdesc'] ? 'true' : 'false').'" '; 
             
             $url = '';
             if($this->_home['home_story_link_use']) 
             {
                 if($this->_home['home_story_link_type'] == CMS_LINK_TYPE_PAGE and $this->_home['home_story_link_page'] != CMS_NOT_SELECTED)
                 {
                    $url = get_permalink($this->_home['home_story_link_page']);       
                 } else { $url = $this->_home['home_story_link']; }
             }
             
             $link = ' link="'.$url.'" ';
             $target = '';
             if($this->_home['home_story_link_target'] == CMS_LINK_TARGET_BLANK) { $target = ' target="_blank" '; } 
             else { $target = ' target="_self" '; }
             
             $scode = "[dcs_story $name $list $w $h $margin $fit $overwrite $group $showtitle $showdesc $link $target /]";
             $out .= do_shortcode($scode);
             
             echo $out;
         }
     }
*/     
     // homepage conent
     public function isHomepageColumnAndSidShowed() { return $this->_home['home_ct_show_column_and_sid']; }
     public function getHomepageContent($place)
     {
         if(!$this->_home['home_ct_show']) { return; }
         if($place != $this->_home['home_ct_place']) { return; }
         
         $conent = '';
         if($this->_home['home_ct_use_page_content'] and $this->_home['home_ct_page'] != CMS_NOT_SELECTED)
         {
            global $wpdb;
            $page_content = $wpdb->get_row("SELECT post_content FROM $wpdb->posts WHERE ID = ".$this->_home['home_ct_page']); 
            
            if($page_content !== null)
            {
                $content = apply_filters('the_content', $page_content->post_content);
            }
         } else
         {
            $content = apply_filters('the_content', $this->_home['home_ct_content']);    
         }
         
         $out = '';
         if($this->_home['home_ct_place'] == CPHome::CONTENT_ON_FULLWIDTH)
         {
            $out .= '<div class="home-selected-content-padding-top-block"></div>';
         }             
         
         $out .= '<div class="home-selected-content">';             
            if($this->_home['home_ct_show_head'])
            {
                $out .= '<div class="head"><span class="head-text">'.$this->_home['home_ct_head_text'].'</span></div>';
            }
            $out .= $content;
            $out .= '<div class="clear-both"></div>';
            if($this->_home['home_ct_add_padding'])
            {
                $out .= '<div class="padding-bottom-block"></div>';
            }
            
         $out .= '</div>'; 
         echo $out;                          
     }        
                                          
    /*********************************************************** 
    * Private functions
    ************************************************************/      
    
    private function process()
    {
        if(isset($_POST['home_save_tabs_opt']))
        {
            
            $this->_home['home_tabs_show'] = isset($_POST['home_tabs_show']) ? true : false;
            $this->_home['home_tabs_head_text'] = $_POST['home_tabs_head_text'];
            $this->_home['home_tabs_hide_head'] = isset($_POST['home_tabs_hide_head']) ? true : false;            
            $this->_home['home_tabs_order'] = $_POST['home_tabs_order'];
            $this->_home['home_tabs_count'] = ((int)$_POST['home_tabs_count'] == 0) ? 1 : (int)$_POST['home_tabs_count'];
            $this->_home['home_tabs_date_show'] = isset($_POST['home_tabs_date_show']) ? true : false;
            $this->_home['home_tabs_date_format'] = $_POST['home_tabs_date_format']; 
            $this->_home['home_tabs_default'] = $_POST['home_tabs_default']; 
            
          //  $this->_home['home_tabs_autoresize'] = isset($_POST['home_tabs_autoresize']) ? true : false; 
          //  $this->_home['home_tabs_autoresize_w'] = $_POST['home_tabs_autoresize_w'];
          //  $this->_home['home_tabs_autoresize_h'] = $_POST['home_tabs_autoresize_h'];
          //  $this->_home['home_tabs_keep_ratio'] = isset($_POST['home_tabs_keep_ratio']) ? true : false;      
          //  $this->_home['home_tabs_disable_gallery'] = isset($_POST['home_tabs_disable_gallery']) ? true : false;
            
            
            $this->_home['home_tabs_selected'] = $_POST['home_tabs_selected']; 
            $this->_home['home_tabs_selected_use'] = isset($_POST['home_tabs_selected_use']) ? true : false;           
          //  $this->_home['home_tabs_btn_color'] = $_POST['home_tabs_btn_color'];                                                                                          
            update_option($this->_DBIDOPT_HOMEPAGE_OPT, $this->_home);
            $this->_saved = true;
        }      
       
        if(isset($_POST['home_save_postthumbs_opt']))
        {
            $this->_home['home_pt_show'] = isset($_POST['home_pt_show']) ? true : false; 
            $this->_home['home_pt_rows'] = $_POST['home_pt_rows'];
            $this->_home['home_pt_head_text'] = $_POST['home_pt_head_text'];
            $this->_home['home_pt_hide_head'] = isset($_POST['home_pt_hide_head']) ? true : false;
            $this->_home['home_pt_place'] = $_POST['home_pt_place'];
            
            $this->_home['home_pt_selected'] = $_POST['home_pt_selected'];
            $this->_home['home_pt_selected_use'] = isset($_POST['home_pt_selected_use']) ? true : false;         
            $this->_home['home_pt_categories'] = $_POST['home_pt_categories'];
            $this->_home['home_pt_categories_use'] = isset($_POST['home_pt_categories_use']) ? true : false;                                                                   

            $this->_home['home_pt_most_use'] = isset($_POST['home_pt_most_use']) ? true : false;  
            $this->_home['home_pt_most_cats'] = $_POST['home_pt_most_cats']; 
            $this->_home['home_pt_most_cats_use'] = isset($_POST['home_pt_most_cats_use']) ? true : false;  
            $this->_home['home_pt_most_from_last'] = ((int)$_POST['home_pt_most_from_last'] < 1) ? 10 : (int)$_POST['home_pt_most_from_last'];                                        
                                                                                                                                                                                                                                                      
            update_option($this->_DBIDOPT_HOMEPAGE_OPT, $this->_home);
            $this->_saved = true;
        }               
        
        if(isset($_POST['home_save_mainstream_opt']))
        {                        
            $this->_home['home_ms_show'] = isset($_POST['home_ms_show']) ? true : false; 
            $this->_home['home_ms_posts_count'] = $_POST['home_ms_posts_count'];                                                                   
            $this->_home['home_ms_show_pages'] = isset($_POST['home_ms_show_pages']) ? true : false;               

            $this->_home['home_ms_layout'] = $_POST['home_ms_layout']; 
            $this->_home['home_ms_categories'] = $_POST['home_ms_categories']; 
            $this->_home['home_ms_categories_use'] = isset($_POST['home_ms_categories_use']) ? true : false; 
            $this->_home['home_ms_ex_categories'] = $_POST['home_ms_ex_categories'];                                                                                                                                                     
            $this->_home['home_ms_head_text'] = $_POST['home_ms_head_text']; 
               
            $this->_home['home_ms_selected'] = $_POST['home_ms_selected']; 
            $this->_home['home_ms_selected_use'] = isset($_POST['home_ms_selected_use']) ? true : false;                                                                                   
                                                                                                        
            update_option($this->_DBIDOPT_HOMEPAGE_OPT, $this->_home);
            $this->_saved = true;
        }  
        
        if(isset($_POST['home_save_featuredvideo_opt']))
        {                        
            $this->_home['home_fv_show'] = isset($_POST['home_fv_show']) ? true : false;                                                                                                                                                   
            $this->_home['home_fv_head_text'] = $_POST['home_fv_head_text'];
            $this->_home['home_fv_media_id'] = (int)$_POST['home_fv_media_id'];            
            
            $this->_home['home_fv_link_page'] = $_POST['home_fv_link_page']; 
            $this->_home['home_fv_link_use'] = isset($_POST['home_fv_link_use']) ? true : false; 
            $this->_home['home_fv_link_type'] = $_POST['home_fv_link_type']; 
            $this->_home['home_fv_link'] = $_POST['home_fv_link']; 
            $this->_home['home_fv_link_target'] = $_POST['home_fv_link_target'];                    
            $this->_home['home_fv_link_text'] = $_POST['home_fv_link_text'];                                                                        
                                                                                                                                        
            update_option($this->_DBIDOPT_HOMEPAGE_OPT, $this->_home);
            $this->_saved = true;
        }        
        
        if(isset($_POST['home_save_featuredgallery_opt']))
        {                        
            $this->_home['home_fg_show'] = isset($_POST['home_fg_show']) ? true : false;                                                                                                                                                   
            $this->_home['home_fg_head_text'] = $_POST['home_fg_head_text'];
            $this->_home['home_fg_media_id'] = (int)$_POST['home_fg_media_id'];            
            
            $this->_home['home_fg_link_page'] = $_POST['home_fg_link_page']; 
            $this->_home['home_fg_link_use'] = isset($_POST['home_fg_link_use']) ? true : false; 
            $this->_home['home_fg_link_type'] = $_POST['home_fg_link_type']; 
            $this->_home['home_fg_link'] = $_POST['home_fg_link']; 
            $this->_home['home_fg_link_target'] = $_POST['home_fg_link_target'];                    
            $this->_home['home_fg_link_text'] = $_POST['home_fg_link_text'];                                                                        
                                                                                                                                        
            update_option($this->_DBIDOPT_HOMEPAGE_OPT, $this->_home);
            $this->_saved = true;
        }            
        
        /*
        if(isset($_POST['home_save_featuredstory_opt']))
        {                        
            $this->_home['home_story_show'] = isset($_POST['home_story_show']) ? true : false;                                                                                                                                                   
            $this->_home['home_story_head_text'] = $_POST['home_story_head_text']; 
            $this->_home['home_story_list'] = $_POST['home_story_list']; 
            $this->_home['home_story_height'] = $_POST['home_story_height']; 
            
            $this->_home['home_story_link'] = $_POST['home_story_link']; 
            $this->_home['home_story_link_type'] = $_POST['home_story_link_type'];
            $this->_home['home_story_link_page'] = $_POST['home_story_link_page'];
            $this->_home['home_story_link_target'] = $_POST['home_story_link_target'];
            
            $this->_home['home_story_fit'] = isset($_POST['home_story_fit']) ? true : false;
            $this->_home['home_story_overwrite'] = isset($_POST['home_story_overwrite']) ? true : false;
            $this->_home['home_story_group'] = isset($_POST['home_story_group']) ? true : false; 
            $this->_home['home_story_link_use'] = isset($_POST['home_story_link_use']) ? true : false;              
            $this->_home['home_story_showdesc'] = isset($_POST['home_story_showdesc']) ? true : false;
            $this->_home['home_story_showtitle'] = isset($_POST['home_story_showtitle']) ? true : false; 
                                                                                                                              
            update_option($this->_DBIDOPT_HOMEPAGE_OPT, $this->_home);
            $this->_saved = true;
        }   
        */
        
        
        if(isset($_POST['home_save_homecontent_opt']))
        {                        
            $this->_home['home_ct_show'] = isset($_POST['home_ct_show']) ? true : false;                                                                                                                                                   
            $this->_home['home_ct_use_page_content'] = isset($_POST['home_ct_use_page_content']) ? true : false;            
            $this->_home['home_ct_content'] = $_POST['home_ct_content'];          
            $this->_home['home_ct_place'] = $_POST['home_ct_place'];                        
            $this->_home['home_ct_head_text'] = $_POST['home_ct_head_text']; 
            $this->_home['home_ct_show_head'] = isset($_POST['home_ct_show_head']) ? true : false;                                                                                 
            $this->_home['home_ct_page'] = $_POST['home_ct_page'];     
            $this->_home['home_ct_add_padding'] = isset($_POST['home_ct_add_padding']) ? true : false;
            $this->_home['home_ct_show_column_and_sid'] = isset($_POST['home_ct_show_column_and_sid']) ? true : false;  
                                                                                                                              
            update_option($this->_DBIDOPT_HOMEPAGE_OPT, $this->_home);
            $this->_saved = true;
        }      
        
        
        
        if(isset($_POST['home_restore_order']))
        {
            $this->_order = $this->_orderDef;
            update_option($this->_DBIDOPT_HOME_SECTIONS_ORDER_OPT, $this->_orderDef);
            $this->_saved = true;    
        }          
                                                                                                 
    }

    private function renderCMS()
    {
        if($this->_saved)
        {                    
            echo '<span class="cms-saved-bar">Settings saved</span>';            
        }  
        
        $out = '';
        $out .= $this->getTopLink();                        
        $out .= '<h6 class="cms-h6">Homepage sections display order (column)</h6><hr class="cms-hr"/>';
   
        $out .= '<div class="cms-sortable-wrapper"><span class="cms-hidden dc-action-name">dc_homeorder</span>';
            $out .= '<ul class="cms-sortable-list-simple">';        
                $counter = 1;
                foreach($this->_order as $key => $value)
                {
                    $out .= '<li id="'.$key.'" ><span>'.$counter.'</span>. '.$value.'</li>';
                    $counter++;
                }     
            $out .= '</ul>';
        $out .= '<div style="height:15px;"></div>';
        $out .= '<a class="cms-gd-button dc-submit-btn" href="#" onclick="this.blur(); return false;" ><span>Save display order</span></a>';
         $out .= ' <form action="#" method="post"><input class="cms-submit" type="submit" value="Restore default order" name="home_restore_order" /></form>';
        $out .= '<div class="cms-clear"></div>';
               
        $out .= '</div>'; // sortable wrapper           
          
        echo $out;
        

        
         // Homepage tabs
         $out = '';
         $out .= '<div style="height:30px;"></div>';  
         $out .= '<a name="home-tabs"></a>';
         $out .= $this->getTopLink();                        
         $out .= '<h6 class="cms-h6">Homepage tabs</h6><hr class="cms-hr"/>';
         $out .= '<form action="#home-tabs" method="post" >';                                                                                         
         $out .= '<input type="checkbox" '.$this->attrChecked($this->_home['home_tabs_show']).' name="home_tabs_show" /> Display homepage tabs<br /><br />';
         $out .= '<input type="text" style="width:60px;text-align:center;" name="home_tabs_count" value="'.$this->_home['home_tabs_count'].'" /> Number of tabs to display<br /><br />';                 
         
         $out .= '<span class="cms-span-10-normal">Section header text</span><br />';  
         $out .= '<input type="text" style="width:400px;text-align:left;" name="home_tabs_head_text" value="'.stripcslashes($this->_home['home_tabs_head_text']).'" /><br />';               
         $out .= '<input type="checkbox" '.$this->attrChecked($this->_home['home_tabs_hide_head']).' name="home_tabs_hide_head" /> Hide header<br /><br />';            
         
         $out .= '<span class="cms-span-10-normal">Tabs selected manually, insert comma separated tabs ID, e.g 12,8,34</span><br />';
         $out .= '<input type="text" style="width:400px;" value="'.$this->_home['home_tabs_selected'].'" name="home_tabs_selected" /><br />';
         $out .= '<input type="checkbox" '.$this->attrChecked($this->_home['home_tabs_selected_use']).' name="home_tabs_selected_use" /> Use manually selected tabs<br /><br />'; 
          
         $out .= '<span class="cms-span-10-normal">Tabs display date order:</span><br />';
         $out .= '<input type="radio" name="home_tabs_order" '.$this->attrChecked($this->_home['home_tabs_order'] == CPHome::TABS_ORDER_ASC).' value="'.CPHome::TABS_ORDER_ASC.'" /> Ascending<br />';
         $out .= '<input type="radio" name="home_tabs_order" '.$this->attrChecked($this->_home['home_tabs_order'] == CPHome::TABS_ORDER_DESC).' value="'.CPHome::TABS_ORDER_DESC.'" /> Descending<br /><br />';          
         
         $out .= '<span class="cms-span-10-normal">Tabs date:</span><br />'; 
         $out .= '<input type="checkbox" '.$this->attrChecked($this->_home['home_tabs_date_show']).' name="home_tabs_date_show" /> Show tabs date<br /><br />';  
         $out .= '<input type="text" style="width:60px;text-align:center;" name="home_tabs_date_format" value="'.$this->_home['home_tabs_date_format'].'" /> Date format (default: F j, Y) <a href="http://codex.wordpress.org/Formatting_Date_and_Time" target="_blank">More about date formating</a><br /><br />';                    
         $out .= '<span class="cms-span-10-normal">Default tab:</span><br />'; 
         $out .= $this->selectCtrlHomeTabsList($this->_home['home_tabs_default'], home_tabs_default, 400);
         $out .= '<br />';
         
         //$out .= '<span class="cms-span-10-normal">Gallery:</span><br />'; 
         //$out .= '<input type="checkbox" '.$this->attrChecked($this->_home['home_tabs_disable_gallery']).' name="home_tabs_disable_gallery" /> Disable gallery view<br /><br />';  
                  
         //$out .= '<span class="cms-span-10-normal">Thumbs image auto resizing:</span><br />';  
         //$out .= '<input type="checkbox" '.$this->attrChecked($this->_home['home_tabs_autoresize']).' name="home_tabs_autoresize" /> Enable<br />';
         //$out .= '<input type="checkbox" '.$this->attrChecked($this->_home['home_tabs_keep_ratio']).' name="home_tabs_keep_ratio" /> Keep image aspect ratio<br /><br />';  
         //$out .= '<input type="text" style="width:60px;text-align:center;" name="home_tabs_autoresize_w" value="'.$this->_home['home_tabs_autoresize_w'].'" /> Maximum width in px<br />';
         //$out .= '<input type="text" style="width:60px;text-align:center;" name="home_tabs_autoresize_h" value="'.$this->_home['home_tabs_autoresize_h'].'" /> Maximum height in px<br />';  
         
         //$out .= '<span class="cms-span-10">Tab button colors:</span><br />';
         //$out .= '<input style="width:70px;text-align:center;" type="text" class="colorpicker {hash:true}" value="'.$this->_home['home_tabs_btn_color'].'" name="home_tabs_btn_color" /> Button color <br />';      
          
         $out .= '<div style="height:20px;"></div>';
         $out .= '<input class="cms-submit" type="submit" value="Save" name="home_save_tabs_opt" /><br />';
         $out .= '</form>'; 
         
         echo $out;               
         
         // Homepage post thumbs
         $out = '';   
         $out .= '<div style="height:30px;"></div>';
         $out .= '<a name="home-postthumbs"></a>'; 
         $out .= $this->getTopLink();                    
         $out .= '<h6 class="cms-h6">Homepage blog post thumbs</h6><hr class="cms-hr"/>';
         $out .= '<form action="#home-postthumbs" method="post" >';                                                                                                   
         
         $out .= '<input type="checkbox" '.$this->attrChecked($this->_home['home_pt_show']).' name="home_pt_show" /> Display homepage blog post thumbs<br /><br />'; 
         $out .= '<input type="text" style="width:60px;text-align:center;" name="home_pt_rows" value="'.$this->_home['home_pt_rows'].'" /> Number of rows to display<br /><br />';
         
         $out .= '<span class="cms-span-10-normal">Place where post thumbs will be displayed:</span><br />'; 
         $out .= '<input type="radio" name="home_pt_place" value="'.CPHome::POSTS_UNDER_SLIDER.'" '.$this->attrChecked($this->_home['home_pt_place'] == CPHome::POSTS_UNDER_SLIDER).' /> Under slider<br />';
         $out .= '<input type="radio" name="home_pt_place" value="'.CPHome::POSTS_UNDER_CONTENT.'" '.$this->attrChecked($this->_home['home_pt_place'] == CPHome::POSTS_UNDER_CONTENT).' /> Under content<br /><br />'; 
         
         $out .= '<span class="cms-span-10-normal">Section header text</span><br />';  
         $out .= '<input type="text" style="width:400px;text-align:left;" name="home_pt_head_text" value="'.$this->_home['home_pt_head_text'].'" /><br />';               
         $out .= '<input type="checkbox" '.$this->attrChecked($this->_home['home_pt_hide_head']).' name="home_pt_hide_head" /> Hide header<br /><br />';                  
                           
         $out .= '<span class="cms-span-10-normal">Blog posts selected manually, insert comma separated posts ID, e.g 3,9,27</span><br />';
         $out .= '<input type="text" style="width:400px;" value="'.$this->_home['home_pt_selected'].'" name="home_pt_selected" /><br />';
         $out .= '<input type="checkbox" '.$this->attrChecked($this->_home['home_pt_selected_use']).' name="home_pt_selected_use" /> Use manually selected blog posts<br /><br />';           
         
         $out .= '<span class="cms-span-10-normal">Insert comma separated posts categories ID, e.g 3,9,27</span><br />';  
         $out .= '<input type="text" style="width:400px;" value="'.$this->_home['home_pt_categories'].'" name="home_pt_categories" /><br />';
         $out .= '<input type="checkbox" '.$this->attrChecked($this->_home['home_pt_categories_use']).' name="home_pt_categories_use" /> Use selected categories<br /><br />'; 
          
         $out .= '<span class="cms-span-10-normal">Here you can setup display for most popular post from last 10, 15, 20 or any other number of posts</span><br />';  
         $out .= '<input type="checkbox" '.$this->attrChecked($this->_home['home_pt_most_use']).' name="home_pt_most_use" /> Use most popular display<br />';
         $out .= '<input type="checkbox" '.$this->attrChecked($this->_home['home_pt_most_cats_use']).' name="home_pt_most_cats_use" /> Use categories in query<br /><br />'; 
         $out .= '<input type="text" style="width:400px;" value="'.$this->_home['home_pt_most_cats'].'" name="home_pt_most_cats" /> Insert comma separated categories ID<br />';
         $out .= 'Display most popular posts from last <input type="text" style="width:60px;text-align:center;" value="'.$this->_home['home_pt_most_from_last'].'" name="home_pt_most_from_last" /> posts<br />';       
          
         $out .= '<div style="height:20px;"></div>';
         $out .= '<input class="cms-submit" type="submit" value="Save" name="home_save_postthumbs_opt" /><br />';
         $out .= '</form>';          
         
         echo $out;         
         
         // Homepage posts main stream
         $out = '';   
         $out .= '<div style="height:30px;"></div>';
         $out .= '<a name="home-mainstream"></a>';
         $out .= $this->getTopLink();                     
         $out .= '<h6 class="cms-h6">Homepage posts main stream</h6><hr class="cms-hr"/>';
         $out .= '<form action="#home-mainstream" method="post" >';                                                                                                   
         
         $out .= '<input type="checkbox" '.$this->attrChecked($this->_home['home_ms_show']).' name="home_ms_show" /> Display homepage posts main stream<br /><br />'; 
         $out .= '<input type="text" style="width:60px;text-align:center;" name="home_ms_posts_count" value="'.$this->_home['home_ms_posts_count'].'" /> Number of posts to display<br />';                 
         $out .= '<input type="checkbox" '.$this->attrChecked($this->_home['home_ms_show_pages']).' name="home_ms_show_pages" /> Show pagination<br /><br />';  
         
         $out .= '<span class="cms-span-10-normal">Section header text</span><br />'; 
         $out .= '<input type="text" style="width:400px;text-align:left;" name="home_ms_head_text" value="'.$this->_home['home_ms_head_text'].'" /><br /><br />'; 
         
         
         $out .= '<span class="cms-span-10-normal">Insert comma separated posts categories ID, e.g 3,9,27</span><br />';  
         $out .= '<input type="text" style="width:400px;" value="'.$this->_home['home_ms_categories'].'" name="home_ms_categories" /><br />';
         $out .= '<input type="checkbox" '.$this->attrChecked($this->_home['home_ms_categories_use']).' name="home_ms_categories_use" /> Use selected categories<br /><br />';           
          
         $out .= '<span class="cms-span-10-normal">Insert comma separated posts categories ID to exclude, e.g 3,9,27</span><br />';  
         $out .= '<input type="text" style="width:400px;" value="'.$this->_home['home_ms_ex_categories'].'" name="home_ms_ex_categories" /><br /><br />';          
          
         $out .= '<span class="cms-span-10-normal">Blog posts selected manually, insert comma separated posts ID, e.g 3,9,27</span><br />';
         $out .= '<input type="text" style="width:400px;" value="'.$this->_home['home_ms_selected'].'" name="home_ms_selected" /><br />';
         $out .= '<input type="checkbox" '.$this->attrChecked($this->_home['home_ms_selected_use']).' name="home_ms_selected_use" /> Use manually selected blog posts<br /><br />';           
         
         $out .= '<span class="cms-span-10-normal">Force posts layout or set it to default</span><br />';  
         $out .= '<input type="radio" '.$this->attrChecked($this->_home['home_ms_layout'] == CMS_POST_LAYOUT_SMALL).' name="home_ms_layout" value="'.CMS_POST_LAYOUT_SMALL.'" /> Small<br />';        
         $out .= '<input type="radio" '.$this->attrChecked($this->_home['home_ms_layout'] == CMS_POST_LAYOUT_MEDIUM).' name="home_ms_layout" value="'.CMS_POST_LAYOUT_MEDIUM.'" /> Medium<br />'; 
         $out .= '<input type="radio" '.$this->attrChecked($this->_home['home_ms_layout'] == CMS_POST_LAYOUT_BIG).' name="home_ms_layout" value="'.CMS_POST_LAYOUT_BIG.'" /> Big<br />'; 
         $out .= '<input type="radio" '.$this->attrChecked($this->_home['home_ms_layout'] == CMS_POST_LAYOUT_DEFAULT).' name="home_ms_layout" value="'.CMS_POST_LAYOUT_DEFAULT.'" /> Use default from posts settings<br />'; 
          
         $out .= '<div style="height:20px;"></div>';
         $out .= '<input class="cms-submit" type="submit" value="Save" name="home_save_mainstream_opt" /><br />';
         $out .= '</form>'; 
         echo $out;   
         
         // Homepage featured video
         $out = '';   
         $out .= '<div style="height:30px;"></div>';
         $out .= '<a name="home-featured-video"></a>';
         $out .= $this->getTopLink();                      
         $out .= '<h6 class="cms-h6">Homepage featured video</h6><hr class="cms-hr"/>';
         $out .= '<form action="#home-featured-video" method="post" >';                                                                                                   
         
         $out .= '<input type="checkbox" '.$this->attrChecked($this->_home['home_fv_show']).' name="home_fv_show" /> Display homepage featured video<br /><br />';
         
         $out .= '<span class="cms-span-10-normal">Section header text</span><br />'; 
         $out .= '<input type="text" style="width:400px;text-align:left;" name="home_fv_head_text" value="'.stripcslashes($this->_home['home_fv_head_text']).'" /><br /><br />';          

         $out .= '<span class="cms-span-10-normal">Wordtube media ID</span><br />'; 
         $out .= '<input type="text" style="width:60px;text-align:center;" name="home_fv_media_id" value="'.$this->_home['home_fv_media_id'].'" /><br /><br />';  
                         
         $out .= '<span class="cms-span-10-normal">Optional link options</span><br />';          
         $out .= '<input type="checkbox" '.$this->attrChecked($this->_home['home_fv_link_use']).' name="home_fv_link_use" /> Show link<br /><br />';
         
         $out .= '<span class="cms-span-10-normal">Manually link</span><br />'; 
         $out .= '<input type="text" style="width:600px;text-align:left;" name="home_fv_link" value="'.$this->_home['home_fv_link'].'" /><br /><br />';  
         
         $out .= '<span class="cms-span-10-normal">Select page to link</span><br />';
         $out .= $this->selectCtrlPagesList($this->_home['home_fv_link_page'], 'home_fv_link_page', 300);  
         $out .= '<br /><br />';
         
         $out .= '<span class="cms-span-10-normal">Select link type to use</span><br />';  
         $out .= '<input type="radio" name="home_fv_link_type" '.$this->attrChecked($this->_home['home_fv_link_type'] == CMS_LINK_TYPE_PAGE).' value="'.CMS_LINK_TYPE_PAGE.'" /> Use page<br />';
         $out .= '<input type="radio" name="home_fv_link_type" '.$this->attrChecked($this->_home['home_fv_link_type'] == CMS_LINK_TYPE_MANUALLY).' value="'.CMS_LINK_TYPE_MANUALLY.'" /> Use manually link<br /><br />'; 
         
         $out .= '<span class="cms-span-10-normal">Select link target</span><br />';  
         $out .= '<input type="radio" name="home_fv_link_target" '.$this->attrChecked($this->_home['home_fv_link_type'] == CMS_LINK_TARGET_SELF).' value="'.CMS_LINK_TARGET_SELF.'" /> Self<br />';
         $out .= '<input type="radio" name="home_fv_link_target" '.$this->attrChecked($this->_home['home_fv_link_type'] == CMS_LINK_TARGET_BLANK).' value="'.CMS_LINK_TARGET_BLANK.'" /> Blank (new tab)<br /><br />';                                  
              
         $out .= '<span class="cms-span-10-normal">Link read more text</span><br />'; 
         $out .= '<input type="text" style="width:200px;" name="home_fv_link_text" value="'.stripcslashes($this->_home['home_fv_link_text']).'" /><br />';                              
                         
         $out .= '<div style="height:20px;"></div>';
         $out .= '<input class="cms-submit" type="submit" value="Save" name="home_save_featuredvideo_opt" /><br />';
         $out .= '</form>'; 
         echo $out; 
         
         // Homepage featured gallery
         $out = '';   
         $out .= '<div style="height:30px;"></div>';
         $out .= '<a name="home-featured-gallery"></a>';
         $out .= $this->getTopLink();                      
         $out .= '<h6 class="cms-h6">Homepage featured gallery</h6><hr class="cms-hr"/>';
         $out .= '<form action="#home-featured-gallery" method="post" >';                                                                                                   
         
         $out .= '<input type="checkbox" '.$this->attrChecked($this->_home['home_fg_show']).' name="home_fg_show" /> Display homepage featured gallery<br /><br />';
         
         $out .= '<span class="cms-span-10-normal">Section header text</span><br />'; 
         $out .= '<input type="text" style="width:400px;text-align:left;" name="home_fg_head_text" value="'.stripcslashes($this->_home['home_fg_head_text']).'" /><br /><br />';          

         $out .= '<span class="cms-span-10-normal">NextGEN Gallery ID</span><br />'; 
         $out .= $this->selectCtrlNGGList($this->_home['home_fg_media_id'], 'home_fg_media_id', 300); 
         $out .= '<br /><br />';
                         
         $out .= '<span class="cms-span-10-normal">Optional link options</span><br />';          
         $out .= '<input type="checkbox" '.$this->attrChecked($this->_home['home_fg_link_use']).' name="home_fg_link_use" /> Show link<br /><br />';
         
         $out .= '<span class="cms-span-10-normal">Manually link</span><br />'; 
         $out .= '<input type="text" style="width:600px;text-align:left;" name="home_fg_link" value="'.$this->_home['home_fg_link'].'" /><br /><br />';  
         
         $out .= '<span class="cms-span-10-normal">Select page to link</span><br />';
         $out .= $this->selectCtrlPagesList($this->_home['home_fg_link_page'], 'home_fg_link_page', 300);  
         $out .= '<br /><br />';
         
         $out .= '<span class="cms-span-10-normal">Select link type to use</span><br />';  
         $out .= '<input type="radio" name="home_fg_link_type" '.$this->attrChecked($this->_home['home_fg_link_type'] == CMS_LINK_TYPE_PAGE).' value="'.CMS_LINK_TYPE_PAGE.'" /> Use page<br />';
         $out .= '<input type="radio" name="home_fg_link_type" '.$this->attrChecked($this->_home['home_fg_link_type'] == CMS_LINK_TYPE_MANUALLY).' value="'.CMS_LINK_TYPE_MANUALLY.'" /> Use manually link<br /><br />'; 
         
         $out .= '<span class="cms-span-10-normal">Select link target</span><br />';  
         $out .= '<input type="radio" name="home_fg_link_target" '.$this->attrChecked($this->_home['home_fg_link_type'] == CMS_LINK_TARGET_SELF).' value="'.CMS_LINK_TARGET_SELF.'" /> Self<br />';
         $out .= '<input type="radio" name="home_fg_link_target" '.$this->attrChecked($this->_home['home_fg_link_type'] == CMS_LINK_TARGET_BLANK).' value="'.CMS_LINK_TARGET_BLANK.'" /> Blank (new tab)<br /><br />';                                  
              
         $out .= '<span class="cms-span-10-normal">Link read more text</span><br />'; 
         $out .= '<input type="text" style="width:200px;" name="home_fg_link_text" value="'.stripcslashes($this->_home['home_fg_link_text']).'" /><br />';                              
                         
         $out .= '<div style="height:20px;"></div>';
         $out .= '<input class="cms-submit" type="submit" value="Save" name="home_save_featuredgallery_opt" /><br />';
         $out .= '</form>'; 
         echo $out;         
       
         // Homepage featured story
    /*     $out = '';   
         $out .= '<div style="height:30px;"></div>';
         $out .= '<a name="home-featured-story"></a>';
         $out .= $this->getTopLink();                      
         $out .= '<h6 class="cms-h6">Homepage featured story</h6><hr class="cms-hr"/>';
         $out .= '<form action="#home-featured-story" method="post" >';            
       
         $out .= '<input type="checkbox" '.$this->attrChecked($this->_home['home_story_show']).' name="home_story_show" /> Display homepage story<br /><br />';  

         $out .= '<span class="cms-span-10-normal">Section header text</span><br />'; 
         $out .= '<input type="text" style="width:400px;text-align:left;" name="home_story_head_text" value="'.stripcslashes($this->_home['home_story_head_text']).'" /><br /><br />';          
       
         $out .= '<span class="cms-span-10-normal">Comma seprated story posts ID to display</span><br />'; 
         $out .= '<input type="text" style="width:400px;text-align:left;" name="home_story_list" value="'.$this->_home['home_story_list'].'" /><br /><br />';  
       
         $out .= '<span class="cms-span-10-normal">Story slider height in pixels (default 620x300, the width is locked and not editable)</span><br />';   
         $out .= '<input type="text" style="width:60px;text-align:center;" name="home_story_height" value="'.$this->_home['home_story_height'].'" /> Height<br /><br />'; 
       
         $out .= '<span class="cms-span-10-normal">Slider link</span><br />'; 
         $out .= '<input type="checkbox" '.$this->attrChecked($this->_home['home_story_link_use']).' name="home_story_link_use" /> Use link<br />'; 
         $out .= '<input type="checkbox" '.$this->attrChecked($this->_home['home_story_overwrite']).' name="home_story_overwrite" /> Overwrite slides links<br /><br />'; 
       
         $out .= '<span class="cms-span-10-normal">Manually link</span><br />'; 
         $out .= '<input type="text" style="width:400px;" name="home_story_link" value="'.$this->_home['home_story_link'].'" /><br /><br />';       
       
         $out .= '<span class="cms-span-10-normal">Select page to link</span><br />';
         $out .= $this->selectCtrlPagesList($this->_home['home_story_link_page'], 'home_story_link_page', 300);  
         $out .= '<br /><br />';       
       
         $out .= '<span class="cms-span-10-normal">Select link type to use</span><br />';  
         $out .= '<input type="radio" name="home_story_link_type" '.$this->attrChecked($this->_home['home_story_link_type'] == CMS_LINK_TYPE_PAGE).' value="'.CMS_LINK_TYPE_PAGE.'" /> Use page<br />';
         $out .= '<input type="radio" name="home_story_link_type" '.$this->attrChecked($this->_home['home_story_link_type'] == CMS_LINK_TYPE_MANUALLY).' value="'.CMS_LINK_TYPE_MANUALLY.'" /> Use manually link<br /><br />'; 
         
         $out .= '<span class="cms-span-10-normal">Select link target</span><br />';  
         $out .= '<input type="radio" name="home_story_link_target" '.$this->attrChecked($this->_home['home_story_link_target'] == CMS_LINK_TARGET_SELF).' value="'.CMS_LINK_TARGET_SELF.'" /> Self<br />';
         $out .= '<input type="radio" name="home_story_link_target" '.$this->attrChecked($this->_home['home_story_link_target'] == CMS_LINK_TARGET_BLANK).' value="'.CMS_LINK_TARGET_BLANK.'" /> Blank (new tab)<br /><br />';                                  
                  
       
         $out .= '<span class="cms-span-10-normal">Other options</span><br />';                   
         $out .= '<input type="checkbox" '.$this->attrChecked($this->_home['home_story_group']).' name="home_story_group" /> Gropu lightbox display<br />';          
         $out .= '<input type="checkbox" '.$this->attrChecked($this->_home['home_story_fit']).' name="home_story_fit" /> Fit images to story slider window<br />'; 
         $out .= '<input type="checkbox" '.$this->attrChecked($this->_home['home_story_showtitle']).' name="home_story_showtitle" /> Show slide title<br />'; 
         $out .= '<input type="checkbox" '.$this->attrChecked($this->_home['home_story_showdesc']).' name="home_story_showdesc" /> Show slide description<br />'; 
       
         $out .= '<div style="height:20px;"></div>';
         $out .= '<input class="cms-submit" type="submit" value="Save" name="home_save_featuredstory_opt" /><br />';
         $out .= '</form>'; 
         echo $out;          
      */ 
         // Homepage content
         $out = '';   
         $out .= '<div style="height:30px;"></div>';
         $out .= '<a name="home-content"></a>';
         $out .= $this->getTopLink();                     
         $out .= '<h6 class="cms-h6">Homepage content</h6><hr class="cms-hr"/>';
         $out .= '<form action="#home-content" method="post" >';                                                                                                   
         
         $out .= '<input type="checkbox" '.$this->attrChecked($this->_home['home_ct_show']).' name="home_ct_show" /> Display homepage content<br />';
         $out .= '<input type="checkbox" '.$this->attrChecked($this->_home['home_ct_show_column_and_sid']).' name="home_ct_show_column_and_sid" /> Display homepage content column and sidebar<br /><br />'; 
         
         
         $out .= '<span class="cms-span-10-normal">Section header text</span><br />'; 
         $out .= '<input type="text" style="width:400px;text-align:left;" name="home_ct_head_text" value="'.stripcslashes($this->_home['home_ct_head_text']).'" /><br />';          
         $out .= '<input type="checkbox" '.$this->attrChecked($this->_home['home_ct_show_head']).' name="home_ct_show_head" /> Show head<br /><br />';  
         
         $out .= '<span class="cms-span-10-normal">Place where content will be displayed</span><br />';  
         $out .= '<input type="radio" name="home_ct_place" '.$this->attrChecked($this->_home['home_ct_place'] == CPHome::CONTENT_ON_COLUMN).' value="'.CPHome::CONTENT_ON_COLUMN.'" /> Display in column<br />';
         $out .= '<input type="radio" name="home_ct_place" '.$this->attrChecked($this->_home['home_ct_place'] == CPHome::CONTENT_ON_FULLWIDTH).' value="'.CPHome::CONTENT_ON_FULLWIDTH.'" /> Display above column and sidebar<br /><br />';                                                                           

         $out .= '<span class="cms-span-10-normal">Optional content for replace selected page content</span><br />'; 
         $out .= '<textarea style="width:700px;height:160px;font-size:11px;" name="home_ct_content">'.stripcslashes($this->_home['home_ct_content']).'</textarea><br /><br />';
         
         $out .= '<span class="cms-span-10-normal">Select page with content</span><br />'; 
         $out .= $this->selectCtrlPagesList($this->_home['home_ct_page'], 'home_ct_page', 320);
         $out .= '<br />';
         $out .= '<input type="checkbox" '.$this->attrChecked($this->_home['home_ct_use_page_content']).' name="home_ct_use_page_content" /> Use selected page content<br /><br />'; 
         
         $out .= '<span class="cms-span-10-normal">Other options</span><br />';
         $out .= '<input type="checkbox" '.$this->attrChecked($this->_home['home_ct_add_padding']).' name="home_ct_add_padding" /> Add bottom 20 pixels padding<br />';  
         
                         
         $out .= '<div style="height:20px;"></div>';
         $out .= '<input class="cms-submit" type="submit" value="Save" name="home_save_homecontent_opt" /><br />';
         $out .= '</form>'; 
         echo $out;             


         
                       
                                                                        
    }      
    
    private function selectCtrlHomeTabsList($itempage, $name, $width)
    {
        $out = '';
        $out .= '<select style="width:'.$width.'px;" name="'.$name.'"><option value="'.CMS_NOT_SELECTED.'">Not selected</option>';
        
        global $wpdb;
        $items = $wpdb->get_results("SELECT ID, post_title FROM $wpdb->posts WHERE post_status = 'publish' AND post_type = '".CPThemeCustomPosts::PT_HOMEPAGE_TAB_POST."' ORDER BY post_title ");
        
        $count = count($items);
        for($i = 0; $i < $count; $i++)
        {
   
            $out .= '<option value="'.$items[$i]->ID.'" ';
            
            if($itempage !== null)
            {
                if($items[$i]->ID == $itempage) $out .= ' selected="selected" ';
            }
            
            $out .= '>';
            $out .= $items[$i]->post_title.' (ID:'.$items[$i]->ID.')';
            $out .= '</option>';
               
        } // for        
        $out .= '</select>';
        return $out;     
    }       
} // class CPHome
        
        
?>