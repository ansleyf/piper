<?php
/**********************************************************************
* MODERN ELEGANCE WORDPRESS THEME 
* (Ideal For Business And Personal Use: Portfolio or Blog)     
* 
* File name:   
*      cp_renderer.php
* Brief:       
*      Part of theme control panel.
* Author:      
*      DigitalCavalry
* Author URI:
*      http://themeforest.net/user/DigitalCavalry
* Contact:
*      digitalcavalry@gmail.com 
***********************************************************************/

/*********************************************************** 
* Definitions
************************************************************/

// layout config
define('CMS_LC_BLOG_IMG_WIDTH', 608); 
define('CMS_LC_BLOG_IMG_HEIGHT', 300);

/*********************************************************** 
* Class name:
*    CPRenderer
* Descripton:
*    Implementation of CPRenderer 
***********************************************************/
class CPRenderer 
{

    /*********************************************************** 
    * Constructor
    ************************************************************/
    public function __construct() 
    {
    
    } // constructor 

    /*********************************************************** 
    * Public members
    ************************************************************/      
    
    /*********************************************************** 
    * Private members
    ************************************************************/      
   
    /*********************************************************** 
    * Public functions
    ************************************************************/               

    public function commentsBlock()
    {
        global $post;
        if('open' == $post->comment_status)
        {
            echo '<a name="comments"></a>';
            comments_template();
        }         
    }
    
    public function renderServices(& $data, $layout, $header, $subtitle, $desc)
    {
        switch($layout)
        {
            case CMS_SERVICE_LAYOUT_SMALL:
            $this->renderServicesSmall($data, $header, $subtitle, $desc);
            break;
            
            case CMS_SERVICE_LAYOUT_MEDIUM:
            $this->renderServicesMedium($data, $header, $subtitle, $desc);
            break;
                                    
            case CMS_SERVICE_LAYOUT_BIG:
            $this->renderServicesBig($data, $header, $subtitle, $desc);
            break;                        
        }    
    }
    
    public function renderServicesSmall(& $data, $header, $subtitle, $desc)
    {
        $count = count($data);
        $out = '';
        
        if($count)
        {
            $out .= '<div class="services-small-list">';
            if($header != '' or $desc != '')
            {
                $out .= '<div class="header">';
                
                    if($header != '')
                    {
                        $out .= '<h3>'.$header;
                        if($subtitle != '')
                        {
                            $out .= '<span>'.$subtitle.'</span>';
                        }                                                           
                        $out .= '</h3>';
                    }
                    if($desc != '')
                    {
                        $out .= $desc;
                    }
                
                $out .= '</div>';
            }
            
            
            # split post into two columns
            $half_count = $count / 2;
            $left_count = ceil($half_count);
            $right_count = floor($half_count);
            $left = array_values(array_slice($data, 0, $left_count));
            $right = array_values(array_slice($data, $left_count)); 
            
            # left column
            $data = $left;
            $count = count($data);
            $out .= '<div class="column-left">';
            for($i = 0; $i < $count; $i++)
            {
                $service_opt = get_post_meta($data[$i]->ID, 'service_opt', true);

                if($i > 0)
                {
                    $out .= '<div class="services-item-separator"></div>';
                }
                
                $readmore = '';
                $link_readmore = '';
                
                if((bool)$service_opt['link_show_cbox'])
                {
                    if($service_opt['linktype'] == DCC_MetaMultiple::LINK_PAGE and $service_opt['linkpage'] != CMS_NOT_SELECTED)
                    {
                        $link_readmore = ' href="'.get_permalink($service_opt['linkpage']).'" ';
                        $readmore .= ' <a class="more-link" href="'.get_permalink($service_opt['linkpage']).'" target="_self">&raquo;&nbsp;Read&nbsp;more</a>';
                    } else                            
                    if($service_opt['linktype'] == DCC_MetaMultiple::LINK_MANUALLY and $service_opt['link'] != '')
                    {
                        $link_readmore = ' href="'.$service_opt['link'].'" ';
                        $readmore .= ' <a class="more-link" href="'.$service_opt['link'].'" target="_self">&raquo;&nbsp;Read&nbsp;more</a>';
                    }                                
                }                
                
                $out .= '<div class="item">';
                    $out .= '<div class="image-wrapper">';
                        $out .= '<a class="image async-img" '.$link_readmore.' rel="'.dcf_getTimThumbURL($service_opt['image_url'], 120, 120).'"></a>';
                        if((bool)$service_opt['show_image_hover_cbox'])
                        {
                            $out .= '<a class="image-hover async-img" '.$link_readmore.' rel="'.dcf_getTimThumbURL($service_opt['image_url_hover'], 120, 120).'"></a>';
                        }
                    $out .= '</div>';
                    
                    $out .= '<div class="content">';
                        $out .= '<h4 class="title">';
                            if($link_readmore != '')
                            {
                                $out .= '<a '.$link_readmore.'>'.$data[$i]->post_title.'</a>';    
                            } else
                            {
                                $out .= $data[$i]->post_title;
                            }
                            if((bool)$service_opt['show_subtitle_cbox'])
                            {
                                $out .= '<span>'.$service_opt['subtitle'].'</span>';
                            }
                        $out .= '</h4>';
                        $out .= $data[$i]->post_content;
                        $out .= $readmore;
                    $out .= '</div >';
                    
                    $out .= '<div class="clear-both"></div>';
                $out .= '</div>';    
            }
            $out .= '</div>';
        
            # right column
            $data = $right;
            $count = count($data);
            $out .= '<div class="column-right">';
            for($i = 0; $i < $count; $i++)
            {
                $service_opt = get_post_meta($data[$i]->ID, 'service_opt', true);

                if($i > 0)
                {
                    $out .= '<div class="services-item-separator"></div>';
                }
                
                $readmore = '';
                $link_readmore = '';
                
                if((bool)$service_opt['link_show_cbox'])
                {
                    if($service_opt['linktype'] == DCC_MetaMultiple::LINK_PAGE and $service_opt['linkpage'] != CMS_NOT_SELECTED)
                    {
                        $link_readmore = ' href="'.get_permalink($service_opt['linkpage']).'" ';
                        $readmore .= ' <a class="more-link" href="'.get_permalink($service_opt['linkpage']).'" target="_self">&raquo;&nbsp;Read&nbsp;more</a>';
                    } else                            
                    if($service_opt['linktype'] == DCC_MetaMultiple::LINK_MANUALLY and $service_opt['link'] != '')
                    {
                        $link_readmore = ' href="'.$service_opt['link'].'" ';
                        $readmore .= ' <a class="more-link" href="'.$service_opt['link'].'" target="_self">&raquo;&nbsp;Read&nbsp;more</a>';
                    }                                
                }                
                
                $out .= '<div class="item">';
                    $out .= '<div class="image-wrapper">';
                        $out .= '<a class="image async-img" '.$link_readmore.' rel="'.dcf_getTimThumbURL($service_opt['image_url'], 120, 120).'"></a>';
                        if((bool)$service_opt['show_image_hover_cbox'])
                        {
                            $out .= '<a class="image-hover async-img" '.$link_readmore.' rel="'.dcf_getTimThumbURL($service_opt['image_url_hover'], 120, 120).'"></a>';
                        }
                    $out .= '</div>';
                    
                    $out .= '<div class="content">';
                        $out .= '<h4 class="title">';
                            if($link_readmore != '')
                            {
                                $out .= '<a '.$link_readmore.'>'.$data[$i]->post_title.'</a>';    
                            } else
                            {
                                $out .= $data[$i]->post_title;
                            }
                            if((bool)$service_opt['show_subtitle_cbox'])
                            {
                                $out .= '<span>'.$service_opt['subtitle'].'</span>';
                            }
                        $out .= '</h4>';
                        $out .= $data[$i]->post_content;
                        $out .= $readmore;
                    $out .= '</div >';
                    
                    $out .= '<div class="clear-both"></div>';
                $out .= '</div>';    
            }
            $out .= '</div>';
            
            $out .= '<div class="clear-both"></div>';
            $out .= '</div>';
            echo $out;
        }                
    }

    public function renderServicesMedium(& $data, $header, $subtitle, $desc)
    {
        $count = count($data);
        $out = '';
        
        if($count)
        {
            $out .= '<div class="services-medium-list">';
                if($header != '' or $desc != '')
                {
                    $out .= '<div class="header">';
                    
                        if($header != '')
                        {
                            $out .= '<h3>'.$header;
                            if($subtitle != '')
                            {
                                $out .= '<span>'.$subtitle.'</span>';
                            }
                            $out .= '</h3>';
                        }
                        if($desc != '')
                        {
                            $out .= $desc;
                        }
                    
                    $out .= '</div>';
                }
                
                for($i = 0; $i < $count; $i++)
                {
                    $service_opt = get_post_meta($data[$i]->ID, 'service_opt', true);

                    if($i > 0 and (($i % 3) == 0))
                    {
                        $out .= '<div class="services-item-separator"></div>';
                    }
                    
                    $readmore = '';
                    $link_readmore = '';
                    
                    if((bool)$service_opt['link_show_cbox'])
                    {
                        if($service_opt['linktype'] == DCC_MetaMultiple::LINK_PAGE and $service_opt['linkpage'] != CMS_NOT_SELECTED)
                        {
                            $link_readmore = ' href="'.get_permalink($service_opt['linkpage']).'" ';
                            $readmore .= ' <a class="more-link" href="'.get_permalink($service_opt['linkpage']).'" target="_self">&raquo;&nbsp;Read&nbsp;more</a>';
                        } else                            
                        if($service_opt['linktype'] == DCC_MetaMultiple::LINK_MANUALLY and $service_opt['link'] != '')
                        {
                            $link_readmore = ' href="'.$service_opt['link'].'" ';
                            $readmore .= ' <a class="more-link" href="'.$service_opt['link'].'" target="_self">&raquo;&nbsp;Read&nbsp;more</a>';
                        }                                
                    }                
                    
                    if(($i % 3) == 2)
                    {
                        $out .= '<div class="item-right">';
                    } else
                    {
                        $out .= '<div class="item-left">';
                    }
                        $out .= '<div class="image-wrapper">';
                            $out .= '<a class="image async-img" '.$link_readmore.' rel="'.dcf_getTimThumbURL($service_opt['image_url'], 278, 120).'"></a>';
                            if((bool)$service_opt['show_image_hover_cbox'])
                            {
                                $out .= '<a class="image-hover async-img" '.$link_readmore.' rel="'.dcf_getTimThumbURL($service_opt['image_url_hover'], 278, 120).'"></a>';
                            }
                        $out .= '</div>';
                        
                        $out .= '<div class="content">';
                            $out .= '<h4 class="title">';
                                if($link_readmore != '')
                                {
                                    $out .= '<a '.$link_readmore.'>'.$data[$i]->post_title.'</a>';    
                                } else
                                {
                                    $out .= $data[$i]->post_title;
                                }
                                if((bool)$service_opt['show_subtitle_cbox'])
                                {
                                    $out .= '<span>'.$service_opt['subtitle'].'</span>';
                                }
                            $out .= '</h4>';
                            $out .= $data[$i]->post_content;
                            $out .= $readmore;
                        $out .= '</div >';
                        
                        $out .= '<div class="clear-both"></div>';
                    $out .= '</div>';    
                }
            
                $out .= '<div class="clear-both"></div>';
            $out .= '</div>';
            echo $out;
        }                           
    }
    
    public function renderServicesBig(& $data, $header, $subtitle, $desc)
    {
        $count = count($data);
        $out = '';
        
        if($count)
        {
            $out .= '<div class="services-big-list">';
            if($header != '' or $desc != '')
            {
                $out .= '<div class="header">';
                
                    if($header != '')
                    {
                        $out .= '<h3>'.$header;
                        if($subtitle != '')
                        {
                            $out .= '<span>'.$subtitle.'</span>';
                        }
                        $out .= '</h3>';
                    }
                    if($desc != '')
                    {
                        $out .= $desc;
                    }
                
                $out .= '</div>';
            }
            
            for($i = 0; $i < $count; $i++)
            {
                $service_opt = get_post_meta($data[$i]->ID, 'service_opt', true);

                if($i > 0)
                {
                    $out .= '<div class="services-item-separator"></div>';
                }
                
                $readmore = '';
                $link_readmore = '';
                
                if((bool)$service_opt['link_show_cbox'])
                {
                    if($service_opt['linktype'] == DCC_MetaMultiple::LINK_PAGE and $service_opt['linkpage'] != CMS_NOT_SELECTED)
                    {
                        $link_readmore = ' href="'.get_permalink($service_opt['linkpage']).'" ';
                        $readmore .= ' <a class="more-link" href="'.get_permalink($service_opt['linkpage']).'" target="_self">&raquo;&nbsp;Read&nbsp;more</a>';
                    } else                            
                    if($service_opt['linktype'] == DCC_MetaMultiple::LINK_MANUALLY and $service_opt['link'] != '')
                    {
                        $link_readmore = ' href="'.$service_opt['link'].'" ';
                        $readmore .= ' <a class="more-link" href="'.$service_opt['link'].'" target="_self">&raquo;&nbsp;Read&nbsp;more</a>';
                    }                                
                }                
                
                $out .= '<div class="item">';
                    $out .= '<div class="image-wrapper">';
                        $out .= '<a class="image async-img" '.$link_readmore.' rel="'.dcf_getTimThumbURL($service_opt['image_url'], 240, 240).'"></a>';
                        if((bool)$service_opt['show_image_hover_cbox'])
                        {
                            $out .= '<a class="image-hover async-img" '.$link_readmore.' rel="'.dcf_getTimThumbURL($service_opt['image_url_hover'], 240, 240).'"></a>';
                        }
                    $out .= '</div>';
                    
                    $out .= '<div class="content">';
                        $out .= '<h3 class="title">';
                            if($link_readmore != '')
                            {
                                $out .= '<a '.$link_readmore.'>'.$data[$i]->post_title.'</a>';
                            } else
                            {
                                $out .= $data[$i]->post_title;    
                            }
                            if((bool)$service_opt['show_subtitle_cbox'])
                            {
                                $out .= '<span>'.$service_opt['subtitle'].'</span>';
                            }
                        $out .= '</h3>';
                        $out .= $data[$i]->post_content;
                        $out .= $readmore;
                    $out .= '</div >';
                    
                    $out .= '<div class="clear-both"></div>';
                $out .= '</div>';    
            }
            
                $out .= '<div class="clear-both"></div>'; 
            $out .= '</div>';
            echo $out;
        }                   
    }          

    public function renderPageFeaturedPost($echo=true)  
    {
        global $post;
        global $more;
        $more = 0;
        $INCLUDE_MAIN_POST_PHOTO_TO_LIST = true;
        
        // meta
        $post_opt = get_post_meta($post->ID, 'post_opt', true); 
        $post_image = $post_opt['image_url'];
        $post_image = dcf_isNGGImageID($post_image);
        $post_image_desc = $post_opt['image_desc'];
        $post_ngg_ids = $post_opt['images_id'];
        
        // other data
        $month = get_the_time('n', $post->ID);
        $year = get_the_time('Y', $post->ID);
        
        $out = '';                     
        $out .= '<div class="news-page-featured">';

            if($post_image != '')
            {                        
                // post image / video
                $img_style = '';
                $img_style = ' style="width:'.CMS_LC_BLOG_IMG_WIDTH.'px;height:'.CMS_LC_BLOG_IMG_HEIGHT.'px;" ';   
                $out .= '<div class="image-wrapper">';

                    $out .= '<a '.$img_style.' class="async-img blog-post-image" rel="'.dcf_getTimThumbURL($post_image, CMS_LC_BLOG_IMG_WIDTH, CMS_LC_BLOG_IMG_HEIGHT).'" href="'.get_permalink($post->ID).'"></a>';
                    if($post_ngg_ids == '' and $post_image != '')
                    {
                        $out .= '<a '.$img_style.' class="triger" href="'.$post_image.'" rel="lightbox[blog-post]" ></a>';
                    } else
                    if($post_ngg_ids != '' and $post_image != '') 
                    {
                        $objs = dcf_getNGGImagesFromIDList($post_ngg_ids);
                       
                        if($INCLUDE_MAIN_POST_PHOTO_TO_LIST)
                        {
                            $new_pic = new DCC_NGGImage(0, $post_image, '', 0, 0, '', '', '');                                
                            array_push($objs, $new_pic);                     
                        }
                        
                        $THUMB_SIZE = 62;
                        $IMAGE_SIZE = 50;                     
                        $counter = 0;
                        $count = count($objs);
                        $offset = floor((CMS_LC_BLOG_IMG_WIDTH+12)/2) - floor($count*$THUMB_SIZE/2);
                        foreach($objs as $pic)
                        {
                            $style = ' style="left:'.($counter*$THUMB_SIZE+$offset).'px;" ';
                            $out .= '<div '.$style.' class="thumb-wrapper">';
                                $out .= '<a class="image async-img-none" rel="'.dcf_getTimThumbURL($pic->_imageURL, $IMAGE_SIZE, $IMAGE_SIZE).'" ></a>';
                                $out .= '<a class="thumb-triger" href="'.$pic->_imageURL.'" rel="lightbox[thumbs_p'.$post->ID.']"></a>';
                            $out .= '</div>';    
                            $counter++;
                        }   
                    }
                $out .= '</div>'; 
                
                // post image description
                if($post_image_desc != '')
                {
                    $out .= '<div class="blog-post-image-desc">'.$post_image_desc.'</div>';    
                } else
                {
                    $out .= '<div class="blog-post-image-desc-empty"></div>';     
                }                              
            } 
            
            $out .= '<div class="text-content">';
                // post title
                $out .= '<a class="date" href="'.get_month_link($year, $month).'">'.get_the_time('F j, Y').'</a>';
                $out .= '<h2 class="post-title"><a href="'.get_permalink().'">'.$post->post_title.'</a></h2>';     
                                    
                     
                // info bar
                $out .= '<div class="post-info">';                            
                $out .= __('Posted by', 'dc_theme').'&nbsp;<a href="'.
                    get_author_posts_url($post->post_author).'" class="author">'.get_the_author_meta('display_name', $post->post_author).'</a>';
                    
                $catlist = wp_get_post_categories($post->ID);
                $count = count($catlist);
                if($count) { $out .= ' in '; }
                for($i = 0; $i < $count; $i++)
                {
                    if($i > 0)
                    {
                       $out .= ', '; 
                    }
                    $cat = get_category($catlist[$i]);
                    $out .= '<a href="'.get_category_link($catlist[$i]).'" >'.$cat->name.'</a>';
                     
                }                  
                    
                $out .= '</div>';                 
                    
                if($post->post_excerpt != '')
                {
                    $post->post_excerpt = wptexturize($post->post_excerpt, true);
                    $post->post_excerpt = convert_smilies($post->post_excerpt);
                    $post->post_excerpt = convert_chars($post->post_excerpt);
                    // $post->post_excerpt = wpautop($post->post_excerpt);
                    $out .= '<p>'.$post->post_excerpt.' <a class="more-link" href="'.get_permalink($post->ID).'" >'.__('Read&nbsp;more', 'dc_theme').'</a></p>';        
                } else
                {                                    
                    $out .= apply_filters('the_content', get_the_content(__('Read&nbsp;more', 'dc_theme')));
                }  
                
                $out .= '<div class="clear-both"></div>';
            $out .= '</div>';                  
        $out .= '</div>';        
        
        // return
        if($echo) { echo $out; } else { return $out; }         
    }       
    
    public function renderHomeBlogPosts($place)
    {
        if(!GetDCCPInterface()->getIHome()->isPostThumbsShowed()) { echo '<div class="blog-post-related-home-empty"></div>'; return; }
        if($place != GetDCCPInterface()->getIHome()->getPostThumbsPlace()) 
        { 
            if($place == CPHome::POSTS_UNDER_SLIDER)
            {
                echo '<div class="blog-post-related-home-empty"></div>'; 
            }
            return; 
        }
        
        global $wpdb;
        $data = array();        
        $querystr = '';
          
        if(GetDCCPInterface()->getIHome()->isPostThumbsSelectedUsed())
        {        
            $querystr = "
            SELECT ID, post_title
            FROM $wpdb->posts
            WHERE $wpdb->posts.post_type = 'post' 
            AND $wpdb->posts.post_status = 'publish'
            AND $wpdb->posts.ID IN (".GetDCCPInterface()->getIHome()->getPostThumbsSelected().") 
            ORDER BY $wpdb->posts.post_date DESC"; 
            
            $data = $wpdb->get_results($querystr, OBJECT); 
        } else
        if(GetDCCPInterface()->getIHome()->isPostThumbsCatsUsed())
        {   
            $querystr = "
                SELECT DISTINCT ID, post_title
                FROM $wpdb->posts
                LEFT JOIN $wpdb->term_relationships ON($wpdb->posts.ID = $wpdb->term_relationships.object_id)
                LEFT JOIN $wpdb->term_taxonomy ON($wpdb->term_relationships.term_taxonomy_id = $wpdb->term_taxonomy.term_taxonomy_id)
                LEFT JOIN $wpdb->terms ON($wpdb->term_taxonomy.term_id = $wpdb->terms.term_id)                    
                WHERE $wpdb->posts.post_type = 'post'                     
                AND $wpdb->posts.post_status = 'publish'
                AND $wpdb->term_taxonomy.taxonomy = 'category'
                AND $wpdb->term_taxonomy.term_id IN (".GetDCCPInterface()->getIHome()->getPostThumbsCats().")
                ORDER BY $wpdb->posts.post_date DESC LIMIT 0, ".(GetDCCPInterface()->getIHome()->getPostThumbsRowsCount()*4); 
                
                $data = $wpdb->get_results($querystr, OBJECT); 
        } else
        if(GetDCCPInterface()->getIHome()->isPostThumbsMostUsed())
        {
            $querystr = "
                SELECT DISTINCT ID, post_title, meta_value
                FROM $wpdb->posts
                LEFT JOIN $wpdb->postmeta ON($wpdb->posts.ID = $wpdb->postmeta.post_id)";   
                
                if(GetDCCPInterface()->getIHome()->isPostThumbsMostCatsUsed())
                {
                   $querystr .= "LEFT JOIN $wpdb->term_relationships ON($wpdb->posts.ID = $wpdb->term_relationships.object_id)
                    LEFT JOIN $wpdb->term_taxonomy ON($wpdb->term_relationships.term_taxonomy_id = $wpdb->term_taxonomy.term_taxonomy_id)
                    LEFT JOIN $wpdb->terms ON($wpdb->term_taxonomy.term_id = $wpdb->terms.term_id)";                               
                }
                
            $querystr .= "WHERE $wpdb->posts.post_type = 'post'                     
                AND $wpdb->posts.post_status = 'publish'
                AND $wpdb->postmeta.meta_key = 'post_visits' ";
                
                if(GetDCCPInterface()->getIHome()->isPostThumbsMostCatsUsed())
                {                
                    $querystr .= "AND $wpdb->term_taxonomy.taxonomy = 'category'
                    AND $wpdb->term_taxonomy.term_id IN (".GetDCCPInterface()->getIHome()->getPostThumbsMostCats().")";
                }
                               
            $querystr .= "ORDER BY CAST(meta_value AS UNSIGNED) DESC LIMIT 0, ".GetDCCPInterface()->getIHome()->getPostThumbsMostFromLastCount(); 
                
            $data = $wpdb->get_results($querystr, OBJECT);  
            
            $new_data = array();
            $posts_count = GetDCCPInterface()->getIHome()->getPostThumbsRowsCount()*4;
            $data_count = count($data);
            if($data_count < $posts_count) { $posts_count = $data_count; }
            
            for($i = 0; $i < $posts_count; $i++)
            {
                if(isset($data[$i]));
                {   
                    $new_data[$i] = $data[$i];    
                }
            }                
            $data = $new_data;
        } else
        {
            $querystr = "
            SELECT ID, post_title
            FROM $wpdb->posts
            WHERE $wpdb->posts.post_type = 'post' 
            AND $wpdb->posts.post_status = 'publish'
            ORDER BY $wpdb->posts.post_date DESC LIMIT 0, ".(GetDCCPInterface()->getIHome()->getPostThumbsRowsCount()*4);       
            
            $data = $wpdb->get_results($querystr, OBJECT);       
        }           
                                                                              
        $this->renderHomeBlogPostsLine($data, GetDCCPInterface()->getIHome()->getPostThumbsHeadText());        
    }    
    
    public function renderHomeFeaturedVideo()
    {
        if(!GetDCCPInterface()->getIHome()->isFeaturedVideoShowed()) { return; }
        
        global $wpdb;                        
        global $wordTube;
        
        if(isset($wordTube))
        {        
            $video_id = GetDCCPInterface()->getIHome()->getFeaturedVideoID(); 
            $head_text = GetDCCPInterface()->getIHome()->getFeaturedVideoHeadText();
            
            $dbresult = $wpdb->get_row('SELECT * FROM '.$wpdb->wordtube.' WHERE vid = '.$video_id);
            // var_dump($dbresult);
            
            $out = '';
            $out .= '<div class="home-featured-video">';
                $out .= '<div class="head"><span class="head-text">'.$head_text.'</span></div>';
                $out .= do_shortcode('[media id='.$video_id.' width=360 height=270]'); 
                
                $out .= '<div class="content">';
                    $out .= '<h3 class="title">'.$dbresult->name.'</h3>';
                    $out .= '<div class="desc">';
                        $out .= stripcslashes($dbresult->description);
                        $out .= GetDCCPInterface()->getIHome()->getFeaturedVideoLink();
                    $out .= '</div>';
                $out .= '</div>';
                
                $out .= '<div class="clear-both"></div>';       
            $out .= '</div>';
            echo $out;
        }
    }      
    
    public function renderHomeFeaturedGallery()
    {
        if(!GetDCCPInterface()->getIHome()->isFeaturedGalleryShowed()) { return; } 
                
        $gallery_id = GetDCCPInterface()->getIHome()->getFeaturedGalleryID();
        $gallery = dcf_getNGGInfo($gallery_id);                    
        // var_dump($gallery);
        
        if($gallery !== false)
        {   
            $pics = dcf_getGalleryNGG($gallery_id, 'sortorder', 'ASC', true, 12);
            
            if($pics != false)
            {     
                $head_text = GetDCCPInterface()->getIHome()->getFeaturedGalleryHeadText();             
                
                $out = '';             
                $out .= '<div class="home-featured-gallery">';
                    $out .= '<div class="head"><span class="head-text">'.$head_text.'</span></div>';                                
                    
                
                    $out .= '<div class="ngg-gallery">';
                        if(is_array($pics))
                        {
                            shuffle($pics);
                            
                            foreach($pics as $pc)
                            {
                                $out .= '<div class="thumb">';
                                    $out .= '<a class="image async-img-none" rel="'.dcf_getTimThumbURL($pc->_imageURL, 80, 80).'"></a>';
                                    $out .= '<a href="'.$pc->_imageURL.'" class="triger" rel="lightbox[hfg_'.$gallery_id.']"></a>';
                                $out .= '</div>';
                            }
                        }
                    $out .= '</div>';
                    
                    $out .= '<div class="content">';
                        $out .= '<h3 class="title">'.$gallery->title.'</h3>';
                        $out .= '<div class="desc">';
                            $out .= stripcslashes($gallery->galdesc);
                            $out .= GetDCCPInterface()->getIHome()->getFeaturedGalleryLink();
                        $out .= '</div>';
                    $out .= '</div>';
                
                    $out .= '<div class="clear-both"></div>'; 
                $out .= '</div>';
                echo $out;   
            }
        }     
    }
   
    
    public function renderHomeBlogPostsLine(& $data, $name)
    {
        $out = '';
        $count = 0;
        if(is_array($data)) { $count = count($data); }
        
        if($count)
        {
            $add_style = '';
            if(CPHome::POSTS_UNDER_CONTENT == GetDCCPInterface()->getIHome()->getPostThumbsPlace())
            {
                $add_style = ' style="margin-bottom:0px;border:none;padding-top:10px;" ';
            }
            
            $out .= '<div class="blog-post-related-home" '.$add_style.'>';
                if(!GetDCCPInterface()->getIHome()->isPostThumbsHeadShowed())
                {
                    $out .= '<div class="head"><span class="head-text">'.$name.'</span></div>';
                }
                $number = 0;
                foreach($data as $p)
                {
                    $post_opt = get_post_meta($p->ID, 'post_opt', true); 
                    $post_image = $post_opt['image_url'];
                    $post_image = dcf_isNGGImageID($post_image);
                    
                    $number++;
                    if(($number % 4) == 0)
                    {
                        $out .= '<div class="blog-post-home-last">';                        
                    } else
                    {
                       $out .= '<div class="blog-post-home">';       
                    }
                    
                    $image_path = '';
                    $image_path = $post_image;                                          

                    $out .= '<a class="image async-img" href="'.get_permalink($p->ID).'" rel="'.dcf_getTimThumbURL($image_path, 220, 110).'" title="'.$p->post_title.'" ></a>';
                    $out .= '<a class="title" href="'.get_permalink($p->ID).'">'.$p->post_title.'</a>';
                    $out .= '</div>';
                    
                    if(($number % 4) == 0)
                    {
                        $out .= '<div class="clear-both"></div>';                         
                    }                    
                }
                $out .= '<div class="clear-both"></div>';
            $out .= '</div>';
        }
        
        echo $out;
    }         
             
    public function renderHomeMainStreamPosts()
    {
        $ihome = GetDCCPInterface()->getIHome();
        if(!$ihome->isPostMainStreamShowed()) { return; }
        global $wp_query;   
                
        $count = $ihome->getPostMainStreamCount();
        $is_paged = $ihome->isPostMainStreamPaged();
        $head_text = $ihome->getPostMainStreamHeadText();
        $use_cats = $ihome->isPostMainStreamCatsUsed();
        $cats = $ihome->getPostMainStreamCats();
        $ex_cats = $ihome->getPostMainStreamExCats(); 
        $use_selected = $ihome->isPostMainStreamSelectedUsed();
        $selected = $ihome->getPostMainStreamSelected();
                                     
        $layout = $ihome->getPostMainStreamLayout(); 
        
        $out = '';
        $out .= '<div class="home-posts-main-stream">';
            if($head_text != '')
            {
                $style = '';
                if($layout == CMS_POST_LAYOUT_SMALL or $layout == CMS_POST_LAYOUT_MEDIUM) { $style = ' margin-bottom:0px;" '; }
                $out .= '<div class="head" '.$style.'><span class="head-text">'.$head_text.'</span></div>';
            }
            echo $out;
            
            $paged = null;
            $paged = ($wp_query->query_vars['page']) ? $wp_query->query_vars['page'] : 1; 
            
            $args = Array();
            $args['post_type'] = 'post';
            $args['posts_per_page'] = $count;
            if($is_paged) { $args['paged'] = $paged; }  
            if($use_cats)
            {
                $args['category__in'] = explode(',', $cats);
            }          
            if($use_selected)
            {
                $args['post__in'] = explode(',', $selected);
            }         
            $args['category__not_in'] = explode(',', $ex_cats);                                         
            
            $new_wp_query = new WP_Query($args);
            $max_page = $new_wp_query->max_num_pages;
            
            $GLOBALS['dc_post_last_layout'] = CMS_POST_LAYOUT_BIG;
            $GLOBALS['dc_post_loop_counter'] = 0;
            
            if($new_wp_query->have_posts())
            {
                while($new_wp_query->have_posts())
                {
                    $new_wp_query->the_post();      
                    $this->renderBlogPost($layout);
                    $GLOBALS['dc_post_loop_counter']++;
                } // while
            } else
            {
                echo '<p class="theme-exception">There are no posts</p>'; 
            }                            

            if($is_paged)
            {
                GetDCCPInterface()->getIGeneral()->renderSitePagination($paged, $max_page); 
            }
            
        $out = '<div class="clear-both"></div>';
        $out .= '</div>';        
        echo $out;    
    }                                            
    
    public function loopBlogPosts($forced_layout=null)
    {
        $GLOBALS['dc_post_last_layout'] = CMS_POST_LAYOUT_BIG;
        $GLOBALS['dc_post_loop_counter'] = 0;
        
        if(have_posts())
        {
            while(have_posts())
            {
                the_post();      
                $this->renderBlogPost($forced_layout);
                $GLOBALS['dc_post_loop_counter']++;
            } // while
        } else
        {
            echo '<p class="theme-exception">There are no posts</p>'; 
        }          
    }    
    
    public function renderBlogPost($forced_layout=null)
    {
        global $post;
        
        
        $layout = null;
        if($forced_layout !== null and $forced_layout != CMS_POST_LAYOUT_DEFAULT)
        {
            $layout = $forced_layout;    
        } else
        {
            $post_opt = get_post_meta($post->ID, 'post_opt', true);
            $layout = (int)$post_opt['layout'];
        }
        
        
        switch($layout)
        {
            case CMS_POST_LAYOUT_SMALL:
            $this->renderBlogPostSmallLayout(true);  
            break;            
            
            case CMS_POST_LAYOUT_MEDIUM:
            $this->renderBlogPostMediumLayout(true);  
            break;            
            
            case CMS_POST_LAYOUT_BIG:
            $this->renderBlogPostBigLayout(true);  
            break;
            
            default:
            $this->renderBlogPostBigLayout(true); 
            break;
        }        
    }

    public function renderBlogPostSmallLayout($echo=true)
    {
        global $post;
        global $more;
        $more = 0;
        
        // meta
        $post_opt = get_post_meta($post->ID, 'post_opt', true);
        $post_image = $post_opt['image_url'];
        $post_image = dcf_isNGGImageID($post_image);
        $post_ngg_ids = $post_opt['images_id'];
        $post_image_desc = $post_opt['image_desc']; 
        
        // other data
        $month = get_the_time('n', $post->ID);
        $year = get_the_time('Y', $post->ID);
        
        $out = '';
     
        if($GLOBALS['dc_post_loop_counter'] > 0 and $GLOBALS['dc_post_last_layout'] != CMS_POST_LAYOUT_SMALL)
        {
            $out .= '<div class="blog-post-separator"></div>'; 
        }          
                                     
        $out .= '<div class="blog-post-small'.($GLOBALS['dc_post_last_layout'] != CMS_POST_LAYOUT_SMALL ? ' blog-post-medium-border-top' : '').'">';
            
            if($post_image != '')
            {            
                $out .= '<div class="left-content">';
                    // image
                    $pu_img_class = GetDCCPInterface()->getIGeneral()->isBlogSmallImgPreviewShowed() ? ' pu_img' : '';
                    
                    $out .= '<a class="left-image async-img-s" rel="'.dcf_getTimThumbURL($post_image, 96, 66).'"></a>';
                    $out .= '<a href="'.get_permalink($post->ID).'" class="left-image-triger'.$pu_img_class.'" title="" alt="'.$post_image_desc.'" rel="'.dcf_getTimThumbURL($post_image, round(CMS_LC_BLOG_IMG_WIDTH/1.5), round(CMS_LC_BLOG_IMG_HEIGHT/1.5)).'"></a>';               
                $out .= '</div>';
            }

            // right content
            $out .= '<div class="right-content">';
                // post title and date
                $out .= '<div>';
                    $out .= '<a class="date" href="'.get_month_link($year, $month).'">'.get_the_time('F j, Y').'</a>'; 
                    if(GetDCCPInterface()->getIGeneral()->isBlogSmallTimeShowed())
                    {                    
                        $out .= '<span class="time">'.dcf_calculatePostPastTime().'</span>';
                    }
                $out .= '</div>';
                $out .= '<h2 class="post-title"><a href="'.get_permalink($post->ID).'">'.$post->post_title.'</a></h2>';                                               
                
                if($post->post_excerpt != '')
                {
                    $post->post_excerpt = wptexturize($post->post_excerpt, true);
                    $post->post_excerpt = convert_smilies($post->post_excerpt);
                    $post->post_excerpt = convert_chars($post->post_excerpt);
                    // $post->post_excerpt = wpautop($post->post_excerpt);
                    $out .= '<div class="text">'.$post->post_excerpt.' <a class="more-link" href="'.get_permalink($post->ID).'" >&raquo;&nbsp;'.__('Read&nbsp;more', 'dc_theme').'</a></div>';        
                } else
                {                   
                    // content
                    $out .= '<div class="text">';
                        $out .= apply_filters('the_content', get_the_content(__('&raquo;&nbsp;Read&nbsp;more', 'dc_theme')));
                    $out .= '</div>';                 
                }  
                  
            $out .= '</div>';
            
            $out .= '<div class="clear-both"></div>';
            
            // post tags list
            $posttags = get_the_tags();                              
            $count = 0;
            if($posttags !== false)
            {
                $count = count($posttags);
            }

            if($count > 0)
            {   
               $out .= '<div class="post-tags"><span class="white-bg">
                        <span class="name">Tags:</span> ';
               
               $i = 0;            
               foreach($posttags as $tag)
               {   
                   if($i > 0)
                   {
                       $out .= ', ';
                   }
                   
                   $title = '';
                   if($tag->count == 1) { $title = 'One post'; } else { $title = $tag->count.' posts'; } 
                   
                   $out .= '<a href="'.get_tag_link($tag->term_id).'" class="tag link-tip-bottom" title="'.$title.'">'.$tag->name.'</a>';
                   $i++;
               }                       
               $out .= '</span></div>';                                            
                
            } else
            {
                $out .= '<div class="post-tags"></div>';       
            }                         
            
        $out .= '</div>';
        
        $GLOBALS['dc_post_last_layout'] = CMS_POST_LAYOUT_SMALL;
        
        // return
        if($echo) { echo $out; } else { return $out; }                            
    }      
    
    public function renderBlogPostMediumLayout($echo=true)
    {
        global $post;
        global $more;
        $more = 0;
        
        // meta
        $post_opt = get_post_meta($post->ID, 'post_opt', true);
        $post_image = $post_opt['image_url'];
        $post_image = dcf_isNGGImageID($post_image);
        $post_ngg_ids = $post_opt['images_id'];
        $post_image_desc = $post_opt['image_desc'];               
        
        // other data
        $month = get_the_time('n', $post->ID);
        $year = get_the_time('Y', $post->ID);
        
        $out = '';
     
        if($GLOBALS['dc_post_loop_counter'] > 0 and $GLOBALS['dc_post_last_layout'] != CMS_POST_LAYOUT_MEDIUM)
        {
            $out .= '<div class="blog-post-separator"></div>'; 
        }          
                                     
        $out .= '<div class="blog-post-medium'.($GLOBALS['dc_post_last_layout'] != CMS_POST_LAYOUT_MEDIUM ? ' blog-post-medium-border-top' : '').'">';
        
        
        // post title and date
                //$out .= '<div style="float:left;">';
                    //$out .= '<a class="date" href="'.get_month_link($year, $month).'">'.get_the_time('F j, Y').'</a>'; 
                    //if(GetDCCPInterface()->getIGeneral()->isBlogMediumTimeShowed())
                    //{
                    //    $out .= '<span class="time">'.dcf_calculatePostPastTime().'</span>';  
                    //}
                //$out .= '</div>';
                $out .= '<h2 class="post-title"><a href="'.get_permalink($post->ID).'">'.$post->post_title.'</a></h2>';
                
                // info bar
                $out .= '<div class="post-info">';
                $out .= '<a class="date" href="'.get_month_link($year, $month).'">'.get_the_time('F j, Y').'</a>&nbsp;|&nbsp;';            
                $out .= __('Posted by', 'dc_theme').'&nbsp;<a href="'.
                    get_author_posts_url($post->post_author).'" class="author">'.get_the_author_meta('display_name', $post->post_author).'</a>';
                    
                $catlist = wp_get_post_categories($post->ID);
                $count = count($catlist);
                if($count) { $out .= ' in '; }
                for($i = 0; $i < $count; $i++)
                {
                    if($i > 0)
                    {
                       $out .= ', '; 
                    }
                    $cat = get_category($catlist[$i]);
                    $out .= '<a href="'.get_category_link($catlist[$i]).'" >'.$cat->name.'</a>';
                     
                }                  
                    
                $comments_num = get_comments_number($post->ID);
                
                if($comments_num > 0 and $post->comment_status == 'open')
                {
                    if($comments_num == 0)
                    {
                        $comments_num = __('No comments', 'dc_theme');    
                    } else if($comments_num == 1)
                    {
                        $comments_num = __('One comment', 'dc_theme');    
                    } else
                    {
                        $comments_num = $comments_num.'&nbsp;'.__('comments', 'dc_theme');
                    }           
                         
                    $out .= '<a href="'.get_comments_link($post->ID).'" class="comments">'.$comments_num.'</a>';
                }
                
                $out .= '</div>';
        
        
        
            $add_style = '';
            if($post_image != '')
            {
                $out .= '<div class="left-content">';
                    // image
                    $pu_img_class = GetDCCPInterface()->getIGeneral()->isBlogMediumImgPreviewShowed() ? ' pu_img' : ''; 
                    $out .= '<a class="left-image async-img-s" rel="'.dcf_getTimThumbURL($post_image, 200, 100).'"></a>';
                    $out .= '<a href="'.get_permalink($post->ID).'" class="left-image-triger'.$pu_img_class.'" title="" alt="'.$post_image_desc.'" rel="'.dcf_getTimThumbURL($post_image, round(CMS_LC_BLOG_IMG_WIDTH/1.5), round(CMS_LC_BLOG_IMG_HEIGHT/1.5)).'"></a>';              
                    
                    $out .= '<div style="float:left;">';
                        if(GetDCCPInterface()->getIGeneral()->isBlogMediumTimeShowed())
                        {
                            $out .= '<span class="time">'.dcf_calculatePostPastTime().'</span>';  
                        }
                    $out .= '</div>';

                $out .= '</div>';
            } else
            {
                $add_style = ' style="width:100%;margin-left:0px;" ';
            }
            
            
            // right content
            $out .= '<div class="right-content" '.$add_style.'>';
                                   
                
                if($post->post_excerpt != '')
                {
                    $post->post_excerpt = wptexturize($post->post_excerpt, true);
                    $post->post_excerpt = convert_smilies($post->post_excerpt);
                    $post->post_excerpt = convert_chars($post->post_excerpt);
                    // $post->post_excerpt = wpautop($post->post_excerpt);
                    $out .= '<div class="text">'.$post->post_excerpt.'<a class="more-link" href="'.get_permalink($post->ID).'" >&raquo;&nbsp;'.__('Read&nbsp;more', 'dc_theme').'</a></div>';        
                } else
                {                
                    // content
                    $out .= '<div class="text">';
                        $out .= apply_filters('the_content', get_the_content(__('&raquo;&nbsp;Read&nbsp;more', 'dc_theme')));
                    $out .= '</div>';                 
                }
                
                
                  
            $out .= '</div>';
            
            
            //$out .= '<div class="clear-both"></div>';
            
            
            $out .= '<div class="tags-bottom-line"></div>';
            
            $tags_rendered = false;
            if(GetDCCPInterface()->getIGeneral()->isBlogMediumTagsShowed())
                {
                    $on_homepage = GetDCCPInterface()->getIGeneral()->isBlogMediumTagsHomeShowed();
                    $show_tags = true;
                    if(!$on_homepage)
                    {
                        $template_name = get_post_meta($GLOBALS['dc_post_id'], '_wp_page_template', true);
                        if($template_name == 'homepage.php')
                        {
                            $show_tags = false;    
                        }    
                    }
                    
                    if($show_tags)
                    {
                        // post tags list
                        $posttags = get_the_tags();                              
                        $count = 0;
                        if($posttags !== false)
                        {
                            $count = count($posttags);
                        }

                        if($count > 0)
                        {   
                            $tags_rendered = true;  
                            $out .= '<div class="post-tags"><span class="white-bg">
                                    <span class="name">Tags:</span> ';
                           
                           $i = 0;            
                           foreach($posttags as $tag)
                           {   
                               if($i > 0)
                               {
                                   $out .= ', ';
                               }
                               
                               $title = '';
                               if($tag->count == 1) { $title = 'One post'; } else { $title = $tag->count.' posts'; } 
                               
                               $out .= '<a href="'.get_tag_link($tag->term_id).'" class="tag link-tip-bottom" title="'.$title.'">'.$tag->name.'</a>';
                               $i++;
                           }                       
                           $out .= '</span></div>';                                            
                            
                        } 
                        
                    }                    
                }    
                
                if(!$tags_rendered)
                {
                    $out .= '<div class="post-tags"></div>';       
                } 
            
                 
            
            
            
            
            
            
            $out .= '<div class="clear-both"></div>';
        $out .= '</div>';
        
        $GLOBALS['dc_post_last_layout'] = CMS_POST_LAYOUT_MEDIUM;
        
        // return
        if($echo) { echo $out; } else { return $out; }                    
    }  
  
    public function renderBlogPostBigLayout($echo=true)  
    {
        global $post;
        global $more;
        $more = 0;
        $INCLUDE_MAIN_POST_PHOTO_TO_LIST = true;
        
        // meta
        $post_opt = get_post_meta($post->ID, 'post_opt', true);
        $post_image = $post_opt['image_url'];
        $post_image = dcf_isNGGImageID($post_image);
        $post_ngg_ids = $post_opt['images_id'];
        $post_image_desc = $post_opt['image_desc'];
        $post_use_video = (bool)$post_opt['video_use_cbox'];
        $post_use_custom_image = (bool)$post_opt['post_custom_image_cbox'];
        $post_video = $post_opt['video'];                        
        $post_wordtube_id = (int)$post_opt['wordtube_id'];
        $post_use_wordtube = (bool)$post_opt['wordtube_use_cbox'];
        $post_wordtube_height = (int)$post_opt['wordtube_height'];         
        
        // other data
        $month = get_the_time('n', $post->ID);
        $year = get_the_time('Y', $post->ID);
        
        $out = '';
        
        if($GLOBALS['dc_post_loop_counter'] > 0)
        {
            $out .= '<div class="blog-post-separator"></div>'; 
        }        
        
        $out .= '<div class="blog-post">';
        
            // post title
            $out .= '<h2 class="post-title"><a href="'.get_permalink().'">'.$post->post_title.'</a></h2>';     
                                
                 
            // info bar
            $out .= '<div class="post-info">';            
            $out .= '<a class="date" href="'.get_month_link($year, $month).'">'.get_the_time('F j, Y').'</a><span class="separator">&nbsp;|&nbsp;</span>';
            $out .= __('Posted by', 'dc_theme').'&nbsp;<a href="'.
                get_author_posts_url($post->post_author).'" class="author">'.get_the_author_meta('display_name', $post->post_author).'</a>';
                
            $catlist = wp_get_post_categories($post->ID);
            $count = count($catlist);
            if($count) { $out .= ' in '; }
            for($i = 0; $i < $count; $i++)
            {
                if($i > 0)
                {
                   $out .= ', '; 
                }
                $cat = get_category($catlist[$i]);
                $out .= '<a href="'.get_category_link($catlist[$i]).'" >'.$cat->name.'</a>';
                 
            }                  
                
            $comments_num = get_comments_number($post->ID);
            
            if($comments_num > 0 and $post->comment_status == 'open')
            {
                if($comments_num == 0)
                {
                    $comments_num = __('No comments', 'dc_theme');    
                } else if($comments_num == 1)
                {
                    $comments_num = __('One comment', 'dc_theme');    
                } else
                {
                    $comments_num = $comments_num.'&nbsp;'.__('comments', 'dc_theme');
                }           
                     
                $out .= '<a href="'.get_comments_link($post->ID).'" class="comments">'.$comments_num.'</a>';
            }
            
            $out .= '</div>';
        
        
        

            // post image / video
            if($post_use_custom_image)
            {
                $out .= '<div class="custom-image-wrapper">';
                    $out .= do_shortcode($post_opt['post_custom_image']);
                    $out .= '<div class="clear-both"></div>';
                $out .= '</div>';    
            } else
            if($post_use_wordtube)
            {
                $out .= '<div class="video-wrapper">';
                    $out .= do_shortcode('[media id='.$post_wordtube_id.' width=620 height='.$post_wordtube_height.']');;
                $out .= '</div>';                

                // post image description
                if($post_image_desc != '')
                {
                    $out .= '<div class="blog-post-image-desc">'.$post_image_desc.'</div>';    
                } else
                {
                    $out .= '<div class="blog-post-image-desc-empty"></div>';     
                }                   
            
            } else         
            if($post_use_video)
            {
                $out .= '<div class="video-wrapper">';
                    $out .= $post_video;
                $out .= '</div>';
                
                // post image description
                if($post_image_desc != '')
                {
                    $out .= '<div class="blog-post-image-desc">'.$post_image_desc.'</div>';    
                } else
                {
                    $out .= '<div class="blog-post-image-desc-empty"></div>';     
                }                     
                    
            } else         
            if($post_image != '')
            {                        
                $img_style = '';
                $img_style = ' style="width:'.CMS_LC_BLOG_IMG_WIDTH.'px;height:'.CMS_LC_BLOG_IMG_HEIGHT.'px;" ';   
                $out .= '<div class="image-wrapper">';

                    $out .= '<a '.$img_style.' class="async-img blog-post-image" rel="'.dcf_getTimThumbURL($post_image, CMS_LC_BLOG_IMG_WIDTH, CMS_LC_BLOG_IMG_HEIGHT).'" href="'.get_permalink($post->ID).'"></a>';
                    if($post_ngg_ids == '' and $post_image != '')
                    {
                        // $out .= '<a '.$img_style.' class="triger" href="'.$post_image.'" rel="lightbox[blog-post]" ></a>';
                        $out .= '<a '.$img_style.' class="triger-no-link" href="'.get_permalink($post->ID).'" ></a>'; 
                    } else
                    if($post_ngg_ids != '' and $post_image != '') 
                    {
                        $objs = dcf_getNGGImagesFromIDList($post_ngg_ids);
                       
                        if($INCLUDE_MAIN_POST_PHOTO_TO_LIST)
                        {
                            $new_pic = new DCC_NGGImage(0, $post_image, '', 0, 0, '', '', '');                                
                            array_push($objs, $new_pic);                     
                        }
                        
                        $THUMB_SIZE = 62;
                        $IMAGE_SIZE = 50;                     
                        $counter = 0;
                        $count = count($objs);
                        $offset = floor((CMS_LC_BLOG_IMG_WIDTH+12)/2) - floor($count*$THUMB_SIZE/2);
                        foreach($objs as $pic)
                        {
                            $style = ' style="left:'.($counter*$THUMB_SIZE+$offset).'px;" ';
                            $out .= '<div '.$style.' class="thumb-wrapper">';
                                $out .= '<a class="image async-img-none" rel="'.dcf_getTimThumbURL($pic->_imageURL, $IMAGE_SIZE, $IMAGE_SIZE).'" ></a>';
                                $out .= '<a class="thumb-triger" href="'.$pic->_imageURL.'" rel="lightbox[thumbs_p'.$post->ID.']"></a>'; 
                            $out .= '</div>';    
                            $counter++;
                        }   
                    }
                $out .= '</div>'; 
                
                // post image description
                if($post_image_desc != '')
                {
                    $out .= '<div class="blog-post-image-desc">'.$post_image_desc.'</div>';    
                } else
                {
                    $out .= '<div class="blog-post-image-desc-empty"></div>';     
                }                              
            } 
            
                             
            
            if($post->post_excerpt != '')
            {
                $post->post_excerpt = wptexturize($post->post_excerpt, true);
                $post->post_excerpt = convert_smilies($post->post_excerpt);
                $post->post_excerpt = convert_chars($post->post_excerpt);
                // $post->post_excerpt = wpautop($post->post_excerpt);
                $out .= '<p>'.$post->post_excerpt.' <a class="more-link" href="'.get_permalink($post->ID).'" >&raquo;&nbsp;'.__('Read&nbsp;more', 'dc_theme').'</a></p>';        
            } else
            {                
                $out .= apply_filters('the_content', get_the_content(__('&raquo;&nbsp;Read&nbsp;more', 'dc_theme')));
            }
            
            $out .= '<div class="tags-bottom-line"></div>';
            
            // post tags list
            $posttags = get_the_tags();                              
            $count = 0;
            if($posttags !== false)
            {
                $count = count($posttags);
            }

            if($count > 0)
            {   
               $out .= '<div class="post-tags"><span class="white-bg">
                        <span class="name">Tags:</span> ';
               
               $i = 0;            
               foreach($posttags as $tag)
               {   
                   if($i > 0)
                   {
                       $out .= ', ';
                   }
                   
                   $title = '';
                   if($tag->count == 1) { $title = 'One post'; } else { $title = $tag->count.' posts'; } 
                   
                   $out .= '<a href="'.get_tag_link($tag->term_id).'" class="tag link-tip-bottom" title="'.$title.'">'.$tag->name.'</a>';
                   $i++;
               }                       
               $out .= '</span></div>';                                            
                
            } else
            {
                $out .= '<div class="post-tags"></div>';       
            }                
            $out .= '<div class="clear-both"></div>';                  
        $out .= '</div>';        

        $GLOBALS['dc_post_last_layout'] = CMS_POST_LAYOUT_BIG;
        
        // return
        if($echo) { echo $out; } else { return $out; }         
    }  
 
    public function renderBlogPostFull($echo=true)
    {
        global $post;
        global $more;
        global $dc_postcommon_opt;
        $more = 1;
        
        // meta  
        $post_opt = get_post_meta($post->ID, 'post_opt', true);
        $post_image = $post_opt['image_url'];
        $post_image = dcf_isNGGImageID($post_image);
        $post_image_desc = $post_opt['image_desc']; 
        $post_ngg_ids = $post_opt['images_id'];
        $post_use_video = (bool)$post_opt['video_use_cbox'];
        $post_use_custom_image = (bool)$post_opt['post_custom_image_cbox'];          
        $post_hide_image = (bool)$dc_postcommon_opt['post_hide_image_cbox'];
        $post_hide_author = (bool)$dc_postcommon_opt['post_hide_author_cbox'];
        $post_video = $post_opt['video'];
        $post_wordtube_id = (int)$post_opt['wordtube_id'];
        $post_use_wordtube = (bool)$post_opt['wordtube_use_cbox'];
        $post_wordtube_height = (int)$post_opt['wordtube_height']; 
        
        // other data
        $month = get_the_time('n', $post->ID);
        $year = get_the_time('Y', $post->ID);
        
        $out = '';
        $out .= '<div class="blog-post">';
        
            $out .= '<h2 class="title">'.$post->post_title.'</h2>';
        
            // info bar
            $out .= '<div class="post-info">';            
            $out .= '<a class="date" href="'.get_month_link($year, $month).'">'.get_the_time('F j, Y').'</a><span class="separator">&nbsp;|&nbsp;</span>';
            $out .= __('Posted by', 'dc_theme').'&nbsp;<a href="'.
                get_author_posts_url($post->post_author).'" class="author">'.get_the_author_meta('display_name', $post->post_author).'</a>';
                
            $catlist = wp_get_post_categories($post->ID);
            $count = count($catlist);
            if($count) { $out .= ' in '; }
            for($i = 0; $i < $count; $i++)
            {
                if($i > 0)
                {
                   $out .= ', '; 
                }
                $cat = get_category($catlist[$i]);
                $out .= '<a href="'.get_category_link($catlist[$i]).'" >'.$cat->name.'</a>';
                 
            }                  
                
            $comments_num = get_comments_number($post->ID);
            if($comments_num > 0 and $post->comment_status == 'open')
            {
                if($comments_num == 0)
                {
                    $comments_num = __('No comments', 'dc_theme');    
                } else if($comments_num == 1)
                {
                    $comments_num = __('One comment', 'dc_theme');    
                } else
                {
                    $comments_num = $comments_num.'&nbsp;'.__('comments', 'dc_theme');
                }           
                     
                $out .= '<a href="'.get_comments_link($post->ID).'" class="comments">'.$comments_num.'</a>';
            }
            
            $out .= '</div>';
        
          
          
            if(!$post_hide_image)
            {            
                // post image / video
                if($post_use_custom_image)
                {
                    $out .= '<div class="custom-image-wrapper">';
                        $out .= do_shortcode($post_opt['post_custom_image']);
                        $out .= '<div class="clear-both"></div>';
                    $out .= '</div>';    
                } else                
                if($post_use_wordtube)
                {
                    $out .= '<div class="video-wrapper">';
                        $out .= do_shortcode('[media id='.$post_wordtube_id.' width=620 height='.$post_wordtube_height.']');;
                    $out .= '</div>';                

                    // post image description
                    if($post_image_desc != '')
                    {
                        $out .= '<div class="blog-post-image-desc">'.$post_image_desc.'</div>';    
                    } else
                    {
                        $out .= '<div class="blog-post-image-desc-empty"></div>';     
                    }                   
                
                } else
                if($post_use_video)
                {
                    $out .= '<div class="video-wrapper">';
                        $out .= $post_video;
                    $out .= '</div>';
                    
                    // post image description
                    if($post_image_desc != '')
                    {
                        $out .= '<div class="blog-post-image-desc">'.$post_image_desc.'</div>';    
                    } else
                    {
                        $out .= '<div class="blog-post-image-desc-empty"></div>';     
                    }                     
                        
                } else            
                if($post_image != '')
                {                        
                    // post image / video
                    $img_style = '';
                    $img_style = ' style="width:'.CMS_LC_BLOG_IMG_WIDTH.'px;height:'.CMS_LC_BLOG_IMG_HEIGHT.'px;" ';   
                    $out .= '<div class="image-wrapper">';

                        $out .= '<a '.$img_style.' class="async-img blog-post-image" rel="'.dcf_getTimThumbURL($post_image, CMS_LC_BLOG_IMG_WIDTH, CMS_LC_BLOG_IMG_HEIGHT).'" href="'.get_permalink($post->ID).'"></a>';
                        if($post_ngg_ids == '' and $post_image != '')
                        {
                            $out .= '<a '.$img_style.' class="triger" href="'.$post_image.'" rel="lightbox[blog-post]" title="'.$post_image_desc.'" ></a>';
                        } else
                        if($post_ngg_ids != '' and $post_image != '') 
                        {
                            $objs = dcf_getNGGImagesFromIDList($post_ngg_ids);
                           
                            if($INCLUDE_MAIN_POST_PHOTO_TO_LIST)
                            {
                                $new_pic = new DCC_NGGImage(0, $post_image, '', 0, 0, '', '', '');                                
                                array_push($objs, $new_pic);                     
                            }
                            
                            $THUMB_SIZE = 62;
                            $IMAGE_SIZE = 50;                     
                            $counter = 0;
                            $count = count($objs);
                            $offset = floor((CMS_LC_BLOG_IMG_WIDTH+12)/2) - floor($count*$THUMB_SIZE/2);
                            foreach($objs as $pic)
                            {
                                $style = ' style="left:'.($counter*$THUMB_SIZE+$offset).'px;" ';
                                $out .= '<div '.$style.' class="thumb-wrapper">';
                                    $out .= '<a class="image async-img-none" rel="'.dcf_getTimThumbURL($pic->_imageURL, $IMAGE_SIZE, $IMAGE_SIZE).'" ></a>';
                                    $out .= '<a class="thumb-triger" href="'.$pic->_imageURL.'" rel="lightbox[thumbs_p'.$post->ID.']"></a>';
                                $out .= '</div>';    
                                $counter++;
                            }   
                        }
                    $out .= '</div>'; 
                    
                    // post image description
                    if($post_image_desc != '')
                    {
                        $out .= '<div class="blog-post-image-desc">'.$post_image_desc.'</div>';    
                    } else
                    {
                        $out .= '<div class="blog-post-image-desc-empty"></div>';     
                    }                           
                }              
            }   
           
            
             
                                           
                            
            $out .= apply_filters('the_content', get_the_content(__('Read&nbsp;more', 'dc_theme')));
            
            $out .= GetDCCPInterface()->getIGeneral()->renderWordPressPagination(0);
            
            // post tags list
            $posttags = get_the_tags();                              
            $count = 0;
            if($posttags !== false)
            {
                $count = count($posttags);
            }

            if($count > 0)
            {   
               $out .= '<div class="post-tags"><span class="white-bg">
                        <span class="name">Tags:</span> ';
               
               $i = 0;            
               foreach($posttags as $tag)
               {   
                   if($i > 0)
                   {
                       $out .= ',&nbsp;';
                   }
                   
                   $title = '';
                   if($tag->count == 1) { $title = 'One post'; } else { $title = $tag->count.' posts'; } 
                   
                   $out .= '<a href="'.get_tag_link($tag->term_id).'" class="tag link-tip-bottom" title="'.$title.'">'.$tag->name.'</a>';
                   $i++;
               }                       
               $out .= '</span></div>';                                            
                
            }
            $out .= '<div class="clear-both"></div>';
            
            if(GetDCCPInterface()->getIGeneral()->isFacebookShowed() or GetDCCPInterface()->getIGeneral()->isTwitterShowed() or GetDCCPInterface()->getIGeneral()->isGooglePlusShowed())  
            {            
                $out .= '<div class="blog-facebook-twitter-google">';     
                if(GetDCCPInterface()->getIGeneral()->isGooglePlusShowed())
                {
                    $out .= '<div class="item">';
                        $out .= '<g:plusone size="medium" annotation="inline" href="'.get_permalink($post->ID).'"></g:plusone>

                                <script type="text/javascript">
                                  (function() {
                                    var po = document.createElement(\'script\'); po.type = \'text/javascript\'; po.async = true;
                                    po.src = \'https://apis.google.com/js/plusone.js\';
                                    var s = document.getElementsByTagName(\'script\')[0]; s.parentNode.insertBefore(po, s);
                                  })();
                                </script>';
                    $out .= '</div>';
                }                
                if(GetDCCPInterface()->getIGeneral()->isFacebookShowed())
                {
                    $out .= '<div class="item"><iframe src="http://www.facebook.com/plugins/like.php?href='.urlencode(get_permalink($post->ID)).'&amp;layout=button_count&amp;show_faces=false&amp;width=110&amp;action=like&amp;font=arial&amp;colorscheme=light&amp;height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:110px; height:21px;" allowTransparency="true"></iframe>'; 
                    $out .= '</div>';
                }
                if(GetDCCPInterface()->getIGeneral()->isTwitterShowed())
                {
                    $out .= '<div class="item"><a href="http://twitter.com/share?url='.urlencode(get_permalink($post->ID)).'" class="twitter-share-button" data-count="horizontal">Tweet</a><script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script>';
                    $out .= '</div>';
                }                
                $out .= '<div class="clear-both"></div></div>';               
            }
            
        $out .= '</div>';
        
        if(GetDCCPInterface()->getIGeneral()->isBlogSingleAuthorShowed() and !$post_hide_author)
        {
            $out .= '<div class="blog-post-author">';
            
                $out .= '<div class="section-title-one">';
                    $out .= '<span class="title-name">About the Author</span>';
                $out .= '</div>';
            
                $authoremail = get_the_author_meta('user_email', $post->post_author);
                $default_avatar = get_bloginfo('template_url').'/img/common/avatar1.jpg';
                $avatar = get_avatar($authoremail, '60', $default_avatar);
                
                $author_url = get_the_author_meta('user_url', $post->post_author);
                if($author_url != '') { $author_url = 'href="'.$author_url.'"'; } else { $author_url = ''; }
                $out .= '<a '.$author_url.' class="avatar">';
                    $out .= $avatar;       
                $out .= '</a>';
                
                $out .= '<div class="content">';
                    $out .= '<span class="name">'.get_the_author_meta('display_name', $post->post_author).'</span>';
                    $out .= '<div class="desc">';
                      $out .= get_the_author_meta('description', $post->post_author);
                    $out .= '</div>';
                $out .= '</div>';
                
                $out .= '<div class="clear-both"></div>';
            $out .= '</div>';
        }
        
        if(is_array($posttags))
        {
            $count = count($posttags);
            if(GetDCCPInterface()->getIGeneral()->isBlogSingleRelatedShowed() and $count > 0)
            {
                $tags_id_array = array();
                foreach($posttags as $pt)
                {
                    array_push($tags_id_array, $pt->term_id);
                }
                
                $related_on_row = 4;
                $related_posts_count = GetDCCPInterface()->getIGeneral()->getBlogRelatedRows()*$related_on_row;                                                                                                                                              
                $args = array(
                    'post_type' => 'post', 
                    'post_status' => 'publish', 
                    'posts_per_page' => $related_posts_count, 
                    'tag__in' => $tags_id_array, 
                    'post__not_in' => array($post->ID));
                    
                
                $new_query = new WP_Query($args);            
                if($new_query->post_count)
                {
                    
                    $out .= '<div class="blog-post-related">';                                                              
                        $out .= '<div class="section-title-one"><span class="title-name">'.__('Related posts', 'dc_theme').'</span></div>';
                    
                        $new_query->current_post = 0;
                        $counter = 0;
                        while($new_query->current_post < $new_query->post_count)
                        {                            
                            $rp = & $new_query->posts[$new_query->current_post];
                            
                            
                            if($counter == $related_on_row)
                            {
                                $out .= '<div class="separator"></div>';
                                $counter = 0;
                            } 
                            $counter++;
                            
                            $rp_opt = get_post_meta($rp->ID, 'post_opt', true);
                            $rp_opt['image_url'] = dcf_isNGGImageID($rp_opt['image_url']);
                            
                            $margin = ($counter != $related_on_row) ? ' add-margin' : '';                      
                            $out .= '<div class="item'.$margin.'">';                                  
                                $out .= '<div class="thumb-wrapper">';
                                    $out .= '<a class="async-img-s" href="'.get_permalink($rp->ID).'" rel="'.dcf_getTimThumbURL($rp_opt['image_url'], 136, 68).'" ></a>';
                                $out .= '</div>';
                                $out .= '<a class="title" href="'.get_permalink($rp->ID).'">'.$rp->post_title.'</a>';
                            $out .= '</div>'; 
                            
                            $new_query->current_post++;
                                                                                   
                        }        
                        
                        
                        
                        $out .= '<div class="clear-both"></div>';            
                    $out .= '</div>';
                }
            } 
        }       
        
        // return
        if($echo) { echo $out; } else { return $out; }        
    } 
 
    public function renderHomepageTabs()
    {
        if(!GetDCCPInterface()->getIHome()->isTabsShowed()) 
        { 
            $out = '';
           // $out .= '<div id="homepage-tabs-empty"></div>'; 
            echo $out;
            return; 
        }
        
        global $wpdb;
        
        $querystr = '';
        if(GetDCCPInterface()->getIHome()->useTabsUserList())
        {
            $querystr = "
                SELECT ID, post_title, post_content, post_date_gmt
                FROM $wpdb->posts
                WHERE $wpdb->posts.post_type = '".CPThemeCustomPosts::PT_HOMEPAGE_TAB_POST."' 
                AND $wpdb->posts.post_status = 'publish' 
                AND $wpdb->posts.ID IN (".GetDCCPInterface()->getIHome()->getTabsUserList().")
                ORDER BY $wpdb->posts.post_date ".GetDCCPInterface()->getIHome()->getTabsOrder();
        } else
        {                    
            $querystr = "
                SELECT ID, post_title, post_content, post_date_gmt
                FROM $wpdb->posts
                WHERE $wpdb->posts.post_type = '".CPThemeCustomPosts::PT_HOMEPAGE_TAB_POST."' 
                AND $wpdb->posts.post_status = 'publish' 
                ORDER BY $wpdb->posts.post_date ".GetDCCPInterface()->getIHome()->getTabsOrder()." LIMIT 0, ".GetDCCPInterface()->getIHome()->getTabsCount();
        }
                                                                                                    
        $data = $wpdb->get_results($querystr, OBJECT);
        
        // check number of grabed tabs from db 
        $out = '';
        $count = count($data);
        if($count > 0)
        {
            $out .= '<div id="homepage-tabs">';
            if(!GetDCCPInterface()->getIHome()->isTabHeadShowed())
            {
			    $out .= '<div class="head"><span class="head-text">'.stripcslashes(GetDCCPInterface()->getIHome()->getTabHeadText()).'</span></div>';
            }
                $out .= '<div class="tabs-wrapper">';
                    for($i = 0; $i < $count; $i++)
                    {
                        $out .= $this->renderHomepageSingleTab($data[$i]);
                    }
                $out .= '</div>';
                $out .= '<div class="btn-bar">';
                    for($i = 0; $i < $count; $i++)
                    {
                        $out .= $this->renderHomepageSingleTabBtn($data[$i]);
                    }                
                $out .= '</div>';
            $out .= '</div>';
                
        } else
        {
            // $out .= '<div id="homepage-tabs-empty"></div>';     
        }     
        
        $out .= '<div class="clear-both"></div>';
        
        echo $out;
    }
 
    /*********************************************************** 
    * Private functions
    ************************************************************/      
    
    public function renderHomepageSingleTab(& $obj)
    {    
        $tabopt =  get_post_meta($obj->ID, 'hometab_opt', true);
        $image = dcf_isNGGImageID($tabopt['image_url']); 
                  
        $link_out = '';
        $link_before = '';
        $link_after = '';
        if((bool)$tabopt['linkuse_cbox'])
        {
            $link_before .= ' <a ';
            
            if($tabopt['linktype'] == DCC_MetaMultiple::LINK_PAGE and $tabopt['linkpage'] != CMS_NOT_SELECTED)
            {
                $link_before .= ' href="'.get_permalink($tabopt['linkpage']).'" ';
            } else
            {
                $link_before .= ' href="'.$tabopt['link'].'" ';
            }                                                        
            
            if($tabopt['linktarget'] == DCC_MetaMultiple::LINK_BLANK)
            {
                $link_before .= ' target="_blank" ';
            } else
            {
                $link_before .= ' target="_self" ';
            }
            $link_before .= '>';
            
            $link_out .= $link_before.'&raquo;&nbsp;'.__('Read more', CMS_TXT_DOMAIN);
            
            $link_after .= '</a>';
            $link_out .= $link_after;
        } 
        if((bool)$tabopt['not_read_more_cbox']) { $link_out = ''; }
        
        $out = '';
        $out .= '<div class="tab">';
        
            /*
            $out .= '<a style="height:'.$tabopt['image_height'].'px;" ';
            if((bool)$tabopt['linkuse_cbox'])
            {
                if($tabopt['linktype'] == DCC_MetaMultiple::LINK_PAGE and $tabopt['linkpage'] != CMS_NOT_SELECTED)
                {
                    $out .= ' href="'.get_permalink($tabopt['linkpage']).'" ';
                } else
                {
                    $out .= ' href="'.$tabopt['link'].'" ';
                }                                                        
                
                if($tabopt['linktarget'] == DCC_MetaMultiple::LINK_BLANK)
                {
                    $out .= ' target="_blank" ';
                } else
                {
                    $out .= ' target="_self" ';
                }
            }
            $out .= ' class="image async-img-s" rel="'.dcf_getTimThumbURL($image, 180, $tabopt['image_height']).'"/></a>'; */
           
            $out .= '<div class="text">';
                        if(GetDCCPInterface()->getIHome()->isTabDateShowed() and !(bool)$tabopt['hide_date_cbox'])      
                        {
                            $out .= '<div class="date">'.mysql2date(GetDCCPInterface()->getIHome()->getTabDateFormat(), $obj->post_date_gmt, true).'</div>';
                        }
                        if(!(bool)$tabopt['hide_title_cbox'])
                        {
                            if((bool)$tabopt['linkuse_cbox'])
                            {
                                $out .= '<h3>'.$link_before.$obj->post_title.$link_after.'</h3>';    
                            } else
                            {
                                $out .= '<h3>'.$obj->post_title.'</h3>';
                            }
                        }
                        $out .= apply_filters('the_content', $obj->post_content.$link_out);

            $out .= '</div>';
            
            /*
            if(!(bool)$tabopt['hide_thumbs_cbox'])
            {   
                // thumbs_desc_cbox thumbs_title_cbox                 
                $ngg_images = $tabopt['images_id'];
                $pics = null;
                if($ngg_images != '')
                {
                    $pics = dcf_getNGGImagesFromIDList($ngg_images);
                   // var_dump($pics);
                    $count = count($pics);
                    if($count > 0)
                    {
                        $out .= '<div class="thumb-bar">';
                            for($i = 0; $i < $count; $i++)
                            {
                                $out .= '<a class="thumb pu_img" ';
                                
                                // 
                                
                                if(GetDCCPInterface()->getIHome()->isResizeEnabled() and !((bool)$tabopt['thumbs_disable_resize_cbox']))
                                {                                    
                                    if(GetDCCPInterface()->getIHome()->isResizeKeepSizeEnabled())
                                    {
                                        $max_w = GetDCCPInterface()->getIHome()->getResizeWidth();
                                        $max_h = GetDCCPInterface()->getIHome()->getResizeHeight();
                                        
                                        $new_w = $max_w;
                                        $new_h = $max_h;
                                        
                                        $ratio_w = $max_w / $pics[$i]->_width;
                                        $ratio_h = $max_h / $pics[$i]->_height;
                                        
                                        if($ratio_w < 1.0 or $ratio_h < 1.0)
                                        {
                                            if($ratio_w < $ratio_h)
                                            {
                                                $new_w = (int)round($ratio_w * $pics[$i]->_width);
                                                $new_h = (int)round($ratio_w * $pics[$i]->_height);     
                                            } else
                                            {
                                                $new_w = (int)round($ratio_h * $pics[$i]->_width);
                                                $new_h = (int)round($ratio_h * $pics[$i]->_height);                                                  
                                            }
                                        } 

                                        $out .= 'rel="'.dcf_getTimThumbURL($pics[$i]->_imageURL, $new_w, $new_h).'" ';                                    
                                    
                                    } else
                                    {                                                                            
                                        $out .= 'rel="'.dcf_getTimThumbURL(
                                            $pics[$i]->_imageURL, GetDCCPInterface()->getIHome()->getResizeWidth(), GetDCCPInterface()->getIHome()->getResizeHeight()).'" '; 
                                    }
                                } else
                                {
                                    $out .= 'rel="'.$pics[$i]->_imageURL.'" '; 
                                }
                                
                                if((bool)$tabopt['thumbs_desc_cbox'])
                                {
                                    $out .= 'title="'.$pics[$i]->_description.'" ';
                                }
                                if((bool)$tabopt['thumbs_title_cbox'])
                                {                                
                                    $out .= 'alt="'.$pics[$i]->_alttext.'" ';
                                } else 
                                {
                                    $out .= 'alt="" ';
                                }
                                $out .= '>';
                                $out .= '<img src="'.dcf_getTimThumbURL($pics[$i]->_thumbURL, 25, 25).'" alt="" />';
                                $out .= '</a>';    
                            }                        
                        $out .= '</div>';
                        
                        if(!(bool)$tabopt['disable_gallery_cbox'] and !GetDCCPInterface()->getIHome()->isTabGalleryDisabled())
                        {
                            $out .= '<div class="gallery-box">';
                                for($i = 0; $i < $count; $i++)
                                {
                                    $out .= '<a href="'.$pics[$i]->_imageURL.'" rel="lightbox[tabgall_'.$obj->ID.']"></a>';
                                }            
                            $out .= '</div>';  
                        }
                    }

                }            
            }
            */
            
            $out .= '<div class="clear-both"></div>';

        $out .= '</div>';
        return $out;    
    } 

    public function renderHomepageSingleTabBtn(& $obj)
    {
        $tabopt =  get_post_meta($obj->ID, 'hometab_opt', true);
        $default_tab_id = GetDCCPInterface()->getIHome()->getDefaultTab();
        
        $out = '';
        $out .= '<div class="btn';
        
        if($obj->ID == (int)$default_tab_id) 
        {  
            $out .= ' btn-selected';    
        }
        
        $out .= '">'.$tabopt['btn_name'].'</div>';
        return $out;        
    } 
         
} // class CPTemp
        
        
?>
