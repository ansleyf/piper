<?php
/**********************************************************************
* MODERN ELEGANCE WORDPRESS THEME 
* (Ideal For Business And Personal Use: Portfolio or Blog)     
* 
* File name:   
*      cp_customposts.php
* Brief:       
*      Part of theme control panel.
* Author:      
*      DigitalCavalry
* Author URI:
*      http://themeforest.net/user/DigitalCavalry
* Contact:
*      digitalcavalry@gmail.com 
***********************************************************************/ 
   
class CPThemeCustomPosts 
{    
    const PT_TOUR_POST = 'tourpost';
    const PT_HOMEPAGE_TAB_POST = 'hometabpost';
    
    const PT_SERVICE_POST = 'servicepost';
    
  //  const PT_STORY_POST = 'storypost'; 
  //  const PT_STORY_CAT = 'storycat';
    
    const PT_MEMBER_POST = 'memberpost';
    
    const PT_QUESTION_POST = 'questionpost';
    const PT_QUESTION_CAT = 'questioncat'; 
    
    
    /*********************************************************** 
    * Constructor
    ************************************************************/       
    public function __construct()
    {                                                              
        $is_admin = is_admin();        
        
        // service posts
        add_action('init', array(&$this, 'createServicePosts'));
        if($is_admin)
        {
            add_filter('manage_edit-servicepost_columns', array(&$this, 'adminServiceColumnsCreate'));
            add_action('manage_servicepost_posts_custom_column', array(&$this, 'adminServiceColumnsRender'), 10, 2);   
        }

        // story posts
   //     add_action('init', array(&$this, 'createStoryPosts'));
   //     add_action('init', array(&$this, 'createStoryTaxonomy'));  
   //     if($is_admin)
   //     {      
   //         add_filter('manage_edit-storypost_columns', array(&$this, 'adminStoryColumnsCreate'));
   //         add_action('manage_storypost_posts_custom_column', array(&$this, 'adminStoryColumnsRender'), 10, 2);       
   //     }
        
        // tour posts
        add_action('init', array(&$this, 'createTourPosts'));
        if($is_admin)
        {
            add_filter('manage_edit-tourpost_columns', array(&$this, 'adminTourColumnsCreate'));
            add_action('manage_tourpost_posts_custom_column', array(&$this, 'adminTourColumnsRender'), 10, 2);        
        }
        
        // homepage tabs
        add_action('init', array(&$this, 'createHomepageTabPosts'));
        if($is_admin)
        {
            add_filter('manage_edit-hometabpost_columns', array(&$this, 'adminHomeTabColumnsCreate'));
            add_action('manage_hometabpost_posts_custom_column', array(&$this, 'adminHomeTabColumnsRender'), 10, 2);        
        }
        
        // team member
        add_action('init', array(&$this, 'createMemberPosts'));
        if($is_admin)
        {
            add_filter('manage_edit-memberpost_columns', array(&$this, 'adminMemberColumnsCreate'));
            add_action('manage_memberpost_posts_custom_column', array(&$this, 'adminMemberColumnsRender'), 10, 2);        
        }        
        
        // quetion ans answers
        add_action('init', array(&$this, 'createQuestionPosts'));    
        add_action('init', array(&$this, 'createQuestionTaxonomy'));                 
        if($is_admin)
        {
            add_filter('manage_edit-questionpost_columns', array(&$this, 'adminQuestionColumnsCreate'));
            add_action('manage_questionpost_posts_custom_column', array(&$this, 'adminQuestionColumnsRender'), 10, 2);        
        }        
        
    }
    
    /*********************************************************** 
    * Public functions
    ************************************************************/

    # SERVICE POSTS
    public function createServicePosts()
    {
         register_post_type(
            CPThemeCustomPosts::PT_SERVICE_POST,
            Array(
                'labels' => Array(
                    'name' => 'Services',
                    'singular_name' => 'Service',
                    'add_new' => 'Add New Service',
                    'edit_item' => 'Edit Service', 
                    'add_new_item' => 'Add New Service'
                ),
            'supports' => Array('title','editor', 'custom-fields'),
            'public' => true,
            'exclude_from_search' => false, 
            'rewrite' => Array('slug' => CPThemeCustomPosts::PT_SERVICE_POST, 'with_front' => false)
        ));
    }  
    
    public function adminServiceColumnsCreate($columns)
    {
        $new_columns['cb'] = '<input type="checkbox" />';
 
        $new_columns['title'] = _x('Title', 'column name');
        $new_columns['id'] = __('ID'); 
        $new_columns['author'] = __('Author');
 
        $new_columns['date'] = _x('Date', 'column name');
 
        return $new_columns;        
    }    
 
    public function adminServiceColumnsRender($column_name, $id) 
    {
        switch ($column_name) {
        case 'id':
            echo $id;
                break;
        default:
            break;
        } // end switch
    }  
    
    # STORY POSTS   
/*    public function createStoryPosts()
    {
         register_post_type(
            CPThemeCustomPosts::PT_STORY_POST,
            Array(
                'labels' => Array(
                    'name' => 'Story Slides',
                    'singular_name' => 'Story slide',
                    'add_new' => 'Add New Story slide',
                    'edit_item' => 'Edit Story slide', 
                    'add_new_item' => 'Add New Story slide'
                ),
            'supports' => Array('title','editor', 'custom-fields'),
            'public' => true,
            'exclude_from_search' => false, 
            'rewrite' => Array('slug' => CPThemeCustomPosts::PT_STORY_POST, 'with_front' => false)
        ));
    }  
    
    public function adminStoryColumnsCreate($columns)
    {
        $new_columns['cb'] = '<input type="checkbox" />';
 
        $new_columns['title'] = _x('Title', 'column name');
        $new_columns['id'] = __('ID'); 
        $new_columns['cats'] = __('Categories');  
        $new_columns['author'] = __('Author');        
        $new_columns['date'] = _x('Date', 'column name');
 
        return $new_columns;        
    }    
 
    public function adminStoryColumnsRender($column_name, $id) 
    {
        switch ($column_name) {
        case 'id':
            echo $id;
                break;
        case 'cats':
            {
                $p_cats = wp_get_object_terms($id, CPThemeCustomPosts::PT_STORY_CAT);
                if(is_array($p_cats))
                {
                    $count = count($p_cats);
                    if($count == 0)
                    {
                        echo __('Uncategorized', 'dc_theme');    
                    } else
                    {
                        $counter = 0;
                        foreach($p_cats as $c)
                        {
                            if($counter > 0) { echo ', '; } 
                            $counter++;
                            echo '<a href="'.get_admin_url().'edit.php?post_type='.CPThemeCustomPosts::PT_STORY_POST.'&'.CPThemeCustomPosts::PT_STORY_CAT.'='.$c->slug.'">'.$c->name.'</a>';        
                        }        
                    }
                } else
                {
                    echo '-';
                }
            }
            break;                
        default:
            break;
        } // end switch
    }                 
    
    public function createStoryTaxonomy()
    {
        
         register_taxonomy(
            CPThemeCustomPosts::PT_STORY_CAT, CPThemeCustomPosts::PT_STORY_POST,
            Array(
                'hierarchical' => true,
                'public' => true,
                'show_ui' => true,
                'query_var' => true,
                'show_tagcloud' => true,
                'rewrite' => Array('slug' => CPThemeCustomPosts::PT_STORY_CAT, 'with_front' => false),
                
                'labels' => Array(
                  'name' => 'Story Category',
                  'singular_name' => 'Story Category',
                  'search_items' => 'Search Story Categories',
                  'popular_items' => 'Popular Story Categories',
                  'all_items' => 'All Story Categories',
                  'parent_item' => 'Parent Story Category',
                  'parent_item_colon' => 'Parent Story Category',
                  'edit_item' => 'Edit Story Category',
                  'update_item' => 'Update Story Category',
                  'add_new_item' => 'Add New Story Category',
                  'new_item_name' => 'New Story Category Name' 
                ),
                
                'label' => 'Story Category'
                
        ) 
         );                                    
    }    
*/    
    # QUESTIONS
    public function createQuestionPosts()
    {
         register_post_type(
            CPThemeCustomPosts::PT_QUESTION_POST,
            Array(
                'labels' => Array(
                    'name' => 'Questions (FAQ)',
                    'singular_name' => 'Question',
                    'add_new' => 'Add New Question',
                    'edit_item' => 'Edit Question', 
                    'add_new_item' => 'Add New Question'
                ),
            'supports' => Array('title','editor', 'custom-fields'),
            'public' => true,
            'exclude_from_search' => false, 
            'rewrite' => Array('slug' => CPThemeCustomPosts::PT_QUESTION_POST, 'with_front' => false)
        ));        
    }    
    
    public function createQuestionTaxonomy()
    {
        
         register_taxonomy(
            CPThemeCustomPosts::PT_QUESTION_CAT, CPThemeCustomPosts::PT_QUESTION_POST,
            Array(
                'hierarchical' => true,
                'public' => true,
                'show_ui' => true,
                'query_var' => true,
                'show_tagcloud' => true,
                'rewrite' => Array('slug' => CPThemeCustomPosts::PT_QUESTION_CAT, 'with_front' => false),
                
                'labels' => Array(
                  'name' => 'Question Category',
                  'singular_name' => 'Question Category',
                  'search_items' => 'Search Question Categories',
                  'popular_items' => 'Popular Question Categories',
                  'all_items' => 'All Question Categories',
                  'parent_item' => 'Parent Question Category',
                  'parent_item_colon' => 'Parent Question Category',
                  'edit_item' => 'Edit Question Category',
                  'update_item' => 'Update Question Category',
                  'add_new_item' => 'Add New Question Category',
                  'new_item_name' => 'New Question Category Name' 
                ),
                
                'label' => 'Question Category'
                
        ) 
         );                                    
    }
            
    public function adminQuestionColumnsCreate($columns)
    {   
        $new_columns['cb'] = '<input type="checkbox" />';
 
        $new_columns['title'] = _x('Title', 'column name');
        $new_columns['id'] = __('ID');
        $new_columns['cats'] = __('Categories');  
        $new_columns['author'] = __('Author');
 
        $new_columns['date'] = _x('Date', 'column name');
 
        return $new_columns;        
    }    
 
    public function adminQuestionColumnsRender($column_name, $id) 
    {
        switch ($column_name) {
        case 'id':
            echo $id;
            break;
        case 'cats':
            {
                $p_cats = wp_get_object_terms($id, CPThemeCustomPosts::PT_QUESTION_CAT);
                if(is_array($p_cats))
                {
                    $count = count($p_cats);
                    if($count == 0)
                    {
                        echo __('Uncategorized', 'dc_theme');    
                    } else
                    {
                        $counter = 0;
                        foreach($p_cats as $c)
                        {
                            if($counter > 0) { echo ', '; } 
                            $counter++;
                            echo '<a href="'.get_admin_url().'edit.php?post_type='.CPThemeCustomPosts::PT_QUESTION_POST.'&'.CPThemeCustomPosts::PT_QUESTION_CAT.'='.$c->slug.'">'.$c->name.'</a>';        
                        }        
                    }
                } else
                {
                    echo '-';
                }
            }
            break;
        default:
            break;
        } // end switch
    }      
    
    # TEAM POSTS
    public function createMemberPosts()
    {
         register_post_type(
            CPThemeCustomPosts::PT_MEMBER_POST,
            Array(
                'labels' => Array(
                    'name' => 'Team Members',
                    'singular_name' => 'Member',
                    'add_new' => 'Add New Member',
                    'edit_item' => 'Edit Member', 
                    'add_new_item' => 'Add New Member'
                ),
            'supports' => Array('title','editor', 'excerpt', 'comments', 'custom-fields'),
            'public' => true,
            'exclude_from_search' => false, 
            'rewrite' => Array('slug' => CPThemeCustomPosts::PT_MEMBER_POST, 'with_front' => false)
        ));
    }   
    
    public function adminMemberColumnsCreate($columns)
    {
        $new_columns['cb'] = '<input type="checkbox" />';
 
        $new_columns['title'] = _x('Title', 'column name');
        $new_columns['id'] = __('ID'); 
        $new_columns['author'] = __('Author');
 
        $new_columns['date'] = _x('Date', 'column name');
 
        return $new_columns;        
    }    
 
    public function adminMemberColumnsRender($column_name, $id) 
    {
        switch ($column_name) {
        case 'id':
            echo $id;
                break;
        default:
            break;
        } // end switch
    }      
        
    
    # TOUR POSTS
    public function createTourPosts()
    {
         register_post_type(
            CPThemeCustomPosts::PT_TOUR_POST,
            Array(
                'labels' => Array(
                    'name' => 'Tour Slides',
                    'singular_name' => 'Tour Slide',
                    'add_new' => 'Add New Tour Slide',
                    'edit_item' => 'Edit Tour Slide', 
                    'add_new_item' => 'Add New Tour Slide'
                ),
            'supports' => Array('title','editor', 'custom-fields'),
            'public' => true,
            'exclude_from_search' => false, 
            'rewrite' => Array('slug' => CPThemeCustomPosts::PT_TOUR_POST, 'with_front' => false)
        ));
    }   
    
    public function adminTourColumnsCreate($columns)
    {
        $new_columns['cb'] = '<input type="checkbox" />';
 
        $new_columns['title'] = _x('Title', 'column name');
        $new_columns['id'] = __('ID'); 
        $new_columns['author'] = __('Author');
 
        $new_columns['date'] = _x('Date', 'column name');
 
        return $new_columns;        
    }    
 
    public function adminTourColumnsRender($column_name, $id) 
    {
        switch ($column_name) {
        case 'id':
            echo $id;
                break;
        default:
            break;
        } // end switch
    }      

    # HOMEPAGE TAB POSTS
    public function createHomepageTabPosts()
    {
         register_post_type(
            CPThemeCustomPosts::PT_HOMEPAGE_TAB_POST,
            Array(
                'labels' => Array(
                    'name' => 'Home Tabs',
                    'singular_name' => 'Home Tab',
                    'add_new' => 'Add New Home Tab',
                    'edit_item' => 'Edit Home Tab', 
                    'add_new_item' => 'Add New Home Tab'
                ),
            'supports' => Array('title','editor', 'custom-fields'),
            'public' => true,
            'exclude_from_search' => false, 
            'rewrite' => Array('slug' => CPThemeCustomPosts::PT_HOMEPAGE_TAB_POST, 'with_front' => false)
        ));
    }   
    
    public function adminHomeTabColumnsCreate($columns)
    {
        $new_columns['cb'] = '<input type="checkbox" />';
 
        $new_columns['title'] = _x('Title', 'column name');
        $new_columns['id'] = __('ID'); 
        $new_columns['author'] = __('Author');
 
        $new_columns['date'] = _x('Date', 'column name');
 
        return $new_columns;        
    }    
 
    public function adminHomeTabColumnsRender($column_name, $id) 
    {
        switch ($column_name) {
        case 'id':
            echo $id;
                break;
        default:
            break;
        } // end switch
    }      
  
} // class    
    
?>
