<?php
/**********************************************************************
* MODERN ELEGANCE WORDPRESS THEME 
* (Ideal For Business And Personal Use: Portfolio or Blog)      
* 
* File name:   
*      cms_functions.php
* Brief:       
*      Theme CMS functions file
* Author:      
*      DigitalCavalry
* Author URI:
*      http://themeforest.net/user/DigitalCavalry
* Contact:
*      digitalcavalry@gmail.com 
***********************************************************************/
              
function dcf_getSecurityImage()
{
  $value = array('image' => '', 'code' => ''); 
    
  $value['code'] = (string)rand(0, 9).(string)rand(0, 9).(string)rand(0, 9).(string)rand(0, 9);  
  $value['image'] = '<img src="'.get_bloginfo('template_url').'/php/authorization.php?code='.(string)$value['code'].'" />';
  $value['code'] = md5(md5($value['code']));
  
/*    
  $url = get_bloginfo('template_url').'/php/authorization.php?code=10';   
  $ch = curl_init();
  $timeout = 5;
  curl_setopt($ch,CURLOPT_URL,$url);
  curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
  curl_setopt($ch,CURLOPT_CONNECTTIMEOUT,$timeout);
  $data = curl_exec($ch);
  curl_close($ch);
    
  $data = base64_encode($data); 
  $value = '<img src="data:image/png;base64,'.$data.'" />'; 
*/  
        
  return $value;
}

/*****************************************************************
    NAME: dcf_getTimThumbURL  
    
    DESCRIPTION:
        This function returns path to timthumb script which should be
        located in main theme folder and named thumb.php
    
    PARAMETERS:
        $w - image width
        $h - image height
        $src - image url, default set to null
        $z - zoom and crop, can be set to 0 or 1, default set to 1
        $q - image quality, can be set value form 1 to 100, default set to 75
        
    RETURN VALUE:
        url to timthumb script with image path and parameters,
        if src parameter is not set, function will return only
        path to timthumb script
         
*****************************************************************/ 
function dcf_getTimThumbURL($src=null, $w=null, $h=null, $z=1, $q=85)
{  
    $value = '';
    
    if($src === null)  
    {
        $value = get_bloginfo('template_url').'/thumb.php';    
    } else
    {
        $value = get_bloginfo('template_url').'/thumb.php';
        $value .= '?src='.$src; 
        $value .= '&w='.$w;
        $value .= '&h='.$h;
        $value .= '&z='.$z; 
        $value .= '&q='.$q;  
    }
     
    //$value = dcf_resizeImage(null, $src, $w, $h, true);
    return $value;
}

/*****************************************************************
    NAME: dcf_strNWords  
    
    DESCRIPTION:
         This function returns first N words from given string  
    
    PARAMETERS:
        $str - string
        $number - number of first words from string to return
        
    RETURN VALUE:
         first N words from given string
         
*****************************************************************/ 
function dcf_strNWords($str, $number=16)
{
    $value = $str;
    $array = explode(' ', $str);

    if(count($array) <= $number)
    {
        $value = $str;
    }
    else
    {
        array_splice($array, $number);
        $value = implode(' ', $array).'..';
    }
    return $value;
}

function dcf_imgCalcMaxSizeInRatio($max_w, $max_h, $width, $height)
{
    $new_w = $max_w;
    $new_h = $max_h;
    
    $ratio_w = $max_w / $width;
    $ratio_h = $max_h / $height;
    
    if($ratio_w < 1.0 or $ratio_h < 1.0)
    {
        if($ratio_w < $ratio_h)
        {
            $new_w = (int)round($ratio_w * $width);
            $new_h = (int)round($ratio_w * $height);     
        } else
        {
            $new_w = (int)round($ratio_h * $width);
            $new_h = (int)round($ratio_h * $height);                                                  
        }
    } 
    
    $rect = new DCC_CRectangle($new_w, $new_h);
    return $rect;    
}

function dcf_getGalleriesNGG($order_by = 'gid', $order_dir = 'ASC', $counter = false, $limit = 0, $start = 0, $exclude = true, $selected_ids='', $excluded_ids='', & $max_page=null) 
{      
        
    global $wpdb; 
    global $nggdb;    
    $galleries = false;  
      
    if(isset($wpdb) and isset($nggdb))  
    {            
        // Check for the exclude setting
        $exclude_clause = ($exclude) ? ' AND exclude<>1 ' : '';
        $order_dir = ( $order_dir == 'DESC') ? 'DESC' : 'ASC';
        $limit_by  = ( $limit > 0 ) ? 'LIMIT ' . intval($start) . ',' . intval($limit) : '';
        $excluded_galleries = ($excluded_ids != '') ? " $wpdb->nggallery.gid NOT IN ($excluded_ids) " : '';        
        $selected_galleries = ($selected_ids != '') ? " $wpdb->nggallery.gid IN ($selected_ids) " : '';
        $before = '';
        
        if($excluded_galleries != '' or $selected_galleries != '')
        {
            $before = ' WHERE ';
        }   
        
        if($excluded_galleries != '' and $selected_galleries != '')
        {
            $selected_galleries = ' AND '.$selected_galleries;    
        }       
        
        $galleries = $wpdb->get_results( "SELECT SQL_CALC_FOUND_ROWS * FROM $wpdb->nggallery $before $excluded_galleries $selected_galleries ORDER BY {$order_by} {$order_dir} {$limit_by}", OBJECT_K );
        
        // Count the number of galleries
        if($max_page !== null and $limit > 0)
        {
            $total = intval( $wpdb->get_var( "SELECT FOUND_ROWS()" ) );            
            $max_page = (int)ceil($total / $limit);
        }       
        
        if ( !$galleries )
        {         
            return array();
        }
        
        $galleriesID = array(); 
        // get the galleries information    
        foreach ($galleries as $key => $value) {
            $galleriesID[] = $key;
            // init the counter values
            $galleries[$key]->counter = 0;
            $galleries[$key]->title = stripslashes($galleries[$key]->title);
            $galleries[$key]->galdesc  = stripslashes($galleries[$key]->galdesc);   
        }

        // if we didn't need to count the images then stop here
        if ( !$counter )
        {
            return $galleries;
        }
        
        // get the counter values   
        $picturesCounter = $wpdb->get_results('SELECT galleryid, COUNT(*) as counter FROM '.$wpdb->nggpictures.' WHERE galleryid IN (\''.implode('\',\'', $galleriesID).'\') ' . $exclude_clause . ' GROUP BY galleryid', OBJECT_K);

        if ( !$picturesCounter )
        {
            return $galleries;
        }
        
        // add the counter to the gallery object    
        foreach ($picturesCounter as $key => $value) {
            $galleries[$value->galleryid]->counter = $value->counter;
        }
    }
    
    return $galleries;    
}

function dcf_getGalleryNGG($id, $order_by = 'sortorder', $order_dir = 'ASC', $exclude = true, $limit = 0, $start = 0, & $max_page=null) 
{
    global $wpdb;
    global $nggdb;    

    // init the gallery as empty array
    $gallery = array();    
    
    if(isset($wpdb) and isset($nggdb))
    {                   
        // Check for the exclude setting
        $exclude_clause = ($exclude) ? ' AND tt.exclude<>1 ' : '';
        
        // Say no to any other value
        $order_dir = ( $order_dir == 'DESC') ? 'DESC' : 'ASC';
        $order_by  = ( empty($order_by) ) ? 'sortorder' : $order_by;
        
        // Should we limit this query ?
        $limit_by  = ( $limit > 0 ) ? 'LIMIT ' . intval($start) . ',' . intval($limit) : '';
        
        // Query database
        $result = $wpdb->get_results(
            $wpdb->prepare( "SELECT SQL_CALC_FOUND_ROWS tt.*, t.* 
            FROM $wpdb->nggallery AS t INNER JOIN $wpdb->nggpictures AS tt ON t.gid = tt.galleryid 
            WHERE t.gid = %d {$exclude_clause} 
            ORDER BY tt.{$order_by} {$order_dir} {$limit_by}", $id ), OBJECT_K );

        if($max_page !== null and $limit > 0)
        {
            $total = intval( $wpdb->get_var( "SELECT FOUND_ROWS()" ) );            
            $max_page = (int)ceil($total / $limit);
        }              
        
        // Build the object
        if($result) 
        {                
            // Now added all image data
            foreach ($result as $key => $value)
            {
                $pic = new nggImage( $value );                
                $new_pic = new DCC_NGGImage(
                    $pic->pid,
                    $pic->imageURL,
                    $pic->thumbURL,
                    $pic->meta_data['width'],
                    $pic->meta_data['height'],
                    $pic->thumbcode,
                    $pic->description,
                    $pic->alttext
                );           
                 
                array_push($gallery, $new_pic);                 
            }
        } else
        {
            $gallery = false;    
        }
    } else
    {
        $gallery = false;    
    } 
    
    return $gallery;        
}

function dcf_getNGGLastImages($page = 0, $limit = 15, $exclude = true, $gid = 0, $orderby = "id")
{
    global $nggdb;
    $gallery = false;
    if(isset($nggdb))
    {
        $result = $nggdb->find_last_images($page, $limit, $exclude, $gid, $orderby); 
        if($result !== null)
        {
            $gallery = array();
            // Now added all image data
            foreach($result as $pic)
            {
                $new_pic = new DCC_NGGImage(
                    $pic->pid,
                    $pic->imageURL,
                    $pic->thumbURL,
                    $pic->meta_data['width'],
                    $pic->meta_data['height'],
                    $pic->thumbcode,
                    $pic->description,
                    $pic->alttext
                );           
                 
                array_push($gallery, $new_pic); 
            }            
        }
    }        
    
    return $gallery;      
}

function dcf_getNGGRandomImages($number = 1, $gid = 0)
{
    global $nggdb;
    $gallery = false;
    if(isset($nggdb))
    {
        $result = $nggdb->get_random_images($number, $gid); 
        if($result !== null)
        {
            $gallery = array();
            // Now added all image data
            foreach($result as $pic)
            {
                $new_pic = new DCC_NGGImage(
                    $pic->pid,
                    $pic->imageURL,
                    $pic->thumbURL,
                    $pic->meta_data['width'],
                    $pic->meta_data['height'],
                    $pic->thumbcode,
                    $pic->description,
                    $pic->alttext
                );           
                 
                array_push($gallery, $new_pic); 
            }            
        }
    }        
    
    return $gallery;          
}

function dcf_getNGGInfo($gid)
{    
    global $nggdb;
    $gallery = false;
    if(isset($nggdb))
    {
        $gallery = $nggdb->find_gallery($gid); 
    }        
    
    return $gallery;  
}

function dcf_getWholeNGG($gid, & $ret_count=null)
{
    
    global $nggdb;
    $objs = array();
    if(isset($nggdb))
    {
        $gallery = $nggdb->find_gallery($gid); 
        if($gallery !== false)
        {
            $pics = $nggdb->get_gallery($gid); 
            
            $count = 0;
            if(is_array($pics)) { $count = count($pics); } 
            if($ret_count !== null) { $ret_count = $count; } 
            
            foreach($pics as $pic)
            {
                $new_pic = new DCC_NGGImage(
                    $pic->pid,
                    $pic->imageURL,
                    $pic->thumbURL,
                    $pic->meta_data['width'],
                    $pic->meta_data['height'],
                    $pic->thumbcode,
                    $pic->description,
                    $pic->alttext
                );           
                 
                array_push($objs, $new_pic); 
            }                                        
        }
    }        
    
    return $objs;  
}

function dcf_sortPostInUserOrder($id_array, $data)
{
    $new_posts_array = Array(); 
    $count = count($data);
    foreach($id_array as $us_id)
    {
        $index = -1;
        for($i = 0; $i < $count; $i++)
        {
            if($data[$i]->ID == $us_id)
            {
                $index = $i;
                break;
            }
        }
        if($index != -1)
        {
            array_push($new_posts_array, $data[$index]);
        }
    }
    return $new_posts_array;                  
}   

function dcf_getWPDBFoundRows()
{
    global $wpdb;
    $querystr = "SELECT FOUND_ROWS();";
    $allitems = $wpdb->get_results($querystr, ARRAY_N);
    $allitems = (int)$allitems[0][0]; 
    
    return $allitems;   
}

function dcf_GetWPMainDir() 
{
    $value = TEMPLATEPATH;
    $pos = strpos(TEMPLATEPATH, 'wp-content');
    $value = substr($value, 0, $pos);
    return $value;
}

function dcf_isNGGImageID($path, $object=false)
{
    $value = $path;
    if($value == '') return $value;
    
    $numeric = is_numeric(trim($path));       
    if($numeric)
    {
        
        global $nggdb;
        if(isset($nggdb))
        {
            $id = (int)$path;
            $list = array();
            $list[0] = $id;
            $pics = $nggdb->find_images_in_list($list, false);
            $pics = array_values($pics);
            $count = count($pics);
            if($count == 0)
            {
                $value = '';    
            } else
            {
                if($object)
                {
                    $new_pic = new DCC_NGGImage(
                        $pics[0]->pid,
                        $pics[0]->imageURL,
                        $pics[0]->thumbURL,
                        $pics[0]->meta_data['width'],
                        $pics[0]->meta_data['height'],
                        $pics[0]->thumbcode,
                        $pics[0]->description,
                        $pics[0]->alttext
                    );                       
                    
                    $value = $new_pic;    
                } else
                {
                    $value = $pics[0]->imageURL; 
                }
            }
        }    
    }
    return $value; 
}

function dcf_getNGGImagesFromGallery($gid, & $ret_count=null, $num=null)
{
    $pics = array();
    if($gid != CMS_NOT_SELECTED)
    {
        global $nggdb;
        if(isset($nggdb))
        {
            $gallery = $nggdb->find_gallery($gid);
            if($gallery != false)
            {                 
                $pics = $nggdb->get_gallery($gid);
            }
        }
    }

    $count = count($pics);
    if($ret_count !== null) { $ret_count = $count; }    
    $objs = array();
    
    $counter = 0;
    foreach($pics as $pic)
    {
        $new_pic = new DCC_NGGImage(
            $pic->pid,
            $pic->imageURL,
            $pic->thumbURL,
            $pic->meta_data['width'],
            $pic->meta_data['height'],
            $pic->thumbcode,
            $pic->description,
            $pic->alttext
        );           
         
        array_push($objs, $new_pic); 
        
        if($num !== null)
        {
            $counter++;
            if($counter >= $num)
            {
                break;
            }    
        }
    }
        
    return $objs;    
}

/*****************************************************************
    NAME: dcf_getNGGImagesFromIDList  
    
    PARAMETERS:
        $img_ids - string with images id's
        $ret_count - optional, variable in which will be return number of images
        $num - optional number of images to get
        
    RETURN VALUE:
        array of image objects
         
*****************************************************************/ 
function dcf_getNGGImagesFromIDList($img_ids, & $ret_count=null, $num=null)
{
    $imga_ids = null;
    
    $pos = strpos($img_ids, '-');
    if($pos !== false)
    {
        $values = explode('-', $img_ids);
        $imga_ids = array();
        for($i = (int)$values[0]; $i <= (int)$values[1]; $i++)
        {
            array_push($imga_ids, $i);    
        }        
    } else
    {
        $imga_ids = explode(',', $img_ids);
    }
    $count = count($imga_ids);
    if($ret_count !== null) { $ret_count = $count; }
    $pics = null;
    if($count > 0)
    {
        global $nggdb;
        if(isset($nggdb))
        {
            $new_array = array();
            if($num !== null)
            {
                for($i = 0; $i < $num; $i++)
                {
                    array_push($new_array, $imga_ids[$i]);     
                }
                $imga_ids = $new_array;    
            }
            $pics = $nggdb->find_images_in_list($imga_ids, false);
        }
    }

    $objs = array();
    if(is_array($pics))
    {
        foreach($pics as $pic)
        {
            $new_pic = new DCC_NGGImage(
                $pic->pid,
                $pic->imageURL,
                $pic->thumbURL,
                $pic->meta_data['width'],
                $pic->meta_data['height'],
                $pic->thumbcode,
                $pic->description,
                $pic->alttext
            );           
             
            array_push($objs, $new_pic); 
        }
    }
        
    return $objs;       
}


/*****************************************************************
    NAME: dcf_naviTree  
    
    PARAMETERS:
        $id - post or page id
        $level - should be set to zero
        $title - if page dont have title you can set it here
        $extraname - name of extra link inserted before selected page
        $extralink - url for extra link
        
    RETURN VALUE:
        none
         
*****************************************************************/ 
function dcf_naviTree($id, $level, $title='', $extraname='', $extralink='')
{          
    if($level == 0)
    {
        echo '<div class="navigation-tree-container">';
    }
    
    $p = null;
    $parent = 0;
    if($id !== null)
    {    
        $p = get_page($id);
        $parent = $p->post_parent;
    }
    
    if($parent != 0)
    {
        dcf_naviTree($parent, $level+1);    
    } else
    {
        $blogname = get_bloginfo('name');
        if(GetDCCPInterface()->getIGeneral()->isBreadcrumbForcedName())
        {
            $blogname = GetDCCPInterface()->getIGeneral()->getBreadcrumbName();    
        }
        echo GetDCCPInterface()->getIGeneral()->getBreadcrumbBefore().' <a class="link" href="'.get_bloginfo('url').'">'.$blogname.'</a>';
    }  
    
    if($level == 0)
    {
        if($extraname !== '')
        {
            if(is_array($extraname))
            {
                echo '&nbsp;&raquo;&nbsp;';
                $counter = 0;
                foreach($extraname as $key => $value)
                {
                    if($counter > 0) { echo ', '; }
                    echo '<a href="'.$value.'" class="link">'.$key.'</a>';
                    $counter++;    
                }    
            } else
            {
                echo '&nbsp;&raquo;&nbsp;<a href="'.$extralink.'" class="link">'.$extraname.'</a>';    
            }
        }        
        
        if($title == '')
        {
            echo '&nbsp;&raquo;&nbsp;<a class="selected">'.$p->post_title.'</a>';
        } else
        {        
            echo '&nbsp;&raquo;&nbsp;<a class="selected">'.$title.'</a>';
        }
        
        echo '</div>'; 
    } else
    {
        echo '&nbsp;&raquo;&nbsp;<a href="'.get_permalink($p->ID).'" class="link">'.$p->post_title.'</a>';
    }
    
        
}     
  
/*****************************************************************
    NAME: dcf_neatTrim
    
    PARAMETERS:
        $str - string to trim
        $n - max number of displayed chars
        $delim - small string added on the end of generated string
        $cuted - true if string was cuted  
        
    RETURN VALUE:
        Cuted string  
*****************************************************************/ 
function dcf_neatTrim($str, $n, $delim='...', & $cuted=null) 
{
    $str = str_replace("\n",'',$str);
    $str = str_replace("\r",'',$str);
    $str = strip_tags($str);
 
   $len = strlen($str); 
   if ($len > $n) { 
       
       if(isset($cuted))
       {
           $cuted = true;
       }
       
       
       preg_match('/(.{' . $n . '}.*?)\b/', $str, $matches);           
        
       return rtrim($matches[1]) . $delim; 
   } 
   else { 
       if(isset($cuted))
       {
           $cuted = false;
       }           
       
       return $str; 
   } 
}

/*****************************************************************
    NAME: dcf_neatTrim
    
    PARAMETERS:
        $str - string to trim
        $n - max number of displayed chars
        $delim - small string added on the end of generated string
        $cuted - true if string was cuted  
        
    RETURN VALUE:
        Cuted string  
*****************************************************************/ 
function dcf_getTwitterTweets($username, $password, $count, &$add) {
    
    if($count < 1) $count = 1;
    
    $format = 'json'; // set format    
    $url = 'http://api.twitter.com/statuses/user_timeline/'.$username.'.'.$format.'?count='.$count;    
    
    $ch = curl_init();
 
    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); //Set curl to return the data instead of printing it to the browser
   // curl_setopt($ch, CURLOPT_USERPWD, $username . ":" . $password);
    curl_setopt($ch, CURLOPT_URL, $url);
 
    $data = curl_exec($ch);
    $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);

    $result = false;
    if($httpCode == 200) // 503 overloading, 404 not exist, 200 success
    {
        $data = json_decode($data);
        
        $result = Array();
        $count = count($data);
        for($i = 0; $i < $count; $i++)
        {
            $obj = new DCC_TwitterTweet();
            $obj->_text = $data[$i]->text;
            $obj->_date = date('F j, Y, g:i a', strtotime($data[$i]->created_at));
            $obj->_source = $data[$i]->source;
            array_push($result, $obj);
        }                             
        
        $url = "http://twitter.com/users/".$username.".json";    
        $ch = curl_init(); 
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); //Set curl to return the data instead of printing it to the browser.
        // curl_setopt($ch, CURLOPT_USERPWD, $username . ":" . $password);
        curl_setopt($ch, CURLOPT_URL, $url);       
        $data = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        curl_close($ch);    
        $data = json_decode($data); 

        $add = $data;
    }
                  
    return $result;
} 


/*****************************************************************
    NAME: dcf_comments_number   
*****************************************************************/
function dcf_calculatePastTime($pre, $hour, $minute, $second, $month, $day, $year)
{
    $old = mktime($hour, $minute, $second, $month, $day, $year);
    $new = mktime();
    $value = $new - $old;
    
    $second = $value;
    if($second < 0) { $second = 0; } 
    $minute = floor($second/60);
    $hour = floor($second/60/60);
    $day = floor($second/60/60/24);
    $month = floor($second/60/60/24/30);
    $year = floor($second/60/60/24/365);
    
    if($pre != '') { $pre .= ' '; }
    
    // check years
    if($year > 0)
    {
        if($year == 1) { $value = $pre.__('1 year ago', 'dc_theme'); } else { $value = $pre.$year.' '.__('years ago', 'dc_theme'); }
    } else // check months
    if($month > 0)
    {
        if($month == 1) { $value = $pre.__('1 month ago', 'dc_theme'); } else { $value = $pre.$month.' '.__('months ago', 'dc_theme'); }
    } else // check days
    if($day > 0)
    {
        if($day == 1) { $value = $pre.__('1 day ago', 'dc_theme'); } else { $value = $pre.$day.' '.__('days ago', 'dc_theme'); }
    } else // check hours
    if($hour > 0)
    {
        if($hour == 1) { $value = $pre.__('1 hour ago', 'dc_theme'); } else { $value = $pre.$hour.' '.__('hours ago', 'dc_theme'); }
    } else // check minutes        
    if($minute > 0)
    {
        if($minute == 1) { $value = $pre.__('1 minute ago', 'dc_theme'); } else { $value = $pre.$minute.' '.__('minutes ago', 'dc_theme'); }
    } else // check seconds
    if($second >= 0)
    {
        if($second == 0) { $value = __('Right now', 'dc_theme'); } else
        if($second == 1) { $value = $pre.__('1 second ago', 'dc_theme'); } else { $value = $pre.$second.' '.__('seconds ago', 'dc_theme'); }
    }                        
    
    return $value;
}


function dcf_calculatePostPastTime($pre='')
{
    global $post;
    
    $hour = get_post_time('H');
    $minute = get_post_time('i');
    $second = get_post_time('s');
    $month = get_post_time('n');
    $day = get_post_time('j');
    $year = get_post_time('Y'); 
    
    $old = mktime($hour, $minute, $second, $month, $day, $year);
    $new = mktime();
    $value = $new - $old;
    
    $second = $value;
    if($second < 0) { $second = 0; } 
    $minute = floor($second/60);
    $hour = floor($second/60/60);
    $day = floor($second/60/60/24);
    $month = floor($second/60/60/24/30);
    $year = floor($second/60/60/24/365);
    
    // check years
    if($year > 0)
    {
        if($year == 1) { $value = $pre.__('1 year ago', 'dc_theme'); } else { $value = $pre.$year.' '.__('years ago', 'dc_theme'); }
    } else // check months
    if($month > 0)
    {
        if($month == 1) { $value = $pre.__('1 month ago', 'dc_theme'); } else { $value = $pre.$month.' '.__('months ago', 'dc_theme'); }
    } else // check days
    if($day > 0)
    {
        if($day == 1) { $value = $pre.__('1 day ago', 'dc_theme'); } else { $value = $pre.$day.' '.__('days ago', 'dc_theme'); }
    } else // check hours
    if($hour > 0)
    {
        if($hour == 1) { $value = $pre.__('1 hour ago', 'dc_theme'); } else { $value = $pre.$hour.' '.__('hours ago', 'dc_theme'); }
    } else // check minutes        
    if($minute > 0)
    {
        if($minute == 1) { $value = $pre.__('1 minute ago', 'dc_theme'); } else { $value = $pre.$minute.' '.__('minutes ago', 'dc_theme'); }
    } else // check seconds
    if($second >= 0)
    {
        if($second == 0) { $value = __('Right now', 'dc_theme'); } else
        if($second == 1) { $value = $pre.__('1 second ago', 'dc_theme'); } else { $value = $pre.$second.' '.__('seconds ago', 'dc_theme'); }
    }                        
    
    return $value;
}

/*****************************************************************
    NAME: dcf_next_post 
*****************************************************************/
function dcf_next_post($type='post', $tax='', $terms='')
{   
    global $wpdb;
    global $post;
                             
    $current_date = $post->post_date;
    
    $querydate = '';
    $querydate .= " AND $wpdb->posts.post_date > TIMESTAMP('$current_date') ";                   
    
    $subquery = '';        
    if($terms != '')
    {
        $a_terms = explode(',', $terms);
        $count_terms = count($a_terms);                
        if($count_terms > 0)
        {
            for($i = 0; $i < $count_terms; $i++)
            {
                if($i > 0)
                {
                    $subquery .= ' OR ';    
                }
                $subquery .= "$wpdb->terms.slug = '".$a_terms[$i]."' ";
            }
            $subquery = ' AND ('.$subquery.') ';
        }
    }          

    $querytax = '';
    if($tax != '')
    {
        $querytax = " AND $wpdb->term_taxonomy.taxonomy = '$tax' "; 
    }
    
    $querystr = "
        SELECT ID, post_title, post_date
        FROM $wpdb->posts
        LEFT JOIN $wpdb->term_relationships ON($wpdb->posts.ID = $wpdb->term_relationships.object_id)
        LEFT JOIN $wpdb->term_taxonomy ON($wpdb->term_relationships.term_taxonomy_id = $wpdb->term_taxonomy.term_taxonomy_id)
        LEFT JOIN $wpdb->terms ON($wpdb->term_taxonomy.term_id = $wpdb->terms.term_id)
        WHERE $wpdb->posts.post_type = '$type'".
        $querydate.
        "AND $wpdb->posts.post_status = 'publish' ".
        $querytax.
        $subquery.
        "GROUP BY $wpdb->posts.ID ORDER BY $wpdb->posts.post_date ASC LIMIT 1";

    $list = $wpdb->get_results($querystr, OBJECT);                
    $count = count($list);    
    
    $value = false;
    if($count > 0) { $value = $list[0]; }
    return $value;
}  

/*****************************************************************
    NAME: dcf_prev_post 
*****************************************************************/
function dcf_prev_post($type='post', $tax='', $terms='')
{   
    global $wpdb;
    global $post;
                             
    
    $current_date = $post->post_date;
    
    $querydate = '';
    $querydate .= " AND $wpdb->posts.post_date < TIMESTAMP('$current_date') ";                   
    
    $subquery = '';        
    if($terms != '')
    {
        $a_terms = explode(',', $terms);
        $count_terms = count($a_terms);                
        if($count_terms > 0)
        {
            for($i = 0; $i < $count_terms; $i++)
            {
                if($i > 0)
                {
                    $subquery .= ' OR ';    
                }
                $subquery .= "$wpdb->terms.slug = '".$a_terms[$i]."' ";
            }
            $subquery = ' AND ('.$subquery.') ';
        }
    }          
    
    $querytax = '';
    if($tax != '')
    {
        $querytax = " AND $wpdb->term_taxonomy.taxonomy = '$tax' "; 
    }
    
    $querystr = "
        SELECT ID, post_title, post_date
        FROM $wpdb->posts
        LEFT JOIN $wpdb->term_relationships ON($wpdb->posts.ID = $wpdb->term_relationships.object_id)
        LEFT JOIN $wpdb->term_taxonomy ON($wpdb->term_relationships.term_taxonomy_id = $wpdb->term_taxonomy.term_taxonomy_id)
        LEFT JOIN $wpdb->terms ON($wpdb->term_taxonomy.term_id = $wpdb->terms.term_id)
        WHERE $wpdb->posts.post_type = '$type'".
        $querydate.
        "AND $wpdb->posts.post_status = 'publish' ".
        $querytax.
        $subquery.
        "GROUP BY $wpdb->posts.ID ORDER BY $wpdb->posts.post_date DESC LIMIT 1";

    $list = $wpdb->get_results($querystr, OBJECT);             
    $count = count($list); 
    
    $value = false;
    if($count > 0) { $value = $list[0]; }
    return $value;       
} 


/*
 * Resize images dynamically using wp built in functions
 * Victor Teixeira
 *
 * php 5.2+
 *
 * Exemple use:
 * 
 * <?php 
 * $thumb = get_post_thumbnail_id(); 
 * $image = vt_resize( $thumb,'' , 140, 110, true, 70 );
 * ?>
 * <img src="<?php echo $image[url]; ?>" width="<?php echo $image[width]; ?>" height="<?php echo $image[height]; ?>" />
 *
 * @param int $attach_id
 * @param string $img_url
 * @param int $width
 * @param int $height
 * @param bool $crop
 * @param int $jpeg_quality
 * @return array
 */
function dcf_resizeImage( $attach_id = null, $img_url = null, $width, $height, $crop = false, $jpeg_quality = 90 ) 
{

    // this is an attachment, so we have the ID
    if ( $attach_id ) {
    
        $image_src = wp_get_attachment_image_src( $attach_id, 'full' );
        $file_path = get_attached_file( $attach_id );
    
    // this is not an attachment, let's use the image url
    } else if ( $img_url ) {
        
           $file_path = parse_url( $img_url );   
        $img_path = $file_path['path'];
        $wp_start = strpos($img_path, "wp-content/");
        $img_path = substr($img_path, $wp_start);
        $file_path = $img_path;
        
        $orig_size = getimagesize( $file_path );
        
        $image_src[0] = $img_url;
        $image_src[1] = $orig_size[0];
        $image_src[2] = $orig_size[1];
    }
    
    $file_info = pathinfo( $file_path );
    $extension = '.'. $file_info['extension'];

    // the image path without the extension
    $no_ext_path = $file_info['dirname'].'/'.$file_info['filename'];

    $cropped_img_path = $no_ext_path.'-'.$width.'x'.$height.$extension;

    // checking if the file size is larger than the target size
    // if it is smaller or the same size, stop right here and return
    if ( $image_src[1] > $width || $image_src[2] > $height ) {

        // the file is larger, check if the resized version already exists (for crop = true but will also work for crop = false if the sizes match)
        if ( file_exists( $cropped_img_path ) ) {

            $cropped_img_url = str_replace( basename( $image_src[0] ), basename( $cropped_img_path ), $image_src[0] );
            
            $vt_image = array (
                'url' => $cropped_img_url,
                'width' => $width,
                'height' => $height
            );
            
            return $vt_image['url'];
        }

        // crop = false
        if ( $crop == false ) {
        
            // calculate the size proportionaly
            $proportional_size = wp_constrain_dimensions( $image_src[1], $image_src[2], $width, $height );
            $resized_img_path = $no_ext_path.'-'.$proportional_size[0].'x'.$proportional_size[1].$extension;            

            // checking if the file already exists
            if ( file_exists( $resized_img_path ) ) {
            
                $resized_img_url = str_replace( basename( $image_src[0] ), basename( $resized_img_path ), $image_src[0] );

                $vt_image = array (
                    'url' => $resized_img_url,
                    'width' => $new_img_size[0],
                    'height' => $new_img_size[1]
                );
                
                return $vt_image['url'];
            }
        }

        // no cached files - let's finally resize it
        $new_img_path = image_resize( $file_path, $width, $height, $crop, $jpeg_quality );
        $new_img_size = getimagesize( $new_img_path );
        $new_img = str_replace( basename( $image_src[0] ), basename( $new_img_path ), $image_src[0] );

        // resized output
        $vt_image = array (
            'url' => $new_img,
            'width' => $new_img_size[0],
            'height' => $new_img_size[1]
        );
        
        return $vt_image['url'];
    }

    // default output - without resizing
    $vt_image = array (
        'url' => $image_src[0],
        'width' => $image_src[1],
        'height' => $image_src[2]
    );
    
    return $vt_image['url'];
}

?>
