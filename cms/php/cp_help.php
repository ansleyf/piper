<?php
/**********************************************************************
* MODERN ELEGANCE WORDPRESS EDITION 
* (Ideal For Business And Personal Use: Portfolio or Blog)     
* 
* File name:   
*      cp_help.php
* Brief:       
*      Part of theme control panel.
* Author:      
*      DigitalCavalry
* Author URI:
*      http://themeforest.net/user/DigitalCavalry
* Contact:
*      digitalcavalry@gmail.com 
***********************************************************************/

/*********************************************************** 
* Definitions
************************************************************/

/*********************************************************** 
* Class name:
*    CPHelp
* Descripton:
*    Implementation of CPHelp 
***********************************************************/
class CPHelp extends DCC_CPBaseClass 
{        
    /*********************************************************** 
    * Constructor
    ************************************************************/
    public function __construct() 
    {
       
        
    } // constructor 

    /*********************************************************** 
    * Public members
    ************************************************************/      
    
    /*********************************************************** 
    * Private members
    ************************************************************/         
   
    /*********************************************************** 
    * Public functions
    ************************************************************/               
 
     public function renderTab()
     {
        echo '<div class="cms-content-wrapper">';
        $this->renderCMS();
        echo '</div>';
     }
 
    /*********************************************************** 
    * Private functions
    ************************************************************/      
    private function renderCMS()
    {      
         global $wpdb; 
      
         # CATEGORIES
         $out = '';        
         $out .= '<h6 class="cms-h6">Post categories ID</h6><hr class="cms-hr"/>';         
         $cats = get_terms('category');         
         if(is_array($cats))
         {
             $out .= '<select style="width:400px;">';
             foreach($cats as $item)
             {
                $out .= '<option>'.$item->name . ' (ID: '.$item->term_id.')</option>';    
             }
             $out .= '</select>';
         } else
         {
            $out .= '<span class="cms-info-bar">No categories</span>';  
         }
         echo $out;
         
         # PAGES
         $pages = $wpdb->get_results("SELECT ID, post_title FROM $wpdb->posts WHERE post_status = 'publish' AND post_type = 'page' ORDER BY post_title ");         
         
         $out = '';        
         $out .= '<div style="height:20px;"></div>';
         $out .= '<h6 class="cms-h6">Pages ID</h6><hr class="cms-hr"/>';
         if(is_array($pages))
         {
             $out .= '<select style="width:400px;">';
             foreach($pages as $item)
             {
                $out .= '<option>'.$item->post_title . ' (ID: '.$item->ID.')</option>';    
             }
             $out .= '</select>';
         } else
         {
            $out .= '<span class="cms-info-bar">No pages</span>';  
         }
         echo $out;                                                   
                    
    }
         
} // class CPHelp
        
        
?>