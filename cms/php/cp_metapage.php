<?php
/**********************************************************************
* MODERN ELEGANCE WORDPRESS THEME 
* (Ideal For Business And Personal Use: Portfolio or Blog)     
* 
* File name:   
*      cp_metapage.php
* Brief:       
*      Part of theme control panel.
* Author:      
*      DigitalCavalry
* Author URI:
*      http://themeforest.net/user/DigitalCavalry
* Contact:
*      digitalcavalry@gmail.com 
***********************************************************************/

/*********************************************************** 
* Definitions
************************************************************/

/*********************************************************** 
* Class name:
*    CPMetaPageCommon
* Descripton:
*    Implementation of CPMetaPageCommon
***********************************************************/
class CPMetaPageCommon extends DCC_MetaMultiple 
{        
    /*********************************************************** 
    * Constructor
    ************************************************************/
    public function __construct() 
    {
        $this->_names = array(
            'pagecommon_opt', 'pagecommon_opt_cbox', 'page_sid', 'page_sid_pos', 'page_bg_img', 'page_bg_attachment',
            'page_bg_color', 'page_bg_color_use_cbox', 'page_bg_repeat', 'page_image', 'page_area_show_cbox', 'page_area_width', 'page_area_height', 'page_area_content',
            'page_area_bg_color', 'page_area_bg_color_use_cbox');              
        $this->_std = array(
            'pagecommon_opt_cbox' => false,
            'page_sid' => CMS_NOT_SELECTED,
            'page_sid_pos' => CMS_SIDEBAR_RIGHT,
            'page_bg_img' => '',
            'page_bg_attachment' => 'scroll',
            'page_bg_repeat' => 'no-repeat',
            'page_bg_color' => 'E2E2E2',
            'page_bg_color_use_cbox' => false,
            'page_image' => '',
            'page_area_show_cbox' => false,
            'page_area_width' => 1000,
            'page_area_height' => 100,
            'page_area_content' => '',
            'page_area_bg_color' => 'FFFFFF',
            'page_area_bg_color_use_cbox' => false
            );
                             
        $this->_title = 'PAGE COMMON SETTINGS:';
        $this->_type = 'page';
        $this->_desc = '';             
    } // constructor 

     private $_panelsubmenu = array (
        'pagecommonsettings' => 'Page common settings ',
        'contactpagetemplate' => 'Contact page',
        'blogpagetemplate' => 'Blog',
        'tourpagetemplate' => 'Tour',
        'servicespagetemplate' => 'Services',
        'gallerylistpagetemplate' => 'Gallery list',
        'gallerypagetemplate' => 'Gallery',
        'portfoliopagetemplate' => 'Portfolio',
        'portfoliolistpagetemplate' => 'Portfolio List',
        'mediapagetemplate' => 'Media',
        'archivelistpagetemplate' => 'Archive list',
        'teampagetemplate' => 'Team',
        'questionspagetemplate' => 'Questions'     
     );       
    
    /*********************************************************** 
    * Public functions
    ************************************************************/    
    public function display()
    {       
        $value = $this->initDisplay();            
        
        $out = '';
        
        global $post;
        echo '<span class="cms-meta-normal">POST INFORMATION: ID='.$post->ID.'</span>';
        $this->renderPanelSubmenu($this->_panelsubmenu);
                 
        // title
        $out .= '<a name="pagecommonsettings"></a>';
        $out .= '<div class="cms-custom-field-panel ">';
        $out .= '<div class="cms-meta-bold">'.$this->_title.'</div><br />';                       
        $out .= '<input type="checkbox" name="pagecommon_opt_cbox" '.$this->attrChecked($value['pagecommon_opt_cbox']).' /> Save settings for page<br /><br />';        
        
        // sibedar
        $out .= '<span class="cms-meta-normal">Select page sidebar (if not set, default sidebar will be used)</span><br />';
        $out .= GetDCCPInterface()->getIGeneral()->printSidebarsList(300, 'page_sid', $value['page_sid']);         
        $out .= '<br /><br />';
        
        // sidebar position
        $out .= '<span class="cms-meta-normal">Page sidebar position</span><br />';
        $out .= '<input type="radio" name="page_sid_pos" '.($value['page_sid_pos'] == CMS_SIDEBAR_LEFT ? ' checked="checked"' : '').' value="'.CMS_SIDEBAR_LEFT.'" /> Left<br />';
        $out .= '<input type="radio" name="page_sid_pos" '.($value['page_sid_pos'] == CMS_SIDEBAR_RIGHT ? ' checked="checked"' : '').' value="'.CMS_SIDEBAR_RIGHT.'" /> Right<br />';
        $out .= '<input type="radio" name="page_sid_pos" '.($value['page_sid_pos'] == CMS_SIDEBAR_GLOBAL ? ' checked="checked"' : '').' value="'.CMS_SIDEBAR_GLOBAL.'" /> Global settings<br /><br />'; 
//        $out .= '<input type="radio" name="page_sid_pos" '.($value['page_sid_pos'] == CMS_SIDEBAR_HIDDEN ? ' checked="checked"' : '').' value="'.CMS_SIDEBAR_HIDDEN.'" /> No sidebar<br /><br />';                  
        
        
        // page image
        $out .= '<span class="cms-meta-normal">Page image URL (this is optional, you can also set image ID from NextGEN Gallery)</span><br />';   
            $path = dcf_isNGGImageID($value['page_image']);
            if($path != '')
            {
                $out .= '<img style="display:block;" src="'.$path.'"/>';
            }        
        
        $out .= '<input style="width:480px;" type="text" id="'.'page_image'.'_path" name="'.'page_image'.'" value="'.$value['page_image'].'" />'; 
        $out .= '<input style="width:180px;" class="cms-upload upload_image_button" type="button" value="Upload page image" name="'.'page_image'.'_path" /><br /><br />';                                
         
        
        $out .= '<span class="cms-meta-normal">Page invidaul background</span><br />'; 
        $out .= '<input style="width:480px;" type="text" id="page_bg_img" name="page_bg_img" value="'.$value['page_bg_img'].'" />'; 
        $out .= '<input style="width:180px;" class="cms-upload upload_image_button" type="button" value="Upload background image" name="page_bg_img" /><br /><br />';                                
        
        $out .= '<span class="cms-meta-normal">Background attachment</span><br />';
        $out .= '<input type="radio" name="page_bg_attachment" '.($value['page_bg_attachment'] == 'scroll' ? ' checked="checked"' : '').' value="scroll" /> Scroll<br />';
        $out .= '<input type="radio" name="page_bg_attachment" '.($value['page_bg_attachment'] == 'fixed' ? ' checked="checked"' : '').' value="fixed" /> Fixed<br /><br />';                
        
        $out .= '<span class="cms-meta-normal">Background repeat:</span><br />';
        $out .= '<input type="radio" '.$this->attrChecked($value['page_bg_repeat'] == 'repeat').' value="repeat" name="page_bg_repeat" /> Repeat<br />';         
        $out .= '<input type="radio" '.$this->attrChecked($value['page_bg_repeat'] == 'repeat-x').' value="repeat-x" name="page_bg_repeat" /> Repeat X<br />';
        $out .= '<input type="radio" '.$this->attrChecked($value['page_bg_repeat'] == 'repeat-y').' value="repeat-y" name="page_bg_repeat" /> Repeat Y<br />';
        $out .= '<input type="radio" '.$this->attrChecked($value['page_bg_repeat'] == 'no-repeat').' value="no-repeat" name="page_bg_repeat" /> No repeat<br /><br />';        
        
        // bg color
        $out .= '<span class="cms-meta-normal">Background color</span><br />';         
        $out .= '<input style="width:70px;text-align:center;" type="text" class="colorpicker {hash:true}" value="'.$value['page_bg_color'].'" name="page_bg_color" /> Background color <br /><br />'; 
        $out .= '<input type="checkbox" name="page_bg_color_use_cbox" '.$this->attrChecked($value['page_bg_color_use_cbox']).' /> Use background color<br /><br />';         
        
        // page custom area 
        $out .= '<span class="cms-meta-normal">Page custom area settings:</span><br />'; 
        $out .= '<input type="checkbox" name="page_area_show_cbox" '.$this->attrChecked($value['page_area_show_cbox']).' /> Show area<br />';
        $out .= '<input type="checkbox" name="page_area_bg_color_use_cbox" '.$this->attrChecked($value['page_area_bg_color_use_cbox']).' /> Use area background color<br /><br />';
          
        $out .= '<input style="width:70px;text-align:center;" type="text" name="page_area_width" value="'.$value['page_area_width'].'" /> Area width<br />';
        $out .= '<input style="width:70px;text-align:center;" type="text" name="page_area_height" value="'.$value['page_area_height'].'" /> Area height<br /><br />';         
        $out .= '<input style="width:70px;text-align:center;" type="text" class="colorpicker {hash:true}" value="'.$value['page_area_bg_color'].'" name="page_area_bg_color" /> Area background color <br /><br />';
        
        
        $out .= '<span class="cms-meta-normal">Page area content:</span><br />';
        $out .= '<textarea style="width:640px;height:160px;" name="page_area_content">'.$value['page_area_content'].'</textarea><br />';
        
        $out .= $this->getUpdateBtnHtmlCode(); 
        
        $out .= '</div>';             
        
        echo $out;
    } 
    
    public function renderPanelSubmenu(& $menu, $head = 'Quick menu:', $echo = true)
    {
        $out = '';
        if(is_array($menu))
        {
             
            $out .= '<div class="cms-sub-menu">'; 
            if($head != '') { $out .= '<div class="head">'.$head.'</div>'; }
            
            foreach($menu as $link => $name)
            {
                $out .= '<a href="#'.$link.'">'.$name.'</a><br />';    
            }
            
            $out .= '</div>';            
        }        
        
        if($echo) { echo $out; } else { return $out; }
    }      
} // class CPMetaPageCommon  

/*********************************************************** 
* Class name:
*    CPMetaPageBlogTemplate
* Descripton:
*    Implementation of CPMetaPageBlogTemplate
***********************************************************/
class CPMetaPageBlogTemplate extends DCC_MetaMultiple 
{        
    /*********************************************************** 
    * Constructor
    ************************************************************/
    public function __construct() 
    {
        $this->_names = array(
            'blogpage_opt', 'blogpage_opt_cbox', 'b_cats', 'b_excats', 'b_layout');              
        $this->_std = array(
            'blogpage_opt_cbox' => false,
            'b_cats' => '',
            'b_excats' => '',
            'b_layout' => CMS_POST_LAYOUT_DEFAULT
            );
                             
        $this->_title = '<span class="cms-meta-color">BLOG</span> PAGE TEMPLATE';
        $this->_type = 'page';
        $this->_desc = '';
    } // constructor 

    /*********************************************************** 
    * Public functions
    ************************************************************/    
    public function display()
    {       
        $value = $this->initDisplay();            
        
        $out = '';
        // title
        $out .= '<a name="blogpagetemplate"></a>';
        $out .= '<div class="cms-custom-field-panel ">';
        $out .= '<div class="cms-meta-bold">'.$this->_title.'</div><br />';                       
        $out .= '<input type="checkbox" name="blogpage_opt_cbox" '.$this->attrChecked($value['blogpage_opt_cbox']).' /> Save settings for page<br /><br />';        
        
        // categoriers
        $out .= '<span class="cms-meta-normal">Insert comma separated post categories ID to display posts only from this category (or categories):</span><br />';
        $out .= '<input style="width:680px;" type="text" id="b_cats" name="b_cats" value="'.$value['b_cats'].'" /><br /><br />';
        
        // excluded
        $out .= '<span class="cms-meta-normal">Insert comma separated post categories ID to exclude posts from display:</span><br />';
        $out .= '<input style="width:680px;" type="text" id="b_excats" name="b_excats" value="'.$value['b_excats'].'" /><br /><br />';              
        
        // Force posts layout
        $out .= '<span class="cms-meta-normal">Select layout:</span><br />';
        $out .= '<input type="radio" name="b_layout" '.$this->attrChecked($value['b_layout'] == CMS_POST_LAYOUT_SMALL).' value="'.CMS_POST_LAYOUT_SMALL.'" /> Small<br />';
        $out .= '<input type="radio" name="b_layout" '.$this->attrChecked($value['b_layout'] == CMS_POST_LAYOUT_MEDIUM).' value="'.CMS_POST_LAYOUT_MEDIUM.'" /> Medium<br />';        
        $out .= '<input type="radio" name="b_layout" '.$this->attrChecked($value['b_layout'] == CMS_POST_LAYOUT_BIG).' value="'.CMS_POST_LAYOUT_BIG.'" /> Big<br />';
        $out .= '<input type="radio" name="b_layout" '.$this->attrChecked($value['b_layout'] == CMS_POST_LAYOUT_DEFAULT).' value="'.CMS_POST_LAYOUT_DEFAULT.'" /> Default<br /><br />';    
                  
        
        $out .= $this->getUpdateBtnHtmlCode(); 
        
        $out .= '</div>';
        echo $out;
    } 
} // class CPMetaPageBlogTemplate  

/*********************************************************** 
* Class name:
*    CPMetaPageTourTemplate
* Descripton:
*    Implementation of CPMetaPageTourTemplate
***********************************************************/
class CPMetaPageTourTemplate extends DCC_MetaMultiple 
{        
    /*********************************************************** 
    * Constructor
    ************************************************************/
    public function __construct() 
    {
        $this->_names = array(
            'tourpage_opt', 'tourpage_opt_cbox', 'to_posts', 'to_effect_mode');              
        $this->_std = array(
            'tourpage_opt_cbox' => false,
            'to_posts' => '',
            'to_effect_mode' => CMS_TOUR_EFFECT_SLIDE
            );
                             
        $this->_title = '<span class="cms-meta-color">TOUR</span> PAGE TEMPLATE';
        $this->_type = 'page';
        $this->_desc = '';
    } // constructor 

    /*********************************************************** 
    * Public functions
    ************************************************************/    
    public function display()
    {       
        $value = $this->initDisplay();            
        
        $out = '';
        // title
        $out .= '<a name="tourpagetemplate"></a>';
        $out .= '<div class="cms-custom-field-panel ">';
        $out .= '<div class="cms-meta-bold">'.$this->_title.'</div><br />';                       
        $out .= '<input type="checkbox" name="tourpage_opt_cbox" '.$this->attrChecked($value['tourpage_opt_cbox']).' /> Save settings for page<br /><br />';        
        
        // posts
        $out .= '<span class="cms-meta-normal">Insert comma separated tour posts ID to display:</span><br />';
        $out .= '<input style="width:680px;" type="text" id="to_posts" name="to_posts" value="'.$value['to_posts'].'" /><br /><br />';                 
        
        // slide transition effect
        $out .= '<span class="cms-meta-normal">Tour slide transition effect:</span><br />';
        $out .= '<input type="radio" name="to_effect_mode" '.$this->attrChecked($value['to_effect_mode'] == CMS_TOUR_EFFECT_SLIDE).' value="'.CMS_TOUR_EFFECT_SLIDE.'" /> Slide<br />';
        $out .= '<input type="radio" name="to_effect_mode" '.$this->attrChecked($value['to_effect_mode'] == CMS_TOUR_EFFECT_FADE).' value="'.CMS_TOUR_EFFECT_FADE.'" /> Fade<br /><br />';           
        
        $out .= $this->getUpdateBtnHtmlCode(); 
        
        $out .= '</div>';
        echo $out;
    } 
} // class CPMetaPageTourTemplate  

/*********************************************************** 
* Class name:
*    CPMetaPageServiceTemplate
* Descripton:
*    Implementation of CPMetaPageServiceTemplate
***********************************************************/
class CPMetaPageServiceTemplate extends DCC_MetaMultiple 
{        
    /*********************************************************** 
    * Constructor
    ************************************************************/
    public function __construct() 
    {
        $this->_names = array(
            'servicepage_opt', 'servicepage_opt_cbox', 
            'se_header_one', 'se_show_header_one_cbox', 'se_layout_one', 'se_posts_one', 'se_show_one_cbox',
            'se_subtitle_one', 'se_show_subtitle_one_cbox', 'se_desc_one', 'se_show_desc_one_cbox');              
        $this->_std = array(
            'servicepage_opt_cbox' => false,
            
            'se_header_one' => '',
            'se_show_header_one_cbox' => false,
            'se_subtitle_one' => '',
            'se_show_subtitle_one_cbox' => false,            
            'se_layout_one' => CMS_SERVICE_LAYOUT_BIG,
            'se_posts_one' => '',
            'se_show_one_cbox' => false,
            'se_desc_one' => '',
            'se_show_desc_one_cbox' => false         
            );
                             
        $this->_title = '<span class="cms-meta-color">SERVICE</span> PAGE TEMPLATE';
        $this->_type = 'page';
        $this->_desc = '';
    } // constructor 

    /*********************************************************** 
    * Public functions
    ************************************************************/    
    public function display()
    {       
        $value = $this->initDisplay();            
        
        $out = '';
        // title
        $out .= '<a name="servicespagetemplate"></a>';
        $out .= '<div class="cms-custom-field-panel ">';
        $out .= '<div class="cms-meta-bold">'.$this->_title.'</div><br />';                       
        $out .= '<input type="checkbox" name="servicepage_opt_cbox" '.$this->attrChecked($value['servicepage_opt_cbox']).' /> Save settings for page<br /><br />';        
        
        # First section
        
        // section posts
        $out .= '<span class="cms-meta-normal">Insert comma separated service posts ID to display:</span><br />';
        $out .= '<input style="width:680px;" type="text" id="se_posts_one" name="se_posts_one" value="'.$value['se_posts_one'].'" /><br /><br />';
 
        // section header
        $out .= '<span class="cms-meta-normal">Header:</span><br />';
        $out .= '<input style="width:680px;" type="text" name="se_header_one" value="'.$value['se_header_one'].'" /><br /><br />';        
  
        // section subtitle
        $out .= '<span class="cms-meta-normal">Subtitle:</span><br />';
        $out .= '<input style="width:680px;" type="text" name="se_subtitle_one" value="'.$value['se_subtitle_one'].'" /><br /><br />';  
 
        // section description
        $out .= '<span class="cms-meta-normal">Description:</span><br />';
        $out .= '<textarea style="width:680px;height:100px;font-size:11px;" name="se_desc_one">'.$value['se_desc_one'].'</textarea><br /><br />'; 
        
        $out .= '<span class="cms-meta-normal">Other options:</span><br />'; 
        $out .= '<input type="checkbox" name="se_show_one_cbox" '.$this->attrChecked($value['se_show_one_cbox']).' /> Show first section<br />';                         
        $out .= '<input type="checkbox" name="se_show_header_one_cbox" '.$this->attrChecked($value['se_show_header_one_cbox']).' /> Show header<br />';         
        $out .= '<input type="checkbox" name="se_show_subtitle_one_cbox" '.$this->attrChecked($value['se_show_subtitle_one_cbox']).' /> Show subtitle<br />'; 
        $out .= '<input type="checkbox" name="se_show_desc_one_cbox" '.$this->attrChecked($value['se_show_desc_one_cbox']).' /> Show description<br /><br />'; 
        
        // layout
        $out .= '<span class="cms-meta-normal">Select layout:</span><br />';
        $out .= '<input type="radio" name="se_layout_one" '.$this->attrChecked($value['se_layout_one'] == CMS_SERVICE_LAYOUT_SMALL).' value="'.CMS_SERVICE_LAYOUT_SMALL.'" /> Small list<br />';
        $out .= '<input type="radio" name="se_layout_one" '.$this->attrChecked($value['se_layout_one'] == CMS_SERVICE_LAYOUT_MEDIUM).' value="'.CMS_SERVICE_LAYOUT_MEDIUM.'" /> Medium boxes<br />';           
        $out .= '<input type="radio" name="se_layout_one" '.$this->attrChecked($value['se_layout_one'] == CMS_SERVICE_LAYOUT_BIG).' value="'.CMS_SERVICE_LAYOUT_BIG.'" /> Big on full page width<br /><br />'; 
        
        $out .= $this->getUpdateBtnHtmlCode();
        
        $out .= '</div>';
        echo $out;
    } 
} // class CPMetaPageServiceTemplate  

/*********************************************************** 
* Class name:
*    CPMetaPageGalleryListTemplate
* Descripton:
*    Implementation of CPMetaPageGalleryListTemplate
***********************************************************/
class CPMetaPageGalleryListTemplate extends DCC_MetaMultiple 
{        
    /*********************************************************** 
    * Constructor
    ************************************************************/
    public function __construct() 
    {
        $this->_names = array(
            'gallerylistpage_opt', 'gallerylistpage_opt_cbox', 'gl_selected', 'gl_excluded', 'gl_per_page', 'gl_height', 'gl_order', 'gl_show_desc_cbox');              
        $this->_std = array(
            'gallerylistpage_opt_cbox' => false,
            'gl_selected' => '',
            'gl_excluded' => '',
            'gl_per_page' => 5,
            'gl_height' => 120,
            'gl_order' => CMS_ORDER_ASC,
            'gl_show_desc_cbox' => true
            );
                             
        $this->_title = '<span class="cms-meta-color">GALLERY LIST</span> PAGE TEMPLATE';
        $this->_type = 'page';
        $this->_desc = '';
    } // constructor 

    /*********************************************************** 
    * Public functions
    ************************************************************/    
    public function display()
    {       
        $value = $this->initDisplay();            
        
        $out = '';
        // title
        $out .= '<a name="gallerylistpagetemplate"></a>'; 
        $out .= '<div class="cms-custom-field-panel ">';
        $out .= '<div class="cms-meta-bold">'.$this->_title.'</div><br />';                       
        $out .= '<input type="checkbox" name="gallerylistpage_opt_cbox" '.$this->attrChecked($value['gallerylistpage_opt_cbox']).' /> Save settings for page<br /><br />';        

        // selected galleries
        $out .= '<span class="cms-meta-normal">Insert comma separated NGG galleries ID to display or leave empty to show all:</span><br />';
        $out .= '<input style="width:680px;" type="text" name="gl_selected" value="'.$value['gl_selected'].'" /><br /><br />';  
        
        // excluded galleries
        $out .= '<span class="cms-meta-normal">Insert comma separated NGG galleries ID to exclude from list:</span><br />';
        $out .= '<input style="width:680px;" type="text" name="gl_excluded" value="'.$value['gl_excluded'].'" /><br /><br />';              

        // galleries per page
        $out .= '<span class="cms-meta-normal">Galleries per page (min. 1):</span><br />';
        $out .= '<input style="width:80px;text-align:center;" type="text" name="gl_per_page" value="'.$value['gl_per_page'].'" /><br /><br />'; 

        // Preview image height in pixels
        $out .= '<span class="cms-meta-normal">Preview image height in pixels (min. 80):</span><br />';
        $out .= '<input style="width:80px;text-align:center;" type="text" name="gl_height" value="'.$value['gl_height'].'" /><br /><br />';         
        
        // Order
        $out .= '<span class="cms-meta-normal">Galleries order:</span><br />';
        $out .= '<input type="radio" name="gl_order" '.$this->attrChecked($value['gl_order'] == CMS_ORDER_ASC).' value="'.CMS_ORDER_ASC.'" /> Ascending<br />';          
        $out .= '<input type="radio" name="gl_order" '.$this->attrChecked($value['gl_order'] == CMS_ORDER_DESC).' value="'.CMS_ORDER_DESC.'" /> Descending<br /><br />';
        
        // other options
        $out .= '<span class="cms-meta-normal">Other options:</span><br />';
        $out .= '<input type="checkbox" name="gl_show_desc_cbox" '.$this->attrChecked($value['gl_show_desc_cbox']).' /> Show description under gallery image preview<br /><br />';
        
        $out .= $this->getUpdateBtnHtmlCode(); 
        
        $out .= '</div>';
        echo $out;
    } 
} // class CPMetaPageGalleryListTemplate  


/*********************************************************** 
* Class name:
*    CPMetaPageGalleryTemplate
* Descripton:
*    Implementation of CPMetaPageGalleryTemplate
***********************************************************/
class CPMetaPageGalleryTemplate extends DCC_MetaMultiple 
{        
    /*********************************************************** 
    * Constructor
    ************************************************************/
    public function __construct() 
    {
        $this->_names = array(
            'gallery_opt', 'gallery_opt_cbox', 'ga_gid', 'ga_per_page', 'ga_main_image_cbox', 'ga_layout',
            'ga_show_title_cbox', 'ga_show_desc_cbox', 'ga_thumb_w', 'ga_thumb_h', 'ga_thumb_margin', 'ga_thumb_padding', 'ga_show_tip_cbox',
            'ga_show_3img_onlist_cbox');              
        $this->_std = array(
            'gallery_opt_cbox' => false,
            'ga_gid' => CMS_NOT_SELECTED,
            'ga_per_page' => 30,
            'ga_main_image_cbox' => true,
            'ga_layout' => CMS_GALLERY_LAYOUT_COMPACT,
            'ga_show_title_cbox' => true,
            'ga_show_desc_cbox' => true,
            'ga_show_tip_cbox' => false,
            'ga_thumb_w' => 90,
            'ga_thumb_h' => 90,
            'ga_thumb_margin' => 14,
            'ga_thumb_padding' => 5,
            'ga_show_3img_onlist_cbox' => false 
            );
                             
        $this->_title = '<span class="cms-meta-color">GALLERY</span> PAGE TEMPLATE';
        $this->_type = 'page';
        $this->_desc = '';
    } // constructor 

    /*********************************************************** 
    * Public functions
    ************************************************************/    
    public function display()
    {       
        $value = $this->initDisplay();            
        
        $out = '';
        // title
        $out .= '<a name="gallerypagetemplate"></a>';
        $out .= '<div class="cms-custom-field-panel ">';
        $out .= '<div class="cms-meta-bold">'.$this->_title.'</div><br />';                       
        $out .= '<input type="checkbox" name="gallery_opt_cbox" '.$this->attrChecked($value['gallery_opt_cbox']).' /> Save settings for page<br /><br />';        
        
        // gallery id
        $out .= '<span class="cms-meta-normal">Select gallery to display:</span><br />';
        $out .= $this->selectCtrlNGGList($value['ga_gid'], 'ga_gid', 320);
        $out .= '<br /><br />'; 
        
        // pictures per page
        $out .= '<span class="cms-meta-normal">Pictures per page, if set to zero all images will be displayed without pagination:</span><br />';
        $out .= '<input style="width:80px;text-align:center;" type="text" name="ga_per_page" value="'.$value['ga_per_page'].'" /><br /><br />';                       
        
        // layout
        $out .= '<span class="cms-meta-normal">Gallery layout:</span><br />';
        $out .= '<input type="radio" name="ga_layout" '.$this->attrChecked($value['ga_layout'] == CMS_GALLERY_LAYOUT_COMPACT).' value="'.CMS_GALLERY_LAYOUT_COMPACT.'" /> Compact<br />';          
        $out .= '<input type="radio" name="ga_layout" '.$this->attrChecked($value['ga_layout'] == CMS_GALLERY_LAYOUT_TABLE3).' value="'.CMS_GALLERY_LAYOUT_TABLE3.'" /> Three columns<br />';        
        $out .= '<input type="radio" name="ga_layout" '.$this->attrChecked($value['ga_layout'] == CMS_GALLERY_LAYOUT_TABLE5).' value="'.CMS_GALLERY_LAYOUT_TABLE5.'" /> Five columns (square thumbs)<br /><br />';

        // compact layout settings
        $out .= '<span class="cms-meta-normal">Compact layout settings:</span><br />';
        $out .= '<input style="width:80px;text-align:center;" type="text" name="ga_thumb_w" value="'.$value['ga_thumb_w'].'" /> Thumb width (px)<br />';
        $out .= '<input style="width:80px;text-align:center;" type="text" name="ga_thumb_h" value="'.$value['ga_thumb_h'].'" /> Thumb height (px)<br />';
        $out .= '<input style="width:80px;text-align:center;" type="text" name="ga_thumb_padding" value="'.$value['ga_thumb_padding'].'" /> Thumb padding (px)<br />';
        $out .= '<input style="width:80px;text-align:center;" type="text" name="ga_thumb_margin" value="'.$value['ga_thumb_margin'].'" /> Margin size (px)<br /><br />';   
        
        // other options
        $out .= '<span class="cms-meta-normal">Other options:</span><br />';
        $out .= '<input type="checkbox" name="ga_main_image_cbox" '.$this->attrChecked($value['ga_main_image_cbox']).' /> Show gallery main image<br />';
        $out .= '<input type="checkbox" name="ga_show_tip_cbox" '.$this->attrChecked($value['ga_show_tip_cbox']).' /> Show image title as tip<br />';
        $out .= '<input type="checkbox" name="ga_show_title_cbox" '.$this->attrChecked($value['ga_show_title_cbox']).' /> Show image title (only three and five rows layout) <br />';
        $out .= '<input type="checkbox" name="ga_show_desc_cbox" '.$this->attrChecked($value['ga_show_desc_cbox']).' /> Show image description (only three and five rows layout)<br />';        
        $out .= '<input type="checkbox" name="ga_show_3img_onlist_cbox" '.$this->attrChecked($value['ga_show_3img_onlist_cbox']).' /> Display first three images on gallery list<br /><br />'; 
        
        
        $out .= $this->getUpdateBtnHtmlCode(); 
        
        $out .= '</div>';
        echo $out;
    } 
} // class CPMetaPageGalleryTemplate  

 
/*********************************************************** 
* Class name:
*    CPMetaPagePortfolioTemplate
* Descripton:
*    Implementation of CPMetaPagePortfolioTemplate
***********************************************************/
class CPMetaPagePortfolioTemplate extends DCC_MetaMultiple 
{        
    /*********************************************************** 
    * Constructor
    ************************************************************/
    public function __construct() 
    {
        $this->_names = array(
            'portfolio_opt', 'portfolio_opt_cbox', 'po_cats', 'po_excats', 'po_per_page',
            'po_show_desc_cbox', 'po_show_title_cbox', 'po_show_imgdesc_cbox', 'po_img_as_link_cbox', 'po_layout', 'po_img_h',
            'po_use_img_h_cbox');              
        $this->_std = array(
            'portfolio_opt_cbox' => false,
            'po_cats' => '',
            'po_excats' => '',
            'po_per_page' => 3,
            'po_show_desc_cbox' => true,
            'po_show_title_cbox' => true,
            'po_show_imgdesc_cbox' => true,
            'po_img_as_link_cbox' => false,
            'po_layout' => CMS_PORTFOLIO_LAYOUT_TABLE3,
            'po_img_h' => 128,
            'po_use_img_h_cbox' => false
            );
                             
        $this->_title = '<span class="cms-meta-color">PORTFOLIO</span> PAGE TEMPLATE';
        $this->_type = 'page';
        $this->_desc = '';
    } // constructor 

    /*********************************************************** 
    * Public functions
    ************************************************************/    
    public function display()
    {       
        $value = $this->initDisplay();            
        
        $out = '';
        // title
        $out .= '<a name="portfoliopagetemplate"></a>';
        $out .= '<div class="cms-custom-field-panel ">';
        $out .= '<div class="cms-meta-bold">'.$this->_title.'</div><br />';                       
        $out .= '<input type="checkbox" name="portfolio_opt_cbox" '.$this->attrChecked($value['portfolio_opt_cbox']).' /> Save settings for page<br /><br />';        
        
        // portfolio categories ID
        $out .= '<span class="cms-meta-normal">Insert comma seprated categories ID to display or leave empty to display all categories:</span><br />';
        $out .= '<input type="text" style="width:680px;" name="po_cats" value="'.$value['po_cats'].'" /><br /><br />';

        $out .= '<span class="cms-meta-normal">Insert comma seprated categories ID to exlcude:</span><br />';
        $out .= '<input type="text" style="width:680px;" name="po_excats" value="'.$value['po_excats'].'" /><br /><br />';        
        
        // posts per page
        $out .= '<span class="cms-meta-normal">Posts per page (min. 1):</span><br />';
        $out .= '<input style="width:80px;text-align:center;" type="text" name="po_per_page" value="'.$value['po_per_page'].'" /><br /><br />';           
        
        // layout
        $out .= '<span class="cms-meta-normal">Portfolio layout:</span><br />';
        $out .= '<input type="radio" name="po_layout" '.$this->attrChecked($value['po_layout'] == CMS_PORTFOLIO_LAYOUT_TABLE2).' value="'.CMS_PORTFOLIO_LAYOUT_TABLE2.'" /> Two columns<br />'; 
        $out .= '<input type="radio" name="po_layout" '.$this->attrChecked($value['po_layout'] == CMS_PORTFOLIO_LAYOUT_TABLE3).' value="'.CMS_PORTFOLIO_LAYOUT_TABLE3.'" /> Three columns<br />';          
        $out .= '<input type="radio" name="po_layout" '.$this->attrChecked($value['po_layout'] == CMS_PORTFOLIO_LAYOUT_TABLE4).' value="'.CMS_PORTFOLIO_LAYOUT_TABLE4.'" /> Four columns<br />';        
        $out .= '<input type="radio" name="po_layout" '.$this->attrChecked($value['po_layout'] == CMS_PORTFOLIO_LAYOUT_SIDEBAR).' value="'.CMS_PORTFOLIO_LAYOUT_SIDEBAR.'" /> One column with sidebar<br /><br />';
                
        // optional image height
        $out .= '<span class="cms-meta-normal">Optional image height:</span><br />';
        $out .= '<input style="width:80px;text-align:center;" type="text" name="po_img_h" value="'.$value['po_img_h'].'" /><br /><br />';   
        
        // other options
        $out .= '<span class="cms-meta-normal">Other options:</span><br />';
        $out .= '<input type="checkbox" name="po_show_title_cbox" '.$this->attrChecked($value['po_show_title_cbox']).' /> Show title<br />'; 
        $out .= '<input type="checkbox" name="po_show_imgdesc_cbox" '.$this->attrChecked($value['po_show_imgdesc_cbox']).' /> Show image description<br />'; 
        $out .= '<input type="checkbox" name="po_show_desc_cbox" '.$this->attrChecked($value['po_show_desc_cbox']).' /> Show description<br />'; 
        $out .= '<input type="checkbox" name="po_use_img_h_cbox" '.$this->attrChecked($value['po_use_img_h_cbox']).' /> Use optional image height<br />';  
        $out .= '<input type="checkbox" name="po_img_as_link_cbox" '.$this->attrChecked($value['po_img_as_link_cbox']).' /> Display image as link to post<br /><br />'; 
                
        $out .= $this->getUpdateBtnHtmlCode(); 
        
        $out .= '</div>';
        echo $out;
    } 
} // class CPMetaPagePortfolioTemplate   
 
 
/*********************************************************** 
* Class name:
*    CPMetaPageMediaTemplate
* Descripton:
*    Implementation of CPMetaPageMediaTemplate
***********************************************************/
class CPMetaPageMediaTemplate extends DCC_MetaMultiple 
{        
    /*********************************************************** 
    * Constructor
    ************************************************************/
    public function __construct() 
    {
        $this->_names = array(
            'media_opt', 'media_opt_cbox', 'mg_ids', 'mg_per_page', 'mg_list',
            'mg_use_list_cbox', 'mg_use_ids_cbox',
            'mg_show_author_cbox', 'mg_show_title_cbox', 'mg_show_desc_cbox', 'mg_show_link_cbox', 'mg_show_link_title_cbox',
            'mg_layout');              
        $this->_std = array(
            'media_opt_cbox' => false,
            'mg_ids' => '',
            'mg_list' => CMS_NOT_SELECTED,
            'mg_per_page' => 3,
            'mg_use_list_cbox' => false,
            'mg_use_ids_cbox' => true,
            'mg_show_author_cbox' => true,
            'mg_show_title_cbox' => true,
            'mg_show_desc_cbox' => true,
            'mg_show_link_cbox' => true,
            'mg_show_link_title_cbox' => true,
            'mg_layout' => CMS_MEDIAGALL_LAYOUT_TABLE3
            );
                             
        $this->_title = '<span class="cms-meta-color">MEDIA GALLERY</span> PAGE TEMPLATE';
        $this->_type = 'page';
        $this->_desc = '';
    } // constructor 

    /*********************************************************** 
    * Public functions
    ************************************************************/    
    public function display()
    {       
        $value = $this->initDisplay();            
        
        $out = '';
        // title
        $out .= '<a name="mediapagetemplate"></a>'; 
        $out .= '<div class="cms-custom-field-panel ">';
        $out .= '<div class="cms-meta-bold">'.$this->_title.'</div><br />';                       
        $out .= '<input type="checkbox" name="media_opt_cbox" '.$this->attrChecked($value['media_opt_cbox']).' /> Save settings for page<br /><br />';        

        // portfolio categories ID
        $out .= '<span class="cms-meta-normal">Insert comma seprated media ID from wortTube library:</span><br />';
        $out .= '<input type="text" style="width:680px;" name="mg_ids" value="'.$value['mg_ids'].'" /><br /><br />';        
        
        $out .= '<span class="cms-meta-normal">Select playlist to display:</span><br />';  
        $out .= $this->selectCtrlWordTubePlaylistList($value['mg_list'], 'mg_list', 320);        
        
        // posts per page
        $out .= '<span class="cms-meta-normal">Posts per page (min. 1):</span><br />';
        $out .= '<input style="width:80px;text-align:center;" type="text" name="mg_per_page" value="'.$value['mg_per_page'].'" /><br /><br />';           
        
        // layout
        $out .= '<span class="cms-meta-normal">Select layout:</span><br />';
        $out .= '<input type="radio" name="mg_layout" '.$this->attrChecked($value['mg_layout'] == CMS_MEDIAGALL_LAYOUT_TABLE2).' value="'.CMS_MEDIAGALL_LAYOUT_TABLE2.'" /> Two columns<br />';
        $out .= '<input type="radio" name="mg_layout" '.$this->attrChecked($value['mg_layout'] == CMS_MEDIAGALL_LAYOUT_TABLE3).' value="'.CMS_MEDIAGALL_LAYOUT_TABLE3.'" /> Three columns<br />'; 
        $out .= '<input type="radio" name="mg_layout" '.$this->attrChecked($value['mg_layout'] == CMS_MEDIAGALL_LAYOUT_TABLE4).' value="'.CMS_MEDIAGALL_LAYOUT_TABLE4.'" /> Four columns<br />';
        $out .= '<input type="radio" name="mg_layout" '.$this->attrChecked($value['mg_layout'] == CMS_MEDIAGALL_LAYOUT_SIDEBAR).' value="'.CMS_MEDIAGALL_LAYOUT_SIDEBAR.'" /> One column with sidebar<br /><br />';  
        
        // other options
        $out .= '<span class="cms-meta-normal">Other options:</span><br />';
        $out .= '<input type="checkbox" name="mg_use_list_cbox" '.$this->attrChecked($value['mg_use_list_cbox']).' /> Use selected playlist<br />'; 
        $out .= '<input type="checkbox" name="mg_use_ids_cbox" '.$this->attrChecked($value['mg_use_ids_cbox']).' /> Use manullay selected media ID\'s<br />';       
        $out .= '<input type="checkbox" name="mg_show_author_cbox" '.$this->attrChecked($value['mg_show_author_cbox']).' /> Show author<br />'; 
        $out .= '<input type="checkbox" name="mg_show_title_cbox" '.$this->attrChecked($value['mg_show_title_cbox']).' /> Show title<br />'; 
        $out .= '<input type="checkbox" name="mg_show_desc_cbox" '.$this->attrChecked($value['mg_show_desc_cbox']).' /> Show description<br />'; 
        $out .= '<input type="checkbox" name="mg_show_link_title_cbox" '.$this->attrChecked($value['mg_show_link_title_cbox']).' /> Show link only on title<br />'; 
        $out .= '<input type="checkbox" name="mg_show_link_cbox" '.$this->attrChecked($value['mg_show_link_cbox']).' /> Show link<br /><br />'; 
        
        $out .= $this->getUpdateBtnHtmlCode(); 
        
        $out .= '</div>';
        echo $out;
    } 
} // class CPMetaPageMediaTemplate   


/*********************************************************** 
* Class name:
*    CPMetaPageArchiveListTemplate
* Descripton:
*    Implementation of CPMetaPageArchiveListTemplate
***********************************************************/
class CPMetaPageArchiveListTemplate extends DCC_MetaMultiple 
{        
    /*********************************************************** 
    * Constructor
    ************************************************************/
    public function __construct() 
    {
        $this->_names = array(
            'archivelist_opt', 'archivelist_opt_cbox', 'alist_per_page', 'alist_cats', 'alist_excluded',
            'alist_head');              
        $this->_std = array(
            'archivelist_opt_cbox' => false,
            'alist_per_page' => 30,
            'alist_cats' => '',
            'alist_excluded' => '',
            'alist_head' => ''
            );
                             
        $this->_title = '<span class="cms-meta-color">ARCHIVE LIST</span> PAGE TEMPLATE';
        $this->_type = 'page';
        $this->_desc = '';
    } // constructor 

    /*********************************************************** 
    * Public functions
    ************************************************************/    
    public function display()
    {       
        $value = $this->initDisplay();            
        
        $out = '';
        // title
        $out .= '<a name="archivelistpagetemplate"></a>'; 
        $out .= '<div class="cms-custom-field-panel ">';
        $out .= '<div class="cms-meta-bold">'.$this->_title.'</div><br />';                       
        $out .= '<input type="checkbox" name="archivelist_opt_cbox" '.$this->attrChecked($value['archivelist_opt_cbox']).' /> Save settings for page<br /><br />';        
        
        // categories
        $out .= '<span class="cms-meta-normal">Type here comma separated ID for posts categories which should be displayed or leave blank to display all categories (you can find categories ID in theme help panel):</span><br />';
        $out .= '<input style="width:680px;" type="text" name="alist_cats" value="'.$value['alist_cats'].'" /><br /><br />';                 
        // uexcluded categories
        $out .= '<span class="cms-meta-normal">Type here comma separated ID for posts excluded categories:</span><br />';
        $out .= '<input style="width:680px;" type="text" name="alist_excluded" value="'.$value['alist_excluded'].'" /><br /><br />';   
        // number of posts per page
        $out .= '<span class="cms-meta-normal">Number of posts per page:</span><br />';
        $out .= '<input style="width:60px;text-align:center;" type="text" name="alist_per_page" value="'.$value['alist_per_page'].'" /><br /><br />';   
        // head text
        $out .= '<span class="cms-meta-normal">Head text:</span><br />';
        $out .= '<input style="width:680px;" type="text" name="alist_head" value="'.$value['alist_head'].'" /><br /><br />'; 
        
        $out .= $this->getUpdateBtnHtmlCode(); 
        
        $out .= '</div>';
        echo $out;
    } 
} // class CPMetaPageArchiveListTemplate  


/*********************************************************** 
* Class name:
*    CPMetaPageTeamTemplate
* Descripton:
*    Implementation of CPMetaPageTeamTemplate
***********************************************************/
class CPMetaPageTeamTemplate extends DCC_MetaMultiple 
{        
    /*********************************************************** 
    * Constructor
    ************************************************************/
    public function __construct() 
    {
        $this->_names = array(
            'team_opt', 'team_opt_cbox', 'team_per_page', 'team_members', 'team_readmore_cbox', 'team_layout');              
        $this->_std = array(
            'team_opt_cbox' => false,
            'team_per_page' => 4,
            'team_members' => '',
            'team_readmore_cbox' => true,
            'team_layout' => CMS_TEAM_LAYOUT_BIG,
            'team_'
            );
                             
        $this->_title = '<span class="cms-meta-color">TEAM</span> PAGE TEMPLATE';
        $this->_type = 'page';
        $this->_desc = '';
    } // constructor 

    /*********************************************************** 
    * Public functions
    ************************************************************/    
    public function display()
    {       
        $value = $this->initDisplay();            
        
        $out = '';
        // title
        $out .= '<a name="teampagetemplate"></a>';
        $out .= '<div class="cms-custom-field-panel ">';
        $out .= '<div class="cms-meta-bold">'.$this->_title.'</div><br />';                       
        $out .= '<input type="checkbox" name="team_opt_cbox" '.$this->attrChecked($value['team_opt_cbox']).' /> Save settings for page<br /><br />';                
        
        // number of posts per page
        $out .= '<span class="cms-meta-normal">Number of posts per page, if zero all post will be displayed without pagination:</span><br />';
        $out .= '<input style="width:60px;text-align:center;" type="text" name="team_per_page" value="'.$value['team_per_page'].'" /><br /><br />';          
        
        // selected team members
        $out .= '<span class="cms-meta-normal">Type here comma separated members ID if you want display information about only selected members:</span><br />';
        $out .= '<input style="width:680px;" type="text" name="team_members" value="'.$value['team_members'].'" /><br /><br />';          
        
        // layout
        $out .= '<span class="cms-meta-normal">Select layout:</span><br />';
        $out .= '<input type="radio" name="team_layout" '.$this->attrChecked($value['team_layout'] == CMS_TEAM_LAYOUT_SMALL).' value="'.CMS_TEAM_LAYOUT_SMALL.'" /> Only image<br />';
        $out .= '<input type="radio" name="team_layout" '.$this->attrChecked($value['team_layout'] == CMS_TEAM_LAYOUT_BIG).' value="'.CMS_TEAM_LAYOUT_BIG.'" /> Image and description<br />';  
        $out .= '<input type="radio" name="team_layout" '.$this->attrChecked($value['team_layout'] == CMS_TEAM_LAYOUT_FULL).' value="'.CMS_TEAM_LAYOUT_FULL.'" /> Full width<br /><br />';  
                
        
        // other settings
        $out .= '<span class="cms-meta-normal">Other options:</span><br />';
        $out .= '<input type="checkbox" name="team_readmore_cbox" '.$this->attrChecked($value['team_readmore_cbox']).' /> Allow read more<br />';
        $out .= '<br />';             
        
        $out .= $this->getUpdateBtnHtmlCode(); 
        
        $out .= '</div>';
        echo $out;
    } 
} // class CPMetaPageTeamTemplate  


/*********************************************************** 
* Class name:
*    CPMetaPageQuestionTemplate
* Descripton:
*    Implementation of CPMetaPageQuestionTemplate
***********************************************************/
class CPMetaPageQuestionTemplate extends DCC_MetaMultiple 
{        
    /*********************************************************** 
    * Constructor
    ************************************************************/
    public function __construct() 
    {
        $this->_names = array(
            'quest_opt', 'quest_opt_cbox', 'quest_per_page', 'quest_color', 'quest_forcecolor_cbox', 'quest_counter_cbox', 'quest_views_cbox',
            'quest_cats', 'quest_selected', 'quest_order_views_cbox', 'quest_group_cbox', 'quest_showcatdesc_cbox');              
        $this->_std = array(
            'quest_opt_cbox' => false,
            'quest_per_page' => 12,
            'quest_forcecolor_cbox' => false,
            'quest_color' => '#888888',
            'quest_counter_cbox' => true,
            'quest_views_cbox' => true,
            'quest_order_views_cbox' => false,
            'quest_group_cbox' => false,
            'quest_showcatdesc_cbox' => false, 
            'quest_cats' => '',
            'quest_selected' => ''
            );
                             
        $this->_title = '<span class="cms-meta-color">QUESTIONS (FAQ)</span> PAGE TEMPLATE';
        $this->_type = 'page';
        $this->_desc = '';
    } // constructor 

    /*********************************************************** 
    * Public functions
    ************************************************************/    
    public function display()
    {       
        $value = $this->initDisplay();            
        
        $out = '';
        // title
        $out .= '<a name="questionspagetemplate"></a>';
        $out .= '<div class="cms-custom-field-panel ">';
        $out .= '<div class="cms-meta-bold">'.$this->_title.'</div><br />';                       
        $out .= '<input type="checkbox" name="quest_opt_cbox" '.$this->attrChecked($value['quest_opt_cbox']).' /> Save settings for page<br /><br />';                
        
        // categories id
        $out .= '<span class="cms-meta-normal">Insert comma separated questions categories ID, or leave empty to display all questions (or questions selected below by ID number):</span><br />';
        $out .= '<input style="width:600px;" type="text" name="quest_cats" value="'.$value['quest_cats'].'" /><br /><br />';             
       
        // selected questions
        $out .= '<span class="cms-meta-normal">Type here comma separated questions ID if you want display only selected questions, will not work with selected categories:</span><br />';
        $out .= '<input style="width:680px;" type="text" name="quest_selected" value="'.$value['quest_selected'].'" /><br /><br />';          
               
        
        // number of posts per page
        $out .= '<span class="cms-meta-normal">Number of questions per page, if zero all questions will be displayed without pagination:</span><br />';
        $out .= '<input style="width:60px;text-align:center;" type="text" name="quest_per_page" value="'.$value['quest_per_page'].'" /><br /><br />';                    
        
        // title color             
        $out .= '<span class="cms-meta-normal">Question title color</span><br />';         
        $out .= '<input style="width:70px;text-align:center;" type="text" class="colorpicker {hash:true}" value="'.$value['quest_color'].'" name="quest_color" /> Title color <br /><br />';       
        
        // other settings
        $out .= '<span class="cms-meta-normal">Other options:</span><br />';
        $out .= '<input type="checkbox" name="quest_forcecolor_cbox" '.$this->attrChecked($value['quest_forcecolor_cbox']).' /> Use and force selected color for question title<br />';
        $out .= '<input type="checkbox" name="quest_counter_cbox" '.$this->attrChecked($value['quest_counter_cbox']).' /> Add order number to questions<br />'; 
        $out .= '<input type="checkbox" name="quest_views_cbox" '.$this->attrChecked($value['quest_views_cbox']).' /> Show question views<br />'; 
        $out .= '<input type="checkbox" name="quest_order_views_cbox" '.$this->attrChecked($value['quest_order_views_cbox']).' /> Order questions by views count<br />';
        $out .= '<input type="checkbox" name="quest_group_cbox" '.$this->attrChecked($value['quest_group_cbox']).' /> Group in categories (categories field must be set, will not work with pagination)<br />';        
        $out .= '<input type="checkbox" name="quest_showcatdesc_cbox" '.$this->attrChecked($value['quest_showcatdesc_cbox']).' /> Show category description instead name (works only with group option)<br />'; 
        
        
        $out .= '<br />';           
        
        $out .= $this->getUpdateBtnHtmlCode(); 
        
        $out .= '</div>';
        echo $out;
    } 
}  

/*********************************************************** 
* Class name:
*    CPMetaPageContactTemplate
* Descripton:
*    Implementation of CPMetaPageContactTemplate
***********************************************************/
class CPMetaPageContactTemplate extends DCC_MetaMultiple 
{        
    /*********************************************************** 
    * Constructor
    ************************************************************/
    public function __construct() 
    {
        $this->_names = array(
            'contactpage_opt', 'contactpage_opt_cbox', 'contact_mail');              
        $this->_std = array(
            'contactpage_opt_cbox' => false,
            'contact_mail' => ''
            );
                             
        $this->_title = '<span class="cms-meta-color">CONTACT</span> PAGE TEMPLATE';
        $this->_type = 'page';
        $this->_desc = '';
    } // constructor 

    /*********************************************************** 
    * Public functions
    ************************************************************/    
    public function display()
    {       
        $value = $this->initDisplay();            
        
        $out = '';
        // title
        $out .= '<a name="contactpagetemplate"></a>';
        $out .= '<div class="cms-custom-field-panel ">';
        $out .= '<div class="cms-meta-bold">'.$this->_title.'</div><br />';                       
        $out .= '<input type="checkbox" name="contactpage_opt_cbox" '.$this->attrChecked($value['contactpage_opt_cbox']).' /> Save settings for page<br /><br />';        
        
        // posts
        $out .= '<span class="cms-meta-normal">Your contact email address. You can also insert comma seprated emails - if you need more than one address for contact form:</span><br />';
        $out .= '<input style="width:680px;" type="text" name="contact_mail" value="'.$value['contact_mail'].'" /><br /><br />';                 
          
        $out .= $this->getUpdateBtnHtmlCode(); 
        
        $out .= '</div>';
        echo $out;
    } 
}  

/*********************************************************** 
* Class name:
*    CPMetaPagePortfolioListTemplate
* Descripton:
*    Implementation of CPMetaPagePortfolioListTemplate
***********************************************************/
class CPMetaPagePortfolioListTemplate extends DCC_MetaMultiple 
{        
    /*********************************************************** 
    * Constructor
    ************************************************************/
    public function __construct() 
    {
        $this->_names = array(
            'portlist_opt', 'portlist_opt_cbox', 'pl_selected', 'pl_per_page', 'pl_height');              
        $this->_std = array(
            'portlist_opt_cbox' => false,
            'pl_selected' => '',
            'pl_per_page' => 5,
            'pl_height' => 120
            );
                             
        $this->_title = '<span class="cms-meta-color">PORTFOLIO LIST</span> PAGE TEMPLATE';
        $this->_type = 'page';
        $this->_desc = '';
    } // constructor 

    /*********************************************************** 
    * Public functions
    ************************************************************/    
    public function display()
    {       
        $value = $this->initDisplay();            
        
        $out = '';
        
        // title
        $out .= '<a name="portfoliolistpagetemplate"></a>'; 
        $out .= '<div class="cms-custom-field-panel ">';
        $out .= '<div class="cms-meta-bold">'.$this->_title.'</div><br />';                       
        $out .= '<input type="checkbox" name="portlist_opt_cbox" '.$this->attrChecked($value['portlist_opt_cbox']).' /> Save settings for page<br /><br />';        

        // selected portfolio pages
        $out .= '<span class="cms-meta-normal">Insert comma separated portfolio pages ID to display on list, or leave empty to display all portfolio pages:</span><br />';
        $out .= '<input style="width:680px;" type="text" name="pl_selected" value="'.$value['pl_selected'].'" /><br /><br />';               

        // portfolios per page
        $out .= '<span class="cms-meta-normal">Portfolios per page (min. 1):</span><br />';
        $out .= '<input style="width:80px;text-align:center;" type="text" name="pl_per_page" value="'.$value['pl_per_page'].'" /><br /><br />'; 

        // preview image height in pixels
        $out .= '<span class="cms-meta-normal">Preview image height in pixels (min. 80):</span><br />';
        $out .= '<input style="width:80px;text-align:center;" type="text" name="pl_height" value="'.$value['pl_height'].'" /><br /><br />';         
        
        $out .= $this->getUpdateBtnHtmlCode(); 
        
        $out .= '</div>';
        echo $out;
    } 
} // class CPMetaPagePortfolioListTemplate  