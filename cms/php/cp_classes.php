<?php
/**********************************************************************
* MODERN ELEGANCE WORDPRESS THEME 
* (Ideal For Business And Personal Use: Portfolio or Blog)     
* 
* File name:   
*      cp_classes.php
* Brief:       
*      Part of theme control panel. Definition of global utility classes
* Author:      
*      DigitalCavalry
* Author URI:
*      http://themeforest.net/user/DigitalCavalry
* Contact:
*      digitalcavalry@gmail.com 
***********************************************************************/

/*********************************************************** 
* Class name:
*    DCC_Meta
* Descripton:
*    Implementation of abstract class DCC_Meta 
***********************************************************/
abstract class DCC_Meta 
{
    /*********************************************************** 
    * Public members
    ************************************************************/    
    public $_name; // meta box name
    public $_std; // standard meta value
    public $_title; // title for meta box
    public $_type; // for page or post
    public $_desc; // meta box description
    
    /*********************************************************** 
    * Public functions
    ************************************************************/    
    abstract public function display();
    
    public function save($postID)
    {
        global $post;
        // verify unique random, one time use token  
        if(!wp_verify_nonce($_POST[$this->_name.'_noncename'], plugin_basename(__FILE__) )) 
        {  
            return $postID;  
        }  
          
        if('page' == $_POST['post_type']) 
        {  
            if(!current_user_can( 'edit_page', $postID ))  
            return $postID;  
        } else 
        {  
            if ( !current_user_can( 'edit_post', $postID ))  
            return $postID;  
        }  
        
        // OK, we are authenticated, now we need to find and save the data
        $data = $_POST[$this->_name];        

        // if meta data dont exist create it in database
        if(get_post_meta($postID, $this->_name) == "")  
        {
            add_post_meta($postID, $this->_name, $data, true); 
        } else
        // if meta data exist but ist different from new update it       
        if($data != get_post_meta($postID, $this->_name, true))
        {  
            update_post_meta($postID, $this->_name, $data);          
        } else
        // if no data to save, delete meta data
        if($data == "")  
        {
            delete_post_meta($postID, $this->_name);        
        }        
    } // save
    
    public function initDisplay()
    {
        // decalre global variable which contain post data
        global $post; 
        
        // get meta box value
        $value = get_post_meta($post->ID, $this->_name, true);        
        // if meta box dont have value set it to standard
        if('' == $value) { $value = $this->_std; }

        //  hidden field used to verify the data, width unique random, one time use token
        echo '<input type="hidden" name="'.$this->_name.'_noncename" id="'
            .$this->_name.'_noncename" value="'.wp_create_nonce( plugin_basename(__FILE__) ).'" />';
        
        return $value;        
    } // initDisplay
}

/*********************************************************** 
* Class name:
*    DCC_MetaMultiple
* Descripton:
*    Implementation of abstract class DCC_MetaMultiple 
***********************************************************/
abstract class DCC_MetaMultiple 
{
    const LINK_MANUALLY = 1;
    const LINK_PAGE = 2; 

    const LINK_SELF = 1;
    const LINK_BLANK = 2;      
    
    /*********************************************************** 
    * Public members
    ************************************************************/    
    public $_names; // meta box name
    public $_std; // standard meta value
    public $_title; // title for meta box
    public $_type; // for page or post
    public $_desc; // meta box description
    
    /*********************************************************** 
    * Public functions
    ************************************************************/    
    abstract public function display();
    
    public function save($postID)
    {
        global $post;
        // verify unique random, one time use token  
        if(!wp_verify_nonce($_POST[$this->_names[0].'_noncename'], plugin_basename(__FILE__) )) 
        {  
            return $postID;  
        }  
          
        if('page' == $_POST['post_type']) 
        {  
            if(!current_user_can( 'edit_page', $postID ))  
            return $postID;  
        } else 
       {  
            if ( !current_user_can( 'edit_post', $postID ))  
           return $postID;  
       }  

        $data = array();
        $count = count($this->_names);
        if(isset($_POST[($this->_names[0].'_cbox')]))
        {        
            // first name is reserver for option name, other names are for fileds
            // there are second special name that are created form first name text + _cbox text 
            for($i = 1; $i < $count; $i++)
            {
                $checkbox = strstr($this->_names[$i], '_cbox');
                if($checkbox !== false) { $checkbox = true; }
                
                if($checkbox)
                {
                   $data[$this->_names[$i]] = isset($_POST[$this->_names[$i]]) ? true : false;
                } else
                {
                   $data[$this->_names[$i]] = $_POST[$this->_names[$i]]; 
                }
            }    

            update_post_meta($postID, $this->_names[0], $data);                    
        } else
        {
           $t_value = get_post_meta($postID, $this->_names[0], true);
           if($t_value != '')
           {
                $t_value[($this->_names[0].'_cbox')] = false;
                update_post_meta($postID, $this->_names[0], $t_value);    
           }  
        }
    } // save
    
    public function initDisplay()
    {
        // decalre global variable which contain post data
        global $post; 
        
        // get meta box value
        $value = get_post_meta($post->ID, $this->_names[0], true);        
        // if meta box dont have value set it to standard
        if('' == $value) { $value = $this->_std; }

        //  hidden field used to verify the data, width unique random, one time use token
        echo '<input type="hidden" name="'.$this->_names[0].'_noncename" id="'
            .$this->_names[0].'_noncename" value="'.wp_create_nonce( plugin_basename(__FILE__) ).'" />';
        
        return $value;        
    } // initDisplay
    
    public function selectCtrlPagesList($itempage, $name, $width)
    {
        $out = '';
        $out .= '<select style="width:'.$width.'px;" name="'.$name.'"><option value="'.CMS_NOT_SELECTED.'">Not selected</option>';
        
        global $wpdb;
        $pages = $wpdb->get_results("SELECT ID, post_title FROM $wpdb->posts WHERE post_status = 'publish' AND post_type = 'page' ORDER BY post_title ");
        
        $count = count($pages);
        for($i = 0; $i < $count; $i++)
        {
   
            $out .= '<option value="'.$pages[$i]->ID.'" ';
            
            if($itempage !== null)
            {
                if($pages[$i]->ID == $itempage) $out .= ' selected="selected" ';
            }
            
            $out .= '>';
            $out .= $pages[$i]->post_title.' (ID:'.$pages[$i]->ID.')';
            $out .= '</option>';
               
        } // for        
        $out .= '</select>';
        return $out;     
    } 
    
    public function selectCtrlNGGList($itempage, $name, $width)
    {    
        $out = '';
        
        global $nggdb;
        if(isset($nggdb))
        {
            $gallerylist = $nggdb->find_all_galleries('gid', 'ASC');
            
            $out .= '<select style="width:'.$width.'px;" id="'.$name.'" name="'.$name.'">';
            $out .= '<option value="'.CMS_NOT_SELECTED.'" '.($itempage == CMS_NOT_SELECTED ? ' selected="selected" ' : '').' >Not selected</option>';
            foreach($gallerylist as $gal)
            {
                $out .= '<option ';
                $out .= ' value="'.$gal->gid.'" ';
                $out .= $itempage == $gal->gid ? ' selected="selected" ' : '';
                $out .= '>'.$gal->title.' (ID: '.$gal->gid.')';
                $out .= '</option>';
            }
            $out .= '</select>';
              
        } else
        {
            $out .= '<br /><span class="cms-info-bar">No NextGen Gallery plugin.</span><br />'; 
        }
        
        return $out;            
    }
    
    public function selectCtrlWordTubePlaylistList($itempage, $name, $width)
    {
        $out = '';
        global $wordTube;
        if(isset($wordTube))
        {
            global $wpdb;
            $dbresult = $wpdb->get_results('SELECT * FROM '.$wpdb->wordtube_playlist);
            // var_dump($dbresult);
            
            $count = count($dbresult);
                
            if($count)
            {   
                $out .= '<select style="width:'.$width.'px;" name="'.$name.'"><option value="'.CMS_NOT_SELECTED.'">Not selected</option>';  
                    for($i = 0; $i < $count; $i++)
                    {
                        $out .= '<option ';
                        $out .= ($itempage == $dbresult[$i]->pid) ? ' selected="selected" ' : '';
                        $out .= ' value="'.$dbresult[$i]->pid.'">';
                        $out .= $dbresult[$i]->playlist_name;
                        $out .= '</option>';    
                    }
                $out .= '</select><br />';            
            } else
            {
                $out .= '<br /><span class="cms-info-bar">No playlist created.</span><br />';    
            }        
        } else
        {
            $out .= '<br /><span class="cms-info-bar">No WordTube plugin installed or activated.</span><br />';
        }
        $out .= '<br />';
        
        return $out;        
    }    
    
    public function attrSelected($value)
    {
        $out = '';
        $out .= $value ? ' selected="selected" ' : ''; 
        return $out;
    }

    public function attrChecked($value)
    {
        $out = '';
        $out .= $value ? ' checked="checked" ' : ''; 
        return $out;
    } 
    
    protected function getUpdateBtnHtmlCode()
    {
        return '<input name="save" type="submit" class="button-primary" id="publish" tabindex="5" accesskey="p" value="Update">';
    }      
}

/*********************************************************** 
* Class name:
*    DCC_TwitterTweet
* Descripton:
*    Implementation of class DCC_TwitterTweet 
***********************************************************/
class DCC_TwitterTweet
{
    
    /*********************************************************** 
    * Constructor
    ************************************************************/     
    public function __construct()
    {
        $this->_date = '';
        $this->_source = '';
        $this->_text = '';
    }
   
    /*********************************************************** 
    * Public members
    ************************************************************/     
    public $_text;
    public $_date;
    public $_source;
}

/*********************************************************** 
* Class name:
*    DCC_NGGImage
* Descripton:
*    Implementation of class DCC_NGGImage 
***********************************************************/
class DCC_NGGImage
{
    
    /*********************************************************** 
    * Constructor
    ************************************************************/     
    public function __construct($pid, $imageURL, $thumbURL, $width, $height, $thumbcode, $description, $alttext)
    {
        $this->_pid = $pid;
        $this->_imageURL = $imageURL;
        $this->_thumbURL = $thumbURL;
        $this->_width = $width;
        $this->_height = $height;
        $this->_thumbcode = $thumbcode;
        $this->_description = $description;
        $this->_alttext = $alttext;
    }
   
    /*********************************************************** 
    * Public members
    ************************************************************/     
    public $_pid;
    public $_imageURL;
    public $_thumbURL;
    public $_width;
    public $_height;
    public $_thumbcode;
    public $_description;
    public $_alttext;
    
    
    public function getTimThumbSrc($width, $height)
    {
        $value = get_bloginfo('template_url').'/thumb.php';
        $value .= '?src='.$this->_thumbURL; 
        $value .= '&w='.$width;
        $value .= '&h='.$height;
        $value .= '&z=1'; 
        $value .= '$q=75';
        
        return $value;         
    }
    
    public function getTimThumbSrcRatio($maxwidth, $maxheight)
    {
        $w = $this->_width;
        $h = $this->_height;
        if($w > $maxwidth)
        {
            $ratio = $maxwidth / $this->_width;
            $w = $maxwidth;
            $h = floor($h * $ratio);    
        }

        if($h > $maxheight)
        {
            $ratio = $maxheight / $this->_height;
            $h = $maxheight;
            $w = floor($w * $ratio);                
        }
        
        $value = get_bloginfo('template_url').'/thumb.php';
        $value .= '?src='.$this->_imageURL; 
        $value .= '&w='.$w;
        $value .= '&h='.$h;
        $value .= '&z=1'; 
        $value .= '$q=75';
        
        $arr = array();
        $arr['path'] = $value;
        $arr['w'] = $w;
        $arr['h'] = $h;
        return $arr;         
    }    
}

/*********************************************************** 
* Class name:
*    DCC_CPBaseClass
* Descripton:
*    Implementation of abstract class DCC_CPBaseClass 
***********************************************************/
class DCC_CPBaseClass 
{
    /*********************************************************** 
    * Constructor
    ************************************************************/    
    public function __construct()
    {
    }    
    
    /*********************************************************** 
    * Public members
    ************************************************************/    
    
    /*********************************************************** 
    * Public functions
    ************************************************************/    
    public function selectCtrlPagesList($itempage, $name, $width)
    {
        $out = '';
        $out .= '<select style="width:'.$width.'px;" name="'.$name.'"><option value="'.CMS_NOT_SELECTED.'">Not selected</option>';
        
        global $wpdb;
        $pages = $wpdb->get_results("SELECT ID, post_title FROM $wpdb->posts WHERE post_status = 'publish' AND post_type = 'page' ORDER BY post_title ");
        
        $count = count($pages);
        for($i = 0; $i < $count; $i++)
        {
   
            $out .= '<option value="'.$pages[$i]->ID.'" ';
            
            if($itempage !== null)
            {
                if($pages[$i]->ID == $itempage) $out .= ' selected="selected" ';
            }
            
            $out .= '>';
            $out .= $pages[$i]->post_title.' (ID:'.$pages[$i]->ID.')';
            $out .= '</option>';
               
        } // for        
        $out .= '</select>';
        return $out;     
    } 
    
    public function selectCtrlNGGList($itempage, $name, $width)
    {    
        $out = '';
        
        global $nggdb;
        if(isset($nggdb))
        {
            $gallerylist = $nggdb->find_all_galleries('gid', 'ASC');
            
            $out .= '<select style="width:'.$width.'px;" id="'.$name.'" name="'.$name.'">';
            $out .= '<option value="'.CMS_NOT_SELECTED.'" '.($itempage == CMS_NOT_SELECTED ? ' selected="selected" ' : '').' >Not selected</option>';
            foreach($gallerylist as $gal)
            {
                $out .= '<option ';
                $out .= ' value="'.$gal->gid.'" ';
                $out .= $itempage == $gal->gid ? ' selected="selected" ' : '';
                $out .= '>'.$gal->title.' (ID: '.$gal->gid.')';
                $out .= '</option>';
            }
            $out .= '</select>';
              
        } else
        {
            $out .= '<br /><span class="cms-info-bar">No NextGen Gallery plugin.</span><br />'; 
        }
        
        return $out;            
    }    
    
    public function renderPanelSubmenu(& $menu, $head = 'Quick menu:', $echo = true)
    {
        $out = '';
        if(is_array($menu))
        {
             
            $out .= '<div class="cms-sub-menu">'; 
            if($head != '') { $out .= '<div class="head">'.$head.'</div>'; }
            
            foreach($menu as $link => $name)
            {
                $out .= '<a href="#'.$link.'">'.$name.'</a><br />';    
            }
            
            $out .= '</div>';            
        }        
        
        if($echo) { echo $out; } else { return $out; }
    }    
    
    public function attrSelected($value)
    {
        $out = '';
        $out .= $value ? ' selected="selected" ' : ''; 
        return $out;
    }

    public function attrChecked($value)
    {
        $out = '';
        $out .= $value ? ' checked="checked" ' : ''; 
        return $out;
    }    
    
    public function intRange($v, $down, $top = null)
    {
        if($v < $down) { $v = $down; }
        if($top !== null) { if($v > $top) { $v = $top; } } 
        
        return $v;   
    }   
    
    public function getTopLink()
    {
        return '<a href="#gototop" class="cms-top-link">Top</a>';
    } 
}

/*********************************************************** 
* Class name:
*    DCC_CRectangle
* Descripton:
*    Implementation of abstract class DCC_CRectangle 
***********************************************************/
 class DCC_CRectangle
{    
    /*********************************************************** 
    * Constructor
    ************************************************************/     
    public function __construct($w=0, $h=0)
    {
        $this->_width = $w;
        $this->_height = $h;
    }
   
    /*********************************************************** 
    * Public members
    ************************************************************/     
    public $_width;
    public $_height;      
}


/*********************************************************** 
* Class name:
*    DCC_CCommonLink
* Descripton:
*    Implementation of abstract class DCC_CCommonLink 
***********************************************************/
 class DCC_CCommonLink
{    
    /*********************************************************** 
    * Constructor
    ************************************************************/     
    public function __construct()
    {
        $this->_link = '';
        $this->_page = CMS_NOT_SELECTED;
        $this->_type = CMS_LINK_TYPE_PAGE;
        $this->_target = CMS_LINK_TARGET_SELF;
        $this->_text = 'Link Name';
        $this->_use = false;
    }
   
    /*********************************************************** 
    * Public members
    ************************************************************/     
    public $_link;
    public $_page;
    public $_type;
    public $_target;
    public $_text;
    public $_use;        
}
 

/*********************************************************** 
* Class name:
*    DCC_CGoogleFont
* Descripton:
*    Implementation of abstract class DCC_CGoogleFont 
***********************************************************/
 class DCC_CGoogleFont
{    
    /*********************************************************** 
    * Constructor
    ************************************************************/     
    public function __construct($name, $css, $url)
    {
        $this->_name = $name;
        $this->_css = $css;
        $this->_url = $url;
    }
   
    /*********************************************************** 
    * Public members
    ************************************************************/ 
    public $_name;    
    public $_css;
    public $_url;     
}

/*********************************************************** 
* Class name:
*    DCC_CCufonFont
* Descripton:
*    Implementation of abstract class DCC_CCufonFont 
***********************************************************/
 class DCC_CCufonFont
{    
    /*********************************************************** 
    * Constructor
    ************************************************************/     
    public function __construct($name, $file)
    {
        $this->_name = $name;
        $this->_file = $file;
    }
   
    /*********************************************************** 
    * Public members
    ************************************************************/ 
    public $_name;    
    public $_file;   
}
        
?>
