<?php
/**********************************************************************
* MODERN ELEGANCE WORDPRESS THEME  
* (Ideal For Business And Personal Use: Portfolio or Blog)    
* 
* File name:   
*      cp_meta.php
* Brief:       
*      Part of theme control panel. Keep custom meta box data module.
* Author:      
*      DigitalCavalry
* Author URI:
*      http://themeforest.net/user/DigitalCavalry
* Contact:
*      digitalcavalry@gmail.com 
***********************************************************************/

/*********************************************************** 
* Definitions
************************************************************/

/*********************************************************** 
* Class name:
*    CPMetaDataCreator
* Descripton:
*    Implementation of meta boxes data creator  
***********************************************************/
class CPMetaDataCreator 
{
    /*********************************************************** 
    * Constructor
    ************************************************************/
    public function __construct() 
    {
        # POST META BOXES        
        $this->_obj = new CPMetaCommonPostOpt();
        array_push($this->_post_boxes, $this->_obj);                  
        $this->_obj = new CPMetaPostOpt();
        array_push($this->_post_boxes, $this->_obj);                
        
        # PAGE META BOXES             
        $this->_obj = new CPMetaPageCommon();
        array_push($this->_page_boxes, $this->_obj);             
               
        $this->_obj = new CPMetaPageContactTemplate();
        array_push($this->_page_boxes, $this->_obj);                                                                         
        
        $this->_obj = new CPMetaPageBlogTemplate();
        array_push($this->_page_boxes, $this->_obj); 
        
        $this->_obj = new CPMetaPageTourTemplate();
        array_push($this->_page_boxes, $this->_obj);                 
        
        $this->_obj = new CPMetaPageServiceTemplate();
        array_push($this->_page_boxes, $this->_obj);         

        $this->_obj = new CPMetaPageGalleryTemplate();
        array_push($this->_page_boxes, $this->_obj);                 
        
        $this->_obj = new CPMetaPageGalleryListTemplate();
        array_push($this->_page_boxes, $this->_obj);                         

        $this->_obj = new CPMetaPagePortfolioTemplate();
        array_push($this->_page_boxes, $this->_obj);  
        
        $this->_obj = new CPMetaPagePortfolioListTemplate();
        array_push($this->_page_boxes, $this->_obj);                  
        
        $this->_obj = new CPMetaPageMediaTemplate();
        array_push($this->_page_boxes, $this->_obj);        
        
        $this->_obj = new CPMetaPageArchiveListTemplate();
        array_push($this->_page_boxes, $this->_obj);  
        
        $this->_obj = new CPMetaPageTeamTemplate();
        array_push($this->_page_boxes, $this->_obj);         
               
        $this->_obj = new CPMetaPageQuestionTemplate();
        array_push($this->_page_boxes, $this->_obj);                 
                                                                                               
        
        # HOMEPAGE TABS META BOXES               
        $this->_obj = new CPMetaHomepageTabsOpt();
        array_push($this->_homepagetabs_boxes, $this->_obj);         
        
        # TOUR SLIDES META BOXES      
        $this->_obj = new CPMetaTourOpt();
        array_push($this->_tour_boxes, $this->_obj);                       
        
        # SERVICE ITEMS META BOXES             
        $this->_obj = new CPMetaServiceOpt();
        array_push($this->_service_boxes, $this->_obj);                 
        
        # TEAM MEMBER META BOXES
        $this->_obj = new CPMetaMemberOpt();
        array_push($this->_member_boxes, $this->_obj);              
        
        # QUESTION META BOXES
        $this->_obj = new CPMetaQuestionOpt();
        array_push($this->_question_boxes, $this->_obj);
        
        # STORY POST META
     //   $this->_obj = new CPMetaStoryPostOpt();
     //   array_push($this->_storypost_boxes, $this->_obj);              
        
        # REGISTER SAVE ACTIONS                                                                           
        add_action('admin_menu', array(&$this, 'adminMenu'));
        add_action('save_post', array(&$this, 'savePostData'));                          
        add_action('save_post', array(&$this, 'savePageData'));
        add_action('save_post', array(&$this, 'saveHomepageTabsData'));
        add_action('save_post', array(&$this, 'saveTourSlidesData'));
        add_action('save_post', array(&$this, 'saveServiceData'));
        add_action('save_post', array(&$this, 'saveMemberData'));
        add_action('save_post', array(&$this, 'saveQuestionData'));
     //   add_action('save_post', array(&$this, 'saveStoryPostData'));         
    } // constructor 

    /*********************************************************** 
    * Public members
    ************************************************************/      
    
    /*********************************************************** 
    * Private members
    ************************************************************/      
   
    private $_post_boxes = Array();
    private $_page_boxes = Array();
    private $_project_boxes = Array();
    private $_homepagetabs_boxes = Array();
    private $_tour_boxes = Array();  
    private $_service_boxes = Array();
    private $_member_boxes = Array(); 
    private $_question_boxes = Array();
  //  private $_storypost_boxes = Array();
   
    /*********************************************************** 
    * Public functions
    ************************************************************/                      
    public function adminMenu() 
    {
        add_meta_box( 'new-meta-boxes', 'Custom Post Settings', array(&$this, 'drawPostMetaBoxes'), 'post', 'normal', 'high' );    
        add_meta_box( 'new-meta-boxes', 'Custom Page Settings', array(&$this, 'drawPageMetaBoxes'), 'page', 'normal', 'high' );
        add_meta_box( 'new-meta-boxes', 'Custom Post Settings', array(&$this, 'drawHomepageTabsMetaBoxes'), CPThemeCustomPosts::PT_HOMEPAGE_TAB_POST, 'normal', 'high' );
        add_meta_box( 'new-meta-boxes', 'Custom Post Settings', array(&$this, 'drawTourSlidesMetaBoxes'), CPThemeCustomPosts::PT_TOUR_POST, 'normal', 'high' );
        add_meta_box( 'new-meta-boxes', 'Custom Post Settings', array(&$this, 'drawServiceMetaBoxes'), CPThemeCustomPosts::PT_SERVICE_POST, 'normal', 'high' );
        add_meta_box( 'new-meta-boxes', 'Custom Post Settings', array(&$this, 'drawMemberMetaBoxes'), CPThemeCustomPosts::PT_MEMBER_POST, 'normal', 'high' );
        add_meta_box( 'new-meta-boxes', 'Custom Post Settings', array(&$this, 'drawQuestionMetaBoxes'), CPThemeCustomPosts::PT_QUESTION_POST, 'normal', 'high' );
      //  add_meta_box( 'new-meta-boxes', 'Custom Post Settings', array(&$this, 'drawStoryPostMetaBoxes'), CPThemeCustomPosts::PT_STORY_POST, 'normal', 'high' );
          
    } // adminMenu     

  
    # POSTS
    public function drawPostMetaBoxes()
    {     
        // check number of meta boxes to draw
        $count = count($this->_post_boxes);
        // for every meta box
        for($i = 0; $i < $count; $i++)
        {
            $this->_post_boxes[$i]->display();                        
        } // for                    
    }     

    public function savePostData($postID)
    {     
        // check number of meta boxes to draw
        $count = count($this->_post_boxes);
        // for every meta box
        for($i = 0; $i < $count; $i++)
        {
            $this->_post_boxes[$i]->save($postID);                        
        } // for                    
    }
    
    # PAGES
    public function drawPageMetaBoxes()
    {     
        // check number of meta boxes to draw
        $count = count($this->_page_boxes);
        // for every meta box
        for($i = 0; $i < $count; $i++)
        {
            $this->_page_boxes[$i]->display();                        
        } // for                    
    }   

    public function savePageData($postID)
    {     
        // check number of meta boxes to draw
        $count = count($this->_page_boxes);
        // for every meta box
        for($i = 0; $i < $count; $i++)
        {
            $this->_page_boxes[$i]->save($postID);                        
        } // for                    
    }       

    # HOMEPAGE TABS
    public function drawHomepageTabsMetaBoxes()
    {     
        // check number of meta boxes to draw
        $count = count($this->_homepagetabs_boxes);
        // for every meta box
        for($i = 0; $i < $count; $i++)
        {
            $this->_homepagetabs_boxes[$i]->display();                        
        } // for                    
    }     

    public function saveHomepageTabsData($postID)
    {     
        // check number of meta boxes to draw
        $count = count($this->_homepagetabs_boxes);
        // for every meta box
        for($i = 0; $i < $count; $i++)
        {
            $this->_homepagetabs_boxes[$i]->save($postID);                        
        } // for                    
    } 
    
    # TOUR SLIDES
    public function drawTourSlidesMetaBoxes()
    {     
        // check number of meta boxes to draw
        $count = count($this->_tour_boxes);
        // for every meta box
        for($i = 0; $i < $count; $i++)
        {
            $this->_tour_boxes[$i]->display();                        
        } // for                    
    }     

    public function saveTourSlidesData($postID)
    {     
        // check number of meta boxes to draw
        $count = count($this->_tour_boxes);
        // for every meta box
        for($i = 0; $i < $count; $i++)
        {
            $this->_tour_boxes[$i]->save($postID);                        
        } // for                    
    }   
    
    # SERVICE ITEMS
    public function drawServiceMetaBoxes()
    {     
        // check number of meta boxes to draw
        $count = count($this->_service_boxes);
        // for every meta box
        for($i = 0; $i < $count; $i++)
        {
            $this->_service_boxes[$i]->display();                        
        } // for                    
    }     

    public function saveServiceData($postID)
    {     
        // check number of meta boxes to draw
        $count = count($this->_service_boxes);
        // for every meta box
        for($i = 0; $i < $count; $i++)
        {
            $this->_service_boxes[$i]->save($postID);                        
        } // for                    
    }       

    # MEMBER ITEMS
    public function drawMemberMetaBoxes()
    {     
        // check number of meta boxes to draw
        $count = count($this->_member_boxes);
        // for every meta box
        for($i = 0; $i < $count; $i++)
        {
            $this->_member_boxes[$i]->display();                        
        } // for                    
    }     

    public function saveMemberData($postID)
    {     
        // check number of meta boxes to draw
        $count = count($this->_member_boxes);
        // for every meta box
        for($i = 0; $i < $count; $i++)
        {
            $this->_member_boxes[$i]->save($postID);                        
        } // for                    
    }    

    # QUESTION ITEMS
    public function drawQuestionMetaBoxes()
    {     
        // check number of meta boxes to draw
        $count = count($this->_question_boxes);
        // for every meta box
        for($i = 0; $i < $count; $i++)
        {
            $this->_question_boxes[$i]->display();                        
        } // for                    
    }     

    public function saveQuestionData($postID)
    {     
        // check number of meta boxes to draw
        $count = count($this->_question_boxes);
        // for every meta box
        for($i = 0; $i < $count; $i++)
        {
            $this->_question_boxes[$i]->save($postID);                        
        } // for                    
    }    

    # STORY POST ITEMS
 /*   public function drawStoryPostMetaBoxes()
    {     
        // check number of meta boxes to draw
        $count = count($this->_storypost_boxes);
        // for every meta box
        for($i = 0; $i < $count; $i++)
        {
            $this->_storypost_boxes[$i]->display();                        
        } // for                    
    }     

    public function saveStoryPostData($postID)
    {     
        // check number of meta boxes to draw
        $count = count($this->_storypost_boxes);
        // for every meta box
        for($i = 0; $i < $count; $i++)
        {
            $this->_storypost_boxes[$i]->save($postID);                        
        } // for                    
    }   
 */ 
    /*********************************************************** 
    * Private functions
    ************************************************************/      
            
} 
        
        
?>