<?php
/**********************************************************************
* MODERN ELEGANCE WORDPRESS THEME 
* (Ideal For Business And Personal Use: Portfolio or Blog)     
* 
* File name:   
*      cp_metastorypost.php
* Brief:       
*      Part of theme control panel.
* Author:      
*      DigitalCavalry
* Author URI:
*      http://themeforest.net/user/DigitalCavalry
* Contact:
*      digitalcavalry@gmail.com 
***********************************************************************/

/*********************************************************** 
* Definitions
************************************************************/

/*********************************************************** 
* Class name:
*    CPMetaStoryPostOpt
* Descripton:
*    Implementation of CPMetaStoryPostOpt
***********************************************************/
class CPMetaStoryPostOpt extends DCC_MetaMultiple 
{            
    /*********************************************************** 
    * Constructor
    ************************************************************/
    public function __construct() 
    {
        $this->_names = array(
            'storypost_opt', 'storypost_opt_cbox', 
            'image_url', 'link_page', 'link', 'link_use_cbox', 'link_type', 'link_target');   
                       
        $this->_std = array(
            'storypost_opt_cbox' => false,
            'image_url' => '',
            'link_page' => CMS_NOT_SELECTED,
            'link' => '',
            'link_use_cbox' => false,
            'link_type' => CMS_LINK_TYPE_PAGE,
            'link_target' => CMS_LINK_TARGET_SELF
            );
                             
        $this->_title = 'STORY SLIDE POST';
        $this->_type = 'storyslide';
        $this->_desc = '';
    } // constructor 

    /*********************************************************** 
    * Public functions
    ************************************************************/    
    public function display()
    {       
        $value = $this->initDisplay();            
                
        global $post;
        echo '<span class="cms-meta-normal">POST INFORMATION: ID='.$post->ID.'</span>';         

        // title
        $out = ''; 
        $out .= '<div class="cms-custom-field-panel ">';
        $out .= '<div class="cms-meta-bold">'.$this->_title.'</div><br />';                       
        $out .= '<input type="hidden" name="storypost_opt_cbox" checked="checked" />';        
        
        // image url
        $out .= '<span class="cms-meta-normal">Image URL or image ID from NextGen Gallery</span><br />';   
            $path = dcf_isNGGImageID($value['image_url']);
            if($path != '')
            {
                $out .= '<img style="display:block;margin:5px 0px 5px 0px;" src="'.$path.'"/>';
            }
                                   
        $out .= '<input style="width:600px;" type="text" id="'.'image_url'.'_path" name="'.'image_url'.'" value="'.$value['image_url'].'" />'; 
        $out .= '<input style="width:140px;" class="cms-upload upload_image_button" type="button" value="Upload Image" name="'.'image_url'.'_path" /><br /><br />';                                           
        
        // page
        $out .= '<span class="cms-meta-normal">Link page</span><br />'; 
        $out .= $this->selectCtrlPagesList($value['link_page'], 'link_page', 300);
        $out .= '<br /><br />'; 
        
        // manually link        
        $out .= '<span class="cms-meta-normal">Manually link</span><br />';     
        $out .= '<input style="width:680px;" type="text" name="link" value="'.$value['link'].'" /><br /><br />';                
        
        // link type
        $out .= '<span class="cms-meta-normal">Link type:</span><br />';     
        $out .= '<input type="radio" name="link_type" '.$this->attrChecked($value['link_type'] == CMS_LINK_TYPE_PAGE).' value="'.CMS_LINK_TYPE_PAGE.'" /> Use page<br />';    
        $out .= '<input type="radio" name="link_type" '.$this->attrChecked($value['link_type'] == CMS_LINK_TYPE_MANUALLY).' value="'.CMS_LINK_TYPE_MANUALLY.'" /> Use manually link<br /><br />'; 

        // link target
        $out .= '<span class="cms-meta-normal">Link target:</span><br />';     
        $out .= '<input type="radio" name="link_target" '.$this->attrChecked($value['link_target'] == CMS_LINK_TARGET_SELF).' value="'.CMS_LINK_TARGET_SELF.'" /> Open link in the same window/tab<br />';    
        $out .= '<input type="radio" name="link_target" '.$this->attrChecked($value['link_target'] == CMS_LINK_TARGET_BLANK).' value="'.CMS_LINK_TARGET_BLANK.'" /> Open link in new window/tab<br /><br />'; 
      
        // other settings
        $out .= '<span class="cms-meta-normal">Other options:</span><br />';
        $out .= '<input type="checkbox" name="link_use_cbox" '.$this->attrChecked($value['link_use_cbox']).' /> Use link<br />';
        $out .= '<br />';       
        
        $out .= $this->getUpdateBtnHtmlCode();
        
        $out .= '</div>';
        echo $out;
    } 
} 
        
?>