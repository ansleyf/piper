<?php
/**********************************************************************
* MODERN ELEGANCE WORDPRESS EDITION 
* (Ideal For Business And Personal Use: Portfolio or Blog)     
* 
* File name:   
*      cp_clientpanel.php
* Brief:       
*      Part of theme control panel.
* Author:      
*      DigitalCavalry
* Author URI:
*      http://themeforest.net/user/DigitalCavalry
* Contact:
*      digitalcavalry@gmail.com 
***********************************************************************/

/*********************************************************** 
* Definitions
************************************************************/

/*********************************************************** 
* Class name:
*    CPClientPanel
* Descripton:
*    Implementation of CPClientPanel 
***********************************************************/
class CPClientPanel extends DCC_CPBaseClass
{   

    /*********************************************************** 
    * Constructor
    ************************************************************/
    public function __construct() 
    {
        if(isset($_COOKIE['dcis_slider_type']))
        {
            $this->_settings['set'] = true;
            $this->_settings['theme_skin'] = $_COOKIE['dcis_theme_skin'];
            $this->_settings['slider_type'] = $_COOKIE['dcis_slider_type'];
            $this->_settings['font_google'] = $_COOKIE['dcis_font_google'];
            $this->_settings['font_cufon'] = $_COOKIE['dcis_font_cufon'];
            $this->_settings['font_type'] = $_COOKIE['dcis_font_type'];
            $this->_settings['main_color'] = $_COOKIE['dcis_main_color'];
        }
    } // constructor 

    /*********************************************************** 
    * Public members
    ************************************************************/      
    
    public $_settings = Array(
      'set' => false,
      'theme_skin' => 'White', 
      'slider_type' => CPClientPanel::ACCORDION_SLIDER,
      'font_google' => 0,
      'font_cufon' => 0,
      'font_type' => CMS_FONT_TYPE_CUFON,
      'main_color' => '#FFA319'
    );
    
    /*********************************************************** 
    * Private members
    ************************************************************/      
  
     const NO_SLIDER = -1; 
     const ACCORDION_SLIDER = 1;
     const PROGRESS_SLIDER = 3;
     const CHAIN_SLIDER = 4;
     private $_sliderType = Array(
        'No slider' => CPClientPanel::NO_SLIDER,
        'Accordion' => CPClientPanel::ACCORDION_SLIDER,
        'Progress' => CPClientPanel::PROGRESS_SLIDER,
        'Chain' => CPClientPanel::CHAIN_SLIDER
     );  
     
     const THEME_SKIN_WHITE = 'White';
     const THEME_SKIN_GREY = 'Grey';
     const THEME_SKIN_BLACK = 'Black';      
     private $_skins = Array(CPClientPanel::THEME_SKIN_WHITE, CPClientPanel::THEME_SKIN_GREY, CPClientPanel::THEME_SKIN_BLACK);
  
    /*********************************************************** 
    * Public functions
    ************************************************************/                     
    
    public function getValue($key)
    {
        $value = null;
        if($this->_settings['set'])
        {
            $value = $this->_settings[$key];    
        }        
        return $value;        
    }            
 
    public function process()
    {
        if(isset($_POST['apply_client_panel']))
        {
            $this->_settings['set'] = true;          

            $this->_settings['theme_skin'] = $_POST['dcis_theme_skin'];
            setcookie('dcis_theme_skin', $_POST['dcis_theme_skin'], 0, '/');              
            $_COOKIE['dcis_theme_skin'] = $_POST['dcis_theme_skin'];
            
            $this->_settings['slider_type'] = $_POST['dcis_slider_type'];
            setcookie('dcis_slider_type', $_POST['dcis_slider_type'], 0, '/');
            $_COOKIE['dcis_slider_type'] = $_POST['dcis_slider_type']; 
            
            $this->_settings['font_google'] = $_POST['dcis_font_google'];
            setcookie('dcis_font_google', $_POST['dcis_font_google'], 0, '/');     
            $_COOKIE['dcis_font_google'] = $_POST['dcis_font_google'];                    

            $this->_settings['font_cufon'] = $_POST['dcis_font_cufon'];
            setcookie('dcis_font_cufon', $_POST['dcis_font_cufon'], 0, '/');     
            $_COOKIE['dcis_font_cufon'] = $_POST['dcis_font_cufon']; 

            $this->_settings['font_type'] = $_POST['dcis_font_type'];
            setcookie('dcis_font_type', $_POST['dcis_font_type'], 0, '/');     
            $_COOKIE['dcis_font_type'] = $_POST['dcis_font_type']; 
            
            $this->_settings['main_color'] = $_POST['dcis_main_color'];
            setcookie('dcis_main_color', $_POST['dcis_main_color'], 0, '/');
            $_COOKIE['dcis_main_color'] = $_POST['dcis_main_color'];   
                                                                          
        }   
    }

    public function renderPanel()
    {

         $out = '';
         
         $out .= '<div id="client-panel-open-btn"></div>'; 
         $out .= '<div id="client-panel">';
         
           $out .= '<div class="close-btn"></div>';
         
            $out .= '<form action="#" method="post">';
            
            $skin = null;
            if(isset($_COOKIE['dcis_theme_skin']))
            {
                $skin = $this->_settings['theme_skin'];   
            } else
            {
                $skin = GetDCCPInterface()->getIGeneral()->getThemeSkin();    
            }           
            // theme skin
      /*      $out .= '<strong>Theme skin:</strong> (Here you can choose theme skin)';            
            $out .= '<select name="dcis_theme_skin">';
                foreach($this->_skins as $sname)
                {
                    $out .= '<option ';
                    $out .= ' value="'.$sname.'" ';
                    if($skin == $sname)
                    {
                        $out .= ' selected="selected" ';
                    }
                    $out .= ' >'.$sname;
                    $out .= '</option>';
                }
            $out .= '</select>';
        */            
            
            // slider type
            $slider = null;
            if(isset($_COOKIE['dcis_slider_type']))
            {
                $slider = $this->_settings['slider_type'];   
            } else
            {                
                $slider = GetDCCPInterface()->getIGeneral()->getSliderType();                    
            } 
            $out .= '<strong>Homepage slider type:</strong> (Here you can choose one of 3 sliders)';
            $out .= '<select name="dcis_slider_type">';
                foreach($this->_sliderType as $key => $value)
                {
                    $out .= '<option ';
                    $out .= ' value="'.$value.'" ';
                    if($slider == $value)
                    {
                        $out .= ' selected="selected" ';
                    }
                    $out .= ' >'.$key;
                    $out .= '</option>';
                }            
            $out .= '</select>';              
                     
             // font google
            $fonts = GetDCCPInterface()->getAllGoogleFonts();
            $count = count($fonts);           
            
            $selected_font = 0;
            if(isset($_COOKIE['dcis_font_google']))
            {                 
                $selected_font = $this->_settings['font_google'];
            } else { $selected_font = (int)GetDCCPInterface()->getIGeneral()->getThemeGoogleFontIndex(); }                

            $out .= '<strong>Select google font:</strong> (Here you can choose google font)';
         
             $out .= '<select name="dcis_font_google">';         
              for($i = 0; $i < $count; $i++)
              {
                    if($selected_font == $i)
                    {
                        $out .= '<option value="'.$i.'" selected="selected">'.$fonts[$i]->_name.'</option>';    
                    } else
                    {
                        $out .= '<option value="'.$i.'">'.$fonts[$i]->_name.'</option>';
                    }                  
              }            
             $out .= '</select>'; 
            
            // font cufon
            $fonts = GetDCCPInterface()->getAllCufonFonts();
            $count = count($fonts);           
            
            $selected_font = 0;
            if(isset($_COOKIE['dcis_font_cufon']))
            {                 
                $selected_font = $this->_settings['font_cufon'];
            } else { $selected_font = (int)GetDCCPInterface()->getIGeneral()->getThemeCufonFontIndex(); }                

            $out .= '<strong>Select cufon font:</strong> (Here you can choose cufon font)';
         
             $out .= '<select name="dcis_font_cufon">';         
              for($i = 0; $i < $count; $i++)
              {
                    if($selected_font == $i)
                    {
                        $out .= '<option value="'.$i.'" selected="selected">'.$fonts[$i]->_name.'</option>';    
                    } else
                    {
                        $out .= '<option value="'.$i.'">'.$fonts[$i]->_name.'</option>';
                    }                  
              }            
             $out .= '</select>';             
            
            // font type
            $font_type = null;  
            if(isset($_COOKIE['dcis_font_type']))
            {                 
                $font_type = $this->_settings['font_type'];
            } else { $font_type = GetDCCPInterface()->getIGeneral()->getThemeFontType(); }                
            $out .= '<strong>Select font type:</strong><br />';
                                                                                     
             $out .= '<input type="radio" name="dcis_font_type" value="'.CMS_FONT_TYPE_CUFON.'" '.$this->attrChecked($font_type == CMS_FONT_TYPE_CUFON).' /> Cufon font<br />';         
             $out .= '<input type="radio" name="dcis_font_type" value="'.CMS_FONT_TYPE_GOOGLE.'" '.$this->attrChecked($font_type == CMS_FONT_TYPE_GOOGLE).' /> Google font<br />';              
             $out .= '<br />';
            
            // main color
            $main_color = null;
            if($_COOKIE['dcis_main_color'])
            {
                 $main_color = $this->_settings['main_color'];
            } else { $main_color = GetDCCPInterface()->getIGeneral()->getThemeColor(); }              
             
             $out .= '<strong>Theme main color:</strong> (Here you can change main color. In CMS you can easy change also colors for main menu.)<br />';  
             $out .= '<input style="width:70px;text-align:center;margin-top:8px;" type="text" class="colorpicker {hash:true}" value="'.$main_color.'" name="dcis_main_color" /> Main color<br />';              
                  
            $out .= '<div class="bottom-block"></div><input type="submit" class="submit-btn" value="Apply Changes" name="apply_client_panel" />';
            $out .= '</form>';            
            
         $out .= '</div>'; 
         echo $out;               
    }
 
    /*********************************************************** 
    * Private functions
    ************************************************************/      
    

            
} // class
        
        
?>