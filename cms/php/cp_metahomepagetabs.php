<?php
/**********************************************************************
* MODERN ELEGANCE WORDPRESS THEME 
* (Ideal For Business And Personal Use: Portfolio or Blog)     
* 
* File name:   
*      cp_metahomepagetabs.php
* Brief:       
*      Part of theme control panel.
* Author:      
*      DigitalCavalry
* Author URI:
*      http://themeforest.net/user/DigitalCavalry
* Contact:
*      digitalcavalry@gmail.com 
***********************************************************************/

/*********************************************************** 
* Definitions
************************************************************/

/*********************************************************** 
* Class name:
*    CPMetaHomepageTabsOpt
* Descripton:
*    Implementation of CPMetaHomepageTabsOpt
***********************************************************/
class CPMetaHomepageTabsOpt extends DCC_MetaMultiple 
{        
    /*********************************************************** 
    * Constructor
    ************************************************************/
    public function __construct() 
    {
        $this->_names = array(
            'hometab_opt', 
            'hometab_opt_cbox', 
            'link', 
            'linkpage', 
            'linkuse_cbox', 
            'linktarget', 
            'linktype', 
            'not_read_more_cbox', 
           // 'hide_thumbs_cbox', 
           // 'image_height',
           // 'thumbs_desc_cbox', 
           // 'thumbs_title_cbox', 
            'hide_title_cbox', 
            'hide_date_cbox', 
           // 'thumbs_disable_resize_cbox',
           // 'disable_gallery_cbox', 
            'btn_name', 
           // 'images_id', 
           // 'image_url'
            );              
        $this->_std = array(
            'hometab_opt_cbox' => true,
            'link' => '', 
            'linkpage' => CMS_NOT_SELECTED, 
            'linkuse_cbox' => false, 
            'linktarget' => DCC_MetaMultiple::LINK_SELF, 
            'linktype' => DCC_MetaMultiple::LINK_MANUALLY,
            'hide_title_cbox' => false,
            'hide_date_cbox' => false,
            'not_read_more_cbox' => false,
           // 'hide_thumbs_cbox' => false,
           // 'image_height' => 120,
           // 'thumbs_desc_cbox' => true,
           // 'thumbs_title_cbox' => false,
           // 'thumbs_disable_resize_cbox' => false,
           // 'disable_gallery_cbox' => false,
            'btn_name' => 'Btn Name',
           // 'images_id' => '',
           // 'image_url' => ''
            );
            
        $this->_title = '[HOMEPAGE TAB] Tab settings:';
        $this->_type = 'homepagetabs';
        $this->_desc = '';
    } // constructor 

    /*********************************************************** 
    * Public functions
    ************************************************************/    
    public function display()
    {       
        $value = $this->initDisplay();            
        
        global $post;
        echo '<span class="cms-meta-normal">POST INFORMATION: ID='.$post->ID.'</span>';        
        
        $out = '';
        // title
        $out .= '<div class="cms-custom-field-panel ">'; 
            $out .= '<div class="cms-meta-bold">'.$this->_title.'</div><br />';                       
            // box value
            $out .= '<input type="hidden" name="hometab_opt_cbox" checked="checked" />'; 
            
            /*
            $out .= '<span class="cms-meta-normal">Tab main image URL (you can use image with any size, but recomended size is 180x120 px or image with similar aspect ratio, you can also set image ID from NextGEN Gallery)</span><br />'; 
            
            $path = dcf_isNGGImageID($value['image_url']);
            if($path != '')
            {
                $out .= '<img style="display:block;" src="'.$path.'"/>';
            }
                                   
            
            $out .= '<input style="width:540px;" type="text" id="'.'image_url'.'_path" name="'.'image_url'.'" value="'.$value['image_url'].'" />'; 
            $out .= '<input style="width:140px;" class="cms-upload upload_image_button" type="button" value="Upload Image" name="'.'image_url'.'_path" /><br /><br />';              
            
            $out .= '<span class="cms-meta-normal">Mini Gallery: Images ID - type here comma separated images ID\'s from NextGEN Gallery (e.g 4,6,76,3,17):</span><br />';
            $out .= '<input type="text" style="text-align:left;width:400px;" name="images_id" value="'.$value['images_id'].'" /><br /><br />';    
            */
            
            $out .= '<span class="cms-meta-normal">Type here name for tab button:</span><br />';
            $out .= '<input type="text" style="text-align:left;width:400px;" name="btn_name" value="'.$value['btn_name'].'" /><br /><br />';            
            
            $out .= '<span class="cms-meta-normal">Manually link:</span><br />';
            $out .= '<input style="width:680px;" type="text" id="link" name="link" value="'.$value['link'].'" /><br /><br />';
            $out .= '<span class="cms-meta-normal">Page link:</span><br />';
            $out .= $this->selectCtrlPagesList($value['linkpage'], 'linkpage', 680);                          
            $out .= '<br /><br />';
            $out .= '<span class="cms-meta-normal">Link target:</span><br />';
            $out .= '<input type="radio" name="linktarget" '.$this->attrChecked($value['linktarget'] == DCC_MetaMultiple::LINK_SELF).' value="'.DCC_MetaMultiple::LINK_SELF.'" /> Self<br />';
            $out .= '<input type="radio" name="linktarget" '.$this->attrChecked($value['linktarget'] == DCC_MetaMultiple::LINK_BLANK).' value="'.DCC_MetaMultiple::LINK_BLANK.'" /> Blank<br /><br />';
            $out .= '<span class="cms-meta-normal">Link type:</span><br />';
            $out .= '<input type="radio" name="linktype" '.$this->attrChecked($value['linktype'] == DCC_MetaMultiple::LINK_PAGE).' value="'.DCC_MetaMultiple::LINK_PAGE.'" /> Use selected page<br />';
            $out .= '<input type="radio" name="linktype" '.$this->attrChecked($value['linktype'] == DCC_MetaMultiple::LINK_MANUALLY).' value="'.DCC_MetaMultiple::LINK_MANUALLY.'" /> Use manually link<br /><br />';
            
            $out .= '<span class="cms-meta-normal">Display link:</span><br />'; 
            $out .= '<input type="checkbox" name="linkuse_cbox" '.$this->attrChecked($value['linkuse_cbox']).' /> Show link<br />';
            $out .= '<input type="checkbox" name="not_read_more_cbox" '.$this->attrChecked($value['not_read_more_cbox']).' /> Hide read more link, use link only for title and image<br /><br />'; 
            
            $out .= '<span class="cms-meta-normal">Other options:</span><br />'; 
            $out .= '<input type="checkbox" name="hide_title_cbox" '.$this->attrChecked($value['hide_title_cbox']).' /> Hide tab title<br />';
            $out .= '<input type="checkbox" name="hide_date_cbox" '.$this->attrChecked($value['hide_date_cbox']).' /> Hide tab date<br /><br />';
            
            /*
            $out .= '<span class="cms-meta-normal">NGG Images thumbs:</span><br />'; 
            $out .= '<input type="checkbox" name="hide_thumbs_cbox" '.$this->attrChecked($value['hide_thumbs_cbox']).' /> Hide image thumbs<br />';
            $out .= '<input type="checkbox" name="thumbs_desc_cbox" '.$this->attrChecked($value['thumbs_desc_cbox']).' /> Show thumbs image description<br />';
            $out .= '<input type="checkbox" name="thumbs_title_cbox" '.$this->attrChecked($value['thumbs_title_cbox']).' /> Show thumbs image title<br />';
            $out .= '<input type="checkbox" name="thumbs_disable_resize_cbox" '.$this->attrChecked($value['thumbs_disable_resize_cbox']).' /> Disable image auto resizing for thumbs<br />';
            $out .= '<input type="checkbox" name="disable_gallery_cbox" '.$this->attrChecked($value['disable_gallery_cbox']).' /> Disable gallery view<br /><br />';
            */
            
            /*
            $out .= '<span class="cms-meta-normal">Main image height (you can use this filed to change image height):</span><br />';
            $out .= '<input type="text" style="text-align:center;width:60px;" name="image_height" value="'.$value['image_height'].'" /><br /><br />';
            */
            
            $out .= $this->getUpdateBtnHtmlCode(); 
            
        $out .= '</div>';
        echo $out;
    } 
} // class CPMetaHomepageTabsOpt   
        
?>