<?php
/**********************************************************************
* MODERN ELEGANCE WORDPRESS THEME 
* (Ideal For Business And Personal Use: Portfolio or Blog)     
* 
* File name:   
*      cp_general.php
* Brief:       
*      Part of theme control panel.
* Author:      
*      DigitalCavalry
* Author URI:
*      http://themeforest.net/user/DigitalCavalry
* Contact:
*      digitalcavalry@gmail.com 
***********************************************************************/

/*********************************************************** 
* Definitions
************************************************************/

/*********************************************************** 
* Class name:
*    CPSidebar
* Descripton:
*    Implementation of single sidebar object    
***********************************************************/
class CPSidebar
{
    /*********************************************************** 
    * Constructor
    ************************************************************/
    public function __construct() 
    {
        $this->_name = '';
        $this->_id = time();        
    } // constructor
    
    /*********************************************************** 
    * Public memebers
    ************************************************************/    
    public $_name;
    public $_id;    
}


/*********************************************************** 
* Class name:
*    CPAddMediaBtn
* Descripton:
*    Implementation of single add media btn object    
***********************************************************/
class CPAddMediaBtn
{
    /*********************************************************** 
    * Constructor
    ************************************************************/
    public function __construct(
        $name='', $start='', $end='', $icon='', $hide=false) 
    {
        $this->_name = $name;
        $this->_start = $start;
        $this->_end = $end;
        if($icon != '') { $this->_icon = $icon; } else { $this->_icon = get_bloginfo('template_url').'/cms/img/panel.png'; }
        $this->_hide = $hide;        
    } // constructor
    
    /*********************************************************** 
    * Public memebers
    ************************************************************/    
    public $_name;
    public $_start;
    public $_end;
    public $_icon; 
    public $_hide;   
}

/*********************************************************** 
* Class name:
*    CPHeaderIcon
* Descripton:
*    Implementation of single CPHeaderIcon object    
***********************************************************/
class CPHeaderIcon
{
    /*********************************************************** 
    * Constructor
    ************************************************************/
    public function __construct($image, $hide, $url, $desc, $hovered) 
    {
        $this->_image = $image;
        $this->_hide = $hide;
        $this->_url = $url;
        $this->_desc = $desc;     
        $this->_hovered = $hovered;   
    } // constructor
    
    /*********************************************************** 
    * Public memebers
    ************************************************************/    
    public $_image;
    public $_hide;
    public $_url;  
    public $_desc; 
    public $_hovered; 
}



/*********************************************************** 
* Class name:
*    CPGeneral
* Descripton:
*    Implementation of CPGeneral 
***********************************************************/
class CPGeneral extends DCC_CPBaseClass
{
    const CMS_THEME_BG_REPEAT = 1;
    const CMS_THEME_BG_REPEATX = 2;
    const CMS_THEME_BG_REPEATY = 1;
    const CMS_THEME_BG_NOREPEAT = 1;  
    
    const SPANEL_PLACE_ABOVE_MENU = 1;
    const SPANEL_PLACE_UNDER_MENU = 2;   
         
    /*********************************************************** 
    * Constructor
    ************************************************************/
    public function __construct() 
    {              
        $this->_DBIDOPT_GENERAL = CMS_THEME_NAME_UPPERCASE.'_GENERAL_OPT';
        $this->_DBIDOPT_SIDEBARS = CMS_THEME_NAME_UPPERCASE.'_GENERAL_SIDEBARS_OPT'; 
        $this->_DBIDOPT_HEADER_ICONS = CMS_THEME_NAME_UPPERCASE.'_HEADER_ICONS_OPT';              
        $this->setDefaults(); 
        
        // header icons
        $this->_icons = get_option($this->_DBIDOPT_HEADER_ICONS);
        if (!is_array($this->_icons))
        {
            add_option($this->_DBIDOPT_HEADER_ICONS, Array());
            $this->_icons = get_option($this->_DBIDOPT_HEADER_ICONS);
        }
        
        // general options        
        $this->_general = get_option($this->_DBIDOPT_GENERAL);
        if (!is_array($this->_general))
        {            
            add_option($this->_DBIDOPT_GENERAL, $this->_generalDef);
            $this->_general = get_option($this->_DBIDOPT_GENERAL);
        }     
    
        // sidebar options
        $this->_sidebars = get_option($this->_DBIDOPT_SIDEBARS);
        if (!is_array($this->_sidebars))
        {
            add_option($this->_DBIDOPT_SIDEBARS, Array());
            $this->_sidebars = get_option($this->_DBIDOPT_SIDEBARS);
        }               
    } // constructor 

    /*********************************************************** 
    * Public members
    ************************************************************/      
    
    /*********************************************************** 
    * Private members
    ************************************************************/           
     private $_DBIDOPT_GENERAL = null; // data base id options
     private $_DBIDOPT_SIDEBARS = null; 
     private $_DBIDOPT_HEADER_ICONS = null;  
     
     private $_general = Array();
     private $_sidebars = Array();
     private $_icons = Array();
     private $_saved = false;   
     
     const NO_SLIDER = -1; 
     const ACCORDION_SLIDER = 1;
     const PROGRESS_SLIDER = 3;
     const CHAIN_SLIDER = 4; 
     private $_sliderType = Array(
        'No slider' => CPGeneral::NO_SLIDER,
        'Accordion' => CPGeneral::ACCORDION_SLIDER,
        'Progress' => CPGeneral::PROGRESS_SLIDER,
        'Chain' => CPGeneral::CHAIN_SLIDER
     );

     const THEME_SKIN_WHITE = 'White';
     const THEME_SKIN_GREY = 'Grey';
     const THEME_SKIN_BLACK = 'Black';      
     private $_skins = Array(CPGeneral::THEME_SKIN_WHITE, CPGeneral::THEME_SKIN_GREY, CPGeneral::THEME_SKIN_BLACK);
     
     private $_generalDef = Array(
     
        // theme core settings
        'theme_skin' => 'White',
        'theme_slider' => CPGeneral::ACCORDION_SLIDER, 
        'theme_font_google' => 18,
        'theme_font_cufon' => 0,
        'theme_font_type' => CMS_FONT_TYPE_CUFON,         
        'theme_color' => 'BD9755',
        'theme_lightbox_mode' => 'light_melegance',
        'theme_lightbox_overlay_gallery' => false,
        'theme_top_space_height' => 0,
        'theme_bottom_space_height' => 30,
        'theme_show_client_panel' => false,
        'theme_disable_autoformating' => false,
        'theme_disable_autotexturize' => false,
        'theme_authorize_comment' => false,
        'theme_authorize_contact' => false,
           
        'theme_favicon_use' => false,
        'theme_favicon_url' => '',
        
        'theme_bg_url' => '',
        'theme_bg_repeat' => 'no-repeat',
        'theme_bg_pos_x' => 'center',
        'theme_bg_pos_y' => 'top',
        'theme_bg_attachment' => 'scroll',
        'theme_bg_force' => false,
        'theme_bg_use' => false,
        
        'theme_bg_color' => 'CCC0A5',
        'theme_bg_color_use' => true,
        'theme_bg_color_force' => false,
        
        'theme_tip_bgcolor' => 'FAFAFA',
        'theme_tip_color' => '666666',
        'theme_tip_bordercolor' => 'FAFAFA',
          
        // blog
        'blog_show_tags_for_medium' => true,
        'blog_show_tags_for_medium_home' => true,
        'blog_show_time_for_medium' => true,
        'blog_show_time_for_small' => true,
        'blog_show_preview_for_small' => true,
        'blog_show_preview_for_medium' => false,   
        
        'blog_show_author_info' => true,
        'blog_show_related' => true,
        'blog_related_rows' => 1,
           
        // splash screen
        'home_ss_show' => false,
        'home_ss_show_bg' => true,
        'home_ss_width' => 800,
        'home_ss_height' => 600,
        'home_ss_top' => 100,
        'home_ss_border' => '888888',
        'home_ss_btns' => array(),
        'home_ss_show_btns' => true,      
        'home_ss_image' => '',
        'home_ss_link' => null,
        'home_ss_page' => CMS_NOT_SELECTED,
        'home_ss_use_page' => false,
        'home_ss_sticked' => false,  
                      
        // header
        'header_height' => 180,
        
        // header menu
        'hmenu_show' => false,
        'hmenu_bg_show' => false,
        'hmenu_bg_color' => '000000',
        'hmenu_bg_opacity' => 70, // from 0 to 100
        'hmenu_color' => '666666',
        'hmenu_hover_color' => '222222',
        
        // header icons
        'hicons_show' => true,
        'hicons_width' => 340, // header icons cointainer width
        'hicons_show_tip' => true,
        'hicons_size' => 36,
        'hicons_left' => 0,
        'hicons_right' => 0,
        'hicons_top' => 10,
        'hicons_bottom' => 20,
        'hicons_use_right' => true,
        'hicons_use_bottom' => false,
        
        // breadcrumb
        'breadcrumb_before' => 'You are here:',
        'breadcrumb_force_name' => false,
        'breadcrumb_name' => 'Alternative page name',
        
        // tracking code         
        'tcode_footer' => 'Put here your tracking code, e.g. from Google Analytics which will be printend in the footer area.',
        'tcode_footer_show' => false,
        'tcode_header' => 'Put here your tracking code, e.g. from Google Analytics which will be printend at the end of header.',
        'tcode_header_show' => false,
        
        // footer
        'footer_show_links' => true,
        'footer_show_copy' => true,
        'footer_copy' => '&copy; Modern Elegance WordPress Theme by Digital Cavalry (Copyright your company)',
        'footer_copy_align' => 'left',
        'footer_logo_path' => '',
        'footer_show_logo' => true,
        'footer_show_widgetized' => false,
        'footer_links_content' => '',
          
        'footer_sid_a' => CMS_NOT_SELECTED,
        'footer_sid_b' => CMS_NOT_SELECTED,
        'footer_sid_c' => CMS_NOT_SELECTED,
        'footer_sid_d' => CMS_NOT_SELECTED,
        'footer_sid_e' => CMS_NOT_SELECTED,
        'footer_sid_f' => CMS_NOT_SELECTED,
        
        'footer_sid_a_width' => 265,
        'footer_sid_b_width' => 265,
        'footer_sid_c_width' => 265,
        'footer_sid_d_width' => 265,
        'footer_sid_e_width' => 265,
        'footer_sid_f_width' => 265,
        
        'footer_sid_a_show' => false,
        'footer_sid_b_show' => false,
        'footer_sid_c_show' => false,
        'footer_sid_d_show' => false,
        'footer_sid_e_show' => false,
        'footer_sid_f_show' => false,                
        
        'footer_sid_separator_width' => 20,
        
        // logo
        'logo_width' => 442,
        'logo_height' => 136,
        'logo_path' => '',
        'logo_left' => 20,
        'logo_top' => 32,
        'logo_bottom' => 0,
        'logo_right' => 0,
        'logo_useright' => false,
        'logo_usebottom' => false,        
        
        // social communities
        'social_show_facebook' => false,
        'social_show_twitter' => false,
        'social_show_google' => false,
        'social_show_facebook_thumb' => false,
        'social_thumb' => '',
        'social_use_thumb' => false,         
        
        // sidebar  
        'sidebar_default' => CMS_NOT_SELECTED,
        'sidebar_global_pos' => CMS_SIDEBAR_RIGHT,
        'sidebar_blog_category' => CMS_NOT_SELECTED,  
        'sidebar_team_member' => CMS_NOT_SELECTED, 
        'sidebar_single' => CMS_NOT_SELECTED, 
        
        // search
        'search_no_results' => 'Sorry, no posts matched your criteria.',
        'search_before_fragment' => 'Search results for:',
        'search_page_size' => 20,
        'search_sidebar' => CMS_NOT_SELECTED,
        'search_in_posts' => true,
        'search_in_pages' => true,
        'search_show_form' => true,
        
        'search_header_panel_show' => true,
        'search_header_panel_right' => 0,
        'search_header_panel_left' => 0,
        'search_header_panel_top' => 0,
        'search_header_panel_bottom' => 0,
        'search_header_panel_use_right' => true,
        'search_header_panel_use_bottom' => false,
        'search_header_panel_place' => CPGeneral::SPANEL_PLACE_UNDER_MENU, 
        
        // announcement-bar 
        'annbar_show' => false,
        'annbar_transparent' => true,
        'annbar_height' => 32,
        'annbar_content' => '',
        'annbar_padding_left' => 0,
        'annbar_padding_right' => 0,
        'annbar_padding_bottom' => 0,
        'annbar_padding_top' => 0,
        'annbar_font_size' => 11,
        'annbar_font' => 'Helvetica',
        'annbar_font_color' => 'FFFFFF',
        'annbar_line_height' => 14,
        'annbar_bg_image' => '',
        'annbar_bg_use' => false,
        'annbar_bg_color' => '000000',
        'annbar_border_color' => '888888',
        'annbar_border_show' => true, 
        
        // media buttons
        'mbtns_show' => false,
        'mbtns_list' => array(),
        
        // 404 page
        '404_title' => 'Page not found - 404 Error',
        '404_text' => 'The page you are looking for doesn\'t exist.'            
     );    
   
     private $_panelsubmenu = array (
        'theme-global' => 'Theme global core settings ',
        'splash-screen' => 'Theme splash screen',
        'annbar' => 'Announcement bar',
        'media-buttons' => 'Programmable media buttons',
        'social' => 'Social communities',
        'breadcrumb' => 'Breadcrumb navigation settings', 
        'header' => 'Header settings',
        'header-menu' => 'Header menu',             
        'header-icons' => 'Header icons settings',
        'blog' => 'Blog and blog posts settings',
        '404page' => '404 page',
        'logo' => 'Logo settings',
        'sidebars' => 'Sidebars',            
        'search' => 'Searching',
        'trackingcode' => 'Footer and header tracking code',
        'footer' => 'Footer'         
     );
   
    /*********************************************************** 
    * Public functions
    ************************************************************/                
    public function renderTab()
    {
        echo '<div class="cms-content-wrapper">';
        echo '<a name="gototop"></a>';   
        $this->process();
        $this->renderPanelSubmenu($this->_panelsubmenu);                
        $this->renderCMS();
        
        echo '</div>';
    }
   
    public function customCSS()
    {
        echo '<style type="text/css">';

            $out = '';
            
            $theme_color = $this->_general['theme_color'];
            if($this->showClientPanel())
            {
                $client_theme_color = GetDCCPInterface()->getIClientPanel()->getValue('main_color');
                if($client_theme_color !== null)
                {
                    $theme_color = $client_theme_color;    
                }
            }
            // theme main color
            $out .= 'h1, h2, h3, h4, h5, h6, a, .blog-post-author .content .name, .section-text-head, blockquote span.author, 
            .table-gallery .thumb192 .bottom-content .title, .table-gallery .thumb320 .bottom-content .title, .dc-story .slides-wrapper .description .title  { ';
                $out .= 'color:'.$theme_color.';';
            $out .= '} ';
            
            // homepage tabs color
            if(is_page_template('homepage.php'))
            {
                $color = GetDCCPInterface()->getIHome()->getTabsBtnColor();
                $out .= '#homepage-tabs .btn-bar .btn:hover, #homepage-tabs .btn-bar .btn-selected { ';
                    $out .= 'background-color:'.$color.';';
                $out .= '} ';
                
                $out .= '#homepage-tabs .btn-bar .btn { ';
                    $out .= 'border-top-color:'.$color.';';
                $out .= '} ';                
            }
                        
            // theme font            
            $fonttype = $this->getThemeFontType();
            if($this->showClientPanel())
            {
                $client_type = GetDCCPInterface()->getIClientPanel()->getValue('font_type');
                if($client_type !== null)
                {
                    $fonttype = $client_type;
                }    
            }            
            
            if($fonttype == CMS_FONT_TYPE_GOOGLE)
            {
                $font = GetDCCPInterface()->getGoogleFont($this->_general['theme_font_google']);
                if($this->showClientPanel())
                {
                    $index = GetDCCPInterface()->getIClientPanel()->getValue('font_google');
                    if($index !== null)
                    {
                        $font = GetDCCPInterface()->getGoogleFont($index);    
                    }                
                }            
                 $out .= 'h1, h2, h3, h4, h5, h6, h1 span, h2 span, h3 span, h4 span, .section-text-head,
                    .section-text-head-small-mono, #accordion-container .slide h3, .home-featured-video .head, .photo-news-slider .number .count,
                    .photo-news-slider .number .counter, .photo-news-slider .area .photo .title, .photo-news-related-home .head,
                    .photo-news-all-tax .head, .photo-news-related .head, .media-gallery .one-columns .item .head     { ';
                    $out .= 'font-family:'.$font->_css.';';
                $out .= '} ';
            }
            
            
            // background image
            if($this->_general['theme_bg_use'] and (trim($this->_general['theme_bg_url']) != ''))
            {
                $out .= 'body { ';
                    $out .= 'background-image:url('.$this->_general['theme_bg_url'].');';
                    $out .= 'background-position:'.$this->_general['theme_bg_pos_x'].' '.$this->_general['theme_bg_pos_y'].';';
                    $out .= 'background-repeat:'.$this->_general['theme_bg_repeat'].';';
                    $out .= 'background-attachment:'.$this->_general['theme_bg_attachment'].';';
                $out .= '} ';
            }

            // background color
            if($this->_general['theme_bg_color_use'])
            {
                $out .= 'body { ';
                    $out .= 'background-color:'.$this->_general['theme_bg_color'].';';
                $out .= '} ';
            }
            
            // header menu
            if($this->_general['hmenu_show'])
            {
                if($this->_general['hmenu_bg_show'])
                {
                    $out .= '#header-top-menu { ';
                    $out .= 'background-color:'.$this->_general['hmenu_bg_color'].';';
                    $out .= 'opacity:'.number_format(((float)$this->_general['hmenu_bg_opacity']/100.0), 2, '.', '').';';
                    $out .= 'filter:alpha(opacity='.$this->_general['hmenu_bg_opacity'].');';
                    $out .= '} '; 
                }
                
                $out .= '#header-top-menu li a { ';
                $out .= 'color:'.$this->_general['hmenu_color'].';'; 
                $out .= '}'; 
                
                $out .= '#header-top-menu li a:hover { ';
                $out .= 'color:'.$this->_general['hmenu_hover_color'].';'; 
                $out .= '}';                 
            }                   
            
            // logo
            $out .= '#header-container  .logo { ';
                
                if(!$this->_general['logo_useright'])
                {
                    $out .= 'left:'.$this->_general['logo_left'].'px;right:auto;';
                } else
                {
                    $out .= 'right:'.$this->_general['logo_right'].'px;left:auto;';     
                }

                if(!$this->_general['logo_usebottom'])
                {
                    $out .= 'top:'.$this->_general['logo_top'].'px;bottom:auto;';
                } else
                {
                    $out .= 'bottom:'.$this->_general['logo_bottom'].'px;top:auto;';     
                }
                
                $out .= 'width:'.$this->_general['logo_width'].'px;
                height:'.$this->_general['logo_height'].'px;';                 
                $out .= 'background-image:url(\''.$this->_general['logo_path'].'\');                            
            } '; 
            
            // empty spaces                        
            $out .= '#content-top-empty-space { height:'.$this->_general['theme_top_space_height'].'px; }';
            $out .= '#content-bottom-empty-space { height:'.$this->_general['theme_bottom_space_height'].'px; }';
            
            // header
            $out .= '#header-container { height:'.$this->_general['header_height'].'px; }';
            
            echo $out;
                          
        echo '</style>';
    }     

    
    public function showClientPanel()
    {
        return $this->_general['theme_show_client_panel'];            
    }
    
    public function isAutoFormatingDisabled()
    {
        return $this->_general['theme_disable_autoformating'];
    }

    public function isAutoTexturizeDisabled()
    {
        return $this->_general['theme_disable_autotexturize'];
    }    
    
    public function isAuthorizationComment()
    {
        return $this->_general['theme_authorize_comment'];
    }

    public function isAuthorizationContact()
    {
        return $this->_general['theme_authorize_contact'];
    }    
                          
    public function printHeadMeda()
    {
        $out = '';
        $out .= '<meta name="cms_tip_bgcolor" content="'.$this->_general['theme_tip_bgcolor'].'" />';
        $out .= '<meta name="cms_tip_color" content="'.$this->_general['theme_tip_color'].'" />'; 
        $out .= '<meta name="cms_tip_bordercolor" content="'.$this->_general['theme_tip_bordercolor'].'" />';  
        return $out;
    }    
   
    public function getThemeSkin()
    {
        return $this->_general['theme_skin'];
    }
    
    public function getThemeColor()
    {
        return $this->_general['theme_color'];
    }
    
    public function getThemeGoogleFontIndex()
    {
        return $this->_general['theme_font_google'];    
    }

    public function getThemeCufonFontIndex()
    {
        return $this->_general['theme_font_cufon'];    
    }
    
    public function getThemeFontType()
    {
        return $this->_general['theme_font_type'];    
    }
    
    // media buttons
    public function getMediaButtons()
    {
        $out = '';
        $out .= $this->getPredefinedMediaButtons();
        if($this->_general['mbtns_show'])
        {
            if(is_array($this->_general['mbtns_list']))
            {
                foreach($this->_general['mbtns_list'] as $item)
                {
                    if(!$item->_hide)
                    {
                        $out .= '<a class="dc-add-media-btn" href="" title="'.$item->_name.'"><img src="'.$item->_icon.'" alt="DC Button" onclick="return false;" /><pre class="start" style="display:none;"><!-- '.stripcslashes($item->_start).' --></pre><pre class="end" style="display:none;"><!-- '.stripcslashes($item->_end).' --></pre></a>'; 
                    }    
                }    
            }    
        }
        return $out;
    }
    
    public function getPredefinedMediaButtons()
    {
        $out = '';
        $out .= '<a class="dc-add-media-shortcodes" href="" title="Theme shortcodes"><img src="'.get_bloginfo('template_url').'/cms/img/shortcodes.png" alt="DC Button" onclick="return false;" /></a>';
        $out .= '<a class="dc-add-media-image" href="" title="Insert image"><img src="'.get_bloginfo('template_url').'/cms/img/insert-image.png" alt="DC Button" onclick="return false;" /></a>';
        return $out;
    }
    
     // announcment bar
    public function renderAnnouncementBar()
    {
        if($this->_general['annbar_show'])
        {                       
            $style = ' style="';
            $style .= 'padding:'.$this->_general['annbar_padding_top'].'px '.$this->_general['annbar_padding_right'].'px '.$this->_general['annbar_padding_bottom'].'px '.$this->_general['annbar_padding_left'].'px;';
            $style .= 'height:'.$this->_general['annbar_height'].'px;';
            $style .= 'font-family:'.$this->_general['annbar_font'].';';
            $style .= 'line-height:'.$this->_general['annbar_line_height'].'px;';
            $style .= 'font-size:'.$this->_general['annbar_font_size'].'px;';  
            $style .= 'color:#'.$this->_general['annbar_font_color'].';';
            if($this->_general['annbar_bg_use'] and $this->_general['annbar_bg_image'] != '') { $style .= 'background-image:url('.$this->_general['annbar_bg_image'].');'; }
            if($this->_general['annbar_transparent'])
            {
                $style .= 'background-color:transparent;';      
            } else { $style .= 'background-color:#'.$this->_general['annbar_bg_color'].';'; }
            
            if($this->_general['annbar_border_show'])
            {
                $style .= 'border-top:1px solid #'.$this->_general['annbar_border_color'].';';    
            }
            
            $style .= '" ';            
                                
            $out = '';
            $out .= '<div id="announcement-bar" '.$style.'>';
            $out .= apply_filters('the_content', stripcslashes($this->_general['annbar_content']));
            $out .= GetDCCPInterface()->getIFloatingObjects()->renderObjects('announcement-bar', false); 
            $out .= '</div>';
            echo $out;
        }
    }     
        
     // splash screen
     public function renderSplashScreen()
     {
         global $post;
         if($this->_general['home_ss_use_page'] and ($post->ID != $this->_general['home_ss_page'])) { return; }
         
         if($this->_general['home_ss_show'] and !isset($_COOKIE['dc_is_splash_screen']))
         {   
             $SINGLE_BTN_HEIGHT = 29;
             $image_style = ' style="width:'.$this->_general['home_ss_width'].'px;height:'.$this->_general['home_ss_height'].'px;" ';
             
             
             $add_height = 0;             
             $count = 0;
             if(is_array($this->_general['home_ss_btns'])) { $count = count($this->_general['home_ss_btns']); }
             if($count)
             {
                 foreach($this->_general['home_ss_btns'] as $btn)
                 {
                    if($btn->_use)
                    {
                        $add_height += $SINGLE_BTN_HEIGHT;
                    }    
                 }
             }
             
             $btns_panel_height = $this->_general['home_ss_height'];
             if($this->_general['home_ss_btns_show'])
             {
                 $btns_panel_height += $add_height;
             }             
             $window_style = ' style="top:'.$this->_general['home_ss_top'].'px;height:'.$btns_panel_height.'px;width:'.$this->_general['home_ss_width'].'px;" ';
             
             $out = '';
             if($this->_general['home_ss_show_bg'])
             {
                 $out .= '<div id="dc-splash-screen-bg"></div>';                          
             }
            $out .= '<div '.$window_style.' id="dc-splash-screen">';
                $out .= '<div class="content" style="height:'.$btns_panel_height.'px;width:'.$this->_general['home_ss_width'].'px;">';
                
                     $lobj = $this->_general['home_ss_link'];
                     if(!is_object($lobj))
                     {
                        $lobj = new DCC_CCommonLink();    
                     }
                
                     $link = '';
                     if($lobj->_use)
                     {
                        if($lobj->_target == CMS_LINK_TARGET_SELF) { $link .= ' target="_self" '; } else { $link .= ' target="_blank" '; }
                        if($lobj->_type == CMS_LINK_TYPE_PAGE)
                        {
                            $link .= ' href="'.get_permalink($lobj->_page).'" ';
                        } else
                        {
                            $link .= ' href="'.$lobj->_link.'" ';
                        } 
                     }
                
                    $out .= '<a '.$link.' class="image-wrapper async-img" '.$image_style.' rel="'.$this->_general['home_ss_image'].'"></a>';
                    $out .= '<div class="close-btn"></div>';
                    
                    $out .= GetDCCPInterface()->getIFloatingObjects()->renderObjects('dc-splash-screen', false);
                    
                    if($this->_general['home_ss_btns_show']) 
                    {
                        if(is_array($this->_general['home_ss_btns']))
                        {
                             $bottom_pos = 0;
                             foreach($this->_general['home_ss_btns'] as $btn)
                             {
                                if($btn->_use)
                                {
                                    $style = ' style="bottom:'.$bottom_pos.'px;" ';
                                    $out .= '<a '.$style.' ';
                                    if($btn->_target == CMS_LINK_TARGET_SELF) { $out .= ' target="_self" '; } else { $out .= ' target="_blank" '; }
                                    if($btn->_type == CMS_LINK_TYPE_PAGE)
                                    {
                                        $out .= ' href="'.get_permalink($btn->_page).'" ';
                                    } else
                                    {
                                        $out .= ' href="'.$btn->_link.'" ';
                                    }
                                    $out .= ' class="btn">'.$btn->_text.'</a>';
                                    $bottom_pos += $SINGLE_BTN_HEIGHT;
                                }    
                             }                            
                        }    
                    }
                    
                $out .= '</div>';
                if($this->_general['home_ss_sticked'])
                {
                    $out .= '<div class="no-cookie"></div>';
                }
            $out .= '</div>';             
             
             echo $out;
         }
     }        
    
    // 404 page
    public function get404Title() { return stripcslashes($this->_general['404_title']); }
    public function get404Text() { return stripcslashes($this->_general['404_text']); } 
        
    // lightbox
    public function getLightBoxMode() { return $this->_general['theme_lightbox_mode']; }
    public function getLightBoxOverlayGallery() { return $this->_general['theme_lightbox_overlay_gallery']; }
    
    // sliders
    public function getSliderType()
    {
        return $this->_general['theme_slider'];
    }    
    
    public function renderHomepageSlider()
    {
        $slidertype = $this->getSliderType();
        
        if($this->showClientPanel())
        {
            $slidertype_client = GetDCCPInterface()->getIClientPanel()->getValue('slider_type');
            if($slidertype_client !== null)
            {
                $slidertype = $slidertype_client;    
            }
        }

        if($slidertype == CPGeneral::NO_SLIDER) 
        { 
            
        } else        
        if($slidertype == CPGeneral::ACCORDION_SLIDER)
        {    
            GetDCCPInterface()->getIAccordionSlider()->renderSlider(); 
        } else
        if($slidertype == CPGeneral::PROGRESS_SLIDER)
        {
            GetDCCPInterface()->getIProgressSlider()->renderSlider(); 
        } else
        if($slidertype == CPGeneral::CHAIN_SLIDER)
        {
            GetDCCPInterface()->getIChainSlider()->renderSlider(); 
        }                                          
    }    
    
    public function getTeamMemberSidebar()
    {
        return $this->_general['sidebar_team_member'];
    }
    
    // header icons
    public function renderHeaderIcons()
    {
        if($this->_general['hicons_show'])
        {
            $out = '';
            
            $style = ' style="';
            $style .= 'height:'.$this->_general['hicons_size'].'px;width:'.$this->_general['hicons_width'].'px;';
            if($this->_general['hicons_use_right']) { $style .= 'right:'.$this->_general['hicons_right'].'px;'; } else { $style .= 'left:'.$this->_general['hicons_left'].'px;'; }
            if($this->_general['hicons_use_bottom']) { $style .= 'bottom:'.$this->_general['hicons_bottom'].'px;'; } else { $style .= 'top:'.$this->_general['hicons_top'].'px;'; } 
            $style .= '" ';
            
            $out .= '<div '.$style.' class="header-icons-panel">';
            
                $style = ' style="';
                $style .= 'width:'.$this->_general['hicons_size'].'px;height:'.$this->_general['hicons_size'].'px;';
                if($this->_general['hicons_use_right']) { $style .= 'float:right;margin-left:5px;'; } else { $style .= 'float:left;margin-right:5px;'; }
                $style .= '" ';               
                
                if(is_array($this->_icons))
                {
                    foreach($this->_icons as $ico)
                    {                      
                        if($ico->_image != '' and !$ico->_hide)
                        {   
                            $class = '';
                            if($ico->_desc != '' and $this->_general['hicons_show_tip']) { $class .= ' tip-left-bottom'; }
                            if($ico->_hovered) { $class .= ' hovered'; }
                                                 
                            $title = '';
                            if($ico->_desc != '') { $title = 'title="'.$ico->_desc.'"'; }                     
                                                 
                            $out .= '<a '.$style.' href="'.$ico->_url.'" target="_blank" class="icon'.$class.'" '.$title.'>';
                                $out .= '<img src="'.$ico->_image.'" alt="'.$ico->_desc.'" />';
                            $out .= '</a>';
                        }
                    }
                }
            
            $out .= '</div>';
            
            echo $out;    
        }    
    }
    
    public function renderHeaderMenu()
    {
        $locations = get_registered_nav_menus();
        $menus = wp_get_nav_menus();
        $menu_locations = get_nav_menu_locations();
        
        $location_id = 'dcm_headertop';        
        $menu_id = 0;
        if(isset($menu_locations[$location_id]))
        {
            $menu_id = $menu_locations[$location_id];
        }
            
        if($this->_general['hmenu_show'])
        {
            if($menu_id != 0)
            {            
                $out = '';
                $out .= '<div id="header-top-menu">';
                $out .= '<div style="width:980px; margin:auto;">';
                    $out .= wp_nav_menu(array('theme_location' => 'dcm_headertop', 'container' => false, 'echo' => false)); 
                $out .= '</div></div>';
                echo $out;
            } else
            {
                $out = '';
                $out .= '<div id="header-top-menu">';
                $out .= '<div style="width:980px;height:11px;margin:auto;">';
                $out .= '</div></div>';
                echo $out;                
            } 
        }       
    }
    
    // footer         
    public function getFooterLogoPath() { return $this->_general['footer_logo_path']; }  
    public function getFooterLinksContent() { return stripcslashes($this->_general['footer_links_content']); }
    public function isFooterLinksShowed() { return $this->_general['footer_show_links']; }       
    public function isFooterWidgetizedShowed() { return $this->_general['footer_show_widgetized']; }           
    public function isFooterLogoShowed() { return $this->_general['footer_show_logo']; }
    public function isFooterCopyShowed() { return $this->_general['footer_show_copy']; }
    public function getFooterCopy() { return stripcslashes($this->_general['footer_copy']); }
    public function getFooterCopyAlign() { return $this->_general['footer_copy_align']; } 
    public function getFooterSidebarSeparatorWidth() { return $this->_general['footer_sid_separator_width']; }
    
    public function printFooterTrackingCode()
    {
        if($this->_general['tcode_footer_show'])
        {
            echo stripcslashes($this->_general['tcode_footer']);
        }
    }
    
    public function renderWidgetizedFooter()
    {
        if($this->isFooterWidgetizedShowed())
        {
            echo '<div id="footer-widgetized">';
            
                $first = true;
                $sep_width = $this->getFooterSidebarSeparatorWidth();
                if($this->_general['footer_sid_a'] != CMS_NOT_SELECTED and $this->_general['footer_sid_a_show'])
                {
                    if(!$first) { echo '<div class="sidebar-separator" style="width:'.$sep_width.'px;"></div>'; }
                    echo '<div class="sidebar-container" style="width:'.$this->_general['footer_sid_a_width'].'px;">';
                        dynamic_sidebar('sidebar-'.$this->_general['footer_sid_a']);        
                    echo '</div>';
                    $first = false;    
                }

                if($this->_general['footer_sid_b'] != CMS_NOT_SELECTED and $this->_general['footer_sid_b_show'])
                {
                    if(!$first) { echo '<div class="sidebar-separator" style="width:'.$sep_width.'px;"></div>'; } 
                    echo '<div class="sidebar-container" style="width:'.$this->_general['footer_sid_b_width'].'px;">';
                        dynamic_sidebar('sidebar-'.$this->_general['footer_sid_b']);        
                    echo '</div>';
                    $first = false;     
                }

                if($this->_general['footer_sid_c'] != CMS_NOT_SELECTED and $this->_general['footer_sid_c_show'])
                {
                    if(!$first) { echo '<div class="sidebar-separator" style="width:'.$sep_width.'px;"></div>'; } 
                    echo '<div class="sidebar-container" style="width:'.$this->_general['footer_sid_c_width'].'px;">';
                        dynamic_sidebar('sidebar-'.$this->_general['footer_sid_c']);        
                    echo '</div>';
                    $first = false;     
                }

                if($this->_general['footer_sid_d'] != CMS_NOT_SELECTED and $this->_general['footer_sid_d_show'])
                {
                    if(!$first) { echo '<div class="sidebar-separator" style="width:'.$sep_width.'px;"></div>'; } 
                    echo '<div class="sidebar-container" style="width:'.$this->_general['footer_sid_d_width'].'px;">';
                        dynamic_sidebar('sidebar-'.$this->_general['footer_sid_d']);        
                    echo '</div>';
                    $first = false;     
                }

                if($this->_general['footer_sid_e'] != CMS_NOT_SELECTED and $this->_general['footer_sid_e_show'])
                {
                    if(!$first) { echo '<div class="sidebar-separator" style="width:'.$sep_width.'px;"></div>'; } 
                    echo '<div class="sidebar-container" style="width:'.$this->_general['footer_sid_e_width'].'px;">';
                        dynamic_sidebar('sidebar-'.$this->_general['footer_sid_e']);        
                    echo '</div>';
                    $first = false;     
                }

                if($this->_general['footer_sid_f'] != CMS_NOT_SELECTED and $this->_general['footer_sid_f_show'])
                {
                    if(!$first) { echo '<div class="sidebar-separator" style="width:'.$sep_width.'px;"></div>'; } 
                    echo '<div class="sidebar-container" style="width:'.$this->_general['footer_sid_f_width'].'px;">';
                        dynamic_sidebar('sidebar-'.$this->_general['footer_sid_f']);        
                    echo '</div>';
                    $first = false;     
                }
            
                echo '<div class="clear-both"></div>';
            echo '</div>';
        }
    }
       
    // header
    public function printHeaderTrackingCode()
    {
        if($this->_general['tcode_header_show'])
        {
            echo stripcslashes($this->_general['tcode_header']);
        }                                                                  
    }            
   
    // pagination
    public function renderSitePagination($paged, $max_page, $echo=true)
    {
        $BEFORE_DOTS = 3;
        $SHOW_PREV_NEXT_BTN = true;
        
        $out = '';
        if($max_page > 1) 
        { 
            $out .= '<div class="page-post-pagination">';
            $out .= '<span class="pages">Pages:</span>';
            
            if($paged > 1 and $SHOW_PREV_NEXT_BTN)
            {
                $out .= '<a class="next-prev" href="'.get_pagenum_link($paged-1).'">'.__('&laquo; Prev', 'dc_theme').'</a>'; 
            }             
                        
            if($max_page > 15)
            {
                 $start = $paged - $BEFORE_DOTS;
                 if($start < 1) { $start = 1; }
                 $last_end = 0;
                 if($start > 5)
                 {
                  
                    $last_end = 2;
                    for($i = 1; $i <= $last_end; $i++)
                    {
                        if($i == $paged)
                        {
                            $out .= '<a class="current" >'.$i.'</a>';    
                        } else
                        {
                            $out .= '<a href="'.get_pagenum_link($i).'">'.$i.'</a>';
                        }
                    }                    
                    $out .= '<span class="separator">...</span>';    
                 }
                                 

                $start = $paged - $BEFORE_DOTS;
                if($start < 6) { $start = 1; }
                $last_end = $paged+$BEFORE_DOTS;
                if($last_end > $max_page)
                {
                    $last_end = $max_page;
                }
                for($i = $start; $i <= $last_end; $i++)
                {
                    if($i == $paged)
                    {
                        $out .= '<a class="current" >'.$i.'</a>';    
                    } else
                    {
                        $out .= '<a href="'.get_pagenum_link($i).'">'.$i.'</a>';
                    }
                }  
                
                if($last_end != $max_page)
                {
                    if($max_page - $BEFORE_DOTS > $last_end)
                    {
                        $out .= '<span class="separator">...</span>';
                        
                        for($i = $max_page-1; $i <= $max_page; $i++)
                        {
                            if($i == $paged)
                            {
                                $out .= '<a class="current" >'.$i.'</a>';    
                            } else
                            {
                                $out .= '<a href="'.get_pagenum_link($i).'">'.$i.'</a>';
                            }
                        }                         
                            
                    } else
                    {
                        for($i = $last_end+1; $i <= $max_page; $i++)
                        {
                            if($i == $paged)
                            {
                                $out .= '<a class="current" >'.$i.'</a>';    
                            } else
                            {
                                $out .= '<a href="'.get_pagenum_link($i).'">'.$i.'</a>';
                            }
                        }                          
                    }
                }
   
            } else
            {
                $out .= $this->renderSiteAllPages($paged, $max_page, false); 
            }
                        
            if($paged < $max_page and $SHOW_PREV_NEXT_BTN)
            {
                $out .= '<a class="next-prev" href="'.get_pagenum_link($paged+1).'">'.__('Next &raquo;', 'dc_theme').'</a>'; 
            }
                       
            $out .= '<div class="clear-both"></div></div>';
        }  
        
        if($echo) { echo $out; } else { return $out; }              
    }
        
    public function renderSiteAllPages($paged, $max_page, $echo=true)
    {
        $out = '';
        for($i = 1; $i <= $max_page; $i++)
        {
            if($i == $paged)
            {
                $out .= '<a class="current" >'.$i.'</a>';    
            } else
            {
                $out .=  '<a href="'.get_pagenum_link($i).'">'.$i.'</a>';
            }
        } 
        
        if($echo) { echo $out; } else { return $out; }       
    } 
              
   /**
    * Constructor that sets the time to a given value.
    * 
    * @param timemillis Number of milliseconds 
    *        passed since Jan 1, 1970.
    * 
    * @return wp_link_pages return value
    */ 
    public function renderWordPressPagination($echo=1)
    {
        $args = array(
            'before'           => '<div class="page-links"><span class="before">Pages: </span>',
            'after'            => '<div class="clear-both"></div></div>',
            'link_before'      => '<span>',
            'link_after'       => '</span>',
            'next_or_number'   => 'number',
            'nextpagelink'     => 'Next page',
            'previouspagelink' => 'Previous page',
            'pagelink'         => '%',
            'more_file'        => '',
            'echo'             => $echo ); 
        
        $result = '';    
        $result = wp_link_pages($args); 
        return $result;                       
    }             
                                                                                                                                 
    // search        
    public function getSearchSidebarID() { return $this->_general['search_sidebar']; }
    public function getSearchNoResultsText() { return stripcslashes($this->_general['search_no_results']); }
    public function getSearchTitleText() { return stripcslashes($this->_general['search_before_fragment']); }        
    public function getSearchPageSize() { return (int)$this->_general['search_page_size']; }    
    public function isSearchedInPosts() { return $this->_general['search_in_posts']; }
    public function isSearchedInPages() { return $this->_general['search_in_pages']; } 
    public function isSearchFormShowed() { return $this->_general['search_show_form']; }
    
    public function isSearchHPanelShowed() { return $this->_general['search_header_panel_show']; }
    public function isSearchHPanelLeft() { return $this->_general['search_header_panel_left']; } 
    public function isSearchHPanelRight() { return $this->_general['search_header_panel_right']; } 
    public function isSearchHPanelTop() { return $this->_general['search_header_panel_top']; } 
    public function isSearchHPanelBottom() { return $this->_general['search_header_panel_bottom']; } 
    public function getSearchHPanelPlace() { return $this->_general['search_header_panel_place']; } 
    
    
    public function isSearchHPanelUseRight() { return $this->_general['search_header_panel_use_right']; } 
    public function isSearchHPanelUseBottom() { return $this->_general['search_header_panel_use_bottom']; }
    
    public function renderSearchHPanel($place)
    {
        if($place != $this->_general['search_header_panel_place']) { return; }
        
        if($this->isSearchHPanelShowed())
        {
            $search_query = (get_query_var( 's' )) ? get_query_var( 's' ) : '';
            
            $style = '';
            $style .= ' style="';
            
                if($this->isSearchHPanelUseRight())
                {
                    $style .= 'right:'.$this->isSearchHPanelRight().'px;';
                } else { $style .= 'left:'.$this->isSearchHPanelLeft().'px;'; }
                
                if($this->isSearchHPanelUseBottom())
                {
                    $style .= 'bottom:'.$this->isSearchHPanelBottom().'px;';
                } else { $style .= 'top:'.$this->isSearchHPanelTop().'px;'; }                    
                
            $style .= '" ';
            
            $out = '';
            $out .= '<div class="search-panel" '.$style.'>';
                $out .= '<form role="search" method="get" id="searchform" action="'.get_bloginfo('url').'">';
                    if($this->isSearchHPanelUseRight())
                    {
                        $out .= '<a class="submit-btn"></a>'; 
                        $out .= '<input type="text" value="'.$search_query.'" name="s" id="s">';   
                    } else
                    {                        
                        $out .= '<input type="text" value="'.$search_query.'" name="s" id="s" style="float:left;">';
                        $out .= '<a class="submit-btn" style="float:left;"></a>';
                    }                    
                $out .= '</form>';
                $out .= '<div class="clear-both"></div>';         
            $out .= '</div>';
            echo $out;
        }        
    }
    
    // sidebar
    public function includeSidebar($sidebarid, $pos)
    {                      
        $sidid = $sidebarid;
        
        if($sidid == CMS_NOT_SELECTED or $sidid == '')
        {
            // if is single assing blog sidebar else assing defeault sidebar
            if(is_single())
            {               
                if($this->_general['sidebar_single'] != CMS_NOT_SELECTED)
                {
                    $sidid = $this->_general['sidebar_single']; 
                } else
                {
                    $sidid = $this->_general['sidebar_default'];     
                }    
            } else
            {
               $sidid = $this->_general['sidebar_default']; 
            }                 
        }        

        if($pos == CMS_SIDEBAR_RIGHT) { echo '<div class="sidebar-container-right">'; } else
        if($pos == CMS_SIDEBAR_LEFT) { echo '<div class="sidebar-container-left">'; } else 
        { 
            // get settings from global
            
            if($this->_general['sidebar_global_pos'] == CMS_SIDEBAR_RIGHT) { echo '<div class="sidebar-container-right">'; } else
            if($this->_general['sidebar_global_pos'] == CMS_SIDEBAR_LEFT) { echo '<div class="sidebar-container-left">'; }
        }                       
        
        if($sidid != CMS_NOT_SELECTED and $sidid != '')
        {      
            //echo '<div class="top-section"></div>';
            if(!function_exists('dynamic_sidebar') or !dynamic_sidebar('sidebar-'.$sidid)) {}
            //echo '<div class="bottom-section"></div>';                 
        } else
        {
            //echo '<div class="top-section"></div>';
            echo '<p class="theme-exception">'.__('No sidebar defined or selected for this page/post. 
            Please go to theme general options and create sidebar - section MElegance > General > Sidebars. Then go to (WordPress) Appearance > Widget section - here you can add some widget to created sidebar. Set default sidebar in theme general 
            options or assign invidual sidebar to given page or post in Custom Page/Post Settings. More about using sidebars and widgets you will find also in PDF User Guide.', 'dc_theme').'</p>';
            //echo '<div class="bottom-section"></div>';     
        }
        echo '</div>';        
    }
    
    public function getSidebarGlobalPos($pos)
    {
        if($pos != CMS_SIDEBAR_LEFT and $pos != CMS_SIDEBAR_RIGHT)
        {
            $pos = $this->_general['sidebar_global_pos'];
        }
        return $pos;
    }
    
    public function getSidebarForBlogCat()
    {
        return $this->_general['sidebar_blog_category'];
    }
    
    public function registerSidebars()
    {        
        $count = count($this->_sidebars);
        for($i = 0; $i < $count; $i++)
        {        
            if(function_exists('register_sidebars') )
            {
                $id = 'sidebar-'.$this->_sidebars[$i]->_id;
                $args = array(
                'name'=>$this->_sidebars[$i]->_name,
                'id'=>$id,
                'before_widget' => $this->getBeforeWidget(),
                'after_widget' => $this->getAfterWidget(),
                'before_title' => '<div class="head">',
                'after_title' => '</div>',
                );                
                                
                register_sidebar($args);  
            }                        
        } // for
    }      
    
    public function getBeforeWidget()
    {
        $out = '';
        $out .= '<div class="dc-widget-wrapper">';
        return $out;
    }   
    
    public function getAfterWidget()
    {
        $out = '';
        $out .= '</div>';
        return $out;
    }        
    
    public function printSidebarsList($width, $name, $sidebar)
    {
        $count = count($this->_sidebars);
        
        $out = '';
        $out .= '<select style="width:'.$width.'px;" name="'.$name.'">';                                   
        $out .= '<option value="'.CMS_NOT_SELECTED.'" '.($value == CMS_NOT_SELECTED ? ' selected="selected" ' : '').'>Not selected</option>';
        for($i = 0; $i < $count; $i++)
        {
            $out .= '<option value="';
            $out .= $this->_sidebars[$i]->_id;
            $out .= '" ';
            $out .= ($sidebar == $this->_sidebars[$i]->_id ? ' selected="selected" ' : '');
            $out .= '>';
            $out .= $this->_sidebars[$i]->_name;
            $out .= '</option>';
        }
        $out .= '</select>';
        return $out;        
    }             
    
    // breadcrumbs
    public function isBreadcrumbForcedName() { return $this->_general['breadcrumb_force_name']; }   
    public function getBreadcrumbName() { return $this->_general['breadcrumb_name']; } 
    public function getBreadcrumbBefore() { return $this->_general['breadcrumb_before']; }
    
    // social communities
    public function isFacebookShowed() { return $this->_general['social_show_facebook']; }  
    public function isTwitterShowed() { return $this->_general['social_show_twitter']; } 
    public function isGooglePlusShowed() { return $this->_general['social_show_google']; } 
    public function isFacebookThumbShowed() { return $this->_general['social_show_facebook_thumb']; } 
    public function isFacebookUseOwnThumb() { return $this->_general['social_use_thumb']; }    
    public function getFacebookThumbURL() { return $this->_general['social_thumb']; }      
    
    // blog
    public function isBlogMediumTagsShowed() { return $this->_general['blog_show_tags_for_medium']; }  
    public function isBlogMediumTagsHomeShowed() { return $this->_general['blog_show_tags_for_medium_home']; }     
    public function isBlogSmallTimeShowed() { return $this->_general['blog_show_time_for_small']; } 
    public function isBlogMediumTimeShowed() { return $this->_general['blog_show_time_for_medium']; }      
    
    public function isBlogSmallImgPreviewShowed() { return $this->_general['blog_show_preview_for_small']; } 
    public function isBlogMediumImgPreviewShowed() { return $this->_general['blog_show_preview_for_medium']; }
    
    public function isBlogSingleAuthorShowed() { return $this->_general['blog_show_author_info']; }
    public function isBlogSingleRelatedShowed() { return $this->_general['blog_show_related']; }  
    public function getBlogRelatedRows() { return $this->_general['blog_related_rows']; }
    
    // favicon
    public function renderFavicon()
    {
        if($this->_general['theme_favicon_use'] and $this->_general['theme_favicon_url'] != '')
        {
            $out = '';
            $out .= '<!-- ICON -->';
            $out .= '<link rel="icon" href="'.$this->_general['theme_favicon_url'].'" type="image/x-icon" /> ';
            echo $out;    
        }
    }
    
    // background
    public function isThemeBgForced() { return $this->_general['theme_bg_force']; }
    
    // background color             
    public function isThemeBgColorForced() { return $this->_general['theme_bg_color_force'];  }
                                   
    /*********************************************************** 
    * Private functions
    ************************************************************/      

    private function setDefaults()
    { 
        $this->_generalDef['logo_path'] = get_bloginfo('template_url').'/img/common/logobig.png';
        $this->_generalDef['footer_logo_path'] = get_bloginfo('template_url').'/img/common/logosmall.png';    
        $this->_generalDef['footer_links_content'] = 
        '
            <div class="text">
                <h6 class="nocufon">ABOUT US</h6>
                <div style="height:2px; width:2px;"></div>
                Suspendisse posuere ultrices metus ac accumsan. In vulputate molestie magna auctor faucibus. 
                Curabitur id nisl nec ipsum molestie egestas in at dolor. Vestibulum a est ac tellus aliquam 
                tempor eu quis quam. Proin tellus metus, sagittis vitae sagittis eu, blandit sed diam. Lorem 
                ipsum dolor sit amet, consectetur adipiscing elit. 
            </div>
            
            <div class="links">
                <h6 class="nocufon">CONTACT / HELP</h6>
                <a class="linkType3" href="#">Help/FAQ</a><br />
                <a class="linkType3" href="#">Contact</a><br />
                <a class="linkType3" href="#">Office Locations</a><br />
                <a class="linkType3" href="#">Company Headquarter</a>
               </div>
               
            <div class="links">
                <h6 class="nocufon">SERVICES</h6>
                <a class="linkType3" href="#">Website Templates</a><br />
                <a class="linkType3" href="#">Logo Design</a><br />
                <a class="linkType3" href="#">Mobile Applications</a><br />
                <a class="linkType3" href="#">Graphic Design</a>
               </div>
               
            <div class="links last">
                <h6 class="nocufon">TEMPLATES</h6>
                <a class="linkType3" href="#">Image positioning</a><br />
                <a class="linkType3" href="#">List examples</a><br />
                <a class="linkType3" href="#">Text and Table</a><br />
                <a class="linkType3" href="#">Full Page examples</a>
            </div>                         
        ';
                       
    }
   
    private function process()
    {
        # THEME CORE
        if(isset($_POST['general_save_theme']))
        {            
            $this->_general['theme_skin'] = $_POST['theme_skin']; 
            $this->_general['theme_slider'] = $_POST['theme_slider']; 
            $this->_general['theme_font_google'] = $_POST['theme_font_google']; 
            $this->_general['theme_font_cufon'] = $_POST['theme_font_cufon'];
            $this->_general['theme_font_type'] = $_POST['theme_font_type'];
            
            $this->_general['theme_color'] = $_POST['theme_color'];
            
            $this->_general['theme_lightbox_mode'] = $_POST['theme_lightbox_mode']; 
            $this->_general['theme_lightbox_overlay_gallery'] = isset($_POST['theme_lightbox_overlay_gallery']) ? true : false;
            
            $this->_general['theme_top_space_height'] = $_POST['theme_top_space_height'];
            $this->_general['theme_bottom_space_height'] = $_POST['theme_bottom_space_height']; 
            $this->_general['theme_show_client_panel'] = isset($_POST['theme_show_client_panel']) ? true : false;
            $this->_general['theme_disable_autoformating'] = isset($_POST['theme_disable_autoformating']) ? true : false;            
            $this->_general['theme_disable_autotexturize'] = isset($_POST['theme_disable_autotexturize']) ? true : false;
            $this->_general['theme_authorize_comment'] = isset($_POST['theme_authorize_comment']) ? true : false; 
            $this->_general['theme_authorize_contact'] = isset($_POST['theme_authorize_contact']) ? true : false;             
            
            $this->_general['theme_favicon_url'] = $_POST['theme_favicon_url'];
            $this->_general['theme_favicon_use'] = isset($_POST['theme_favicon_use']) ? true : false; 
            
            $this->_general['theme_bg_url'] = $_POST['theme_bg_url'];
            $this->_general['theme_bg_repeat'] = $_POST['theme_bg_repeat'];
            $this->_general['theme_bg_pos_x'] = $_POST['theme_bg_pos_x'];
            $this->_general['theme_bg_pos_y'] = $_POST['theme_bg_pos_y'];
            $this->_general['theme_bg_attachment'] = $_POST['theme_bg_attachment']; 
            $this->_general['theme_bg_force'] = isset($_POST['theme_bg_force']) ? true : false; 
            $this->_general['theme_bg_use'] = isset($_POST['theme_bg_use']) ? true : false;            

            $this->_general['theme_bg_color'] = $_POST['theme_bg_color']; 
            $this->_general['theme_bg_color_use'] = isset($_POST['theme_bg_color_use']) ? true : false; 
            $this->_general['theme_bg_color_force'] = isset($_POST['theme_bg_color_force']) ? true : false;   
            
            $this->_general['theme_tip_bgcolor'] = $_POST['theme_tip_bgcolor'];
            $this->_general['theme_tip_color'] = $_POST['theme_tip_color'];
            $this->_general['theme_tip_bordercolor'] = $_POST['theme_tip_bordercolor'];        
            
            update_option($this->_DBIDOPT_GENERAL, $this->_general);
            $this->_saved = true;              
        }    

        # SOCIAL COMMUNITIES
        if(isset($_POST['general_save_social']))
        {
            $this->_general['social_show_facebook'] = isset($_POST['social_show_facebook']) ? true : false;
            $this->_general['social_show_twitter'] = isset($_POST['social_show_twitter']) ? true : false; 
            $this->_general['social_show_google'] = isset($_POST['social_show_google']) ? true : false;
            $this->_general['social_show_facebook_thumb'] = isset($_POST['social_show_facebook_thumb']) ? true : false;
            $this->_general['social_use_thumb'] = isset($_POST['social_use_thumb']) ? true : false;                             
            $this->_general['social_thumb'] = $_POST['social_thumb'];                
            
            update_option($this->_DBIDOPT_GENERAL, $this->_general);
            $this->_saved = true;              
        }                      
        
        # HEADER
        if(isset($_POST['general_save_header']))
        {
            $this->_general['header_height'] = $_POST['header_height']; 
            
            update_option($this->_DBIDOPT_GENERAL, $this->_general);
            $this->_saved = true;              
        }    

        # HEADER MENU
        if(isset($_POST['general_save_header_menu']))
        {
            $this->_general['hmenu_show'] = isset($_POST['hmenu_show']) ? true : false; 
            $this->_general['hmenu_bg_show'] = isset($_POST['hmenu_bg_show']) ? true : false; 
            $this->_general['hmenu_bg_color'] = $_POST['hmenu_bg_color'];
            $this->_general['hmenu_bg_opacity'] = (int)$_POST['hmenu_bg_opacity'];
            if($this->_general['hmenu_bg_opacity'] > 100) { $this->_general['hmenu_bg_opacity'] = 100; }
            if($this->_general['hmenu_bg_opacity'] < 0) { $this->_general['hmenu_bg_opacity'] = 0; }
            
            $this->_general['hmenu_color'] = $_POST['hmenu_color'];
            $this->_general['hmenu_hover_color'] = $_POST['hmenu_hover_color'];
            
            update_option($this->_DBIDOPT_GENERAL, $this->_general);
            $this->_saved = true;              
        }                  

        # 404 PAGE
        if(isset($_POST['general_save_404']))
        {
            $this->_general['404_title'] = $_POST['404_title'];
            $this->_general['404_text'] = $_POST['404_text'];
            
            update_option($this->_DBIDOPT_GENERAL, $this->_general);
            $this->_saved = true;  
        }                  
        
        # LOGO IN HEADER
        if(isset($_POST['general_save_logo']))
        {
            $this->_general['logo_width'] = $_POST['logo_width'];
            $this->_general['logo_height'] = $_POST['logo_height'];
            $this->_general['logo_path'] = $_POST['logo_path'];
            $this->_general['logo_left'] = $_POST['logo_left'];
            $this->_general['logo_top'] = $_POST['logo_top'];
            $this->_general['logo_right'] = $_POST['logo_right'];
            $this->_general['logo_bottom'] = $_POST['logo_bottom'];
            $this->_general['logo_useright'] = isset($_POST['logo_useright']) ? true : false;
            $this->_general['logo_usebottom'] = isset($_POST['logo_usebottom']) ? true : false;
            
            update_option($this->_DBIDOPT_GENERAL, $this->_general);
            $this->_saved = true;  
        }  
        
        # FOOTER
        if(isset($_POST['general_save_footer']))
        {                                        
            $this->_general['footer_logo_path'] = $_POST['footer_logo_path']; 
            $this->_general['footer_show_links'] = isset($_POST['footer_show_links']) ? true : false; 
            $this->_general['footer_links_content'] = $_POST['footer_links_content']; 
            $this->_general['footer_show_widgetized'] = isset($_POST['footer_show_widgetized']) ? true : false;
            $this->_general['footer_show_logo'] = isset($_POST['footer_show_logo']) ? true : false;                       
            $this->_general['footer_show_copy'] = isset($_POST['footer_show_copy']) ? true : false; 
            $this->_general['footer_copy'] = $_POST['footer_copy'];                        
            $this->_general['footer_copy_align'] = $_POST['footer_copy_align'];
            
            
            $this->_general['footer_sid_a'] = $_POST['footer_sid_a'];
            $this->_general['footer_sid_a_width'] = (int)$_POST['footer_sid_a_width'];
            $this->_general['footer_sid_a_show'] = isset($_POST['footer_sid_a_show']) ? true : false; 

            $this->_general['footer_sid_b'] = $_POST['footer_sid_b'];
            $this->_general['footer_sid_b_width'] = (int)$_POST['footer_sid_b_width'];
            $this->_general['footer_sid_b_show'] = isset($_POST['footer_sid_b_show']) ? true : false; 
            
            $this->_general['footer_sid_c'] = $_POST['footer_sid_c'];
            $this->_general['footer_sid_c_width'] = (int)$_POST['footer_sid_c_width'];
            $this->_general['footer_sid_c_show'] = isset($_POST['footer_sid_c_show']) ? true : false; 
            
            $this->_general['footer_sid_d'] = $_POST['footer_sid_d'];
            $this->_general['footer_sid_d_width'] = (int)$_POST['footer_sid_d_width'];
            $this->_general['footer_sid_d_show'] = isset($_POST['footer_sid_d_show']) ? true : false; 
            
            $this->_general['footer_sid_e'] = $_POST['footer_sid_e'];
            $this->_general['footer_sid_e_width'] = (int)$_POST['footer_sid_e_width'];
            $this->_general['footer_sid_e_show'] = isset($_POST['footer_sid_e_show']) ? true : false; 
            
            $this->_general['footer_sid_f'] = $_POST['footer_sid_f'];
            $this->_general['footer_sid_f_width'] = (int)$_POST['footer_sid_f_width'];
            $this->_general['footer_sid_f_show'] = isset($_POST['footer_sid_f_show']) ? true : false;                                                 
            
            $this->_general['footer_sid_separator_width'] = (int)$_POST['footer_sid_separator_width'];            
                                  
            update_option($this->_DBIDOPT_GENERAL, $this->_general);
            $this->_saved = true;  
        } 
        
        if(isset($_POST['general_restore_footer_links']))
        {
            $this->_general['footer_links_content'] = $this->_generalDef['footer_links_content']; 
            
            update_option($this->_DBIDOPT_GENERAL, $this->_general);
            $this->_saved = true;             
        }

        # TRACKING CODE         
        if(isset($_POST['general_tcode_settings']))
        {
            $this->_general['tcode_header'] = $_POST['tcode_header'];
            $this->_general['tcode_header_show'] =  isset($_POST['tcode_header_show']) ? true : false; 
            
            $this->_general['tcode_footer'] = $_POST['tcode_footer'];
            $this->_general['tcode_footer_show'] =  isset($_POST['tcode_footer_show']) ? true : false; 
            
            update_option($this->_DBIDOPT_GENERAL, $this->_general);
            $this->_saved = true;  
        }    
        
        # BREADCRUMB         
        if(isset($_POST['general_breadcrumb_settings']))
        {
            $this->_general['breadcrumb_before'] = $_POST['breadcrumb_before'];
            $this->_general['breadcrumb_force_name'] =  isset($_POST['breadcrumb_force_name']) ? true : false; 
            $this->_general['breadcrumb_name'] =  $_POST['breadcrumb_name']; 
            
            update_option($this->_DBIDOPT_GENERAL, $this->_general);
            $this->_saved = true;  
        }            
        
        # SIDEBAR
        if(isset($_POST['general_sidebar_settings']))
        {
            $this->_general['sidebar_default'] = $_POST['sidebar_default'];
            $this->_general['sidebar_global_pos'] =  $_POST['sidebar_global_pos']; 
            $this->_general['sidebar_blog_category'] =  $_POST['sidebar_blog_category']; 
            $this->_general['sidebar_team_member'] =  $_POST['sidebar_team_member'];
            $this->_general['sidebar_single'] =  $_POST['sidebar_single']; 
            
            update_option($this->_DBIDOPT_GENERAL, $this->_general);
            $this->_saved = true;  
        }          
        
        if(isset($_POST['general_add_sidebar']))
        {
            $temp = new CPSidebar();
            $temp->_name = $_POST['name'];
            array_push($this->_sidebars, $temp);
            update_option($this->_DBIDOPT_SIDEBARS, $this->_sidebars);
            $this->_saved = true;               
        }

        if(isset($_POST['general_save_sidebar']))
        {
            $index = $_POST['index'];
            $this->_sidebars[$index]->_name = $_POST['name'];
            update_option($this->_DBIDOPT_SIDEBARS, $this->_sidebars);
            $this->_saved = true;               
        }        

        if(isset($_POST['general_delete_sidebar']))
        {
            $index = $_POST['index'];
            unset($this->_sidebars[$index]);
            $this->_sidebars = array_values($this->_sidebars);
            update_option($this->_DBIDOPT_SIDEBARS, $this->_sidebars);
            $this->_saved = true;               
        } 
        
        # BLOG
        if(isset($_POST['general_blog_settings']))
        {
            $this->_general['blog_show_tags_for_medium'] =  isset($_POST['blog_show_tags_for_medium']) ? true : false; 
            $this->_general['blog_show_tags_for_medium_home'] =  isset($_POST['blog_show_tags_for_medium_home']) ? true : false; 
            $this->_general['blog_show_time_for_medium'] =  isset($_POST['blog_show_time_for_medium']) ? true : false; 
            $this->_general['blog_show_time_for_small'] =  isset($_POST['blog_show_time_for_small']) ? true : false; 
            $this->_general['blog_show_preview_for_small'] =  isset($_POST['blog_show_preview_for_small']) ? true : false;   
            $this->_general['blog_show_preview_for_medium'] =  isset($_POST['blog_show_preview_for_medium']) ? true : false;   
            $this->_general['blog_show_author_info'] =  isset($_POST['blog_show_author_info']) ? true : false;                           
            $this->_general['blog_show_related'] =  isset($_POST['blog_show_related']) ? true : false;                                      
            $this->_general['blog_related_rows'] =  $this->intRange((int)$_POST['blog_related_rows'], 1, 4); 
                                                           
            update_option($this->_DBIDOPT_GENERAL, $this->_general); 
            $this->_saved = true;               
        }  
        
        # HEADER ICONS                
        if(isset($_POST['general_save_header_icons']))
        {        
            $this->_general['hicons_show'] = isset($_POST['hicons_show']) ? true : false;
            $this->_general['hicons_show_tip'] = isset($_POST['hicons_show_tip']) ? true : false;                                                                                              
            $this->_general['hicons_size'] = (int)$_POST['hicons_size']; 
            $this->_general['hicons_width'] = $_POST['hicons_width'];
            
            $this->_general['hicons_left'] = $_POST['hicons_left'];
            $this->_general['hicons_right'] = $_POST['hicons_right'];
            $this->_general['hicons_top'] = $_POST['hicons_top'];
            $this->_general['hicons_bottom'] = $_POST['hicons_bottom'];
            
            $this->_general['hicons_use_right'] = isset($_POST['hicons_use_right']) ? true : false;
            $this->_general['hicons_use_bottom'] = isset($_POST['hicons_use_bottom']) ? true : false;                   
                               
                                                                     
            update_option($this->_DBIDOPT_GENERAL, $this->_general);
            $this->_saved = true;  
        }  
        
        if(isset($_POST['general_save_hicon']))
        {            
            $index = $_POST['index'];    
            $this->_icons[$index]->_hide = isset($_POST['hide']) ? true : false;
            $this->_icons[$index]->_image = $_POST['image'];
            $this->_icons[$index]->_url = $_POST['url'];
            $this->_icons[$index]->_desc = $_POST['desc'];
            $this->_icons[$index]->_hovered = isset($_POST['hovered']) ? true : false; 
            
            update_option($this->_DBIDOPT_HEADER_ICONS, $this->_icons); 
            $this->_saved = true;                   
        }           
        
        if(isset($_POST['general_add_hicon']))
        {
            $icon = new CPHeaderIcon('', false, '', '', false);          
            array_push($this->_icons, $icon);
            update_option($this->_DBIDOPT_HEADER_ICONS, $this->_icons);
            $this->_saved = true;                      
        } 
        
        if(isset($_POST['general_delete_hicon']))
        {
            $index = $_POST['index'];
            unset($this->_icons[$index]);
            $this->_icons = array_values($this->_icons);
            update_option($this->_DBIDOPT_HEADER_ICONS, $this->_icons); 
            $this->_saved = true;                      
        }
        
        if(isset($_POST['general_movedown_hicon']))
        {
            $index = $_POST['index'];
            $count = count($this->_icons); 
            $last = $count - 1;
            if($index < $last)
            {         
                $temp = $this->_icons[$index + 1];
                $this->_icons[$index + 1] = $this->_icons[$index];
                $this->_icons[$index] = $temp;
                
                update_option($this->_DBIDOPT_HEADER_ICONS, $this->_icons);
                $this->_saved = true;
            }                      
        }  
        
        if(isset($_POST['general_moveup_hicon']))
        {
            $index = $_POST['index'];
            if($index > 0)
            {         
                $temp = $this->_icons[$index - 1];
                $this->_icons[$index - 1] = $this->_icons[$index];
                $this->_icons[$index] = $temp;
                
                update_option($this->_DBIDOPT_HEADER_ICONS, $this->_icons);
                $this->_saved = true;
            }                      
        }         
                
        # SEARCH
        if(isset($_POST['general_save_search']))
        {        
            $this->_general['search_no_results'] = $_POST['search_no_results'];        
            $this->_general['search_before_fragment'] = $_POST['search_before_fragment'];
            $this->_general['search_page_size'] = $_POST['search_page_size'];
            $this->_general['search_sidebar'] = $_POST['search_sidebar']; 
            $this->_general['search_in_posts'] = isset($_POST['search_in_posts']) ? true : false;
            $this->_general['search_in_pages'] = isset($_POST['search_in_pages']) ? true : false;     
            $this->_general['search_show_form'] = isset($_POST['search_show_form']) ? true : false;            
                                         
            $this->_general['search_header_panel_show'] = isset($_POST['search_header_panel_show']) ? true : false;
            $this->_general['search_header_panel_use_right'] = isset($_POST['search_header_panel_use_right']) ? true : false; 
            $this->_general['search_header_panel_use_bottom'] = isset($_POST['search_header_panel_use_bottom']) ? true : false;
            $this->_general['search_header_panel_right'] = (int)$_POST['search_header_panel_right'];
            $this->_general['search_header_panel_left'] = (int)$_POST['search_header_panel_left'];
            $this->_general['search_header_panel_top'] = (int)$_POST['search_header_panel_top'];
            $this->_general['search_header_panel_bottom'] = (int)$_POST['search_header_panel_bottom'];                                                                                   
            $this->_general['search_header_panel_place'] = (int)$_POST['search_header_panel_place'];                                                          
                                                                     
            update_option($this->_DBIDOPT_GENERAL, $this->_general);
            $this->_saved = true;  
        }   
        
        # SPLASH SCREEN
        if(isset($_POST['general_save_splashscreen_opt']))
        {                        
            $this->_general['home_ss_show'] = isset($_POST['home_ss_show']) ? true : false;                                                                                                                                                   
            $this->_general['home_ss_show_bg'] = isset($_POST['home_ss_show_bg']) ? true : false; 
            $this->_general['home_ss_image'] = $_POST['home_ss_image']; 
            $this->_general['home_ss_width'] = (int)$_POST['home_ss_width']; 
            $this->_general['home_ss_height'] = (int)$_POST['home_ss_height']; 
            $this->_general['home_ss_top'] = (int)$_POST['home_ss_top']; 
            $this->_general['home_ss_btns_show'] = isset($_POST['home_ss_btns_show']) ? true : false; 
            $this->_general['home_ss_page'] = $_POST['home_ss_page'];
            $this->_general['home_ss_use_page'] = isset($_POST['home_ss_use_page']) ? true : false;
            $this->_general['home_ss_sticked'] = isset($_POST['home_ss_sticked']) ? true : false; 
            
            
            
            if(!is_object($this->_general['home_ss_link']))
            {
               $this->_general['home_ss_link'] = new DCC_CCommonLink();    
            }
            $this->_general['home_ss_link']->_use = isset($_POST['home_ss_lobj_use']) ? true : false;             
            $this->_general['home_ss_link']->_link = $_POST['home_ss_lobj_link'];  
            $this->_general['home_ss_link']->_target = $_POST['home_ss_lobj_target'];
            $this->_general['home_ss_link']->_type = $_POST['home_ss_lobj_type'];
            $this->_general['home_ss_link']->_page = $_POST['home_ss_lobj_page'];             
                                                                                                                         
            update_option($this->_DBIDOPT_GENERAL, $this->_general);
            $this->_saved = true;
        } 
        
        # SPLASH SCREEN ADD BTN
        if(isset($_POST['general_addbtn_splashscreen_opt']))
        {                        
            $new_btn = new DCC_CCommonLink();
            if(!is_array($this->_general['home_ss_btns']))
            {
                $this->_general['home_ss_btns'] = array();
            }
            array_push($this->_general['home_ss_btns'], $new_btn);                                                                                                                                                   
                                                                                                                         
            update_option($this->_DBIDOPT_GENERAL, $this->_general);
            $this->_saved = true;
        }  
        
        # SPLASH SCREEN DELETE BUTTON          
        if(isset($_POST['general_delete_splashscreen_btn']))
        {                        
            $index = $_POST['index'];
            unset($this->_general['home_ss_btns'][$index]);
            $this->_general['home_ss_btns'] = array_values($this->_general['home_ss_btns']);                                                                                                                                                 
                                                                                                                         
            update_option($this->_DBIDOPT_GENERAL, $this->_general);
            $this->_saved = true;
        }  
        
        # SPLASH SCREEN SAVE BUTTON 
        if(isset($_POST['general_save_splashscreen_btn']))
        {            
            $index = $_POST['index'];    
            $this->_general['home_ss_btns'][$index]->_use = isset($_POST['use']) ? true : false;
            $this->_general['home_ss_btns'][$index]->_page = $_POST['page'];
            $this->_general['home_ss_btns'][$index]->_link = $_POST['link'];
            $this->_general['home_ss_btns'][$index]->_type = $_POST['type'];
            $this->_general['home_ss_btns'][$index]->_target = $_POST['target']; 
            $this->_general['home_ss_btns'][$index]->_text = $_POST['text'];
            
            update_option($this->_DBIDOPT_GENERAL, $this->_general);
            $this->_saved = true;                 
        }          
           
        # ANNONCMENT BAR
        if(isset($_POST['general_save_annbar']))
        {
            $this->_general['annbar_show'] = isset($_POST['annbar_show']) ? true : false; 
            $this->_general['annbar_height'] = $_POST['annbar_height'];
            $this->_general['annbar_content'] = $_POST['annbar_content'];
            $this->_general['annbar_transparent'] = isset($_POST['annbar_transparent']) ? true : false;
                        
            $this->_general['annbar_padding_left'] = $_POST['annbar_padding_left']; 
            $this->_general['annbar_padding_right'] = $_POST['annbar_padding_right']; 
            $this->_general['annbar_padding_top'] = $_POST['annbar_padding_top']; 
            $this->_general['annbar_padding_bottom'] = $_POST['annbar_padding_bottom']; 
            
            $this->_general['annbar_font_size'] = $_POST['annbar_font_size']; 
            $this->_general['annbar_font'] = $_POST['annbar_font']; 
            $this->_general['annbar_line_height'] = $_POST['annbar_line_height'];                                                             
            $this->_general['annbar_bg_image'] = $_POST['annbar_bg_image'];  
            $this->_general['annbar_bg_use'] = isset($_POST['annbar_bg_use']) ? true : false; 
            $this->_general['annbar_border_show'] = isset($_POST['annbar_border_show']) ? true : false; 
            
            $this->_general['annbar_font_color'] = $_POST['annbar_font_color']; 
            $this->_general['annbar_bg_color'] = $_POST['annbar_bg_color']; 
            $this->_general['annbar_border_color'] = $_POST['annbar_border_color']; 
                        
            update_option($this->_DBIDOPT_GENERAL, $this->_general);
            $this->_saved = true;  
        }  
        
        # PROGAMMABLE MEDIA BUTTONS
        if(isset($_POST['general_save_mbtns']))
        {
            $this->_general['mbtns_show'] = isset($_POST['mbtns_show']) ? true : false; 
                        
            update_option($this->_DBIDOPT_GENERAL, $this->_general);
            $this->_saved = true;  
        }                       
     
        if(isset($_POST['general_add_mbtns']))
        {
            $btn = new CPAddMediaBtn(); 
            if(!is_array($this->_general['mbtns_list']))
            {
                $this->_general['mbtns_list'] = array();
            }           
            array_push($this->_general['mbtns_list'], $btn);
                        
            update_option($this->_DBIDOPT_GENERAL, $this->_general);
            $this->_saved = true;  
        }  
        
        if(isset($_POST['general_delete_mbtn']))
        {
            $index = $_POST['index'];
            unset($this->_general['mbtns_list'][$index]);
            $this->_general['mbtns_list'] = array_values($this->_general['mbtns_list']);
            update_option($this->_DBIDOPT_GENERAL, $this->_general); 
            $this->_saved = true;                      
        }     
        
        if(isset($_POST['general_save_mbtn']))
        {
            $index = $_POST['index'];
            
            $this->_general['mbtns_list'][$index]->_name = $_POST['name'];
            $this->_general['mbtns_list'][$index]->_icon = $_POST['icon']; 
            $this->_general['mbtns_list'][$index]->_hide = isset($_POST['hide']) ? true : false;
            $this->_general['mbtns_list'][$index]->_start = $_POST['start'];
            $this->_general['mbtns_list'][$index]->_end = $_POST['end'];
            
            update_option($this->_DBIDOPT_GENERAL, $this->_general); 
            $this->_saved = true;                      
        }          
                
                                       
    }

    
    private function renderCMS()
    {       
        if($this->_saved)
        {                    
            echo '<span class="cms-saved-bar">Settings saved</span>';            
        }           
         
         // theme global core settings
         $out = '';
         $out .= '<div style="margin-top:0px;"></div>';
         $out .= '<a href="#theme-global"></a>';
         $out .= $this->getTopLink();
         $out .= '<h6 class="cms-h6">Theme global core settings</h6><hr class="cms-hr"/>'; 
         $out .= '<form action="#" method="post">';        
        
         // theme skin
 /*        $out .= '<span class="cms-span-10">Choose theme skin:</span><br />';
         $out .= '<select style="width:300px;" name="theme_skin">';
            foreach($this->_skins as $skin)
            {
                if($this->_general['theme_skin'] == $skin)
                {
                    $out .= '<option value="'.$skin.'" selected="selected">'.$skin.'</option>';     
                } else
                {
                    $out .= '<option value="'.$skin.'">'.$skin.'</option>';
                }
            }
         $out .= '</select><br /><br />';
  */       
         // theme slider
         $out .= '<span class="cms-span-10">Choose homepage slider:</span><br />';
         $out .= '<select style="width:300px;" name="theme_slider">';
            foreach($this->_sliderType as $name => $value)
            {
                if($this->_general['theme_slider'] == $value)
                {
                    $out .= '<option value="'.$value.'" selected="selected">'.$name.'</option>';    
                } else
                {
                    $out .= '<option value="'.$value.'">'.$name.'</option>';
                }
            }   
         $out .= '</select><br /><br />';

         // theme font
         $out .= '<span class="cms-span-10">Theme headings Google font:</span><br />';
         $fonts = GetDCCPInterface()->getAllGoogleFonts();
         $out .= '<select style="width:300px;" name="theme_font_google">';
            $count = count($fonts);
            for($i = 0; $i < $count; $i++)
            {
                if($this->_general['theme_font_google'] == $i)
                {
                    $out .= '<option value="'.$i.'" selected="selected">'.$fonts[$i]->_name.'</option>';    
                } else
                {
                    $out .= '<option value="'.$i.'">'.$fonts[$i]->_name.'</option>';
                }                
            }
         $out .= '</select><br /><br />';

         $out .= '<span class="cms-span-10">Theme headings Cufon font:</span><br />';
         $fonts = GetDCCPInterface()->getAllCufonFonts();
         $out .= '<select style="width:300px;" name="theme_font_cufon">';
            $count = count($fonts);
            for($i = 0; $i < $count; $i++)
            {
                if($this->_general['theme_font_cufon'] == $i)
                {
                    $out .= '<option value="'.$i.'" selected="selected">'.$fonts[$i]->_name.'</option>';    
                } else
                {
                    $out .= '<option value="'.$i.'">'.$fonts[$i]->_name.'</option>';
                }                
            }
         $out .= '</select><br /><br />';
        
         $out .= '<span class="cms-span-10">Select font type:</span><br />';
         $out .= '<input type="radio" '.$this->attrChecked($this->_general['theme_font_type'] == CMS_FONT_TYPE_CUFON).' value="'.CMS_FONT_TYPE_CUFON.'" name="theme_font_type" /> Cufon font<br />';         
         $out .= '<input type="radio" '.$this->attrChecked($this->_general['theme_font_type'] == CMS_FONT_TYPE_GOOGLE).' value="'.CMS_FONT_TYPE_GOOGLE.'" name="theme_font_type" /> Google font<br />';        
         $out .= '<br />';
        
         // main colors
         $out .= '<span class="cms-span-10">Theme main colors:</span><br />';
         $out .= '<input style="width:70px;text-align:center;" type="text" class="colorpicker {hash:true}" value="'.$this->_general['theme_color'].'" name="theme_color" /> Main color <br /><br />';
        
         // lightbox
         $out .= '<span class="cms-span-10">Choose lightbox mode:</span><br /><select style="width:300px;" name="theme_lightbox_mode">';         
         $lbmodes = Array('light_melegance', 'facebook', 'dark_rounded', 'dark_square', 'light_rounded', 'light_square');

         foreach($lbmodes as $lbmode)
         {
            $out .= '<option value="'.$lbmode.'" ';
            $out .= $this->_general['theme_lightbox_mode'] == $lbmode ? ' selected="selected" ' : '';
            $out .= '>';
            $out .= $lbmode.'</option>';
         }        
         $out .= '</select><br />';
         $out .= '<input type="checkbox" name="theme_lightbox_overlay_gallery" '.($this->_general['theme_lightbox_overlay_gallery'] ? ' checked="checked" ' : '').' /> Display lightbox overlay mini gallery<br />';             
         $out .= '<br />';
         
         // background image
         $out .= '<span class="cms-span-10">Theme background image settings:</span><br />'; 
         $out .= '<input type="text" style="width:600px;" id="theme_bg_url" name="theme_bg_url" value="'.$this->_general['theme_bg_url'].'" /> '; 
         $out .= '<input class="cms-upload upload_image_button" type="button" value="Select background" name="theme_bg_url" /><br /><br />';
         
         $out .= '<div style="width:160px;float:left;margin-right:20px;">';
             $out .= '<span class="cms-span-10">Background repeat:</span><br />';
             $out .= '<input type="radio" '.$this->attrChecked($this->_general['theme_bg_repeat'] == 'repeat').' value="repeat" name="theme_bg_repeat" /> Repeat<br />';         
             $out .= '<input type="radio" '.$this->attrChecked($this->_general['theme_bg_repeat'] == 'repeat-x').' value="repeat-x" name="theme_bg_repeat" /> Repeat X<br />';
             $out .= '<input type="radio" '.$this->attrChecked($this->_general['theme_bg_repeat'] == 'repeat-y').' value="repeat-y" name="theme_bg_repeat" /> Repeat Y<br />';
             $out .= '<input type="radio" '.$this->attrChecked($this->_general['theme_bg_repeat'] == 'no-repeat').' value="no-repeat" name="theme_bg_repeat" /> No repeat<br />';
         $out .= '</div>';
         
         $out .= '<div style="width:160px;float:left;margin-right:20px;">';
             $out .= '<span class="cms-span-10">Background X position:</span><br />';
             $out .= '<input type="radio" '.$this->attrChecked($this->_general['theme_bg_pos_x'] == 'left').' value="left" name="theme_bg_pos_x" /> Left<br />';         
             $out .= '<input type="radio" '.$this->attrChecked($this->_general['theme_bg_pos_x'] == 'center').' value="center" name="theme_bg_pos_x" /> Center<br />';
             $out .= '<input type="radio" '.$this->attrChecked($this->_general['theme_bg_pos_x'] == 'right').' value="right" name="theme_bg_pos_x" /> Right<br />';
         $out .= '</div>';         

         $out .= '<div style="width:160px;float:left;margin-right:20px;">';
             $out .= '<span class="cms-span-10">Background Y position:</span><br />';
             $out .= '<input type="radio" '.$this->attrChecked($this->_general['theme_bg_pos_y'] == 'top').' value="top" name="theme_bg_pos_y" /> Top<br />';         
             $out .= '<input type="radio" '.$this->attrChecked($this->_general['theme_bg_pos_y'] == 'center').' value="center" name="theme_bg_pos_y" /> Center<br />';
             $out .= '<input type="radio" '.$this->attrChecked($this->_general['theme_bg_pos_y'] == 'bottom').' value="bottom" name="theme_bg_pos_y" /> Bottom<br />';
         $out .= '</div>';  

         $out .= '<div style="width:160px;float:left;margin-right:20px;">';
             $out .= '<span class="cms-span-10">Background attachment:</span><br />';
             $out .= '<input type="radio" '.$this->attrChecked($this->_general['theme_bg_attachment'] == 'scroll').' value="scroll" name="theme_bg_attachment" /> Scroll<br />';         
             $out .= '<input type="radio" '.$this->attrChecked($this->_general['theme_bg_attachment'] == 'fixed').' value="fixed" name="theme_bg_attachment" /> Fixed<br />';
         $out .= '</div>'; 
         
         $out .= '<div style="clear:both;height:20px;"></div>';
         $out .= '<input type="checkbox" name="theme_bg_use" '.$this->attrChecked($this->_general['theme_bg_use']).' /> Use background image<br />'; 
         $out .= '<input type="checkbox" name="theme_bg_force" '.$this->attrChecked($this->_general['theme_bg_force']).' /> Force theme background on all pages<br /><br />';
        
         // background color
         $out .= '<span class="cms-span-10">Theme background color settings:</span><br />';         
         $out .= '<input style="width:70px;text-align:center;" type="text" class="colorpicker {hash:true}" value="'.$this->_general['theme_bg_color'].'" name="theme_bg_color" /> Background color <br />'; 
         $out .= '<input type="checkbox" name="theme_bg_color_use" '.$this->attrChecked($this->_general['theme_bg_color_use']).' /> Use background color<br />'; 
         $out .= '<input type="checkbox" name="theme_bg_color_force" '.$this->attrChecked($this->_general['theme_bg_color_force']).' /> Force background color on all pages<br /><br />';        

         // tip color
         $out .= '<span class="cms-span-10">Theme tip color settings:</span><br />';         
         $out .= '<input style="width:70px;text-align:center;" type="text" class="colorpicker {hash:true}" value="'.$this->_general['theme_tip_bgcolor'].'" name="theme_tip_bgcolor" /> Tip background color <br />'; 
         $out .= '<input style="width:70px;text-align:center;" type="text" class="colorpicker {hash:true}" value="'.$this->_general['theme_tip_color'].'" name="theme_tip_color" /> Tip text color <br />';
         $out .= '<input style="width:70px;text-align:center;" type="text" class="colorpicker {hash:true}" value="'.$this->_general['theme_tip_bordercolor'].'" name="theme_tip_bordercolor" /> Tip border color <br /><br />';   
        
         // favicon
         $out .= '<span class="cms-span-10">Theme favicon 16x16px ICO file:</span><br />';
         $out .= '<input type="text" style="width:600px;" id="theme_favicon_url" name="theme_favicon_url" value="'.$this->_general['theme_favicon_url'].'" /> '; 
         $out .= '<input class="cms-upload upload_image_button" type="button" value="Upload icon" name="theme_favicon_url" /><br />';
         $out .= '<input type="checkbox" name="theme_favicon_use" '.$this->attrChecked($this->_general['theme_favicon_use']).' /> Show favicon <br /><br />';
         
         
         $out .= '<span class="cms-span-10">Top and bottom empty space:</span><br />'; 
         $out .= '<input type="text" style="width:60px;text-align:center;" name="theme_top_space_height" value="'.$this->_general['theme_top_space_height'].'" /> Top empty space height in pixels <br />'; 
         $out .= '<input type="text" style="width:60px;text-align:center;" name="theme_bottom_space_height" value="'.$this->_general['theme_bottom_space_height'].'" /> Bottom empty space height in pixels <br />';
         $out .= '<br />';
         $out .= '<input type="checkbox" name="theme_show_client_panel" '.($this->_general['theme_show_client_panel'] ? ' checked="checked" ' : '').' /> Show client panel<br />';
         $out .= '<input type="checkbox" name="theme_disable_autoformating" '.($this->_general['theme_disable_autoformating'] ? ' checked="checked" ' : '').' /> Disable paragraph autoformatting<br />';          
         $out .= '<input type="checkbox" name="theme_disable_autotexturize" '.($this->_general['theme_disable_autotexturize'] ? ' checked="checked" ' : '').' /> Disable texturize formatting<br />'; 
         $out .= '<input type="checkbox" name="theme_authorize_comment" '.$this->attrChecked($this->_general['theme_authorize_comment']).' /> Enable code authorization for comments<br />'; 
         $out .= '<input type="checkbox" name="theme_authorize_contact" '.$this->attrChecked($this->_general['theme_authorize_contact']).' /> Enable code authorization for contact forms<br />';
         
         
         $out .= '<div style="height:20px;"></div>';
         $out .= '<input type="submit" class="cms-submit" name="general_save_theme" value="Save" /> ';         
         
         $out .= '</form>'; 
         echo $out;                
         
       
         // splash screen
         $out = '';   
         $out .= '<div style="height:30px;"></div>';
         $out .= '<a name="splash-screen"></a>';                             
         $out .= $this->getTopLink();
         $out .= '<h6 class="cms-h6">Theme splash screen</h6><hr class="cms-hr"/>';
          
         $out .= '<form action="#splash-screen" method="post" >';                                                                                                   
         
         $out .= '<input type="checkbox" '.$this->attrChecked($this->_general['home_ss_show']).' name="home_ss_show" /> Display theme splash screen<br />';         
         $out .= '<input type="checkbox" '.$this->attrChecked($this->_general['home_ss_show_bg']).' name="home_ss_show_bg" /> Display splash screen background<br />';
         $out .= '<input type="checkbox" '.$this->attrChecked($this->_general['home_ss_btns_show']).' name="home_ss_btns_show" /> Show buttons<br />';  
         $out .= '<input type="checkbox" '.$this->attrChecked($this->_general['home_ss_sticked']).' name="home_ss_sticked" /> Show with next refresh after close<br />';
         $out .= '<input type="checkbox" '.$this->attrChecked($this->_general['home_ss_use_page']).' name="home_ss_use_page" /> Display splash screen only on selected page<br /><br />'; 
         
         
         
         $out .= '<span class="cms-span-10">Select page for splash screen when you want display screen only on this single page:</span><br />'; 
         $out .= $this->selectCtrlPagesList($this->_general['home_ss_page'], 'home_ss_page', 320);
         $out .= '<br /><br />';         
         
         $out .= '<span class="cms-span-10">Screen image:</span><br />';
         $out .= '<input type="text" style="width:600px;" id="home_ss_image" name="home_ss_image" value="'.$this->_general['home_ss_image'].'" /> '; 
         $out .= '<input class="cms-upload upload_image_button" type="button" value="Upload image" name="home_ss_image" /><br /><br />';              
         
         $out .= '<span class="cms-span-10">Screen size:</span><br />';
         $out .= '<input type="text" style="width:60px;text-align:center;" name="home_ss_width" value="'.$this->_general['home_ss_width'].'" /> Screen width<br />';
         $out .= '<input type="text" style="width:60px;text-align:center;" name="home_ss_height" value="'.$this->_general['home_ss_height'].'" /> Screen height<br /><br />'; 

         $out .= '<span class="cms-span-10">Position:</span><br />';
         $out .= '<input type="text" style="width:60px;text-align:center;" name="home_ss_top" value="'.$this->_general['home_ss_top'].'" /> Screen top position in pixels<br /><br />';
         
         
         $out .= '<span class="cms-span-10">Link assigned to image:</span><br />';
         $lobj = $this->_general['home_ss_link'];
         if(!is_object($lobj))
         {
            $lobj = new DCC_CCommonLink();    
         }
         $out .= '<input type="checkbox" '.$this->attrChecked($lobj->_use).' name="home_ss_lobj_use" /> Use link<br /><br />';
         
         $out .= '<span class="cms-span-10">Select page for link:</span><br />'; 
         $out .= $this->selectCtrlPagesList($lobj->_page, 'home_ss_lobj_page', 320);
         $out .= '<br /><br />';
         
         $out .= '<span class="cms-span-10">Manually link:</span><br />';
         $out .= '<input type="text" style="width:600px;" name="home_ss_lobj_link" value="'.$lobj->_link.'" /><br /><br />';
         
         $out .= '<span class="cms-span-10">Link target:</span><br />'; 
         $out .= '<input type="radio" '.$this->attrChecked($lobj->_target == CMS_LINK_TARGET_SELF).' value="'.CMS_LINK_TARGET_SELF.'" name="home_ss_lobj_target" /> _self<br />'; 
         $out .= '<input type="radio" '.$this->attrChecked($lobj->_target == CMS_LINK_TARGET_BLANK).' value="'.CMS_LINK_TARGET_BLANK.'" name="home_ss_lobj_target" /> _blank <br /><br />';         

         $out .= '<span class="cms-span-10">Link type:</span><br />'; 
         $out .= '<input type="radio" '.$this->attrChecked($lobj->_type == CMS_LINK_TYPE_PAGE).' value="'.CMS_LINK_TYPE_PAGE.'" name="home_ss_lobj_type" /> Use pge<br />'; 
         $out .= '<input type="radio" '.$this->attrChecked($lobj->_type == CMS_LINK_TYPE_MANUALLY).' value="'.CMS_LINK_TYPE_MANUALLY.'" name="home_ss_lobj_type" /> Use manually link <br />'; 
         
         $out .= '<div style="height:20px;"></div>';
         $out .= '<input class="cms-submit" type="submit" value="Save" name="general_save_splashscreen_opt" /> ';
         $out .= '<input class="cms-submit" type="submit" value="Add button to splash screen" name="general_addbtn_splashscreen_opt" />';
         $out .= '</form>';         
         
         $count = count($this->_general['home_ss_btns']);

         if($count)
         {
            $out .= '<br /><span class="cms-span-10">Edit splash screen buttons:</span><br />';  
            $out .= '<table>';        
            $out .= '<thead>
                    <tr>
                        <th style="padding:0px 3px 0px 3px;">Show</th>
                        <th style="padding:0px 3px 0px 3px;">Name</th>
                        <th style="padding:0px 3px 0px 3px;">Target</th> 
                        <th style="padding:0px 3px 0px 3px;">Page</th>
                        <th style="padding:0px 3px 0px 3px;">Link</th>
                        <th style="padding:0px 3px 0px 3px;">Type</th>
                                               
                        
                        <th style="padding:0px 3px 0px 3px;">Save</th>
                        <th style="padding:0px 3px 0px 3px;">Delete</th>                        
                    </tr>
                  </thead>';          
          
            $counter = 0;
            foreach($this->_general['home_ss_btns'] as $btn)
            {
                $out .= '<tr>';
                
                // use
                $out .= '              
                    <td style="text-align:center;"><form action="#splash-screen" method="POST">
                    <input type="checkbox" '.$this->attrChecked($btn->_use).' name="use" />  
                    <input type="hidden" name="index" value="'.$counter.'" />';
                $out .= '</td>';                 
                
                // text
                $out .= '<td style="text-align:center;padding-left:5px;padding-right:5px;">';
                    $out .= '<input type="text" style="width:100px;" name="text" value="'.$btn->_text.'" /> '; 
                $out .= '</td>';

                // target
                $out .= '              
                    <td style="text-align:center;padding-left:5px;padding-right:5px;">
                    <input type="radio" '.$this->attrChecked($btn->_target == CMS_LINK_TARGET_SELF).' value="'.CMS_LINK_TARGET_SELF.'" name="target" /> _self 
                    <input type="radio" '.$this->attrChecked($btn->_target == CMS_LINK_TARGET_BLANK).' value="'.CMS_LINK_TARGET_BLANK.'" name="target" /> _blank ';
                $out .= '</td>'; 
                
                // page
                $out .= '<td>';    
                    $out .= $this->selectCtrlPagesList($btn->_page, 'page', 140);                                           
                $out .= '</td>';
                 
                // link
                $out .= '<td style="padding-left:5px;padding-right:5px;">';    
                    $out .= '<input type="text" style="width:140px;" name="link" value="'.$btn->_link.'" /> ';                                            
                $out .= '</td>';                

                // type
                $out .= '              
                    <td style="text-align:center;padding-left:5px;padding-right:5px;">
                    <input type="radio" '.$this->attrChecked($btn->_type == CMS_LINK_TYPE_PAGE).' value="'.CMS_LINK_TYPE_PAGE.'" name="type" /> Page 
                    <input type="radio" '.$this->attrChecked($btn->_type == CMS_LINK_TYPE_MANUALLY).' value="'.CMS_LINK_TYPE_MANUALLY.'" name="type" /> Link ';
                $out .= '</td>';    
                
                // save / delete 
                $out .= '<td><input  class="cms-submit" type="submit" value="Save" name="general_save_splashscreen_btn" /></td> 
                    <td><input  onclick="return confirm('."'Delete this button?'".')" class="cms-submit-delete" type="submit" value="Delete" name="general_delete_splashscreen_btn" /></form></td>';                                 
                $out .= '</tr>';                
                
                $counter++;                
            }
            $out .= '</table>';
         }
                       
 
         echo $out;           
        
        
         // announcement bar
         $out = '';
         $out .= '<a name="annbar"></a>';
         $out .= '<div style="margin-top:50px;"></div>';
         $out .= $this->getTopLink();         
         $out .= '<h6 class="cms-h6">Announcement bar</h6><hr class="cms-hr"/>';
         $out .= '<form action="#annbar" method="post" >';
         
         $out .= '<span class="cms-span-10">Content:</span><br />';                                                                                                                                        
         $out .= '<textarea style="font-size:11px;padding:8px;width:700px;max-width:600px;height:160px;color:#222222;" name="annbar_content" >'.stripcslashes($this->_general['annbar_content']).'</textarea><br /><br />';         
         
         $out .= '<span class="cms-span-10">Other settings:</span><br />'; 
         $out .= '<input type="text" style="width:60px;text-align:center;" value="'.$this->_general['annbar_height'].'" name="annbar_height" /> Bar height (px)<br /><br />'; 
         
         $out .= 'Paddding (px) <input type="text" style="width:60px;text-align:center;" value="'.$this->_general['annbar_padding_top'].'" name="annbar_padding_top" /> Top '; 
         $out .= '<input type="text" style="width:60px;text-align:center;" value="'.$this->_general['annbar_padding_right'].'" name="annbar_padding_right" /> Right '; 
         $out .= '<input type="text" style="width:60px;text-align:center;" value="'.$this->_general['annbar_padding_bottom'].'" name="annbar_padding_bottom" /> Bottom '; 
         $out .= '<input type="text" style="width:60px;text-align:center;" value="'.$this->_general['annbar_padding_left'].'" name="annbar_padding_left" /> Left <br /><br />';                   
        
         $out .= '<input type="text" style="width:600px;" id="annbar_bg_image" name="annbar_bg_image" value="'.$this->_general['annbar_bg_image'].'" /> '; 
         $out .= '<input class="cms-upload upload_image_button" type="button" value="Upload background image" name="annbar_bg_image" /><br /><br />';        
         
         $out .= '<input type="text" style="width:60px;text-align:center;" value="'.$this->_general['annbar_font_size'].'" name="annbar_font_size" /> Font size (px)<br />'; 
         $out .= '<input type="text" style="width:60px;text-align:center;" value="'.$this->_general['annbar_line_height'].'" name="annbar_line_height" /> Line height (px)<br /><br />';
         
         $out .= '<input style="width:70px;text-align:center;" type="text" class="colorpicker {hash:true}" value="'.$this->_general['annbar_bg_color'].'" name="annbar_bg_color" /> Background color <br />';
         $out .= '<input style="width:70px;text-align:center;" type="text" class="colorpicker {hash:true}" value="'.$this->_general['annbar_border_color'].'" name="annbar_border_color" /> Border color <br />';
         $out .= '<input style="width:70px;text-align:center;" type="text" class="colorpicker {hash:true}" value="'.$this->_general['annbar_font_color'].'" name="annbar_font_color" /> Font color <br /><br />';
         
         $fonts_array = array('Helvetica', 'Arial', 'Tahoma', 'Trebuchet');
         $temp = '';
         $temp .= '<select style="width:120px;" name="annbar_font">';
            foreach($fonts_array as $font)
            {
                $temp .= '<option '.$this->attrSelected($font == $this->_general['annbar_font']).' value="'.$font.'">'.$font.'</option>';
            }
         $temp .= '</select> Select font<br /><br />';
         
         $out .= $temp;
         
         $out .= '<input type="checkbox" '.$this->attrChecked($this->_general['annbar_show']).' name="annbar_show" /> Display announcement bar <br />';
         $out .= '<input type="checkbox" '.$this->attrChecked($this->_general['annbar_transparent']).' name="annbar_transparent" /> Transparent background <br />';   
         $out .= '<input type="checkbox" '.$this->attrChecked($this->_general['annbar_border_show']).' name="annbar_border_show" /> Show border <br />';
         $out .= '<input type="checkbox" '.$this->attrChecked($this->_general['annbar_bg_use']).' name="annbar_bg_use" /> Use background image bar <br />';
         
         $out .= '<div style="margin-top:20px;"></div>';
         $out .= '<input class="cms-submit" type="submit" value="Save" name="general_save_annbar" /><br />';                                   
         $out .= '</form>';            
         echo $out;        

         // programmable media buttons 
         $out = '';
         $out .= '<a name="media-buttons"></a>';                  
         $out .= '<div style="margin-top:50px;"></div>';
         $out .= $this->getTopLink();         
         $out .= '<h6 class="cms-h6">Programmable media buttons</h6><hr class="cms-hr"/>';
         $out .= '<form action="#media-buttons" method="post" >';                                                                                                                                        
         $out .= '<input type="checkbox" name="mbtns_show" '.$this->attrChecked($this->_general['mbtns_show']).' /> Show programmable media buttons<br />';
         
         $out .= '<div style="margin-top:20px;"></div>';
         $out .= '<input class="cms-submit" type="submit" value="Save" name="general_save_mbtns" /> '; 
         $out .= '<input class="cms-submit" type="submit" value="Add new media button" name="general_add_mbtns" />';                                  
         $out .= '</form>';
         echo $out;    
        
         $count_btns = count($this->_general['mbtns_list']);
         if($count_btns > 0)
         {
            $out = '';     
            $out .= '<div style="height:20px;"></div>';       
            
            $counter = 0;       
            foreach($this->_general['mbtns_list'] as $item)
            {                
                if($counter > 0) { $out .= '<div style="height:25px;"></div>'; }
                  
                $out .= '<h6 class="cms-h6s">Button #'.($counter+1).': '.$item->_name.'</h6>';
                
                $out .= '<form action="#media-buttons" method="POST">';                
                    $out .= '<input type="hidden" name="index" value="'.$counter.'" />'; 

                    // icon and nanme
                    $out .= '<div style="width:220px;float:left;">';
                        $out .= '<span class="cms-span-10">Name:</span><br />';
                        $out .= '<input type="text" style="width:180px;" name="name" value="'.$item->_name.'" /><br />';                                                 
                        $out .= '<span class="cms-span-10">Icon URL (png, gif):</span><br />';
                        $out .= '<input type="text" style="width:180px;" id="image_mbtn_'.$counter.'" name="icon" value="'.$item->_icon.'" /> ';
                        if($item->_icon != '') { $out .= '<img src="'.$item->_icon.'" alt="preview" />'; }
                        $out .= '<br /><br />';               
                        $out .= '<input type="checkbox" '.($item->_hide ? ' checked="checked" ' : '').' name="hide" /> <span style="font-size:11px;">Hide this button</span><br />';
                                                                   
                    $out .= '</div>';
                
                    // before                    
                    $out .= '<div style="width:330px;float:left;">';
                        $out .= '<span class="cms-span-10">Text before:</span><br />';  
                        $out .= '<textarea style="width:310px;height:90px;font-size:11px;" name="start">'.stripcslashes($item->_start).'</textarea> ';                                                           
                    $out .= '</div>';
                    
                    // after   
                    $out .= '<div style="width:320px;float:left;">';
                        $out .= '<span class="cms-span-10">Text after:</span><br />';
                        $out .= '<textarea style="width:310px;height:90px;font-size:11px;" name="end">'.stripcslashes($item->_end).'</textarea> '; 
                    $out .= '</div>';  
                                                                                                           
                    $out .= '<div style="clear:both;height:1px;"></div>'; 
                
                    // save / delete
                    $out .= '<div style="height:20px;"></div>';  
                    $out .= '<input  class="cms-submit" type="submit" value="Save" name="general_save_mbtn" /> 
                        <input  onclick="return confirm('."'Delete button?'".')" class="cms-submit-delete" type="submit" value="Delete" name="general_delete_mbtn" /> ';
                    $out .= '<input class="cms-upload upload_image_button" type="button" value="Upload Icon 16x16" name="image_mbtn_'.$counter.'" />';              
                
                $out .= '</form>';                                                                 
                      
                
                $counter++;
            }                
            echo $out;
         }          
        
         // social communities 
         $out = '';
         $out .= '<a name="social"></a>';                  
         $out .= '<div style="margin-top:50px;"></div>';
         $out .= $this->getTopLink();         
         $out .= '<h6 class="cms-h6">Social communities</h6><hr class="cms-hr"/>';
         $out .= '<form action="#social" method="post" >';                                                                                                                                        
         $out .= '<input type="checkbox" name="social_show_facebook" '.($this->_general['social_show_facebook'] ? ' checked="checked" ' : '').' /> Show facebook share control in post single page<br />';
         $out .= '<input type="checkbox" name="social_show_twitter" '.($this->_general['social_show_twitter'] ? ' checked="checked" ' : '').' /> Show twitter share control in post single page<br />';
         $out .= '<input type="checkbox" name="social_show_google" '.($this->_general['social_show_google'] ? ' checked="checked" ' : '').' /> Show google plus share control in post single page<br />'; 
         $out .= '<input type="checkbox" name="social_show_facebook_thumb" '.($this->_general['social_show_facebook_thumb'] ? ' checked="checked" ' : '').' /> Add thumb to facebook sharing<br />';
         $out .= '<input type="checkbox" name="social_use_thumb" '.($this->_general['social_use_thumb'] ? ' checked="checked" ' : '').' /> Use for facebook below thumb instead thumb from post settings<br />';  
                                          
         $out .= '<br /><span class="cms-span-10">Facebook alternative thumb image:</span><br />';
        
         if($this->_general['social_thumb'] != '')
         {
            $out .= '<img src="'.dcf_getTimThumbURL($this->_general['social_thumb'], 200, 200).'" alt="image" />';    
         } else
         {
            $out .= '<div style="width:200px;height:200px;border:1px solid #DDD;line-height:200px;text-align:center;">Place for image</div>';
         }
        
         $out .= '<input style="width:100%;" id="social_thumb" type="text" name="social_thumb" value="'.$this->_general['social_thumb'].'" /><br />';              
         
         $out .= '<div style="margin-top:20px;"></div>';
         $out .= '<input class="cms-submit" type="submit" value="Save" name="general_save_social" /><br />';                                   
         $out .= '</form>';
         echo $out;                                      
         
         // breadcrumb
         $out = '';
         $out .= '<a name="breadcrumb"></a>';                  
         $out .= '<div style="margin-top:50px;"></div>'; 
         $out .= $this->getTopLink();        
         $out .= '<h6 class="cms-h6">Breadcrumb navigation settings</h6><hr class="cms-hr"/>';
         $out .= '<form action="#breadcrumb" method="post" >';
         
         $out .= '<input type="text" style="width:300px;text-align:left;" name="breadcrumb_before" value="'.$this->_general['breadcrumb_before'].'" /> Text before breadcrumb navigation  <br />';                                                                                                                                        
         $out .= '<input type="text" style="width:300px;text-align:left;" name="breadcrumb_name" value="'.$this->_general['breadcrumb_name'].'" /> Alternative site name for breadcrumb navigation <br />';
         $out .= '<input type="checkbox" name="breadcrumb_force_name" '.$this->attrChecked($this->_general['breadcrumb_force_name']).' /> Use alternative name<br />';
         
         $out .= '<div style="margin-top:20px;"></div>';
         $out .= '<input class="cms-submit" type="submit" value="Save" name="general_breadcrumb_settings" /><br />';                                   
         $out .= '</form>';
         echo $out;                          
              
         // header
         $out = '';
         $out .= '<a name="header"></a>';                  
         $out .= '<div style="margin-top:50px;"></div>'; 
         $out .= $this->getTopLink();        
         $out .= '<h6 class="cms-h6">Header settings</h6><hr class="cms-hr"/>';
         $out .= '<form action="#header" method="post" >';                                                                                                                                        
         $out .= '<input type="text" style="width:60px;text-align:center;" name="header_height" value="'.$this->_general['header_height'].'" /> Header height in px <br />';

         $out .= '<div style="margin-top:20px;"></div>';
         $out .= '<input class="cms-submit" type="submit" value="Save" name="general_save_header" /><br />';                                   
         $out .= '</form>';
         echo $out;  

         // header menu
         $out = '';
         $out .= '<a name="header-menu"></a>';                  
         $out .= '<div style="margin-top:50px;"></div>';  
         $out .= $this->getTopLink();       
         $out .= '<h6 class="cms-h6">Header menu</h6><hr class="cms-hr"/>';
         $out .= '<form action="#header-menu" method="post" >';                                                                                                                                        
         $out .= '<input type="checkbox" name="hmenu_show" '.$this->attrChecked($this->_general['hmenu_show']).' /> Show header menu <br />';
         $out .= '<input type="checkbox" name="hmenu_bg_show" '.$this->attrChecked($this->_general['hmenu_bg_show']).' /> Use background color option <br /><br />';
         
         $out .= '<input type="text" style="width:60px;text-align:center;" name="hmenu_bg_opacity" value="'.$this->_general['hmenu_bg_opacity'].'" /> Header menu opacity (0 - 100) <br /><br />';  
          
         $out .= '<input style="width:70px;text-align:center;" type="text" class="colorpicker {hash:true}" value="'.$this->_general['hmenu_bg_color'].'" name="hmenu_bg_color" /> Background color <br />';         
         $out .= '<input style="width:70px;text-align:center;" type="text" class="colorpicker {hash:true}" value="'.$this->_general['hmenu_color'].'" name="hmenu_color" /> Link color <br />'; 
         $out .= '<input style="width:70px;text-align:center;" type="text" class="colorpicker {hash:true}" value="'.$this->_general['hmenu_hover_color'].'" name="hmenu_hover_color" /> Link hover color <br />'; 
         
         $out .= '<div style="margin-top:20px;"></div>';
         $out .= '<input class="cms-submit" type="submit" value="Save" name="general_save_header_menu" /><br />';                                   
         $out .= '</form>';
         echo $out;           
         
         // header icons
         $out = '';
         $out .= '<a name="header-icons"></a>';                  
         $out .= '<div style="margin-top:50px;"></div>'; 
         $out .= $this->getTopLink();        
         $out .= '<h6 class="cms-h6">Header icons settings</h6><hr class="cms-hr"/>';
         $out .= '<form action="#header-icons" method="post" >';                                                                                                                                        
         $out .= '<input type="checkbox" '.($this->_general['hicons_show'] ? ' checked="checked" ' : '').' name="hicons_show" /> Show header icons<br />'; 
         $out .= '<input type="checkbox" '.($this->_general['hicons_show_tip'] ? ' checked="checked" ' : '').' name="hicons_show_tip" /> Show icons description tip<br /><br />';
         $out .= '<input type="text" style="width:60px;text-align:center;" name="hicons_size" value="'.$this->_general['hicons_size'].'" /> Icon width and height in px <br />'; 
         $out .= '<input type="text" style="width:60px;text-align:center;" name="hicons_width" value="'.$this->_general['hicons_width'].'" /> Icons container width in px <br /><br />';
                  
         $out .= '<input type="text" style="width:60px;text-align:center;" value="'.$this->_general['hicons_left'].'" name="hicons_left" /> Left position in px<br />';
         $out .= '<input type="text" style="width:60px;text-align:center;" value="'.$this->_general['hicons_right'].'" name="hicons_right" /> Right position in px<br />';
         $out .= '<input type="text" style="width:60px;text-align:center;" value="'.$this->_general['hicons_top'].'" name="hicons_top" /> Top position in px<br />';
         $out .= '<input type="text" style="width:60px;text-align:center;" value="'.$this->_general['hicons_bottom'].'" name="hicons_bottom" /> Bottom position in px<br /><br />';
         
         $out .= '<input type="checkbox" '.$this->attrChecked($this->_general['hicons_use_right']).' name="hicons_use_right" /> Use right position<br />';
         $out .= '<input type="checkbox" '.$this->attrChecked($this->_general['hicons_use_bottom']).' name="hicons_use_bottom" /> Use bottom position<br />';                  
                  
         $out .= '<div style="margin-top:20px;"></div>';
         $out .= '<input class="cms-submit" type="submit" value="Save" name="general_save_header_icons" /> ';                                   
         $out .= '<input class="cms-submit" type="submit" value="Add new header icon" name="general_add_hicon" /><br />'; 
         $out .= '</form>';
         echo $out; 
         
         $count_icons = count($this->_icons);
         if($count_icons > 0)
         {
            $out = '';     
            $out .= '<div style="height:20px;"></div>';       
            $out .= '<table>';        
            $out .= '<thead>
                    <tr>
                        <th style="padding:0px 3px 0px 3px;">Hide</th>
                        <th style="padding:0px 3px 0px 3px;">Preview</th>
                        <th style="padding:0px 3px 0px 3px;">Image url</th>
                        <th style="padding:0px 3px 0px 3px;">Hovered</th>
                        <th style="padding:0px 3px 0px 3px;">Link</th>
                        <th style="padding:0px 3px 0px 3px;">Description</th>
                        
                        <th style="padding:0px 3px 0px 3px;">Save</th>
                        <th style="padding:0px 3px 0px 3px;">Delete</th>
                        <th style="padding:0px 3px 0px 3px;">Upload</th>
                        <th style="padding:0px 3px 0px 3px;">Up</th>
                        <th style="padding:0px 3px 0px 3px;">Down</th>                          
                    </tr>
                  </thead>';
            
            $counter = 0;       
            foreach($this->_icons as $ico)
            {                
                $out .= '<tr>';
                
                // hide icon
                $out .= '              
                    <td style="text-align:center;"><form action="#header-icons" method="POST">
                    <input type="checkbox" '.($ico->_hide ? ' checked="checked" ' : '').' name="hide" />  
                    <input type="hidden" name="index" value="'.$counter.'" />';
                $out .= '</td>';                 
                
                // preview icon
                $out .= '<td style="text-align:center;">';
                    if($ico->_image != '') { $out .= '<img src="'.$ico->_image.'" alt="preview" />'; } else { $out .= '-';}
                $out .= '</td>';
                
                // image url
                $out .= '<td>';    
                    $out .= '<input type="text" style="width:100px;" id="image_hi_'.$counter.'" name="image" value="'.$ico->_image.'" /> ';                                            
                $out .= '</td>';

                // hovered
                $out .= '              
                    <td style="text-align:center;">
                    <input type="checkbox" '.($ico->_hovered ? ' checked="checked" ' : '').' name="hovered" />';
                $out .= '</td>';  
                
                // link url
                $out .= '<td>';    
                    $out .= '<input type="text" style="width:120px;" name="url" value="'.$ico->_url.'" /> ';                                            
                $out .= '</td>';                

                // desc
                $out .= '<td>';    
                    $out .= '<input type="text" style="width:120px;" name="desc" value="'.$ico->_desc.'" /> ';                                            
                $out .= '</td>';    
                
                // save / delete 
                $out .= '<td><input  class="cms-submit" type="submit" value="Save" name="general_save_hicon" /></td> 
                    <td><input  onclick="return confirm('."'Delete icon?'".')" class="cms-submit-delete" type="submit" value="Delete" name="general_delete_hicon" /></td>';
                $out .= '<td><input class="cms-upload upload_image_button" type="button" value="Upload" name="image_hi_'.$counter.'" /></td>';     
                $out .= '<td><input  class="cms-submit" type="submit" value="Up" name="general_moveup_hicon" /></td>
                <td><input  class="cms-submit" type="submit" value="Down" name="general_movedown_hicon" /></form></td>';                                
                $out .= '</tr>';                
                
                $counter++;
            }    
            
            $out .= '</table>';
            $out .= '<div style="height:10px;"></div>';
            $out .= '<span class="cms-span-10">If you have checked hovered option, an icon image should be constructed from two icons - main icon at the top and hover version right below (see theme icon images).<br /> If icon size is set to 36x36px the hovered version of icon image should be 36x72px.</span><br />'; 
            echo $out;
         }   
         
         // blog
         $out = '';
         $out .= '<a name="blog"></a>';                  
         $out .= '<div style="margin-top:50px;"></div>';  
         $out .= $this->getTopLink();       
         $out .= '<h6 class="cms-h6">Blog and blog posts settings</h6><hr class="cms-hr"/>';
         $out .= '<form action="#blog" method="post" >';                                                                                                                                        
         $out .= '<input type="checkbox" name="blog_show_tags_for_medium" '.$this->attrChecked($this->_general['blog_show_tags_for_medium']).' /> Show tags for post medium layout<br />'; 
         $out .= '<input type="checkbox" name="blog_show_tags_for_medium_home" '.$this->attrChecked($this->_general['blog_show_tags_for_medium_home']).' /> Show tags for post medium layout on homepage<br />'; 
         $out .= '<input type="checkbox" name="blog_show_time_for_medium" '.$this->attrChecked($this->_general['blog_show_time_for_medium']).' /> Show time from post action in post medium layout<br />';
         $out .= '<input type="checkbox" name="blog_show_time_for_small" '.$this->attrChecked($this->_general['blog_show_time_for_small']).' /> Show time from post action in post small layout<br />';               

         $out .= '<input type="checkbox" name="blog_show_preview_for_small" '.$this->attrChecked($this->_general['blog_show_preview_for_small']).' /> Show popup image preview in post small layout<br />';
         $out .= '<input type="checkbox" name="blog_show_preview_for_medium" '.$this->attrChecked($this->_general['blog_show_preview_for_medium']).' /> Show popup image preview in post medium layout<br /><br />';
         
         $out .= '<input type="text" style="width:60px;text-align:center;" name="blog_related_rows" value="'.$this->_general['blog_related_rows'].'" /> Number of related posts rows<br />';
         
         $out .= '<br />';
         $out .= '<span class="cms-span-10">Blog post single page options:</span><br />';
         $out .= '<input type="checkbox" name="blog_show_author_info" '.$this->attrChecked($this->_general['blog_show_author_info']).' /> Show author info<br />';
         $out .= '<input type="checkbox" name="blog_show_related" '.$this->attrChecked($this->_general['blog_show_related']).' /> Show related posts<br />';      
         
         $out .= '<div style="margin-top:20px;"></div>';
         $out .= '<input class="cms-submit" type="submit" value="Save" name="general_blog_settings" /><br />';                                   
         $out .= '</form>';
         echo $out;               
                                             
                                                                   
         // logo
         $out = '';
         $out .= '<a name="logo"></a>';                  
         $out .= '<div style="margin-top:50px;"></div>';
         $out .= $this->getTopLink();         
         $out .= '<h6 class="cms-h6">Logo settings</h6><hr class="cms-hr"/>';
         $out .= '<form action="#logo" method="post" >';                                                                                                                                        
         $out .= '<input type="text" style="width:60px;text-align:center;" name="logo_width" value="'.$this->_general['logo_width'].'" /> Logo width in pixels <br />';
         $out .= '<input type="text" style="width:60px;text-align:center;" name="logo_height" value="'.$this->_general['logo_height'].'" /> Logo height in pixels <br />';
         $out .= '<input type="text" style="width:60px;text-align:center;" name="logo_left" value="'.$this->_general['logo_left'].'" /> Logo X position in pixels from left<br />';
         $out .= '<input type="text" style="width:60px;text-align:center;" name="logo_top" value="'.$this->_general['logo_top'].'" /> Logo Y postion in pixels from top<br />'; 
         $out .= '<input type="text" style="width:60px;text-align:center;" name="logo_right" value="'.$this->_general['logo_right'].'" /> Logo X position in pixels from right<br />';
         $out .= '<input type="text" style="width:60px;text-align:center;" name="logo_bottom" value="'.$this->_general['logo_bottom'].'" /> Logo Y postion in pixels from bottom<br /><br />'; 
         $out .= '<input type="checkbox" name="logo_useright" '.($this->_general['logo_useright'] ? ' checked="checked" ' : '').' /> Use right position<br />';
         $out .= '<input type="checkbox" name="logo_usebottom" '.($this->_general['logo_usebottom'] ? ' checked="checked" ' : '').' /> Use bottom position<br /><br />';
         
         $out .= '<input type="text" style="width:600px;" id="logo_path" name="logo_path" value="'.$this->_general['logo_path'].'" /> ';
         $out .= '<input class="cms-upload upload_image_button" type="button" value="Upload Image" name="logo_path" /> <br />';
         $out .= '<div style="margin-top:20px;"></div>';
         $out .= '<input class="cms-submit" type="submit" value="Save" name="general_save_logo" /><br />';                                   
         $out .= '</form>';
         echo $out;  

         // 404 page
         $out = '';
         $out .= '<a name="404page"></a>';                  
         $out .= '<div style="margin-top:50px;"></div>';
         $out .= $this->getTopLink();         
         $out .= '<h6 class="cms-h6">404 page</h6><hr class="cms-hr"/>';
         $out .= '<form action="#404page" method="post" >'; 
         
         $out .= '<span class="cms-span-10">Title for page:</span><br />';                                                                                                                                           
         $out .= '<input type="text" style="width:600px;" name="404_title" value="'.stripcslashes($this->_general['404_title']).'" /><br /><br /> ';

         $out .= '<span class="cms-span-10">Description:</span><br />';                                                                                                                                           
         $out .= '<textarea type="text" style="width:600px;height:80px;font-size:11px;padding:8px;" name="404_text">'.stripcslashes($this->_general['404_text']).'</textarea>';
         
         $out .= '<div style="margin-top:20px;"></div>';
         $out .= '<input class="cms-submit" type="submit" value="Save" name="general_save_404" /><br />';                                   
         $out .= '</form>';
         echo $out;  
            
         // sidebars
         $out = '';
         $out .= '<a name="sidebars"></a>';
         $out .= '<div style="margin-top:50px;"></div>'; 
         $out .= $this->getTopLink();
         $out .= '<h6 class="cms-h6">Sidebars</h6><hr class="cms-hr" />';
         $out .= '<form action="#sidebars" method="post" >
         <input type="text" style="width:240px;" name="name" value="Custom sidebar name" />
         <input class="cms-submit" type="submit" style="width:200px;" value="Add new sidebar" name="general_add_sidebar" /></form>';
 
         $count = count($this->_sidebars);
         if($count > 0)
         {         
            $out .= '<div style="height:20px;"></div>';
            $out .= '<h6 class="cms-h6s">Sidebars list:</h6>';  
            
            for($i = 0; $i < $count; $i++)
            {
                $out .= '<form action="#sidebars" method="post">';
                $out .= '<input type="hidden" value="'.$i.'" name="index" />';
                $out .= '<input type="text" style="width:300px;" name="name" value="'.$this->_sidebars[$i]->_name.'" /> ';
                $out .= '<input type="submit" class="cms-submit" name="general_save_sidebar" value="Save" /> ';
                $out .= '<input type="submit" onclick="return confirm('."'Delete this sidebar?'".')" class="cms-submit-delete" value="Delete" name="general_delete_sidebar" />';
                $out .= '</form>';
            } // for                   
         }  
         echo $out;
                         
         $out = '<div style="height:20px;"></div>';
         $out .= '<h6 class="cms-h6s">Other sidebar options:</h6>'; 
         $out .= '<form action="#sidebars" method="post">';
         $out .= $this->printSidebarsList(240, 'sidebar_default', $this->_general['sidebar_default']) . ' Select default sidebar<br />'; 
         $out .= $this->printSidebarsList(240, 'sidebar_single', $this->_general['sidebar_single']) . ' Select default sidebar for posts<br />'; 
         $out .= $this->printSidebarsList(240, 'sidebar_blog_category', $this->_general['sidebar_blog_category']) . ' Select sidebar for blog categories<br />'; 
         $out .= $this->printSidebarsList(240, 'sidebar_team_member', $this->_general['sidebar_team_member']) . ' Select sidebar for team member single page<br /><br />'; 
         
         $out .= '<span class="cms-span-10">Sidebar global position:</span><br />';
         $out .= '<input type="radio" name="sidebar_global_pos" '.$this->attrChecked($this->_general['sidebar_global_pos'] == CMS_SIDEBAR_LEFT).' value="'.CMS_SIDEBAR_LEFT.'" /> Left<br />';
         $out .= '<input type="radio" name="sidebar_global_pos" '.$this->attrChecked($this->_general['sidebar_global_pos'] == CMS_SIDEBAR_RIGHT).' value="'.CMS_SIDEBAR_RIGHT.'" /> Right<br />';
         
         $out .= '<div style="height:20px;"></div>';
         $out .= '<input type="submit" class="cms-submit" value="Save" name="general_sidebar_settings" />';
         $out .= '</form>';
         echo $out;               
                 
         // search
         $out = ''; 
         $out .= '<a name="search"></a>';
         $out .= '<div style="margin-top:50px;"></div>'; 
         $out .= $this->getTopLink();        
         $out .= '<h6 class="cms-h6">Searching</h6><hr class="cms-hr"/>';                 
         
         $out .= '<form action="#search" method="post" >';                                                                                                                                        
         
         $out .= '<span class="cms-span-10">Search page settings:</span><br />'; 
         $out .= '<input type="text" style="width:400px;" name="search_no_results" value="'.stripcslashes($this->_general['search_no_results']).'" /> Text displayed when there are no search results <br />';
         $out .= '<input type="text" style="width:400px;" name="search_before_fragment" value="'.stripcslashes($this->_general['search_before_fragment']).'" /> Text displayed in page title before searched fragment <br />';
         $out .= $this->printSidebarsList(400, 'search_sidebar', $this->_general['search_sidebar']);
         $out .= ' Select search page sidebar <br />';
         
         $out .= '<div style="height:10px;"></div>';
         $out .= '<input type="checkbox" '.($this->_general['search_in_posts'] ? ' checked="checked" ' : '').' name="search_in_posts" /> Search in posts<br />'; 
         $out .= '<input type="checkbox" '.($this->_general['search_in_pages'] ? ' checked="checked" ' : '').' name="search_in_pages" /> Search in pages<br />';
  
         $out .= '<div style="height:10px;"></div>'; 
         $out .= '<input type="text" style="width:60px;text-align:center;" value="'.$this->_general['search_page_size'].'" name="search_page_size" /> Number of search results displayed on one page<br />';         
         $out .= '<input type="checkbox" '.($this->_general['search_show_form'] ? ' checked="checked" ' : '').' name="search_show_form" /> Show search form<br /><br />'; 
         
         $out .= '<span class="cms-span-10">Header search panel settings:</span><br />';
         $out .= '<input type="checkbox" '.$this->attrChecked($this->_general['search_header_panel_show']).' name="search_header_panel_show" /> Show header search panel<br /><br />'; 
         $out .= '<input type="text" style="width:60px;text-align:center;" value="'.$this->_general['search_header_panel_left'].'" name="search_header_panel_left" /> Left position in px<br />';
         $out .= '<input type="text" style="width:60px;text-align:center;" value="'.$this->_general['search_header_panel_right'].'" name="search_header_panel_right" /> Right position in px<br />';
         $out .= '<input type="text" style="width:60px;text-align:center;" value="'.$this->_general['search_header_panel_top'].'" name="search_header_panel_top" /> Top position in px<br />';
         $out .= '<input type="text" style="width:60px;text-align:center;" value="'.$this->_general['search_header_panel_bottom'].'" name="search_header_panel_bottom" /> Bottom position in px<br /><br />';
         
         $out .= '<input type="checkbox" '.$this->attrChecked($this->_general['search_header_panel_use_right']).' name="search_header_panel_use_right" /> Use right position<br />';
         $out .= '<input type="checkbox" '.$this->attrChecked($this->_general['search_header_panel_use_bottom']).' name="search_header_panel_use_bottom" /> Use bottom position<br /><br />';
         
         $out .= '<span class="cms-span-10-normal">Place where search panel will be displayed:</span><br />'; 
         $out .= '<input type="radio" name="search_header_panel_place" value="'.CPGeneral::SPANEL_PLACE_ABOVE_MENU.'" '.$this->attrChecked($this->_general['search_header_panel_place'] == CPGeneral::SPANEL_PLACE_ABOVE_MENU).' /> Above menu<br />';
         $out .= '<input type="radio" name="search_header_panel_place" value="'.CPGeneral::SPANEL_PLACE_UNDER_MENU.'" '.$this->attrChecked($this->_general['search_header_panel_place'] == CPGeneral::SPANEL_PLACE_UNDER_MENU).' /> Under menu<br />';                  
         
         $out .= '<br /><span class="cms-span-10">Search position parameters work only for <strong>Above menu</strong> option (you can change position of the search form in header container).</span><br />'; 
                  
         
         $out .= '<div style="height:20px;"></div>';
         $out .= '<input class="cms-submit" type="submit" value="Save" name="general_save_search" /><br />';                                  
         $out .= '</form>';           
         echo $out;                 
         
         // tracking code
         $out = '';
         $out .= '<a name="trackingcode"></a>';                  
         $out .= '<div style="margin-top:50px;"></div>'; 
         $out .= $this->getTopLink();        
         $out .= '<h6 class="cms-h6">Footer and header tracking code</h6><hr class="cms-hr"/>';
         $out .= '<form action="#trackingcode" method="post" >';
         
         $out .= '<span class="cms-span-10">Header tracking code:</span><br />'; 
         $out .= '<textarea style="width:700px;height:120px;font-size:11px;padding:8px;" name="tcode_header" >'.stripcslashes($this->_general['tcode_header']).'</textarea><br />';
         $out .= '<input type="checkbox" name="tcode_header_show" '.$this->attrChecked($this->_general['tcode_header_show']).' /> Use header tracking code<br /><br />';

         $out .= '<span class="cms-span-10">Footer tracking code:</span><br />'; 
         $out .= '<textarea style="width:700px;height:120px;font-size:11px;padding:8px;" name="tcode_footer" >'.stripcslashes($this->_general['tcode_footer']).'</textarea><br />';
         $out .= '<input type="checkbox" name="tcode_footer_show" '.$this->attrChecked($this->_general['tcode_footer_show']).' /> Use footer tracking code<br />';
         
         $out .= '<div style="margin-top:20px;"></div>';
         $out .= '<input class="cms-submit" type="submit" value="Save" name="general_tcode_settings" /><br />';                                   
         $out .= '</form>';
         echo $out;                   
                            
         // footer
         $out = '';
         $out .= '<a name="footer"></a>';
         $out .= '<div style="margin-top:50px;"></div>'; 
         $out .= $this->getTopLink();        
         $out .= '<h6 class="cms-h6">Footer</h6><hr class="cms-hr"/>';

         $out .= '<form action="#footer" method="post" >';
         $out .= '<span class="cms-span-10">Footer logo image path:</span><br />';                                                                                                                                         
         $out .= '<input type="text" style="width:600px;" id="footer_logo_path" name="footer_logo_path" value="'.$this->_general['footer_logo_path'].'" /> ';
         $out .= '<input class="cms-upload upload_image_button" type="button" value="Upload Image" name="footer_logo_path" /> <br />';
         $out .= '<input type="checkbox" name="footer_show_logo" '.$this->attrChecked($this->_general['footer_show_logo']).' /> Show footer logo<br /><br />';
         
         $out .= '<span class="cms-span-10">Copyright text:</span><br />'; 
         $out .= '<textarea style="width:600px;height:60px;font-size:11px;padding:8px;" name="footer_copy">'.stripcslashes($this->_general['footer_copy']).'</textarea><br />';
         $out .= '<input type="checkbox" name="footer_show_copy" '.$this->attrChecked($this->_general['footer_show_copy']).' /> Show footer copyright text<br /><br />';
                                            
         $out .= '<span class="cms-span-10">Copyright text align:</span><br />';
         $out .= '<input type="radio" name="footer_copy_align" value="left" '.$this->attrChecked($this->_general['footer_copy_align'] == 'left').' /> Left<br />';                                    
         $out .= '<input type="radio" name="footer_copy_align" value="center" '.$this->attrChecked($this->_general['footer_copy_align'] == 'center').' /> Center<br />';
         $out .= '<input type="radio" name="footer_copy_align" value="right" '.$this->attrChecked($this->_general['footer_copy_align'] == 'right').' /> Right<br /><br />';                                            
                                            
         $out .= '<input type="checkbox" name="footer_show_links" '.$this->attrChecked($this->_general['footer_show_links']).' /> Show footer links<br /><br />';
         
         $out .= '<span class="cms-span-10">Footer links content:</span><br />';
         $out .= '<textarea style="width:600px;height:400px;max-width:600px;padding:5px;font-size:11px;" name="footer_links_content">';
         $out .= stripcslashes($this->_general['footer_links_content']);   
         $out .= '</textarea><br />';
         $out .= '<input class="cms-submit" type="submit" value="Restore default footer links content" name="general_restore_footer_links" /><br />'; 
         $out .= '<br />';
         
         $out .= '<span class="cms-span-10">Widgetized footer settings (select sidebars and set it width):</span><br />';  
         $out .= '<input type="checkbox" name="footer_show_widgetized" '.$this->attrChecked($this->_general['footer_show_widgetized']).' /> Show widgets<br /><br />';
         
         // a
         $out .= $this->printSidebarsList(300, 'footer_sid_a', $this->_general['footer_sid_a']);
         $out .= ' <input type="text" style="width:60px;text-align:center;" name="footer_sid_a_width" value="'.$this->_general['footer_sid_a_width'].'" /> Width';
         $out .= ' <input type="checkbox" name="footer_sid_a_show" '.$this->attrChecked($this->_general['footer_sid_a_show']).' /> Display<br />'; 
         // b
         $out .= $this->printSidebarsList(300, 'footer_sid_b', $this->_general['footer_sid_b']);
         $out .= ' <input type="text" style="width:60px;text-align:center;" name="footer_sid_b_width" value="'.$this->_general['footer_sid_b_width'].'" /> Width';
         $out .= ' <input type="checkbox" name="footer_sid_b_show" '.$this->attrChecked($this->_general['footer_sid_b_show']).' /> Display<br />'; 
         // c
         $out .= $this->printSidebarsList(300, 'footer_sid_c', $this->_general['footer_sid_c']);
         $out .= ' <input type="text" style="width:60px;text-align:center;" name="footer_sid_c_width" value="'.$this->_general['footer_sid_c_width'].'" /> Width';
         $out .= ' <input type="checkbox" name="footer_sid_c_show" '.$this->attrChecked($this->_general['footer_sid_c_show']).' /> Display<br />'; 
         // d
         $out .= $this->printSidebarsList(300, 'footer_sid_d', $this->_general['footer_sid_d']);
         $out .= ' <input type="text" style="width:60px;text-align:center;" name="footer_sid_d_width" value="'.$this->_general['footer_sid_d_width'].'" /> Width';
         $out .= ' <input type="checkbox" name="footer_sid_d_show" '.$this->attrChecked($this->_general['footer_sid_d_show']).' /> Display<br />'; 
         // e
         $out .= $this->printSidebarsList(300, 'footer_sid_e', $this->_general['footer_sid_e']);
         $out .= ' <input type="text" style="width:60px;text-align:center;" name="footer_sid_e_width" value="'.$this->_general['footer_sid_e_width'].'" /> Width';
         $out .= ' <input type="checkbox" name="footer_sid_e_show" '.$this->attrChecked($this->_general['footer_sid_e_show']).' /> Display<br />'; 
         // f
         $out .= $this->printSidebarsList(300, 'footer_sid_f', $this->_general['footer_sid_f']);
         $out .= ' <input type="text" style="width:60px;text-align:center;" name="footer_sid_f_width" value="'.$this->_general['footer_sid_f_width'].'" /> Width';
         $out .= ' <input type="checkbox" name="footer_sid_f_show" '.$this->attrChecked($this->_general['footer_sid_f_show']).' /> Display<br /><br />';
         
         $out .= '<input type="text" style="width:60px;text-align:center;" name="footer_sid_separator_width" value="'.$this->_general['footer_sid_separator_width'].'" /> Sidebars columns separator width in pixels (default 20 px)<br />';                                              
         
         $out .= '<br /><span class="cms-span-10">Widgetized Footer sidebars area is 960 pixels wide. Default footer sidebar column is set to 265 pixels, but you can change it and fit sidebar to your needs. With smaller width you can insert more sidebar in footer but remember that some widgets will not look good in any column size, some of widgets need sidebar 265px width.</span><br />'; 
         
         $out .= '<div style="margin-top:20px;"></div>';
         $out .= '<input class="cms-submit" type="submit" value="Save" name="general_save_footer" /><br />';                                  
         $out .= '</form>';       
         echo $out;                                   
    }        
} // class CPGeneral
        
        
?>
