<?php
/**********************************************************************
* MODERN ELEGANCE WORDPRESS THEME 
* (Ideal For Business And Personal Use: Portfolio or Blog)   
* 
* File name:   
*      cp_controlpanel.php
* Brief:       
*      Core PHP code for wordpress admin panel
* Author:      
*      DigitalCavalry
* Author URI:
*      http://themeforest.net/user/DigitalCavalry
* Contact:
*      digitalcavalry@gmail.com 
**********************************************************************/
                        
/*********************************************************** 
* Class name:
*    CDCThemeControlPanel
* Descripton:   
***********************************************************/
class CDCThemeControlPanel 
{
    /*********************************************************** 
    * Constructor
    ************************************************************/
    public function __construct() 
    {           
        
                                        
        // call only if admin page
        if(is_admin())
        {   
                               
            if(!is_dir(CMS_THEME_TEMP_FOLDER_PATH))
            {
               mkdir(CMS_THEME_TEMP_FOLDER_PATH);
            }              
            add_post_type_support('page', 'excerpt'); 
            add_action('admin_print_scripts', array(&$this, 'loadAdminScripts'));
            add_action('admin_print_styles', array(&$this, 'loadAdminStyles'));               
            add_action('admin_menu', array(&$this, 'adminMenu'));
            add_action('admin_head', array(&$this, 'adminHead'));               
                  
            add_filter('media_buttons_context', array(&$this,'wp_myplugin_media_button')); 
            
            add_filter('manage_edit-post_columns', array(&$this, 'adminPostColumnsCreate'));
            add_action('manage_post_posts_custom_column', array(&$this, 'adminPostColumnsRender'), 10, 2);
            
            add_filter('manage_edit-page_columns', array(&$this, 'adminPageColumnsCreate'));
            add_action('manage_page_posts_custom_column', array(&$this, 'adminPageColumnsRender'), 10, 2);
            
            $this->getIMeta();                                       
        } else
        {            
            $this->addThemeActions();                        
            $this->_theme_color = $this->getIGeneral()->getThemeColor();
            
            
            if($this->getIGeneral()->showClientPanel()) 
            {
                $this->getIClientPanel()->process();    
                $main_color = $this->getIClientPanel()->getValue('main_color');                
                if($main_color !== null) { $this->_theme_color = $main_color; }
            }
             
             remove_filter('the_content', 'do_shortcode', 11);                 
             add_filter('the_content', 'do_shortcode', 7);         
             if($this->getIGeneral()->isAutoFormatingDisabled())
             {
                 remove_filter('the_content', 'wpautop');
                                
             }
             if($this->getIGeneral()->isAutoTexturizeDisabled())
             {             
                remove_filter('the_content', 'wptexturize'); 
             } 
             $this->getIShortCodes();
             
           
            if($this->getIGeneral()->isAuthorizationComment())
            {           
                add_action('pre_comment_on_post',  array(&$this, 'processCommentPost'));                        
            }   
        }       
        
        load_theme_textdomain(CMS_TXT_DOMAIN, get_template_directory().'/languages');
        $this->getICustomPosts();
        $this->getIGeneral()->registerSidebars();
        $this->getIMenu();
        $this->createGoogleFonts();    
        $this->createCufonFonts();              
    } // constructor

    /*********************************************************** 
    * Public members
    ************************************************************/     
    public $_theme_color = '#999999';
    
    public $_general = null;
    public $_home = null; 
    public $_menu = null;
    public $_progressslider = null;
    public $_chainslider = null;
    public $_accordion = null;
    public $_clientpanel = null;
    public $_meta = null;
    public $_shortcodes = null;
    public $_customposts = null;
    public $_renderer = null; 
    public $_floatingobjects = null;
    public $_quicknewspanel = null;
    public $_help = null;
    
    public $_fonts_google = array();
    public $_fonts_cufon = array();
    
    /*********************************************************** 
    * Private members
    ************************************************************/     
    private $_navigaton = Array(
        'General' => 'dc-theme-opt',
        'Menu' => 'dc-theme-opt-menu',
        'Quick news' => 'dc-theme-opt-quick-news',
        'Floating objects' => 'dc-theme-opt-fobjects',
        'Home' => 'dc-theme-opt-home',
        'Accordion slider' => 'dc-theme-opt-accordion-slider',
        'Progress slider' => 'dc-theme-opt-progress-slider',
        'Chain slider' => 'dc-theme-opt-chain-slider',
        'ID-Help' => 'dc-theme-opt-help' 
    );    
    
    public function processCommentPost($id)
    {
       $user = wp_get_current_user();

        if(!$user->ID)
        {
           $is_scode = isset($_POST['dc_scode']);
           $is_scodeuser = isset($_POST['dc_scodeuser']);
           
           if($_POST['dc_scode'] != md5(md5($_POST['dc_scodeuser'])) or !$is_scode or !$is_scodeuser)
           {
                wp_die( __('Sorry, the authorization code from image is wrong.') );  
           } 
        } 
    }
    
    /*********************************************************** 
    * Public functions
    ************************************************************/       
    public function getAllCufonFonts()
    {
        return $this->_fonts_cufon;
    } 
    
    public function getCufonFont($index)
    {
        return $this->_fonts_cufon[$index];
    }
    
    public function createCufonFonts()
    {       
        // A
        $this->addCufonFont('Advent', 'advent_400.font.js');
//        $this->addCufonFont('Akadora', 'akaDora_400.font.js');
//        $this->addCufonFont('Alako Bold', 'Alako-Bold_400.font.js');         
        // B
//        $this->addCufonFont('Bebas Neue', 'Bebas_Neue_400.font.js'); 
//        $this->addCufonFont('Boycott', 'Boycott_400.font.js'); 
        // C
        $this->addCufonFont('Citon Light', 'CitonLightDB_400.font.js');
        $this->addCufonFont('Cuprum FFU', 'CuprumFFU_400.font.js');
        // E
//        $this->addCufonFont('Edmunds', 'Edmunds_400.font.js');
//        $this->addCufonFont('Edo', 'Edo_400.font.js'); 
//        $this->addCufonFont('Engebrechtre', 'Engebrechtre_400.font.js');
        $this->addCufonFont('Eurofurence', 'Eurofurence_500.font.js');    
        // F    
        $this->addCufonFont('Folks', 'Folks_400.font.js');
        // G
        $this->addCufonFont('Gabrielle', 'Gabrielle_400.font.js'); 
//        $this->addCufonFont('Gesso', 'Gesso_400.font.js'); 
//        $this->addCufonFont('GoodDog', 'GoodDog_400.font.js'); 
        // I
        $this->addCufonFont('Ingleby Regular', 'Ingleby_Regular_400.font.js');
        // J
        $this->addCufonFont('Jesaya', 'Jesaya_Free_400.font.js'); 
//        $this->addCufonFont('Josefine Small', 'Josefine_Small.font.js');
        // K
        $this->addCufonFont('Kingthings Petrock', 'Kingthings_Petrock_400.font.js'); 
//        $this->addCufonFont('Kirsty', 'Kirsty_400.font.js'); 
//        $this->addCufonFont('Komika Axis', 'Komika_Axis_400.font.js');     
        // L
//        $this->addCufonFont('League Gothic', 'League_Gothic_400.font.js'); 
//        $this->addCufonFont('Lobster', 'Lobster_14_400.font.js'); 
        // M
//        $this->addCufonFont('Molengo', 'Molengo.font.js');
        // O
//        $this->addCufonFont('Old Stamper', 'Old_Stamper_400.font.js');
//        $this->addCufonFont('Origicide', 'Origicide_400.font.js');
        // P
//        $this->addCufonFont('Playtime Hot Toddies', 'Playtime_With_Hot_Toddies_400.font.js');        
        // Q
        $this->addCufonFont('Qlassik Medium', 'Qlassik_Medium_500.font.js'); 
        // R
        $this->addCufonFont('Rolina', 'Rolina_400.font.js');                                  
        // S
//        $this->addCufonFont('Sansation', 'Sansation_300.font.js');
//        $this->addCufonFont('Sansation Regular', 'Sansation_400.font.js'); 
//        $this->addCufonFont('Scratch Italic', 'Scratch_italic_400.font.js');
//        $this->addCufonFont('Snickles', 'Snickles_400.font.js');
        // T
        $this->addCufonFont('Tallys', 'Tallys_400.font.js'); 
        $this->addCufonFont('Tenderness', 'Tenderness_400.font.js');  
        // V
        $this->addCufonFont('Vera Humana', 'Vera_Humana_95_400.font.js'); 
//        $this->addCufonFont('Veteran Typewriter', 'Veteran_Typewriter_400.font.js');        
        // Y
        $this->addCufonFont('Yanone Kaffeesatz', 'Yanone_Kaffeesatz_400.font.js'); 
//        $this->addCufonFont('Yataghan', 'Yataghan_400.font.js'); 
        // Z
//        $this->addCufonFont('Zero Zero', 'Zero__Zero_Is_400.font.js'); 
    }
      
   
    public function getAllGoogleFonts()
    {
        return $this->_fonts_google;
    }

    public function getGoogleFont($index)
    {
        return $this->_fonts_google[$index];
    }
    
    public function createGoogleFonts()
    {
        // A
//        $this->addGoogleFont('Aclonica', "'Aclonica', arial, serif", 'http://fonts.googleapis.com/css?family=Aclonica'); 
//        $this->addGoogleFont('Allan', "'Allan', arial, serif", 'http://fonts.googleapis.com/css?family=Allan:bold');
//        $this->addGoogleFont('Allerta', "'Allerta', arial, serif", 'http://fonts.googleapis.com/css?family=Allerta'); 
//        $this->addGoogleFont('Amaranth', "'Amaranth', arial, serif", 'http://fonts.googleapis.com/css?family=Amaranth'); 
//        $this->addGoogleFont('Annie Use Your Telescope', "'Annie Use Your Telescope', arial, serif", 'http://fonts.googleapis.com/css?family=Annie+Use+Your+Telescope');
//        $this->addGoogleFont('Anton', "'Anton', arial, serif", 'http://fonts.googleapis.com/css?family=Anton');
//        $this->addGoogleFont('Architects Daughter', "'Architects Daughter', arial, serif", 'http://fonts.googleapis.com/css?family=Architects+Daughter'); 
        // B
//        $this->addGoogleFont('Bangers', "'Bangers', arial, serif", 'http://fonts.googleapis.com/css?family=Bangers'); 
        // C
//        $this->addGoogleFont('Cabin', "'Cabin', arial, serif", 'http://fonts.googleapis.com/css?family=Cabin'); 
//        $this->addGoogleFont('Cabin Sketch', "'Cabin Sketch', arial, serif", 'http://fonts.googleapis.com/css?family=Cabin+Sketch:bold'); 
        $this->addGoogleFont('Calligraffitti', "'Calligraffitti', arial, serif", 'http://fonts.googleapis.com/css?family=Calligraffitti');
//        $this->addGoogleFont('Carter One', "'Carter One', arial, serif", 'http://fonts.googleapis.com/css?family=Carter+One'); 
//        $this->addGoogleFont('Chewy', "'Chewy', arial, serif", 'http://fonts.googleapis.com/css?family=Chewy'); 
//        $this->addGoogleFont('Copse', "'Copse', arial, serif", 'http://fonts.googleapis.com/css?family=Copse'); 
//        $this->addGoogleFont('Corben', "'Corben', arial, serif", 'http://fonts.googleapis.com/css?family=Corben:bold');
//        $this->addGoogleFont('Crafty Girls', "'Crafty Girls', arial, serif", 'http://fonts.googleapis.com/css?family=Crafty+Girls'); 
//        $this->addGoogleFont('Crushed', "'Crushed', arial, serif", 'http://fonts.googleapis.com/css?family=Crushed');
        $this->addGoogleFont('Cuprum', "'Cuprum', arial, serif", 'http://fonts.googleapis.com/css?family=Cuprum');        
        // D
//        $this->addGoogleFont('Droid Sans', "'Droid Sans', arial, serif", 'http://fonts.googleapis.com/css?family=Droid+Sans');
//        $this->addGoogleFont('Droid Sans Bold', "'Droid Sans', arial, serif", 'http://fonts.googleapis.com/css?family=Droid+Sans:bold');    
        // E
//        $this->addGoogleFont('Expletus Sans', "'Expletus Sans', arial, serif", 'http://fonts.googleapis.com/css?family=Expletus+Sans');
        // F
//        $this->addGoogleFont('Francois One', "'Francois One', arial, serif", 'http://fonts.googleapis.com/css?family=Francois+One');
        // I
        $this->addGoogleFont('IM Fell English', "'IM Fell English', arial, serif", 'http://fonts.googleapis.com/css?family=IM+Fell+English'); 
//        $this->addGoogleFont('Indie Flower', "'Indie Flower', arial, serif", 'http://fonts.googleapis.com/css?family=Indie+Flower');
        // J
        $this->addGoogleFont('Josefin Sans', "'Josefin Sans', arial, serif", 'http://fonts.googleapis.com/css?family=Josefin+Sans'); 
//        $this->addGoogleFont('Josefin Sans Semi Bold', "'Josefin Sans', arial, serif", 'http://fonts.googleapis.com/css?family=Josefin+Sans:600');
//        $this->addGoogleFont('Judson', "'Judson', arial, serif", 'http://fonts.googleapis.com/css?family=Judson');
//        $this->addGoogleFont('Just Another Hand', "'Just Another Hand', arial, serif", 'http://fonts.googleapis.com/css?family=Just+Another+Hand');  
        // L
//       $this->addGoogleFont('Lato Ultra Light', "'Lato', arial, serif", 'http://fonts.googleapis.com/css?family=Lato:100');
        $this->addGoogleFont('Lato Light', "'Lato', arial, serif", 'http://fonts.googleapis.com/css?family=Lato:light'); 
//        $this->addGoogleFont('Lato Regular', "'Lato', arial, serif", 'http://fonts.googleapis.com/css?family=Lato');  
//        $this->addGoogleFont('Lekton', "'Lekton', arial, serif", 'http://fonts.googleapis.com/css?family=Lekton');  
//        $this->addGoogleFont('Lobster', "'Lobster', arial, serif;", 'http://fonts.googleapis.com/css?family=Lobster'); 
//        $this->addGoogleFont('Luckiest Guy', "'Luckiest Guy', arial, serif", 'http://fonts.googleapis.com/css?family=Luckiest+Guy');
        // M
//        $this->addGoogleFont('Mako', "'Mako', arial, serif", 'http://fonts.googleapis.com/css?family=Mako'); 
//        $this->addGoogleFont('Metrophobic', "'Metrophobic', arial, serif", 'http://fonts.googleapis.com/css?family=Metrophobic'); 
//        $this->addGoogleFont('Michroma', "'Michroma', arial, serif", 'http://fonts.googleapis.com/css?family=Michroma'); 
//        $this->addGoogleFont('Miltonian', "'Miltonian', arial, serif", 'http://fonts.googleapis.com/css?family=Miltonian'); 
//        $this->addGoogleFont('Miltonian Tattoo', "'Miltonian Tattoo', arial, serif", 'http://fonts.googleapis.com/css?family=Miltonian+Tattoo'); 
//        $this->addGoogleFont('Molengo', "'Molengo', arial, serif;", 'http://fonts.googleapis.com/css?family=Molengo'); 
        // N
        $this->addGoogleFont('News Cycle', "'News Cycle', arial, serif", 'http://fonts.googleapis.com/css?family=News+Cycle');
//        $this->addGoogleFont('Nobile Regular', "'Nobile', arial, serif", 'http://fonts.googleapis.com/css?family=Nobile'); 
//        $this->addGoogleFont('Nobile Bold', "'Nobile', arial, serif", 'http://fonts.googleapis.com/css?family=Nobile:bold'); 
//        $this->addGoogleFont('Nunito Light', "'Nunito', arial, serif", 'http://fonts.googleapis.com/css?family=Nunito:light'); 
//        $this->addGoogleFont('Nunito Regular', "'Nunito', arial, serif", 'http://fonts.googleapis.com/css?family=Nunito'); 
        // O 
//        $this->addGoogleFont('Orbitron', "'Orbitron', arial, serif", 'http://fonts.googleapis.com/css?family=Orbitron');
//        $this->addGoogleFont('Oswald', "'Oswald', arial, serif", 'http://fonts.googleapis.com/css?family=Oswald');           
        // P
//        $this->addGoogleFont('Pacifico', "'Pacifico', arial, serif", 'http://fonts.googleapis.com/css?family=Pacifico'); 
//        $this->addGoogleFont('Permanent Marker', "'Permanent Marker', arial, serif", 'http://fonts.googleapis.com/css?family=Permanent+Marker');         
        $this->addGoogleFont('Philosopher', "'Philosopher', arial, serif", 'http://fonts.googleapis.com/css?family=Philosopher'); 
//        $this->addGoogleFont('Play', "'Play', arial, serif", 'http://fonts.googleapis.com/css?family=Play');
        $this->addGoogleFont('PT Sans Narrow', "'PT Sans Narrow', arial, serif", 'http://fonts.googleapis.com/css?family=PT+Sans+Narrow');
//        $this->addGoogleFont('Puritan', "'Puritan', arial, serif", 'http://fonts.googleapis.com/css?family=Puritan'); 
        // Q
        $this->addGoogleFont('Quattrocento Sans', "'Quattrocento Sans', arial, serif", 'http://fonts.googleapis.com/css?family=Quattrocento+Sans');
        // R
//        $this->addGoogleFont('Raleway', "'Raleway', arial, serif", 'http://fonts.googleapis.com/css?family=Raleway:100');
//        $this->addGoogleFont('Rock Salt', "'Rock Salt', arial, serif", 'http://fonts.googleapis.com/css?family=Rock+Salt');
//        $this->addGoogleFont('Reenie Beanie', "'Reenie Beanie', arial, serif", 'http://fonts.googleapis.com/css?family=Reenie+Beanie'); 
        // S                         
//        $this->addGoogleFont('Salsa', "'Salsa', cursive", 'http://fonts.googleapis.com/css?family=Salsa');
//        $this->addGoogleFont('Schoolbell', "'Schoolbell', arial, serif", 'http://fonts.googleapis.com/css?family=Schoolbell');
//        $this->addGoogleFont('Special Elite', "'Special Elite', arial, serif", 'http://fonts.googleapis.com/css?family=Special+Elite'); 
//        $this->addGoogleFont('Sue Ellen Francisco', "'Sue Ellen Francisco', arial, serif", 'http://fonts.googleapis.com/css?family=Sue+Ellen+Francisco');    
        // T
        $this->addGoogleFont('Terminal Dosis Light', "'Terminal Dosis Light', arial, serif", 'http://fonts.googleapis.com/css?family=Terminal+Dosis+Light'); 
//        $this->addGoogleFont('The Girl Next Door', "'The Girl Next Door', arial, serif", 'http://fonts.googleapis.com/css?family=The+Girl+Next+Door'); 
        // U
//        $this->addGoogleFont('Ubuntu Light', "'Ubuntu', arial, serif", 'http://fonts.googleapis.com/css?family=Ubuntu:light'); 
//       $this->addGoogleFont('Ubuntu Regular', "'Ubuntu', arial, serif", 'http://fonts.googleapis.com/css?family=Ubuntu'); 
        // Y
        $this->addGoogleFont('Yanone Kaffeesatz Extra Light', "'Yanone Kaffeesatz', arial, serif", 'http://fonts.googleapis.com/css?family=Yanone+Kaffeesatz:extralight');
        $this->addGoogleFont('Yanone Kaffeesatz Light', "'Yanone Kaffeesatz', arial, serif", 'http://fonts.googleapis.com/css?family=Yanone+Kaffeesatz:light');         
//        $this->addGoogleFont('Yanone Kaffeesatz Extra Regular', "'Yanone Kaffeesatz', arial, serif", 'http://fonts.googleapis.com/css?family=Yanone+Kaffeesatz');         
//        $this->addGoogleFont('Yanone Kaffeesatz Extra Bold', "'Yanone Kaffeesatz', arial, serif", 'http://fonts.googleapis.com/css?family=Yanone+Kaffeesatz:bold');         
        
        // W
        $this->addGoogleFont('Waiting for the Sunrise', "'Waiting for the Sunrise', arial, serif", 'http://fonts.googleapis.com/css?family=Waiting+for+the+Sunrise');
//        $this->addGoogleFont('Wire One', "'Wire One', arial, serif", 'http://fonts.googleapis.com/css?family=Wire+One'); 
        
        // $this->addFont('', "", '');
    }

    public function getSelectedFontCSSLink()
    {
        $index = (int)$this->getIGeneral()->getThemeFontIndex();
        if($this->getIGeneral()->showClientPanel())
        {
            $client_index = $this->getIClientPanel()->getValue('font');
            if($client_index !== null)
            {
                $index = (int)$client_index;
            }    
        }
        echo '<link href="'.$this->_fonts[$index]->_url.'" rel="stylesheet" type="text/css">';
    }
    
     public function getSelectedFontLink($area)
    {
        $type = $this->getIGeneral()->getThemeFontType();
        if($this->getIGeneral()->showClientPanel())
        {
            $font_type = $this->getIClientPanel()->getValue('font_type');
            if($font_type !== null)
            {
                $type = $font_type;
            } 
                
        }
        
        if($type == CMS_FONT_TYPE_GOOGLE and $area == CMS_CSS_AREA)
        {
            $index = (int)$this->getIGeneral()->getThemeGoogleFontIndex();
            if($this->getIGeneral()->showClientPanel())
            {
                $client_index = $this->getIClientPanel()->getValue('font_google');
                if($client_index !== null)
                {
                    $index = (int)$client_index;
                }    
            }
            echo '<link href="'.$this->_fonts_google[$index]->_url.'" rel="stylesheet" type="text/css">';
        } else
        if($type == CMS_FONT_TYPE_CUFON and $area == CMS_JAVASCRIPT_AREA)
        {
            $index = (int)$this->getIGeneral()->getThemeCufonFontIndex();
            if($this->getIGeneral()->showClientPanel())
            {
                $client_index = $this->getIClientPanel()->getValue('font_cufon');
                if($client_index !== null)
                {
                    $index = (int)$client_index;
                }    
            }
            echo '<script type="text/javascript" src="'.get_bloginfo('template_url').'/fonts/'.$this->_fonts_cufon[$index]->_file.'"></script>';        
        }
    }
    
    private function addGoogleFont($name, $css, $url)
    {
        $font = new DCC_CGoogleFont($name, $css, $url);
        array_push($this->_fonts_google, $font);    
    }

    private function addCufonFont($name, $file)
    {
        $font = new DCC_CCufonFont($name, $file);
        array_push($this->_fonts_cufon, $font);    
    }
    
    public function getThemeSkinCSSLink()    
    {
      /*  $skin = $this->getIGeneral()->getThemeSkin();
        
        if($this->getIGeneral()->showClientPanel())
        {
            $client_skin = $this->getIClientPanel()->getValue('theme_skin'); 
            if($client_skin !== null)
            {
                $skin = $client_skin;    
            } 
        }
        
        
    
        switch($skin)
        {
            case CPGeneral::THEME_SKIN_WHITE:
                echo '<link type="text/css" rel="stylesheet" href="'.get_bloginfo("template_url").'/css/common.css" />'; 
            break;
                        
            case CPGeneral::THEME_SKIN_GREY:
                echo '<link type="text/css" rel="stylesheet" href="'.get_bloginfo("template_url").'/css/common_grey.css" />';
            break;
            
            case CPGeneral::THEME_SKIN_BLACK:
                echo '<link type="text/css" rel="stylesheet" href="'.get_bloginfo("template_url").'/css/common_dark.css" />'; 
            break;                        
        }
        
        */
        echo '<link type="text/css" rel="stylesheet" href="'.get_bloginfo("template_url").'/css/common.css" />';          
    }
    
    public function wp_myplugin_media_button($context) 
    {
        $btns = '%s';
        $btns .= $this->getIGeneral()->getMediaButtons();    
        return sprintf($context, $btns);
    }

    function addThemeActions()
    {
       // add_action('init', array(&$this, 'add_filters'), 0);
        add_post_type_support('page', 'excerpt'); 
        add_action('wp_print_scripts', array(&$this, 'loadScripts'));     
    }
        
    function add_filters() 
    {                              
        add_filter( 'option_posts_per_page', array(&$this, 'option_posts_per_page'));
    }

    function option_posts_per_page($value) 
    {

        $rows = 1;           
        $value = $rows*5;                                                             
        return $value;
    }          
     
    public function & getIGeneral()
    {                                 
        if($this->_general === null){ $this->_general = new CPGeneral(); }
        return $this->_general;
    } 
    
    public function & getIRenderer()
    {                                 
        if($this->_renderer === null){ $this->_renderer = new CPRenderer(); }
        return $this->_renderer;
    }    

    public function & getIFloatingObjects()
    {                                 
        if($this->_floatingobjects === null){ $this->_floatingobjects = new CPFloatingObjects(); }
        return $this->_floatingobjects;
    }       
    
    public function & getIHome()
    {                                 
        if($this->_home === null){ $this->_home = new CPHome(); }
        return $this->_home;
    }      
    
    public function & getIHelp()
    {                                 
        if($this->_help === null){ $this->_help = new CPHelp(); }
        return $this->_help;
    }   

    public function & getIQuickNewsPanel()
    {                                 
        if($this->_quicknewspanel === null){ $this->_quicknewspanel = new CPQuickNewsPanel(); }
        return $this->_quicknewspanel;
    }   
    
    public function & getIShortCodes()
    {                                
        if($this->_shortcodes === null){ $this->_shortcodes = new CPThemeShortCodes($this->_theme_color); }
        return $this->_shortcodes;
    }    
    
    public function & getICustomPosts()
    {                                 
        if($this->_customposts === null){ $this->_customposts = new CPThemeCustomPosts(); }
        return $this->_customposts;
    }    
  
    public function & getIMenu()
    {                                 
        if($this->_menu === null){ $this->_menu = new CPMenu(); }
        return $this->_menu;
    }                
    
    public function & getIAccordionSlider()
    {                                 
        if($this->_accordion === null){ $this->_accordion = new CPAccordionSlider(); }
        return $this->_accordion;
    }        
    
    public function & getIProgressSlider()
    {                                 
        if($this->_progressslider === null){ $this->_progressslider = new CPProgressSlider(); }
        return $this->_progressslider;
    }      

    public function & getIChainSlider()
    {                                 
        if($this->_chainslider === null){ $this->_chainslider = new CPChainSlider(); }
        return $this->_chainslider;
    }      
    
    public function & getIClientPanel()
    {                                 
        if($this->_clientpanel === null){ $this->_clientpanel = new CPClientPanel(); }
        return $this->_clientpanel;
    }       
    
    public function & getIMeta()
    {                                 
        if($this->_meta === null){ $this->_meta = new CPMetaDataCreator(); }
        return $this->_meta;
    }   
             
    public function adminMenu() 
    {
        add_menu_page(CMS_THEME_NAME.' Theme Options', CMS_THEME_NAME, 10, 'dc-theme-opt', array(&$this, 'showOptions'));
        add_submenu_page('dc-theme-opt', CMS_THEME_NAME.' Theme Options', 'General', 10, 'dc-theme-opt', array(&$this, 'showOptions'));
        add_submenu_page('dc-theme-opt', CMS_THEME_NAME.' Theme Options', 'Menu', 10, 'dc-theme-opt-menu', array(&$this, 'showOptions'));
        add_submenu_page('dc-theme-opt', CMS_THEME_NAME.' Theme Options', 'Quick News', 10, 'dc-theme-opt-quick-news', array(&$this, 'showOptions'));
        add_submenu_page('dc-theme-opt', CMS_THEME_NAME.' Theme Options', 'Floating Objects', 10, 'dc-theme-opt-fobjects', array(&$this, 'showOptions'));
        add_submenu_page('dc-theme-opt', CMS_THEME_NAME.' Theme Options', 'Home', 10, 'dc-theme-opt-home', array(&$this, 'showOptions'));
        add_submenu_page('dc-theme-opt', CMS_THEME_NAME.' Theme Options', 'Accordion slider', 10, 'dc-theme-opt-accordion-slider', array(&$this, 'showOptions'));  
        add_submenu_page('dc-theme-opt', CMS_THEME_NAME.' Theme Options', 'Progress slider', 10, 'dc-theme-opt-progress-slider', array(&$this, 'showOptions'));
        add_submenu_page('dc-theme-opt', CMS_THEME_NAME.' Theme Options', 'Chain slider', 10, 'dc-theme-opt-chain-slider', array(&$this, 'showOptions')); 
        add_submenu_page('dc-theme-opt', CMS_THEME_NAME.' Theme Options', 'ID-Help', 10, 'dc-theme-opt-help', array(&$this, 'showOptions'));    
    } 

    public function adminHead() 
    {        

        
        $out = '';
        $out .= '<meta name="cms_theme_url" content="'.get_bloginfo('template_url').'" />';
        $out .= '<meta name="cms_url" content="'.get_bloginfo('url').'" />';   
        $out .= '<meta name="cms_admin_url" content="'.get_admin_url().'" />'; 
        echo $out;        
    } 
    
    public function adminPostColumnsCreate($columns)
    {
        $new_columns['cb'] = '<input type="checkbox" />';
 
        $new_columns['title'] = _x('Post Name', 'column name');
        $new_columns['id'] = __('ID'); 
        $new_columns['author'] = __('Author');
 
        $new_columns['categories'] = __('Categories');
        $new_columns['tags'] = __('Tags');
 
        $new_columns['date'] = _x('Date', 'column name');
 
        return $new_columns;        
    }    

    public function adminPageColumnsCreate($columns)
    {
        $new_columns['cb'] = '<input type="checkbox" />';
 
        
        $new_columns['title'] = _x('Page Name', 'column name');
        $new_columns['id'] = __('ID'); 
        $new_columns['template'] = __('Page template file'); 
        $new_columns['author'] = __('Author');
 
        $new_columns['comments'] = '<div class="vers"><img alt="Comments" src="'.get_admin_url().'images/comment-grey-bubble.png" /></div>';
 
        $new_columns['date'] = _x('Date', 'column name');
        return $new_columns;        
    }  
 
    public function adminPostColumnsRender($column_name, $id) 
    {
        switch ($column_name) {
        case 'id':
            echo $id;
                break;
        default:
            break;
        } // end switch
    } 
    
    public function adminPageColumnsRender($column_name, $id) 
    {
        switch ($column_name) {
        case 'id':
            echo $id;
            break;
        case 'template':
            $template = get_post_meta($id, '_wp_page_template', true);
            if($template == '') { echo '-'; } else { echo $template; }
            break;
        default:
            break;
        } // end switch
    }       
    
    private function getFontType()
    {
        $type = $this->getIGeneral()->getThemeFontType();
        if($this->getIGeneral()->showClientPanel())
        {
            $client_type = $this->getIClientPanel()->getValue('font_type');
            if($client_type !== null)
            {
                $type = $client_type;
            }    
        } 
        
        return $type;        
    }    
    
    public function printHeadMeta() 
    {        
        $type = $this->getFontType();        
        $fonttype = 'cufon';
        if($type == CMS_FONT_TYPE_CUFON) { $fonttype = 'cufon'; } else
        { $fonttype = 'google'; }          
        
        $out = '';
        $out .= '<meta name="cms_theme_url" content="'.get_bloginfo('template_url').'" />';
        $out .= '<meta name="cms_theme_name" content="'.CMS_THEME_NAME.'" />';
        $out .= '<meta name="cms_admin_url" content="'.get_admin_url().'" />'; 
        $out .= '<meta name="cms_font_type" content="'.$fonttype.'" />';
        $out .= '<meta name="cms_site_url" content="'.get_bloginfo('siteurl').'" />';
        $out .= '<meta name="cms_lightbox_mode" content="'.$this->getIGeneral()->getLightBoxMode().'" />';
        $out .= '<meta name="cms_lightbox_overlay" content="'.(int)$this->getIGeneral()->getLightBoxOverlayGallery().'" />'; 
        $out .= $this->getIGeneral()->printHeadMeda();
        echo $out;
                
    }     
      

          
    public function loadAdminScripts() 
    {           
        wp_enqueue_script('jquery'); 
        wp_enqueue_script('media-upload');
        wp_enqueue_script('thickbox');
        
        wp_register_script(CMS_THEME_NAME.'_admin_color_picker', get_bloginfo('template_url').'/cms/lib/js/color/jscolor.js', array('jquery'), ''); 
        wp_register_script(CMS_THEME_NAME.'_admin_js', get_bloginfo('template_url').'/cms/js/cms.js', array('jquery', 'jquery-ui-core','jquery-ui-sortable'), '');
        wp_register_script(CMS_THEME_NAME.'_admin_box', get_bloginfo('template_url').'/cms/lib/js/box/tinybox.js', array('jquery'), '');
        wp_register_script(CMS_THEME_NAME.'_htmleditor', get_bloginfo('template_url').'/cms/js/htmleditor.js', array('quicktags'), '');
        
        wp_enqueue_script(CMS_THEME_NAME.'_admin_color_picker');         
        wp_enqueue_script(CMS_THEME_NAME.'_admin_js');
        wp_enqueue_script(CMS_THEME_NAME.'_admin_box');
        wp_enqueue_script(CMS_THEME_NAME.'_htmleditor'); 
                                     
        if(CMS_TURN_OFF_AUTOSAVE)
        {
            wp_deregister_script('autosave');
        }                      
    } 

    public function loadAdminStyles() 
    {
        wp_enqueue_style('thickbox');
        wp_enqueue_style(CMS_THEME_NAME.'_admin_css', get_bloginfo('template_url').'/cms/css/cms.css', false);
    }    
    

    public function loadScripts()
    {
        if (!is_admin())
        {          
           // wp_register_script( 'cms_easing', get_bloginfo("template_url").'/lib/js/jquery.easing.1.2.js', array('jquery'), ''); 
            
            $fonttype = $this->getFontType(); 
            if($fonttype == CMS_FONT_TYPE_CUFON)
            {
                wp_register_script( 'cms_cufon_lib', get_bloginfo("template_url").'/lib/js/cufon-yui.js', array('jquery'), '');
            }
            
            if($this->getIGeneral()->showClientPanel()) 
            {                         
                wp_register_script( 'cms_admin_color_picker', get_bloginfo('template_url').'/cms/lib/js/color/jscolor.js', array('jquery'), '');
            }
            
            $is_homepage = is_page_template('homepage.php'); 
            $is_accordion = false;
            $is_progress = false; 
            $slider_type = $this->getIGeneral()->getSliderType();
            
            if($is_homepage)
            {
                if($this->getIGeneral()->showClientPanel())
                {
                    $client_slider_type = $this->getIClientPanel()->getValue('slider_type');                
                    if($client_slider_type !== null)
                    {
                        $slider_type = $client_slider_type;    
                    }
                }                
                
                if($slider_type == CPGeneral::ACCORDION_SLIDER) { $is_accordion = true; }
                if($slider_type == CPGeneral::PROGRESS_SLIDER) { $is_progress = true; } 
            }
            
            if($is_accordion)
            {
                wp_register_script( 'cms_accordion', get_bloginfo('template_url').'/js/slider_accordion.js', array('jquery'), '');
            }
            if($is_progress) 
            {
                wp_register_script( 'cms_progress', get_bloginfo('template_url').'/js/slider_progress.js', array('jquery'), '');
            }
            
            //wp_register_script( 'cms_hoverintent', get_bloginfo("template_url").'/lib/js/jquery.hoverIntent.js', array('jquery'), ''); 
            wp_register_script( 'cms_superfish', get_bloginfo("template_url").'/lib/js/superfish.js', array('jquery'), '');
            wp_register_script( 'cms_pphoto', get_bloginfo("template_url").'/lib/js/jquery.prettyPhoto.js', array('jquery'), '');

           // wp_register_script( 'cms_dc_tips', get_bloginfo("template_url").'/lib/js/jquery-dc-tips.1.0.js', array('jquery'), ''); 
            wp_register_script( 'cms_common', get_bloginfo("template_url").'/js/common.js', array('jquery'), '');
           // wp_register_script( 'cms_swfobject', get_bloginfo("template_url").'/lib/js/swfobject/swfobject.js', array('jquery'), '');
            
 
            wp_dequeue_script('swfobject');
            wp_enqueue_script('jquery');          
            
          //  wp_enqueue_script('cms_easing');
            if($is_accordion)
            {
                wp_enqueue_script('cms_accordion');
            }
            if($is_progress)
            {
                wp_enqueue_script('cms_progress');
            }
            if($this->getIGeneral()->showClientPanel()) 
            {                
                wp_enqueue_script('cms_admin_color_picker');
            }
            if($fonttype == CMS_FONT_TYPE_CUFON)
            {
                wp_enqueue_script( 'cms_cufon_lib');
            }             
                
            //wp_enqueue_script('cms_hoverintent');
            wp_enqueue_script('cms_superfish');
            wp_enqueue_script('cms_pphoto');
       //     wp_enqueue_script('cms_dc_tips');
            wp_enqueue_script('cms_common');
          //  wp_enqueue_script('cms_swfobject'); 
                       
        }          
    }    
    
    public function showOptions()
    {
        switch($_GET['page'])
        {
            case 'dc-theme-opt':
                echo '<div class="wrap"><div id="icon-tools" class="icon32"></div>
                <h2>'.CMS_THEME_NAME.' - General Options</h2>';             
                $this->renderNavigation('General');
                $this->getIGeneral()->renderTab();
                echo '</div>';             
            break;

            case 'dc-theme-opt-fobjects':
                echo '<div class="wrap"><div id="icon-tools" class="icon32"></div>
                <h2>'.CMS_THEME_NAME.' - Floating Objects</h2>';             
                $this->renderNavigation('Floating objects');
                $this->getIFloatingObjects()->renderTab();
                echo '</div>';             
            break;
            
            case 'dc-theme-opt-home':
                echo '<div class="wrap"><div id="icon-tools" class="icon32"></div>
                <h2>'.CMS_THEME_NAME.' - Home Options</h2>';    
                $this->renderNavigation('Home');
                $this->getIHome()->renderTab();                            
                echo '</div>';            
            break;

            case 'dc-theme-opt-quick-news':
                echo '<div class="wrap"><div id="icon-tools" class="icon32"></div>
                <h2>'.CMS_THEME_NAME.' - Quick News</h2>';    
                $this->renderNavigation('Quick news');
                $this->getIQuickNewsPanel()->renderTab();                            
                echo '</div>';            
            break;
            
            case 'dc-theme-opt-accordion-slider':
                echo '<div class="wrap"><div id="icon-tools" class="icon32"></div>
                <h2>'.CMS_THEME_NAME.' - Accordion Slider Options</h2>';    
                $this->renderNavigation('Accordion slider');
                $this->getIAccordionSlider()->renderTab();                            
                echo '</div>';            
            break; 

            case 'dc-theme-opt-progress-slider':
                echo '<div class="wrap"><div id="icon-tools" class="icon32"></div>
                <h2>'.CMS_THEME_NAME.' - Progress Slider Options</h2>';    
                $this->renderNavigation('Progress slider');
                $this->getIProgressSlider()->renderTab();                            
                echo '</div>';            
            break;                                    

            case 'dc-theme-opt-chain-slider':
                echo '<div class="wrap"><div id="icon-tools" class="icon32"></div>
                <h2>'.CMS_THEME_NAME.' - Chain Slider Options</h2>';    
                $this->renderNavigation('Chain slider');
                $this->getIChainSlider()->renderTab();                            
                echo '</div>';            
            break;  
            
            case 'dc-theme-opt-menu':
                echo '<div class="wrap"><div id="icon-tools" class="icon32"></div>
                <h2>'.CMS_THEME_NAME.' - Menu Options</h2>';            
                $this->renderNavigation('Menu');  
                $this->getIMenu()->renderTab();                                     
                echo '</div>';             
            break;       
            
            case 'dc-theme-opt-help':
                echo '<div class="wrap"><div id="icon-tools" class="icon32"></div>
                <h2>'.CMS_THEME_NAME.' - Help</h2>';    
                $this->renderNavigation('ID-Help');
                $this->getIHelp()->renderTab();                            
                echo '</div>';            
            break;                         
                      
        }
    }
    
    /*********************************************************** 
    * Private functions
    ************************************************************/  
        
    private function renderNavigation($selected=null)
    {
        $out = '';
         $out .= '    
        <div id="cms-tabs">
            <ul>';
            
            foreach($this->_navigaton as $key => $value)
            {
                $out .= '<li ';
                if($selected !== null)
                {
                    if($selected == $key)
                    {
                       $out .= ' class="cms-tabs-selected" '; 
                    }
                } 
                $out .= ' ><a href="'.admin_url().'admin.php?page='.$value.'">'.$key.'</a></li>';   
            }
            $out .= '</ul>
            <div style="clear:both;margin-bottom:0px;height:1px;"></div><hr class="cms-hr"/></div>';
            
          echo $out;           
    }        
    
} // class
 
?>
