<?php
/**********************************************************************
* MODERN ELEGANCE WORDPRESS THEME 
* (Ideal For Business And Personal Use: Portfolio or Blog)     
* 
* File name:   
*      cp_metaquestion.php
* Brief:       
*      Part of theme control panel.
* Author:      
*      DigitalCavalry
* Author URI:
*      http://themeforest.net/user/DigitalCavalry
* Contact:
*      digitalcavalry@gmail.com 
***********************************************************************/

/*********************************************************** 
* Definitions
************************************************************/

/*********************************************************** 
* Class name:
*    CPMetaQuestionOpt
* Descripton:
*    Implementation of CPMetaQuestionOpt
***********************************************************/
class CPMetaQuestionOpt extends DCC_MetaMultiple 
{            
    /*********************************************************** 
    * Constructor
    ************************************************************/
    public function __construct() 
    {
        $this->_names = array(
            'question_opt', 'question_opt_cbox', 'color', 'usecolor_cbox', 'forcecolor_cbox');   
                       
        $this->_std = array(
            'question_opt_cbox' => false,
            'color' => '#888888',
            'usecolor_cbox' => false,
            'forcecolor_cbox' => false
            );
                             
        $this->_title = 'QUESTIONS POST';
        $this->_type = 'question';
        $this->_desc = '';
    } // constructor 

    /*********************************************************** 
    * Public functions
    ************************************************************/    
    public function save($postID)
    {
        parent::save($postID);
        
        $question_views = get_post_meta($postID, 'questviews', true);
        if($question_views == '') 
        { 
            $question_views = 0;
            update_post_meta($postID, 'questviews', (int)$question_views); 
        }
                
    }
    
    public function display()
    {       
        $value = $this->initDisplay();            
                
        global $post;
        echo '<span class="cms-meta-normal">POST INFORMATION: ID='.$post->ID.'</span>';         

        // title
        $out = ''; 
        $out .= '<div class="cms-custom-field-panel ">';
        $out .= '<div class="cms-meta-bold">'.$this->_title.'</div><br />';                       
        $out .= '<input type="hidden" name="question_opt_cbox" checked="checked" />'; 
        
        // title color             
        $out .= '<span class="cms-meta-normal">Question title color</span><br />';         
        $out .= '<input style="width:70px;text-align:center;" type="text" class="colorpicker {hash:true}" value="'.$value['color'].'" name="color" /> Title color <br /><br />';       
        
        // other settings
        $out .= '<span class="cms-meta-normal">Other options:</span><br />';
        $out .= '<input type="checkbox" name="usecolor_cbox" '.$this->attrChecked($value['usecolor_cbox']).' /> Use selected color for question title<br />';
        $out .= '<input type="checkbox" name="forcecolor_cbox" '.$this->attrChecked($value['forcecolor_cbox']).' /> Use selected color even if <strong>Questions Page Template</strong> force to use own color<br />';
        $out .= '<br />';           
        
        $out .= $this->getUpdateBtnHtmlCode();
        
        $out .= '</div>';
        echo $out;
    } 
} 
        
?>