<?php
/**********************************************************************
* MODERN ELEGANCE WORDPRESS EDITION 
* (Ideal For Business And Personal Use: Portfolio or Blog)     
* 
* File name:   
*      cp_widgets.php
* Brief:       
*      Part of theme control panel.
* Author:      
*      DigitalCavalry
* Author URI:
*      http://themeforest.net/user/DigitalCavalry
* Contact:
*      digitalcavalry@gmail.com 
***********************************************************************/

/*********************************************************** 
* Definitions
************************************************************/

/*
 * Plugin Name: Advertisement 265 
 * Version: 1.0
 * Plugin URI: http://themeforest.net/user/DigitalCavalry
 * Description: Advertisement 265
 * Author: Digital Cavalry
 * Author URI: http://themeforest.net/user/DigitalCavalry
 */
 
class dcwp_advertisement265 extends WP_Widget
{
    /*********************************************************** 
    * Constructor
    ************************************************************/
    function dcwp_advertisement265()
    {
        $widget_ops = array('classname' => 'dcwp_advertisement265', 'description' => __( "Advertisement 265 (".CMS_THEME_NAME." theme)") );
        $control_ops = array('width' => 240, 'height' => 360);
        $this->WP_Widget('dcwp_advertisement265', __('dcwp_Advertisement265'), $widget_ops, $control_ops);
    }

    /*********************************************************** 
    * Public functions
    ************************************************************/
    function widget($args, $instance)
    {
        extract($args);
        $title = apply_filters('widget_title', empty($instance['title']) ? '' : $instance['title']);
        
        $url_1 = empty($instance['url_1']) ? '' : $instance['url_1'];
        $link_1 = empty($instance['link_1']) ? '' : $instance['link_1'];
    
        
        // before the widget
        echo $before_widget;
        // title
        if($title) { echo $before_title . $title . $after_title; }

        $out = '<div class="dc-widget-adv-big">';                
        

        if($url_1 != '')
        {                       
            if($link_1 != '') { $out .= '<a target="_blank" href="'.$link_1.'" >'; }
            $out .= '<img src="'.$url_1.'" />';
            if($link_1 != '') { $out .= '</a>'; }
        }                                         
     
        $out .= '</div>';
        echo $out;

        // after the widget
        echo $after_widget;
    } // widget

    function update($new_instance, $old_instance)
    {
        $instance = $old_instance;
        $instance['title'] = strip_tags(stripslashes($new_instance['title']));
        
        $instance['url_1'] = strip_tags(stripslashes($new_instance['url_1']));
        $instance['link_1'] = strip_tags(stripslashes($new_instance['link_1']));      
        
        return $instance;
    } // update

    function form($instance)
    {
      //Defaults
      $instance = wp_parse_args( (array) $instance, 
        array('title'=>'', 'big_title'=>false, 'url_1'=>'', 'link_1'=>'') );

      $title = htmlspecialchars($instance['title']);

      
      $url_1 = htmlspecialchars($instance['url_1']);
      $link_1 = htmlspecialchars($instance['link_1']); 
      
      # Output the options
      echo '<p style="text-align:left;font-size:10px;"><label for="' . $this->get_field_name('title') . '">' . __('Title:') . ' <input style="width: 220px;" id="' . $this->get_field_id('title') . '" name="' . $this->get_field_name('title') . '" type="text" value="' . $title . '" /></label></p>';
      
      # Advert 1 image url
      echo '<p style="text-align:left;font-size:10px;margin-bottom:0px;">Image URL (265x px):<br/><input style="width: 220px;" id="' . $this->get_field_id('url_1') . '" name="' . $this->get_field_name('url_1') . '" type="text" value="' . $url_1 . '" /></p>';
      # Advert 1 link
      echo '<p style="text-align:left;font-size:10px;margin-bottom:30px;"><label for="' . $this->get_field_name('link_1') . '">' . __('Link:') . ' <input style="width: 220px;" id="' . $this->get_field_id('link_1') . '" name="' . $this->get_field_name('link_1') . '" type="text" value="' . $link_1 . '" /></label></p>';         
    
    } // form
}

// Register widget
function dcwp_advertisement265Init() { register_widget('dcwp_advertisement265'); }
add_action('widgets_init', 'dcwp_advertisement265Init');        
  
  
/*
 * Plugin Name: Gallery
 * Version: 1.0
 * Plugin URI: http://themeforest.net/user/DigitalCavalry
 * Description: Gallery
 * Author: Digital Cavalry
 * Author URI: http://themeforest.net/user/DigitalCavalry
 */

class dcwp_gallery extends WP_Widget 
{

   function dcwp_gallery() {
       $widget_ops = array('classname' => 'dcwp_gallery', 'description' => "Your images gallery (".CMS_THEME_NAME." theme)");
       $this->WP_Widget('dcwp_gallery', 'dcwp_Gallery', $widget_ops);
   }

   function widget( $args, $instance ) {
       extract($args);
       $title = apply_filters('widget_title', empty($instance['title']) ? '' : $instance['title'], $instance, $this->id_base);
       $gallery = isset($instance['gallery']) ? $instance['gallery'] : CMS_NOT_SELECTED;
       $color = !empty($instance['color']) ? $instance['color'] : '#FFCC00';
       $url = !empty($instance['url']) ? $instance['url'] : '';
       $trans = !empty($instance['trans']) ? $instance['trans'] : 'fade';
       $number = !empty($instance['number']) ? $instance['number'] : 1;
       $height = !empty($instance['height']) ? $instance['height'] : 180; 
       $pageid = !empty($instance['pageid']) ? $instance['pageid'] : CMS_NOT_SELECTED; 
       
       $desc = empty($instance['show_desc']) ? false : true;           
       $desc = $desc ? 'true' : 'false';

       $autoplay = empty($instance['autoplay']) ? false : true;           
       $autoplay = $autoplay ? 'true' : 'false';

       $random = empty($instance['random']) ? false : true;           
       $random = $random ? 'true' : 'false';
      
      echo $before_widget;
      if($title != '')
      {
         echo $before_title . $title . $after_title;
      }

      $gall_content = '[dcs_chain_gallery margin="0px 0px 0px 0px" trans="'.$trans.'" pageid="'.$pageid.'" url="'.$url.'" id="'.$gallery.'" random="'.$random.'" desc="'.$desc.'" autoplay="'.$autoplay.'" bcolor="'.$color.'" number="'.$number.'" tw="31" th="30" h="'.$height.'" w="260"]';
      $code = do_shortcode($gall_content);
      echo $code;
      
      echo $after_widget;
  }

  function update( $new_instance, $old_instance ) {
      $instance['title'] = strip_tags(stripslashes($new_instance['title']));
      $instance['gallery'] = $new_instance['gallery'];
      $instance['show_desc'] = empty($new_instance['show_desc']) ? 0 : 1;
      $instance['autoplay'] = empty($new_instance['autoplay']) ? 0 : 1;
      $instance['random'] = empty($new_instance['random']) ? 0 : 1;
      $instance['color'] = $new_instance['color'];
      $instance['trans'] = $new_instance['trans']; 
      $instance['number'] = $new_instance['number'];
      $instance['height'] = $new_instance['height'];
      $instance['url'] = $new_instance['url'];
      $instance['pageid'] = $new_instance['pageid'];              
      return $instance;
  }

  function form( $instance ) {
  $instance = wp_parse_args( (array) $instance, 
    array('color'=>'#FFCC00','number'=>10,'height'=>180,'trans'=>'fade' ));
    
      
      $gallery = $instance['gallery'];
      $color = $instance['color'];
      $number = $instance['number'];
      $trans = $instance['trans'];
      $height = $instance['height'];  
      $url = $instance['url'];
      $pageid = $instance['pageid'];
      $show_desc = isset($instance['show_desc']) ? (bool) $instance['show_desc'] :false;
      $autoplay = isset($instance['autoplay']) ? (bool) $instance['autoplay'] :false;
      $random = isset($instance['random']) ? (bool) $instance['random'] :false;  
?>
  <p><label for="<?php echo $this->get_field_id('title'); ?>">Title:</label>
  <input type="text" class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" value="<?php if (isset ( $instance['title'])) {echo esc_attr( $instance['title'] );} ?>" /></p>
  <?php
  # show image description
  echo '<p style="text-align:left;font-size:10px;"><input type="checkbox" '.($show_desc ? ' checked="checked" ' : '').' id="' . 
  $this->get_field_id('show_desc') . '" name="' . $this->get_field_name('show_desc'). '" /> Show image description</p>';        
  # autoplay
  echo '<p style="text-align:left;font-size:10px;"><input type="checkbox" '.($autoplay ? ' checked="checked" ' : '').' id="' . 
  $this->get_field_id('autoplay') . '" name="' . $this->get_field_name('autoplay'). '" /> Autoplay</p>';  
  # random
  echo '<p style="text-align:left;font-size:10px;"><input type="checkbox" '.($random ? ' checked="checked" ' : '').' id="' . 
  $this->get_field_id('random') . '" name="' . $this->get_field_name('random'). '" /> Random images</p>'; 
  # number
  echo '<p style="text-align:left;font-size:10px;"><input type="text" value="'.$number.'" id="' . 
  $this->get_field_id('number') . '" name="' . $this->get_field_name('number'). '" /> Images number</p>';
  # height
  echo '<p style="text-align:left;font-size:10px;"><input type="text" value="'.$height.'" id="' . 
  $this->get_field_id('height') . '" name="' . $this->get_field_name('height'). '" /> Slider height in pixels</p>';      
  # color
  echo '<p style="text-align:left;font-size:10px;"><input type="text" class="colorpicker {hash:true}" value="'.$color.'" id="' . 
  $this->get_field_id('color') . '" name="' . $this->get_field_name('color'). '" /> Thumb border color eg. #FFCC00</p>';
  # url
  echo '<p style="text-align:left;font-size:10px;">Link path URL:<br /><input type="text" style="width:220px;" value="'.$url.'" id="' . 
  $this->get_field_id('url') . '" name="' . $this->get_field_name('url'). '" /></p>';                 
  
  $out = '<p style="text-align:left;font-size:10px;">Select page for link:<br />';
  $out .= $this->selectCtrlPagesList($pageid, $this->get_field_name('pageid'), 220);
  $out .= '</p>';
  echo $out;

    $out = '';
    $out .= '<p style="text-align:left;font-size:10px;"><label>Choose transition mode</label>';        

    $modes = Array();
    $modes['none'] = 'Switch without transition'; 
    $modes['fade'] = 'Fade images';    
    $modes['slide'] = 'Slide images';    
        
    $out .= '<select style="width:220px;" id="'.$this->get_field_id('trans').'" name="'.$this->get_field_name('trans').'">';
    foreach($modes as $key => $mode)
    {
        $out .= '<option ';
        $out .= ' value="'.$key.'" ';
        $out .= $trans == $key ? ' selected="selected" ' : '';
        $out .= '>'.$mode;
        $out .= '</option>';
    }
    $out .= '</select>';  
    $out .= '</p>';
    echo $out;
  
        
    $out = '';
    $out .= '<p style="text-align:left;font-size:10px;"><label>Choose gallery</label>';        
    global $nggdb;
    if(isset($nggdb))
    {
        $gallerylist = $nggdb->find_all_galleries('gid', 'DESC');
        
        
        $out .= '<select style="width:220px;" id="'.$this->get_field_id('gallery').'" name="'.$this->get_field_name('gallery').'">';
        $out .= '<option value="'.CMS_NOT_SELECTED.'" '.($value == CMS_NOT_SELECTED ? ' selected="selected" ' : '').' >Not selected</option>';
        foreach($gallerylist as $gal)
        {
            $out .= '<option ';
            $out .= ' value="'.$gal->gid.'" ';
            $out .= $gallery == $gal->gid ? ' selected="selected" ' : '';
            $out .= '>'.$gal->title;
            $out .= '</option>';
        }
        $out .= '</select>';  
          
    } else
    {
        $out .= '<span style="color:#880000;font-size:10px;">Can\'t find NextGen Gallery Plugin</span>'; 
    }
    $out .= '</p>';
    echo $out;
}  

public function selectCtrlPagesList($itempage, $name, $width)
{
    $out = '';
    $out .= '<select style="width:'.$width.'px;" name="'.$name.'"><option value="'.CMS_NOT_SELECTED.'">Not selected</option>';
    
    global $wpdb;
    $pages = $wpdb->get_results("SELECT ID, post_title FROM $wpdb->posts WHERE post_status = 'publish' AND post_type = 'page' ORDER BY post_title ");
    
    $count = count($pages);
    for($i = 0; $i < $count; $i++)
    {

        $out .= '<option value="'.$pages[$i]->ID.'" ';
        
        if($itempage !== null)
        {
            if($pages[$i]->ID == $itempage) $out .= ' selected="selected" ';
        }
        
        $out .= '>';
        $out .= $pages[$i]->post_title;
        $out .= '</option>';
           
    } // for        
    $out .= '</select>';
    return $out;     
} 

}

// Register widget
function dcwp_galleryInit() { register_widget('dcwp_gallery'); }
add_action('widgets_init', 'dcwp_galleryInit');
  

/*
 * Plugin Name: Advertisement 125 
 * Version: 1.0
 * Plugin URI: http://themeforest.net/user/DigitalCavalry
 * Description: Advertisement 125
 * Author: Digital Cavalry
 * Author URI: http://themeforest.net/user/DigitalCavalry
 */
 
class dcwp_advertisement125 extends WP_Widget
{
    /*********************************************************** 
    * Constructor
    ************************************************************/
    function dcwp_advertisement125()
    {
        $widget_ops = array('classname' => 'dcwp_advertisement125', 'description' => "Advertisement 125 (".CMS_THEME_NAME." theme)" );
        $control_ops = array('width' => 240, 'height' => 360);
        $this->WP_Widget('dcwp_advertisement125', 'dcwp_Advertisement125', $widget_ops, $control_ops);
    }

    /*********************************************************** 
    * Public functions
    ************************************************************/
    function widget($args, $instance)
    {
        extract($args);
        $title = apply_filters('widget_title', empty($instance['title']) ? '' : $instance['title']);
        
        $url_1 = empty($instance['url_1']) ? '' : $instance['url_1'];
        $link_1 = empty($instance['link_1']) ? '' : $instance['link_1'];
        $url_2 = empty($instance['url_2']) ? '' : $instance['url_2'];
        $link_2 = empty($instance['link_2']) ? '' : $instance['link_2'];
        $url_3 = empty($instance['url_3']) ? '' : $instance['url_3'];
        $link_3 = empty($instance['link_3']) ? '' : $instance['link_3'];
        $url_4 = empty($instance['url_4']) ? '' : $instance['url_4'];
        $link_4 = empty($instance['link_4']) ? '' : $instance['link_4'];
       
        $url_5 = empty($instance['url_5']) ? '' : $instance['url_5'];
        $link_5 = empty($instance['link_5']) ? '' : $instance['link_5'];
        $url_6 = empty($instance['url_6']) ? '' : $instance['url_6'];
        $link_6 = empty($instance['link_6']) ? '' : $instance['link_6'];        

        $url_7 = empty($instance['url_7']) ? '' : $instance['url_7'];
        $link_7 = empty($instance['link_7']) ? '' : $instance['link_7'];
        $url_8 = empty($instance['url_8']) ? '' : $instance['url_8'];
        $link_8 = empty($instance['link_8']) ? '' : $instance['link_8'];  
        
        // before the widget
        echo $before_widget;
        // title
        if($title) { echo $before_title . $title . $after_title; }

        $out = '<div class="dc-widget-adv-125">';                
        
        $rendered = 0;
        if($url_1 != '')
        {                       
            $out .= '<div class="item" style="'.(($rendered % 2) == 0 ? 'float:left;' : 'float:right;').'">';
            if($link_1 != '') { $out .= '<a target="_blank" href="'.$link_1.'" >'; }
            $out .= '<img src="'.$url_1.'" />';
            if($link_1 != '') { $out .= '</a>'; }
            $out .= '</div>';
            
            $rendered++;
        }                                         
        if($url_2 != '')
        {
            
            $out .= '<div class="item" style="'.(($rendered % 2) == 0 ? 'float:left;' : 'float:right;').'">';
            if($link_2 != '') { $out .= '<a target="_blank" href="'.$link_2.'" >'; }
            $out .= '<img src="'.$url_2.'" />';
            if($link_2 != '') { $out .= '</a>'; }
            $out .= '</div>';
            
            $rendered++;
        } 
        if($url_3 != '')
        {
            
            if(($rendered % 2) == 0)
            {
                $out .= '<div class="separator"></div>';
            }             
            $out .= '<div class="item" style="'.(($rendered % 2) == 0 ? 'float:left;' : 'float:right;').'">';
            if($link_3 != '') { $out .= '<a target="_blank" href="'.$link_3.'" >'; }
            $out .= '<img src="'.$url_3.'" />';
            if($link_3 != '') { $out .= '</a>'; }
            $out .= '</div>';
            
            $rendered++;
        }         
        if($url_4 != '')
        {
           
            if(($rendered % 2) == 0)
            {
                $out .= '<div class="separator"></div>';
            }              
            $out .= '<div class="item" style="'.(($rendered % 2) == 0 ? 'float:left;' : 'float:right;').'">';
            if($link_4 != '') { $out .= '<a target="_blank" href="'.$link_4.'" >'; }
            $out .= '<img src="'.$url_4.'" />';
            if($link_4 != '') { $out .= '</a>'; }
            $out .= '</div>';
            
            $rendered++;
        } 
        if($url_5 != '')
        {
            if(($rendered % 2) == 0)
            {
                $out .= '<div class="separator"></div>';
            }              
            $out .= '<div class="item" style="'.(($rendered % 2) == 0 ? 'float:left;' : 'float:right;').'">';
            if($link_5 != '') { $out .= '<a target="_blank" href="'.$link_5.'" >'; }
            $out .= '<img src="'.$url_5.'" />';
            if($link_5 != '') { $out .= '</a>'; }
            $out .= '</div>';
            
            $rendered++;
        }         
        if($url_6 != '')
        {
            if(($rendered % 2) == 0)
            {
                $out .= '<div class="separator"></div>';
            }              
            $out .= '<div class="item" style="'.(($rendered % 2) == 0 ? 'float:left;' : 'float:right;').'">';
            if($link_6 != '') { $out .= '<a target="_blank" href="'.$link_6.'" >'; }
            $out .= '<img src="'.$url_6.'" />';
            if($link_6 != '') { $out .= '</a>'; }
            $out .= '</div>';
            
            $rendered++;
        }       
        if($url_7 != '')
        {
            if(($rendered % 2) == 0)
            {
                $out .= '<div class="separator"></div>';
            }              
            $out .= '<div class="item" style="'.(($rendered % 2) == 0 ? 'float:left;' : 'float:right;').'">';
            if($link_7 != '') { $out .= '<a target="_blank" href="'.$link_7.'" >'; }
            $out .= '<img src="'.$url_7.'" />';
            if($link_7 != '') { $out .= '</a>'; }
            $out .= '</div>';
            
            $rendered++;
        }         
        if($url_8 != '')
        {
            if(($rendered % 2) == 0)
            {
                $out .= '<div class="separator"></div>';
            }              
            $out .= '<div class="item" style="'.(($rendered % 2) == 0 ? 'float:left;' : 'float:right;').'">';
            if($link_8 != '') { $out .= '<a target="_blank" href="'.$link_8.'" >'; }
            $out .= '<img src="'.$url_8.'" />';
            if($link_8 != '') { $out .= '</a>'; }
            $out .= '</div>';
            
            $rendered++;
        }  
        
        
        $out .= '<div style="clear:both;"></div></div>';
        echo $out;

        // after the widget
        echo $after_widget;
    } // widget

    function update($new_instance, $old_instance)
    {
        $instance = $old_instance;
        $instance['title'] = strip_tags(stripslashes($new_instance['title']));
        
        $instance['url_1'] = strip_tags(stripslashes($new_instance['url_1']));
        $instance['link_1'] = strip_tags(stripslashes($new_instance['link_1']));      
        $instance['url_2'] = strip_tags(stripslashes($new_instance['url_2']));
        $instance['link_2'] = strip_tags(stripslashes($new_instance['link_2'])); 
        $instance['url_3'] = strip_tags(stripslashes($new_instance['url_3']));
        $instance['link_3'] = strip_tags(stripslashes($new_instance['link_3'])); 
        $instance['url_4'] = strip_tags(stripslashes($new_instance['url_4']));
        $instance['link_4'] = strip_tags(stripslashes($new_instance['link_4']));                 

        $instance['url_5'] = strip_tags(stripslashes($new_instance['url_5']));
        $instance['link_5'] = strip_tags(stripslashes($new_instance['link_5'])); 
        $instance['url_6'] = strip_tags(stripslashes($new_instance['url_6']));
        $instance['link_6'] = strip_tags(stripslashes($new_instance['link_6']));  

        $instance['url_7'] = strip_tags(stripslashes($new_instance['url_7']));
        $instance['link_7'] = strip_tags(stripslashes($new_instance['link_7'])); 
        $instance['url_8'] = strip_tags(stripslashes($new_instance['url_8']));
        $instance['link_8'] = strip_tags(stripslashes($new_instance['link_8'])); 
        
        return $instance;
    } // update

    function form($instance)
    {
      //Defaults
      $instance = wp_parse_args( (array) $instance, 
        array('title'=>'', 'big_title'=>false, 'url_1'=>'', 'link_1'=>'', 'url_2'=>'', 'link_2'=>'',
         'url_3'=>'', 'link_3'=>'', 'url_4'=>'', 'link_4'=>'', 'url_5'=>'', 'link_5'=>'', 'url_6'=>'', 'link_6'=>'', 'url_7'=>'', 'link_7'=>'', 'url_8'=>'', 'link_8'=>'') );

      $title = htmlspecialchars($instance['title']);

      
      $url_1 = htmlspecialchars($instance['url_1']);
      $link_1 = htmlspecialchars($instance['link_1']); 

      $url_2 = htmlspecialchars($instance['url_2']);
      $link_2 = htmlspecialchars($instance['link_2']); 
      
      $url_3 = htmlspecialchars($instance['url_3']);
      $link_3 = htmlspecialchars($instance['link_3']); 
      
      $url_4 = htmlspecialchars($instance['url_4']);
      $link_4 = htmlspecialchars($instance['link_4']);             

      $url_5 = htmlspecialchars($instance['url_5']);
      $link_5 = htmlspecialchars($instance['link_5']); 
      
      $url_6 = htmlspecialchars($instance['url_6']);
      $link_6 = htmlspecialchars($instance['link_6']);    

      $url_7 = htmlspecialchars($instance['url_7']);
      $link_7 = htmlspecialchars($instance['link_7']); 
      
      $url_8 = htmlspecialchars($instance['url_8']);
      $link_8 = htmlspecialchars($instance['link_8']);  
      
      # Output the options
      echo '<p style="text-align:left;font-size:10px;"><label for="' . $this->get_field_name('title') . '">' .'Title:'. ' <input style="width: 220px;" id="' . $this->get_field_id('title') . '" name="' . $this->get_field_name('title') . '" type="text" value="' . $title . '" /></label></p>';
      
      # Advert 1 image url
      echo '<p style="text-align:left;font-size:10px;margin-bottom:0px;">#1 Image URL (125x125px):<br/><input style="width: 220px;" id="' . $this->get_field_id('url_1') . '" name="' . $this->get_field_name('url_1') . '" type="text" value="' . $url_1 . '" /></p>';
      # Advert 1 link
      echo '<p style="text-align:left;font-size:10px;margin-bottom:30px;"><label for="' . $this->get_field_name('link_1') . '">' .'Link:'. ' <input style="width: 220px;" id="' . $this->get_field_id('link_1') . '" name="' . $this->get_field_name('link_1') . '" type="text" value="' . $link_1 . '" /></label></p>';      

      # Advert 2 image url
      echo '<p style="text-align:left;font-size:10px;margin-bottom:0px;"><label for="' . $this->get_field_name('url_2') . '">' .'#2 Image URL (125x125px):'. ' <input style="width: 220px;" id="' . $this->get_field_id('url_2') . '" name="' . $this->get_field_name('url_2') . '" type="text" value="' . $url_2 . '" /></label></p>';
      # Advert 2 link
      echo '<p style="text-align:left;font-size:10px;margin-bottom:30px;"><label for="' . $this->get_field_name('link_2') . '">' .'Link 2:'. ' <input style="width: 220px;" id="' . $this->get_field_id('link_2') . '" name="' . $this->get_field_name('link_2') . '" type="text" value="' . $link_2 . '" /></label></p>';            
      
      # Advert 3 image url
      echo '<p style="text-align:left;font-size:10px;margin-bottom:0px;"><label for="' . $this->get_field_name('url_3') . '">' .'#3 Image URL (125x125px):'. ' <input style="width: 220px;" id="' . $this->get_field_id('url_3') . '" name="' . $this->get_field_name('url_3') . '" type="text" value="' . $url_3 . '" /></label></p>';
      # Advert 3 link
      echo '<p style="text-align:left;font-size:10px;margin-bottom:30px;"><label for="' . $this->get_field_name('link_3') . '">' .'Link:'. ' <input style="width: 220px;" id="' . $this->get_field_id('link_3') . '" name="' . $this->get_field_name('link_3') . '" type="text" value="' . $link_3 . '" /></label></p>';      

      # Advert 4 image url
      echo '<p style="text-align:left;font-size:10px;margin-bottom:0px;"><label for="' . $this->get_field_name('url_4') . '">' .'#4 Image URL (125x125px):'. ' <input style="width: 220px;" id="' . $this->get_field_id('url_4') . '" name="' . $this->get_field_name('url_4') . '" type="text" value="' . $url_4 . '" /></label></p>';
      # Advert 4 link
      echo '<p style="text-align:left;font-size:10px;margin-bottom:30px;"><label for="' . $this->get_field_name('link_4') . '">' .'Link:'. ' <input style="width: 220px;" id="' . $this->get_field_id('link_4') . '" name="' . $this->get_field_name('link_4') . '" type="text" value="' . $link_4 . '" /></label></p>';        

      # Advert 5 image url
      echo '<p style="text-align:left;font-size:10px;margin-bottom:0px;"><label for="' . $this->get_field_name('url_5') . '">' .'#5 Image URL (125x125px):'. ' <input style="width: 220px;" id="' . $this->get_field_id('url_5') . '" name="' . $this->get_field_name('url_5') . '" type="text" value="' . $url_5 . '" /></label></p>';
      # Advert 5 link
      echo '<p style="text-align:left;font-size:10px;margin-bottom:30px;"><label for="' . $this->get_field_name('link_5') . '">' .'Link:'. ' <input style="width: 220px;" id="' . $this->get_field_id('link_5') . '" name="' . $this->get_field_name('link_5') . '" type="text" value="' . $link_5 . '" /></label></p>';      

      # Advert 6 image url                                                                                                             
      echo '<p style="text-align:left;font-size:10px;margin-bottom:0px;"><label for="' . $this->get_field_name('url_6') . '">' .'#6 Image URL (125x125px):'. ' <input style="width: 220px;" id="' . $this->get_field_id('url_6') . '" name="' . $this->get_field_name('url_6') . '" type="text" value="' . $url_6 . '" /></label></p>';
      # Advert 6 link
      echo '<p style="text-align:left;font-size:10px;margin-bottom:30px;"><label for="' . $this->get_field_name('link_6') . '">' .'Link:'. ' <input style="width: 220px;" id="' . $this->get_field_id('link_6') . '" name="' . $this->get_field_name('link_6') . '" type="text" value="' . $link_6 . '" /></label></p>';            

      # Advert 7 image url
      echo '<p style="text-align:left;font-size:10px;margin-bottom:0px;"><label for="' . $this->get_field_name('url_7') . '">' .'#7 Image URL (125x125px):'. ' <input style="width: 220px;" id="' . $this->get_field_id('url_7') . '" name="' . $this->get_field_name('url_7') . '" type="text" value="' . $url_7 . '" /></label></p>';
      # Advert 7 link
      echo '<p style="text-align:left;font-size:10px;margin-bottom:30px;"><label for="' . $this->get_field_name('link_7') . '">' .'Link:'. ' <input style="width: 220px;" id="' . $this->get_field_id('link_7') . '" name="' . $this->get_field_name('link_7') . '" type="text" value="' . $link_7 . '" /></label></p>';      

      # Advert 8 image url                                                                                                             
      echo '<p style="text-align:left;font-size:10px;margin-bottom:0px;"><label for="' . $this->get_field_name('url_8') . '">' . '#8 Image URL (125x125px):'. ' <input style="width: 220px;" id="' . $this->get_field_id('url_8') . '" name="' . $this->get_field_name('url_8') . '" type="text" value="' . $url_8 . '" /></label></p>';
      # Advert 8 link
      echo '<p style="text-align:left;font-size:10px;margin-bottom:30px;"><label for="' . $this->get_field_name('link_8') . '">' .'Link:'. ' <input style="width: 220px;" id="' . $this->get_field_id('link_8') . '" name="' . $this->get_field_name('link_8') . '" type="text" value="' . $link_8 . '" /></label></p>'; 
    
    } // form
}

  // Register widget
  function dcwp_advertisement125Init() { register_widget('dcwp_advertisement125'); }
  add_action('widgets_init', 'dcwp_advertisement125Init');  
  
  
/*
 * Plugin Name: Search 
 * Version: 1.0
 * Plugin URI: http://themeforest.net/user/DigitalCavalry
 * Description: Search
 * Author: Digital Cavalry
 * Author URI: http://themeforest.net/user/DigitalCavalry
 */  

class dcwp_search extends WP_Widget {

    function dcwp_search() {
        $widget_ops = array('classname' => 'dcwp_search', 'description' => __( "Search (".CMS_THEME_NAME." theme)" ));
        $this->WP_Widget('dcwp_search', __('dcwp_Search'), $widget_ops);
    }

    function widget( $args, $instance ) {
        extract($args);
        $title = apply_filters('widget_title', $instance['title'], $instance, $this->id_base);

        echo $before_widget;
        if($title)
        {
            echo $before_title . $title . $after_title;
        }
        
        // Use current theme search form if it exists
        $out = '';        
        $out .= '<div class="dc-widget-search">';
            $out .= '<form role="search" method="get" id="searchform" action="'.get_bloginfo('url').'">';
                $out .= '<input type="text" value="" name="s" id="s">';
                $out .= '<a class="submit-btn"></a>';
            $out .= '</form>';
            $out .= '<div class="clear-both"></div>';
        $out .= '</div>';                
        echo $out;         
        echo $after_widget;
    }

    function form( $instance ) 
    {
        $instance = wp_parse_args( (array) $instance, array( 'title' => '') );
        $title = $instance['title'];

        $out = '';
        $out .= '<p><label for="'; 
            $out .= $this->get_field_id('title'); 
        $out .= '">';
        $out .= 'Title: <input class="widefat" id="';
        $out .= $this->get_field_id('title'); 
        $out .= '" name="';
        $out .= $this->get_field_name('title'); 
        $out .= '" type="text" value="'; 
        $out .= esc_attr($title); 
        $out .= '" /></label></p>';
        
        echo $out;
    }

    function update( $new_instance, $old_instance ) 
    {
        $instance = $old_instance;
        $new_instance = wp_parse_args((array) $new_instance, array( 'title' => ''));
        $instance['title'] = strip_tags($new_instance['title']);
        return $instance;
    }

}

  // Register widget
  function dcwp_searchInit() { register_widget('dcwp_search'); }
  add_action('widgets_init', 'dcwp_searchInit');  
  

/*
 * Plugin Name: Post Slider
 * Version: 1.0
 * Plugin URI: http://themeforest.net/user/DigitalCavalry
 * Description: Post Slider
 * Author: Digital Cavalry
 * Author URI: http://themeforest.net/user/DigitalCavalry
 */

  class dcwp_postslider extends WP_Widget {
   
       function dcwp_postslider() {
           $widget_ops = array('classname' => 'dcwp_postslider', 'description' => "Your posts slider (".CMS_THEME_NAME." theme)" );
           $this->WP_Widget('dcwp_postslider', 'dcwp_PostSlider', $widget_ops);
       }
   
       function widget( $args, $instance ) {
           extract($args);
           $title = apply_filters('widget_title', empty($instance['title']) ? '' : $instance['title'], $instance, $this->id_base);
           $category = empty($instance['category']) ? CMS_NOT_SELECTED : $instance['category'];  
           $posts_list = empty($instance['posts_list']) ? '' : $instance['posts_list']; 
           $number = empty($instance['number']) ? 5 : $instance['number'];
           $oder = empty($instance['order']) ? 'DESC' : $instance['order']; 
           $height = empty($instance['height']) ? 120 : $instance['height']; 
           
           $posts_array = array();
           if(trim($posts_list) != '')
           {
                $posts_array = explode(',', $posts_list );
           }
           $count = count($posts_array);
                                                                                                              
           $query_args = array('nopaging' => 0, 'post_status' => 'publish', 'caller_get_posts' => 1, 'order' => $oder);
           
           if($category == CMS_NOT_SELECTED)
           {
               if(count($posts_array))
               {
                    $query_args['post__in'] = $posts_array;
               }
               $query_args['posts_per_page'] = $count;
           } else
           {
               $query_args['cat'] = $category;
               $query_args['posts_per_page'] = $number;      
           }
           
           
           
          echo $before_widget;
          if($title)
          {
              echo $before_title . $title . $after_title;
          }
          $out = '';
          $out .= '<div class="dc-widget-postslider" style="height:'.$height.'px;">';           
           $out .= '<div class="slides-wrapper" style="height:'.$height.'px;">';
           $r = new WP_Query($query_args);
           if($r->post_count) 
           {
               $counter = 0; 
               $r->current_post = 0;
               while($r->current_post < $r->post_count)
               { 
                    $rp = & $r->posts[$r->current_post]; 
                    $counter++;
                    
                    $thumb = get_post_meta($rp->ID, 'post_opt', true);
                    $thumb['image_url'] = dcf_isNGGImageID($thumb['image_url']);    
                                         
                    $out .= '<div class="slide" style="height:'.$height.'px;"><img src="'.dcf_getTimThumbURL($thumb['image_url'], 253, $height).'" /><a href="'.get_permalink($rp->ID).'" class="desc">'.$rp->post_title.'</a></div>';
                    $r->current_post++;
               }
               $out .= '</div>';
               $out .= '<div class="btn-bar">';
               for($i = 0; $i < $counter; $i++)
               {
                   $out .= '<a '.($i == 0 ? ' class="btn-active" ' : ' class="btn" ').' >'.($i+1).'</a>'; 
               }                          
               $out .= '</div>';
           }    
          
          $out .= '</div>';
          echo $out;
          echo $after_widget;
      }
  
      function update( $new_instance, $old_instance ) {
          $instance['title'] = strip_tags(stripslashes($new_instance['title']));
          $instance['posts_list'] = $new_instance['posts_list'];
          $instance['number'] = (int) $new_instance['number']; 
          $instance['category'] = (int) $new_instance['category'];  
          $instance['order'] = $new_instance['order']; 
          $instance['height'] = $new_instance['height'];  
          
          return $instance;
      }
  
      function form( $instance ) 
      {
        $instance = wp_parse_args( (array) $instance, 
            array('posts_list'=>'', 'category'=>CMS_NOT_SELECTED, 'title'=>'', 'number'=>5, 'order' => 'DESC', 'height' => 120));
          
          $posts_list = $instance['posts_list'];
          $category = (int)$instance['category'];
          $title = $instance['title']; 
          $number = $instance['number'];         
          $order = $instance['order'];
          $height = $instance['height'];
            
          $out = '';
          $out .= '<p><label>Title:</label>';
          $out .= '<input type="text" class="widefat" id="'.$this->get_field_id('title').'" name="'.$this->get_field_name('title').'" value="'.$title.'" /></p>';
           
                   
          $out .= '<p><label>Posts ID e.g 1,82,6 (max 9):</label>';
          $out .= '<input class="widefat" id="'.$this->get_field_id('posts_list').'" name="'.$this->get_field_name('posts_list').'" type="text" value="'.$posts_list.'" /></p>';
          
          $out .= '<p><label>Posts order:</label><br />';
          $out .= '<input type="radio" name="'.$this->get_field_name('order').'" '.($order == 'DESC' ? ' checked="checked" ' : '').' value="DESC" /> Descending<br />';
          $out .= '<input type="radio" name="'.$this->get_field_name('order').'" '.($order == 'ASC' ? ' checked="checked" ' : '').' value="ASC" /> Ascending'; 
          $out .= '</p>'; 

          $out .= '<p><label>Slider height (default 120):</label>';
          $out .= '<input style="width:80px;text-align:center;" name="'.$this->get_field_name('height').'" type="text" value="'.$height.'" /></p>';
                                
            $out .= '<p><label>Number of posts when category is selected:</label>';
            $out .= '<input name="'.$this->get_field_name('number').'" type="text" value="'.$number.'" size="3" /></p>';        

            $out .= '<p><label>Category ID of posts to show:</label>';

            $cats = get_categories();
            $cats = array_values($cats);
            $count = count($cats);

            $out .= '<select style="width:100%;" id="'.$this->get_field_id('category').'" name="'.$this->get_field_name('category').'">';
            $out .= '<option value="'.CMS_NOT_SELECTED.'" '.($category == CMS_NOT_SELECTED ? ' selected="selected" ' : '').' >Not selected</option>';
            for($i = 0; $i < $count; $i++)
            {
                $out .= '<option value="'.$cats[$i]->term_id.'" ';
                $out .= ($category == $cats[$i]->term_id ? ' selected="selected" ' : '');
                $out .= '>'.$cats[$i]->name.'</option>';            
            }
            $out .= '</select></p>';                                                       
            
            echo $out;      
      }
  

  }

  // Register widget
  function dcwp_postsliderInit() { register_widget('dcwp_postslider'); }
  add_action('widgets_init', 'dcwp_postsliderInit');

/*
 * Plugin Name: Recent Posts
 * Version: 1.0
 * Plugin URI: http://themeforest.net/user/DigitalCavalry
 * Description: Recent Posts
 * Author: Digital Cavalry
 * Author URI: http://themeforest.net/user/DigitalCavalry
 */

class dcwp_recentposts extends WP_Widget {

   function dcwp_recentposts() {
       $widget_ops = array('classname' => 'dcwp_recentposts', 'description' => "The most recent posts on your site (".CMS_THEME_NAME." theme)" );
       $this->WP_Widget('dcwp_recentposts', 'dcwp_RecentPosts', $widget_ops);
       $this->alt_option_name = 'widget_recent_entries';
   }

   function widget($args, $instance) 
   {
       extract($args);

       $title = apply_filters('widget_title', empty($instance['title']) ? '' : $instance['title'], $instance, $this->id_base);
       if ( !$number = (int) $instance['number'] )
       {    $number = 10; }
       else if ( $number < 1 )
       {
           $number = 1;
       }
       else if ( $number > 15 )
       {
           $number = 15;
       }

       $showthumbs = $instance['showthumbs'] ? true : false;  
       $excluded = $instance['excluded'];
       if($excluded != '') { $excluded = explode(',', $instance['excluded']); };
       $cats = $instance['cats'];
       if($cats != '') { $cats = explode(',', $instance['cats']); }
       
       $query_args = array('posts_per_page' => $number, 'nopaging' => 0, 'post_status' => 'publish', 'caller_get_posts' => 1);
       if(is_array($excluded) and count($excluded))
       {
            $query_args['category__not_in'] = $excluded;
       }
       if(is_array($cats) and count($cats))
       {
            $query_args['category__in'] = $cats; 
       }
       
       $r = new WP_Query($query_args);
       if($r->post_count)
       {

           echo $before_widget;
           if ( $title ) { echo $before_title . $title . $after_title; } 
                    
           echo '<div class="dc-widget-recent-posts"><ul>';
            $r->current_post = 0;
            $counter = 0;
            while($r->current_post < $r->post_count)
            {    
                $rpost = & $r->posts[$r->current_post];           

                $out  = '<li>';
                if($r->current_post > 0)
                {
                    $out .= '<div class="separator"></div>';
                }

                $post_op = '';
                
                if($showthumbs)
                {
                    $post_op = get_post_meta($rpost->ID, 'post_opt', true);
                    $post_op['image_url'] = dcf_isNGGImageID($post_op['image_url']);
                }
                
                $out .= '<div class="item">';
                if($showthumbs)
                {
                    if($post_op['image_url'] != '')
                    { 
                        $out .= '<a href="'.get_permalink($rpost->ID).'" ><img class="image" src="'.dcf_getTimThumbURL($post_op['image_url'], 50, 50).'" /></a>';
                    }
                }
                
                $excerpt = '';
                if(!$showthumbs) { $add_style = ' style="width:auto;" '; }                                              
                if($rpost->post_excerpt != '') { $excerpt = dcf_strNWords($rpost->post_excerpt, 10); }
                $out .= '<div class="description" '.$add_style.'><span class="date">'.mysql2date('F j, Y', $rpost->post_date_gmt, true).'</span><br /><a class="title" href="'.get_permalink($rpost->ID).'" >'.$rpost->post_title.'</a>'.$excerpt.'</div>';

                $out .= '<div style="clear:left;height:1px;"></div></div>';                
                $out .= '</li>';
                echo $out;
                
                $r->current_post++;
            } 
           echo '</ul></div>';
           echo $after_widget;         
       }
   }
   
function update( $new_instance, $old_instance ) {
   $instance = $old_instance;
   $instance['title'] = strip_tags($new_instance['title']);
   $instance['number'] = (int) $new_instance['number'];
   $instance['showthumbs'] = isset($new_instance['showthumbs']);
   $instance['excluded'] = $new_instance['excluded'];  
   $instance['cats'] = $new_instance['cats'];
   
   return $instance;
}
   
       function form( $instance ) {
           $title = isset($instance['title']) ? esc_attr($instance['title']) : '';
           if ( !isset($instance['number']) || !$number = (int) $instance['number'] )
           {
               $number = 5;
           }    
           $showthumbs = $instance['showthumbs'] ? true : false;
           $excluded = $instance['excluded'];    
           $cats = $instance['cats'];
     
            $out = '';
            $out .= '
           <p><label>Title:</label>
           <input class="widefat" name="'.$this->get_field_name('title').'" type="text" value="'.$title.'" /></p>
   
           <p><label>Number of posts to show:</label>
           <input name="'.$this->get_field_name('number').'" type="text" value="'.$number.'" size="3" /></p>
  
          <p><input name="'.$this->get_field_name('showthumbs').'" type="checkbox"'.($showthumbs ? ' checked="checked" ' : '').' /> Show thumbs</p>

           <p><label>Optional comma separated cats ID:</label>
           <input class="widefat" name="'.$this->get_field_name('cats').'" type="text" value="'.$cats.'" /></p>
      
           <p><label>Excluded categories:</label>
           <input class="widefat" name="'.$this->get_field_name('excluded').'" type="text" value="'.$excluded.'" /></p>';
           echo $out;                 
       }
   }

// Register widget
function dcwp_recentpostsInit() { register_widget('dcwp_recentposts'); }
add_action('widgets_init', 'dcwp_recentpostsInit');  
 


/*
 * Plugin Name: Tags
 * Version: 1.0
 * Plugin URI: http://themeforest.net/user/DigitalCavalry
 * Description: Tags
 * Author: Digital Cavalry
 * Author URI: http://themeforest.net/user/DigitalCavalry
 */

class dcwp_tags extends WP_Widget {

   function dcwp_tags() {
       $widget_ops = array('classname' => 'dcwp_tags', 'description' => "Your most used tags in cloud format (".CMS_THEME_NAME." theme)");
       $this->WP_Widget('dcwp_tags', 'dcwp_Tags', $widget_ops);
   }

   function widget( $args, $instance ) {
       extract($args);
       $title = apply_filters('widget_title', empty($instance['title']) ? '' : $instance['title'], $instance, $this->id_base);
       
      $tags_args = array('taxonomy' => 'post_tag', 'format'=>'list', 'smallest'=>9, 'largest'=>9, 'unit'=>'px');  
      echo $before_widget;
      if ( $title )
      {
          echo $before_title . $title . $after_title;
      }
      
      echo '<div class="dc-widget-tags">';
        wp_tag_cloud($tags_args);
      echo '<div style="clear:both"></div></div>';
      echo $after_widget;
  }

  function update( $new_instance, $old_instance ) {
      $instance['title'] = strip_tags(stripslashes($new_instance['title']));
      return $instance;
  }

  function form( $instance ) {
      
      $title =  isset($instance['title']) ? $instance['title'] : '';

      $out = '';
      $out .= '<p><label>Title:</label>';
      $out .= '<input type="text" class="widefat" name="'.$this->get_field_name('title').'" value="'.$title.'" /></p>';
      echo $out;
  }


}

// Register widget
function dcwp_tagsInit() { register_widget('dcwp_tags'); }
add_action('widgets_init', 'dcwp_tagsInit'); 
 
  

/*
 * Plugin Name: Featured Posts
 * Version: 1.0
 * Plugin URI: http://themeforest.net/user/DigitalCavalry
 * Description: Featured Posts
 * Author: Digital Cavalry
 * Author URI: http://themeforest.net/user/DigitalCavalry
 */

class dcwp_featuredposts extends WP_Widget {

   function dcwp_featuredposts() {
       $widget_ops = array('classname' => 'dcwp_featuredposts', 'description' => "The featured posts from your site (".CMS_THEME_NAME." theme)");
       $this->WP_Widget('dcwp_featuredposts', 'dcwp_FeaturedPosts', $widget_ops);
       $this->alt_option_name = 'widget_featured_posts';
   }

   function widget($args, $instance) {
       extract($args);
        
       $title = apply_filters('widget_title', empty($instance['title']) ? 'Featured Posts' : $instance['title'], $instance, $this->id_base);
       $posts_list = empty($instance['posts_list']) ? '' : $instance['posts_list']; 
       
       global $wpdb;
       $dbresult = $wpdb->get_results("SELECT ID, post_title, post_content, post_excerpt, post_type FROM $wpdb->posts WHERE post_status = 'publish' AND ID IN ($posts_list)");                                                                                                      

       echo $before_widget;
       if($title) { echo $before_title . $title . $after_title; }  
       
       $out = '';
       $out .= '<div class="dc-widget-featured-posts">';
       
            if(is_array($dbresult))
            {
                $counter = 0;
                foreach($dbresult as $pt)
                {
                    if($pt->post_type == 'post')
                    {                    
                        $link = get_permalink($pt->ID);
                        if($counter > 0) { $out .= '<div class="separator"></div>'; }
                        $post_opt = get_post_meta($pt->ID, 'post_opt', true);
                        $post_opt['image_url'] = dcf_isNGGImageID($post_opt['image_url']);
                        
                        $out .= '<h3 class="title"><a href="'.$link.'">'.$pt->post_title.'</a></h3>';
                        $out .= '<a class="image async-img-s" href="'.$link.'" rel="'.dcf_getTimThumbURL($post_opt['image_url'], 253, 120).'"></a>';
                        
                        $desc = false;
                        if($pt->post_excerpt != '')
                        {
                            $desc = $pt->post_excerpt;    
                        } else
                        {
                            $pos = strpos($pt->post_content, '<!--more-->');
                            if($pos !== false)
                            {
                                $desc = substr($pt->post_content, 0, $pos);
                            }
                        }
                            
                        $out .= '<div class="description">';
                            if($desc !== false) { $out .= $desc; } else { $out .= $pt->post_content; } 
                            if($desc !== false)
                            {
                                $out .= ' <a class="more-link" href="'.$link.'">&raquo;&nbsp;'.__('Read&nbsp;more', 'dc_theme').'</a>';  
                            } 
                        $out .= '<div class="clear-both"></div></div>';                    
                        $counter++;                    
                    } else
                    if($pt->post_type == 'page')
                    {
                        $link = get_permalink($pt->ID);
                        if($counter > 0) { $out .= '<div class="separator"></div>'; }
                        $post_opt = get_post_meta($pt->ID, 'pagecommon_opt', true);
                        $post_opt['page_image'] = dcf_isNGGImageID($post_opt['page_image']);
                        
                        $out .= '<h3 class="title"><a href="'.$link.'">'.$pt->post_title.'</a></h3>';
                        $out .= '<a class="image async-img-s" href="'.$link.'" rel="'.dcf_getTimThumbURL($post_opt['page_image'], 253, 120).'"></a>';
                        
                        $desc = false;
                        if($pt->post_excerpt != '')
                        {
                            $desc = $pt->post_excerpt;    
                        } else
                        {
                            $pos = strpos($pt->post_content, '<!--more-->');
                            if($pos !== false)
                            {
                                $desc = substr($pt->post_content, 0, $pos);
                            }
                        }
                            
                        $out .= '<div class="description">';
                            if($desc !== false) { $out .= $desc; } else { $out .= $pt->post_content; } 
                            if($desc !== false)
                            {
                                $out .= ' <a class="more-link" href="'.$link.'">&raquo;&nbsp;'.__('Read&nbsp;more', 'dc_theme').'</a>';  
                            } 
                        $out .= '<div class="clear-both"></div></div>';                    
                        $counter++;                               
                    }
                }
            }
       
       $out .= '</div>';
       echo $out;
       
       echo $after_widget;                         
   }

   function update( $new_instance, $old_instance ) 
   {
       $instance = $old_instance;
       $instance['title'] = strip_tags($new_instance['title']);
       $instance['posts_list'] = $new_instance['posts_list'];

       return $instance;
   }

   function form( $instance ) 
   {
       $title = isset($instance['title']) ? esc_attr($instance['title']) : '';
       $posts_list = empty($instance['posts_list']) ? '' : $instance['posts_list'];

       $out = '';       
       $out .= '<p><label>Title:</label>';
       $out .= '<input class="widefat" name="'.$this->get_field_name('title').'" type="text" value="'.$title.'" /></p>';

       $out .= '<p><label>Posts ID e.g 1,82,6 (max 10):</label>';
       $out .= '<input class="widefat" name="'.$this->get_field_name('posts_list').'" type="text" value="'.$posts_list.'" /></p>';
       echo $out;
   }
}

// Register widget
function dcwp_featuredpostsInit() { register_widget('dcwp_featuredposts'); }
add_action('widgets_init', 'dcwp_featuredpostsInit');  
  
  

/*
 * Plugin Name: NGG Images
 * Version: 1.0
 * Plugin URI: http://themeforest.net/user/DigitalCavalry
 * Description: NGG Images
 * Author: Digital Cavalry
 * Author URI: http://themeforest.net/user/DigitalCavalry
 */

class dcwp_nggimages extends WP_Widget 
{

   function dcwp_nggimages() {
       $widget_ops = array('classname' => 'dcwp_nggimages', 'description' => "NextGEN Gallery images (".CMS_THEME_NAME." theme)");
       $this->WP_Widget('dcwp_nggimages', 'dcwp_NGGImages', $widget_ops);
       $this->alt_option_name = 'widget_ngg_images';
   }                                               

   function widget($args, $instance) {
       extract($args);
        
       $title = apply_filters('widget_title', empty($instance['title']) ? '' : $instance['title'], $instance, $this->id_base);
       $images_list = empty($instance['images_list']) ? '' : $instance['images_list'];                                                                                                       
       $size_x = empty($instance['size_x']) ? 71 : $instance['size_x'];
       $size_y = empty($instance['size_y']) ? 58 : $instance['size_y']; 
       $gallery = empty($instance['gallery']) ? CMS_NOT_SELECTED : $instance['gallery'];
       $number = empty($instance['number']) ? 9 : $instance['number'];
       $use_gallery = $instance['use_gallery'] ? true : false; 
       $use_recent = $instance['use_recent'] ? true : false; 
       $show_random = $instance['show_random'] ? true : false;
       $show_link = $instance['show_link'] ? true : false; 
       $page_link = empty($instance['page_link']) ? CMS_NOT_SELECTED : $instance['page_link'];
       $link = empty($instance['link']) ? '' : $instance['link'];
       $use_manually = $instance['use_manually'] ? true : false; 
       
       echo $before_widget;
       if($title) { echo $before_title . $title . $after_title; }  
       
       $out = '';
       $out .= '<div class="dc-widget-ngg-images">';     
       
           $pics = null;
           if($gallery != CMS_NOT_SELECTED and $use_gallery)
           {            
                if($show_random)
                {
                    $pics = dcf_getNGGRandomImages($number, $gallery);    
                } else
                {
                    $pics = dcf_getGalleryNGG($gallery, 'sortorder', 'ASC', true, $number);
                    shuffle($pics);     
                } 
           } else
           if($use_recent)
           {
               $pics = dcf_getNGGLastImages(0, $number, true);   
           } else
           if(trim($images_list) != '')
           {
                $pics = dcf_getNGGImagesFromIDList($images_list);
                shuffle($pics);   
           }
           
           if(is_array($pics))
           {
               $time = time();
               foreach($pics as $pic)
               {
                   $style = ' style="width:'.$size_x.'px;height:'.$size_y.'px;" ';
                   $out .= '<div '.$style.' class="thumb">';
                        $out .= '<a '.$style.' class="image async-img-none" rel="'.dcf_getTimThumbURL($pic->_thumbURL, $size_x, $size_y).'"></a>';
                        $out .= '<a href="'.$pic->_imageURL.'" '.$style.' class="triger" rel="lightbox[sid_gall_'.$time.']"></a>';
                   $out .= '</div>';
               }
           }
           $out .= '<div class="clear-both"></div>'; 
       
            if($show_link)
            {
                $out .= '<div class="link-wrapper">';
                    if($use_manually)
                    {
                        $out .= '<a class="more-link" href="'.$link.'" >&raquo;&nbsp;'.__('View more', 'dc_theme').'</a>';     
                    } else
                    if($page_link != CMS_NOT_SELECTED)
                    {
                        $out .= '<a class="more-link" href="'.get_permalink($page_link).'" >&raquo;&nbsp;'.__('View more', 'dc_theme').'</a>';
                    }
                $out .= '<div class="clear-both"></div></div>';
            }
            
       $out .= '</div>';
       echo $out;
       
       echo $after_widget;                         
   }

   function update( $new_instance, $old_instance ) 
   {
       $instance = $old_instance;
       $instance['title'] = strip_tags($new_instance['title']);
       $instance['images_list'] = $new_instance['images_list'];
       $instance['size_x'] = $new_instance['size_x']; 
       $instance['size_y'] = $new_instance['size_y']; 
       $instance['gallery'] = $new_instance['gallery'];
       $instance['number'] = $new_instance['number'];
       $instance['use_gallery'] = $new_instance['use_gallery'];
       $instance['use_recent'] = $new_instance['use_recent'];
       $instance['show_random'] = $new_instance['show_random'];
       $instance['show_link'] = $new_instance['show_link'];
       $instance['page_link'] = $new_instance['page_link']; 
       $instance['link'] = $new_instance['link'];
       $instance['use_manually'] = $new_instance['use_manually']; 
       
       return $instance;
   }

   function form( $instance ) 
   {
        $instance = wp_parse_args((array)$instance, 
            array('link' => '', 'use_manually' => false, 'title'=>'', 'page_link' => CMS_NOT_SELECTED, 'show_link' => false, 'use_recent' => false, 'show_random' => false, 'use_gallery' => false, 'gallery' => CMS_NOT_SELECTED, 'images_list'=>'', 'size_x' => 71, 'size_y' => 58, 'number' => 9));       
       
       $title = $instance['title'];
       $images_list = $instance['images_list'];
       $size_x = $instance['size_x'];
       $size_y = $instance['size_y'];       
       $gallery = $instance['gallery'];
       $number = $instance['number']; 
       $use_gallery = $instance['use_gallery'];
       $use_recent = $instance['use_recent']; 
       $show_random = $instance['show_random']; 
       $show_link = $instance['show_link'];
       $page_link = $instance['page_link'];
       $link = $instance['link'];
       $use_manually = $instance['use_manually'];
       
       
       $out = '';       
       $out .= '<p><label>Title:</label>';
       $out .= '<input class="widefat" name="'.$this->get_field_name('title').'" type="text" value="'.$title.'" /></p>';

       $out .= '<p><label>Comma separated NGG images ID:</label>';
       $out .= '<input class="widefat" name="'.$this->get_field_name('images_list').'" type="text" value="'.$images_list.'" />';
       $out .= '<br /><input name="'.$this->get_field_name('use_recent').'" type="checkbox" '.($use_recent ? ' checked="checked" ' : '').' /> Use recent';        
       $out .= '</p>';

       $out .= '<p><label>Select NGG gallery:</label>';
       $out .= $this->selectCtrlNGGList($gallery, $this->get_field_name('gallery'), 200);
       $out .= '<br /><input name="'.$this->get_field_name('use_gallery').'" type="checkbox" '.($use_gallery ? ' checked="checked" ' : '').' /> Use gallery'; 
       $out .= '<br /><input name="'.$this->get_field_name('show_random').'" type="checkbox" '.($show_random ? ' checked="checked" ' : '').' /> Display random images'; 
       $out .= '</p>';  
       
       
       $out .= '<p><label>Number images to display:</label>';
       $out .= '<input style="width:60px;text-align:center;" type="text" name="'.$this->get_field_name('number').'" value="'.$number.'" />';  
       $out .= '</p>';
       
       $out .= '<p><label>Thumb size:</label>';
       $out .= '<input style="width:60px;text-align:center;" type="text" name="'.$this->get_field_name('size_x').'" value="'.$size_x.'" /> x '; 
       $out .= '<input style="width:60px;text-align:center;" type="text" name="'.$this->get_field_name('size_y').'" value="'.$size_y.'" />';      
       $out .= '</p>';
       
       $out .= '<p><label>Optional link:</label>';
       $out .= $this->selectCtrlPagesList($page_link, $this->get_field_name('page_link'), 200);
       $out .= '<br /><input name="'.$this->get_field_name('show_link').'" type="checkbox" '.($show_link ? ' checked="checked" ' : '').' /> Show link';           
       $out .= '</p>';

       $out .= '<p><label>Optional manually link:</label>';
       $out .= '<input class="widefat" name="'.$this->get_field_name('link').'" type="text" value="'.$link.'" />'; 
       $out .= '<br /><input name="'.$this->get_field_name('use_manually').'" type="checkbox" '.($use_manually ? ' checked="checked" ' : '').' /> Use manually link instead page link';           
       $out .= '</p>';
       
       echo $out;
   }
   
    public function selectCtrlNGGList($itempage, $name, $width)
    {    
        $out = '';
        
        global $nggdb;
        if(isset($nggdb))
        {
            $gallerylist = $nggdb->find_all_galleries('gid', 'ASC');
            
            $out .= '<select style="width:'.$width.'px;" id="'.$name.'" name="'.$name.'">';
            $out .= '<option value="'.CMS_NOT_SELECTED.'" '.($itempage == CMS_NOT_SELECTED ? ' selected="selected" ' : '').' >Not selected</option>';
            foreach($gallerylist as $gal)
            {
                $out .= '<option ';
                $out .= ' value="'.$gal->gid.'" ';
                $out .= $itempage == $gal->gid ? ' selected="selected" ' : '';
                $out .= '>'.$gal->title.' (ID: '.$gal->gid.')';
                $out .= '</option>';
            }
            $out .= '</select>';
              
        } else
        {
            $out .= '<br /><span class="cms-span-10">No NextGen Gallery plugin.</span><br />'; 
        }
        
        return $out;            
    }
   
    public function selectCtrlPagesList($itempage, $name, $width)
    {
        $out = '';
        $out .= '<select style="width:'.$width.'px;" name="'.$name.'"><option value="'.CMS_NOT_SELECTED.'">Not selected</option>';
        
        global $wpdb;
        $pages = $wpdb->get_results("SELECT ID, post_title FROM $wpdb->posts WHERE post_status = 'publish' AND post_type = 'page' ORDER BY post_title ");
        
        $count = count($pages);
        for($i = 0; $i < $count; $i++)
        {
   
            $out .= '<option value="'.$pages[$i]->ID.'" ';
            
            if($itempage !== null)
            {
                if($pages[$i]->ID == $itempage) $out .= ' selected="selected" ';
            }
            
            $out .= '>';
            $out .= $pages[$i]->post_title.' (ID:'.$pages[$i]->ID.')';
            $out .= '</option>';
               
        } // for        
        $out .= '</select>';
        return $out;     
    }    
   
}

// Register widget
function dcwp_nggimagesInit() { register_widget('dcwp_nggimages'); }
add_action('widgets_init', 'dcwp_nggimagesInit');    
  
 
/*
 * Plugin Name: Text
 * Version: 1.0
 * Plugin URI: http://themeforest.net/user/DigitalCavalry
 * Description: Text
 * Author: Digital Cavalry
 * Author URI: http://themeforest.net/user/DigitalCavalry
 */
 
class dcwp_text extends WP_Widget 
{
   
   function dcwp_text() {
       $widget_ops = array('classname' => 'dcwp_text', 'description' => __('Arbitrary text or HTML ('.CMS_THEME_NAME.' theme)'));
       $control_ops = array('width' => 400, 'height' => 350);
       $this->WP_Widget('dcwp_text', __('dcwp_Text'), $widget_ops, $control_ops);
   }

   function widget( $args, $instance ) {
       extract($args);
       $title = apply_filters( 'widget_title', empty($instance['title']) ? '' : $instance['title'], $instance, $this->id_base);
       $text = apply_filters( 'widget_text', $instance['text'], $instance );
       echo $before_widget;
       if ( !empty( $title ) ) { echo $before_title . $title . $after_title; }
           echo $instance['filter'] ? wpautop(do_shortcode($text)) : do_shortcode($text);
       echo $after_widget;
   }

   function update( $new_instance, $old_instance ) {
       $instance = $old_instance;
       $instance['title'] = strip_tags($new_instance['title']);
       if ( current_user_can('unfiltered_html') )
           $instance['text'] =  $new_instance['text'];
       else
           $instance['text'] = stripslashes( wp_filter_post_kses( addslashes($new_instance['text']) ) ); // wp_filter_post_kses() expects slashed
       $instance['filter'] = isset($new_instance['filter']);
       return $instance;
   }

   function form( $instance ) {
       $instance = wp_parse_args( (array) $instance, array( 'title' => '', 'text' => '', 'filter' => false ) );
       $title = $instance['title'];
       $text = $instance['text'];
       $filter = $instance['filter'];
       
       $out = '';
       
       $out .= '<p>Title:';
       $out .= '<input class="widefat" id="'.$this->get_field_id('title').'" name="'.$this->get_field_name('title').'" type="text" value="'.$title.'" /></p>';

       $out .= '<textarea class="widefat" rows="16" cols="20" id="'.$this->get_field_id('text').'" name="'.$this->get_field_name('text').'">'.$text.'</textarea>';

       $out .= '<p><input id="'.$this->get_field_id('filter').'" name="'.$this->get_field_name('filter').'" type="checkbox" '.($filter ? ' checked="checked" ' : '').'/> Automatically add paragraphs</p>';
       echo $out;
   }
}

// Register widget
function dcwp_textInit() { register_widget('dcwp_text'); }
add_action('widgets_init', 'dcwp_textInit');
  
?>