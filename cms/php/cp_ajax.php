<?php
/**********************************************************************
* DC THEME WORDPRESS EDITION 
* (Ideal For Business And Personal Use: Portfolio or Blog)     
* 
* File name:   
*      cp_ajax.php
* Brief:       
*      Part of theme control panel.
* Author:      
*      DigitalCavalry
* Author URI:
*      http://themeforest.net/user/DigitalCavalry
* Contact:
*      digitalcavalry@gmail.com 
***********************************************************************/

/*********************************************************** 
* Definitions
************************************************************/
if(is_user_logged_in())
{
    add_action('wp_ajax_dc_tinymce', 'dc_ajax_tinymce');
    add_action('wp_ajax_dc_ngg_list', 'dc_ajax_ngg_list');
    add_action('wp_ajax_dc_page_list', 'dc_ajax_page_list'); 
    add_action('wp_ajax_dc_homeorder', 'dc_ajax_homeorder'); 
    
    add_action('wp_ajax_dc_questpost', 'dc_ajax_questpost'); 

} else
{
    add_action('wp_ajax_nopriv_dc_questpost', 'dc_ajax_questpost'); 
}

function dc_ajax_homeorder()
{
    $data = $_POST['data']; 
    $neworder = explode(',', $data);
    GetDCCPInterface()->getIHome()->saveSectionsOrder($neworder); 
    die();
}
            
function dc_ajax_tinymce() 
{                
    include_once(TEMPLATEPATH . '/cms/tinymce/window.php');
    die();      
}

function dc_ajax_questpost() 
{                
    $postID = (int)$_POST['postid'];
    
    $question_views = 0;
    if(!isset($_SESSION[('pv'.$postID)])) { $_SESSION[('pv'.$postID)] = false; } 
    if(isset($_SESSION[('pv'.$postID)]) and $_SESSION[('pv'.$postID)] == false)
    {   
        $question_views = get_post_meta($postID, 'questviews', true);
        if($question_views == '') { $question_views = 0; } else { $question_views++; }
        update_post_meta($postID, 'questviews', (int)$question_views);
        $_SESSION[('pv'.$postID)] = true; 
    } else
    {
        $question_views = get_post_meta($postID, 'questviews', true);     
    }  
    
    echo $question_views;
    die();                          
}

function dc_ajax_ngg_list() 
{
    global $nggdb;
    global $wpdb;
    $out = '';
    
    $out .= '<option value="'.CMS_NOT_SELECTED.'">Not selected</option>';
    $result = null;
    
    if(isset($nggdb))
    {
        $result = $wpdb->get_results("SELECT gid, title FROM $wpdb->nggallery");
        if($result !== null)
        {
            if(is_array($result))
            {
                foreach($result as $item)
                {
                    $out .= '<option value="'.$item->gid.'">'.$item->title.'</option>';
                }
            }
        }
    }
    echo $out;
    
    die();      
}  

function dc_ajax_page_list() 
{
    global $wpdb;
    $out = '';
    
    $out .= '<option value="'.CMS_NOT_SELECTED.'">Not selected</option>';
    $result = null;
    $result = $wpdb->get_results("SELECT ID, post_title FROM $wpdb->posts WHERE $wpdb->posts.post_type = 'page' AND $wpdb->posts.post_status = 'publish'");
    
    if($result !== null)
    {
        if(is_array($result))
        {
            foreach($result as $item)
            {
                $out .= '<option value="'.$item->ID.'">'.$item->post_title.'</option>';
            }
        }
    }
    echo $out;
    
    die();      
}              
        
?>