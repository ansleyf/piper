<?php
/**********************************************************************
* MODERN ELEGANCE WORDPRESS EDITION 
* (Ideal For Business And Personal Use: Portfolio or Blog)     
* 
* File name:   
*      cp_quicknewspanel.php
* Brief:       
*      Part of theme control panel.
* Author:      
*      DigitalCavalry
* Author URI:
*      http://themeforest.net/user/DigitalCavalry
* Contact:
*      digitalcavalry@gmail.com 
***********************************************************************/

/*********************************************************** 
* Definitions
************************************************************/

/*********************************************************** 
* Class name:
*    CPQuickNews
* Descripton:
*    Implementation of CPQuickNews 
***********************************************************/
class CPQuickNews 
{        
    const LINK_BLANK = 1;
    const LINK_SELF = 2;
    const LINK_MANUALLY = 1;
    const LINK_PAGE = 2;
    
    /*********************************************************** 
    * Constructor
    ************************************************************/
    public function __construct() 
    {
        $this->_hide = false;
        $this->_text = '';
        $this->_link = '';
        $this->_linkuse = true;
        $this->_linkpage = CMS_NOT_SELECTED;
        $this->_linktarget = CPQuickNews::LINK_SELF;
        $this->_linktype = CPQuickNews::LINK_MANUALLY;             
    } // constructor 

    /*********************************************************** 
    * Public members
    ************************************************************/          
    public $_hide;
    public $_text;
    public $_link;
    public $_linkuse;
    public $_linkpage;
    public $_linktype;
    public $_linktarget;
}

/*********************************************************** 
* Class name:
*    CPQuickNewsPanel
* Descripton:
*    Implementation of CPQuickNewsPanel 
***********************************************************/
class CPQuickNewsPanel extends DCC_CPBaseClass 
{        
    /*********************************************************** 
    * Constructor
    ************************************************************/
    public function __construct() 
    {
        $this->_DBIDOPT_QUICK_NEWS = CMS_THEME_NAME_UPPERCASE.'_QUICK_NEWS_OPT';
        
        // temp
        $this->_qn = get_option($this->_DBIDOPT_QUICK_NEWS);
        if (!is_array($this->_qn))
        {
            add_option($this->_DBIDOPT_QUICK_NEWS, $this->_qnDef);
            $this->_qn = get_option($this->_DBIDOPT_QUICK_NEWS);
        }           
        
    } // constructor 

    /*********************************************************** 
    * Public members
    ************************************************************/      
    
    /*********************************************************** 
    * Private members
    ************************************************************/      
     
     private $_DBIDOPT_QUICK_NEWS = null;  // data base id options 
     
     private $_qn;
     private $_qnDef = Array(
        'panelname' => 'Quick news:',
        'linkname' => 'Read more',
        'hide' => false,
        'time' => 6000,
        'news' => Array()
     );
     private $_saved = false;
   
    /*********************************************************** 
    * Public functions
    ************************************************************/               
 
     public function renderTab()
     {
        echo '<div class="cms-content-wrapper">';
        $this->process();
        $this->renderCMS();
        $this->renderQuickNewsCMS();
        echo '</div>';
     }                                  
 
     public function renderPanel()
     {
         $out = '';
         
         $count = count($this->_qn['news']);
         if($count == 0){ return; }
     
        if(!$this->_qn['hide'])
        {
             $add_style = '';
             if(!GetDCCPInterface()->getIGeneral()->isSearchHPanelShowed() or GetDCCPInterface()->getIGeneral()->getSearchHPanelPlace() == CPGeneral::SPANEL_PLACE_ABOVE_MENU)
             {
                $add_style = ' style="width:960px;" ';    
             }
                 
             $out .= '<div id="quick-news-panel" '.$add_style.'>';
             

                $out .= '<span class="panel-name">'.$this->_qn['panelname'].'</span>';
                
                for($i = 0; $i < $count; $i++)
                {
                    if($this->_qn['news'][$i]->_hide) { continue; }

                    $out .= '<div class="wrapper">
                        <span class="news">'.stripcslashes($this->_qn['news'][$i]->_text).'</span> ';
                    
                    if($this->_qn['news'][$i]->_linkuse)
                    {
                        $out .= '<a ';
                        if($this->_qn['news'][$i]->_linktype == CPQuickNews::LINK_PAGE)
                        {
                            $out .= ' href="'.get_permalink($this->_qn['news'][$i]->_linkpage).'" ';    
                        } else
                        {
                            $out .= ' href="'.$this->_qn['news'][$i]->_link.'" ';
                        }
                        
                        if($this->_qn['news'][$i]->_linktarget == CPQuickNews::LINK_SELF)
                        {
                            $out .= ' target="_self" ';    
                        } else
                        {
                            $out .= ' target="_blank" ';
                        }                  
                        
                        $out .= '>&raquo;&nbsp;'.$this->_qn['linkname'].'</a>';                        
                    }
                      
                    $out .= '</div>';
                    
                }            
                
                $out .= '<span class="time">'.$this->_qn['time'].'</span>';
                
            $out .= '</div>';          
        } else
        {
            $out .= '<div id="quick-news-panel-hidden"></div>';     
        }
         
        echo $out;
     }
 
    /*********************************************************** 
    * Private functions
    ************************************************************/      
    
    private function process()
    {
        if(isset($_POST['save_qnews_settings']))
        {       
            $this->_qn['time'] = $_POST['time'];
            $this->_qn['hide'] = isset($_POST['hide']) ? true : false; 
            $this->_qn['panelname'] = $_POST['panelname']; 
            $this->_qn['linkname'] = $_POST['linkname']; 
              
            update_option($this->_DBIDOPT_QUICK_NEWS, $this->_qn);            
            $this->_saved = true;   
        }      
        
        if(isset($_POST['qn_delete']))
        {
            $index = $_POST['index'];
            unset($this->_qn['news'][$index]);
            $this->_qn['news'] = array_values($this->_qn['news']);
            
            update_option($this->_DBIDOPT_QUICK_NEWS, $this->_qn);
            $this->_saved = true;                      
        }
        
        if(isset($_POST['qn_save']))
        {
            
            $index = $_POST['index'];
    
            $this->_qn['news'][$index]->_hide = isset($_POST['hide']) ? true : false;
            $this->_qn['news'][$index]->_text = $_POST['text'];
            $this->_qn['news'][$index]->_linktype = $_POST['linktype'];
            $this->_qn['news'][$index]->_linktarget = $_POST['linktarget'];
            $this->_qn['news'][$index]->_link = $_POST['link'];
            $this->_qn['news'][$index]->_linkpage = $_POST['linkpage'];
            $this->_qn['news'][$index]->_linkuse = $_POST['linkuse']; 
            
            update_option($this->_DBIDOPT_QUICK_NEWS, $this->_qn);
            $this->_saved = true;                   
        }        
        
        if(isset($_POST['qn_add']))
        {
            $item = new CPQuickNews();           
            array_push($this->_qn['news'], $item);
            
            update_option($this->_DBIDOPT_QUICK_NEWS, $this->_qn);
            $this->_saved = true;                      
        } 
        
        if(isset($_POST['qn_moveup']))
        {
            $index = $_POST['index'];
            if($index > 0)
            {         
                $temp = $this->_qn['news'][$index - 1];
                $this->_qn['news'][$index - 1] = $this->_qn['news'][$index];
                $this->_qn['news'][$index] = $temp;
                
                update_option($this->_DBIDOPT_QUICK_NEWS, $this->_qn);
                $this->_saved = true;
            }                      
        } 
        
        if(isset($_POST['qn_movedown']))
        {
            $index = $_POST['index'];
            $count = count($this->_qn['news']); 
            $last = $count - 1;
            if($index < $last)
            {         
                $temp = $this->_qn['news'][$index + 1];
                $this->_qn['news'][$index + 1] = $this->_qn['news'][$index];
                $this->_qn['news'][$index] = $temp;
                
                update_option($this->_DBIDOPT_QUICK_NEWS, $this->_qn);
                $this->_saved = true;
            }                      
        }                
    }

    private function renderCMS()
    {
        if($this->_saved)
        {                    
            echo '<span class="cms-saved-bar">Settings saved</span>';            
        } 
        
         // quick news core settings
         $out = '';         
         $out .= '<h6 class="cms-h6">Quick news settings</h6><hr class="cms-hr"/>';
         $out .= '<form action="#homepage_extra_content" method="post" >';                
                  
         $out .= '<input style="width:60px;text-align:center;" type="text" name="time" value="'.$this->_qn['time'].'" /> Switch time in miliseconds<br /><br />';
         $out .= '<input type="checkbox" name="hide" '.$this->attrChecked($this->_qn['hide']).' /> Hide quick news panel<br /><br />';
         
         $out .= '<input type="text" style="width:200px;" name="panelname" value="'.$this->_qn['panelname'].'" /> Panel name<br />'; 
         $out .= '<input type="text" style="width:200px;" name="linkname" value="'.$this->_qn['linkname'].'" /> Read more link text<br />'; 
         
         $out .= '<div style="height:20px;"></div>';
         $out .= '<input class="cms-submit" type="submit" value="Save" name="save_qnews_settings" /> ';         
         $out .= '<input class="cms-submit" type="submit" value="Add new quick news" name="qn_add" /> '; 
         $out .= '</form>';
         
         echo $out;           
    }
    
    private function renderQuickNewsCMS()
    {
        $count = count($this->_qn['news']);
        if($count == 0) { return; }
        
        
        for($i = 0; $i < $count; $i++)
        {
             $out = '';         
             $out .= '<div style="margin-top:30px;"></div>';
             $out .= '<a name="quick-news-'.$i.'"></a>';
             $out .= '<h6 class="cms-h6">News No '.($i+1).'</h6><hr class="cms-hr"/>';
             $out .= '<form action="#quick-news-'.$i.'" method="post" >';                
             
             $out .= '<input type="hidden" name="index" value="'.$i.'" />';
             
             $out .= '<span class="cms-span-10">Text message:</span><br />';
             $out .= '<textarea style="width:800px;height:60px;font-size:11px;padding:8px;" name="text" >'.stripcslashes($this->_qn['news'][$i]->_text).'</textarea><br /><br />';
             $out .= '<input type="checkbox" name="hide" '.$this->attrChecked($this->_qn['news'][$i]->_hide).' /> Hide news <br /><br />';
             
             $out .= '<div style="float:left;width:200px;border-right:0px solid red;">';
             $out .= '<span class="cms-span-10">Message link:</span><br />';
             $out .= '<input type="radio" name="linktype" '.$this->attrChecked($this->_qn['news'][$i]->_linktype == CPQuickNews::LINK_MANUALLY).' value="'.CPQuickNews::LINK_MANUALLY.'" /> Use manually link<br />';
             $out .= '<input type="radio" name="linktype" '.$this->attrChecked($this->_qn['news'][$i]->_linktype == CPQuickNews::LINK_PAGE).' value="'.CPQuickNews::LINK_PAGE.'" /> Use selected page<br />';             
             $out .= '<br />';
             $out .= '<input type="radio" name="linktarget" '.$this->attrChecked($this->_qn['news'][$i]->_linktarget == CPQuickNews::LINK_SELF).' value="'.CPQuickNews::LINK_SELF.'" /> Self<br />';
             $out .= '<input type="radio" name="linktarget" '.$this->attrChecked($this->_qn['news'][$i]->_linktarget == CPQuickNews::LINK_BLANK).' value="'.CPQuickNews::LINK_BLANK.'" /> Blank<br />';  
             $out .= '<br />';
             $out .= '</div>';

             $out .= '<div style="float:left;width:320px;border-right:0px solid red;">';
             $out .= '<span class="cms-span-10">Manually link</span><br />';
             $out .= '<input type="text" style="width:300px;" name="link" value="'.$this->_qn['news'][$i]->_link.'" /><br />';
             $out .= '<span class="cms-span-10">Page link</span><br />';
             $out .= $this->selectCtrlPagesList($this->_qn['news'][$i]->_linkpage, 'linkpage', 300);
             $out .= '<br /><br /><input type="checkbox" name="linkuse" '.$this->attrChecked($this->_qn['news'][$i]->_linkuse).' /> Use link'; 
             $out .= '</div>';
             
             $out .= '<div style="clear:both;"></div>';
             
             $out .= '<div style="height:20px;"></div>';
             $out .= '<input class="cms-submit" type="submit" value="Save" name="qn_save" /> ';
             $out .= '<input class="cms-submit" type="submit" value="Up" name="qn_moveup" /> ';
             $out .= '<input class="cms-submit" type="submit" value="Down" name="qn_movedown" /> ';             
             $out .= '<input onclick="return confirm('."'Delete this slide?'".')" class="cms-submit-delete" type="submit" value="Delete" name="qn_delete" />';          
             $out .= '</form>';
             
             echo $out; 
        } // for   
    }    
         
} // class CPQuickNewsPanel
        
        
?>