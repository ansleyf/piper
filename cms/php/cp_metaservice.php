<?php
/**********************************************************************
* MODERN ELEGANCE WORDPRESS THEME 
* (Ideal For Business And Personal Use: Portfolio or Blog)     
* 
* File name:   
*      cp_metaservice.php
* Brief:       
*      Part of theme control panel.
* Author:      
*      DigitalCavalry
* Author URI:
*      http://themeforest.net/user/DigitalCavalry
* Contact:
*      digitalcavalry@gmail.com 
***********************************************************************/

/*********************************************************** 
* Definitions
************************************************************/

/*********************************************************** 
* Class name:
*    CPMetaServiceOpt
* Descripton:
*    Implementation of CPMetaServiceOpt
***********************************************************/
class CPMetaServiceOpt extends DCC_MetaMultiple 
{        
    const WIDTH = 24;
    const HEIGHT = 24;    
    
    /*********************************************************** 
    * Constructor
    ************************************************************/
    public function __construct() 
    {
        $this->_names = array(
            'service_opt', 'service_opt_cbox', 
            'image_url', 'image_url_hover', 'show_image_hover_cbox', 
            'subtitle', 'show_subtitle_cbox',
            'link', 'linktype', 'linkpage', 'link_show_cbox');   
                       
        $this->_std = array(
            'service_opt_cbox' => false,
            'image_url' => '',
            'image_url_hover' => '',
            'show_image_hover_cbox' => false,
            'subtitle' => '',
            'show_subtitle_cbox' => false,
            'link' => '',
            'linkpage' => CMS_NOT_SELECTED,
            'linktype' => DCC_MetaMultiple::LINK_PAGE,
            'link_show_cbox' => false,        
            );
                             
        $this->_title = 'SERVICE POST';
        $this->_type = 'service';
        $this->_desc = '';
    } // constructor 

    /*********************************************************** 
    * Public functions
    ************************************************************/    
    public function display()
    {       
        $value = $this->initDisplay();            
                
        global $post;
        echo '<span class="cms-meta-normal">POST INFORMATION: ID='.$post->ID.'</span>';         

        // title
        $out = ''; 
        $out .= '<div class="cms-custom-field-panel ">';
        $out .= '<div class="cms-meta-bold">'.$this->_title.'</div><br />';                       
        $out .= '<input type="hidden" name="service_opt_cbox" checked="checked" />';        
        
        // image
        $out .= '<span class="cms-meta-normal">Optional image URL</span><br />';   
            $path = $value['image_url'];
            if($path != '')
            {
                $out .= '<img style="display:block;margin:5px 0px 5px 0px;" src="'.$path.'"/>';
            }
                                   
        $out .= '<input style="width:480px;" type="text" id="'.'image_url'.'_path" name="'.'image_url'.'" value="'.$value['image_url'].'" />'; 
        $out .= '<input style="width:140px;" class="cms-upload upload_image_button" type="button" value="Upload Image" name="'.'image_url'.'_path" /><br /><br />';                                
        
        // hover image
        $out .= '<span class="cms-meta-normal">Optional hover image URL</span><br />';   
            $path = $value['image_url_hover'];
            if($path != '')
            {
                $out .= '<img style="display:block;margin:5px 0px 5px 0px;" src="'.$path.'"/>';
            }
                                   
        $out .= '<input style="width:480px;" type="text" id="'.'image_url_hover'.'_path" name="'.'image_url_hover'.'" value="'.$value['image_url_hover'].'" />'; 
        $out .= '<input style="width:140px;" class="cms-upload upload_image_button" type="button" value="Upload Image" name="'.'image_url_hover'.'_path" /><br /><br />';                                
                
        $out .= '<span class="cms-meta-normal">Service subtitle:</span><br />';  
        $out .= '<input style="width:480px;" type="text" name="subtitle" value="'.$value['subtitle'].'" /><br /><br />'; 
        
        $out .= '<span class="cms-meta-normal">Page link:</span><br />';
        $out .= $this->selectCtrlPagesList($value['linkpage'], 'linkpage', 320);
        $out .= '<br /><br />';

        $out .= '<span class="cms-meta-normal">Manually link:</span><br />';  
        $out .= '<input style="width:480px;" type="text" name="link" value="'.$value['link'].'" /><br /><br />';         
        
        $out .= '<input type="radio" name="linktype" '.($value['linktype'] == DCC_MetaMultiple::LINK_PAGE ? ' checked="checked"' : '').' value="'.DCC_MetaMultiple::LINK_PAGE.'" /> Use page link<br />';
        $out .= '<input type="radio" name="linktype" '.($value['linktype'] == DCC_MetaMultiple::LINK_MANUALLY ? ' checked="checked"' : '').' value="'.DCC_MetaMultiple::LINK_MANUALLY.'" /> Use manually link<br />';
        $out .= '<br />';        
        
        $out .= '<span class="cms-meta-normal">Other options:</span><br />';
        $out .= '<input type="checkbox" name="show_image_hover_cbox" '.$this->attrChecked($value['show_image_hover_cbox']).' /> Show hover image<br />'; 
        $out .= '<input type="checkbox" name="show_subtitle_cbox" '.$this->attrChecked($value['show_subtitle_cbox']).' /> Show subtitle<br />'; 
        $out .= '<input type="checkbox" name="link_show_cbox" '.$this->attrChecked($value['link_show_cbox']).' /> Show link<br /><br />'; 
        
        $out .= $this->getUpdateBtnHtmlCode();
        
        $out .= '</div>';
        echo $out;
    } 
} // class CPMetaServiceOpt   
        
?>