<?php
/**********************************************************************
* MODERN ELEGANCE WORDPRESS THEME 
* (Ideal For Business And Personal Use: Portfolio or Blog)     
* 
* File name:   
*      cp_menu.php
* Brief:       
*      Part of theme control panel.
* Author:      
*      DigitalCavalry
* Author URI:
*      http://themeforest.net/user/DigitalCavalry
* Contact:
*      digitalcavalry@gmail.com 
***********************************************************************/

/*********************************************************** 
* Definitions
************************************************************/

/*********************************************************** 
* Class name:
*    CPMenuItem
* Descripton:
*    Implementation of CPMenuItem 
***********************************************************/
class CPMenuItem
{
    const ITEM_USE_PAGE = 1;
    const ITEM_USE_LINK = 2;
    const ITEM_TARGET_BLANK = 1;
    const ITEM_TARGET_SELF = 2;
    
    /*********************************************************** 
    * Constructor
    ************************************************************/    
    public function __construct() 
    {   
        $this->_hide = false;
        $this->_link = '';
        $this->_name = '';
        $this->_page = CMS_NOT_SELECTED;
        $this->_submenu = CMS_NOT_SELECTED;
        $this->_type = CPMenuItem::ITEM_USE_PAGE;
        $this->_target = CPMenuItem::ITEM_TARGET_SELF;
    } 
    
    /*********************************************************** 
    * Public members  
    ************************************************************/  
    public $_name;
    public $_link;
    public $_page;
    public $_type;
    public $_submenu;
    public $_hide;
    public $_target;
       
} // CPMenuItem

/*********************************************************** 
* Class name:
*    CPSubMenu
* Descripton:
*    Implementation of CPSubMenu 
***********************************************************/
class CPSubMenu
{
    /*********************************************************** 
    * Constructor
    ************************************************************/    
    public function __construct() 
    {   
        $this->_name = '';
        $this->_items = Array();
    } 
    
    /*********************************************************** 
    * Public members  
    ************************************************************/
    
    public $_name;
    public $_items;    
} // CPSubMenu

/*********************************************************** 
* Class name:
*    CPMenu
* Descripton:
*    Implementation of CPGeneral 
***********************************************************/
class CPMenu extends DCC_CPBaseClass
{
    const MAX_SUBMENU_LEVEL = 3;
    
    /*********************************************************** 
    * Constructor
    ************************************************************/
    public function __construct() 
    {
        $this->registerWordPressCustomMenus();
        
        $this->_DBIDOPT_TOP_ITEMS = CMS_THEME_NAME_UPPERCASE.'_MENU_TOP_ITEMS_OPT';
        $this->_DBIDOPT_SUBMENUS = CMS_THEME_NAME_UPPERCASE.'_SUBMENUS_OPT';
        $this->_DBIDOPT_SETTINGS = CMS_THEME_NAME_UPPERCASE.'_MENU_SETTINGS_OPT';        
        
        // menu settings
        $this->_set = get_option($this->_DBIDOPT_SETTINGS);
        if (!is_array($this->_set))
        {
            add_option($this->_DBIDOPT_SETTINGS, $this->_setDef);
            $this->_set = get_option($this->_DBIDOPT_SETTINGS);
        } 

        // menu top level elements
        $this->_top = get_option($this->_DBIDOPT_TOP_ITEMS);
        if (!is_array($this->_top))
        {
            add_option($this->_DBIDOPT_TOP_ITEMS, Array());
            $this->_top = get_option($this->_DBIDOPT_TOP_ITEMS);
        }  
        
        // submenus
        $this->_sub = get_option($this->_DBIDOPT_SUBMENUS);
        if (!is_array($this->_sub))
        {
            add_option($this->_DBIDOPT_SUBMENUS, Array());
            $this->_sub = get_option($this->_DBIDOPT_SUBMENUS);
        }

                                     
    } // constructor 

    /*********************************************************** 
    * Public members
    ************************************************************/      
    
    /*********************************************************** 
    * Private members
    ************************************************************/   
    private $_DBIDOPT_TOP_ITEMS = null;
    private $_DBIDOPT_SUBMENUS = null;
    private $_DBIDOPT_SETTINGS = null;    
       
    private $_set = Array(); // menu settings
    private $_setDef = Array( // default menu settings 
        'menu_bg_color' => '#FFFFFF',  
        'menu_top_item_text_color' => '#5E5E5E',
        'menu_top_item_text_hover_color' => '#000000',
        'menu_sub_item_text_color' => '#757575', 
        'menu_sub_item_text_hover_color' => '#000000',
        'menu_sub_item_bg_color' => '#FFFFFF',
        'menu_boxed_mode' => false,
        'menu_no_bmargin' => false
        );  
    
    private $_top = Array(); // menu top items list
    private $_sub = Array(); // sumenus list 
    private $_saved = false; // have true if some settings was processed and saved
   
    /*********************************************************** 
    * Public functions
    ************************************************************/               
    public function renderTab()
    {
        $this->process();
       
        echo '<div class="cms-content-wrapper">';
        $this->renderMenuSettingsCMS();
        $this->renderTopMenuCMS();
        $this->renderSubmenuCMS();      
        echo '</div>';
    }    
  
    public function renderMenu()
    {     
        global $post;               
        
        echo '<div id="navigation-wrapper"><ul class="sf-menu">';        
        
        foreach($this->_top as $item)
        {
            if($item->_hide) { continue; }
            
            $out = '<li';
            $out .= '><a ';

            if($item->_type != CMS_NOT_SELECTED)
            {
                
                if((trim($item->_link) != '' and $item->_type == CPMenuItem::ITEM_USE_LINK) or
                   ($item->_page != CMS_NOT_SELECTED and $item->_type == CPMenuItem::ITEM_USE_PAGE))
                {
                    if($item->_type == CPMenuItem::ITEM_USE_LINK)
                    {
                        $out .= 'href="'.$item->_link.'" ';
                    } else
                    {
                        $out .= 'href="'.get_permalink($item->_page).'" ';
                    }
                    
                    if($item->_target == CPMenuItem::ITEM_TARGET_BLANK)
                    {
                        $out .= 'target="_blank" ';
                    } else
                    {
                        $out .= 'target="_self" '; 
                    }                    
                }
            }
            
            $out .= '>';
            $out .= $item->_name;
            $out .= '</a>';
            echo $out;

            // if have submenu we must render it
            if($item->_submenu != CMS_NOT_SELECTED)
            {
                 $this->renderSubmenu($item->_submenu, 1);
            } 
            
            echo '</li>';

        } // for
        echo '</ul></div>';                                                                  
    }       

    public function customCSS()
    {   
        $out = '';
        $out .= '<style type="text/css">';        
        
            if($this->_set['menu_boxed_mode'])
            {
                $out .= '#content-menu-wrapper {';
                    $out .= 'width:1000px;';
                $out .= '} ';
            }
        
            if($this->_set['menu_no_bmargin'])
            {
                $out .= '#content-menu-wrapper  {';
                    $out .= 'margin-bottom:0px;';
                $out .= '} ';
            }        
        
        
            $out .= '#navigation-wrapper, #content-menu-wrapper { ';
            $out .= 'background-color:'.$this->_set['menu_bg_color'].';';
            $out .= '} ';

            // menu top items text color            
            $out .= '.sf-menu li a {';
            $out .= 'color:'.$this->_set['menu_top_item_text_color'].';';
            $out .= '} ';                                                     

            // menu top items text hover color            
            $out .= '.sf-menu li:hover > a {';
            $out .= 'color:'.$this->_set['menu_top_item_text_hover_color'].';';
            $out .= '} ';      
        
            // submenu items text color
            $out .= '.sf-menu li ul li a  {';
            $out .= 'color:'.$this->_set['menu_sub_item_text_color'].';';
            $out .= '} ';              
 
            // submenu items background color
            $out .= '.sf-menu li ul li {';
            $out .= 'background-color:'.$this->_set['menu_sub_item_bg_color'].';';
            $out .= '} ';                            
            
            // menu sub items text hover color            
            $out .= '.sf-menu li ul li:hover > a {';
            $out .= 'color:'.$this->_set['menu_sub_item_text_hover_color'].';';
            $out .= '} ';               
        
        $out .= '</style>';                                                              
        echo $out;        
    }
 
    /*********************************************************** 
    * Private functions
    ************************************************************/
    
    private function registerWordPressCustomMenus()
    {
        register_nav_menus(array('dcm_headertop' => CMS_THEME_NAME.' additional header top menu'));         
    }    
    
    private function renderSubmenu($index, $level)
    {
        // check submenu level
        if($level > CPMenu::MAX_SUBMENU_LEVEL)
        {
            return;
        }
        
        $subcount = count($this->_sub);
        if($index < $subcount)
        {        
            echo '<ul>';
            
            foreach($this->_sub[$index]->_items as $item)
            {
                if($item->_hide) { continue; }
                
                $out = '<li><a ';
                
                if($item->_type == CPMenuItem::ITEM_USE_LINK)
                {
                    $out .= 'href="'.$item->_link.'" ';
                } else
                {
                    $out .= 'href="'.get_permalink($item->_page).'" ';
                }
                
                if($item->_target == CPMenuItem::ITEM_TARGET_BLANK)
                {
                    $out .= 'target="_blank" ';
                } else
                {
                    $out .= 'target="_self" '; 
                }
                
                $out .= '>';
                $out .= $item->_name;
                $out .= '</a>';
                echo $out;

                // if have submenu we must render it
                if($item->_submenu != CMS_NOT_SELECTED)
                {
                     $this->renderSubmenu($item->_submenu, $level + 1);
                } 
                
                echo '</li>';
            } // for                        
            echo '</ul>';
        }    
    }
            
    private function process()
    {        
        $this->processMenuSettings(); 
        $this->processMenuTopItems();
        $this->processSubmenus();     
    }    
   
    private function processMenuSettings()
    {
        if(isset($_POST['menu_save_settings']))
        {
            $this->_set['menu_bg_color'] = $_POST['menu_bg_color'];             
            $this->_set['menu_top_item_text_color'] = $_POST['menu_top_item_text_color'];
            $this->_set['menu_top_item_text_hover_color'] = $_POST['menu_top_item_text_hover_color']; 
            $this->_set['menu_sub_item_text_hover_color'] = $_POST['menu_sub_item_text_hover_color']; 
            $this->_set['menu_sub_item_text_color'] = $_POST['menu_sub_item_text_color'];
            $this->_set['menu_sub_item_bg_color'] = $_POST['menu_sub_item_bg_color'];
            $this->_set['menu_boxed_mode'] = isset($_POST['menu_boxed_mode']) ? true : false; 
            $this->_set['menu_no_bmargin'] = isset($_POST['menu_no_bmargin']) ? true : false;
                                                                                   
            update_option($this->_DBIDOPT_SETTINGS, $this->_set);
            $this->_saved = true;                                
        }         
    }
   
    private function processMenuTopItems()
    {        
        if(isset($_POST['tmi_delete']))
        {
            $index = $_POST['index'];
            unset($this->_top[$index]);
            $this->_top = array_values($this->_top);
            update_option($this->_DBIDOPT_TOP_ITEMS, $this->_top);
            $this->_saved = true;                      
        }
        
        if(isset($_POST['tmi_save']))
        {
            
            $index = $_POST['index'];
    
            $this->_top[$index]->_hide = isset($_POST['hide']) ? true : false;
            $this->_top[$index]->_name = $_POST['name'];
            $this->_top[$index]->_page = $_POST['page'];
            $this->_top[$index]->_type = $_POST['type'];
            $this->_top[$index]->_link = $_POST['link'];
            $this->_top[$index]->_target = $_POST['target']; 
            $this->_top[$index]->_submenu = $_POST['submenu'];
            $this->_top[$index]->_page = $_POST['page'];
            
            update_option($this->_DBIDOPT_TOP_ITEMS, $this->_top);
            $this->_saved = true;                   
        }        
        
        if(isset($_POST['tmi_add']))
        {
            $item = new CPMenuItem();
            $item->_name = $_POST['name'];            
            array_push($this->_top, $item);
            update_option($this->_DBIDOPT_TOP_ITEMS, $this->_top);
            $this->_saved = true;                      
        } 
        
        if(isset($_POST['tmi_moveup']))
        {
            $index = $_POST['index'];
            if($index > 0)
            {         
                $temp = $this->_top[$index - 1];
                $this->_top[$index - 1] = $this->_top[$index];
                $this->_top[$index] = $temp;
                
                update_option($this->_DBIDOPT_TOP_ITEMS, $this->_top);
                $this->_saved = true;
            }                      
        } 
        
        if(isset($_POST['tmi_movedown']))
        {
            $index = $_POST['index'];
            $count = count($this->_top); 
            $last = $count - 1;
            if($index < $last)
            {         
                $temp = $this->_top[$index + 1];
                $this->_top[$index + 1] = $this->_top[$index];
                $this->_top[$index] = $temp;
                
                update_option($this->_DBIDOPT_TOP_ITEMS, $this->_top);
                $this->_saved = true;
            }                      
        }                       
 
        if(isset($_POST['tmi_addbelow']))
        {
            $index = $_POST['index'];
            $count = count($this->_top);
            $item = new CPMenuItem();                              
            
            if($count == 0 or $index == $count-1)
            {
                array_push($this->_top, $item); 
            } else
            {
                // get the start of the array  
                $start = array_slice($this->_top, 0, $index+1); 
                // get the end of the array
                $end = array_slice($this->_top, $index+1);
                // add the new element to the array
                $start[] = $item;
                // glue them back together and return
                $this->_top = array_merge($start, $end);   
            } 
            update_option($this->_DBIDOPT_TOP_ITEMS, $this->_top);
            $this->_saved = true;                      
        }  
        
    }
    
    private function processSubmenus()
    {
        if(isset($_POST['sub_add']))
        {
            $sub = new CPSubMenu();
            $sub->_name = $_POST['name'];            
            array_push($this->_sub, $sub);
            update_option($this->_DBIDOPT_SUBMENUS, $this->_sub);
            $this->_saved = true;                      
        }  
       
        if(isset($_POST['sub_delete']))
        {
            $index = $_POST['subindex'];
            unset($this->_sub[$index]);
            $this->_sub = array_values($this->_sub);
            update_option($this->_DBIDOPT_SUBMENUS, $this->_sub);
            $this->_saved = true;                      
        }
    
        if(isset($_POST['sub_save']))
        {
            $index = $_POST['subindex'];
            $this->_sub[$index]->_name = $_POST['name'];
            update_option($this->_DBIDOPT_SUBMENUS, $this->_sub);
            $this->_saved = true;                      
        }    
        
        if(isset($_POST['subi_add']))
        {
            $item = new CPMenuItem();
            $item->_name = $_POST['name']; 
            
            $index = $_POST['subindex'];
                       
            array_push($this->_sub[$index]->_items, $item);
            update_option($this->_DBIDOPT_SUBMENUS, $this->_sub);
            $this->_saved = true;                      
        }         
      
        if(isset($_POST['subi_delete']))
        {
            $index = $_POST['index'];
            $subindex = $_POST['subindex']; 
            unset($this->_sub[$subindex]->_items[$index]);
            $this->_sub[$subindex]->_items = array_values($this->_sub[$subindex]->_items);
            update_option($this->_DBIDOPT_SUBMENUS, $this->_sub);
            $this->_saved = true;                      
        }
    
        if(isset($_POST['subi_addbelow']))
        {
            $index = $_POST['index'];
            $subindex = $_POST['subindex'];
            $count = count($this->_sub[$subindex]->_items);
            $item = new CPMenuItem();                              
            
            if($count == 0 or $index == $count-1)
            {
                array_push($this->_sub[$subindex]->_items, $item); 
            } else
            {
                // get the start of the array  
                $start = array_slice($this->_sub[$subindex]->_items, 0, $index+1); 
                // get the end of the array
                $end = array_slice($this->_sub[$subindex]->_items, $index+1);
                // add the new element to the array
                $start[] = $item;
                // glue them back together and return
                $this->_sub[$subindex]->_items = array_merge($start, $end);   
            } 
            update_option($this->_DBIDOPT_SUBMENUS, $this->_sub);
            $this->_saved = true;                      
        }                
        
        if(isset($_POST['subi_moveup']))
        {
            $index = $_POST['index'];
            $subindex = $_POST['subindex'];
            if($index > 0)
            {         
                $temp = $this->_sub[$subindex]->_items[$index - 1];
                $this->_sub[$subindex]->_items[$index - 1] = $this->_sub[$subindex]->_items[$index];
                $this->_sub[$subindex]->_items[$index] = $temp;
                
                update_option($this->_DBIDOPT_SUBMENUS, $this->_sub);
                $this->_saved = true;
            }                      
        } 
        
        if(isset($_POST['subi_movedown']))
        {
            $index = $_POST['index'];
            $subindex = $_POST['subindex'];
            $count = count($this->_sub[$subindex]->_items); 
            $last = $count - 1;
            if($index < $last)
            {         
                $temp = $this->_sub[$subindex]->_items[$index + 1];
                $this->_sub[$subindex]->_items[$index + 1] = $this->_sub[$subindex]->_items[$index];
                $this->_sub[$subindex]->_items[$index] = $temp;
                
                update_option($this->_DBIDOPT_SUBMENUS, $this->_sub);
                $this->_saved = true;
            }                      
        }          
       
        if(isset($_POST['subi_save']))
        {          
            $index = $_POST['index'];
            $subindex = $_POST['subindex'];
    
            $this->_sub[$subindex]->_items[$index]->_hide = isset($_POST['hide']) ? true : false;
            $this->_sub[$subindex]->_items[$index]->_name = $_POST['name'];
            $this->_sub[$subindex]->_items[$index]->_page = $_POST['page'];
            $this->_sub[$subindex]->_items[$index]->_type = $_POST['type'];
            $this->_sub[$subindex]->_items[$index]->_link = $_POST['link'];
            $this->_sub[$subindex]->_items[$index]->_target = $_POST['target']; 
            $this->_sub[$subindex]->_items[$index]->_submenu = $_POST['submenu'];
            $this->_sub[$subindex]->_items[$index]->_page = $_POST['page'];
            
            update_option($this->_DBIDOPT_SUBMENUS, $this->_sub);
            $this->_saved = true;                     
        }             
                       
    }
 
    private function renderMenuSettingsCMS()
    {
        if($this->_saved)
        {                    
            echo '<span class="cms-saved-bar">Settings saved</span>';            
        }   
        
         // menu core settings
         $out = '';
         $out .= '<a name="menu-set"></a>';
         $out .= '<div style="margin-top:0px;">';         
         $out .= '<h6 class="cms-h6">Menu settings</h6><hr class="cms-hr"/>';
         $out .= '<form action="#menu-set" method="post" >';                                                                                                                                        
         $out .= '<br />';        
         $out .= '<input style="width:70px;text-align:center;" type="text" class="colorpicker {hash:true}" value="'.$this->_set['menu_bg_color'].'" name="menu_bg_color" /> Menu background color<br />'; 
         $out .= '<br />';
         $out .= '<input style="width:70px;text-align:center;" type="text" class="colorpicker {hash:true}" value="'.$this->_set['menu_top_item_text_color'].'" name="menu_top_item_text_color" /> Menu top items text color <br />'; 
         $out .= '<br />';
         $out .= '<input style="width:70px;text-align:center;" type="text" class="colorpicker {hash:true}" value="'.$this->_set['menu_top_item_text_hover_color'].'" name="menu_top_item_text_hover_color" /> Menu top item text hover color <br />'; 
         $out .= '<br />';        
         $out .= '<input style="width:70px;text-align:center;" type="text" class="colorpicker {hash:true}" value="'.$this->_set['menu_sub_item_text_color'].'" name="menu_sub_item_text_color" /> Submenu items text color <br />';
         $out .= '<br />';        
         $out .= '<input style="width:70px;text-align:center;" type="text" class="colorpicker {hash:true}" value="'.$this->_set['menu_sub_item_bg_color'].'" name="menu_sub_item_bg_color" /> Submenu background color <br />';
         $out .= '<br />';
         $out .= '<input style="width:70px;text-align:center;" type="text" class="colorpicker {hash:true}" value="'.$this->_set['menu_sub_item_text_hover_color'].'" name="menu_sub_item_text_hover_color" /> Submenu item text hover color <br /><br />'; 
               
         $out .= '<input type="checkbox" name="menu_boxed_mode" '.$this->attrChecked($this->_set['menu_boxed_mode']).' /> Display menu in boxed mode<br />';      
         $out .= '<input type="checkbox" name="menu_no_bmargin" '.$this->attrChecked($this->_set['menu_no_bmargin']).' /> Remove menu bottom margin<br />'; 
                  
         $out .= '<div style="margin-top:20px;">';         
         $out .= '<input class="cms-submit" type="submit" value="Save" name="menu_save_settings" /><br />';         
         $out .= '</div>';                           
         $out .= '</form></div>';
         echo $out;                
    }
    
    private function renderTopMenuCMS()
    {   
        $num = count($this->_top);         
        if($num > 0)
        {
            // menu top elements list
            echo '<div style="height:50px;"></div>';  
            echo '<h6 class="cms-h6">Top level elements</h6><hr class="cms-hr"/>';

            echo '<div style="margin-bottom:30px;">';
            
            echo '<table>';        
            echo '<thead>
                    <tr>
                        <th style="padding:0px 3px 0px 3px;">Hide</th>
                        <th style="padding:0px 3px 0px 3px;">Name</th>
                        <th style="padding:0px 3px 0px 3px;">Page</th>
                        <th style="padding:0px 3px 0px 3px;">Use</th>
                        <th style="padding:0px 3px 0px 3px;">Link</th>
                        <th style="padding:0px 3px 0px 3px;">Use</th>
                        <th style="padding:0px 3px 0px 3px;">Submenu</th>
                        <th style="padding:0px 3px 0px 3px;">Target</th>
                        <th style="padding:0px 3px 0px 3px;">Save</th>
                        <th style="padding:0px 3px 0px 3px;">Delete</th>
                        <th style="padding:0px 3px 0px 3px;">Add</th>
                        <th style="padding:0px 3px 0px 3px;">Up</th>
                        <th style="padding:0px 3px 0px 3px;">Down</th>                        
                    </tr>
                  </thead>';
            $num = count($this->_top);
            for($i = 0; $i < $num; $i++)
            {            
                echo '<tr>';
                // hide menu item
                echo '              
                    <td style="text-align:center;"><form action="#" method="POST">
                    <input type="checkbox" '.($this->_top[$i]->_hide ? ' checked="checked" ' : '').' name="hide" title="Select to hide element" /></td>'; 
                // name     
                echo '<td><input type="hidden" name="index" value="'.$i.'" />
                    <input style="width:100px;" type="text" name="name" value="'.$this->_top[$i]->_name.'" title="Menu item name" /></td>';
                // page / use page                   
                echo '<td>'.$this->selectCtrlPagesList($this->_top[$i]->_page, 'page', 120).'</td>';
               
                echo '<td style="text-align:center;"><input type="radio" '.($this->_top[$i]->_type == CPMenuItem::ITEM_USE_PAGE ? ' checked="checked" ' : '').'  
                    value="'.CPMenuItem::ITEM_USE_PAGE.'" name="type" title="Select to use page address as link" /></td>';
                // link / use link   
                echo '<td><input style="width:100px;" type="text" name="link" value="'.$this->_top[$i]->_link.'" /></td>
                    <td style="text-align:center;"><input type="radio" name="type" '.($this->_top[$i]->_type == CPMenuItem::ITEM_USE_LINK ? ' checked="checked" ' : '').'
                    value="'.CPMenuItem::ITEM_USE_LINK.'" title="Select to use own address as link" /></td>';
                // submenu     
                $this->printSubmenuList(null, $this->_top[$i]->_submenu, 100);
                
                echo '<td style="width:110px;text-align:center;">
                    <input type="radio" name="target" '.$this->attrChecked($this->_top[$i]->_target == CPMenuItem::ITEM_TARGET_BLANK).' value="'.CPMenuItem::ITEM_TARGET_BLANK.'" />Blank '; 
                echo '<input type="radio" name="target" '.$this->attrChecked($this->_top[$i]->_target == CPMenuItem::ITEM_TARGET_SELF).' value="'.CPMenuItem::ITEM_TARGET_SELF.'" />Self</td>';
                
                // save / delete 
                echo '<td><input  class="cms-submit" type="submit" value="Save" name="tmi_save" /></td> 
                    <td><input  onclick="return confirm('."'Delete item?'".')" class="cms-submit-delete" type="submit" value="Delete" name="tmi_delete" /></td>
                <td><input  class="cms-submit" type="submit" value="Add" name="tmi_addbelow" /></td>
                <td><input  class="cms-submit" type="submit" value="Up" name="tmi_moveup" /></td>
                <td><input  class="cms-submit" type="submit" value="Down" name="tmi_movedown" /></form></td>';                                
                echo '</tr>';
            }    
            echo '</table>';     
            echo '</div>';        
        } // if
        
        // add new menu top item
        echo '<div style="margin-bottom:15px;margin-top:15px;">';
        echo '<form action="#" method="POST">';
        echo '
            <input style="width:140px;" type="text" name="name" value="Item name" />
            <input style="width:180px;" class="cms-submit" type="submit" value="Add new menu top item" name="tmi_add" />';
        echo '</form>';
        
        // add new submenu
        echo '<form action="#" method="POST">';
        echo '
            <input style="width:140px;" type="text" name="name" value="Name" />
            <input style="width:180px;" class="cms-submit" type="submit" value="Create new submenu" name="sub_add" />';
        echo '</form></div>';                     
    }
        
    private function renderSubmenuCMS()
    {     
        $subcount = count($this->_sub);
        
        for($i = 0; $i < $subcount; $i++)
        {   
            echo '<a name="submenu_'.$i.'"></a>';
            echo '<div style="margin-bottom:30px;margin-top:70px;">';
            echo '<h6 class="cms-h6">Submenu: '.$this->_sub[$i]->_name.'</h6><hr class="cms-hr"/>';
            // submenu settings
            echo '<form action="#submenu_'.$i.'" method="POST">';
            echo '
                <input style="width:120px;" type="text" name="name" value="'.$this->_sub[$i]->_name.'" />
                <input  class="cms-submit" type="hidden" value="'.$i.'" name="subindex" />
                <input  class="cms-submit" type="submit" value="Save" name="sub_save" />
                <input onclick="return confirm('."'Delete submenu?'".')" class="cms-submit-delete" type="submit" value="Delete" name="sub_delete" />';
            echo '</form></div>'; 
            
            $itemscount = count($this->_sub[$i]->_items);
            
            if($itemscount > 0)
            {            
          
                echo '<div style="margin-bottom:30px;"><table>';        
                echo '<thead>
                        <tr>
                            <th style="padding:0px 3px 0px 3px;">Hide</th>
                            <th style="padding:0px 3px 0px 3px;">Name</th>
                            <th style="padding:0px 3px 0px 3px;">Page</th>
                            <th style="padding:0px 3px 0px 3px;">Use</th>
                            <th style="padding:0px 3px 0px 3px;">Link</th>
                            <th style="padding:0px 3px 0px 3px;">Use</th>
                            <th style="padding:0px 3px 0px 3px;">Submenu</th>
                            <th style="padding:0px 3px 0px 3px;">Target</th>
                            <th style="padding:0px 3px 0px 3px;">Save</th>
                            <th style="padding:0px 3px 0px 3px;">Delete</th>
                            <th style="padding:0px 3px 0px 3px;">Add</th>
                            <th style="padding:0px 3px 0px 3px;">Up</th>
                            <th style="padding:0px 3px 0px 3px;">Down</th>
                        </tr>
                      </thead>';

                for($j = 0; $j < $itemscount; $j++)
                {            
                    echo '<tr>';
                    // hide menu item
                    echo '              
                        <td style="text-align:center;"><form action="#submenu_'.$i.'" method="POST">
                        <input type="checkbox" '.($this->_sub[$i]->_items[$j]->_hide ? ' checked="checked" ' : '').' name="hide" title="Select to hide element" /></td>'; 
                    // name     
                    echo '<td><input type="hidden" name="subindex" value="'.$i.'" /><input type="hidden" name="index" value="'.$j.'" />
                        <input style="width:100px;" type="text" name="name" value="'.$this->_sub[$i]->_items[$j]->_name.'" title="Menu item name" /></td>';
                    // page / use page                   
                    echo '<td>'.$this->selectCtrlPagesList($this->_sub[$i]->_items[$j]->_page, 'page', 120).'</td>'; 
                    
                    echo '<td style="text-align:center;"><input type="radio" '.($this->_sub[$i]->_items[$j]->_type == CPMenuItem::ITEM_USE_PAGE ? ' checked="checked" ' : '').'  
                        value="'.CPMenuItem::ITEM_USE_PAGE.'" name="type" title="Select to use page address as link" /></td>';
                    // link / use link   
                    echo '<td><input style="width:100px;" type="text" name="link" value="'.$this->_sub[$i]->_items[$j]->_link.'" /></td>
                        <td style="text-align:center;"><input type="radio" name="type" '.($this->_sub[$i]->_items[$j]->_type == CPMenuItem::ITEM_USE_LINK ? ' checked="checked" ' : '').'
                        value="'.CPMenuItem::ITEM_USE_LINK.'" title="Select to use own address as link" /></td>';
                    // submenu     
                    $this->printSubmenuList($i, $this->_sub[$i]->_items[$j]->_submenu, 100);
                    
                    echo '<td style="width:110px;text-align:center;">
                        <input type="radio" name="target" '.$this->attrChecked($this->_sub[$i]->_items[$j]->_target == CPMenuItem::ITEM_TARGET_BLANK).' value="'.CPMenuItem::ITEM_TARGET_BLANK.'" />Blank '; 
                    echo '<input type="radio" name="target" '.$this->attrChecked($this->_sub[$i]->_items[$j]->_target == CPMenuItem::ITEM_TARGET_SELF).' value="'.CPMenuItem::ITEM_TARGET_SELF.'" />Self</td>';
                                        
                    
                    // save / delete 
                    echo '<td><input  class="cms-submit" type="submit" value="Save" name="subi_save" /></td> 
                        <td><input  onclick="return confirm('."'Delete item?'".')" class="cms-submit-delete" type="submit" value="Delete" name="subi_delete" /></td>
                    <td><input  class="cms-submit" type="submit" value="Add" name="subi_addbelow" /></td>
                    <td><input  class="cms-submit" type="submit" value="Up" name="subi_moveup" /></td>
                    <td><input  class="cms-submit" type="submit" value="Down" name="subi_movedown" /></form></td>';                                
                    echo '</tr>';
                } // for $j   
                echo '</table></div>';          
            
            } // if                
            
            // add new menu top item
            echo '<div style="margin-bottom:20px;">';
            echo '<form action="#submenu_'.$i.'" method="POST">';
            echo '
                <input style="width:280px;" type="text" name="name" value="Item name" />
                <input type="hidden" name="subindex" value="'.$i.'" />
                <input class="cms-submit" type="submit" value="Add new submenu item" name="subi_add" />';
            echo '</form></div>';            
            
        } // for $i        
    }       
            
    private function printSubmenuList($subindex, $itemsub, $width)
    {
        echo '<td><select style="width:'.$width.'px;" name="submenu"><option value="'.CMS_NOT_SELECTED.'">Not selected</option>';
        
        $count = count($this->_sub);
        for($i = 0; $i < $count; $i++)
        {
            if($subindex !== null)
            {
                if($subindex == $i) continue;    
            }
            
            $out = '<option value="'.$i.'" ';
            
            if($itemsub !== null)
            {
                if($i == $itemsub) $out .= ' selected="selected" ';
            }
            
            $out .= '>';
            $out .= $this->_sub[$i]->_name;
            $out .= '</option>';
            
            echo $out;    
        } // for        
        echo '</select></td>';        
    }              
               
} // class CPMenu
        
        
?>