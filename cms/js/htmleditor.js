/*********************************************************** 
* MAIN
************************************************************/  


edButtons[edButtons.length] = new edButton('ed_p', 'p', '<p>', '</p>','p');

// h1
edButtons[edButtons.length] = new edButton('ed_H1', 'H1',
    '<h1>',
    '</h1>','H1');

// h2
edButtons[edButtons.length] = new edButton('ed_H2', 'H2',
    '<h2>',
    '</h2>','H2');
    
// h3
edButtons[edButtons.length] = new edButton('ed_H3', 'H3',
    '<h3>',
    '</h3>','H3');
    
// h4
edButtons[edButtons.length] = new edButton('ed_H4', 'H4',
    '<h4>',
    '</h4>','H4');
    
// h5
edButtons[edButtons.length] = new edButton('ed_H5', 'H5',
    '<h5>',
    '</h5>','H5');
    
// h6
edButtons[edButtons.length] = new edButton('ed_H6', 'H6',
    '<h6>',
    '</h6>','H6');
    
// li
edButtons[edButtons.length] = new edButton('ed_li', 'li',
    '\t<li>',
    '</li>','li');    
    
// pre
edButtons[edButtons.length] = new edButton('ed_pre', 'pre',
    '<pre>',
    '</pre>','pre');    

// HSubtitle
edButtons[edButtons.length] = new edButton('ed_HSubtitle', 'H-Subtitle',
    '<h3>Heading text<span>',
    '</span></h3>','HSubtitle');    
    
// note
edButtons[edButtons.length] = new edButton('ed_note', 'note',
    '<div class="note-block">',
    '</div>','note');   

// span
edButtons[edButtons.length] = new edButton('ed_cspan', 'cspan',
    '<span style="color:#3399CC;">',
    '</span>','cspan');      
    
// small
edButtons[edButtons.length] = new edButton('ed_small', 'small',
    '<small>',
    '</small>','small');  
    
// psmall
edButtons[edButtons.length] = new edButton('ed_psmall', 'psmall',
    '<p class="small">',
    '</p>','psmall');         
    
// clear
edButtons[edButtons.length] = new edButton('ed_clear', 'clear',
    '<div class="clear-both"></div>',
    '','clear'); 
    
// a
edButtons[edButtons.length] = new edButton('ed_a', 'a',
    '<a href="#" target="_self">',
    '</a>','a');         
    
// br
edButtons[edButtons.length] = new edButton('ed_br', 'br',
    '<br />',
    '','br');      
    
// blockquote
edButtons[edButtons.length] = new edButton('ed_blockquote', 'blockquote',
    '<blockquote>\rBlock quote text\r<span class="author">Author name</span><span class="authortitle">Title</span>\r</blockquote>',
    '','blockquote'); 
  
// head block
edButtons[edButtons.length] = new edButton('ed_headblock', 'head-block',
    '<div class="common-block-head"><span class="head-text">',
    '</span></div>','headblock');     
    
// ul arrow
edButtons[edButtons.length] = new edButton('ed_ularrow', 'ul-arrow',
    '<ul class="list-arrow">\r\t<li>List item</li>\r</ul>',
    '','ularrow');          

// ul accept
edButtons[edButtons.length] = new edButton('ed_ulaccept', 'ul-accept',
    '<ul class="list-accept">\r\t<li>List item</li>\r</ul>',
    '','ulaccept');  
    
// ul exception
edButtons[edButtons.length] = new edButton('ed_ulexception', 'ul-exception',
    '<ul class="list-exception">\r\t<li>List item</li>\r</ul>',
    '','ulexception');      

// ul dot
edButtons[edButtons.length] = new edButton('ed_uldot', 'ul-dot',
    '<ul class="list-dot-blue">\r\t<li>List item</li>\r</ul>',
    '','uldot');

// ul square
edButtons[edButtons.length] = new edButton('ed_ulsquare', 'ul-square',
    '<ul class="list-squ-blue">\r\t<li>List item</li>\r</ul>',
    '','ulsquare');
    
// empty space
edButtons[edButtons.length] = new edButton('ed_emptyspace', 'empty space',
    '<div style="clear:both;height:15px;"></div>',
    '','emptyspace');       
    
// table
edButtons[edButtons.length] = new edButton('ed_table', 'table',
    '<table class="dc-theme-table" style="width:100%;">\r<thead>\r\t<tr>\r\t<td>First</td>\r\t<td>Second</td>\r\t</tr>\r</thead>\r<tbody>\r\t<tr><td>sample</td><td>sample</td></tr>\r</tbody>\r</table>',
    '','table');  
    
// nextpage
edButtons[edButtons.length] = new edButton('ed_page', 'page',
    '<!--nextpage-->',
    '','page');     