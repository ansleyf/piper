/**********************************************************************
* MODERN ELEGANCE WORDPRESS THEME 
* (Ideal For Business And Personal Use: Portfolio or Blog)   
* 
* File name:   
*      cms.js
* Brief:       
*      JavaScript code
* Author:      
*      DigitalCavalry
* Author URI:
*      http://themeforest.net/user/DigitalCavalry
* Contact:
*      digitalcavalry@gmail.com 
**********************************************************************/

/*********************************************************** 
* MAIN
************************************************************/  

function setupTogglePanelsCMS()
{
    var q = jQuery.noConflict(); 
    
    q('.cms-toggle-triger').click(function() {q(this).parent().find('.cms-toggle-content').slideToggle(200, 
    function()
    {
        var state = q(this).css('display');
        var parent = q(this).parent();
        
        if(state == 'block')
        {
            q(parent).find('.cms-toggle-icon-open').removeClass('cms-toggle-icon-open').addClass('cms-toggle-icon-close');
        } else
        {
            q(parent).find('.cms-toggle-icon-close').removeClass('cms-toggle-icon-close').addClass('cms-toggle-icon-open');  
        }
    }
    
    ); });        
}

var dc_formfield = null; 
jQuery(document).ready(function($) 
    {   
        var q = jQuery.noConflict(); 
        var dc_url = q('meta[name=cms_url]').attr('content');
        
        setupTogglePanelsCMS();
         
        q('ul.cms-sortable-list-simple').sortable({
        axis: "y",
        cursor: "move",
        update: function() {  }
    }); 
       
         q('.cms-sortable-wrapper .dc-submit-btn').each(function() { q(this).click(
            function() 
            { 
                var parent = q(this).parent();
                var data = parent.find('ul').sortable('toArray');
                var action = parent.find('.dc-action-name').text();
                
                var len = q('.cms-ajax-progress').length;
                if(len)
                {
                    q('.cms-ajax-progress').stop().remove();     
                } 
                q('body').append('<div class="cms-ajax-progress"><div class="image"></div>Wait. Ajax in work..</div>');                
                q('.cms-ajax-progress').css('opacity', 1.0);
                
                q.ajax({
               type: "POST",
               url: dc_url+'/wp-admin/admin-ajax.php',
               data: "action="+action+'&data='+data,
               success: function(str){
                 
                 var count = parent.find('ul li').length;  
                 for(i = 0; i < count; i++)
                 {
                    parent.find('ul li:eq('+(i)+') span').text(i+1);    
                 }
                 q('.cms-ajax-progress').animate({opacity:0.0}, 1000, function() { q(this).remove(); });
               }
             });                  
                
            });
            
         });

    
          
        // saved bar
        if(q('.cms-saved-bar').length)
        {
            function cmsHideSaveBox(box) { q(box).animate({opacity:0.0}, 1600); }
            $savebox = q('.cms-saved-bar');
            $savebox.stop().css('opacity', 0.0).animate({opacity:1.0}, 1200).animate({opacity:0.0}, 1200).animate({opacity:1.0}, 1200, function() { setTimeout(function(){cmsHideSaveBox($savebox);}, 8000); });        
        }
        
        // image loading        
        
        q('.upload_image_button').click(function() {
               
         dc_formfield = q(this).attr('name');  
         tb_show('', 'media-upload.php?type=image&amp;TB_iframe=true'); 
         return false;

        });

        window.original_send_to_editor = null;
        if(window.original_send_to_editor == null)
        {
            window.original_send_to_editor = window.send_to_editor;
            
            window.send_to_editor = function(html) {

                if(dc_formfield)
                {
                   // imgurl = q('img',html).attr('src');
                   imgurl = q(html).attr('href');  

                    q('#'+dc_formfield).val(imgurl);
                    tb_remove();
                    dc_formfield = null;
                } else
                {
                    window.original_send_to_editor(html);
                }
            }
        }  
        
        q('.dc-add-media-image').click(function(){
            var d=new Date();
            var dc_theme_path = q('meta[name=cms_theme_url]').attr('content'); 
            var boxpath = dc_theme_path+'/cms/lib/js/box/image.html?'+d.getTime();
            TINY.box.show({url:boxpath,width:920,height:555,closejs:function(){ dcAdminBoxImageClose(); },openjs:function(){ dcAdminBoxImageSetup(); }});
            
        });
        
        q('.dc-add-media-shortcodes').click(function(){
            var d=new Date();
            var dc_theme_path = q('meta[name=cms_theme_url]').attr('content'); 
            var boxpath = dc_theme_path+'/cms/lib/js/box/shortcodes.html?'+d.getTime();
            TINY.box.show({url:boxpath,width:920,height:245,closejs:function(){ dcAdminBoxShortcodesClose(); },openjs:function(){ dcAdminBoxShortcodesSetup(); }});
            
        });        
        
        q('.dc-add-media-btn').click(function() {                      
                   
            var start = ''; 
            if(q(this).find('.start').length)
            {
                start = q(this).find('.start').html();
                start = start.replace('<!-- ', '');
                start = start.replace(' -->', '');
            }
            var end = ''; 
            if(q(this).find('.end').length)
            {
                end = q(this).find('.end').html();
                end = end.replace('<!-- ', '');
                end = end.replace(' -->', '');                
            }   

            var tag = { tagStart: start, tagEnd : end };
            edDCThemeInsertTag(window.edCanvas, tag);         
            return false;
        
        });               
    }); 
   
function dcAdminBoxShortcodesSetup()
{
    var q = jQuery.noConflict(); 
    var dc_theme_url = q('meta[name=cms_theme_url]').attr('content');
    var dc_url = q('meta[name=cms_url]').attr('content');
    
    var $con = q('.cms-box .cms-box-content');    
    jscolor.bind();
     
    var default_panel = $con.find('select[name=shortcode]').find(':selected').val(); 
    $con.find('#panel-'+default_panel).css('display', 'block');
     
    $con.find('select[name=shortcode]').change(function() { 
        var value = q(this).find(':selected').val();
         
        $con.find('.panel').css('display', 'none');
        $con.find('.buttons-panel').css('opacity', 0.0);
        $con.find('#panel-'+value).css('opacity', 0.0).css('display', 'block');
        
        var w = $con.width();
        var h = $con.height();
        TINY.box.size(w,h,1, function() { $con.find('#panel-'+value).css('opacity', 1.0); $con.find('.buttons-panel').css('opacity', 1.0); });
    }); 
    
    
    q.ajax({
   type: "POST",
   url: dc_url+'/wp-admin/admin-ajax.php',
   data: "action=dc_ngg_list",
   success: function(str){
     $con.find('select[name=ngg_list]').append(str); 
   }
 });    
    
    q.ajax({
   type: "POST",
   url: dc_url+'/wp-admin/admin-ajax.php',
   data: "action=dc_page_list",
   success: function(str){
     $con.find('select[name=page_list]').append(str); 
   }
 });   
    
     
    q('.cms-box .cms-box-content .insert-btn').click(function() 
    {    
                         
        var dcs_code = $con.find('select[name=shortcode]').find(':selected').val();         
              
        var ft = '';        
        var $panel = $con.find('#panel-'+dcs_code); 
        var CMS_NOT_SELECTED = -1;
        
        switch(dcs_code)
        {
            case 'dcs_head':    
            {            
                var color = $panel.find('input[name=color]').val();
                var topmargin = $panel.find('input[name=topmargin]').val();
                ft = '[dcs_head top="'+topmargin+'" color="'+color+'"]Heading text here[/dcs_head]';
            }
            break;

            case 'dcs_btn':    
            {            
                var name = $panel.find('input[name=name]').val();
                var url = $panel.find('input[name=url]').val();
                var color = $panel.find('input[name=color]').val();
                var hcolor = $panel.find('input[name=hcolor]').val();
                ft = '[dcs_btn name="'+name+'" url="'+url+'" color="'+color+'" hcolor="'+hcolor+'" /]';
            }
            break;

            case 'dcs_btn_fullwidth':    
            {            
                var name = $panel.find('input[name=name]').val();
                var url = $panel.find('input[name=url]').val();
                ft = '[dcs_btn_fullwidth name="'+name+'" url="'+url+'" /]';
            }
            break;
         /*   
            case 'dcs_story':    
            {            
                var name = $panel.find('input[name=name]').val();
                if(name != '') { name = ' name="'+name+'" '; } else { name = ''; }

                var list = $panel.find('input[name=list]').val();
                list = ' list="'+list+'" '; 

                var link = $panel.find('input[name=link]').val();
                if(link != '') { link = ' link="'+link+'" '; } else { link = ''; } 

                var target = $panel.find('input[name=target]').val();
                target = ' target="'+target+'" '; 
                if(link == '') { target = ''; }
                
                var width = $panel.find('input[name=width]').val();
                width = ' w="'+width+'" ';
                
                var height = $panel.find('input[name=height]').val();
                height = ' h="'+height+'" ';                            
                
                var mleft = $panel.find('input[name=mleft]').val();   
                if(mleft != 'auto') { mleft = mleft+'px'; }            
                var mright = $panel.find('input[name=mright]').val();
                if(mright != 'auto') { mright = mright+'px'; }
                var mtop = $panel.find('input[name=mtop]').val();
                if(mtop != 'auto') { mtop = mtop+'px'; }
                var mbottom = $panel.find('input[name=mbottom]').val();
                if(mbottom != 'auto') { mbottom = mbottom+'px'; }
                var margin = ' margin="'+mtop+' '+mright+' '+mbottom+' '+mleft+'" ';                   
                
                var floating = $panel.find('input[name=float]:checked').val();
                if(floating != 'none') { floating = ' float="'+floating+'" '; } else { floating = ''; }                
                
                var group = $panel.find('input[name=group]').is(':checked') ? true : false; 
                if(group) { group = ' group="true" '; } else { group = ''; }; 

                var overwrite = $panel.find('input[name=overwrite]').is(':checked') ? true : false; 
                if(overwrite) { overwrite = ' overwrite="true" '; } else { overwrite = ''; };                 

                var fit = $panel.find('input[name=fit]').is(':checked') ? true : false; 
                if(!fit) { fit = ' fit="false" '; } else { fit = ''; };   
                
                var showdesc = $panel.find('input[name=showdesc]').is(':checked') ? true : false; 
                if(!showdesc) { showdesc = ' showdesc="false" '; } else { showdesc = ''; }; 

                var showtitle = $panel.find('input[name=showtitle]').is(':checked') ? true : false; 
                if(!showtitle) { showtitle = ' showtitle="false" '; } else { showtitle = ''; };                 
                
                ft = '[dcs_story '+name+width+height+list+link+target+margin+floating+fit+group+overwrite+showdesc+showtitle+' /]';
            }
            break;            
           */ 
            case 'dcs_hidden':
            {
                var title = $panel.find('input[name=title]').val();
                var color = $panel.find('input[name=color]').val();
                var execute = $panel.find('input[name=execute]').is(':checked') ? true : false;
                if(execute) { execute = ''; } else { execute = ' execute="false" '; }
                
                var mleft = $panel.find('input[name=mleft]').val();               
                var mright = $panel.find('input[name=mright]').val();
                var mtop = $panel.find('input[name=mtop]').val();
                var mbottom = $panel.find('input[name=mbottom]').val();
                var margin = ' margin="'+mtop+'px '+mright+'px '+mbottom+'px '+mleft+'px" '; 
                if(mleft == 0 && mright == 0 && mbottom == 15 && mtop == 0) { margin = ''; }
                               
                
                ft = '[dcs_hidden title="'+title+'" color="'+color+'" '+execute+margin+']Your hidden content here[/dcs_hidden]'; 
            }
            break;
            
            case 'dcs_span':
            {
                var color = $panel.find('input[name=color]').val();
                var bgcolor = $panel.find('input[name=bgcolor]').val();
                var use_bgcolor = $panel.find('input[name=use_bgcolor]').is(':checked') ? true : false; 
                if(use_bgcolor) { bgcolor = ' bgcolor="'+bgcolor+'" '; } else { bgcolor = ''; }
                ft = '[dcs_span color="'+color+'" '+bgcolor+']Your content here[/dcs_span]'; 
            }
            break;   
            
            case 'dcs_highlight':
            {
                var color = $panel.find('input[name=color]').val();
                var use_color = $panel.find('input[name=use_color]').is(':checked') ? true : false; 
                if(use_color) { color = ' color="'+color+'" '; } else { color = ''; }
                ft = '[dcs_highlight'+color+']Your content here[/dcs_highlight]';             
            }
            break; 

            case 'dcs_empty':
            {
                var height = $panel.find('input[name=height]').val();
                ft = '[dcs_empty h="'+height+'" /]';             
            }
            break;             

            case 'dcs_clear':
            {
                var height = $panel.find('input[name=height]').val();                
                ft = '[dcs_clear h="'+height+'" /]';             
            }
            break; 
                  
            case 'dcs_top':
            {
                var top = $panel.find('input[name=top]').val();
                var bottom = $panel.find('input[name=bottom]').val();
                ft = '[dcs_top top="'+top+'" bottom="'+bottom+'" /]';             
            }
            break;
                               
            case 'dcs_fancy_header':
            {
                var mleft = $panel.find('input[name=mleft]').val();               
                var mright = $panel.find('input[name=mright]').val();
                var mtop = $panel.find('input[name=mtop]').val();
                var mbottom = $panel.find('input[name=mbottom]').val();
                var margin = ' margin="'+mtop+'px '+mright+'px '+mbottom+'px '+mleft+'px" ';
                
                var smleft = $panel.find('input[name=smleft]').val();   
                if(smleft != 'auto') { smleft = smleft+'px'; }            
                var smright = $panel.find('input[name=smright]').val();
                if(smright != 'auto') { smright = smright+'px'; }
                var smtop = $panel.find('input[name=smtop]').val();
                if(smtop != 'auto') { smtop = smtop+'px'; }
                var smbottom = $panel.find('input[name=smbottom]').val();
                if(smbottom != 'auto') { smbottom = smbottom+'px'; }
                var smargin = ' smargin="'+smtop+' '+smright+' '+smbottom+' '+smleft+'" '; 
                
                var width = $panel.find('input[name=width]').val();
                var align = $panel.find('input[name=align]:checked').val(); 
                var salign = $panel.find('input[name=salign]:checked').val();
                
                var color = $panel.find('input[name=color]').val();
                var use_color = $panel.find('input[name=use_color]').is(':checked') ? true : false; 
                if(use_color) { color = ' color="'+color+'" '; } else { color = ''; }                
                
                ft = '[dcs_fancy_header'+margin+smargin+' width="'+width+'" align="'+align+'" salign="'+salign+'"'+color+']Your content here[/dcs_fancy_header]';  
            }           
            break;
            
            case 'dcs_small_block':
            {
                var color = $panel.find('input[name=color]').val();
                var use_color = $panel.find('input[name=use_color]').is(':checked') ? true : false; 
                if(use_color) { color = ' color="'+color+'" '; } else { color = ''; }                

                var bgcolor = $panel.find('input[name=bgcolor]').val();
                var use_bgcolor = $panel.find('input[name=use_bgcolor]').is(':checked') ? true : false; 
                if(use_bgcolor) { bgcolor = ' bgcolor="'+bgcolor+'" '; } else { bgcolor = ''; }      
                
                var bcolor = $panel.find('input[name=bcolor]').val();
                var border = $panel.find('input[name=border]').is(':checked') ? true : false;
                if(border) { border = ' border="true" '; } else { border = ' border="false" '; }
                
                var pleft = $panel.find('input[name=pleft]').val();               
                var pright = $panel.find('input[name=pright]').val();
                var ptop = $panel.find('input[name=ptop]').val();
                var pbottom = $panel.find('input[name=pbottom]').val();
                var padding = ' padding="'+ptop+'px '+pright+'px '+pbottom+'px '+pleft+'px" ';                
                if(pleft == 0 && pright == 0 && ptop == 0 && pbottom == 0) { padding = ''; }
                
                var align = $panel.find('input[name=align]:checked').val();
                var fsize = $panel.find('select[name=fsize]').find(':selected').val(); 
                var fheight = $panel.find('select[name=fheight]').find(':selected').val()
                
                ft = '[dcs_small_block '+color+border+padding+' align="'+align+'" bcolor="'+bcolor+'" '+bgcolor+' fheight="'+fheight+'" fsize="'+fsize+'"]Your content here[/dcs_small_block]';              
            }
            break;
            
            case 'dcs_lb_link':
            {
                var color = $panel.find('input[name=color]').val();
                var use_color = $panel.find('input[name=use_color]').is(':checked') ? true : false; 
                if(use_color) { color = ' color="'+color+'" '; } else { color = ''; }                

                var group = $panel.find('input[name=group]').val();
                var use_group = $panel.find('input[name=use_group]').is(':checked') ? true : false; 
                if(use_group) { group = ' group="'+group+'" '; } else { group = ''; }  
                
                var title = $panel.find('input[name=title]').val();
                if(title != '') { title = ' title="'+title+'" '; }
                var url = $panel.find('input[name=url]').val();
                
                ft = '[dcs_lb_link '+color+title+group+' url="'+url+'"]Your content here[/dcs_lb_link]';                  
            }
            break;

            case 'dcs_lb_ngg':
            {
                var color = $panel.find('input[name=color]').val();
                var use_color = $panel.find('input[name=use_color]').is(':checked') ? true : false; 
                if(use_color) { color = ' color="'+color+'" '; } else { color = ''; }                

                var group = $panel.find('input[name=group]').val();
                var use_group = $panel.find('input[name=use_group]').is(':checked') ? true : false; 
                if(use_group) { group = ' group="'+group+'" '; } else { group = ''; }  
                
                var id = $panel.find('input[name=id]').val();
                
                ft = '[dcs_lb_ngg '+color+group+' id="'+id+'"]Your content here[/dcs_lb_ngg]';                  
            }
            break;
            
            case 'dcs_blockquote':  
            {          
                var color = $panel.find('input[name=color]').val();
                var use_color = $panel.find('input[name=use_color]').is(':checked') ? true : false; 
                if(use_color) { color = ' color="'+color+'" '; } else { color = ''; }
                
                var width = $panel.find('input[name=width]').val();
                if(width > 0) { width = ' width="'+width+'" '; } else { width = ''; }

                var author = $panel.find('input[name=author]').val();
                if(author != '') { author = ' author="'+author+'" '; } else { author = ''; }

                var title = $panel.find('input[name=title]').val();
                if(title != '') { title = ' title="'+title+'" '; } else { title = ''; }
                
                var mleft = $panel.find('input[name=mleft]').val();   
                if(mleft != 'auto') { mleft = mleft+'px'; }            
                var mright = $panel.find('input[name=mright]').val();
                if(mright != 'auto') { mright = mright+'px'; }
                var mtop = $panel.find('input[name=mtop]').val();
                if(mtop != 'auto') { mtop = mtop+'px'; }
                var mbottom = $panel.find('input[name=mbottom]').val();
                if(mbottom != 'auto') { mbottom = mbottom+'px'; }
                var margin = ' margin="'+mtop+' '+mright+' '+mbottom+' '+mleft+'" ';              
                
                var floating = $panel.find('input[name=float]:checked').val();
                if(floating != 'none') { floating = ' float="'+floating+'" '; } else { floating = ''; }
                
                var border = $panel.find('input[name=show_border]').is(':checked') ? true : false; 
                if(!border) { border = ' border="false" '; } else { border = ''; };
                
                var align = $panel.find('input[name=align]:checked').val();
                
                ft = '[dcs_blockquote '+margin+color+width+floating+border+author+title+' align="'+align+'"]Your content here[/dcs_blockquote]';                
            }
            break;
            
            case 'dcs_ul_list':
            {
                var type = $panel.find('select[name=type]').find(':selected').val() 
                
                ft = '[dcs_ul_list type="'+type+'"] <li>list item</li> <li>list item</li> [/dcs_ul_list]';    
            }
            break;             
            
            case 'dcs_note':
            {
                var color = $panel.find('input[name=color]').val();
                color = ' color="'+color+'" ';
                var bcolor = $panel.find('input[name=bcolor]').val();
                bcolor = ' bcolor="'+bcolor+'" ';
                var bgcolor = $panel.find('input[name=bgcolor]').val();
                bgcolor = ' bgcolor="'+bgcolor+'" ';
                
                var execute = $panel.find('input[name=execute]').is(':checked') ? true : false;
                if(execute) { execute = ''; } else { execute = ' execute="false" '; }
                
                var align = $panel.find('input[name=align]:checked').val(); 
                if(align != 'left') { align = ' align="'+align+'" '; } else { align = ''; }
                
                var rounded = $panel.find('select[name=rounded]').find(':selected').val();
                if(rounded > 0) { rounded = ' rounded="'+rounded+'" '; } else { rounded = ''; }                
                
                ft = '[dcs_note '+color+bcolor+bgcolor+execute+align+rounded+']Your content here[/dcs_note]'; 
            }
            break;
            
            case 'dcs_box':
            {
                var width = $panel.find('input[name=width]').val(); 
                width = ' w="'+width+'" '; 
                var use_width = $panel.find('input[name=use_width]').is(':checked') ? true : false;
                if(!use_width) { width = ''; } 
                
                var height = $panel.find('input[name=height]').val(); 
                height = ' h="'+height+'" '; 
                var use_height = $panel.find('input[name=use_height]').is(':checked') ? true : false;
                if(!use_height) { height = ''; }                 
                
                var color = $panel.find('input[name=color]').val();
                color = ' color="'+color+'" ';
                var use_color = $panel.find('input[name=use_color]').is(':checked') ? true : false;
                if(!use_color) { color = ''; }
                
                var bwidth = $panel.find('input[name=bwidth]').val(); 
                bwidth = ' bwidth="'+bwidth+'" ';
                var bcolor = $panel.find('input[name=bcolor]').val();
                bcolor = ' bcolor="'+bcolor+'" ';
                var show_border = $panel.find('input[name=show_border]').is(':checked') ? true : false;
                if(!show_border) { bcolor = ''; bwidth = ''; }                
                
                var bgcolor = $panel.find('input[name=bgcolor]').val();
                bgcolor = ' bgcolor="'+bgcolor+'" ';
                var use_bgcolor = $panel.find('input[name=use_bgcolor]').is(':checked') ? true : false;
                if(!use_bgcolor) { bgcolor = ''; }                
                
                var execute = $panel.find('input[name=execute]').is(':checked') ? true : false;
                if(execute) { execute = ''; } else { execute = ' execute="false" '; }
                
                var align = $panel.find('input[name=align]:checked').val(); 
                if(align != 'left') { align = ' align="'+align+'" '; } else { align = ''; }
                
                var rounded = $panel.find('select[name=rounded]').find(':selected').val();
                if(rounded > 0) { rounded = ' rounded="'+rounded+'" '; } else { rounded = ''; }                
                
                var pleft = $panel.find('input[name=pleft]').val();               
                var pright = $panel.find('input[name=pright]').val();
                var ptop = $panel.find('input[name=ptop]').val();
                var pbottom = $panel.find('input[name=pbottom]').val();
                var padding = ' padding="'+ptop+'px '+pright+'px '+pbottom+'px '+pleft+'px" ';                
                if(pleft == 0 && pright == 0 && ptop == 0 && pbottom == 0) { padding = ''; }                 
                
                var mleft = $panel.find('input[name=mleft]').val();                              
                var mright = $panel.find('input[name=mright]').val();
                var mtop = $panel.find('input[name=mtop]').val();                
                var mbottom = $panel.find('input[name=mbottom]').val();
                
                var margin = ''; 
                if(mleft == 0 && mright == 0 && mbottom == 15 && mtop == 0) 
                {  
                    margin = ''; 
                } 
                else
                {
                    if(mbottom != 'auto') { mbottom = mbottom+'px'; }
                    if(mleft != 'auto') { mleft = mleft+'px'; } 
                    if(mright != 'auto') { mright = mright+'px'; }
                    if(mtop != 'auto') { mtop = mtop+'px'; } 
                    margin = ' margin="'+mtop+' '+mright+' '+mbottom+' '+mleft+'" ';                   
                }                
                
                var floating = $panel.find('input[name=float]:checked').val();
                if(floating != 'none') { floating = ' float="'+floating+'" '; } else { floating = ''; }
                
                var fsize = $panel.find('select[name=fsize]').find(':selected').val(); 
                if(fsize != 'default') { fsize = ' fsize="'+fsize+'" '; } else { fsize = ''; }               
                
                var font = $panel.find('select[name=font]').find(':selected').val(); 
                if(font != 'default') { font = ' font="'+font+'" '; } else { font = ''; }                  
                
                var bg = $panel.find('input[name=bg]').val();
                if(bg != '') { bg = ' bg="'+bg+'" '; } else { bg = ''; } 
                
                var bgrepeat = $panel.find('select[name=bgrepeat]').find(':selected').val(); ;
                if(bgrepeat != 'no-repeat') { bgrepeat = ' bgrepeat="'+bgrepeat+'" '; } else { bgrepeat = ''; }                 
                
                var bgpos = $panel.find('input[name=bgpos]').val();
                if(bgpos != 'left top') { bgpos = ' bgpos="'+bgpos+'" '; } else { bgpos = ''; }                    
                
                var uppercase = $panel.find('input[name=uppercase]').is(':checked') ? true : false;
                if(uppercase) { uppercase = ' uppercase="true" '; } else { uppercase = ' uppercase="false" '; } 
                
                ft = '[dcs_box '+width+height+color+bcolor+bgcolor+execute+align+rounded+bwidth+padding+margin+floating+font+fsize+bg+bgpos+bgrepeat+uppercase+']Your content here[/dcs_box]'; 
            }
            break;            
            
            case 'dcs_column':
            {
                var color = $panel.find('input[name=color]').val();
                var use_color = $panel.find('input[name=use_color]').is(':checked') ? true : false; 
                if(use_color) { color = ' color="'+color+'" '; } else { color = ''; }            

                var bgcolor = $panel.find('input[name=bgcolor]').val();
                var use_bgcolor = $panel.find('input[name=use_bgcolor]').is(':checked') ? true : false; 
                if(use_bgcolor) { bgcolor = ' bgcolor="'+bgcolor+'" '; } else { bgcolor = ''; }  
                
                var bcolor = $panel.find('input[name=bcolor]').val();
                bcolor = ' bcolor="'+bcolor+'" ';
                var bwidth = $panel.find('input[name=bwidth]').val();
                bwidth = ' bwidth="'+bwidth+'" ';
                var border = $panel.find('input[name=show_border]').is(':checked') ? true : false;
                if(!border) { border = bcolor = bwidth = ''; } else { border = ' border="true" '; }
                
                var align = $panel.find('input[name=align]:checked').val();
                if(align == 'left') { align = ''; } else { align = ' align="'+align+'" '; }
                        
                var mleft = $panel.find('input[name=mleft]').val();                             
                if(mleft > 0) { mleft = ' mleft="'+mleft+'" '; } else { mleft = ''; }
                
                var mright = $panel.find('input[name=mright]').val();        
                mright = ' mright="'+mright+'" ';
                
                var floating = $panel.find('input[name=float]:checked').val();
                floating = ' float="'+floating+'" ';
                
                var width = $panel.find('input[name=width]').val();                 
                width = ' w="'+width+'" ';
                
                var pleft = $panel.find('input[name=pleft]').val();               
                var pright = $panel.find('input[name=pright]').val();
                var ptop = $panel.find('input[name=ptop]').val();
                var pbottom = $panel.find('input[name=pbottom]').val();
                var padding = ' padding="'+ptop+'px '+pright+'px '+pbottom+'px '+pleft+'px" ';                
                if(pleft == 0 && pright == 0 && ptop == 0 && pbottom == 0) { padding = ''; }                
            
                ft = '[dcs_column '+width+floating+mleft+mright+padding+color+bgcolor+align+border+bcolor+bwidth+']Your content here\r[/dcs_column]'; 
            }    
            break;
            
            case 'dcs_one_half':
            {            
                ft = '[dcs_one_half]Your content here\r[/dcs_one_half]';
            }
            break;

            case 'dcs_one_half_last':
            {
                ft = '[dcs_one_half_last]Your content here\r[/dcs_one_half_last]';
            }
            break;            
            
            case 'dcs_one_third':
            {
                ft = '[dcs_one_third]Your content here\r[/dcs_one_third]';
            }
            break; 
            
            case 'dcs_one_third_last':
            {
                ft = '[dcs_one_third_last]Your content here\r[/dcs_one_third_last]';
            }
            break;   
            
            case 'dcs_two_third':
            {
                ft = '[dcs_two_third]Your content here\r[/dcs_two_third]';
            }
            break; 

            case 'dcs_two_third_last':
            {
                ft = '[dcs_two_third_last]Your content here\r[/dcs_two_third_last]';
            }
            break; 
            
            case 'dcs_one_fourth':
            {
                ft = '[dcs_one_fourth]Your content here\r[/dcs_one_fourth]';
            }
            break; 
            
            case 'dcs_one_fourth_last':
            {
                ft = '[dcs_one_fourth_last]Your content here\r[/dcs_one_fourth_last]';
            }
            break;  

            case 'dcs_three_fourth':
            {
                ft = '[dcs_three_fourth]Your content here\r[/dcs_three_fourth]';
            }
            break; 
            
            case 'dcs_three_fourth_last':
            {
                ft = '[dcs_three_fourth_last]Your content here\r[/dcs_three_fourth_last]';
            }
            break;
            
            case 'dcs_chain_gallery':
            {            
                var id = $panel.find('select[name=ngg_list]').find(':selected').val(); 
                if(id == CMS_NOT_SELECTED) { id = ''; } else { id = ' id="'+id+'" '; }
                
                var width = $panel.find('input[name=width]').val(); 
                width = ' w="'+width+'" ';
                var height = $panel.find('input[name=height]').val(); 
                height = ' h="'+height+'" ';

                var tw = $panel.find('input[name=tw]').val(); 
                tw = ' tw="'+tw+'" ';
                var th = $panel.find('input[name=th]').val(); 
                th = ' th="'+th+'" ';

                var set = $panel.find('input[name=set]').val(); 
                if(set != '' ) { set = ' set="'+set+'" '; }
                
                var number = $panel.find('input[name=number]').val(); 
                number = ' number="'+number+'" ';                
                
                var random = $panel.find('input[name=random]').is(':checked') ? true : false; 
                if(random) { random = ' random="true" '; } else { random = ''; }
                
                var show_desc = $panel.find('input[name=show_desc]').is(':checked') ? true : false;
                if(show_desc) { show_desc = ''; } else { show_desc = ' desc="false" '; }

                var show_title = $panel.find('input[name=show_title]').is(':checked') ? true : false;
                if(!show_title) { show_title = ''; } else { show_title = ' title="true" '; }

                var autoplay = $panel.find('input[name=autoplay]').is(':checked') ? true : false;
                if(autoplay) { autoplay = ''; } else { autoplay = ' autoplay="false" '; }
                
                var trans = $panel.find('select[name=trans]').find(':selected').val(); 
                if(trans == 'fade') { trans = ''; } else { trans = ' trans="'+trans+'" '; }
               
                var size = $panel.find('select[name=size]').find(':selected').val(); 
                if(size == 3) { size = ''; } else { size = ' size="'+size+'" '; }                    
                
                var pageid = $panel.find('select[name=page_list]').find(':selected').val(); 
                if(pageid == CMS_NOT_SELECTED) { pageid = ''; } else { pageid = ' pageid="'+pageid+'" '; }                
                
                var mleft = $panel.find('input[name=mleft]').val();                              
                var mright = $panel.find('input[name=mright]').val();
                var mtop = $panel.find('input[name=mtop]').val();                
                var mbottom = $panel.find('input[name=mbottom]').val();
                                             
                var margin = ''; 
                if(mleft == 0 && mright == 0 && mbottom == 15 && mtop == 0) 
                {  
                    margin = ''; 
                } 
                else
                {
                    margin = ' margin="'+mtop+'px '+mright+'px '+mbottom+'px '+mleft+'px" ';                   
                }                   
                
                var floating = $panel.find('input[name=float]:checked').val();
                if(floating != 'none') { floating = ' float="'+floating+'" '; } else { floating = ''; }                
                
                var bcolor = $panel.find('input[name=bcolor]').val();
                if(bcolor != '#3399CC') { bcolor = ' bcolor="'+bcolor+'" '; } else { bcolor = ''; }                
                
                ft = '[dcs_chain_gallery '+id+autoplay+width+height+tw+th+number+random+set+show_desc+show_title+trans+size+pageid+margin+floating+bcolor+' /]';    
            }
            break;                 

            case 'dcs_simple_gallery_ngg':
            {     
                var id = $panel.find('select[name=ngg_list]').find(':selected').val(); 
                if(id == CMS_NOT_SELECTED) { id = ''; } else { id = ' id="'+id+'" '; }
                
                var width = $panel.find('input[name=width]').val(); 
                width = ' w="'+width+'" ';
                var height = $panel.find('input[name=height]').val(); 
                height = ' h="'+height+'" ';               

                var ts = $panel.find('input[name=ts]').val(); 
                ts = ' ts="'+ts+'" ';

                var desc = $panel.find('input[name=desc]').val(); 
                if(desc != '' ) { desc = ' desc="'+desc+'" '; }
                
                var number = $panel.find('input[name=number]').val(); 
                number = ' number="'+number+'" ';                
                
                var random = $panel.find('input[name=random]').is(':checked') ? true : false; 
                if(!random) { random = ' random="false" '; } else { random = ''; }
                  
                var trans = $panel.find('select[name=trans]').find(':selected').val(); 
                if(trans == 'fade') { trans = ''; } else { trans = ' trans="'+trans+'" '; }                  
                    
                var floating = $panel.find('input[name=float]:checked').val();
                if(floating != 'none') { floating = ' float="'+floating+'" '; } else { floating = ''; }                      
                    
                var mleft = $panel.find('input[name=mleft]').val();                              
                var mright = $panel.find('input[name=mright]').val();
                var mtop = $panel.find('input[name=mtop]').val();                
                var mbottom = $panel.find('input[name=mbottom]').val();
                
                var margin = ''; 
                if(mleft == 'auto' && mright == 'auto' && mbottom == 15 && mtop == 0) 
                {  
                    margin = ''; 
                } 
                else
                {
                    if(mbottom != 'auto') { mbottom = mbottom+'px'; }
                    if(mleft != 'auto') { mleft = mleft+'px'; } 
                    if(mright != 'auto') { mright = mright+'px'; }
                    if(mtop != 'auto') { mtop = mtop+'px'; } 
                    margin = ' margin="'+mtop+' '+mright+' '+mbottom+' '+mleft+'" ';                   
                }
                
                var bcolor = $panel.find('input[name=bcolor]').val();
                if(bcolor != '#FFFFFF') { bcolor = ' bcolor="'+bcolor+'" '; } else { bcolor = ''; }                   
                
                var use_top = $panel.find('input[name=use_top]').is(':checked') ? true : false; 
                var tb = $panel.find('input[name=tb]').val();
                if(tb != 15 || use_top) { tb = ' tb="'+tb+'" ' } else { tb = ''; }
                var tt = $panel.find('input[name=tt]').val();
                tt = ' tt="'+tt+'" ';
                if(use_top) { tb = ''; } else { tt = ''; }
                 
                var sdesc = $panel.find('input[name=sdesc]').is(':checked') ? true : false;
                if(sdesc) { sdesc = ' sdesc="true" '; } else { sdesc = ''; }  
                var stitle = $panel.find('input[name=stitle]').is(':checked') ? true : false;
                if(stitle) { stitle = ' stitle="true" '; } else { stitle = ''; }  
                            
                ft = '[dcs_simple_gallery_ngg '+id+width+height+ts+tt+tb+trans+margin+sdesc+stitle+floating+random+number+bcolor+desc+' /]';  
            }
            break; 
            
            case 'dcs_simple_gallery_thumbs_ngg':
            {     
                var id = $panel.find('select[name=ngg_list]').find(':selected').val(); 
                if(id == CMS_NOT_SELECTED) { id = ''; } else { id = ' id="'+id+'" '; }
                
                var width = $panel.find('input[name=width]').val(); 
                width = ' w="'+width+'" ';
                var height = $panel.find('input[name=height]').val(); 
                height = ' h="'+height+'" ';               

                var ts = $panel.find('input[name=ts]').val(); 
                ts = ' ts="'+ts+'" ';

                var desc = $panel.find('input[name=desc]').val(); 
                if(desc != '' ) { desc = ' desc="'+desc+'" '; }
                
                var number = $panel.find('input[name=number]').val(); 
                number = ' number="'+number+'" ';                
                
                var random = $panel.find('input[name=random]').is(':checked') ? true : false; 
                if(!random) { random = ' random="false" '; } else { random = ''; }
                  
                var trans = $panel.find('select[name=trans]').find(':selected').val(); 
                if(trans == 'fade') { trans = ''; } else { trans = ' trans="'+trans+'" '; }                  
                    
                var floating = $panel.find('input[name=float]:checked').val();
                if(floating != 'none') { floating = ' float="'+floating+'" '; } else { floating = ''; }                      
                    
                var mleft = $panel.find('input[name=mleft]').val();                              
                var mright = $panel.find('input[name=mright]').val();
                var mtop = $panel.find('input[name=mtop]').val();                
                var mbottom = $panel.find('input[name=mbottom]').val();
                
                var margin = ''; 
                if(mleft == 'auto' && mright == 'auto' && mbottom == 15 && mtop == 0) 
                {  
                    margin = ''; 
                } 
                else
                {
                    if(mbottom != 'auto') { mbottom = mbottom+'px'; }
                    if(mleft != 'auto') { mleft = mleft+'px'; } 
                    if(mright != 'auto') { mright = mright+'px'; }
                    if(mtop != 'auto') { mtop = mtop+'px'; } 
                    margin = ' margin="'+mtop+' '+mright+' '+mbottom+' '+mleft+'" ';                   
                }
                
                var bcolor = $panel.find('input[name=bcolor]').val();
                if(bcolor != '#999999') { bcolor = ' bcolor="'+bcolor+'" '; } else { bcolor = ''; }                                   
                    
                var sdesc = $panel.find('input[name=sdesc]').is(':checked') ? true : false;
                if(sdesc) { sdesc = ' sdesc="true" '; } else { sdesc = ''; }  
                var stitle = $panel.find('input[name=stitle]').is(':checked') ? true : false;
                if(stitle) { stitle = ' stitle="true" '; } else { stitle = ''; }                      
                            
                ft = '[dcs_simple_gallery_thumbs_ngg '+id+width+height+ts+trans+margin+sdesc+stitle+floating+random+number+bcolor+desc+' /]';  
            }
            break;             
            
            case 'dcs_postbox':
            {   
              
                var type = $panel.find('select[name=type]').find(':selected').val(); 
                type = ' type="'+type+'" '; 
                
                var id = $panel.find('input[name=id]').val();
                id = ' id="'+id+'" ';  
                
                var desc = $panel.find('input[name=desc]').val();
                if(desc != '') { desc = ' desc="'+desc+'" '; } else { desc = ''; } 
                
                var img = $panel.find('input[name=img]').val();
                if(img != '') { img = ' img="'+img+'" '; } else { img = ''; }                 
                
                var lightbox = $panel.find('input[name=lightbox]').is(':checked') ? true : false; 
                if(lightbox) { lightbox = ' lightbox="true" '; } else { lightbox = ''; } 

                var words = $panel.find('input[name=words]').val();
                words = ' words="'+words+'" ';
                if(desc != '') { words = ''; }
                
                var mleft = $panel.find('input[name=mleft]').val();                              
                var mright = $panel.find('input[name=mright]').val();
                var mtop = $panel.find('input[name=mtop]').val();                
                var mbottom = $panel.find('input[name=mbottom]').val();                
                
                var margin = ''; 
                if(mleft == 10 && mright == 0 && mbottom == 10 && mtop == 0) 
                {  
                    margin = ''; 
                } 
                else
                {
                    if(mbottom != 'auto') { mbottom = mbottom+'px'; }
                    if(mleft != 'auto') { mleft = mleft+'px'; } 
                    if(mright != 'auto') { mright = mright+'px'; }
                    if(mtop != 'auto') { mtop = mtop+'px'; } 
                    margin = ' margin="'+mtop+' '+mright+' '+mbottom+' '+mleft+'" ';                   
                }                
                
                var floating = $panel.find('input[name=float]:checked').val();
                if(floating != 'right') { floating = ' float="'+floating+'" '; } else { floating = ''; }                         
                
                ft = '[dcs_postbox '+id+type+lightbox+desc+words+margin+floating+img+' /]';  
            } 
            break           
              
            case 'dcs_pagebox':
            {   
              
                var type = $panel.find('select[name=type]').find(':selected').val(); 
                type = ' type="'+type+'" '; 
                
                var id = $panel.find('input[name=id]').val();
                id = ' id="'+id+'" ';  
                
                var desc = $panel.find('input[name=desc]').val();
                if(desc != '') { desc = ' desc="'+desc+'" '; } else { desc = ''; } 
                
                var img = $panel.find('input[name=img]').val();
                if(img != '') { img = ' img="'+img+'" '; } else { img = ''; }                 
                
                var lightbox = $panel.find('input[name=lightbox]').is(':checked') ? true : false; 
                if(lightbox) { lightbox = ' lightbox="true" '; } else { lightbox = ''; } 

                var words = $panel.find('input[name=words]').val();
                words = ' words="'+words+'" ';
                if(desc != '') { words = ''; }
                
                var mleft = $panel.find('input[name=mleft]').val();                              
                var mright = $panel.find('input[name=mright]').val();
                var mtop = $panel.find('input[name=mtop]').val();                
                var mbottom = $panel.find('input[name=mbottom]').val();                
                
                var margin = ''; 
                if(mleft == 10 && mright == 0 && mbottom == 10 && mtop == 0) 
                {  
                    margin = ''; 
                } 
                else
                {
                    if(mbottom != 'auto') { mbottom = mbottom+'px'; }
                    if(mleft != 'auto') { mleft = mleft+'px'; } 
                    if(mright != 'auto') { mright = mright+'px'; }
                    if(mtop != 'auto') { mtop = mtop+'px'; } 
                    margin = ' margin="'+mtop+' '+mright+' '+mbottom+' '+mleft+'" ';                   
                }                
                
                var floating = $panel.find('input[name=float]:checked').val();
                if(floating != 'right') { floating = ' float="'+floating+'" '; } else { floating = ''; }                         
                
                ft = '[dcs_pagebox '+id+type+lightbox+desc+words+margin+floating+img+' /]';  
            } 
            break                
            
            case 'dcs_ngg':
            {                 
                var id = $panel.find('select[name=ngg_list]').find(':selected').val(); 
                if(id == CMS_NOT_SELECTED) { id = ''; } else { id = ' id="'+id+'" '; }  

                var width = $panel.find('input[name=width]').val(); 
                width = ' w="'+width+'" ';
                var height = $panel.find('input[name=height]').val(); 
                height = ' h="'+height+'" ';                    
                
                var number = $panel.find('input[name=number]').val(); 
                number = ' number="'+number+'" ';                  
                
                var random = $panel.find('input[name=random]').is(':checked') ? true : false;
                if(random) { random = ' random="true" '; } else { random = ''; }                  
                
                var tips = $panel.find('input[name=tips]').is(':checked') ? true : false;
                if(tips) { tips = ' tips="true" '; } else { tips = ''; }                   
            
                var bshow = $panel.find('input[name=bshow]').is(':checked') ? true : false;
                if(!bshow) { bshow = ' bshow="false" '; } else { bshow = ''; }             
                
                var mleft = $panel.find('input[name=mleft]').val();                              
                var mright = $panel.find('input[name=mright]').val();
                var mtop = $panel.find('input[name=mtop]').val();                
                var mbottom = $panel.find('input[name=mbottom]').val();
                
                var margin = ''; 
                if(mleft == 0 && mright == 15 && mbottom == 15 && mtop == 0) 
                {  
                    margin = ''; 
                } 
                else
                {
                    if(mbottom != 'auto') { mbottom = mbottom+'px'; }
                    if(mleft != 'auto') { mleft = mleft+'px'; } 
                    if(mright != 'auto') { mright = mright+'px'; }
                    if(mtop != 'auto') { mtop = mtop+'px'; } 
                    margin = ' margin="'+mtop+' '+mright+' '+mbottom+' '+mleft+'" ';                   
                }                
                
                var padding = $panel.find('input[name=padding]').val(); 
                if(padding > 0) { padding = ' padding="'+padding+'" '; } else { padding = ''; }                                 
                
                var group = $panel.find('input[name=group]').val(); 
                if(group != '') { group = ' group="'+group+'" '; } else { group = ''; }                                  
                
                var bcolor = $panel.find('input[name=bcolor]').val();
                if(bcolor != '#CCCCCC') { bcolor = ' bcolor="'+bcolor+'" '; } else { bcolor = ''; }                   
                
                var bwidth = $panel.find('input[name=bwidth]').val(); 
                if(bwidth > 1) { bwidth = ' bwidth="'+bwidth+'" '; } else { bwidth = ''; }                 
                if(bshow != '') { bwidth = ''; bcolor = ''; }  
                
                var rounded = $panel.find('select[name=rounded]').find(':selected').val();
                if(rounded > 0) { rounded = ' rounded="'+rounded+'" '; } else { rounded = ''; }                               
                
                var bgcolor = $panel.find('input[name=bgcolor]').val();
                if(bgcolor != '#FFFFFF') { bgcolor = ' bgcolor="'+bgcolor+'" '; } else { bgcolor = ''; }                   
              
                var con_mbottom = $panel.find('input[name=con_mbottom]').val(); 
                if(con_mbottom > 0) { con_mbottom = ' mbottom="'+con_mbottom+'" '; } else { con_mbottom = ''; }                    
                
                ft = '[dcs_ngg '+id+width+height+number+random+tips+bshow+margin+padding+group+bcolor+bwidth+rounded+bgcolor+con_mbottom+' /]';  
            } 
            break
            
            case 'dcs_ngg_single':
            {
                var list = $panel.find('input[name=list]').val(); 
                list = ' list="'+list+'" ';

                var width = $panel.find('input[name=width]').val(); 
                width = ' w="'+width+'" ';
                var height = $panel.find('input[name=height]').val(); 
                height = ' h="'+height+'" ';                                                   
                
                var exclude = $panel.find('input[name=exclude]').is(':checked') ? true : false;
                if(exclude) { exclude = ' exclude="true" '; } else { exclude = ''; }                  
                
                var tips = $panel.find('input[name=tips]').is(':checked') ? true : false;
                if(tips) { tips = ' tips="true" '; } else { tips = ''; }                   
            
                var bshow = $panel.find('input[name=bshow]').is(':checked') ? true : false;
                if(!bshow) { bshow = ' bshow="false" '; } else { bshow = ''; }             
                
                var mleft = $panel.find('input[name=mleft]').val();                              
                var mright = $panel.find('input[name=mright]').val();
                var mtop = $panel.find('input[name=mtop]').val();                
                var mbottom = $panel.find('input[name=mbottom]').val();
                
                var margin = ''; 
                if(mleft == 0 && mright == 15 && mbottom == 15 && mtop == 0) 
                {  
                    margin = ''; 
                } 
                else
                {
                    if(mbottom != 'auto') { mbottom = mbottom+'px'; }
                    if(mleft != 'auto') { mleft = mleft+'px'; } 
                    if(mright != 'auto') { mright = mright+'px'; }
                    if(mtop != 'auto') { mtop = mtop+'px'; } 
                    margin = ' margin="'+mtop+' '+mright+' '+mbottom+' '+mleft+'" ';                   
                }                
                
                var padding = $panel.find('input[name=padding]').val(); 
                if(padding > 0) { padding = ' padding="'+padding+'" '; } else { padding = ''; }                                 
                
                var group = $panel.find('input[name=group]').val(); 
                if(group != '') { group = ' group="'+group+'" '; } else { group = ''; }                                  
                
                var bcolor = $panel.find('input[name=bcolor]').val();
                if(bcolor != '#CCCCCC') { bcolor = ' bcolor="'+bcolor+'" '; } else { bcolor = ''; }                   
                
                var bwidth = $panel.find('input[name=bwidth]').val(); 
                if(bwidth > 1) { bwidth = ' bwidth="'+bwidth+'" '; } else { bwidth = ''; }                 
                if(bshow != '') { bwidth = ''; bcolor = ''; }  
                
                var rounded = $panel.find('select[name=rounded]').find(':selected').val();
                if(rounded > 0) { rounded = ' rounded="'+rounded+'" '; } else { rounded = ''; }                               
                
                var bgcolor = $panel.find('input[name=bgcolor]').val();
                if(bgcolor != '#FFFFFF') { bgcolor = ' bgcolor="'+bgcolor+'" '; } else { bgcolor = ''; }                   
              
                var con_mbottom = $panel.find('input[name=con_mbottom]').val(); 
                if(con_mbottom > 0) { con_mbottom = ' mbottom="'+con_mbottom+'" '; } else { con_mbottom = ''; }                
                
                ft = '[dcs_ngg_single '+list+width+height+exclude+tips+bshow+margin+padding+group+bcolor+bwidth+rounded+bgcolor+con_mbottom+' /]';
            }
            break;               
            
            case 'dcs_ngg_last':
            {
                var id = $panel.find('select[name=ngg_list]').find(':selected').val(); 
                if(id == CMS_NOT_SELECTED) { id = ''; } else { id = ' id="'+id+'" '; }  

                var width = $panel.find('input[name=width]').val(); 
                width = ' w="'+width+'" ';
                var height = $panel.find('input[name=height]').val(); 
                height = ' h="'+height+'" ';                    
                
                var number = $panel.find('input[name=number]').val(); 
                number = ' number="'+number+'" ';                  
                
                var page = $panel.find('input[name=page]').val(); 
                if(page > 0) { page = ' page="'+page+'" '; } else { page = ''; }                    
                
                var exclude = $panel.find('input[name=exclude]').is(':checked') ? true : false;
                if(exclude) { exclude = ' exclude="true" '; } else { exclude = ''; }                     
                
                var tips = $panel.find('input[name=tips]').is(':checked') ? true : false;
                if(tips) { tips = ' tips="true" '; } else { tips = ''; }                   
            
                var bshow = $panel.find('input[name=bshow]').is(':checked') ? true : false;
                if(!bshow) { bshow = ' bshow="false" '; } else { bshow = ''; }             
                
                var mleft = $panel.find('input[name=mleft]').val();                              
                var mright = $panel.find('input[name=mright]').val();
                var mtop = $panel.find('input[name=mtop]').val();                
                var mbottom = $panel.find('input[name=mbottom]').val();
                
                var margin = ''; 
                if(mleft == 0 && mright == 15 && mbottom == 15 && mtop == 0) 
                {  
                    margin = ''; 
                } 
                else
                {
                    if(mbottom != 'auto') { mbottom = mbottom+'px'; }
                    if(mleft != 'auto') { mleft = mleft+'px'; } 
                    if(mright != 'auto') { mright = mright+'px'; }
                    if(mtop != 'auto') { mtop = mtop+'px'; } 
                    margin = ' margin="'+mtop+' '+mright+' '+mbottom+' '+mleft+'" ';                   
                }                
                
                var padding = $panel.find('input[name=padding]').val(); 
                if(padding > 0) { padding = ' padding="'+padding+'" '; } else { padding = ''; }                                 
                
                var group = $panel.find('input[name=group]').val(); 
                if(group != '') { group = ' group="'+group+'" '; } else { group = ''; }                                  
                
                var bcolor = $panel.find('input[name=bcolor]').val();
                if(bcolor != '#CCCCCC') { bcolor = ' bcolor="'+bcolor+'" '; } else { bcolor = ''; }                   
                
                var bwidth = $panel.find('input[name=bwidth]').val(); 
                if(bwidth > 1) { bwidth = ' bwidth="'+bwidth+'" '; } else { bwidth = ''; }                 
                if(bshow != '') { bwidth = ''; bcolor = ''; }  
                
                var rounded = $panel.find('select[name=rounded]').find(':selected').val();
                if(rounded > 0) { rounded = ' rounded="'+rounded+'" '; } else { rounded = ''; }                               
                
                var bgcolor = $panel.find('input[name=bgcolor]').val();
                if(bgcolor != '#FFFFFF') { bgcolor = ' bgcolor="'+bgcolor+'" '; } else { bgcolor = ''; }                   
              
                var con_mbottom = $panel.find('input[name=con_mbottom]').val(); 
                if(con_mbottom > 0) { con_mbottom = ' mbottom="'+con_mbottom+'" '; } else { con_mbottom = ''; }                    
                
                ft = '[dcs_ngg_last '+id+width+height+number+exclude+tips+bshow+margin+padding+group+bcolor+bwidth+rounded+bgcolor+con_mbottom+page+' /]';                  
            }
            break;            
           
            case 'dcs_ngg_random':
            {
                var id = $panel.find('select[name=ngg_list]').find(':selected').val(); 
                if(id == CMS_NOT_SELECTED) { id = ''; } else { id = ' id="'+id+'" '; }  

                var width = $panel.find('input[name=width]').val(); 
                width = ' w="'+width+'" ';
                var height = $panel.find('input[name=height]').val(); 
                height = ' h="'+height+'" ';                    
                
                var number = $panel.find('input[name=number]').val(); 
                number = ' number="'+number+'" ';                                                                                
                
                var tips = $panel.find('input[name=tips]').is(':checked') ? true : false;
                if(tips) { tips = ' tips="true" '; } else { tips = ''; }                   
            
                var bshow = $panel.find('input[name=bshow]').is(':checked') ? true : false;
                if(!bshow) { bshow = ' bshow="false" '; } else { bshow = ''; }             
                
                var mleft = $panel.find('input[name=mleft]').val();                              
                var mright = $panel.find('input[name=mright]').val();
                var mtop = $panel.find('input[name=mtop]').val();                
                var mbottom = $panel.find('input[name=mbottom]').val();
                
                var margin = ''; 
                if(mleft == 0 && mright == 15 && mbottom == 15 && mtop == 0) 
                {  
                    margin = ''; 
                } 
                else
                {
                    if(mbottom != 'auto') { mbottom = mbottom+'px'; }
                    if(mleft != 'auto') { mleft = mleft+'px'; } 
                    if(mright != 'auto') { mright = mright+'px'; }
                    if(mtop != 'auto') { mtop = mtop+'px'; } 
                    margin = ' margin="'+mtop+' '+mright+' '+mbottom+' '+mleft+'" ';                   
                }                
                
                var padding = $panel.find('input[name=padding]').val(); 
                if(padding > 0) { padding = ' padding="'+padding+'" '; } else { padding = ''; }                                 
                
                var group = $panel.find('input[name=group]').val(); 
                if(group != '') { group = ' group="'+group+'" '; } else { group = ''; }                                  
                
                var bcolor = $panel.find('input[name=bcolor]').val();
                if(bcolor != '#CCCCCC') { bcolor = ' bcolor="'+bcolor+'" '; } else { bcolor = ''; }                   
                
                var bwidth = $panel.find('input[name=bwidth]').val(); 
                if(bwidth > 1) { bwidth = ' bwidth="'+bwidth+'" '; } else { bwidth = ''; }                 
                if(bshow != '') { bwidth = ''; bcolor = ''; }  
                
                var rounded = $panel.find('select[name=rounded]').find(':selected').val();
                if(rounded > 0) { rounded = ' rounded="'+rounded+'" '; } else { rounded = ''; }                               
                
                var bgcolor = $panel.find('input[name=bgcolor]').val();
                if(bgcolor != '#FFFFFF') { bgcolor = ' bgcolor="'+bgcolor+'" '; } else { bgcolor = ''; }                   
              
                var con_mbottom = $panel.find('input[name=con_mbottom]').val(); 
                if(con_mbottom > 0) { con_mbottom = ' mbottom="'+con_mbottom+'" '; } else { con_mbottom = ''; }                       
                
                ft = '[dcs_ngg_random '+id+width+height+number+tips+bshow+margin+padding+group+bcolor+bwidth+rounded+bgcolor+con_mbottom+' /]';     
            }   
            break;        
                     
        } // switch
        
             
        // insert to editor
        if(q('#wp-content-wrap').hasClass('html-active'))
        {
            edDCThemeInsertText(window.edCanvas, ft);    
        } else                               
        if(window.tinyMCE) 
        {
            window.tinyMCE.execInstanceCommand('content', 'mceInsertContent', false, ft);
        }                                         
        
        TINY.box.hide();    
    });      
}     
    
function dcAdminBoxImageSetup()
{
    var q = jQuery.noConflict(); 

    var dc_theme_url = q('meta[name=cms_theme_url]').attr('content');
    var dc_url = q('meta[name=cms_url]').attr('content');    
    var $con = q('.cms-box .cms-box-content');     
     
    q('.upload_image_button').click(function() {
           
     dc_formfield = q(this).attr('name');  
     tb_show('', 'media-upload.php?type=image&amp;TB_iframe=true'); 
     return false;

    });      
    
    if(window.original_send_to_editor == null)
    {
        window.original_send_to_editor = window.send_to_editor;
        
        window.send_to_editor = function(html) {

            if(dc_formfield)
            {
               // imgurl = q('img',html).attr('src');
               imgurl = q(html).attr('href');  

                q('#'+dc_formfield).val(imgurl);
                tb_remove();
                dc_formfield = null;
            } else
            {
                window.original_send_to_editor(html);
            }
        }
    }    
   
    q.ajax({
   type: "POST",
   url: dc_url+'/wp-admin/admin-ajax.php',
   data: "action=dc_page_list",
   success: function(str){
     $con.find('select[name=page_list]').append(str); 
   }
 });       
     
    q('.cms-box .cms-box-content .insert-btn').click(function() 
    {    
        var dc_theme_url = q('meta[name=cms_theme_url]').attr('content');         
        var $con = q('.cms-box .cms-box-content');
        
        var image_url = $con.find('input[name=image_url]').val(); 
        var image_w = $con.find('input[name=image_w]').val(); 
        var image_h = $con.find('input[name=image_h]').val();
        var image_desc = $con.find('input[name=image_desc]').val();

        var image_async = $con.find('input[name=image_async]').is(':checked') ? true : false; 
        var image_tt = $con.find('input[name=image_tt]').is(':checked') ? true : false;
        var image_lb = $con.find('input[name=image_lb]').is(':checked') ? true : false;
        var as_shortcode = $con.find('input[name=as_shortcode]').is(':checked') ? true : false; 
        
        var imglink = $con.find('input[name=image_as_link]').is(':checked') ? true : false;  
        var desclink = $con.find('input[name=desc_as_link]').is(':checked') ? true : false;  
        var remove_border = $con.find('input[name=remove_border]').is(':checked') ? true : false;  
        var remove_padding = $con.find('input[name=remove_padding]').is(':checked') ? true : false; 
        var link = $con.find('input[name=link]').val(); 
        var viewmore = $con.find('input[name=viewmore]').val();
        
        
        var desc_align = $con.find('input[name=desc_align]:checked').val();
        var img_align = $con.find('input[name=img_align]:checked').val();            
        var target = $con.find('input[name=target]:checked').val();
        
        var mleft = $con.find('input[name=mleft]').val();
        var mright = $con.find('input[name=mright]').val();
        var mtop = $con.find('input[name=mtop]').val();     
        var mbottom = $con.find('input[name=mbottom]').val();
        
        
        var group = $con.find('input[name=group]').val(); 
                      
        var ft = '';        
        
        var container_w = image_w+'px';
        if(img_align == 'center') { container_w = 'auto'; }        
        var image_url_orig = image_url;
        
        if(as_shortcode)
        {
            if(image_desc != '') { image_desc = ' desc="'+image_desc+'" '; } else { image_desc = ''; }
            if(desc_align != 'left') { desc_align = ' descalign="'+desc_align+'" '; } else { desc_align = ''; }
            if(image_async) { image_async = ''; } else { image_async = ' async="false" '; }
            
            if(target != '_self') { target = ' target="'+target+'" '; } else { target = ''; }
            
            if(image_tt) { image_tt = ''; } else { image_tt = ' tt="false" '; } 
            if(image_lb) { image_lb = ''; } else { image_lb = ' lb="false" '; } 
            
            if(group != '') { group = ' group="'+group+'" '; } else { group = ''; }
            
            if(mleft != 0) { mleft = ' mleft="'+mleft+'" '; } else { mleft = ''; }
            if(mright != 0) { mright = ' mright="'+mright+'" '; } else { mright = ''; }
            if(mtop != 0) { mtop = ' mtop="'+mtop+'" '; } else { mtop = ''; }            
            if(mbottom != 0) { mbottom = ' mbottom="'+mbottom+'" '; } else { mbottom = ''; }
            
            if(desclink) { viewmore = ' viewmore="'+viewmore+'" '; } else { viewmore = ''; } 
            if(link != '') { link = ' link="'+link+'" '; }  
            if(desclink) { desclink = ' desclink="true" '; } else { desclink = ''; } 
            if(imglink) { imglink = ' imglink="true" '; } else { imglink = ''; } 
            
            if(remove_border) { remove_border = ' borderremove="true" '; } else { remove_border = ' borderremove="false" '; }
            if(remove_padding) { remove_padding = ' paddingremove="true" '; } else { remove_padding = ' paddingremove="false" '; } 
            
            ft = '[dcs_image align="'+img_align+'" src="'+image_url+'" w="'+image_w+'" h="'+image_h+'" '+mleft+mtop+mright+mbottom+image_desc+desc_align+imglink+desclink+image_async+target+image_tt+image_lb+group+link+viewmore+remove_border+remove_padding+' /]';    
        } else
        {
            if(group != '') { group = '['+group+']'; } 
   
            var remove_style = '';
            var remove_triger_style = '';
            if(remove_border) { remove_style += 'border:none;'; }
            if(remove_padding) { remove_style += 'padding:0px;'; remove_triger_style = 'left:0px;top:0px;';  }
            
            if(image_async)
            {

                 
                ft += '<div style="width:'+container_w+';margin:'+mtop+'px '+mright+'px '+mbottom+'px '+mleft+'px;" class="theme-image-'+img_align+'">';
                
                if(image_tt)
                {
                    image_url = dc_theme_url+'/thumb.php?src='+image_url+'&w='+image_w+'&h='+image_h;
                }
                
                ft += '<div style="width:'+image_w+'px;height:'+image_h+'px;'+remove_style+'" class="img-wrapper">';
                    ft += '<a style="width:'+image_w+'px;height:'+image_h+'px;" class="loader async-img-s" rel="'+image_url+'"></a>';
                    if(imglink)
                    { 
                        ft += '<a style="width:'+image_w+'px;height:'+image_h+'px;'+remove_triger_style+'" href="'+link+'" target="'+target+'" class="triger-link" ></a>';     
                    } else                    
                    if(image_lb)
                    {                      
                        ft += '<a style="width:'+image_w+'px;height:'+image_h+'px;'+remove_triger_style+'" href="'+image_url_orig+'" class="triger" rel="lightbox'+group+'"></a>';  
                    }
                ft += '</div>';
                
                if(image_desc != '' || desclink)
                {
                    ft += '<div style="text-align:'+desc_align+';width:'+image_w+'px;" class="desc">';
                        ft += image_desc;
                        if(desclink)
                        {
                            ft += ' <a href="'+link+'" target="'+target+'">'+viewmore+'</a>';
                        }
                    ft += '</div>';
                }
                ft += '</div>';
            } else
            {
                ft += '<div style="width:'+container_w+';margin:'+mtop+'px '+mright+'px '+mbottom+'px '+mleft+'px;" class="theme-image-'+img_align+'">';
                
                if(image_tt)
                {
                    image_url = dc_theme_url+'/thumb.php?src='+image_url+'&w='+image_w+'&h='+image_h;
                }
                
                ft += '<div style="width:'+image_w+'px;height:'+image_h+'px;'+remove_style+'" class="img-wrapper">';
                    ft += '<img style="width:'+image_w+'px;height:'+image_h+'px;" src="'+image_url+'" />';
                    if(imglink)
                    { 
                        ft += '<a style="width:'+image_w+'px;height:'+image_h+'px;'+remove_triger_style+'" href="'+link+'" target="'+target+'" class="triger-link" ></a>';     
                    } else                       
                    if(image_lb)
                    {
                        ft += '<a style="width:'+image_w+'px;height:'+image_h+'px;'+remove_triger_style+'" href="'+image_url_orig+'" class="triger" rel="lightbox'+group+'"></a>';    
                    }                
                ft += '</div>'; 
                
                if(image_desc != '' || desclink)
                {
                    ft += '<div style="text-align:'+desc_align+';width:'+image_w+'px;" class="desc">';
                        ft += image_desc;
                        if(desclink)
                        {
                            ft += ' <a href="'+link+'" target="'+target+'">'+viewmore+'</a>';
                        }
                    ft += '</div>';
                }
                ft += '</div>';            
            }
        
        }
        
        // insert to editor
        if(q('#wp-content-wrap').hasClass('html-active'))
        {
            edDCThemeInsertText(window.edCanvas, ft);    
        } else                               
        if(window.tinyMCE) 
        {
            window.tinyMCE.execInstanceCommand('content', 'mceInsertContent', false, ft);
        }                      
        
        TINY.box.hide();    
    });      
}  

function dcAdminBoxImageClose()
{
    var q = jQuery.noConflict(); 
   
}    

function dcAdminBoxShortcodesClose()
{
    var q = jQuery.noConflict(); 
   
} 
 
function edDCThemeInsertText(d, instext)
{
    if(document.selection)
    {
        d.focus();
        var e=document.selection.createRange();
        if(e.text.length > 0)
        {
            e.text=instext+e.text;
        } else
        {   
            e.text=instext; 
        }
        
        d.focus()
    } else
    { 
        if(d.selectionStart||d.selectionStart=="0")
        {
            var b=d.selectionStart,a=d.selectionEnd,g=a,f=d.scrollTop;
            if(b!=a)
            {   d.value=d.value.substring(0,b)+instext+d.value.substring(b,a)+d.value.substring(a,d.value.length);
                g+=instext.length;
            }   
            else
            {
                d.value=d.value.substring(0,b)+instext+d.value.substring(a,d.value.length);
                g+=instext.length;
            }
            d.focus();
            d.selectionStart=g;
            d.selectionEnd=g;
            d.scrollTop=f
        } else
        { 
            d.value=instext;
            d.focus()
        }
    }
} 
            
function edDCThemeInsertTag(d,c)
{       
    var tag = c;
    
    if(document.selection)
    {
        d.focus();
        var e=document.selection.createRange();
        if(e.text.length > 0)
        {
            e.text=tag.tagStart+e.text+tag.tagEnd;
        } else
        {   
            e.text=tag.tagStart+tag.tagEnd; 
        }
        
        d.focus()
    } else
    { 
        if(d.selectionStart||d.selectionStart=="0")
        {
            var b=d.selectionStart,a=d.selectionEnd,g=a,f=d.scrollTop;
            if(b!=a)
            {   d.value=d.value.substring(0,b)+tag.tagStart+d.value.substring(b,a)+tag.tagEnd+d.value.substring(a,d.value.length);
                g+=tag.tagStart.length+tag.tagEnd.length;
            }   
            else
            {
                d.value=d.value.substring(0,b)+tag.tagStart+tag.tagEnd+d.value.substring(a,d.value.length);
                g+=tag.tagStart.length+tag.tagEnd.length;
            }
            d.focus();
            d.selectionStart=g;
            d.selectionEnd=g;
            d.scrollTop=f
        } else
        { 
            d.value=tag.tagStart+tag.tagEnd;
            d.focus()
        }
    }
}    
