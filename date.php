<?php
/**********************************************************************
* MODERN ELEGANCE WORDPRESS THEME  
* (Ideal For Business And Personal Use: Portfolio or Blog)   
* 
* File name:   
*      date.php
* Brief:       
*      Theme date page code
* Author:      
*      DigitalCavalry
* Author URI:
*      http://themeforest.net/user/DigitalCavalry
* Contact:
*      digitalcavalry@gmail.com   
***********************************************************************/ 

    get_header();     
                                                      
    $year = (get_query_var('year')) ? get_query_var('year') : '';
    $month = (get_query_var('monthnum')) ? get_query_var('monthnum') : '';
    $m = get_query_var('m');
    if($month == '')
    {
        $month = (int)substr($m, 4, 2);
        $year = (int)substr($m, 0, 4);
    }
    $timestamp = mktime(0, 0, 0, $month, 1, 2010);
    $datetime = date('Y-m-d H:i:s', $timestamp);    
    $monthname = mysql2date('F', $datetime);   
                                 
?>

    <div id="content">
    
        <?php
            GetDCCPInterface()->getIGeneral()->includeSidebar(GetDCCPInterface()->getIGeneral()->getSidebarForBlogCat(), CMS_SIDEBAR_RIGHT); 
        
            echo '<div class="page-width-left">';      
            dcf_naviTree($post->ID, 0, 'Archive for '.$monthname.' '.$year);            
            
            // here you can ucomment below line and add some description for archive page 
            // echo '<div class="page-description">'.Archive page description.'</div>';           
                          
            echo '<h1>'.'Archive for '.$monthname.' '.$year.'</h1>';
 
            $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;            
            $args=array('paged'=>$paged,'year'=>$year,'monthnum'=>$month);           
            
            query_posts($args);                       
            $max_page = $wp_query->max_num_pages;
                             
            GetDCCPInterface()->getIRenderer()->loopBlogPosts();                        
            GetDCCPInterface()->getIGeneral()->renderSitePagination($paged, $max_page);             
                               
        ?>                    
        </div> <!-- page-width --> 
        <div class="clear-both"></div>
    </div> <!-- content -->
    
<?php    
    get_footer();
?>



