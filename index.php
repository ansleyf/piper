<?php
/**********************************************************************
* MODERN ELEGANCE WORDPRESS THEME 
* (Ideal For Business And Personal Use: Portfolio or Blog)   
* 
* File name:   
*      index.php
* Brief:       
*      Theme index page code
* Author:      
*      DigitalCavalry
* Author URI:
*      http://themeforest.net/user/DigitalCavalry
* Contact:
*      digitalcavalry@gmail.com   
***********************************************************************/ 

    get_header(); 
    $dccp = GetDCCPInterface();     
?>
<ul class="bxslider">
	<li>
		<a href="/memphis-orthodontics/" class="slide">
			<img src="/wp-content/themes/ModernElegance/custom/css/slide1.jpg" />	
			<p>Two locations, East Memphis &amp; Midtown</p>
		</a>
	</li>
	<li>
		<a href="/orthodontic-therapy/" class="slide">
			<img src="/wp-content/themes/ModernElegance/custom/css/slide2.jpg" />
			<p>New techniques, high quality care</p>
		</a>
	</li>
	<li>
		<a href="/memphis-orthodontics/" class="slide">
			<img src="/wp-content/themes/ModernElegance/custom/css/slide3.jpg" />
			<p>Dr. Piper's clinical team</p>
		</a>
	</li>
	<li>
		<a href="/tmj-specialist-memphis/" class="slide">
			<img src="/wp-content/themes/ModernElegance/custom/css/slide4.jpg" />
			<p>Where the bottom line is a better smile</p>
		</a>
	</li>
	<li>
		<a href="/orthodontists-memphis-tn/" class="slide">
			<img src="/wp-content/themes/ModernElegance/custom/css/slide5.jpg" />
			<p>Your appointment time in real time</p>
		</a>
	</li>
</ul>
<script>
	$('.bxslider').bxSlider({
		mode: 'fade',
	});
</script>
	<div id="boxes">	
		<div class="homepage-box first">			
			<a href="/meet-piper-orthodontics-team/" id="box3">
				<p>Meet Dr. Piper's<br />Team</p>
				<img src="/wp-content/themes/ModernElegance/custom/css/team.png" />
			</a>		
		</div>	
		<div class="homepage-box">			
			<a href="/memphis-orthodontics/" id="box1">			
				<p>Visit Us in<br />East Memphis<br/>and Midtown</p>				
				<img src="/wp-content/themes/ModernElegance/custom/css/building.png" />			
			</a>		
		</div>		
		<div class="homepage-box">			
			<a href="/category/tmj-specialist-memphis/" id="box2">
				<p>Meet Our<br />Families</p>
				<img src="/wp-content/themes/ModernElegance/custom/css/blog.png" />
			</a>		
		</div>			
		<div class="homepage-box last hidden-small">
			<div>
				<p>Hear Our Stories</p>
				<iframe id="box4" width="218" src="http://www.youtube.com/embed/z8tSGvWnKA8?wmode=transparent&rel=0;showinfo=0;controls=0;color1=#ffffff" frameborder="0" allowfullscreen></iframe>		

			</div>
		</div>		
		<div class="clear"></div>	
	</div>
	
	<div id="homepage-content">
		<h1 class="nocufon">Welcome</h1>
		
		<p>Dr. Fred Piper has been in practice as a Memphis orthodontist since 1986. With locations in Midtown and East Memphis, his offices are within easy reach of dozens of schools. Nearby suburbs are East Memphis, Germantown, Cordova, Eads, and Collierville.</p> 


<p>With over 25 years of experience in orthodontics, Dr. Piper can correct conditions that interfere with the function of the teeth. Those methods might include <a href="/braces-for-adults-memphis/">traditional braces</a>, <a href="/braces-for-adults-memphis/#retainers">retainers</a>, <a href="/braces-for-adults-memphis/#clear-aligners">clear aligners</a>, and <a href="/tmj-therapy-memphis/">TMJ therapy</a>.</p>

<p>As a Memphis orthodontist, Dr. Piper understands the importance of the after-school appointment and makes every effort to accommodate families on tight schedules. Braces for adults are well within Dr. Piper’s range of specialty too.</p>
 
<p>There is no substitute for the building blocks of traditional orthodontics: state-of-the-art wires, the child's own growth, and the doctor's talent.</p>

	</div>
	<div id="testimonial" class="hidden-small">
		<img src="/wp-content/themes/ModernElegance/custom/css/testimonial.jpg" />
		<p>
			<img class="quote" src="/wp-content/themes/ModernElegance/custom/css/begin-quote.png" />
			
			Since having braces, I have so much more confidence in my smile whenever I'm on stage or at an audition.

			<img class="end quote" src="/wp-content/themes/ModernElegance/custom/css/end-quote.png" />
		</p><br />
		<div class="person">Gillian, 12, actress &amp; student</div>
	</div>
	<div id="testimonial" class="two visible-small">
		<img src="/wp-content/themes/ModernElegance/custom/css/testimonial.jpg" />
		<p>
			<img class="quote" src="/wp-content/themes/ModernElegance/custom/css/begin-quote.png" />
			
			Since having braces, I have so much more confidence in my smile whenever I'm on stage or at an audition.

			<img class="end quote" src="/wp-content/themes/ModernElegance/custom/css/end-quote.png" />
		</p><br />
		<div class="person">Gillian, 12, actress &amp; student</div>
	</div>
	<div class="clear"></div>
<?php    
    get_footer();
?>