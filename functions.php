<?php
session_start();
    
/**********************************************************************
* MODERN ELEGANCE WORDPRESS THEME 
* (Ideal For Business And Personal Use: Portfolio or Blog)      
* 
* File name:   
*      functions.php
* Brief:       
*      Theme functions file
* Author:      
*      DigitalCavalry
* Author URI:
*      http://themeforest.net/user/DigitalCavalry
* Contact:
*      digitalcavalry@gmail.com 
***********************************************************************/
define('CMS_PATH', TEMPLATEPATH . '/cms');
define('CMS_TURN_OFF_AUTOSAVE', true);
define('CMS_THEME_NAME', 'MElegance');
define('CMS_THEME_NAME_UPPERCASE', 'MODERNELEGANCE'); 
define('CMS_TXT_DOMAIN', 'dc_theme');

define('CMS_NOT_SELECTED', -1);

define('CMS_FONT_TYPE_CUFON', 1);
define('CMS_FONT_TYPE_GOOGLE', 2);

define('CMS_CSS_AREA', 1);
define('CMS_JAVASCRIPT_AREA', 2);

define('CMS_SIDEBAR_LEFT', 1);
define('CMS_SIDEBAR_RIGHT', 2);  
define('CMS_SIDEBAR_GLOBAL', 3);
define('CMS_SIDEBAR_HIDDEN', 4);

define('CMS_LINK_TYPE_PAGE', 1);
define('CMS_LINK_TYPE_MANUALLY', 2);

define('CMS_LINK_TARGET_SELF', 1);
define('CMS_LINK_TARGET_BLANK', 2);

define('CMS_POST_LAYOUT_SMALL', 1);
define('CMS_POST_LAYOUT_MEDIUM', 2);  
define('CMS_POST_LAYOUT_BIG', 3);
define('CMS_POST_LAYOUT_DEFAULT', 4); 

define('CMS_SERVICE_LAYOUT_SMALL', 1);
define('CMS_SERVICE_LAYOUT_MEDIUM', 2);  
define('CMS_SERVICE_LAYOUT_BIG', 3);

define('CMS_PORTFOLIO_LAYOUT_TABLE3', 1);
define('CMS_PORTFOLIO_LAYOUT_TABLE4', 2); 
define('CMS_PORTFOLIO_LAYOUT_TABLE2', 3);
define('CMS_PORTFOLIO_LAYOUT_SIDEBAR', 4);  

define('CMS_TEAM_LAYOUT_SMALL', 1);
define('CMS_TEAM_LAYOUT_BIG', 2);
define('CMS_TEAM_LAYOUT_FULL', 3);

define('CMS_TOUR_EFFECT_FADE', 1);
define('CMS_TOUR_EFFECT_SLIDE', 2);

define('CMS_ORDER_ASC', 1);
define('CMS_ORDER_DESC', 2);

define('CMS_GALLERY_LAYOUT_COMPACT', 1);
define('CMS_GALLERY_LAYOUT_TABLE3', 2);
define('CMS_GALLERY_LAYOUT_TABLE5', 5);

define('CMS_MEDIAGALL_LAYOUT_TABLE3', 1);
define('CMS_MEDIAGALL_LAYOUT_TABLE4', 2);
define('CMS_MEDIAGALL_LAYOUT_SIDEBAR', 3);
define('CMS_MEDIAGALL_LAYOUT_TABLE2', 4); 

define('CMS_WP_CONTENT_PATH', WP_CONTENT_DIR.'/' );
define('CMS_WP_CONTENT_URL', WP_CONTENT_URL.'/' );
 
define('CMS_THEME_TEMP_FOLDER_NAME', CMS_THEME_NAME.'_temp');
define('CMS_THEME_TEMP_FOLDER_PATH', CMS_WP_CONTENT_PATH.CMS_THEME_TEMP_FOLDER_NAME);
define('CMS_THEME_TEMP_FOLDER_URL', CMS_WP_CONTENT_URL.CMS_THEME_TEMP_FOLDER_NAME); 
  


require_once(TEMPLATEPATH . '/cms/php/cp_classes.php'); 
require_once(TEMPLATEPATH . '/cms/php/cp_widgets.php'); 
require_once(TEMPLATEPATH . '/cms/php/cp_renderer.php'); 
require_once(TEMPLATEPATH . '/cms/php/cp_metapost.php');
require_once(TEMPLATEPATH . '/cms/php/cp_metapage.php');
require_once(TEMPLATEPATH . '/cms/php/cp_metatour.php'); 
require_once(TEMPLATEPATH . '/cms/php/cp_metahomepagetabs.php');
require_once(TEMPLATEPATH . '/cms/php/cp_metaservice.php');
require_once(TEMPLATEPATH . '/cms/php/cp_metamember.php');
require_once(TEMPLATEPATH . '/cms/php/cp_metaquestion.php');
require_once(TEMPLATEPATH . '/cms/php/cp_metastorypost.php');

// require_once(TEMPLATEPATH . '/cms/tinymce/tinymce.php');

require_once(TEMPLATEPATH . '/cms/php/cp_meta.php');     
require_once(TEMPLATEPATH . '/cms/php/cp_functions.php');
require_once(TEMPLATEPATH . '/cms/php/cp_floatingobjects.php');
require_once(TEMPLATEPATH . '/cms/php/cp_home.php');
require_once(TEMPLATEPATH . '/cms/php/cp_quicknewspanel.php');
require_once(TEMPLATEPATH . '/cms/php/cp_help.php'); 
require_once(TEMPLATEPATH . '/cms/php/cp_menu.php');
require_once(TEMPLATEPATH . '/cms/php/cp_general.php');
require_once(TEMPLATEPATH . '/cms/php/cp_progressslider.php');
require_once(TEMPLATEPATH . '/cms/php/cp_accordion.php');
require_once(TEMPLATEPATH . '/cms/php/cp_chainslider.php');   
require_once(TEMPLATEPATH . '/cms/php/cp_clientpanel.php'); 
require_once(TEMPLATEPATH . '/cms/php/cp_shortcodes.php');
require_once(TEMPLATEPATH . '/cms/php/cp_customposts.php'); 
require_once(TEMPLATEPATH . '/cms/php/cp_ajax.php'); 
require_once(TEMPLATEPATH . '/cms/php/cp_controlpanel.php');

function & GetDCCPInterface() // Get DC Control Panel Interface 
{
    $p = null;
    if(!isset($GLOBALS['cms_pcp']))
    {
        $GLOBALS['cms_pcp'] = new CDCThemeControlPanel(); 
    }
    $p = $GLOBALS['cms_pcp'];
    return $p;      
}
GetDCCPInterface();
      

 
      
 ?>
